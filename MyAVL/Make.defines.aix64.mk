################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using gxlc and gxlc++.

OBJECT_MODE = 64

################################################################################
### fedault for OS AIX ###
# AS        = as
# AR        = ar
# CC        = cc
# CCFLAGS   = -O
# LD        = ld
# CPP       = cpp
# CCC       = xlC
# ARFLAGS   = -rv
# LFLAGS    =
# YACC      = yacc
# CCSTD     = -qlanglvl=extc99
# CCCSTD    = -qlanglvl=extended

CC          = xlc_r
LD          = xlc++_r -o
CCC         = xlc++_r
CFLAGS      = -c $(CCSTD) -qlonglong -qmaxmem=-1 -qnoansialias -q$(OBJECT_MODE)
CCFLAGS     = -c $(CCCSTD) -qlonglong -qmaxmem=-1 -qnoansialias -q$(OBJECT_MODE)
LDFLAGS     = -brtl -bdynamic -b$(OBJECT_MODE)

#EOF
