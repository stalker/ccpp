// MyAVL.hpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#ifndef _MyAVL_HPP_
#define _MyAVL_HPP_

#define DEBUG 4

#ifdef DEBUG
# include "mydebug.hpp"
# define DEBUG_PRINT2(a,b) printd(a, b)
# define DEBUG_PRINT2LN(a,b) printd(a, b) << std::endl
# define DEBUG_PRINT3(a,b,c) printd(a, b, c)
# define DEBUG_PRINT3LN(a,b,c) printd(a, b, c) << std::endl
# define DEBUG_PRINT4(a,b,c,d) printd(a, b, c, d)
# define DEBUG_PRINT4LN(a,b,c,d) printd(a, b, c, d) << std::endl
# define DEBUG_PRINT5(a,b,c,d,e) printd(a, b, c, d, e)
# define DEBUG_PRINT5LN(a,b,c,d,e) printd(a, b, c, d, e) << std::endl
# define DEBUG_PRINT6(a,b,c,d,e,f) printd(a, b, c, d, e, f)
# define DEBUG_PRINT6LN(a,b,c,d,e,f) printd(a, b, c, d, e, f) << std::endl
#else
# define DEBUG_PRINT2(a,b)
# define DEBUG_PRINT2LN(a,b)
# define DEBUG_PRINT3(a,b,c)
# define DEBUG_PRINT3LN(a,b,c)
# define DEBUG_PRINT4(a,b,c,d)
# define DEBUG_PRINT4LN(a,b,c,d)
# define DEBUG_PRINT5(a,b,c,d,e)
# define DEBUG_PRINT5LN(a,b,c,d,e)
# define DEBUG_PRINT6(a,b,c,d,e,f)
# define DEBUG_PRINT6LN(a,b,c,d,e,f)
#endif

#define DEBUG_PRINTLN_go_right(c,k) DEBUG_PRINT4LN("go right: ", c->right, "; k: ", k);
#define DEBUG_PRINTLN_go_left(c,k) DEBUG_PRINT4LN("go left: ", c->left, "; k: ", k);
#define DEBUG_PRINTLN_c_p_k(c,p,k) DEBUG_PRINT6LN("current ", c,  " parent: ", p, "; k: ", k)
#define DEBUG_PRINTLN_c_k(c,k) DEBUG_PRINT4LN("current ", c, "; k: ", k)

#define SETWZ(x) std::setw(x) << std::setfill('0')
#define SETWS(x) std::setw(x) << std::setfill(' ')

#ifndef _XOPEN_SOURCE
# if defined(SOLARIS)       /* Solaris 10 */
#  define _XOPEN_SOURCE  600
# else
#  define _XOPEN_SOURCE  700
# endif
#endif

extern "C" {
#include "wgetopt.h"
}
#include "cpperror.hpp"

#include <cctype>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>

#ifndef  INT8
# define INT8 int_fast8_t
#endif
#ifndef  UINT32
# define UINT32 uint_fast32_t
#endif
#ifndef  SINT64
# define SINT64 int_fast64_t
#endif
#ifndef  UINT64
# define UINT64 uint_fast64_t
#endif
#ifndef  CAST_SINT64
# define CAST_SINT64(x) static_cast<SINT64>(x)
#endif
#ifndef  CAST_UINT64
# define CAST_UINT64(x) static_cast<UINT64>(x)
#endif


struct metricsStruct {
    SINT64 size:56;
    SINT64 hight:8;
    metricsStruct(UINT32 s, INT8 h) : size(s), hight(h) { /* None */ }
};

union metricsUnion {
    SINT64 raw;
    metricsStruct ms;
    metricsUnion(UINT32 s, INT8 h) : ms(s, h) { /* None */ }
};

template <class T = SINT64>
struct node {
    node<T> * left, * right;
    T key;
    metricsUnion mu;

    node(T k) 
    : left(nullptr), right(nullptr), key(k), mu(1, 0)
    { /* None */ DEBUG_PRINT2LN(" new node(): ", this); }

    ~node() {
        delete left;
        delete right;
        DEBUG_PRINT2LN("delete node(): ", this);
    }

    bool isLeftNull()  { return nullptr == left; }
    bool isRightNull() { return nullptr == right; }
    void link(node<T> * l, node<T> * r) { left = l; right = r; }
    void unLink()      { left = nullptr; right = nullptr; }
};

template <class T = SINT64>
struct avl {
    typedef node<T> node_t;

    avl() : root(nullptr) { /* None */ }

    virtual ~avl() { delete root; }

    INT8 checkBalance(node_t * c) { return rightHeight(c) - leftHeight(c); }
    INT8 height(node_t * c)       { return nullptr != c ? c->mu.ms.hight : -1; }
    INT8 leftHeight(node_t * c)   { return height(c->left); }
    INT8 rightHeight(node_t * c)  { return height(c->right); }
    UINT32 getSize(node_t * c)    { return nullptr != c ? c->mu.ms.size : 0; }
    UINT32 leftSize(node_t * c)   { return getSize(c->left); }
    UINT32 rightSize(node_t * c)  { return getSize(c->right); }

    INT8 leftLeftHeight(node_t * c) {
        return nullptr != c->left->left ? c->left->left->mu.ms.hight : -1; 
    }

    INT8 rightRightHeight(node_t * c) {
        return nullptr != c->right->right ? c->right->right->mu.ms.hight : -1; 
    }

    bool isBigLeftBias(node_t * c) {
        return 0 < checkBalance(c->left);
    }

    bool isBigRightBias(node_t * c) {
        return checkBalance(c->right) < 0;
    }

    void add(T k) {
        root = addNode(root, nullptr, k);
    }

    // Проход по пути поиска, пока не убедимся, что ключа в дереве нет.
    node_t * addNode(node_t * c, node_t * p, T k) {
        DEBUG_PRINTLN_c_p_k(c, p, k);
        // Включения новой вершины в дерево.
        if (nullptr == c)
            return new node_t(k);
        if (k < c->key) {
            DEBUG_PRINTLN_go_left(c, k);
            c->left = addNode(c->left, c,  k);
        } else if (c->key < k) {
            DEBUG_PRINTLN_go_right(c, k);
            c->right = addNode(c->right, c, k);
        }
        // Определение результирующих показателей балансировки.
        return balance(c); // назад по пути поиска и ...
        // проверка в каждой вершине показателя сбалансированности.
    }

    void erase(T k) {
        root = eraseNode(root, nullptr, k);
    }

    node_t * eraseNode(node_t * c, node_t * p, T k) {
        DEBUG_PRINTLN_c_p_k(c, p, k);
        if (nullptr == c)
            return nullptr;
        if (k < c->key) {
            DEBUG_PRINTLN_go_left(c, k);
            c->left = eraseNode(c->left, c,  k);
        } else if (c->key < k) {
            DEBUG_PRINTLN_go_right(c, k);
            c->right = eraseNode(c->right, c, k);
        } else {
            //  Если вершина — лист, то удалим  её и вызовем  балансировку всех
            //  её предков в порядке от родителя к корню.
            // Иначе найдём самую  близкую  по  значению  вершину  в  поддереве
            // наибольшей высоты (правом или левом) и переместим  её  на  место
            // удаляемой  вершины,  при  этом  вызвав  процедуру  её  удаления.
            node_t * l = c->left;
            node_t * r = c->right;
            c->unLink();
            delete c;
            if (nullptr == r) return l;
            node_t * min = subTreeFindMin(r);
            min->link(l, eraseMinNode(r));
            return balance(min);
        }
        return balance(c);
    }

    node_t * eraseMinNode(node_t * c) {
        if (c->isLeftNull()) {
            return c->right;
        }
        c->left = eraseMinNode(c->left);
        return balance(c);
    }

    node_t * eraseMaxNode(node_t * c) {
        if (c->isRightNull()) {
            return c->left;
        }
        c->right = eraseMinNode(c->right);
        return balance(c);
    }

    node_t * balance(node_t * c) {
        fixHeight(c); fixSize(c);
        if (2 == checkBalance(c)) {
            DEBUG_PRINT2LN(" BAD balance: ", checkBalance(c));
            DEBUG_PRINT2LN(" BAD node: ", c);
            if (isBigRightBias(c)) {
                DEBUG_PRINT2LN(" is  big  Right Bias: ", "yes");
                c->right = smallLeftRotation(c->right);
            }
            // Выполняем мaлый поворот и возвращаем вершину в поддереве после 
            return smallRightRotation(c); // поворота.
        }
        if (-2 == checkBalance(c)) {
            DEBUG_PRINT2LN(" BAD balance: ", checkBalance(c));
            DEBUG_PRINT2LN(" BAD node: ", c);
            if (isBigLeftBias(c)) {
                DEBUG_PRINT2LN(" is  big  Right Bias: ", "yes");
                c->left = smallRightRotation(c->left);
            }
            // Выполняем мaлый поворот и возвращаем вершину в поддереве после 
            return smallLeftRotation(c); // поворота.

        }
        return c; // Ничего делать не нужно. Возвращаем текущую ноду.
    }

    node_t * find(T k) {
        DEBUG_PRINT2LN(" k: ", k);
        return subTreeFind(root, k);
    }

    node_t * subTreeFind(node_t * c, T k) {
        DEBUG_PRINTLN_c_k(c, k);
        if (nullptr == c)
            return nullptr;
        if (k < c->key) {
            DEBUG_PRINTLN_go_left(c, k);
            return subTreeFind(c->left, k);
        } else if (c->key < k) {
            DEBUG_PRINTLN_go_right(c, k);
            return subTreeFind(c->right, k);
        }
        return c;
    }

    node_t * findMin() {
        DEBUG_PRINT2LN(" : ", "root");
        return subTreeFindMin(root);
    }

    node_t * subTreeFindMin(node_t * c) {
        DEBUG_PRINT2LN("current: ", c);
        if (nullptr != c && not c->isLeftNull()) {
            DEBUG_PRINT2LN("go left: ", c->left);
            return subTreeFindMin(c->left);
        }
        return c;
    }

    node_t * findMax() {
        DEBUG_PRINT2LN(" : ", "root");
        return subTreeFindMax(root);
    }

    node_t * subTreeFindMax(node_t * c) {
        DEBUG_PRINT2LN("current: ", c);
        if (nullptr != c && not c->isRightNull()) {
            DEBUG_PRINT2LN("go left: ", c->right);
            return subTreeFindMax(c->right);
        }
        return c;
    }

    void fixHeight(node_t * c) {
        DEBUG_PRINT4LN("current: ", c, "; old hight: ", c->mu.ms.hight);
        INT8 lh = leftHeight(c);
        INT8 rh = rightHeight(c);
        c->mu.ms.hight  = lh < rh ? rh + 1 : lh + 1;
        DEBUG_PRINT4LN("current: ", c, "; new hight: ", c->mu.ms.hight);
    }

    void updateHeightSizeUp(node_t * c) {
        if (nullptr == c) return;
        fixHeightSizeUpTo(root, c->key);
    }

    void fixHeightSizeUp(node_t * c) {
        DEBUG_PRINT4LN("current: ", c, "; old hight: ", c->mu.ms.hight);
        fixHeight(c); fixSize(c);
        DEBUG_PRINT4LN("current: ", c, "; new hight: ", c->mu.ms.hight);
    }

    void fixHeightSizeUpTo(node_t * c, T k) {
        DEBUG_PRINTLN_c_k(c, k);
        if (nullptr == c)
            return;
        if (k < c->key) {
            DEBUG_PRINTLN_go_left(c, k);
            fixHeightSizeUpTo(c->left, k);
        } else if (c->key < k) {
            DEBUG_PRINTLN_go_right(c, k);
            fixHeightSizeUpTo(c->right, k);
        }
        fixHeightSizeUp(c);
    }

    void fixSize(node_t * c) {
        DEBUG_PRINT4LN("current: ", c, "; old size: ", c->mu.ms.size);
        INT8 lh = leftSize(c);
        INT8 rh = rightSize(c);
        c->mu.ms.size  = rh + lh + 1;
        DEBUG_PRINT4LN("current: ", c, "; new size: ", c->mu.ms.size);
    }

    std::string nodeKeyToString(node_t * c) {
        return nullptr != c ? std::to_string(c->key) : "";
    }

    std::ostream & treePrint(std::ostream & outstm, const char * sep = "\n") {
        subTreePrint(outstm, root, nullptr, sep);
        return outstm;
    }

    void subTreePrint(std::ostream & outstm, node_t * c, node_t * p, const char * sep)
    {
        if (nullptr == c)
            return;
        subTreePrint(outstm, c->left, c, sep);
        outstm << SETWZ(8) << c
               <<        " key: " << SETWS(4)  << c->key
               <<      " (left: " << SETWZ(14) << c->left
               <<            "|"  << SETWS(4)  << nodeKeyToString(c->left)
               <<    ") (right: " << SETWZ(14) << c->right
               <<            "|"  << SETWS(4)  << nodeKeyToString(c->right)
               <<   ") (parent: " << SETWS(14) << p
               <<            "|"  << SETWS(4)  << nodeKeyToString(p)
               <<     ") hight: " << SETWS(3)  << c->mu.ms.hight
               <<       " size: " << c->mu.ms.size   << sep;
        subTreePrint(outstm, c->right, c, sep);
    }

    node_t * treeCheck() { return subTreeCheck(root); }

    node_t * subTreeCheck(node_t * c) {
        if (nullptr == c)
            return nullptr;
        DEBUG_PRINT2LN("go left: ", c->left);
        node_t * l = subTreeCheck(c->left);
        if (nullptr != l) return l;
        INT8 lh = leftHeight(c);
        INT8 rh = rightHeight(c);
        DEBUG_PRINT6LN("current: ", c,
                       "; hight: ", c->mu.ms.hight,
                       "; abs(lh-rh): ", std::abs(lh - rh));
        if (1 < std::abs(lh - rh))
            return c;
        DEBUG_PRINT2LN("go right: ", c->right);
        return subTreeCheck(c->right);
    }

    node_t * smallLeftRotation(node_t * a) {
        node_t * b = a->left;
        DEBUG_PRINT4LN("a: ", a, "         b: ",  b);
        DEBUG_PRINT4LN("b: ", b, "   a->left: ",    a->left);
        DEBUG_PRINT4LN("L: ", b->left, " b->left: ", b->left);
        DEBUG_PRINT4LN("C: ", b->right, " b->right: ", b->right);
        DEBUG_PRINT4LN("R: ", a->right, " a->right: ", a->right);
        a->left = b->right;        // switch C to a
        b->right = a;
        DEBUG_PRINT4LN("a: ", a, "         b: ",   b);
        DEBUG_PRINT4LN("L: ", b->left,  "  b->left: ", b->left);
        DEBUG_PRINT4LN("C: ", a->left,  "  a->left: ", a->left);
        DEBUG_PRINT4LN("R: ", a->right, " a->right: ", a->right);
        fixHeight(a); fixSize(a);
        fixHeight(b); fixSize(b);
        return b;
    }

    node_t * smallRightRotation(node_t * a) {
        node_t * b = a->right;
        DEBUG_PRINT4LN("a: ", a, "         b: ",  b);
        DEBUG_PRINT4LN("b: ", b, "  a->right: ",   a->right);
        DEBUG_PRINT4LN("L: ", a->left, " a->left: ", a->left);
        DEBUG_PRINT4LN("C: ", b->left, " b->left: ", b->left);
        DEBUG_PRINT4LN("R: ", b->right, " b->right: ", b->right);
        a->right = b->left;       // switch C to a
        b->left = a;              // swap a b | a to left b
        DEBUG_PRINT4LN("a: ", a, "         b: ",    b);
        DEBUG_PRINT4LN("L: ", a->left,  "  a->left: ", a->left);
        DEBUG_PRINT4LN("C: ", a->right, " a->right: ", a->right);
        DEBUG_PRINT4LN("R: ", b->right, " b->right: ", b->right);
        fixHeight(a); fixSize(a);
        fixHeight(b); fixSize(b);
        return b;
    }

    node_t * root;
};

#endif /* _MyAVL_HPP_ */

/* EOF */
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
