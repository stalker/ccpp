################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using gxlc and gxlc++.

OBJECT_MODE = 32

################################################################################
### fedault for OS AIX ###
# AS        = as
# AR        = ar
# CC        = cc
# CCFLAGS   = -O
# LD        = ld
# CPP       = cpp
# CCC       = xlC
# ARFLAGS   = -rv
# LFLAGS    =
# YACC      = yacc
# CCSTD     = -qlanglvl=extc99
# CCCSTD    = -qlanglvl=extended

CCSTD       =
CCCSTD      =
CC          = gxlc
LD          = gxlc++ -o
CCC         = gxlc++
CFLAGS      = -c $(CCSTD)
CCFLAGS     = -c $(CCCSTD)
#LDFLAGS     = -brtl -bdynamic -b$(OBJECT_MODE)
LDFLAGS     =

#EOF
