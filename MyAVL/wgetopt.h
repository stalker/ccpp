/* wingetopt.h */
/*
 * getopt for
 */


#ifdef __GNUC__
# include <getopt.h>
#else
# include <unistd.h>
#endif

#if defined(__WIN32__) || defined(__NT__) || defined(_WIN32)

#ifndef _WINGETOPT_H_
#define _WINGETOPT_H_

#ifdef __cplusplus
extern "C" {
#endif

extern int opterr;
extern int optind;
extern int optopt;
extern char *optarg;
extern int getopt(int argc, char **argv, char *opts);

#ifdef __cplusplus
}
#endif

#endif  /* __WIN32__ || __NT__ || _WIN32 */
#endif  /* _WINGETOPT_H_ */
