///////////////////////////////////////////////////////////////////////////////
// MyAVL.hpp
/* $Date$
 * $Id$
 * $Version: 0.75$
 * $Revision: 1$
 */
///////////////////////////////////////////////////////////////////////////////

#ifndef _MyAVL_HPP_
#define _MyAVL_HPP_

#define DEBUG 4

#ifdef DEBUG
# include "mydebug.hpp"
# define DEBUG_PRINT2(a,b) printd(a, b)
# define DEBUG_PRINT2LN(a,b) printd(a, b) << std::endl
# define DEBUG8_PRINT2LN(a,b) if (DEBUG > 8) { printd(a, b) << std::endl; }
# define DEBUG_PRINT4(a,b,c,d) printd(a, b, c, d)
# define DEBUG_PRINT4LN(a,b,c,d) printd(a, b, c, d) << std::endl
# define DEBUG8_PRINT4LN(a,b,c,d) \
         if (DEBUG > 8) { printd(a, b, c, d) << std::endl; }
# define DEBUG_PRINT6(a,b,c,d,e,f) printd(a, b, c, d, e, f)
# define DEBUG_PRINT6LN(a,b,c,d,e,f) printd(a, b, c, d, e, f) << std::endl
# define DEBUG8_PRINT6LN(a,b,c,d,e,f) \
         if (DEBUG > 8) { printd(a, b, c, d, e, f) << std::endl; }
#else
# define DEBUG_PRINT2(a,b)
# define DEBUG_PRINT2LN(a,b)
# define DEBUG8_PRINT2LN(a,b)
# define DEBUG_PRINT4(a,b,c,d)
# define DEBUG_PRINT4LN(a,b,c,d)
# define DEBUG8_PRINT4LN(a,b,c,d)
# define DEBUG_PRINT6(a,b,c,d,e,f)
# define DEBUG_PRINT6LN(a,b,c,d,e,f)
# define DEBUG8_PRINT6LN(a,b,c,d,e,f)
#endif

#define DEBUG8_PRINTLN_go_right(c,k) \
        DEBUG8_PRINT4LN("go right: ", c->right, "; k: ", k);
#define DEBUG8_PRINTLN_go_left(c,k) \
        DEBUG8_PRINT4LN("go left: ", c->left, "; k: ", k);
#define DEBUG8_PRINTLN_c_p_k(c,p,k) \
        DEBUG8_PRINT6LN("current ", c,  " parent: ", p, "; k: ", k)
#define DEBUG8_PRINTLN_old_hight(c) \
        DEBUG8_PRINT4LN("current: ", c, "; old hight: ", CAST_UINT32(c->hight));
#define DEBUG8_PRINTLN_new_hight(c) \
        DEBUG8_PRINT4LN("current: ", c, "; new hight: ", CAST_UINT32(c->hight));
#define DEBUG8_PRINTLN_old_size(c) \
        DEBUG8_PRINT4LN("current: ", c, "; old  size: ", CAST_UINT32(c->hight));
#define DEBUG8_PRINTLN_new_size(c) \
        DEBUG8_PRINT4LN("current: ", c, "; new  size: ", CAST_UINT32(c->hight));
#define DEBUG8_PRINTLN_c_k(c,k) DEBUG8_PRINT4LN("current ", c, "; k: ", k)

#define SETWZ(x) std::setw(x) << std::setfill('0')
#define SETWS(x) std::setw(x) << std::setfill(' ')

#ifndef _XOPEN_SOURCE
# if defined(SOLARIS)       /* Solaris 10 */
#  define _XOPEN_SOURCE  600
# else
#  define _XOPEN_SOURCE  700
# endif
#endif

extern "C" {
#include "wgetopt.h"
}
#include "cpperror.hpp"

#include <cctype>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>

#ifndef  INT8
# define INT8 int_fast8_t
#endif
#ifndef  UINT32
# define UINT32 uint_fast32_t
#endif
#ifndef  SINT64
# define SINT64 int_fast64_t
#endif
#ifndef SINT64_MIN
# define SINT64_MIN INT_FAST64_MIN
#endif
#ifndef SINT64_MAX
# define SINT64_MAX INT_FAST64_MAX
#endif
#ifndef  UINT64
# define UINT64 uint_fast64_t
#endif
#ifndef  CAST_UINT32
# define CAST_UINT32(x) static_cast<UINT32>(x)
#endif
#ifndef  CAST_SINT64
# define CAST_SINT64(x) static_cast<SINT64>(x)
#endif
#ifndef  CAST_UINT64
# define CAST_UINT64(x) static_cast<UINT64>(x)
#endif

template <class T = SINT64>
struct node {
    node<T> * left, * right;
    T key;
    UINT32 size;
    INT8 hight;

    node(T k)
    : left(nullptr), right(nullptr), key(k), size(1), hight(0)
    { /* None */ DEBUG8_PRINT2LN(" new node(): ", this); }

    ~node() {
        delete left;
        delete right;
        DEBUG8_PRINT2LN("delete node(): ", this);
    }

    bool isLeftNull()  { return nullptr == left; }
    bool isRightNull() { return nullptr == right; }
    void link(node<T> * l, node<T> * r) { left = l; right = r; }
    void unLink()      { left = nullptr; right = nullptr; }
};

template <class T = SINT64>
struct avl {
    typedef node<T> node_t;

    avl() : root(nullptr) { /* None */ }

    virtual ~avl() { delete root; }

    size_t size()   { return nullptr != root ? root->size  : 0; }
    size_t height() { return nullptr != root ? root->hight : 0; }

    INT8 checkBalance(node_t * c) { return rightHeight(c) - leftHeight(c); }
    INT8 hight(node_t * c)        { return nullptr != c ? c->hight : -1; }
    INT8 leftHeight(node_t * c)   { return hight(c->left); }
    INT8 rightHeight(node_t * c)  { return hight(c->right); }
    UINT32 getSize(node_t * c)    { return nullptr != c ? c->size : 0; }
    UINT32 leftSize(node_t * c)   { return getSize(c->left); }
    UINT32 rightSize(node_t * c)  { return getSize(c->right); }

    INT8 leftLeftHeight(node_t * c) {
        return nullptr != c->left->left ? c->left->left->hight : -1;
    }

    INT8 rightRightHeight(node_t * c) {
        return nullptr != c->right->right ? c->right->right->hight : -1;
    }

    bool isBigLeftBias(node_t * c) {
        return 0 < checkBalance(c->left);
    }

    bool isBigRightBias(node_t * c) {
        return checkBalance(c->right) < 0;
    }

    void add(T k) {
        root = addNode(root, nullptr, k);
    }

    // Проход по пути поиска, пока не убедимся, что ключа в дереве нет.
    node_t * addNode(node_t * c, node_t * p, T k) {
        DEBUG8_PRINTLN_c_p_k(c, p, k);
        // Включения новой вершины в дерево.
        if (nullptr == c) {
            return new node_t(k);
        }
        if (k < c->key) {
            DEBUG8_PRINTLN_go_left(c, k);
            c->left = addNode(c->left, c,  k);
        } else if (c->key < k) {
            DEBUG8_PRINTLN_go_right(c, k);
            c->right = addNode(c->right, c, k);
        }
        // Определение результирующих показателей балансировки.
        return balance(c); // назад по пути поиска и ...
        // проверка в каждой вершине показателя сбалансированности.
    }

    void erase(T k) {
        root = eraseNode(root, nullptr, k);
    }

    node_t * eraseNode(node_t * c, node_t * p, T k) {
        DEBUG8_PRINTLN_c_p_k(c, p, k);
        if (nullptr == c)
            return nullptr;
        if (k < c->key) {
            DEBUG8_PRINTLN_go_left(c, k);
            c->left = eraseNode(c->left, c,  k);
        } else if (c->key < k) {
            DEBUG8_PRINTLN_go_right(c, k);
            c->right = eraseNode(c->right, c, k);
        } else {
            //  Если вершина — лист, то удалим  её и вызовем  балансировку всех
            //  её предков в порядке от родителя к корню.
            // Иначе найдём самую  близкую  по  значению  вершину  в  поддереве
            // наибольшей высоты (правом или левом) и переместим  её  на  место
            // удаляемой  вершины,  при  этом  вызвав  процедуру  её  удаления.
            node_t * l = c->left;
            node_t * r = c->right;
            c->unLink();
            delete c;
            if (nullptr == r) return l;
            node_t * min = subTreeFindMin(r);
            min->link(l, eraseMinNode(r));
            return balance(min);
        }
        return balance(c);
    }

    node_t * eraseMinNode(node_t * c) {
        if (c->isLeftNull()) {
            return c->right;
        }
        c->left = eraseMinNode(c->left);
        return balance(c);
    }

    node_t * eraseMaxNode(node_t * c) {
        if (c->isRightNull()) {
            return c->left;
        }
        c->right = eraseMinNode(c->right);
        return balance(c);
    }

    node_t * balance(node_t * c) {
        fixHeight(c); fixSize(c);
        if (2 == checkBalance(c)) {
            DEBUG8_PRINT2LN(" BAD balance: ", checkBalance(c));
            DEBUG8_PRINT2LN(" BAD node: ", c);
            if (isBigRightBias(c)) {
                DEBUG8_PRINT2LN(" is  big  Right Bias: ", "yes");
                c->right = smallLeftRotation(c->right);
            }
            // Выполняем мaлый поворот и возвращаем вершину в поддереве после
            return smallRightRotation(c); // поворота.
        }
        if (-2 == checkBalance(c)) {
            DEBUG8_PRINT2LN(" BAD balance: ", checkBalance(c));
            DEBUG8_PRINT2LN(" BAD node: ", c);
            if (isBigLeftBias(c)) {
                DEBUG8_PRINT2LN(" is  big  Right Bias: ", "yes");
                c->left = smallRightRotation(c->left);
            }
            // Выполняем мaлый поворот и возвращаем вершину в поддереве после
            return smallLeftRotation(c); // поворота.

        }
        return c; // Ничего делать не нужно. Возвращаем текущую ноду.
    }

    node_t * find(T k) {
        DEBUG8_PRINT2LN(" k: ", k);
        return subTreeFind(root, k);
    }

    node_t * subTreeFind(node_t * c, T k) {
        DEBUG8_PRINTLN_c_k(c, k);
        if (nullptr == c)
            return nullptr;
        if (k < c->key) {
            DEBUG8_PRINTLN_go_left(c, k);
            return subTreeFind(c->left, k);
        } else if (c->key < k) {
            DEBUG8_PRINTLN_go_right(c, k);
            return subTreeFind(c->right, k);
        }
        return c;
    }

    node_t * findMin() {
        DEBUG8_PRINT2LN(" : ", "root");
        return subTreeFindMin(root);
    }

    node_t * subTreeFindMin(node_t * c) {
        DEBUG8_PRINT2LN("current: ", c);
        if (nullptr != c && not c->isLeftNull()) {
            DEBUG8_PRINT2LN("go left: ", c->left);
            return subTreeFindMin(c->left);
        }
        return c;
    }

    node_t * findMax() {
        DEBUG8_PRINT2LN(" : ", "root");
        return subTreeFindMax(root);
    }

    node_t * subTreeFindMax(node_t * c) {
        DEBUG8_PRINT2LN("current: ", c);
        if (nullptr != c && not c->isRightNull()) {
            DEBUG8_PRINT2LN("go left: ", c->right);
            return subTreeFindMax(c->right);
        }
        return c;
    }

    node_t * orderStatistics(node_t * c, size_t index) {
        if (nullptr == c or c->isLeftNull()) return nullptr;
        if (c->left->size + 1 == index)
            return c;
        if (index < c->left->size + 1)
            return orderStatistics(c->left, index);
        else
            return orderStatistics(c->right, index - c->left->size - 1);
    }

    void fixHeight(node_t * c) {
        DEBUG8_PRINTLN_old_hight(c);
        INT8 lh = leftHeight(c);
        INT8 rh = rightHeight(c);
        c->hight  = lh < rh ? rh + 1 : lh + 1;
        DEBUG8_PRINTLN_new_hight(c);
    }

    void updateHeightSizeUp(node_t * c) {
        if (nullptr == c) return;
        fixHeightSizeUpTo(root, c->key);
    }

    void fixHeightSize(node_t * c) {
        DEBUG8_PRINTLN_old_hight(c);
        fixHeight(c); fixSize(c);
        DEBUG8_PRINTLN_new_hight(c);
    }

    void fixHeightSizeUpTo(node_t * c, T k) {
        DEBUG8_PRINTLN_c_k(c, k);
        if (nullptr == c)
            return;
        if (k < c->key) {
            DEBUG8_PRINTLN_go_left(c, k);
            fixHeightSizeUpTo(c->left, k);
        } else if (c->key < k) {
            DEBUG8_PRINTLN_go_right(c, k);
            fixHeightSizeUpTo(c->right, k);
        }
        fixHeightSize(c);
    }

    void fixSize(node_t * c) {
        DEBUG8_PRINTLN_old_size(c);
        UINT32 lh = leftSize(c);
        UINT32 rh = rightSize(c);
        c->size  = rh + lh + 1;
        DEBUG8_PRINTLN_new_size(c);
    }

    std::string nodeKeyToString(node_t * c) {
        return nullptr != c ? std::to_string(c->key) : "";
    }

    void printRange(T l, T r) {
        bool in = false, out = false;
        subPrintRange(root, nullptr, l, r, in, out);
    }

    void subPrintRange(node_t * c, node_t * p, T l, T r, bool & i, bool & o) {
        DEBUG_PRINT4LN("current ", c,  " parent: ", p);
        DEBUG_PRINT4LN("; l: ", l, "; r: ", r); // left right
        DEBUG_PRINT4LN("; i: ", i, "; o: ", o); // in   out
        if (nullptr == c) return;
        subPrintRange(c->left, c, l, r, i, o);
        if (l <= c->key and c->key <= r)
            std::cout << c->key << " ";
        subPrintRange(c->right, c, l, r, i, o);
    }

    std::ostream & treeKeyOutInOrder(
      std::ostream & outstm, const char * sep = " "
      ) {
        subTreeKeyOutInOrder(outstm, root, sep);
        return outstm;
    }

    void subTreeKeyOutInOrder(
      std::ostream & outstm, node_t * c, const char * sep
      ) {
        if (nullptr == c) return;
        subTreeKeyOutInOrder(outstm, c->left, sep);
        outstm << c->key << sep;
        subTreeKeyOutInOrder(outstm, c->right, sep);
    }

    std::ostream & treeKeyOutPreOrder(
      std::ostream & outstm, const char * sep = " "
      ) {
        subTreeKeyOutPreOrder(outstm, root, sep);
        return outstm;
    }

    void subTreeKeyOutPreOrder(
      std::ostream & outstm, node_t * c, const char * sep
      ) {
        outstm << c->key << sep;
        if (not c->isLeftNull())
            subTreeKeyOutPreOrder(outstm, c->left, sep);
        if (not c->isRightNull())
            subTreeKeyOutPreOrder(outstm, c->right, sep);
    }

    std::ostream & treeKeyOutPostOrder(
      std::ostream & outstm, const char * sep = " "
      ) {
        subTreeKeyOutPostOrder(outstm, root, sep);
        return outstm;
    }

    void subTreeKeyOutPostOrder(
      std::ostream & outstm, node_t * c, const char * sep
      ) {
        if (nullptr == c) return;
        subTreeKeyOutPostOrder(outstm, c->left, sep);
        subTreeKeyOutPostOrder(outstm, c->right, sep);
        outstm << c->key << sep;
    }

    std::ostream & treePrint(std::ostream & outstm, const char * sep = "\n") {
        subTreePrint(outstm, root, nullptr, sep);
        return outstm;
    }

    void subTreePrint(
      std::ostream & outstm, node_t * c, node_t * p, const char * sep
      ) {
        if (nullptr == c)
            return;
        subTreePrint(outstm, c->left, c, sep);
        outstm << SETWZ(8) << c
               <<        " key: " << SETWS(4)  << c->key
               <<      " (left: " << SETWZ(14) << c->left
               <<            "|"  << SETWS(4)  << nodeKeyToString(c->left)
               <<    ") (right: " << SETWZ(14) << c->right
               <<            "|"  << SETWS(4)  << nodeKeyToString(c->right)
               <<   ") (parent: " << SETWS(14) << p
               <<            "|"  << SETWS(4)  << nodeKeyToString(p)
               <<     ") hight: " << SETWS(3)  << CAST_UINT32(c->hight)
               <<       " size: " << c->size   << sep;
        subTreePrint(outstm, c->right, c, sep);
    }

    bool searchTreeCheck(T min, T max) {
        return (nullptr == getIncorrectNodeInSearchTree(root, min, max));
    }

    node_t * getIncorrectNodeInSearchTree(node_t * c, T min, T max) {
        if (nullptr == c) return nullptr;
        if (c->key <= min or max <= c->key) return c;
        node_t * l = getIncorrectNodeInSearchTree(c->left, min, c->key);
        if (nullptr != l) return l;
        return getIncorrectNodeInSearchTree(c->right, c->key, max);
    }

    bool avlCheck() { return (nullptr == avlSubTreeCheck(root)); }

    node_t * avlSubTreeCheck(node_t * c) {
        if (nullptr == c)
            return nullptr;
        DEBUG8_PRINT2LN("go left: ", c->left);
        node_t * l = avlSubTreeCheck(c->left);
        if (nullptr != l) return l;
        INT8 lh = leftHeight(c);
        INT8 rh = rightHeight(c);
        DEBUG8_PRINT6LN("current: ", c,
                        "; hight: ", c->hight,
                        "; abs(lh-rh): ", std::abs(lh - rh));
        if (1 < std::abs(lh - rh))
            return c;
        DEBUG8_PRINT2LN("go right: ", c->right);
        return avlSubTreeCheck(c->right);
    }

    node_t * smallLeftRotation(node_t * a) {
        node_t * b = a->left;
        DEBUG8_PRINT4LN("a: ", a, "         b: ",  b);
        DEBUG8_PRINT4LN("b: ", b, "   a->left: ",    a->left);
        DEBUG8_PRINT4LN("L: ", b->left, " b->left: ", b->left);
        DEBUG8_PRINT4LN("C: ", b->right, " b->right: ", b->right);
        DEBUG8_PRINT4LN("R: ", a->right, " a->right: ", a->right);
        a->left = b->right;        // switch C to a
        b->right = a;
        DEBUG8_PRINT4LN("a: ", a, "         b: ",   b);
        DEBUG8_PRINT4LN("L: ", b->left,  "  b->left: ", b->left);
        DEBUG8_PRINT4LN("C: ", a->left,  "  a->left: ", a->left);
        DEBUG8_PRINT4LN("R: ", a->right, " a->right: ", a->right);
        fixHeight(a); fixSize(a);
        fixHeight(b); fixSize(b);
        return b;
    }

    node_t * smallRightRotation(node_t * a) {
        node_t * b = a->right;
        DEBUG8_PRINT4LN("a: ", a, "         b: ",  b);
        DEBUG8_PRINT4LN("b: ", b, "  a->right: ",   a->right);
        DEBUG8_PRINT4LN("L: ", a->left, " a->left: ", a->left);
        DEBUG8_PRINT4LN("C: ", b->left, " b->left: ", b->left);
        DEBUG8_PRINT4LN("R: ", b->right, " b->right: ", b->right);
        a->right = b->left;       // switch C to a
        b->left = a;              // swap a b | a to left b
        DEBUG8_PRINT4LN("a: ", a, "         b: ",    b);
        DEBUG8_PRINT4LN("L: ", a->left,  "  a->left: ", a->left);
        DEBUG8_PRINT4LN("C: ", a->right, " a->right: ", a->right);
        DEBUG8_PRINT4LN("R: ", b->right, " b->right: ", b->right);
        fixHeight(a); fixSize(a);
        fixHeight(b); fixSize(b);
        return b;
    }

    node_t * root;
};

#endif /* _MyAVL_HPP_ */

///////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
