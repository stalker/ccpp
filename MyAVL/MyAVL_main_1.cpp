// MyAVL_main_1.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */


#include <cfloat>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include <algorithm>
#include <iostream>
#include <locale>
#include <vector>

#include "MyAVL.hpp"

// #define DEBUG 4

#ifdef DEBUG
# include "mydebug.hpp"
#endif

/**
 * 
 */

int test0(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-0-----------------------------------" << std::endl;
    node<> n(0);
    outstm << "sizeof: " << sizeof(n) << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    avl<> a;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 40" << std::endl;
    a.add(40);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 20" << std::endl;
    a.add(20);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 60" << std::endl;
    a.add(60);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 30" << std::endl;
    a.add(30);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "FIND 30" << std::endl;
    outstm << a.find(30) << std::endl;
    outstm << "FIND 10" << std::endl;
    outstm << a.find(10) << std::endl;
    outstm << "FIND 25" << std::endl;
    outstm << a.find(25) << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 25" << std::endl;
    a.add(25);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 35" << std::endl;
    a.add(35);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 27" << std::endl;
    a.add(27);
    outstm << "a.root: " << a.root << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "--END--TEST-0------------------------------" << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    return 0;
}

int test1(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-1-----------------------------------" << std::endl;
    node<> * n50 = new node<>(50);
    node<> * n25 = new node<>(25);
    node<> * n75 = new node<>(75);
    n50->left = n25;
    n50->right = n75;
    node<> * n20 = new node<>(20);
    node<> * n30 = new node<>(30);
    n25->left = n20;
    n25->right = n30;
    node<> * n17 = new node<>(17);
    node<> * n22 = new node<>(22);
    n20->left = n17;
    n20->right = n22;
    node<> * n27 = new node<>(27);
    node<> * n32 = new node<>(32);
    n30->left = n27;
    n30->right = n32;
    node<> * n70 = new node<>(70);
    node<> * n80 = new node<>(80);
    n75->left = n70;
    n75->right = n80;
    node<> * n67 = new node<>(67);
    node<> * n72 = new node<>(72);
    n70->left = n67;
    n70->right = n72;
    node<> * n73 = new node<>(73);
    n72->right = n73;
    node<> * n77 = new node<>(77);
    node<> * n82 = new node<>(82);
    n80->left = n77;
    n80->right = n82;

    avl<> a;
    a.root = n50;
    a.updateHeightSizeUp(n17);
    a.updateHeightSizeUp(n22);
    a.updateHeightSizeUp(n25);
    a.updateHeightSizeUp(n27);
    a.updateHeightSizeUp(n32);
    a.updateHeightSizeUp(n50);
    a.updateHeightSizeUp(n67);
    a.updateHeightSizeUp(n70);
    a.updateHeightSizeUp(n73);
    a.updateHeightSizeUp(n75);
    a.updateHeightSizeUp(n77);
    a.updateHeightSizeUp(n82);

    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 74" << std::endl;
    a.add(74);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "--END-TEST-1-------------------------------" << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    return 0;
}

int test2(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-2-----------------------------------" << std::endl;
    node<> * n50 = new node<>(50);
    node<> * n25 = new node<>(25);
    node<> * n75 = new node<>(75);
    n50->left = n25;
    n50->right = n75;

    node<> * n20 = new node<>(20);
    node<> * n30 = new node<>(30);
    n25->left = n20;
    n25->right = n30;

    node<> * n17 = new node<>(17);
    node<> * n22 = new node<>(22);
    n20->left = n17;
    n20->right = n22;

    node<> * n13 = new node<>(13);
    n17->left = n13;

    node<> * n27 = new node<>(27);
    node<> * n32 = new node<>(32);
    n30->left = n27;
    n30->right = n32;

    node<> * n70 = new node<>(70);
    node<> * n80 = new node<>(80);
    n75->left = n70;
    n75->right = n80;

    node<> * n67 = new node<>(67);
    node<> * n72 = new node<>(72);
    n70->left = n67;
    n70->right = n72;

    node<> * n77 = new node<>(77);
    node<> * n82 = new node<>(82);
    n80->left = n77;
    n80->right = n82;

    avl<> a;
    a.root = n50;
    a.updateHeightSizeUp(n17);
    a.updateHeightSizeUp(n30);
    a.updateHeightSizeUp(n72);
    a.updateHeightSizeUp(n80);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 14" << std::endl;
    a.add(14);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    std::vector<SINT64> other = {
        10, 11, 18, 19, 21, 24, 26, 28, 29, 31, 33, 88, 34, 63, 36, 74, 76, 78,
        37, 53, 39, 40, 58, 42, 51, 43, 87, 45, 47, 48, 49, 66, 52, 54, 41, 55,
        56, 68, 57, 59, 60, 61, 12, 23, 15, 38, 16, 62, 64, 46, 65, 69, 71, 73,
        79, 85, 81, 83, 35, 84, 86, 89, 44, 90 };
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ADD " << *it << std::endl;
        a.add(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "PRINT" << std::endl;
        a.treePrint(outstm);
    }
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ERASE " << *it << std::endl;
        a.erase(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
        outstm << "CHECK2: " << std::boolalpha
               << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "--END-TEST-2-------------------------------" << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    return 0;
}

int test3(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-3-----------------------------------" << std::endl;
    avl<> a;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "FIND_MIN" << std::endl;
    outstm << a.nodeKeyToString(a.findMin()) << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 50" << std::endl;
    a.add(50);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 25" << std::endl;
    a.add(25);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 75" << std::endl;
    a.add(75);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ADD 30" << std::endl;
    a.add(30);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "FIND_MIN" << std::endl;
    node<> * t = a.findMin();
    outstm << a.nodeKeyToString(t) << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ERASE_MIN" << std::endl;
    a.eraseMinNode(a.root);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ERASE_MIN" << std::endl;
    a.eraseMinNode(a.root);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "a.root: " << a.root << std::endl;
    outstm << "ERASE_MAX" << std::endl;
    a.eraseMaxNode(a.root);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--END-TEST-3-------------------------------" << std::endl;
    return 0;
}

int test4(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-4-----------------------------------" << std::endl;
    avl<> a;
    std::vector<SINT64> other = {
        59, 1, 95, 81, 3, 4, 5, 6, 7, 45, 8, 9, 10, 11, 12, 13, 14, 73, 74, 15,
        55, 16, 20, 93, 94, 96, 97, 46, 98, 18, 84, 85, 91, 70, 79, 72, 17, 21,
        38, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32, 66, 33, 34, 35, 36, 37, 39,
        40, 41, 42, 43, 44, 47, 48, 49, 50, 51, 99, 52, 53, 54, 56, 19, 57, 60,
        86, 87, 88, 89, 90, 61, 62, 63, 64, 65, 67, 68, 69, 58, 75, 71, 77, 24,
        78, 80, 82, 83, 76, 92, 2
    };
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ADD " << *it << std::endl;
        a.add(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
        outstm << "CHECK2: " << std::boolalpha
               << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ERASE " << *it << std::endl;
        a.erase(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
        outstm << "CHECK2: " << std::boolalpha
               << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--END-TEST-4-------------------------------" << std::endl;
    return 0;
}

int test5(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-5-----------------------------------" << std::endl;
    node<> * n50 = new node<>(50);
    node<> * n25 = new node<>(25);
    node<> * n75 = new node<>(75);
    n50->left = n25;
    n50->right = n75;

    node<> * n20 = new node<>(20);
    node<> * n30 = new node<>(30);
    n25->left = n20;
    n25->right = n30;

    node<> * n17 = new node<>(17);
    node<> * n22 = new node<>(22);
    n20->left = n17;
    n20->right = n22;

    node<> * n13 = new node<>(13);
    n17->left = n13;

    node<> * n27 = new node<>(27);
    node<> * n32 = new node<>(32);
    n30->left = n27;
    n30->right = n32;

    node<> * n70 = new node<>(70);
    node<> * n80 = new node<>(80);
    n75->left = n70;
    n75->right = n80;

    node<> * n67 = new node<>(67);
    node<> * n72 = new node<>(72);
    n70->left = n67;
    n70->right = n72;

    node<> * n77 = new node<>(77);
    node<> * n82 = new node<>(82);
    n80->left = n77;
    n80->right = n82;

    avl<> a;
    a.root = n50;
    a.updateHeightSizeUp(n17);
    a.updateHeightSizeUp(n30);
    a.updateHeightSizeUp(n72);
    a.updateHeightSizeUp(n80);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "ADD 14" << std::endl;
    a.add(14);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT RANGE 20 - 76" << std::endl;
    a.printRange(20, 76);
    outstm << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT RANGE 21 - 76" << std::endl;
    a.printRange(21, 76);
    outstm << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT RANGE 21 - 77" << std::endl;
    a.printRange(21, 77);
    outstm << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    std::vector<SINT64> other = {
        10, 11, 18, 19, 21, 24, 26, 28, 29, 31, 33, 88, 34, 63, 36, 74, 76, 78,
        37, 53, 39, 40, 58, 42, 51, 43, 87, 45, 47, 48, 49, 66, 52, 54, 41, 55,
        56, 68, 57, 59, 60, 61, 12, 23, 15, 38, 16, 62, 64, 46, 65, 69, 71, 73,
        79, 85, 81, 83, 35, 84, 86, 89, 44, 90 };
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ADD " << *it << std::endl;
        a.add(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "PRINT" << std::endl;
        a.treePrint(outstm);
    }
    for (auto it = other.begin(); it != other.end(); ++it) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ERASE " << *it << std::endl;
        a.erase(*it);
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
        outstm << "CHECK2: " << std::boolalpha
               << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "PRINT" << std::endl;
    a.treePrint(outstm);
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "--END-TEST-5-------------------------------" << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    return 0;
}

int test100000random(std::istream & instm, std::ostream & outstm) {
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--TEST-100000------------------------------" << std::endl;
    std::srand(std::time(NULL));
    avl<> a;
    for (size_t i = 0; i < 100000; ++i) {
        SINT64 rnd = rand() % 9223372036829775808;
        outstm << "ADD " << rnd << std::endl;
        a.add(rnd);
        double size = static_cast<double>(a.size());
        double height = static_cast<double>(a.height());
        if ( height <= (1.45*std::log2(size + 2)) ) {
            outstm << "CHECK3: " << std::boolalpha
                   <<  true
                   << std::endl;
        } else {
            outstm << "CHECK3: " << std::boolalpha
                   <<  false << " " << i << " " << size << " " << height
                   << std::endl;
        }
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "--END-TEST-100000--------------------------" << std::endl;
    return 0;
}

int test250000(std::istream & instm, std::ostream & outstm) {
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--TEST-250000------------------------------" << std::endl;
    std::srand(std::time(NULL));
    avl<> a;
    SINT64 rnd = rand() % 9223372036829775808;
    for (size_t i = 0; i < 250000; ++i) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK4: " << std::boolalpha <<(a.size() == i)<< std::endl;
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ADD " << i << std::endl;
        a.add(rnd + i);
        double size = static_cast<double>(a.size());
        double height = static_cast<double>(a.height());
        if ( height <= (1.45*std::log2(size + 2)) ) {
            outstm << "CHECK3: " << std::boolalpha
                   <<  true
                   << std::endl;
        } else {
            outstm << "CHECK3: " << std::boolalpha
                   <<  false << " " << i << " " << size << " " << height
                   << std::endl;
        }
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK4: " << std::boolalpha <<(a.size() == 250000)<< std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    rnd = rand() % 9223372036829775808 + 250000;
    for (size_t i = 250000; i > 0; --i) {
        outstm << "-------------------------------------------" << std::endl;
        outstm << "CHECK4: " << std::boolalpha <<(a.size() == i)<< std::endl;
        // outstm << "CHECK4: " << a.size() << " " << i << std::endl;
        outstm << "-------------------------------------------" << std::endl;
        outstm << "ERASE " << a.root->key << std::endl;
        a.erase(a.root->key);
        double size = static_cast<double>(a.size());
        double height = static_cast<double>(a.height());
        if ( height <= (1.45*std::log2(size + 2)) ) {
            outstm << "CHECK3: " << std::boolalpha
                   <<  true
                   << std::endl;
        } else {
            outstm << "CHECK3: " << std::boolalpha
                   <<  false << " " << i << " " << size << " " << height
                   << std::endl;
        }
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK1: " << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "CHECK2: " << std::boolalpha
           << a.searchTreeCheck(SINT64_MIN, SINT64_MAX)     << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--END-TEST-250000--------------------------" << std::endl;
    return 0;
}

int test100000(std::istream & instm, std::ostream & outstm) {
    outstm << "--TEST-100000------------------------------" << std::endl;
    avl<> a;
    for (size_t i = 0; i < 100000; ++i) {
        a.add(i);
        node<> * t = a.avlSubTreeCheck(a.root);
        if (nullptr != t)
            outstm << "i: " << i << " => " << t << std::endl;
    }
    outstm << "-------------------------------------------" << std::endl;
    outstm << "CHECK: "  << std::boolalpha << a.avlCheck()  << std::endl;
    outstm << "-------------------------------------------" << std::endl;
    outstm << "--END-TEST-100000-------------------------" << std::endl;
    return 0;
}

int tests(std::istream & instm, std::ostream & outstm) try {
    // test0(instm, outstm);
    // test1(instm, outstm);
    // test2(instm, outstm);
    // test3(instm, outstm);
    // test4(instm, outstm);
    test5(instm, outstm);
    // test100000random(instm, outstm);
    // test250000(instm, outstm);
    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

int go(std::istream & instm, std::ostream & outstm) try {
    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

/* let's go */
int main(int argc, char *argv[]) {
#if defined(DEBUG) && defined(PRINTM)
    printd("argc: ", argc, "; argv: ", argv) << std::endl;
#endif
    // return tests(std::cin, std::cout);
    return test100000(std::cin, std::cout);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
