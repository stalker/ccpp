// cpperror.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cpperror.hpp>

namespace local
{
    extern "C" {
      int strerror_r(int errnum, char *buf, size_t buflen); /* XSI-compliant */
    }
}

std::ostream& cpperror(int err, const char* msg) {
    static char buf[BUF_SIZE + 1];
    static char str[BUF_SIZE / 2 + 32];
    int ret = local::strerror_r(err, str, BUF_SIZE / 2 + 31);
    if (ret == 0)
        snprintf(buf, BUF_SIZE, "%s: %s", msg, str);
    else
        snprintf(buf, BUF_SIZE, "%s: Unknown error %d", msg, err);
    std::cerr << buf;
    return std::cerr;
}


/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
