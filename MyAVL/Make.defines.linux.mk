################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using gcc and g++.

RANLIB      = echo
AR          = ar
AWK         = awk

CCSTD       = -std=c99
CXXSTD      = -std=c++11
CC          = gcc
LD          = g++ -o
CXX         = g++
#CFLAGS     = -ansi -I$(ROOT)/include -Wall -DLINUX -D_GNU_SOURCE $(EXTRA)
CFLAGS      = -c $(CCSTD) -Wall
CXXFLAGS    = -c $(CCCSTD) -Wall
CFLAGS     += -DLINUX -D_GNU_SOURCE $(EXTRA)
CXXFLAGS   += -DLINUX -D_GNU_SOURCE $(EXTRA)

LDFLAGS     =

#EOF
