/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#pragma pack(1)

#include <time.h>
#include <ctype.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>

/**
 * PARTY_FACTOR = 8   ->     262142 nodes, ~  5.28 Mb, ~0.5s ! ~0.07s
 * PARTY_FACTOR = 16  ->    1048573 nodes, ~ 21.13 Mb, ~4s   !    ~2s !  ~0.5s ! ~0.2s
 * PARTY_FACTOR = 128 ->   67108863 nodes, ~  1.32 Gb, ~32m  !   ~27s !   ~57s !  ~24s
 * PARTY_FACTOR = 160 ->  104857599 nodes, ~  2.05 Gb,       !   ~44s ! ~1m44s !  ~42s
 * PARTY_FACTOR = 256 -> =134217727 nodes, ~  2.64 Gb,       !  ~1m4s ! ~2m24s ! ~1m7s
 * PARTY_FACTOR = 256 -> =149127167 nodes, ~  2.93 Gb,       ! ~1m12s ! ~2m43s
 * PARTY_FACTOR = 256 -> =209715198 nodes, ~  4.12 Gb,       !        !  ~4m5s
 * PARTY_FACTOR = 256 ->  268435455 nodes, ~  5.28 Gb,       !        ! ~5m33s
 * PARTY_FACTOR = 512 -> =419430396 nodes, ~  8.25 Gb,       !        !~11m21s
 * PARTY_FACTOR = 512 -> =858993459 nodes, ~ 16.9  Gb,       !        !~27m34s
 * PARTY_FACTOR = 512 -> 1073741823 nodes, ~ 21.13 Gb,       !        !
 * PARTY_FACTOR = 725 ->=2147483647 nodes, ~ 42.36 Gb,       !        !
 * PARTY_FACTOR = 725 -> 2152960000 nodes, ~       Gb,       !        !
 */

/** with value
 * PARTY_FACTOR = 160 ->   34949119 nodes, 1,017,894,416 ! 14s
 */

#define KEY_TYPE                  unsigned
#define VALUE_TYPE                void *
#define SUBMASK_BITS              64
#define PARTY_FACTOR              160
#define NUM_PARTIES               (SUBMASK_BITS*PARTY_FACTOR)
#define SUBMASK_MAX               UINT64_MAX
#define LIM_PARTIES               (NUM_PARTIES/3)
#define LIM_NODES                 (NUM_PARTIES*LIM_PARTIES-1)
#define TEST_PARTY_BIT(_n, _p)    !! ( (_n[_p/SUBMASK_BITS]) & \
                                       (  1ull << (_p % SUBMASK_BITS) ) )
#define SET_PARTY_BIT(_n, _p)        ( (_n[_p/SUBMASK_BITS]) |= \
                                       (  1ull << (_p % SUBMASK_BITS) ) )
#define CLEAR_PARTY_BIT(_n, _p)      ( (_n[_p/SUBMASK_BITS]) &= \
                                        ~(1ull << (_p % SUBMASK_BITS) ) )

typedef uint32_t uiptr_t;

struct BasicNode {
    uiptr_t left_;
    uiptr_t right_;
};

struct __attribute__((__packed__)) Node {
    size_t     size_;
    VALUE_TYPE value_;
    uiptr_t    left_;
    uiptr_t    right_;
    KEY_TYPE   key_;
    uint8_t    hight_;
};

typedef struct Node node_t;
typedef node_t * pnode_t;

struct __attribute__((__packed__)) PartyNodes {
    node_t   members[NUM_PARTIES];
    uint64_t mask[PARTY_FACTOR];
};

typedef struct PartyNodes party_t;

#define MAX_MEMORY                ( ( NUM_PARTIES*sizeof(node_t) \
                                      + PARTY_FACTOR*sizeof(uint64_t) ) \
                                      * NUM_PARTIES )

struct PoolOfParties {
    party_t * parties;
    size_t    size;
    uint64_t  mask[PARTY_FACTOR];
};

typedef struct PoolOfParties pool_t;

node_t * getNode(pool_t p, uiptr_t idx) {
    return &p.parties[idx / NUM_PARTIES].members[idx % NUM_PARTIES];
}

int isMaxParties(party_t * p) {
    for (int idx = 0; idx < PARTY_FACTOR; ++idx)
        if (p->mask[idx] != SUBMASK_MAX)
            return 0;
    return 1;
}

uiptr_t newNode(pool_t * p, pnode_t * n) {
    int idx = 0;         // idx - Index by parties

    for (int kdx = 0; kdx < PARTY_FACTOR; ++kdx) {
        idx = SUBMASK_BITS*kdx;
        if (p->mask[kdx] != SUBMASK_MAX)
            break;
    }

    for ( /* None */; idx < p->size; ++idx)
        if ( ! TEST_PARTY_BIT(p->mask, idx) ) {
            int jdx = 0; // jdx - Index by members

            for (int kdx = 0; kdx < PARTY_FACTOR; ++kdx) {
                jdx = SUBMASK_BITS*kdx;
                if (p->parties[idx].mask[kdx] != SUBMASK_MAX)
                    break;
            }

            for ( /*None */; jdx < NUM_PARTIES; ++jdx)
                if ( ! TEST_PARTY_BIT(p->parties[idx].mask, jdx) ) {
                    SET_PARTY_BIT(p->parties[idx].mask, jdx);

                    if (isMaxParties(&p->parties[idx]))
                        SET_PARTY_BIT(p->mask, idx);

                    *n = &p->parties[idx].members[jdx];
                    (*n)->left_ = (*n)->right_ = 0;

                    return NUM_PARTIES*idx + jdx; // Total Index
                }
        }
    *n = NULL;

    return 0;
}

int freeNode(pool_t * p, uiptr_t idx) {
    if ((idx / NUM_PARTIES) < p->size) {
        CLEAR_PARTY_BIT(p->mask, idx / NUM_PARTIES);
        CLEAR_PARTY_BIT(p->parties[idx / NUM_PARTIES].mask, idx % NUM_PARTIES);

        return 1;
    }
    return 0;
}

pool_t * createPool(size_t number) {
    pool_t * pool = (pool_t *)malloc(sizeof(pool_t));

    if (NULL == pool) return NULL;

    pool->size = number;
    pool->parties = (party_t *)calloc(number, sizeof(party_t));

    if (NULL != pool->parties) {
        SET_PARTY_BIT(pool->parties[0].mask, 0); // pseudo-NULL

        for (int idx = 0; idx < PARTY_FACTOR; ++idx)
            pool->mask[idx] = 0;

        return pool;
    }
    free(pool);

    return NULL;
}

int printMasks(pool_t * p, uiptr_t in) {
    fputs("pool->mask: 0x", stdout);
    for (int idx = 0; idx < PARTY_FACTOR; ++idx) {
        printf("%016llX", p->mask[idx]);
    }
    printf(" pool->parties[%d].mask: ", in / NUM_PARTIES);
    for (int idx = 0; idx < PARTY_FACTOR; ++idx) {
        printf("%016llX", p->parties[in / NUM_PARTIES].mask[idx]);
    }
}

inline int isNull(pool_t p, uiptr_t idx) {
    return ! TEST_PARTY_BIT(p.parties[idx / NUM_PARTIES].mask,
                            idx % NUM_PARTIES);
}

int isLeftNull(pool_t p, uiptr_t idx)  { return 0 == getNode(p, idx)->left_; }
int isRightNull(pool_t p, uiptr_t idx) { return 0 == getNode(p, idx)->right_; }

void link(pool_t p, uiptr_t nidx, uiptr_t lidx, uiptr_t ridx) {
    node_t * n = getNode(p, nidx);
    n->left_ = lidx;
    n->right_ = ridx;
}

void unLink(pool_t p, uiptr_t nidx) {
    node_t * n = getNode(p, nidx);
    n->left_ = 0;
    n->right_ = 0;
}

void unLinkLeft(pool_t p, uiptr_t nidx) {
    node_t * n = getNode(p, nidx);
    n->left_ = 0;
}

void unLinkRight(pool_t p, uiptr_t nidx) {
    node_t * n = getNode(p, nidx);
    n->left_ = 0;
    n->right_ = 0;
}

uiptr_t left(pool_t p, uiptr_t nidx)  { return getNode(p, nidx)->left_; }
uiptr_t right(pool_t p, uiptr_t nidx) { return getNode(p, nidx)->right_; }

void sleep_us(unsigned int nusecs) {
    struct timeval    tval;

    tval.tv_sec = nusecs / 1000000;
    tval.tv_usec = nusecs % 1000000;
    select(0, NULL, NULL, NULL, &tval);
}
/* let's go */
int main(int argc, char *argv[]) {
    printf("sizeof(uiptr_t) = %d\n", sizeof(uiptr_t));
    printf("sizeof(node_t)  = %d\n", sizeof(node_t));
    printf("sizeof(party_t) = %d\n", sizeof(party_t));
    printf("MAX_MEMORY : %llu\n", MAX_MEMORY);
    printf("LIM_PARTIES: %llu\n", LIM_PARTIES);
    printf("LIM_NODES  : %llu\n", LIM_NODES);
    pool_t * pool = createPool(LIM_PARTIES);
    printf("pool->parties[0].members: %p\n", pool->parties[0].members);
    if (NULL != pool) {
        int i = 0;
        for (i = 0; i < LIM_NODES; ++i) {
            pnode_t pn;
            uiptr_t in = newNode(pool, &pn);
            // printf("i: %8d, pn: %p, in: %-8u, ", i, pn, in);
            // printMasks(pool, in);
            // fputs("\n", stdout);
        }
        srand(time(NULL));
        int r = rand() % LIM_NODES;
        freeNode(pool, r);
        printf("r: %8d, ", r);
        printMasks(pool, r);
        fputs("\n", stdout);
        pnode_t pn;
        uiptr_t in = newNode(pool, &pn);
        printf("i: %8d, pn: %p, in: %-8u, ", i, pn, in);
        printMasks(pool, in);
        fputs("\n", stdout);
        fprintf(stderr, "DONE!\n");
        // sleep_us(25000000);
        free(pool->parties);
    }
    free(pool);
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
