////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////
// Compilation:
// Execution:
// Dependencies: node.hpp basictree.hpp

#ifndef _BSTREE_HPP_
#define _BSTREE_HPP_

#include <iomanip>
#include <iostream>
#include <sstream>

#include "node.hpp"
#include "basictree.hpp"
#include "ifacebst.hpp"

#undef DEBUG
#ifdef DEBUG
# include "mydebug.hpp"
#endif

namespace svn {

template<typename T, class NODE>
class BSTreeIterator;

////////////////////////////////////////////////////////////////////////////////
// Binary Search Tree class.
//
// The BSTree class represents an ordered set of comparable keys. It supports
// the usual add, contains, and delete methods.   It  also  provides  ordered
// methods for finding the minimum,  maximum,  floor,  and  ceiling  and  set
// methods for union, intersection, and equality.
//
// Even though this implementation include the method equals(), it  does  not
// support the method hashCode() because sets are mutable.
//
// The insert (add), find (contains),  remove  (delete),  findMin  (minimum),
// findMax (maximum), next, previous  and  remove  methods  take  logarithmic
// time in the worst case.
//
// The isEmpty operations take constant time.
// Standart Construction takes constant time.
// Explicit Construction takes constant time.
// Clone operation takes linear time.
// Verification (isBinarySearchTree) takes linear time.
//
//  @author Victor Skurikhin
//
template<typename T, class NODE = StoreNode<T> >
class BSTree : public BasicT<NODE>, virtual public IfaceBSTree<T> {
    friend class BSTreeIterator<T, NODE>;
    template <typename Y, class N>
    friend bool operator==(const BSTree<Y,N> &, const BSTree<Y,N> &);
    template <typename Y, class N>
    friend bool operator!=(const BSTree<Y,N> &, const BSTree<Y,N> &);
public:
    typedef NODE node_t;
    typedef BasicT<NODE> basict_t;
    typedef BSTreeIterator<T, NODE> iterator;
    typedef BSTreeIterator<T, const NODE> const_iterator;
    using basict_t::root;
    using basict_t::detailOut;

    ////////////////////////////////////////////////////////////////////////////

    BSTree()          { /* None */ } // Initializes an empty set.
    virtual ~BSTree() { /* None */ } // Virtual destructor.

    // Copy constructor and operator-function =.
    BSTree(const BSTree & other) : basict_t(other) { /* None */ }
    BSTree & operator=(const BSTree & rhs) {
        if (&rhs == this) return *this;
        auto newRoot = rhs.clone();
        delete root;
        root = newRoot;
        return *this;
    }

    BSTree(BSTree && other) { basict_t::move(std::move(other)); }
    BSTree & operator=(BSTree && o) { return basict_t::operator=(o); }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Adds the key to the set if it is not already present.
    // @param key the key to add
    // @return false if the set contains key and true otherwise
    // @throws std::bad_alloc if can't allocated a memory
    virtual bool insert(const T & key) throw(std::bad_alloc) {
        bool found = false;
        root = add(root, found, key);
        return not found;
    }

    // Does the set contain the given key?
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool find(const T & key) const {
        return isNull(searchNode(root, key)) ? false : true;
    }

    // Returns the smallest key in the set.
    // @return the smallest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMin() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return searchMinimum(root)->key();
    }

    // Returns the largest key in the set.
    // @return the largest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMax() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return searchMaximum(root)->key();
    }

    // Returns  the smallest key in  the set greater than  to key  or if this
    // set is empty returns the key.
    // @param key the key
    // @return the next key in the set greater than to key
    virtual const T & next(const T & key) const {
        node_t * successor = nextNode(key);
        return isNull(successor) ? key : successor->key();
    }

    // Returns the largest key in the set less than or equal to key.
    // @param key the key
    // @return the largest key in the set table less than or equal to key
    virtual const T & previous(const T & key) const {
        node_t * predecessor = prevNode(key);
        return isNull(predecessor) ? key : predecessor->key();
    }

    // Does the set contain the given key?
    // @param key the key
    // @return a pointer to the node_t that contains key
    const node_t * const searchNode(const T & key) const {
        return searchNode(root, key);
    }

    // @param key the key to extracting
    // @return a pointer to the extracted (by key) node,
    //         if tagret set is empty return the NULL
    node_t * extractNode(const T & key) { return extractNodeFrom(root, key); }
    node_t * extractMinimum()           { return extractMinimumFrom(root); }
    node_t * extractMaximum()           { return extractMaximumFrom(root); }

    size_t size()          const { return root->size(); }
    virtual bool isEmpty() const { return isNull(root); } // Is the set empty?

    // Removes the key from the set if the key is present.
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool remove(const T & key) {
        bool found = false;
        const node_t * deletedNode = extractNode(key);
        found = isNull(deletedNode) ? false : true;
        delete deletedNode;
        return found;
    }

    // Does this BSTree equal to other and the structure coincides  to other?
    // @param  o the other BStree
    // @return true  if  the two sets  are equal and structure  of both trees
    //         equal; false otherwise
    bool coincides(const BSTree & o) const {
        if (isNull(root))
            return isNull(o.root) ? true : false;
        return coincides(root, o.root);
    }

    // Does this BSTree equals o?
    // @param o the other BStree
    // @return true if the two sets are equal; false otherwise
    bool equals(const BSTree & o) const {
        iterator it = const_cast<BSTree &>(o).begin();
        return (isNull(root) and isNotNull(it)) ? false : equals(root, it);
    }

    // Does this binary tree satisfy symmetric order?
    // Note: this test also ensures that data structure is a binary tree since
    // order is strict
    virtual bool isBinarySearchTree() const {
        return binarySearchTreeCheck(std::numeric_limits<T>::lowest(),
                                     std::numeric_limits<T>::max());
    }

    bool binarySearchTreeCheck(T min, T max) const {
        return isNull(getIncorrectNodeInSearchTree(root, min, max));
    }

    // @param  c is a pointer to the root node of subtree
    // @param  min
    // @param  max
    const node_t * getIncorrectNodeInSearchTree(node_t * c, T min, T max) const
    {
        if (isNull(c)) return nullptr;

        if (c->key() <= min or max <= c->key()) return c;
        const node_t * l
            = getIncorrectNodeInSearchTree(c->left(), min, c->key());
        if (isNotNull(l)) return l;
        return  getIncorrectNodeInSearchTree(c->right(), c->key(), max);
    }

    /**
     * The Join proceudre takes this Binary Search  Tree  and  other  Binary
     * Search Tree as input, such that o, and any keyword in  this  is  less
     * than any keyword in o.  It unions of this and  other.   Other  Binary
     * Search Tree o will be empty.
     */
    // @param  o is a other BSTree class
    virtual void toMerge(BSTree & o) {
        root = merge(root, o.root);
        o.root = nullptr;
    }

    // @param  key the key
    // @return b is a BSTree class
    // @return c is a BSTree class
    virtual void splitTo(BSTree & b, BSTree & c, const T & key) {
        if (isNotNull(b.root) or isNotNull(c.root)) return;
        split(b.root, c.root, root, key);
        root = nullptr;
    }
    ////////////////////////////////////////////////////////////////////////////

    iterator begin() { return iterator(*this, searchMinimum(root)); }
    iterator end()   { return iterator(*this, nullptr); }

    const_iterator begin() const {
        return iterator(*this, searchMinimum(root));
    }
    const_iterator end() const { return iterator(*this, nullptr); }

    iterator rbegin() { return iterator(*this, searchMaximum(root)); }
    iterator rend()   { return iterator(*this, nullptr); }

protected:
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Insertion begins as a search would begin; if the key is not equal  to
     * that of the root, we search the left or  right  subtrees  as  before.
     * Eventually, we will reach an external node and add the new  key-value
     * pair (here encoded as a record 'new node_t(k)') as its right  or left
     * child, * depending on the node's key.  In other words, we examine the
     * root and recursively insert the new node to  the left  subtree if its
     * key is less  than  that of  the root, or  the  right  subtree  if its
     * key is greater than or equal to the root.
     * The part that is rebuilt uses O(log n) space in  the average case and
     * O(n) in the worst case.
     */
    // Adds the key to the set if it is not already present.
    // @param  c (current) is a pointer to the root of current subtree
    // @param  found the boolean attribute for return
    // @param  k the key
    // @return pointer to the root of current subtree
    // @return found feature is set to true if key already present
    //         otherwise found is set to false
    node_t * add(node_t * c, bool & found, const T & k)
      throw(std::bad_alloc) {
        if (isNull(c)) return new node_t(k);

        if (k < c->key())
            c->linkLeft(add(c->left(), found, k));
        else if (c->key() < k)
            c->linkRight(add(c->right(), found, k));
        else
            found = true;

        return c;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * We begin by examining the root node.  If the tree is null, the key we
     * are searching for does not exist in the tree.  Otherwise, if the  key
     * equals that of the root, the search is successful and we  return  the
     * node.  If the key is less than that of the root, we search  the  left
     * subtree.  Similarly, if the key is greater than that of the root,  we
     * search the right subtree.  This process is repeated until the key  is
     * found or the remaining subtree is null.  If the searched key  is  not
     * found after a null subtree is reached, then the key is not present in
     * the tree.
     * On average, binary search trees with n nodes have O(log n) height.
     * However, in the worst case, binary search trees can have O(n) height.
     */
    // Does the set contain the given key?
    // @param  current is a pointer to root node of subtree
    // @param  key the key
    // @return pointer to the searched node,  if tagret set is empty  return 
    //         the NULL and if node with key doesn't exist return NULL
    node_t * searchNode(const node_t * current, const T & key) const {
        if (isNull(current)) return nullptr;

        if (key < current->key())
            return searchNode(current->left(), key);
        else if (current->key() < key)
            return searchNode(current->right(), key);
        else
            return const_cast<node_t *>(current);

        return nullptr;
    }

    /* Just traverse the node from root  to left  recursively  until  left is
     * NULL. The node whose left is NULL is the node with minimum value.
     */
    // @param  current is a pointer to root node of subtree
    // @return a pointer to the node  with minimum,
    //         if tagret set is empty return the NULL
    node_t * searchMinimum(const node_t * current) const {
        if (isNull(current)) return nullptr;
        if (current->isLeftNull()) return const_cast<node_t *>(current);
        return searchMinimum(current->left());
    }

    /* Just traverse the node from root  to right recursively  until right is
     * NULL. The node whose right is NULL is the node with maximum value.
     */
    // @param  current is a pointer to the root node of subtree
    // @return a pointer to the node with maximum,
    //         if tagret set is empty return the NULL
    node_t * searchMaximum(const node_t * current) const {
        if (isNull(current)) return nullptr;
        if (current->isRightNull()) return const_cast<node_t *>(current);
        return searchMaximum(current->right());
    }

    // Finding the successor node of a node with key.
    // @param  key the key
    // @return a pointer to the node with successor key,
    //         if tagret set is empty return the NULL
    node_t * nextNode(const T & key) const {
        node_t * current = root, * successor = nullptr;

        while (isNotNull(current)) {
            if (key < current->key()) {
                successor = current;
                current = current->left();
            } else
                current = current->right();
        }

        return successor;
    }

    // Finding the successor node of a given node.
    // @param  x is a pointer to given node
    // @return a pointer to the node with successor key,
    //         if tagret set is empty return the NULL
    node_t * nextNode(const node_t * x) const {
        if (isNull(x)) return nullptr;
        return nextNode(x->key());
    }

    // Finding the predecessor node of a node with key.
    // @param  key the key
    // @return the pointer to the node with predecessor key,
    //         if tagret set is empty return the NULL
    node_t * prevNode(const T & key) const {
        node_t * current = root, * predecessor = nullptr;

        while (isNotNull(current)) {
            if (current->key() < key) {
                predecessor = current;
                current = current->right();
            } else
                current = current->left();
        }

        return predecessor;
    }

    // Finding the predecessor node of a given node.
    // @param x is a pointer to a given node
    // @return the pointer to the node with predecessor key,
    //         if tagret set is empty return the NULL
    node_t * prevNode(const node_t * x) const {
        if (isNull(x)) return nullptr;
        return prevNode(x->key());
    }

    /**
     * Just traverse the node from root  to left  recursively  until  left is
     * NULL. The node whose left is NULL is the node with minimum value.
     * The  procedure   for  deleting   the  minimum  of  from   the  subtree
     * (effectively extracting the minimum element)
     */
    // @param  current is a pointer to the root node of subtree
    // @param  parent is a pointer to the root of previous subtree,
    //         parent for node current
    // @return pointer to the root node of changed tree
    //         if tagret set is empty return the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMinimum(node_t *& result, node_t * current) {
        if (isNull(current)) return nullptr;

        if (current->isLeftNull()) {
            result = current;
            if (current->isRightNull())
                return nullptr;
            node_t * right = current->right();
            current->unLinkRight();
            return right;
        }

        current->linkLeft(extractMinimum(result, current->left()));
        return current;
    }

    // Wrapper function for extractMinimum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with minimum key,
    //         if tagret set is empty return the NULL
    node_t * extractMinimumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMinimum(result, fromRoot);
        return result;
    }

    /**
     * Just traverse the node from root  to right  recursively until right is
     * NULL. The node whose right is NULL is the node with maximum value.
     * Time Complexity: O(n) Worst case happens for right skewed trees.
     */
    // @param  current the pointer to root node of subtree
    // @return pointer to the root node of changed tree
    //         if tagret set is empty return the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMaximum(node_t *& result, node_t * current) {
        if (isNull(current)) return nullptr;

        if (current->isRightNull()) {
            result = current;
            if (current->isLeftNull())
                return nullptr;
            node_t * left = current->left();
            current->unLinkLeft();
            return left;
        }

        current->linkRight(extractMaximum(result, current->right()));
        return current;
    }

    // Wrapper function for extractMaximum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with maximum key, 
    //         if tagret set is empty return the NULL
    node_t * extractMaximumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMaximum(result, fromRoot);
        return result;
    }

    /**
     * When removing a node from a binary search tree  it  is  mandatory  to
     * maintain  the  in-order  sequence  of  the  nodes.   There  are  many
     * possibilities to do this.  However, the following  method  which  has
     * been proposed by T.  Hibbard in 1962 guarantees that the  heights  of
     * the subject subtrees are changed by at most  one.   There  are  three
     * possible cases to consider:
     * (1) Deleting  a node with  no children:  simply remove  the node from 
     *     the tree.
     * (2) Deleting  a node with one child:  remove the node  and replace it
     *     with its child.
     * (3) Deleting a node with two children: call the node to be deleted D.
     *     Do  not  delete  D.    Instead,  choose   either   its   in-order
     *     predecessor node or its in-order successor  node  as  replacement
     *     node E.  Copy the user values of E to D.  If E does  not  have  a
     *     child simply remove E from its previous parent G.   If  E  has  a
     *     child, say F, it is a right child. Replace E with F at E's
     *     parent.
     *                  D                  E                  E
     *                 / \                / \                / \
     *                /   +              /   +              /   +
     *               /   / \            /   / \            /   / \
     *              /   G  ...  ==>    /   G  ...  ==>    /   G  ...
     *             +   / \            +     \            +   / \
     *            / \ E   \          / \     \          / \ F   \
     *            ---  \   +         ---  F   +         ---      +
     *                  F / \                / \                / \
     *                    ---                ---                ---
     */
    // @param  result is a pointer to extracted node,
    //         place for returning node
    // @param  current is a pointer to root node of subtree
    // @param  key the key to extracting
    // @return pointer to the root of current subtree
    //         if tagret set is empty return the NULL
    // @return result is a pointer indicates the found and extracted node
    //         if key not found or if tagret set is empty result unchanged!
    node_t * extractNode(node_t *& result, node_t * current, const T & key) {
        if (isNull(current)) return nullptr;

        if (key < current->key()) {
            current->linkLeft(  extractNode(result, current->left(), key) );
        } else if (current->key() < key) {
            current->linkRight( extractNode(result, current->right(), key) );
        }   // (3)
        else if (current->left() != nullptr and current->right() != nullptr ) {
            result = current;
            node_t * left  = result->left();
            node_t * right = result->right(); // result->right() Must be change!
            current = extractMinimumFrom(right);                  // after this
            result->unLink();
            current->link(left, right);
        } else {
            // (1), (2)
            if (not current->isLeftNull()) {
                result = current;
                current = current->left();
                result->unLinkLeft();
            } else {
                result = current;
                current = current->right();
                // if (result == root) ???
                //     root = current; ???
                result->unLinkRight();
            }
        }

        return current;
    }

    // Wrapper function for extractNode.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @param  key the key to extracting
    // @return the pointer to the node with key, if key not found or if
    //         tagret set is empty return the NULL
    node_t * extractNodeFrom(node_t *& fromRoot, const T & key) {
        node_t * result = nullptr;
        root = extractNode(result, root, key);
        return result;
    }

    // @param  l the pointer to root node of first subtree
    // @param  r the pointer to root node of second subtree
    // @return true if the two sets are equal and the structure of both trees
    //         is the same; false otherwise
    bool coincides(const node_t * l, const node_t * r) const {
        if (isNull(l))
            return isNull(r) ? true : false;
        if (isNull(r)) return  false;
        return coincides(l->left(),  r->left())
           and l->key() == r->key()
           and coincides(l->right(), r->right());
    }

    // Does this set equals to other?
    // @param  current the pointer to root node of subtree
    // @param  it the iterator from other set
    // @return true if the two sets are equal; false otherwise
    bool equals(const node_t * c, iterator & it) const {
        if (isNull(it))
            return isNull(c) ? true : false;
        if (isNull(c)) return  true;
        return equals(c->left(), it)
           and c->key() == it++->key()
           and equals(c->right(), it);
    }

    // @param  v1 is a pointer to top (root) node of left tree
    // @param  v2 is a pointer to top (root) node of right tree
    // @param  t is a pointer to the root node of the unifying tree
    // @return a pointer to the root of merged tree
    node_t * mergeWithRoot(node_t * v1, node_t * v2, node_t * t) {
        if (isNull(t)) return nullptr;
        t->linkLeft(v1);
        t->linkRight(v2);
        return t;
    }

    /**
     * The Join proceudre takes v1 and v2 as input, such that v1 and v2  are
     * Binary Search trees, and any keyword in v1 is less than  any  keyword
     * in v2. It returns a Binary tree T that is the union of v1 and v2.  We
     * claim that this can be done in O(|ht(T1) − ht(T2)|), where ht denotes
     * the height.
     */
    // @param  v1 is a pointer to top (root) node of left tree
    // @param  v2 is a pointer to top (root) node of right tree
    // @return a pointer to the root of merged tree
    node_t * merge(node_t * v1, node_t * v2) {
        if (isNull(v1)) return v2;
        node_t * t = extractMaximumFrom(v1);
        t = mergeWithRoot(v1, v2, t);
        return t;
    }

    /**
     * The Split procedure takes T and k as inputs, such that T is an Binary
     * Search tree, and k is a keyword of T.  It returns Binary Search trees
     * T1, T2, such that T1 contains the elements that is less than k in  T,
     * and T2 contains those larger than k.  We claim that this can be  done
     * in O(ht(T)).
     */
    // @param  v is a pointer to root node of tree
    // @param  key the keyword
    // @return result1 a pointer to the root of left tree
    // @return result2 a pointer to the root of right tree
    void split(node_t *& result1, node_t *& result2, node_t * v, const T & key)
    {
        if (isNull(v)) return;
        if (key < v->key()) {
            node_t * v1 = nullptr, * v2 = nullptr;
            split(v1, v2, v->left(), key);
            result1 = v1;
            result2 = mergeWithRoot(v2, v->right(), v);
        } else {
            node_t * v1 = nullptr, * v2 = nullptr;
            split(v1, v2, v->right(), key);
            result1 = mergeWithRoot(v->left(), v1, v);
            result2 = v2;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Returns a string representation of key.
    // @return a string representation of key
    virtual std::string keyToString(const node_t * current) const {
        return nodeKeyToString<node_t>(current);
    }

    // Returns a string representation of node_t.
    // @return a string representation of node_t
    virtual std::string nodeToString(const node_t * current) const {
        return nodeKeyToString<node_t>(current);
    }

    // Returns a stream representation of this set.
    // @return a stream representation of this set, with the keys separated
    virtual std::ostream & treeOutInOrder(std::ostream & os) const {
        size_t index = 0;
        const char * sep = detailOut ? nl : sp;
        subTreeOutInOrder(os, root, index, sep);
        return os;
    }

    ////////////////////////////////////////////////////////////////////////////
    virtual void subTreeOutInOrder(
      std::ostream & os, const node_t * c, size_t & i, const char * sep
      ) const {
        if (isNull(c)) return;
        subTreeOutInOrder(os, c->left(), i, sep);
        if (detailOut) {
            nodeOutDetail(os, c);
            os << " index:" << i << sep;
        } else if (0 == i)
            os << keyToString(c);
        else
            os << sep << keyToString(c);
        i++;
        subTreeOutInOrder(os, c->right(), i, sep);
    }
};

} // namespace svn

#endif // _BSTREE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
