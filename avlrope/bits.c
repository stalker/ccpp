
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

#define test_bit(_n, _p)    !! ( _n & ( 1u << _p))
#define set_bit(_n, _p)     ((_n) |= 1 << (_p))
#define clear_bit(_n, _p)   ((_n) &= ~(1 << (_p)))

#define BIT_SIZE 32

int main(int argc, char * argv[]) {
    char * endl = NULL;
    uint32_t n = strtol(argv[1], &endl, 10);

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

    set_bit(n, 0); 

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

    set_bit(n, 2); 

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

    set_bit(n, 15); 

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

    set_bit(n, 31); 

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

    clear_bit(n, 1); 

    printf("\n");
    for (int i = 0; i < BIT_SIZE; i++)
        printf("no:%u -> %d\n", n, test_bit(n, i));

}
