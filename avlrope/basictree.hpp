////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _BASICTREE_HPP_
#define _BASICTREE_HPP_

#include <iomanip>
#include <iostream>
#include <queue>

#include "node.hpp"

#undef DEBUG
#ifdef DEBUG
# include "mydebug.hpp"
#endif

namespace svn {

////////////////////////////////////////////////////////////////////////////////
//
class IfaceBasicTree {
public:
    virtual ~IfaceBasicTree() { /* None */ }
    virtual void insert() = 0;
    virtual bool isEmpty() const = 0;
    virtual std::ostream & treeOutInOrder(std::ostream & os) const = 0;
};

////////////////////////////////////////////////////////////////////////////////
//
template<class NODE = BasicNode>
class BasicT {
public:
    typedef NODE node_t;

    BasicT() : root(nullptr) { /* None */ }
    virtual ~BasicT()        { delete root; }

    BasicT(const BasicT & other ) : root(other.clone()) { /* None */ }
    BasicT & operator=(const BasicT & rhs) {
        if (&rhs == this) return *this;
        auto newRoot = rhs.clone();
        delete root;
        root = newRoot;
        return *this;
    }

    BasicT(BasicT && other) { move(other); }
    BasicT & operator=(BasicT && other) {
        std::swap(root, other.root);
        return *this;
    }

    explicit BasicT(node_t * p) : root(p) { /* None */ }

    ////////////////////////////////////////////////////////////////////////////

    virtual void checkRoot(int l, const char * f) const throw(std::range_error)
    {
        if (nullptr == root)
            throw std::range_error("Operation with empty tree at line: " +
                  std::to_string(l) + std::string(" in file: ") + f);
    }

    virtual void checkNode(const node_t * n, int l, const char * f) const
      throw(std::range_error) {
        if (nullptr == n)
            throw std::range_error("Operation with nullptr Node at line: " +
                  std::to_string(l) + std::string(" in file: ") + f);
    }

    virtual const node_t * const top() const { return root; }

    // Create the self clone.
    // @return pointer to the root node of new tree
    // @throws std::bad_alloc if can't allocated a memory
    virtual node_t * clone() const throw(std::bad_alloc) {
        return clone(root);
    }

protected:
    // Create the self clone.
    // @param  current is a pointer to the root node of subtree
    // @return a pointer to the root node of new tree
    // @throws std::bad_alloc if can't allocated a memory
    virtual node_t * clone(const node_t * current) const throw(std::bad_alloc) {
        if (isNull(current)) return nullptr;
        node_t * result = new node_t(current->key());
        result->link(clone(current->left()), clone(current->right()));
        return result;
    }

    virtual void move(BasicT && other) {
        root = other.root;
        if (this != &other)
            other.root = nullptr;
    }

    node_t * root; // root of BST
public:
    bool detailOut = false;
};

////////////////////////////////////////////////////////////////////////////////
//
template<class NODE = BasicNode>
class BasicTree : public BasicT<NODE>, virtual public IfaceBasicTree {
public:
    typedef NODE node_t;
    using BasicT<NODE>::root;

    BasicTree() { /* None */ }
    virtual ~BasicTree() { /* None */ }

    BasicTree(const BasicTree &) = delete;
    BasicTree & operator=(const BasicTree &) = delete;

    BasicTree(BasicTree && other) { BasicT<NODE>::move(std::move(other)); }
    BasicTree & operator=(BasicTree && o) { return BasicT<NODE>::operator=(o); }

    ///////////////////////////////////////////////////////////////////////////

    virtual bool isEmpty() const { return isNull(root); }

    void insert() throw(std::bad_alloc) {
        if (isNull(root))
            root = new node_t;
        emptyInsert(root);
    }

    virtual bool coincides(BasicTree & o) const {
        if (isNull(root))
            return isNull(o.root) ? true : false;
        return coincides(root, o.root);
    }

private:

    ////////////////////////////////////////////////////////////////////////////
    void emptyInsert(const node_t * c) throw(std::bad_alloc) {
        std::queue<const node_t *> q;
        q.push(c);
        while (not q.empty()) {
            node_t * current = const_cast<node_t *>(q.front());
            q.pop();

            if (current->isLeftNull()) {
                current->linkLeft(new node_t);
                return;
            }
            else q.push(current->left());

            if (current->isRightNull()) {
                current->linkRight(new node_t);
                return;
            }
            else q.push(current->right());
        }
    }

    bool coincides(const node_t * l, const node_t * r) const {
        if (isNull(l))
            return isNull(r) ? true : false;
        if (isNull(r)) return  false;
        return coincides(l->left(),  r->left())
           and coincides(l->right(), r->right());
    }

};

} // namespace svn

#endif // _BASICTREE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
