// 3_2_Step_03_main_1.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include "avlrope.hpp"
#include "avltree.hpp"

su::svn::AVLRope<char> test6(char c) {
    su::svn::AVLRope<char> result;
    result.append(c);
    return result;
}

int test5(std::istream & instm, std::ostream & outstm) try {
    {
    const std::string line1  =  "ab";
    su::svn::AVLRope<char> str1, str2, str3, str4;
    outstm << std::boolalpha << (str1.equally(str2)) << std::endl;
    outstm << std::boolalpha << (str1.coincides(str2)) << std::endl;
    su::svn::getAVLRopeFromString(str1, line1);
    su::svn::getAVLRopeFromString(str2, line1);
    outstm << "--- str1:"  << std::endl;
    str1.treePrintByIndex(outstm);
    outstm << "--- str2:"  << std::endl;
    str2.treePrintByIndex(outstm);
    outstm << std::boolalpha << (str1.equally(str2)) << std::endl;
    outstm << std::boolalpha << (str1.coincides(str2)) << std::endl;
    su::svn::test1(str3);
    su::svn::test2(str4);
    outstm << "--- str3:"  << std::endl;
    str3.treePrintByIndex(outstm);
    outstm << "--- str4:"  << std::endl;
    str4.treePrintByIndex(outstm);
    outstm << std::boolalpha << (str3.equally(str4)) << std::endl;
    outstm << std::boolalpha << (str3.coincides(str4)) << std::endl;
    }
    {
    const std::string line1  =  "helloworld";
    su::svn::AVLRope<char> str1, str2, str3, str4;
    su::svn::getAVLRopeFromString(str1, line1);
    su::svn::getAVLRopeFromString(str2, line1);
    outstm << "--- str1:"  << std::endl;
    str1.treePrintByIndex(outstm);
    outstm << "--- str2:"  << std::endl;
    str2.treePrintByIndex(outstm);
    outstm << std::boolalpha << (str1.equally(str2)) << std::endl;
    outstm << std::boolalpha << (str1.coincides(str2)) << std::endl;
    su::svn::test3(str3);
    su::svn::test4(str4);
    outstm << "--- str3:"  << std::endl;
    str3.treePrintByIndex(outstm);
    str3.append('l');
    outstm << "--- str3:"  << std::endl;
    str3.treePrintByIndex(outstm);
    str3.append('l');
    str3.append('o');
    str3.append('w');
    str3.append('o');
    str3.append('r');
    str3.append('l');
    str3.append('d');
    for (size_t i = 0; i < str3.size(); ++i)
        outstm << i << ": " << str3[i] << " " << std::endl;
    outstm << "--- str3:"  << std::endl;
    str3.treePrintByIndex(outstm);
    str4.append('l');
    str4.append('l');
    str4.append('o');
    str4.append('w');
    str4.append('o');
    str4.append('r');
    str4.append('l');
    str4.append('d');
    outstm << "--- str4:"  << std::endl;
    str4.treePrintByIndex(outstm);
    outstm << std::boolalpha << (str3.equally(str4)) << std::endl;
    outstm << std::boolalpha << (str3.coincides(str4)) << std::endl;
    }
    /*
    const std::string line1  =  "hlelowrold";
    const std::string line2  =  "hellowrold";
    const std::string line3  =  "abcdef";
    const std::string line4  =  "cabdef";
    const std::string line5  =  "hellowrold";

    su::svn::AVLRope<char> str1, str2, str3, str4, str5;
    outstm << "str1: " << &str1 << std::endl;
    outstm << "str2: " << &str2 << std::endl;
    outstm << "str3: " << &str3 << std::endl;
    outstm << "str4: " << &str4 << std::endl;

    su::svn::getAVLRopeFromString(str1, line1);
    su::svn::getAVLRopeFromString(str2, line2);
    su::svn::getAVLRopeFromString(str5, line5);

    outstm << "str1: ";
    str1.treeOutString(outstm) << std::endl;
    outstm << "str2: ";
    str2.treeOutString(outstm) << std::endl;
    outstm << "str5: ";
    str5.treeOutString(outstm) << std::endl;

    outstm << std::boolalpha << (str1.equally(str5)) << std::endl;
    outstm << std::boolalpha << (str2.equally(str5)) << std::endl;

    str1.split(str2, str3, 0);
    outstm << "str1: ";
    str1.treeOutString(outstm) << std::endl;
    outstm << "str2: ";
    str2.treeOutString(outstm) << std::endl;
    outstm << "str3: ";
    str3.treeOutString(outstm) << std::endl;

    str4 = std::move(str2);

    str1 = std::move(str3 + str4);

    su::svn::AVLRope<char> str, str2;

    su::svn::getAVLRopeFromString(str, line1);

    su::svn::rope(str, 1, 1, 2);
    str.treeOutString(outstm) << std::endl;

    su::svn::rope(str, 6, 6, 7);
    str.treeOutString(outstm) << std::endl;

    su::svn::avlRopeDelete(str);

    su::svn::getAVLRopeFromString(str, line3);

    su::svn::rope(str, 0, 1, 1);
    str.treeOutString(outstm) << std::endl;

    su::svn::rope(str, 4, 5, 0);
    str.treeOutString(outstm) << std::endl;

    str2 = test6('Z');
    str2.treeOutString(outstm) << std::endl;

    str += str2;
    str.treeOutString(outstm) << std::endl; */

    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

/* let's go */
int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(0);
    return test5(std::cin, std::cout);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
