////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _AVLROPE_CPP_
#define _AVLROPE_CPP_

#include "avlrope.hpp"
#include <utility>

namespace su {
namespace svn {

template<class T> using node = typename AVLRope<T>::node_t;

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::subTreeFindMinimum(node_t * c) {
    if (isNotNull(c) && not c->isLeftNull()) {
        return subTreeFindMinimum(c->left);
    }
    return c;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::subTreeFindMaximum(node_t * c) {
    if (isNotNull(c) && not c->isRightNull()) {
        return subTreeFindMaximum(c->right);
    }
    return c;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::deleteMinimumFrom(node_t * c) {
    if (c->isLeftNull()) {
        if (c == root) root = c->right;
        return c->right;
    }
    c->left = deleteMinimumFrom(c->left);
    return avlRestoreBalance(c);
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::deleteMaximumFrom(node_t * c) {
    if (c->isRightNull()) {
        if (c == root) root = c->left;
        return c->left;
    }
    c->right = deleteMaximumFrom(c->right);
    return avlRestoreBalance(c);
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::avlSmallLeftRotation(node_t * a) {
    node_t * b = a->left;
    a->left = b->right;
    b->right = a;
    fixHeightSize(a);
    fixHeightSize(b);
    return b;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::avlSmallRightRotation(node_t * a) {
    node_t * b = a->right;
    a->right = b->left;
    b->left = a;
    fixHeightSize(a);
    fixHeightSize(b);
    return b;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::avlRestoreBalance(node_t * c) {
    fixHeightSize(c);
    if (2 == getBalance(c)) {
        if (isBigRightBias(c)) {
            c->right = avlSmallLeftRotation(c->right);
        }
        return avlSmallRightRotation(c);
    }
    if (-2 == getBalance(c)) {
        if (isBigLeftBias(c)) {
            c->left = avlSmallRightRotation(c->left);
        }
        return avlSmallLeftRotation(c);
    }
    return c;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::orderStatistics(node_t * c, size_t index) const {
    if (isNull(c)) return nullptr;
    size_t subIdx = leftSize(c) + 1;
    if (index == subIdx) return c;
    if (index <  subIdx)
        return orderStatistics(c->left, index);
    else
        return orderStatistics(c->right, index - subIdx);
    return nullptr;
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::avlMerge(node_t * v2) {
    node<T> * t = subTreeFindMaximum(root);
    root = deleteMaximumFrom(root);
    return avlMergeWithRoot(root, v2, t);
}

template<class T> typename AVLRope<T>::node_t *
AVLRope<T>::avlMergeWithRoot(node_t * v1, node_t * v2, node_t * t) {
    if (isNull(t)) return nullptr;

    if (std::abs(hight(v1) - hight(v2)) <= 1) {
        mergeWithRoot(v1, v2, t);
        fixHeightSize(t);
        return t;
    } else if (hight(v2) < hight(v1)) {
        node_t * t1 = avlMergeWithRoot(v1->right, v2, t);
        if (isNotNull(v1)) v1->right = t1;
        return avlRestoreBalance(v1);
    } else if (hight(v1) < hight(v2)) {
        node_t * t2 = avlMergeWithRoot(v1, v2->left, t);
        if (isNotNull(v2)) v2->left = t2;
        return avlRestoreBalance(v2);
    }
    return nullptr;
}

template<class T> void
AVLRope<T>::fixHeightUpTo(node_t * c, T k) {
    if (isNull(c))
        return;
    if (k < c->key) {
        fixHeightUpTo(c->left, k);
    } else if (c->key < k) {
        fixHeightUpTo(c->right, k);
    }
    fixHeightSize(c);
}

template<class T> void
AVLRope<T>::avlSplit(node_t *& result1, node_t *& result2, node_t * v, T k) {
    if (isNull(v)) return;

    if (k < v->key) {
        node_t * v1 = nullptr, * v2 = nullptr;
        avlSplit(v1, v2, v->left, k);
        result1 = v1;
        result2 = avlMergeWithRoot(v2, v->right, v);
    } else {
        node_t * v1 = nullptr, * v2 = nullptr;
        avlSplit(v1, v2, v->right, k);
        result1 = avlMergeWithRoot(v->left, v1, v);
        result2 = v2;
    }
}

template<class T> void
AVLRope<T>::avlSplitByIdx(
  node_t *& result1, node_t *& result2, node_t * v, ptrdiff_t idx
  ){
    if (isNull(v)) return;

    ptrdiff_t subIdx = leftSize(v) + 1;

    if (idx < subIdx) {
        node_t * v1 = nullptr, * v2 = nullptr;
        avlSplitByIdx(v1, v2, v->left, idx);
        result1 = v1;
        result2 = avlMergeWithRoot(v2, v->right, v);
    } else {
        node_t * v1 = nullptr, * v2 = nullptr;
        avlSplitByIdx(v1, v2, v->right, idx - subIdx);
        result1 = avlMergeWithRoot(v->left, v1, v);
        result2 = v2;
    }
}

template<class T> void
AVLRope<T>::subTreeOut(std::ostream & outstm, node_t * c) const {
    if (isNull(c)) return;
    subTreeOut(outstm, c->left);
    outstm << nodeKeyToString(c);
    subTreeOut(outstm, c->right);
}

template<class T> void
AVLRope<T>::subTreeOutString(std::ostream & outstm, node_t * c) const {
    if (isNull(c)) return;
    subTreeOutString(outstm, c->left);
    outstm << nodeKeyToChar(c);
    subTreeOutString(outstm, c->right);
}

////////////////////////////////////////////////////////////////////////////////
// avlRopeMerge
template<class T> AVLRope<T> &
operator+(AVLRope<T> & left, AVLRope<T> & right) {
    std::cerr << "operator+ in file:" << __FILE__ 
              << " on line:" << __LINE__ 
              << " left:" << &left 
              << " right:" << &right << std::endl;
    left.root = (nullptr == left.root) ? right.root : left.avlMerge(right.root);
    right.root = nullptr;
    return left;
}

template<class T> AVLRope<T> &
operator+=(AVLRope<T> & left, AVLRope<T> & right) {
    std::cerr << "operator+= in file:" << __FILE__ 
              << " on line:" << __LINE__ 
              << " left:" << &left 
              << " right:" << &right << std::endl;
    return operator+(left, right);
}

template<class T>
void avlRopeMerge(AVLRope<T> & a, AVLRope<T> & b) {
    a.root = isNull(a.root) ? b.root : a.avlMerge(b.root);
    b.root = nullptr;
}

template<class T>
void avlRopeMerge(AVLRope<T> & a, AVLRope<T> & b, AVLRope<T> & c) {
    if (isNotNull(b.root)) {
        a.root = b.avlMerge(c.root);
        b.root = nullptr;
        c.root = nullptr;
    } else {
        a.root = c.root;
        c.root = nullptr;
    }
}

template<class T>
void avlRopeSplit(AVLRope<T> & b, AVLRope<T> & c, AVLRope<T> & a, T k) {
    a.avlRopeSplit(b.root, c.root, a.root, k);
    a.root = nullptr;
}

template<class T>
void avlSplitByIndex(
  AVLRope<T> & b, AVLRope<T> & c, AVLRope<T> & a, size_t index
  ){
    a.avlSplitByIndex(b.root, c.root, a.root, index);
    a.root = nullptr;
}

void getAVLRopeChars(std::istream & instm, AVLRope<char_t> & str) {
    int c = instm.get();
    str.root = new node_char_t(static_cast<char_t>(c));
    while (EOF != c) {
        AVLRope<char_t> x;
        c = instm.get();
        if ('\n' == c) break;
        x.root = new node_char_t(static_cast<char_t>(c));
        avlRopeMerge(str, x);
        x.root = nullptr;
    }
}

template<class T>
void avlRopeDelete(AVLRope<T> & str) {
    delete str.root;
    str.root = nullptr;
}

void getAVLRopeFromString(AVLRope<char_t> & str, const std::string line) {
    auto ch = line.begin();
    str.root = new node_char_t(*ch);
    for (ch++; ch != line.end(); ++ch) {
        AVLRope<char> c;
        c.root = new node_char_t(*ch);
        avlRopeMerge(str, c);
        c.root = nullptr;
    }
}

void rope(AVLRope<char_t> & str, size_t i, size_t j, size_t k) {
    AVLRope<char_t> str1, strM, strS, str2;

    if (i > 0)
        // avl Split By Index <char_t> (str1, strM, str, i - 1);
        str.split(str1, strM, i - 1);
    else {
        strM = std::move(str);
    }

    // avl Split By Index <char_t> (strS, str2, strM, j - i);
    strM.split(strS, str2, j - i);

    // avl Rope Merge <char_t> (strM, str1, str2);
    strM = std::move(str1 + str2);

    if (0 == k) {
        str = std::move(strS + strM);
        // avlRopeMerge(str, strS, strM);
    } else {
        strM.split(str1, str2, k - 1);
        // avlSplitByIndex<char_t>(str1, str2, strM, k - 1);
        strM = std::move(str1 + strS);
        // avlRopeMerge<char_t>(strM, str1, strS);
        str = std::move(strM + str2);
        // avlRopeMerge<char_t>(str, strM, str2);
    }
}

} // namespace svn
} // namespace su

#endif

////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
