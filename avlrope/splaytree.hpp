////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////
// Compilation:
// Execution:
// Dependencies: node.hpp basictree.hpp bstree.hpp

#ifndef _SPLAYTREE_HPP_
#define _SPLAYTREE_HPP_

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "node.hpp"
#include "basictree.hpp"
#include "ibstree.hpp"

#define DEBUG 1
#ifdef DEBUG
# include "mydebug.hpp"
#endif

namespace svn {

////////////////////////////////////////////////////////////////////////////////
//
//  @author Victor Skurikhin
//
template<typename T, class NODE = StoreNode<T> >
class SplayTree : public IBSTree<T, NODE> {
    friend class BSTreeIterator<T, NODE>;
    template <typename Y, class N>
    friend bool operator==(const BSTree<Y,N> &, const BSTree<Y,N> &);
    template <typename Y, class N>
    friend bool operator!=(const BSTree<Y,N> &, const BSTree<Y,N> &);
public:
    typedef NODE node_t;
    typedef BasicT<NODE> basict_t;
    typedef IBSTree<T, NODE> ibstree_t;
    typedef BSTreeIterator<T, NODE> iterator;
    typedef BSTreeIterator<T, const NODE> const_iterator;
    using basict_t::root;
    using basict_t::detailOut;

    ////////////////////////////////////////////////////////////////////////////
    
    SplayTree() : dummy(nullptr) { /* None */ }
    explicit SplayTree(const T & notFound) : dummy(new node_t(notFound))
    { /* None */ }
    virtual ~SplayTree() { 
        if (isNotNull(dummy)) {
            dummy->unLink();
            delete dummy; 
        }
    }

    // Copy constructor and operator-function =.
    SplayTree(const SplayTree & other) : ibstree_t(other) { /* None */ }
    SplayTree & operator=(const SplayTree & rhs) {
        if (&rhs == this) return *this;
        auto newRoot = rhs.ibstree_t::clone();
        auto newDummy = new node_t(rhs.dummy->key());
        ibstree_t::postOrderIterativeFree(); // delete root;
        delete dummy;
        root = newRoot;
        dummy = newDummy;
        return *this;
    }

    SplayTree(SplayTree && other) {
        basict_t::move(std::move(other));
        dummy = std::move(other.dummy); 
    }
    SplayTree & operator=(SplayTree && o) { return ibstree_t::operator=(o); }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Adds the key to the set if it is not already present.
    // @param key the key to add
    // @return false if the set contains key and true otherwise
    // @throws std::bad_alloc if can't allocated a memory
    virtual bool insert(const T & key) throw(std::bad_alloc) {
        bool found = false;
        root = add(root, found, key);
        return not found;
    }

    // Does the set contain the given key?
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool find(const T & key) const {
        return isNull(ibstree_t::searchNode(root, key)) ? false : true;
    }

    virtual bool findSplay(const T & key) {
        DEBUG_FUNC1(key);
        root = splay(root, key);
        return root->key() == key;
        // return isNull(search(root, key)) ? false : true;
    }

    // Returns the smallest key in the set.
    // @return the smallest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMin() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return ibstree_t::searchMinimum(root)->key();
    }

    virtual const T & findMinSplay() throw(std::range_error) {
        DEBUG_FUNC1("");
        basict_t::checkRoot(__LINE__, __FILE__);
        node_t * ptr = root;
        while (ptr->left() != nullptr)
            ptr = ptr->left();
        root = splay(root, ptr->key());
        return ptr->key();
    }

    // Returns the largest key in the set.
    // @return the largest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMax() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return ibstree_t::searchMaximum(root)->key();
    }

    virtual const T & findMaxSplay() throw(std::range_error) {
        DEBUG_FUNC1("");
        basict_t::checkRoot(__LINE__, __FILE__);
        node_t * ptr = root;
        while( ptr->right() != nullptr)
            ptr = ptr->right();
        root = splay(root, ptr->key());
        return ptr->key();
    }

    // Returns  the smallest key in  the set greater than  to key  or if this
    // set is empty returns the key.
    // @param key the key
    // @return the next key in the set greater than to key
    virtual const T & next(const T & key) const {
        node_t * successor = ibstree_t::nextNode(key);
        return isNull(successor) ? key : successor->key();
    }

    // Returns the largest key in the set less than or equal to key.
    // @param key the key
    // @return the largest key in the set table less than or equal to key
    virtual const T & previous(const T & key) const {
        node_t * predecessor = ibstree_t::prevNode(key);
        return isNull(predecessor) ? key : predecessor->key();
    }

    size_t size()          const { return root->size(); }
    virtual bool isEmpty() const { return isNull(root); } // Is the set empty?

    // Removes the key from the set if the key is present.
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool remove(const T & key) {
        DEBUG_FUNC1(key);
        node_t * newTree;
        // If x is found, it will be at the root
        root = splay(root, key);
        if (root->key() != key)
            return false; // Item not found; do nothing
        if (root->isLeftNull())
            newTree = root->right();
        else {
            // Find the maximum in the left subtree
            // Splay it to the root; and then attach right child
            newTree = root->left();
            newTree = splay(newTree, key);
            newTree->linkRight(root->right());
        }
        root->unLink();
        delete root;
        root = newTree;
        return true;
    }

    // Returns a stream representation of this set.
    // @return a stream representation of this set, with the keys separated
    std::ostream & treeOutInOrder(std::ostream & os) const {
        size_t index = 0;
        const char * sep = detailOut ? nl : sp;
        this->subTreeOutInOrder(os, root, index, sep);
        return os;
    }
protected:
    ////////////////////////////////////////////////////////////////////////////

    // A utility function to right rotate subtree rooted with y
    // See the diagram given above.
    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty return the NULL
    //         if left child of x is NULL return x
    node_t * rightRotate(node_t * x) {
        if (isNull(x)) return nullptr;
        node_t * y = x->left();      // Node y  = x.left
        if (isNotNull(y)) {        
            x->linkLeft(y->right()); // x.left  = y.right
            y->linkRight(x);         // y.right = x
        } else
            return x;
        // updateNode(x); TODO for splayR
        // updateNode(y); TODO for splayR
        return y;
    }

    // A utility function to left rotate subtree rooted with x
    // See the diagram given above.
    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty returns the NULL
    //         if right child of x is NULL returns x
    node_t * leftRotate(node_t * x) {
        if (isNull(x)) return nullptr;
        node_t * y = x->right();     // Node y  = x.right
        if (isNotNull(y)) {
            x->linkRight(y->left()); // x.right = y.left # Inner child of y
            y->linkLeft(x);          // y.left  = x
        } else
            return x;
        // updateNode(x); TODO for splayR
        // updateNode(y); TODO for splayR
        return y;
    }

    /**
     * Top-down Splay
     * # Top-down splay is an operation, like BSTFinder, on a binary  search
     *   tree. It differs from BSTFinder in that it restructures the tree as
     *   it descends toward the desired key's place in the tree.
     *   During descent, ...
     *   * long "straight" paths are shortened by rotation.  For example, if
     *     the desired key is greater than both the current node's  key  and
     *     its right child's key, a left rotation is performed.
     *   * all nodes are removed from the original tree  and  reinserted  in
     *     one of two new trees built from scratch (i.e., initially  empty).
     *     One of these new  trees  contains  all  elements  less  than  the
     *     desired key and the other contains all elements greater than  the
     *     desired key.  Let's call them "new left" and "new right".
     *     # This sounds hard and costly, but isn't.  We'll see why later.
     * # Ultimately, if we find the desired key, we  reassemble  the  binary
     *   search tree making the desired key's node the new  root  with  "new
     *   left" as its left subtree and "new right" as its right subtree.
     *   * If the desired key is not found, the next smaller or next  larger
     *     key is made the new root.
     * # The algorithm is simple.  The performance proof is hard.
     *   * Tarjan and Sleator proved that a sequence of n  splay  operations
     *     on a tree of n nodes would take O(n log n) time.
     *     # Any individual splay operation (taken in  isolation)  may  take
     *       O(n) time; but the extra work done  for  that  splay  operation
     *       will make future splay operations cheaper.
     * # The traditional dictionary operations, i.e.,  lookup,  insert,  and
     *   remove, can be reimplemented using top-down splay as a helper.
     *   * There is little work for lookup, insert, and remove  to  do;  the
     *     hard work is done by splay.
     * https://www.clear.rice.edu/comp212/07-spring/lectures/34/
     * http://www.cs.cmu.edu/~sleator/papers/self-adjusting.pdf
     * TODO!!!BROKEN FOR SIZE AND HEIGHT!!!
     */
    node_t * splayTD(node_t * peak, const T & key) {
        if (isNull(peak)) return nullptr;
        DEBUG_FUNC3(key, peak, "peak: ");
        // nodeOutDetail(std::cerr, peak) << std::endl;
        node_t & header = *dummy;
        node_t * leftTreeMax, * rightTreeMin, * child;

        header.unLink();
        leftTreeMax = rightTreeMin = &header;
        DEBUG_FUNC4(key, peak, "header: ", &header);
        // nodeOutDetail(std::cerr, &header) << std::endl;

        for (; true; /* peak = child */ ) {
            DEBUG_FUNC3(key, peak, "peak: ");
            // nodeOutDetail(std::cerr, peak) << std::endl;
            if (key < peak->key()) {
                if (isNull(child = peak->left()))
                    break;
                DEBUG_FUNC4(key, peak, "left child: ", child);
                // nodeOutDetail(std::cerr, child) << std::endl;
                if (key < child->key()) {
                    peak = rightRotate(peak); // Rotate right.
                    if (isNull(child = peak->left()))
                        break; 
                }
                /* Link into the new peak's right tree. */
                rightTreeMin->linkLeft(peak);
                rightTreeMin = peak;
                peak = peak->left();
            } else if (key > peak->key()) {
                if (isNull(child = peak->right()))
                    break;
                DEBUG_FUNC4(key, peak, "right child: ", child);
                // nodeOutDetail(std::cerr, child) << std::endl;
                if (key > child->key()) {
                    peak = leftRotate(peak);  // Rotate left.
                    if (isNull(child = peak->right()))
                        break;
                }
                /* Link into the new peak's left tree. */
                leftTreeMax->linkRight(peak);
                leftTreeMax = peak;
                peak = peak->right();
            } else
                break;
        }
        /* Assemble the new peak. */
        leftTreeMax->linkRight(peak->left());
        rightTreeMin->linkLeft(peak->right());
        peak->linkLeft(header.right());
        peak->linkRight(header.left());
        return peak;
    }

    // Getting around 80 000 nodes in SplayTree!!!TODO!!!
    // Splaying Recursively:
    //  To “splay node peak”, traverse up the tree from
    //  node peak to root, rotating along the way until peak 
    //  is the root.
    //
    //  peak === chosen node
    //
    //  For each rotation:
    //    1. If peak is the root, do nothing.
    //    2. "Zig": If peak has no grandparent, rotate x about its parent.
    //    3. If the peak has a grandparent:
    //      a. "Zig-Zig": if peak  and its parent  are both left children  or 
    //         both right children, rotate the parent about  the grandparent,
    //         then rotate peak about its parent.
    //      b. "Zig-Zag": if  the  peak  and  its  parent  are  opposite type
    //         children (one left and the other right), rotate peak about its
    //         parent, then rotate
    // This function brings the key at peak if key is present in tree.
    // If key is not present, then it brings the last accessed item at
    // peak.  This function modifies the tree and returns the new peak
    //
    // Reference:
    //  http://cs.brynmawr.edu/Courses/cs206/fall2012/slides/09_SplayTrees.pdf
    //  http://algs4.cs.princeton.edu/33balanced/
    //  http://algs4.cs.princeton.edu/33balanced/SplayBST.java.html
    node_t * splayR(node_t * peak, const T & key) {
        DEBUG_FUNC2(peak, key);
        // Base cases: root is NULL or key is present at root
        if (isNull(peak) or peak->key() == key) return peak;

        // Key lies in left subtree
        if (key < peak->key()) {
            // Key is not in tree, we are done
            if (peak->isLeftNull()) return peak;

            // Zig-Zig (Left Left)
            if (key < peak->left()->key()) {
                // First recursively bring the key as peak of left-left
                peak->left()->linkLeft(splayR(peak->left()->left(), key));

                DEBUG_FUNC3(peak, key, "[Zig-Zig]");
                // Do first rotation for peak, second rotation is done after
                peak = rightRotate(peak);                           // else
            } else if (peak->left()->key() < key) { // Zig-Zag (Left Right)
                // First recursively bring the key as peak of left-right
                peak->left()->linkRight(splayR(peak->left()->right(), key));

                DEBUG_FUNC3(peak, key, "[Zig-Zag]");
                // Do first rotation for peak->left
                if (peak->left()->right() != nullptr)
                    peak->linkLeft(leftRotate(peak->left()));
            }

            DEBUG_FUNC3(peak, key, "[Zag Right]");
            // Do second rotation for peak
            return peak->isLeftNull() ? peak : rightRotate(peak);
        } else { // Key lies in right subtree
            // Key is not in tree, we are done
            if (peak->isRightNull()) return peak;

            // Zag-Zig (Right Left)
            if (key < peak->right()->key()) {
                // Bring the key as peak of right-left
                peak->right()->linkLeft(splayR(peak->right()->left(), key));

                DEBUG_FUNC3(peak, key, "[Zag-Zig]");
                // Do first rotation for peak->right
                if (peak->right()->left() != NULL)
                    peak->linkRight(rightRotate(peak->right()));
            } else if (peak->right()->key() < key) { // Zag-Zag (Right Right)
                DEBUG_FUNC3(peak, key, "[Zag-Zag]");
                // Bring the key as peak of right-right and do first rotation
                peak->right()->linkRight(splayR(peak->right()->right(), key));
                peak = leftRotate(peak);
            }

            DEBUG_FUNC3(peak, key, "[Zig Left]");
            // Do second rotation for peak
            return peak->isRightNull() ? peak : leftRotate(peak);
        }
    }
    node_t * splay(node_t * peak, const T & key) {
        return splayTD(peak, key);
    }

    // Adds the key to the set if it is not already present.
    // @param  c (current) is a pointer to the root of current subtree
    // @param  found the boolean attribute for return
    // @param  k the key
    // @return pointer to the root of current subtree
    // @return found feature is set to true if key already present
    //         otherwise found is set to false
    node_t * add(node_t * peak, bool & found, const T & key)
      throw(std::bad_alloc) {
        DEBUG_FUNC2(key, peak);
        if (isNull(dummy))
            dummy = new node_t(key);
        // node_t * newNode = new node_t(key);              // v1
        if (isNull(peak))
            return new node_t(key); // v2 // return newNode // v1
        peak = splay(peak, key);
        DEBUG_FUNC3(key, peak, " splay done");
        if (key < peak->key()) {
            node_t * newNode = new node_t(key); // v2
            newNode->linkLeft(peak->left());
            newNode->linkRight(peak);
            peak->unLinkLeft();
            return newNode;
        } else if (peak->key() < key) {
            node_t * newNode = new node_t(key); // v2
            newNode->linkRight(peak->right());
            newNode->linkLeft(peak);
            peak->unLinkRight();
            return newNode;
        } else {
            found = true;
            // delete newNode; // v1
            return peak;
        }
    }

    node_t * search(node_t * current, const T & key) {
        return (root = splay(current, key));
    }
private:
    node_t * dummy;
};

} // namespace svn

#endif // _SPLAYTREE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
