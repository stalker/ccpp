////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////
// Compilation:
// Execution:
// Dependencies: node.hpp basictree.hpp

#ifndef _IFACEBST_HPP_
#define _IFACEBST_HPP_

#include <iomanip>
#include <iostream>
#include <sstream>

#ifdef DEBUG
# include "mydebug.hpp"
#endif

#include "node.hpp"
#include "basictree.hpp"

namespace svn {

////////////////////////////////////////////////////////////////////////////////
// Interface (contract) for Binary Search Tree.
template<class T>
class IfaceBSTree {
public:
    virtual ~IfaceBSTree() { /* None */ }
    virtual bool insert(const T &) = 0;
    virtual bool find(const T &) const = 0;
    virtual const T & findMin() const = 0;
    virtual const T & findMax() const = 0;
    virtual const T & next(const T &) const = 0;
    virtual const T & previous(const T &) const = 0;
    virtual bool remove(const T &) = 0;
    virtual bool isEmpty() const = 0;
    virtual bool isBinarySearchTree() const = 0;

    virtual std::ostream & treeOutInOrder(std::ostream & os) const = 0;

    ////////////////////////////////////////////////////////////////////////////

    template<typename Y> friend std::ostream &
        operator<<(std::ostream &, const IfaceBSTree<Y> &);
    template<typename Y> friend std::istream &
        operator>>(std::istream &, const IfaceBSTree<Y> &);
};

template<typename T> std::ostream &
operator<<(std::ostream& os, const IfaceBSTree<T> & bt) {
    bt.treeOutInOrder(os);
    return os;
}

template<typename T> std::istream &
operator>>(std::istream& is, const IfaceBSTree<T> & bt) {
    std::string in;
    std::getline(is, in);
    std::istringstream iss(in);
    for (T e; iss >> e;)
        bt.insert(e);
    return is;
}

template<class T, class NODE> class BSTree;

////////////////////////////////////////////////////////////////////////////////
//
// Returns all of the keys in the set, as an iterator.
// To iterate  over  all of  the keys in  a set  named  set, use  the foreach
// notation: for (auto key : set).
// @return an iterator to all of the keys in the set
//
template<typename T, class NODE>
class BSTreeIterator: public std::iterator<std::input_iterator_tag, NODE> {
    friend class BSTree<T, NODE>;
    template<typename Y, class N> friend
    bool isNull(BSTreeIterator<Y, N> const & i);
    template<typename Y, class N> friend
    bool isNotNull(BSTreeIterator<Y, N> const & i);

    BSTreeIterator(BSTree<T, NODE> & bt, NODE * np)
    : t(bt), p(np) { /* None */ }
public:
    BSTreeIterator(const BSTreeIterator &it)
    : t(it.t), p(it.p) { /* None */ }

    bool operator!=(BSTreeIterator const & other) const;
    bool operator==(BSTreeIterator const & other) const;
    const NODE & operator*()        const { return *p; }
    const NODE * const operator->() const { return p; }
    BSTreeIterator & operator++();
    BSTreeIterator operator++(int);
    BSTreeIterator & operator--();
    BSTreeIterator operator--(int);
private:
    BSTree<T, NODE> & t;
    NODE * p;
};

template<typename T, class NODE>
bool BSTreeIterator<T, NODE>::operator!=(BSTreeIterator const & other) const {
    return p != other.p;
}

template<typename T, class NODE>
bool BSTreeIterator<T, NODE>::operator==(BSTreeIterator const & other) const {
    return p == other.p;
}

template<typename T, class NODE>
BSTreeIterator<T, NODE> & BSTreeIterator<T, NODE>::operator++() {
    p = t.nextNode(p);
    return *this;
}

template<typename T, class NODE>
BSTreeIterator<T, NODE> BSTreeIterator<T, NODE>::operator++(int) {
    BSTreeIterator result(*this);
    ++(*this);
    return result;
}

template<typename T, class NODE>
BSTreeIterator<T, NODE> & BSTreeIterator<T, NODE>::operator--() {
    p = t.prevNode(p);
    return *this;
}

template<typename T, class NODE>
BSTreeIterator<T, NODE> BSTreeIterator<T, NODE>::operator--(int) {
    BSTreeIterator result(*this);
    --(*this);
    return result;
}

template<typename T, class N>
bool isNull(BSTreeIterator<T, N> const & i)    { return isNull(i.p); }

template<typename T, class N>
bool isNotNull(BSTreeIterator<T, N> const & i) { return isNotNull(i.p); }

} // namespace svn


template<typename T,class N> bool
operator==(const svn::BSTree<T,N> & left, const svn::BSTree<T,N> & right) {
    return left.equals(right);
}

template<typename T, class N> bool
operator!=(const svn::BSTree<T,N> & left, const svn::BSTree<T,N> & right) {
    return !(left == right);
}

#endif // _IFACEBST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
