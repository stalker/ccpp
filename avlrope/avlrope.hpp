////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _AVLROPE_HPP_
#define _AVLROPE_HPP_

#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#ifndef  SETWZ
# define SETWZ(x) std::setw(x) << std::setfill('0')
#endif
#ifndef  SETWS
# define SETWS(x) std::setw(x) << std::setfill(' ')
#endif
#ifndef  UINT32
# define UINT32 uint_fast32_t
#endif
#ifndef  CAST_UINT32
# define CAST_UINT32(x) static_cast<UINT32>(x)
#endif

namespace su {
namespace svn {

typedef char char_t;
typedef int_fast8_t int8_t;

template<class T = char_t>
struct Node {
    Node<T> * left, * right;
    size_t size;
    T key;
    int8_t hight;

    Node(T k)
    : left(nullptr), right(nullptr), size(1), key(k), hight(0)
    { /* None */ }

    ~Node() { delete left; delete right; }

    bool isLeftNull()  const { return nullptr == left; }
    bool isRightNull() const { return nullptr == right; }
    void link(Node<T> * l, Node<T> * r) { left = l; right = r; }
    void unLink()      { left = nullptr; right = nullptr; }
};

typedef Node<char_t> node_char_t;

template<class T>
bool isNull        (Node<T> * c) { return nullptr == c; }
template<class T>
bool isNotNull     (Node<T> * c) { return nullptr != c; }
template<class T>
int8_t getBalance  (Node<T> * c) { return rightHeight(c) - leftHeight(c);}
template<class T>
int8_t hight       (Node<T> * c) { return nullptr != c ? c->hight : -1; }
template<class T>
int8_t leftHeight  (Node<T> * c) { return hight(c->left); }
template<class T>
int8_t rightHeight (Node<T> * c) { return hight(c->right); }
template<class T>
size_t getSize     (Node<T> * c) { return nullptr != c ? c->size : 0; }
template<class T>
size_t leftSize    (Node<T> * c) { return getSize(c->left); }
template<class T>
size_t rightSize   (Node<T> * c) { return getSize(c->right); }

template<class T = char_t>
class AVLRope {
public:
    typedef Node<T> node_t;

    AVLRope() : root(nullptr) { /* None */ }

    virtual ~AVLRope() { delete root; }

    AVLRope(const AVLRope & c) = delete;
    AVLRope & operator=(const AVLRope & r) = delete;

    AVLRope(AVLRope && other) {
        root = other.root;
        if (this != &other)
        other.root = nullptr;
    }

    AVLRope& operator=(AVLRope && other) {
        std::swap(root, other.root);
        return *this;
    }

    T operator[](size_t i) const throw(std::range_error) {
        checkRootAndIndex(i, __LINE__, __FILE__);
        return orderStatistics(root, i + 1)->key;
    }

    T & operator[](size_t i) throw(std::range_error) {
        checkRootAndIndex(i, __LINE__, __FILE__);
        return orderStatistics(root, i + 1)->key;
    }

    ///////////////////////////////////////////////////////////////////////////
    template<class C> friend AVLRope<C> &
        operator+(AVLRope<C> & left, AVLRope<C> & right);
    template<class C> friend AVLRope<C> &
        operator+=(AVLRope<C> & left, AVLRope<C> & right);
    // template<class C> friend bool operator==(AVLRope<C> & l, AVLRope<C> & r);
    template<class C> friend void
        avlRopeMerge(AVLRope<C> & a, AVLRope<C> & b);
    template<class C> friend void
        avlRopeMerge(AVLRope<C> & a, AVLRope<C> & b, AVLRope<C> & c);
    template<class C> friend void 
        avlRopeSplit(AVLRope<C> & b, AVLRope<C> & c, AVLRope<C> & a, C k);
    template<class C> friend void 
        avlSplitByIndex(AVLRope<C> & b, AVLRope<C> & c, AVLRope<C> & a,
                        size_t index);
    template<class C> friend void avlRopeDelete(AVLRope<C> & str);
    friend void 
        getAVLRopeFromString(AVLRope<char_t> & str, const std::string line);
    friend void getAVLRopeChars(std::istream & instm, AVLRope<char_t> & str);
    friend void rope(AVLRope<char_t> & str, size_t i, size_t j, size_t k);

    friend AVLRope<char_t> & test1(AVLRope<char_t> & result);
    friend AVLRope<char_t> & test2(AVLRope<char_t> & result);
    friend AVLRope<char_t> & test3(AVLRope<char_t> & result);
    friend AVLRope<char_t> & test4(AVLRope<char_t> & result);
    ///////////////////////////////////////////////////////////////////////////

    size_t size()   const { return isNotNull(root) ? root->size  : 0; }
    int8_t height() const { return isNotNull(root) ? root->hight : 0; }

    void append(const T & a) {
        if (isNull(root))
            root = new node_t(a);
        else {
            AVLRope<T> t;
            t.append(a);
            *this += t;
            t.root = nullptr;
        }
    }

    void split(AVLRope<T> & b, AVLRope<T> & c, size_t i) {
        avlSplitByIdx(b.root, c.root, root, i + 1);
        root = nullptr;
    }

    bool equally(AVLRope<T> & o) const {
        if (size() != o.size()) return false;
        size_t index = 0;
        return equal(root, o.root, index);
    }

    bool coincides(AVLRope<T> & o) const {
        if (size() != o.size()) return false;
        return coincides(root, o.root);
    }

    T findMinimum() const   {
        checkRoot(__LINE__, __FILE__);
        return subTreeFindMinimum(root)->key;
    }

    T findMaximum() const {
        checkRoot(__LINE__, __FILE__);
        return subTreeFindMaximum(root)->key;
    }

    const T & findMinimum() {
        checkRoot(__LINE__, __FILE__);
        return subTreeFindMinimum(root)->key;
    }
    const T & findMaximum() {
        checkRoot(__LINE__, __FILE__);
        return subTreeFindMaximum(root)->key;
    }

    std::ostream & treeOut(std::ostream & outstm) {
        subTreeOut(outstm, root);
        return outstm;
    }

    std::ostream & treeOutString(std::ostream & outstm) {
        subTreeOutString(outstm, root);
        return outstm;
    }

    ////////////////////////////////////////////////////////////////////////////
    std::ostream & treePrintByIndex(std::ostream & outstm, const char * sep = "\n") {
        size_t index = 0;
        subTreePrintByIndex(outstm, root, index, sep);
        return outstm;
    }

    void subTreePrintByIndex(
      std::ostream & outstm, node_t * c, size_t & i, const char * sep
      ) {
        if (nullptr == c)
            return;
        subTreePrintByIndex(outstm, c->left, i, sep);
        outstm << SETWS(8) << c
               <<            ":" << SETWS(2)  << c->key
               <<      " (left:" << SETWZ(14) << c->left
               <<            "|" << SETWS(2)  << nodeKeyToChar(c->left)
               <<    ") (right:" << SETWZ(14) << c->right
               <<            "|" << SETWS(4)  << nodeKeyToChar(c->right)
               <<    ") (index:" << SETWS(10) << i++
               <<    ") (hight:" << SETWS(3)  << CAST_UINT32(c->hight)
               <<      ") size:" << c->size   << sep;
           subTreePrintByIndex(outstm, c->right, i, sep);
    }
private:
    int8_t leftLeftHeight(node_t * c) const {
        return nullptr != c->left->left ? c->left->left->hight : -1;
    }

    int8_t leftRightHeight(node_t * c) const {
        return nullptr != c->left->right ? c->left->right->hight : -1;
    }

    int8_t rightLeftHeight(node_t * c) const {
        return nullptr != c->right->left ? c->right->left->hight : -1;
    }

    int8_t rightRightHeight(node_t * c) const {
        return nullptr != c->right->right ? c->right->right->hight : -1;
    }

    bool isBigLeftBias(node_t * c) const {
        return 0 < getBalance(c->left);
    }

    bool isBigRightBias(node_t * c) const {
        return getBalance(c->right) < 0;
    }

    node_t * deleteMinimum() {
        return deleteMinimumFrom(root);;
    }

    node_t * deleteMaximum() {
        return deleteMaximumFrom(root);;
    }

    void fixHeight(node_t * c) {
        int8_t lh = leftHeight(c);
        int8_t rh = rightHeight(c);
        c->hight = lh < rh ? rh + 1 : lh + 1;
    }

    void fixSize(node_t * c) {
        size_t ls = leftSize(c);
        size_t rs = rightSize(c);
        c->size = rs + ls + 1;
    }

    void updateHeightUp(node_t * c) {
        if (isNull(c)) return;
        fixHeightUpTo(root, c->key);
    }

    void fixHeightSize(node_t * c) {
        fixHeight(c); fixSize(c);
    }

    node_t * mergeWithRoot(node_t * v1, node_t * v2, node_t * t) {
        if (isNull(t)) return nullptr;
        t->left  = v1;
        t->right = v2;
        return t;
    }

    std::string nodeKeyToString(node_t * c) const {
        return isNotNull(c) ? std::to_string(c->key) : "";
    }

    char_t nodeKeyToChar(node_char_t * c) const {
        return isNotNull(c) ? static_cast<char_t>(c->key) : ' ';
    }

    void avlSplitByIndex(node_t *& res1, node_t *& res2, node_t * v, size_t i) {
        return avlSplitByIdx(res1, res2, v, i + 1);
    }

    bool equal(node_t * c, node_t * o, size_t & i) const {
        if (isNull(c)) return true;
        bool letfEqual = equal(c->left, o, i);
        node_t * p = orderStatistics(o, i + 1);
        ++i;
        return letfEqual and (c->key == p->key) and equal(c->right, o, i);
    }

    bool coincides(node_t * l, node_t * r) const {
        if (isNull(l))
            return isNull(r) ? true : false;
        if (l->size != r->size)  return false;
        return coincides(l->left, r->left)
           and l->key == r->key
           and coincides(l->right, r->right);
    }

    ////////////////////////////////////////////////////////////////////////////


    node_t * deleteMinimumFrom     (node_t * c);
    node_t * deleteMaximumFrom     (node_t * c);
    node_t * subTreeFindMinimum    (node_t * c);
    node_t * subTreeFindMaximum    (node_t * c);
    node_t * avlSmallLeftRotation  (node_t * c);
    node_t * avlSmallRightRotation (node_t * c);
    node_t * avlRestoreBalance     (node_t * c);
    void fixHeightUpTo             (node_t * c, T k);
    node_t * orderStatistics       (node_t * c, size_t index) const;
    node_t * avlMerge              (node_t * v2);
    node_t * avlMergeWithRoot      (node_t * v1, node_t * v2, node_t * t);
    void avlSplit     (node_t *& result1, node_t *& result2, node_t * v, T k);
    void avlSplitByIdx(node_t *& res1, node_t *& res2, node_t * v, ptrdiff_t i);
    void subTreeOut      (std::ostream & out, node_t * c) const;
    void subTreeOutString(std::ostream & out, node_t *) const;

    void checkRootAndIndex(size_t i, int l, const char * f)
      throw(std::range_error) {
        if (nullptr == root || root->size <= i || (SIZE_MAX - 2) < i)
            throw std::range_error("AVLRope Operation Error on empty set at "
                  "line: " + std::to_string(l) + std::string(" in file: ") + f);
    }

    void checkRoot(int l, const char * f) throw(std::range_error) {
        if (nullptr == root)
            throw std::range_error("AVLRope Range Error Index at line: " +
                  std::to_string(l) + std::string(" in file: ") + f);
    }

    node_t * root;
};

template<class T> AVLRope<T> & operator+(AVLRope<T> & left, AVLRope<T> & right);
void rope(AVLRope<char_t> & str, size_t i, size_t j, size_t k);

AVLRope<char_t> & test1(AVLRope<char_t> & result) {
    delete result.root;
    result.root = new Node<char_t>('a');
    result.root->right = new Node<char_t>('b');
    result.root->size++;
    result.root->hight++;
    return result;
}

AVLRope<char_t> & test2(AVLRope<char_t> & result) {
    delete result.root;
    result.root = new Node<char_t>('b');
    result.root->left = new Node<char_t>('a');
    result.root->size++;
    result.root->hight++;
    return result;
}

AVLRope<char_t> & test3(AVLRope<char_t> & result) {
    delete result.root;
    result.root = new Node<char_t>('h');
    result.root->right = new Node<char_t>('e');
    result.root->size++;
    result.root->hight++;
    return result;
}

AVLRope<char_t> & test4(AVLRope<char_t> & result) {
    delete result.root;
    result.root = new Node<char_t>('e');
    result.root->left = new Node<char_t>('h');
    result.root->size++;
    result.root->hight++;
    return result;
}

} // namespace svn
} // namespace su

#include "avlrope.cpp"

#endif
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
