////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODE_HPP_
#define _NODE_HPP_

#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <iomanip>
#include <queue>
#include <sstream>
#include <stack>

namespace svn {

typedef char char_t;
typedef int_fast8_t int8_t;
typedef int_fast16_t int16_t;

int16_t int16_cast(int8_t c) { return static_cast<int16_t>(c); }

const char * nl = "\n";
const char * sp = " ";

std::ostream & setws(std::ostream & outstm, size_t x) {
    return (outstm << std::setw(x) << std::setfill(' '));
}

std::ostream & setwz(std::ostream & outstm, size_t x) {
    return (outstm << std::setw(x) << std::setfill('0'));
}

/**
 * Node classes.
 *
 * Operations for all nodes:
 *
 * bool isNull(c)           -> check for existence
 * bool isNotNull(c)        -> check for lack of
 * bool Node::isLeftNull    -> check for existence left node
 * bool Node::isRightNull   -> check for existence right node
 * void Node::linkLeft(l)   -> link tree with root of node c and left sub tree
 *                             of l
 * void Node::linkRight(l)  -> link tree with root of node c and right sub tree
 *                             of r
 * void Node::link(l, r)    -> link tree with root of node c and left sub tree
 *                             of l and right sub tree of r
 * void Node::unLink()      -> unlink node
 *
 * Operations for Regular nodes:
 *
 * bool getSize(c)          -> get size of tree with root of node c
 * bool leftSize(c)         -> get size of left sub tree
 * bool rightSize(c)        -> get size of right sub tree
 *
 * Operations for Value nodes with height:
 *
 * int8_t hight(c)
 * int8_t leftHight(c)
 * int8_t rightHight(c)
 */

////////////////////////////////////////////////////////////////////////////////
struct BasicNode {
    BasicNode() : left_(nullptr), right_(nullptr) { /* None */ }
    ~BasicNode() { delete left_; delete right_; }

    bool isLeftNull()  const { return nullptr == left_; }
    bool isRightNull() const { return nullptr == right_; }
    void link(BasicNode * l, BasicNode * r) { left_ = l; right_ = r; }
    void linkLeft (BasicNode * l) { left_ = l; }
    void linkRight(BasicNode * r) { right_ = r; }
    void unLink() { left_ = nullptr; right_ = nullptr; }
    void unLinkLeft() { left_ = nullptr; }
    void unLinkRight() { right_ = nullptr; }
    BasicNode * left()  const { return left_; }
    BasicNode * right() const { return right_; }
private:
    BasicNode(const BasicNode &) = delete;
    BasicNode & operator=(const BasicNode &) = delete;

    BasicNode * left_, * right_;
};

////////////////////////////////////////////////////////////////////////////////
template<class T> bool isNull      (const T * c) { return nullptr == c; }
template<class T> bool isNotNull   (const T * c) { return nullptr != c; }
template<class N>
size_t getSize(const N * c) { return nullptr != c ? c->size() : 0; }
template<class N> size_t leftSize  (const N * c) { return getSize(c->left()); }
template<class N> size_t rightSize (const N * c) { return getSize(c->right()); }

////////////////////////////////////////////////////////////////////////////////
struct RegularNode {
    RegularNode() : left_(nullptr), right_(nullptr), size_(1) { /* None */ }
    ~RegularNode() { delete left_; delete right_; }

    bool isLeftNull()  const { return nullptr == left_; }
    bool isRightNull() const { return nullptr == right_; }

    void linkLeft(RegularNode * l) {
        left_ = l;
        size_ = leftSize(this) + rightSize(this) + 1;
    }

    void linkRight(RegularNode * r) {
        right_ = r;
        size_  = leftSize(this) + rightSize(this) + 1;
    }

    void link(RegularNode * l, RegularNode * r) {
        left_ = l; right_ = r;
        size_ = leftSize(this) + rightSize(this) + 1;
    }

    void unLink() {
        left_ = nullptr; right_ = nullptr;
        size_ = 1;
    }

    void unLinkLeft() {
        left_ = nullptr;
        size_ = rightSize(this) + 1;
    }

    void unLinkRight() {
        right_ = nullptr;
        size_  = leftSize(this) + 1;
    }

    RegularNode * left()  const { return left_; }
    RegularNode * right() const { return right_; }
    size_t        size()  const { return size_; }
private:
    RegularNode(const RegularNode &) = delete;
    RegularNode & operator=(const RegularNode &) = delete;

    RegularNode * left_, * right_;
    size_t size_;
};

////////////////////////////////////////////////////////////////////////////////
template<typename T>
struct StoreNode {
    StoreNode(T k) : left_(nullptr), right_(nullptr), key_(k) { /* None */ }
    ~StoreNode() { delete left_; delete right_; }

    bool isLeftNull()  const { return nullptr == left_; }
    bool isRightNull() const { return nullptr == right_; }
    void linkLeft(StoreNode * l)  { left_  = l; }
    void linkRight(StoreNode * r) { right_ = r; }
    void link(StoreNode * l, StoreNode * r) { left_ = l; right_ = r; }
    void unLink() { left_ = nullptr; right_ = nullptr; }
    void unLinkLeft()  { left_  = nullptr; }
    void unLinkRight() { right_ = nullptr; }
    void setKey(const T & k) { key_ = k; }
    StoreNode * left()  const { return left_; }
    StoreNode * right() const { return right_; }
    const T &         key()   const { return key_; }
    template<class N> friend N * updateNode(N * c);
private:
    StoreNode(const StoreNode &) = delete;
    StoreNode & operator=(const StoreNode &) = delete;

    StoreNode * left_, * right_;
    T key_;
};

////////////////////////////////////////////////////////////////////////////////
template<typename T>
struct ValueNode {
    ValueNode(T k) : left_(nullptr), right_(nullptr), size_(1), key_(k)
    { /* None */ }
    ~ValueNode() { delete left_; delete right_; }

    bool isLeftNull()  const { return nullptr == left_; }
    bool isRightNull() const { return nullptr == right_; }

    void linkLeft(ValueNode * l) {
        left_ = l;
        size_ = leftSize(this) + rightSize(this) + 1;
    }

    void linkRight(ValueNode * r) {
        right_ = r;
        size_  = leftSize(this) + rightSize(this) + 1;
    }

    void link(ValueNode * l, ValueNode * r) {
        left_ = l; right_ = r;
        size_ = leftSize(this) + rightSize(this) + 1;
    }

    void unLink() {
        left_ = nullptr; right_ = nullptr;
        size_ = 1;
    }

    void unLinkLeft() {
        left_ = nullptr;
        size_ = rightSize(this) + 1;
    }

    void unLinkRight() {
        right_ = nullptr;
        size_  = leftSize(this) + 1;
    }

    ValueNode * left()  const { return left_; }
    ValueNode * right() const { return right_; }
    size_t      size()  const { return size_; }
    const T &   key()   const { return key_; }
private:
    ValueNode(const ValueNode &) = delete;
    ValueNode & operator=(const ValueNode &) = delete;

    ValueNode * left_, * right_;
    size_t size_;
    T key_;
};

////////////////////////////////////////////////////////////////////////////////
template<class T> struct Node;
template<class T>
int8_t getBalance (const Node<T> * c) { return rightHight(c) - leftHight(c);}
template<class T>
int8_t hight      (const Node<T> * c) { return nullptr != c ? c->hight() : -1;}
template<class T>
int8_t leftHight  (const Node<T> * c) { return hight(c->left()); }
template<class T>
int8_t rightHight (const Node<T> * c) { return hight(c->right()); }

////////////////////////////////////////////////////////////////////////////////
template<typename T>
struct Node {
    Node(T k) : left_(nullptr), right_(nullptr), size_(1), key_(k), hight_(0)
    { /* None */ }
    ~Node() { delete left_; delete right_; }

    bool isLeftNull()  const { return nullptr == left_; }
    bool isRightNull() const { return nullptr == right_; }

    void linkLeft(Node * l) {
        left_  = l;
        size_  = leftSize(this) + rightSize(this) + 1;
        hight_ = std::max(leftHight(this), rightHight(this)) + 1;
    }

    void linkRight(Node * r) {
        right_ = r;
        size_  = leftSize(this) + rightSize(this) + 1;
        hight_ = std::max(leftHight(this), rightHight(this)) + 1;
    }

    void link(Node * l, Node * r) {
        left_  = l; right_ = r;
        size_  = leftSize(this) + rightSize(this) + 1;
        hight_ = std::max(leftHight(this), rightHight(this)) + 1;
    }

    void unLink() {
        left_ = nullptr; right_ = nullptr;
        size_ = 1;       hight_ = 0;
    }

    void unLinkLeft() {
        left_  = nullptr;
        size_  = rightSize(this) + 1;
        hight_ = rightHight(this) + 1;
    }

    void unLinkRight() {
        right_ = nullptr;
        size_  = leftSize(this) + 1;
        hight_ = leftHight(this) + 1;
    }

    Node *    left()  const { return left_; }
    Node *    right() const { return right_; }
    const T & key()   const { return key_; }
    size_t    size()  const { return size_; }
    int8_t    hight() const { return hight_; }
    template<class C> friend Node<C> * updateNode(Node<C> * c);
private:
    Node(const Node &) = delete;
    Node & operator=(const Node &) = delete;

    Node * left_, * right_;
    size_t size_;
    T key_;
    char hight_;
};

template<typename T>
StoreNode<T> * updateNode(StoreNode<T> * c) {
    return c;
}

template<typename T>
Node<T> * updateNode(Node<T> * c) {
    c->size_  = leftSize(c) + rightSize(c) + 1;
    c->hight_ = std::max(leftHight(c), rightHight(c)) + 1;
    return c;
}

template<class N>
N * updateNode(N * c) {
    c->size_  = leftSize(c) + rightSize(c) + 1;
    return c;
}

std::string toString(bool v) {
    std::ostringstream oss;
    std::boolalpha(oss);
    oss << v;
    return oss.str();
}
std::string toString(int v)                { return std::to_string(v); }
std::string toString(long v)               { return std::to_string(v); }
std::string toString(long long v)          { return std::to_string(v); }
std::string toString(unsigned v)           { return std::to_string(v); }
std::string toString(unsigned long v)      { return std::to_string(v); }
std::string toString(unsigned long long v) { return std::to_string(v); }
std::string toString(float v)              { return std::to_string(v); }
// std::string toString(double v)          { return std::to_string(v); }
std::string toString(long double v)        { return std::to_string(v); }
std::string toString(double v) {
    std::ostringstream oss;
    oss.precision(2);
    oss << std::scientific << v;
    return oss.str();
}

template<class N>
std::string nodeKeyToString(const N * c) {
    return isNotNull(c) ? toString(c->key()) : "";
}

std::ostream & nodeOutDetail(std::ostream & os, const BasicNode * c) {
    if (isNotNull(c)) {
        setws(os, 8) << c
          <<   " (left:"; setwz(os, 14) << c->left()
          << ") (right:"; setwz(os, 14) << c->right();
    }
    return os;
}

std::ostream & nodeOutDetail(std::ostream & os, const RegularNode * c) {
    if (isNotNull(c)) {
        setws(os, 8) << c
          <<   " (left:"; setwz(os, 14) << c->left()
          << ") (right:"; setwz(os, 14) << c->right()
          <<   ") size:" << c->size();
    }
    return os;
}

template<typename T>
std::ostream & nodeOutDetail(std::ostream & os, const StoreNode<T> * c) {
    if (isNotNull(c)) {
        setws(os, 8) << c
          <<         "|"; setws(os, 9)  << nodeKeyToString(c)
          <<   " (left:"; setwz(os, 14) << c->left()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->left())
          << ") (right:"; setwz(os, 14) << c->right()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->right());
    }
    return os;
}

template<typename T>
std::ostream & nodeOutDetail(std::ostream & os, const ValueNode<T> * c) {
    if (isNotNull(c)) {
        setws(os, 8) << c
          <<         "|"; setws(os, 9)  << nodeKeyToString(c)
          <<   " (left:"; setwz(os, 14) << c->left()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->left())
          << ") (right:"; setwz(os, 14) << c->right()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->right())
          <<   ") size:"; setws(os, 4)  << c->size();
    }
    return os;
}

template<typename T>
std::ostream & nodeOutDetail(std::ostream & os, const Node<T> * c) {
    if (isNotNull(c)) {
        setws(os, 8) << c
          <<         "|"; setws(os, 9)  << nodeKeyToString(c)
          <<   " (left:"; setwz(os, 14) << c->left()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->left())
          << ") (right:"; setwz(os, 14) << c->right()
          <<         "|"; setws(os, 9)  << nodeKeyToString(c->right())
          <<  ") hight:"; setws(os, 2)  << static_cast<int>(c->hight())
          <<    " size:"; setws(os, 4)  << c->size();
    }
    return os;
}

template<class N>
void parentReLink(N *& parent, N * oldNode, N * newNode) {
     if (parent->left() == oldNode)
         parent->linkLeft(newNode);
     else
         parent->linkRight(newNode);
}

template<class N>
void setChild(N *& node, bool toLeft, N * child) {
    if (isNull(node)) return;
    if (toLeft)
        node->linkLeft(child);
    else
        node->linkRight(child);
}


template<class N> struct NodePtrIntPair : public std::pair<N *, int> {
    NodePtrIntPair(const N * x, const int y) : std::pair<N *, int>(x, y)
    { /* None */ }
};

template<class N> bool
operator==(const NodePtrIntPair<N> & left, const NodePtrIntPair<N> & right) {
    return left.first == right.first;
}

template<class N> std::ostream & printHorizontal(std::ostream & os, N * v) {
    // Check for empty tree
    if (isNull(v)) return os;
    /* Initialize stack */
    N * c = v;
    std::stack<N *> stack;
    int position = 0;
    std::vector<NodePtrIntPair<N> > positions;
    while (not stack.empty() or isNotNull(v)) {
        if (isNotNull(v)) {
            stack.push(v);
            v = v->left();
        } else {
            v = stack.top(); stack.pop();
            // begin visit current
            std::string keyStr = isNotNull(v) ? toString(v->key()) : " ";
            os << " " << keyStr;
            position += 1;
            positions.push_back(NodePtrIntPair<N>(v, position));
            position += keyStr.length();
            // end visit current
            v = v->right();
        }
    }

    /*
    os << std::endl;
    for (auto & e : positions)
        os << e.second << " ";
    */
    os << std::endl;


    position = 0;
    int subPosition1 = 0;
    int currentLevel = 0;
    std::string subString;
    std::queue<NodePtrIntPair<N> > queue;
    queue.push(NodePtrIntPair<N>(c, 0));
    while (not queue.empty()) {
        auto element = queue.front(); queue.pop();
        auto current = element.first;
        int &  level = element.second;
        auto it = std::find(positions.begin(), positions.end(), element);
        int currentPosition = it->second;
        if (currentLevel < level) {
            os << std::endl << subString << std::endl;
            position = 0;
            subPosition1 = 0;
            currentLevel = level;
            subString.clear();
        }
        std::string keyStr = isNotNull(current) ? toString(current->key()) : " ";
        for (; position < currentPosition; ++position)
            os << " ";
        os << keyStr;
        position += keyStr.length();
        if (not current->isLeftNull()) {
            auto left = current->left();
            auto newElement = NodePtrIntPair<N>(left, level + 1);
            auto it = std::find(positions.begin(), positions.end(), newElement);
            queue.push(newElement);
            int currentSubPosition = it->second;

            std::string kStr = isNotNull(left) ? toString(left->key()) : " ";
            int limit = currentSubPosition + kStr.length() - 1;

            for (; subPosition1 < limit; ++subPosition1)
                subString.push_back(' ');
            subString.push_back('+'); ++subPosition1;
            limit = position - 1;
            for (; subPosition1 < limit; ++subPosition1)
                subString.push_back('-');
            subString.push_back('+'); ++subPosition1;
        } else {
            for (; subPosition1 < position - 1; ++subPosition1)
                subString.push_back(' ');
            if (current->isRightNull()) {
                subString.push_back(' '); ++subPosition1;
            } else {
                subString.push_back('+'); ++subPosition1;
            }
        }
        if (not current->isRightNull()) {
            auto right = current->right();
            auto newElement = NodePtrIntPair<N>(right, level + 1);
            auto it = std::find(positions.begin(), positions.end(), newElement);
            queue.push(newElement);
            int currentSubPosition = it->second;

            std::string kStr = isNotNull(right) ? toString(right->key()) : " ";
            int limit = currentSubPosition + kStr.length() - 1;
            for (; subPosition1 < limit; ++subPosition1)
                subString.push_back('-');
            subString.push_back('+'); ++subPosition1;
        }
    }

    return os;
}

template<class N> std::ostream & printVertical(std::ostream & os, N * v) {
    return printVertical(os, const_cast<const N *>(v), "", true);
}

template<class N> std::ostream &
printVertical(std::ostream & os, const N * v, std::string prefix, bool isTail)
{
    os << prefix << (isTail ? "└── " : "├── ") << v->key() << std::endl;
    std::string nextPrefix = prefix + (isTail ? "    " : "│   ");
    if (not v->isLeftNull() || not v->isRightNull()) {
        if (v->isLeftNull())
            os << nextPrefix << (v->isRightNull() ? "└── " : "├── ")
               << std::endl;
        else
            printVertical(os, v->left(), nextPrefix, v->isRightNull());
        if (v->isRightNull())
            ;// os << nextPrefix << "└── " << std::endl;
        else
            printVertical(os, v->right(), nextPrefix, true);
    }
    return os;
}

} // namespace svn

#endif // _NODE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
