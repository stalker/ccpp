################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using gxlc and gxlc++.

.SUFFIXES: .o .cpp

CFLAGS     += -DAIX -D_AIX_SOURCE $(EXTRA)
CCFLAGS    += -DAIX -D_AIX_SOURCE $(EXTRA)
RANLIB      = echo
AR          = ar
AWK         = awk

################################################################################
### modified built-in check make -p ###
# .cc recipe to execute (modified built-in):

.cpp   :
	$(CCC) $(CCFLAGS) $(LDFLAGS) $(LDLIBS) $< -o $@

.cpp.o :
	$(CCC) $(CCFLAGS) -c $<

#EOF
