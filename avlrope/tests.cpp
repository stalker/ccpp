// 3_2_Step_03_main_1.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <iostream>
#include "node.hpp"

int test1(std::istream & instm, std::ostream & outstm) try {
    {
        svn::BasicNode * bn1 = nullptr;
        outstm << "--- isNull(bn1):\t"        << std::boolalpha
               << svn::isNull(bn1)            << std::endl;
        outstm << "--- isNotNull(bn1):\t"     << std::boolalpha
               << svn::isNotNull(bn1)         << std::endl;
        bn1 = new svn::BasicNode;             
        outstm << "--- isNull(bn1):\t"        << std::boolalpha
               << svn::isNull(bn1)            << std::endl;
        outstm << "--- isNotNull(bn1):\t"     << std::boolalpha
               << isNotNull(bn1)              << std::endl;
        outstm << "--- bn1->isLeftNull():\t"  << std::boolalpha
               << bn1->isLeftNull()           << std::endl;
        outstm << "--- bn1->isRightNull():\t" << std::boolalpha
               << bn1->isRightNull()          << std::endl;
        svn::BasicNode * bn2 = new svn::BasicNode;
        svn::BasicNode * bn3 = new svn::BasicNode;
        bn1->link(bn1, bn2);
        outstm << "--- bn1->isLeftNull():\t"  << std::boolalpha
               << bn1->isLeftNull()           << std::endl;
        outstm << "--- bn1->isRightNull():\t" << std::boolalpha
               << bn1->isRightNull()          << std::endl;
        bn1->unLink();
        outstm << "--- bn1->isLeftNull():\t"  << std::boolalpha
               << bn1->isLeftNull()           << std::endl;
        outstm << "--- bn1->isRightNull():\t" << std::boolalpha
               << bn1->isRightNull()          << std::endl;
        delete bn3;
        delete bn2;
        delete bn1;
    }

    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

/* let's go */
int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(0);
    return test1(std::cin, std::cout);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
