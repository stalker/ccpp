/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#define _CXXTEST_HAVE_EH
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_IbsTreeTest_init = false;
#include "ibsTreeTest.hpp"

static IbsTreeTest suite_IbsTreeTest;

static CxxTest::List Tests_IbsTreeTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_IbsTreeTest( "ibsTreeTest.hpp", 57, "IbsTreeTest", suite_IbsTreeTest, Tests_IbsTreeTest );

static class TestDescription_suite_IbsTreeTest_test11PublicExtractNode : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_IbsTreeTest_test11PublicExtractNode() : CxxTest::RealTestDescription( Tests_IbsTreeTest, suiteDescription_IbsTreeTest, 578, "test11PublicExtractNode" ) {}
 void runTest() { suite_IbsTreeTest.test11PublicExtractNode(); }
} testDescription_suite_IbsTreeTest_test11PublicExtractNode;

static class TestDescription_suite_IbsTreeTest_test00ProtectedSplit : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_IbsTreeTest_test00ProtectedSplit() : CxxTest::RealTestDescription( Tests_IbsTreeTest, suiteDescription_IbsTreeTest, 645, "test00ProtectedSplit" ) {}
 void runTest() { suite_IbsTreeTest.test00ProtectedSplit(); }
} testDescription_suite_IbsTreeTest_test00ProtectedSplit;

static class TestDescription_suite_IbsTreeTest_test99PublicSplitTo : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_IbsTreeTest_test99PublicSplitTo() : CxxTest::RealTestDescription( Tests_IbsTreeTest, suiteDescription_IbsTreeTest, 719, "test99PublicSplitTo" ) {}
 void runTest() { suite_IbsTreeTest.test99PublicSplitTo(); }
} testDescription_suite_IbsTreeTest_test99PublicSplitTo;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
