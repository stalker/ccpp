////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#include "../node.hpp"

#include <cstddef>
#include <cstdint>

#undef DEBUG
#ifdef DEBUG
# include "mydebug.hpp"
#endif

class BasicNodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::BasicNode * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testLink(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            delete a;
        }
        void testLinkLeft(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            a->linkLeft(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete a;
        }
        void testLinkRight(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            a->linkRight(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            delete a;
        }
        void testUnLink(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete c;
            delete b;
            delete a;
        }
        void testUnLinkLeft(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLinkLeft();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            // c deleted 
            delete b;
            delete a;
        }
        void testUnLinkRight(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLinkRight();
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete c;
            // b deleted 
            delete a;
        }
};

class StoreNodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::StoreNode<int> * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testLink(void) {
            svn::StoreNode<int> * a = new svn::StoreNode<int>(1);
            svn::StoreNode<int> * b = new svn::StoreNode<int>(2);
            svn::StoreNode<int> * c = new svn::StoreNode<int>(3);
            a->link(b, c);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );

            delete a;
        }
        void testLinkLeft(void) {
            svn::StoreNode<int> * a = new svn::StoreNode<int>(1);
            svn::StoreNode<int> * b = new svn::StoreNode<int>(2);
            a->linkLeft(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            delete a;
        }
        void testLinkRight(void) {
            svn::StoreNode<int> * a = new svn::StoreNode<int>(1);
            svn::StoreNode<int> * b = new svn::StoreNode<int>(2);
            a->linkRight(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->right()->key() , b->key() );
            delete a;
        }
        void testUnLink(void) {
            svn::StoreNode<int> * a = new svn::StoreNode<int>(1);
            svn::StoreNode<int> * b = new svn::StoreNode<int>(2);
            svn::StoreNode<int> * c = new svn::StoreNode<int>(3);
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete c;
            delete b;
            delete a;
        }
        void testUnLinkLeft(void) {
            svn::StoreNode<int> * a = new svn::StoreNode<int>(1);
            svn::StoreNode<int> * b = new svn::StoreNode<int>(2);
            svn::StoreNode<int> * c = new svn::StoreNode<int>(3);
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            a->unLinkLeft();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            // c deleted 
            delete b;
            delete a;
        }
};

class RegularNodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::RegularNode * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testSize(void) {
            svn::RegularNode * a = new svn::RegularNode;
            TS_ASSERT_EQUALS( a->size() , 1 );
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            svn::RegularNode * b = new svn::RegularNode;
            TS_ASSERT_EQUALS( b->size() , 1 );
            TS_ASSERT_EQUALS( getSize(b) , 1 );
            svn::RegularNode * c = new svn::RegularNode;
            TS_ASSERT_EQUALS( c->size() , 1 );
            TS_ASSERT_EQUALS( getSize(c) , 1 );
            a->link(b, c);
            TS_ASSERT_EQUALS( a->size() , 3 );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 1 );
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            delete c;
            delete b;
            delete a;
        }
        void testLink(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            svn::RegularNode * c = new svn::RegularNode;
            a->link(b, c);
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            // c deleted
            // b deleted
            delete a;
        }
        void testLinkLeft(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            a->linkLeft(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            delete a;
            // b deleted
        }
        void testLinkRight(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            a->linkRight(b);
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            delete a;
            // b deleted
        }
        void testUnLink(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            svn::RegularNode * c = new svn::RegularNode;
            a->link(b, c);
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 1 );
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            delete c;
            delete b;
            delete a;
        }
        void testUnLinkLeft(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            svn::RegularNode * c = new svn::RegularNode;
            a->link(b, c);
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLinkLeft();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            delete b;
            // c delete below
            delete a;
        }
        void testUnLinkRight(void) {
            svn::RegularNode * a = new svn::RegularNode;
            svn::RegularNode * b = new svn::RegularNode;
            svn::RegularNode * c = new svn::RegularNode;
            a->link(b, c);
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLinkRight();
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            // delete b;
            delete c;
            delete a;
        }
};

class ValueNodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::ValueNode<char> * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testSize(void) {
            svn::ValueNode<char> * a = new svn::ValueNode<char>('a');
            TS_ASSERT_EQUALS( a->size() , 1 );
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            svn::ValueNode<char> * b = new svn::ValueNode<char>('b');
            TS_ASSERT_EQUALS( b->size() , 1 );
            TS_ASSERT_EQUALS( getSize(b) , 1 );
            svn::ValueNode<char> * c = new svn::ValueNode<char>('c');
            TS_ASSERT_EQUALS( c->size() , 1 );
            TS_ASSERT_EQUALS( getSize(c) , 1 );
            a->link(b, c);
            TS_ASSERT_EQUALS( a->size() , 3 );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            a->unLink();
            TS_ASSERT_EQUALS( a->size() , 1 );
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            delete a;
            delete b;
            delete c;
        }
        void testLink(void) {
            svn::ValueNode<char> * a = new svn::ValueNode<char>('a');
            svn::ValueNode<char> * b = new svn::ValueNode<char>('b');
            svn::ValueNode<char> * c = new svn::ValueNode<char>('c');
            a->link(b, c);
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS(a->left()->key(), b->key());
            TS_ASSERT_EQUALS(a->right()->key(), c->key());
            delete a;
            // b deleted
            // c deleted
        }
        void testLinkLeft(void) {
            svn::ValueNode<char> * a = new svn::ValueNode<char>('a');
            svn::ValueNode<char> * b = new svn::ValueNode<char>('b');
            a->linkLeft(b);
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS(a->left()->key(), b->key());
            delete a;
            // b deleted
        }
        void testLinkRight(void) {
            svn::ValueNode<char> * a = new svn::ValueNode<char>('a');
            svn::ValueNode<char> * b = new svn::ValueNode<char>('b');
            a->linkRight(b);
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS(a->right()->key(), b->key());
            delete a;
            // b deleted
            // c deleted
        }
};

class NodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::ValueNode<char> * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testLink(void) {
            svn::Node<int> * a = new svn::Node<int>(1);
            svn::Node<int> * b = new svn::Node<int>(2);
            svn::Node<int> * c = new svn::Node<int>(3);
            a->link(b, c);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT_EQUALS( hight(a) , 1 );

            delete a;
        }
        void testLinkLeft(void) {
            svn::Node<int> * a = new svn::Node<int>(1);
            svn::Node<int> * b = new svn::Node<int>(2);
            a->linkLeft(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            delete a;
        }
        void testLinkRight(void) {
            svn::Node<int> * a = new svn::Node<int>(1);
            svn::Node<int> * b = new svn::Node<int>(2);
            a->linkRight(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->right()->key() , b->key() );
            TS_ASSERT_EQUALS( a->size() , 2 );
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            delete a;
        }
        void testUnLink(void) {
            svn::Node<char> * a = new svn::Node<char>('a');
            svn::Node<char> * b = new svn::Node<char>('b');
            svn::Node<char> * c = new svn::Node<char>('c');
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( getSize(a) , 1 );
            TS_ASSERT_EQUALS( hight(a) , 0 );
            delete c;
            delete b;
            delete a;
        }
        void testUnLinkLeft(void) {
            svn::Node<char> * a = new svn::Node<char>('a');
            svn::Node<char> * b = new svn::Node<char>('b');
            svn::Node<char> * c = new svn::Node<char>('c');
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            a->unLinkLeft();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            delete a;
            delete b;
            // delete c;
        }
        void testUnLinkRight(void) {
            svn::Node<char> * a = new svn::Node<char>('a');
            svn::Node<char> * b = new svn::Node<char>('b');
            svn::Node<char> * c = new svn::Node<char>('c');
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            TS_ASSERT_EQUALS( a->right()->key() , c->key() );
            TS_ASSERT_EQUALS( getSize(a) , 3 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            a->unLinkRight();
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            TS_ASSERT_EQUALS( getSize(a) , 2 );
            TS_ASSERT_EQUALS( hight(a) , 1 );
            TS_ASSERT_EQUALS( a->left()->key() , b->key() );
            delete a;
            // delete b;
            delete c;
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
