////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#include "../basictree.hpp"
#include "../node.hpp"

#include <cstddef>
#include <cstdint>

#include <sstream>

#undef DEBUG
#ifdef DEBUG
# include "mydebug.hpp"
#endif

class BasicTreeTest : public CxxTest::TestSuite {
    public:
        void testBasicTree(void) {
            svn::BasicTree<svn::BasicNode> t1;
            std::ostringstream oss1;
            t1.treeOutByInOrder(oss1);
            TS_ASSERT_EQUALS( oss1.str(), "" );

            svn::BasicTree<svn::BasicNode> t3;
            std::ostringstream oss3;
            for (int i = 0; i < 6; ++i)
                t3.insert();
            t3.treeOutByInOrder(oss3) << std::endl;
            std::cout << std::endl;
            t3.treeOutByInOrder(std::cout);

            svn::BasicTree<svn::BasicNode> t4 = std::move(t3);
            std::ostringstream oss4;
            t3.treeOutByInOrder(oss4);
            TS_ASSERT_EQUALS( oss4.str(), "" );
        }

        void testBasicTreeCoincides(void) {
            svn::BasicTree<svn::BasicNode> t1;
            svn::BasicTree<svn::BasicNode> t2;

            for (int i = 0; i < 6; ++i)
                t1.insert();
            for (int i = 0; i < 6; ++i)
                t2.insert();
            TS_ASSERT(t1.coincides(t2));

            svn::BasicTree<svn::BasicNode> t3;
            svn::BasicTree<svn::BasicNode> t4;
            TS_ASSERT(t3.coincides(t4));
            TS_ASSERT(not t1.coincides(t3));

            svn::BasicTree<svn::BasicNode> t5;
            for (int i = 0; i < 8; ++i)
                t5.insert();
            TS_ASSERT(not t1.coincides(t5));

            svn::BasicTree<svn::BasicNode> t6;
            for (int i = 0; i < 5; ++i)
                t6.insert();
            TS_ASSERT(not t1.coincides(t6));
            t6.insert();
            TS_ASSERT(t1.coincides(t6));
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
