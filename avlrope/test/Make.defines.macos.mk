################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Common make definitions, customized for each platform
# Definitions required in all program directories to compile and link
# C programs using clang.

RANLIB      = echo
AR          = ar
AWK         = awk

CCSTD       = -std=c99
CXXSTD      = -std=c++11
CC          = clang
LD          = clang++ -o
CXX         = clang++
CFLAGS      = -c $(CCSTD) -Wall
CXXFLAGS    = -c $(CXXSTD) -Wall
CFLAGS     += -DMACOS -D_DARWIN_C_SOURCE $(EXTRA)
CXXFLAGS   += -DMACOS -D_DARWIN_C_SOURCE $(EXTRA)
CFLAGS     += -I/opt/local/include
CXXFLAGS   += -I/opt/local/include -I/Users/vnsk/bin/cxxtest
LDFLAGS    += -L/opt/local/lib

#EOF
