////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#include "../bstree.hpp"
#include "../basictree.hpp"
#include "../node.hpp"

#include <cfloat>  // DBL_MAX
#include <climits> // INT_MIN INT_MAX
#include <cstddef>
#include <cstdint>
#include <cstdlib> // std::rand
#include <ctime>   // std::time
#include <unistd.h>

#include <algorithm>
#include <sstream>
#include <vector>

#undef DEBUG
#ifdef DEBUG
# include "mydebug.hpp"
#endif

// #define ARRAY_SIZE01 16
// #define ARRAY_SIZE10 16
#define RANDOM_SIZE1 32
#define ARRAY_SIZE01 256
#define ARRAY_SIZE10 256
// #define ARRAY_SIZE01 131072
// #define ARRAY_SIZE10 131072

typedef svn::ValueNode<int>         ValueNodeInt;
typedef svn::BSTree<int,ValueNodeInt>  BSTreeInt;

class BasicTreeTest : public CxxTest::TestSuite, public BSTreeInt {
    public:
        const int sequance9[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
        const int * sequance9end = sequance9 + 9;
        void fillTree7(svn::IfaceBSTree<int> & t) {
            const int sequance7[] = {3, 1, 2, 0, 5, 4, 6};
            for (size_t i = 0; i < sizeof(sequance7) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance7[i]) );
                TS_ASSERT( not t.insert(sequance7[i]) );
            }
        }
        void fillTree9(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance9) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance9[i]) );
                TS_ASSERT( not t.insert(sequance9[i]) );
            }
        }
        const int sequance70[7] = {40, 20, 30, 15, 60, 50, 70};
        void fillTree70(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance70) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance70[i]) );
                TS_ASSERT( not t.insert(sequance70[i]) );
            }
        }
        void fillTreeRandom(svn::IfaceBSTree<int> & t, size_t n = ARRAY_SIZE10) {
            std::srand(std::time(NULL));
            for (size_t i = 0; i < n; /* None*/ ) {
                bool inserted = t.insert( static_cast<int>(std::rand()) );
                if (inserted) ++i;
            }
        }
        void testBSTreeInt(void) {
            TS_ASSERT_EQUALS( top(), nullptr );
            TS_ASSERT_EQUALS( detailOut, false );
        }
        void testProtectedInsert(void) {
            bool found = false;
            root = insert(root, found, 3);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->key(), 3 );
            root = insert(root, found, 3);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 1);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->key(), 1 );
            root = insert(root, found, 1);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 0);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->left()->key(), 0 );
            root = insert(root, found, 0);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 2);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->right()->key(), 2 );
            root = insert(root, found, 2);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 5);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->key(), 5 );
            root = insert(root, found, 5);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 4);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->left()->key(), 4 );
            root = insert(root, found, 4);
            TS_ASSERT( found );

            found = false;
            root = insert(root, found, 6);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->right()->key(), 6 );
            root = insert(root, found, 6);
            TS_ASSERT( found );

            delete root;
            root = nullptr;

            fillTree7(*this);
            TS_ASSERT_EQUALS( top()->key(), 3 );
            TS_ASSERT_EQUALS( top()->left()->key(), 1 );
            TS_ASSERT_EQUALS( top()->left()->left()->key(), 0 );
            TS_ASSERT_EQUALS( top()->left()->right()->key(), 2 );
            TS_ASSERT_EQUALS( top()->right()->key(), 5 );
            TS_ASSERT_EQUALS( top()->right()->left()->key(), 4 );
            TS_ASSERT_EQUALS( top()->right()->right()->key(), 6 );

            delete root;
            root = nullptr;
        }
        void testProtectedTreeOutInOrder(void) {
            size_t index = 0;
            std::ostringstream oss0;
            treeOutInOrder(oss0, root, index, ",");
            TS_ASSERT_EQUALS( oss0.str(), "" );

            fillTree7(*this);
            std::ostringstream oss7;
            index = 0;
            treeOutInOrder(oss7, root, index, ",");
            TS_ASSERT_EQUALS( oss7.str(), "0,1,2,3,4,5,6" );
            delete root;
            root = nullptr;

            fillTree9(*this);
            std::ostringstream oss9;
            index = 0;
            treeOutInOrder(oss9, root, index, ",");
            TS_ASSERT_EQUALS( oss9.str(), "1,3,4,6,7,8,10,13,14" );
            delete root;
            root = nullptr;
        }
        void testPublicTreeOutInOrder(void) {
            BSTreeInt t0;
            std::ostringstream oss0;
            t0.treeOutInOrder(oss0);
            TS_ASSERT_EQUALS( oss0.str(), "" );

            BSTreeInt t7;
            fillTree7(t7);
            std::ostringstream oss7;
            t7.treeOutInOrder(oss7);
            TS_ASSERT_EQUALS( oss7.str(), "0 1 2 3 4 5 6" );

            BSTreeInt t9;
            fillTree9(t9);
            std::ostringstream oss9;
            t9.treeOutInOrder(oss9);
            TS_ASSERT_EQUALS( oss9.str(), "1 3 4 6 7 8 10 13 14" );
        }
        void testPublicTreeOutInOrderRandom(void) {
            BSTreeInt bt;
            std::srand(std::time(NULL));
            int * testArray = new int[ARRAY_SIZE10];
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                bool inserted = bt.insert(rnd);
                if (inserted)
                   testArray[i++] = rnd;
            }
            std::sort(testArray, testArray + ARRAY_SIZE10);
            std::ostringstream ossArray;
            size_t i = 0; for (; i < ARRAY_SIZE10 - 1; ++i) {
                ossArray << testArray[i] << svn::sp;
            }
            ossArray << testArray[i];
            std::ostringstream ossTree;
            bt.treeOutInOrder(ossTree);
            TS_ASSERT_EQUALS( ossArray.str(), ossTree.str() );
            delete[] testArray;
        }
        void testProtectedTreeSearchNode(void) {
            TS_ASSERT_EQUALS( searchNode(top(), INT_MAX), nullptr );
            fillTree7(*this);
            for (int i = -1; i < 15; ++i) {
                if (-1 < i && i < 7) {
                    TS_ASSERT( svn::isNotNull(searchNode(top(), i)) );
                } else
                    TS_ASSERT( svn::isNull(searchNode(top(), i)) );
            }
            delete root;
            root = nullptr;

            fillTree9(*this);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance9, sequance9end, i);
                if (sequance9end != p) {
                    TS_ASSERT( svn::isNotNull(searchNode(top(), i)) );
                } else
                    TS_ASSERT( svn::isNull(searchNode(top(), i)) );
            }
            delete root;
            root = nullptr;
        }
        void testProtectedTreeSearchNodeRandom(void) {
            std::srand(std::time(NULL));
            std::vector<int> testVector;
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                if (insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }
            while (not testVector.empty()) {
                int i = testVector.back();
                testVector.pop_back();
                TS_ASSERT( svn::isNotNull(searchNode(top(), i)) );
            }
            delete root;
            root = nullptr;
        }
        void testPublicFind(void) {
            BSTreeInt t0;
            TS_ASSERT( not t0.find(INT_MIN) );

            BSTreeInt t7;
            fillTree7(t7);
            for (int i = -1; i < 15; ++i) {
                if (-1 < i && i < 7) {
                    TS_ASSERT( t7.find(i) );
                } else
                    TS_ASSERT( not t7.find(i) );
            }
            BSTreeInt t9;
            fillTree9(t9);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance9, sequance9end, i);
                if (sequance9end != p) {
                    TS_ASSERT( t9.find(i) );
                } else
                    TS_ASSERT( not t9.find(i) );
            }
        }
        void testProtectedSearchMinimum(void) {
            TS_ASSERT_EQUALS( searchMinimum(top()), nullptr );

            fillTree9(*this);
            TS_ASSERT_EQUALS( searchMinimum(top())->key(), 1);
            delete root;
            root = nullptr;

            insert(1);
            TS_ASSERT_EQUALS( searchMinimum(top())->key(), 1);
            delete root;
            root = nullptr;

            fillTree9(*this);
            insert(2);
            TS_ASSERT_EQUALS( searchMinimum(top())->key(), 1);
            delete root;
            root = nullptr;
        }
        void testProtectedSearchMinimumRandom(void) {
            int min = INT_MAX;
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                if (insert(rnd)) {
                    ++i;
                    if (rnd < min) min = rnd;
                }
            }
            TS_ASSERT_EQUALS( searchMinimum(top())->key(), min);
            delete root;
            root = nullptr;
        }
        void testPublicFindMin(void) {
            BSTreeInt t0;
            try {
                t0.findMin();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            BSTreeInt t1;
            fillTree9(t1);
            TS_ASSERT_EQUALS( t1.findMin(), 1);

            BSTreeInt t11;
            t11.insert(1);
            TS_ASSERT_EQUALS( t11.findMin(), 1);

            BSTreeInt t2;
            fillTree9(t2);
            t2.insert(2);
            TS_ASSERT_EQUALS( t2.findMin(), 1);

            BSTreeInt t91;
            fillTree9(t91);
            t91.insert(INT_MIN);
            TS_ASSERT_EQUALS( t91.findMin(), INT_MIN);
        }
        void testProtectedSearchMaximum(void) {
            TS_ASSERT_EQUALS( searchMaximum(top()), nullptr );
            delete root;
            root = nullptr;

            fillTree9(*this);
            TS_ASSERT_EQUALS( searchMaximum(top())->key(), 14);
            delete root;
            root = nullptr;

            insert(1);
            TS_ASSERT_EQUALS( searchMaximum(top())->key(), 1);
            delete root;
            root = nullptr;

            fillTree9(*this);
            insert(15);
            TS_ASSERT_EQUALS( searchMaximum(top())->key(), 15);
            delete root;
            root = nullptr;
        }
        void testProtectedSearchMaximumRandom(void) {
            int max = INT_MIN;
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                if (insert(rnd)) {
                    ++i;
                    if (max < rnd) max = rnd;
                }
            }
            TS_ASSERT_EQUALS( searchMaximum(top())->key(), max);
            delete root;
            root = nullptr;
        }
        void testPublicFindMax(void) {
            BSTreeInt t0;
            try {
                t0.findMax();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            BSTreeInt t1;
            fillTree9(t1);
            TS_ASSERT_EQUALS( t1.findMax(), 14);

            BSTreeInt t11;
            t11.insert(1);
            TS_ASSERT_EQUALS( t11.findMax(), 1);

            BSTreeInt t21;
            fillTree9(t21);
            t21.insert(15);
            TS_ASSERT_EQUALS( t21.findMax(), 15);

            BSTreeInt t99;
            fillTree9(t99);
            t99.insert(INT_MAX);
            TS_ASSERT_EQUALS( t99.findMax(), INT_MAX);
        }
        void testProtectedNextNode(void) {
            fillTree9(*this);
            TS_ASSERT_EQUALS( nextNode(searchMaximum(top())), nullptr );
            TS_ASSERT_EQUALS( nextNode(searchMinimum(top()))->key(), 3 );
            delete root;
            root = nullptr;

            fillTree7(*this);
            for (int i = 0, j = 1; i < 6; ++i, ++j) {
                TS_ASSERT_EQUALS( nextNode(searchNode(top(), i))->key(), j );
            }
            TS_ASSERT_EQUALS( nextNode(searchNode(top(), 6)), nullptr );
            delete root;
            root = nullptr;
        }

        void testProtectedPrevNode(void) {
            fillTree9(*this);
            TS_ASSERT_EQUALS( prevNode(searchMaximum(top()))->key(), 13 );
            TS_ASSERT_EQUALS( prevNode(searchMinimum(top())), nullptr );
            delete root;
            root = nullptr;

            fillTree7(*this);

#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "this->root: " << top() << std::endl;
            detailOut = true;
            std::cerr << std::endl << *this << std::endl;
#endif
            for (int i = 6, j = 5; i > 0; --i, --j) {
                TS_ASSERT_EQUALS( prevNode(searchNode(top(), i))->key(), j );
            }
            TS_ASSERT_EQUALS( prevNode(searchNode(top(), 0)), nullptr );
            delete root;
            root = nullptr;
        }

        void testPublicNext(void) {
            BSTreeInt bt;
            std::srand(std::time(NULL));
            int * testArray = new int[ARRAY_SIZE10];
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                bool inserted = bt.insert(rnd);
                if (inserted)
                   testArray[i++] = rnd;
            }
            std::sort(testArray, testArray + ARRAY_SIZE10);

            for (int i = 0, j = 1; i < ARRAY_SIZE10 - 1; ++i, ++j) {
                TS_ASSERT_EQUALS( bt.next(testArray[i]), testArray[j] );
            }
            TS_ASSERT_EQUALS( nextNode(searchNode(top(), ARRAY_SIZE10)), nullptr);

            delete[] testArray;
        }
        void testProtectedExtractNode(void) {
            BSTreeInt::node_t * result = nullptr;
            root = extractNode(result, root, INT_MAX);
            TS_ASSERT_EQUALS( root, nullptr);
            TS_ASSERT_EQUALS( result, nullptr);

            fillTree9(*this);
            BSTreeInt::node_t * n13 = nullptr;
            root = extractNode(n13, root, 13);
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS( n13->key(), 13 );
            delete n13;
            delete root;
            root = nullptr;

            fillTree9(*this);
            BSTreeInt::node_t * n14 = nullptr;
            root = extractNode(n14, root, 14);
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS( n14->key(), 14 );
            delete n14;
            delete root;
            root = nullptr;

            fillTree9(*this);
            BSTreeInt::node_t * n3 = nullptr;
            root = extractNode(n3, root, 3);
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS( n3->key(), 3 );
            delete n3;
            delete root;
            root = nullptr;

            std::cerr << std::endl;
            fillTree9(*this);
            detailOut = true;
            for (int * p = const_cast<int *>(sequance9); p < sequance9end; ++p)
            {
                BSTreeInt::node_t * n = nullptr;
                root = extractNode(n, root, *p);
                TS_ASSERT( isNotNull(n) );
                TS_ASSERT_EQUALS( n->key(), *p );
                delete n;
            }
            // delete root;
            root = nullptr;
        }
        void testProtectedExtractNodeRandom(void) {
            std::srand(std::time(NULL));
            std::vector<int> testVector;
            for (size_t i = 0; i < ARRAY_SIZE01; /* None*/ ) {
                int rnd = static_cast<int>(std::rand()) % (10*ARRAY_SIZE01);
                if (insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }
#ifdef DETAIL_OUT_CERR
            std::cerr << "==============================================="
                         "================================="<< std::endl;
#endif
            while (not testVector.empty()) {
                int e = testVector.back();
                testVector.pop_back();

#ifdef DETAIL_OUT_CERR
                std::cerr << "i: " << e << std::endl;
#endif
                BSTreeInt::node_t * n = nullptr;
#ifdef DETAIL_OUT_CERR
                std::cerr << "before: extractNode(" << e
                          << ") root: " << root << std::endl;
                std::cerr << *this << std::endl;
#endif
                root = extractNode(n, root, e);
#ifdef DETAIL_OUT_CERR
                std::cerr << "after:  extractNode(" << e
                          << ") root: " << root << std::endl;
                std::cerr << *this << std::endl;
#endif
                TS_ASSERT( isNotNull(n) );
                TS_ASSERT_EQUALS( n->key(), e );
#ifdef DETAIL_OUT_CERR
                std::cerr << "==============================================="
                             "================================="<< std::endl;
#endif
                delete n;
            }
            // delete root;
            root = nullptr;
        }

        void testPublicRemove(void) {
            BSTree t0;
            TS_ASSERT( not t0.remove(INT_MAX) );

            BSTree t9_13;
            fillTree9(t9_13);
            TS_ASSERT( t9_13.remove(13) );

            BSTree t9_14;
            fillTree9(t9_14);
            TS_ASSERT( t9_14.remove(14) );

            BSTree t9_3;
            fillTree9(t9_3);
            TS_ASSERT( t9_3.remove(3) );
        }
        void testPublicRemoveRandom(void) {
            BSTree bt;
            std::srand(std::time(NULL));
            std::vector<int> testVector;
            for (size_t i = 0; i < ARRAY_SIZE01; /* None*/ ) {
                int rnd = static_cast<int>(std::rand()) % (10*ARRAY_SIZE01);
                if (bt.insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }
            bt.detailOut = true;
#ifdef DETAIL_OUT_CERR
            std::cerr << "==============================================="
                         "================================="<< std::endl;
#endif
            while (not testVector.empty()) {
                int e = testVector.back();
                testVector.pop_back();

#ifdef DETAIL_OUT_CERR
                std::cerr << "e: " << e << std::endl;
                std::cerr << "before: extractNode(" << e
                          << ") root: " << bt.top() << std::endl;
                std::cerr << bt << std::endl;
#endif
                bt.remove(e);
#ifdef DETAIL_OUT_CERR
                std::cerr << "after:  extractNode(" << e
                          << ") root: " << bt.top() << std::endl;
                std::cerr << bt << std::endl;
                std::cerr << "==============================================="
                             "================================="<< std::endl;
#endif
            }

        }
        void testProtectedExtractMinimum(void) {
            BSTreeInt::node_t * result = nullptr;
            root = extractNode(result, root, INT_MIN);
            TS_ASSERT_EQUALS( root, nullptr);
            TS_ASSERT_EQUALS( result, nullptr);

            fillTree9(*this);
            BSTreeInt::node_t * n1 = nullptr;
            root = extractMinimum(n1, root);
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS( n1->key(), 1 );
            delete n1;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n2 = nullptr;
            root = extractMinimum(n2, root);
            TS_ASSERT( isNotNull(n2) );
            TS_ASSERT_EQUALS( n2->key(), 3 );
            delete n2;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n4 = nullptr;
            root = extractMinimum(n4, root);
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS( n4->key(), 4 );
            delete n4;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n6 = nullptr;
            root = extractMinimum(n6, root);
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS( n6->key(), 6 );
            delete n6;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n7 = nullptr;
            root = extractMinimum(n7, root);
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS( n7->key(), 7 );
            delete n7;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n8 = nullptr;
            root = extractMinimum(n8, root);
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS( n8->key(), 8 );
            delete n8;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n10 = nullptr;
            root = extractMinimum(n10, root);
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS( n10->key(), 10 );
            delete n10;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n13 = nullptr;
            root = extractMinimum(n13, root);
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS( n13->key(), 13 );
            delete n13;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n14 = nullptr;
            root = extractMinimum(n14, root);
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS( n14->key(), 14 );
            delete n14;
            TS_ASSERT( isNull(top()) );

            delete root;
            root = nullptr;
        }
        void testProtectedExtractMinimumRandom(void) {
            std::srand(std::time(NULL));
            std::vector<int> testVector;
            for (size_t i = 0; i < ARRAY_SIZE01; /* None*/ ) {
                int rnd = static_cast<int>(std::rand()) % (10*ARRAY_SIZE01);
                if (insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }
            std::sort(testVector.rbegin(), testVector.rend());

#ifdef DETAIL_OUT_CERR
            detailOut = true;
            std::cerr << "==============================================="
                         "================================="<< std::endl;
#endif
            while (not testVector.empty()) {
                int e = testVector.back();
                testVector.pop_back();
                BSTreeInt::node_t * n1 = nullptr;
#ifdef DETAIL_OUT_CERR
                    std::cerr << "before: extractMinimum("
                              << ") root: " << top() << std::endl;
                    std::cerr << *this << std::endl;
#endif
                root = extractMinimum(n1, root);
#ifdef DETAIL_OUT_CERR
                    std::cerr << "after:  extractMinimum("
                              << ") root: " << top() << std::endl;
                    std::cerr << *this << std::endl;
                    std::cerr << "==============================================="
                                 "================================="<< std::endl;
#endif
                TS_ASSERT( isNotNull(n1) );
                TS_ASSERT_EQUALS( n1->key(), e );
                delete n1;
                if (testVector.empty()) {
                    TS_ASSERT( isNull(top()) );
                } else
                    TS_ASSERT( isNotNull(top()) );
            }
        }
        void testProtectedExtractMaximum(void) {
            BSTreeInt::node_t * result = nullptr;
            root = extractNode(result, root, INT_MAX);
            TS_ASSERT_EQUALS( root, nullptr);
            TS_ASSERT_EQUALS( result, nullptr);

            fillTree9(*this);
            std::cerr << "root: " << this->root << std::endl;
            std::cerr << *this << std::endl;
            BSTreeInt::node_t * n14 = nullptr;
            root = extractMaximum(n14, root);
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS( n14->key(), 14 );
            delete n14;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n13 = nullptr;
            root = extractMaximum(n13, root);
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS( n13->key(), 13 );
            delete n13;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n10 = nullptr;
            root = extractMaximum(n10, root);
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS( n10->key(), 10 );
            delete n10;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n8 = nullptr;
            root = extractMaximum(n8, root);
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS( n8->key(), 8 );
            delete n8;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n7 = nullptr;
            root = extractMaximum(n7, root);
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS( n7->key(), 7 );
            delete n7;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n6 = nullptr;
            root = extractMaximum(n6, root);
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS( n6->key(), 6 );
            delete n6;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n4 = nullptr;
            root = extractMaximum(n4, root);
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS( n4->key(), 4 );
            delete n4;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n2 = nullptr;
            root = extractMaximum(n2, root);
            TS_ASSERT( isNotNull(n2) );
            TS_ASSERT_EQUALS( n2->key(), 3 );
            delete n2;
            TS_ASSERT( isNotNull(top()) );

            BSTreeInt::node_t * n1 = nullptr;
            root = extractMaximum(n1, root);
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS( n1->key(), 1 );
            delete n1;
            TS_ASSERT( isNull(top()) );

            delete root;
            root = nullptr;
        }
        void testProtectedExtractMaximumRandom(void) {
            std::srand(std::time(NULL));
            std::vector<int> testVector;
            for (size_t i = 0; i < ARRAY_SIZE01; /* None*/ ) {
                int rnd = static_cast<int>(std::rand()) % (10*ARRAY_SIZE01);
                if (insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }
            std::sort(testVector.begin(), testVector.end());

            detailOut = true;
            
#ifdef DETAIL_OUT_CERR
            std::cerr << "==============================================="
                         "================================="<< std::endl;
#endif
            while (not testVector.empty()) {
                int e = testVector.back();
                testVector.pop_back();
                BSTreeInt::node_t * n1 = nullptr;
#ifdef DETAIL_OUT_CERR
                    std::cerr << "before: extractMaximum("
                              << ") root: " << top() << std::endl;
                    std::cerr << *this << std::endl;
#endif
                root = extractMaximum(n1 , root);
#ifdef DETAIL_OUT_CERR
                    std::cerr << "after:  extractMaximum("
                              << ") root: " << top() << std::endl;
                    std::cerr << *this << std::endl;
                    std::cerr << "==============================================="
                                 "================================="<< std::endl;
#endif
                TS_ASSERT( isNotNull(n1) );
                TS_ASSERT_EQUALS( n1->key(), e );
                delete n1;
                if (testVector.empty()) {
                    TS_ASSERT( isNull(top()) );
                } else
                    TS_ASSERT( isNotNull(top()) );
            }
        }

        void testPublicIsEmptyRandom(void) {
            std::srand(std::time(NULL));
            std::vector<int> testVector;

            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ ) {
                int rnd = static_cast<int>(std::rand());
                if (insert(rnd)) {
                    testVector.push_back(rnd);
                    ++i;
                }
            }

            while (not testVector.empty()) {
                int e = testVector.back();
                testVector.pop_back();
                TS_ASSERT( remove(e) );
            }
            TS_ASSERT( isNull(top()) );
            TS_ASSERT( isEmpty() );

            delete root;
            root = nullptr;
        }
        void testPublicIsBinarySearchTree(void) {
            BSTreeInt t0;
            TS_ASSERT( t0.isBinarySearchTree() );

            BSTreeInt t7;
            fillTree7(t7);
            TS_ASSERT( t7.isBinarySearchTree() );

            fillTree9(*this);
            TS_ASSERT( isBinarySearchTree() );
            TS_ASSERT( isNull(root->right()->left()) );
            root->right()->linkLeft(new BSTree::node_t(2));
            TS_ASSERT( not isBinarySearchTree() );

            delete root;
            root = nullptr;
        }
        void testPublicIsBinarySearchTreeRandom(void) {
            BSTreeInt t;
            for (size_t i = 0; i < ARRAY_SIZE10; /* None*/ )
                if (t.insert(static_cast<int>(std::rand()))) ++i;
            TS_ASSERT( t.isBinarySearchTree() );
        }
        double random(double min, double max) {
            return static_cast<double>(std::rand())/RAND_MAX*(max - min) + min;
        }
        void testPublicIsBinarySearchTreeDoubleRandom(void) {
            svn::BSTree<double> t;
            for (size_t i = 0; i < ARRAY_SIZE10; ++i ) {
                double rnd = random(-FLT_MAX, FLT_MAX);
                if (t.insert(rnd)) ++i;
            }
            t.detailOut = true;
#ifdef DETAIL_OUT_CERR
            std::cerr << "isBinarySearchTree()"
                      << " root: " << t.top() << std::endl;
            std::cerr << t << std::endl;
            std::cerr << "==============================================="
                         "================================="<< std::endl;
#endif
            TS_ASSERT( t.isBinarySearchTree() );
        }
        void testPublicCoincides(void) {
            BSTreeInt t0l, t0r;
            TS_ASSERT( t0l.coincides(t0r) );

            BSTreeInt t7l, t7r;
            fillTree7(t7l);
            fillTree7(t7r);
            TS_ASSERT( t7l.coincides(t7r) );

            BSTreeInt t9l, t9r;
            fillTree9(t9l);
            fillTree9(t9r);
            TS_ASSERT( t9l.coincides(t9r) );
        }
        void testPublicBSTreeIterator(void) {
            BSTreeInt t0;
            auto it = t0.begin();
            TS_ASSERT( isNull(it) );
            ++it;
            it++;

            BSTreeInt t7;
            fillTree7(t7);
            int i = 0;
            for (auto it = t7.begin(); it != t7.end(); ++it, ++i) {
                TS_ASSERT_EQUALS(it->key(), i);
            }
            i = 0;
            for (auto & e : t7) {
                TS_ASSERT_EQUALS(e.key(), i++);
            }
        }
        void testPublicBSTreeEquals(void) {
            BSTreeInt t0l;
            BSTreeInt t0r;
            TS_ASSERT( t0l.equals(t0r) );


            BSTreeInt t7l;
            fillTree7(t7l);
            BSTreeInt t7r;
            fillTree7(t7r);
            TS_ASSERT( t7l.equals(t7r) );
            TS_ASSERT( not t7l.equals(t0r) );
            TS_ASSERT( not t0l.equals(t7r) );

            BSTreeInt t7u;
            for (int i = 0 ; i < 7; ++i) {
                t7u.insert(i);
            }
            TS_ASSERT( t7l.equals(t7u) );

            BSTreeInt t7d;
            for (int i = 6 ; 0 <= i; --i) {
                t7d.insert(i);
            }
            TS_ASSERT( t7l.equals(t7d) );


            BSTreeInt t9l;
            fillTree7(t9l);
            BSTreeInt t9r;
            fillTree7(t9r);
        }  
        void testPublicBSTreeEqualRandom(void) {
            std::vector<int> testVector1;
            std::vector<int> testVector2;
            BSTreeInt tl, tr, t0;
            std::srand(std::time(NULL));
            for (size_t i = 0; i < RANDOM_SIZE1; /* Node */ ) {
                int rnd = static_cast<int>(std::rand()) % (3*RANDOM_SIZE1);
                if (t0.insert(rnd)) {
                    testVector1.push_back(rnd);
                    testVector2.push_back(rnd);
                    ++i;
                }
            }
            std::srand(std::time(NULL)-getpid());
            while (not testVector1.empty()) {
                int rnd = static_cast<int>(std::rand()) % testVector1.size();
                auto it = testVector1.begin() + rnd;
                tl.insert(*it);
                testVector1.erase(it);
            }
            std::srand(std::time(NULL));
#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "tl.root: " << tl.top() << std::endl;
            tl.detailOut = true;
#endif
            std::cerr << std::endl << tl << std::endl;
            while (not testVector2.empty()) {
                int rnd = static_cast<int>(std::rand()) % testVector2.size();
                auto it = testVector2.begin() + rnd;
                tr.insert(*it);
                testVector2.erase(it);
            }
#ifdef DETAIL_OUT_CERR
            std::cerr << "tr.root: " << tr.top() << std::endl;
            tr.detailOut = true;
#endif
            std::cerr <<  tr << std::endl;
            TS_ASSERT( tl.equals(tr) );
            std::boolalpha(std::cerr);
            std::cerr << "tl == tr: " << (tl == tr) << std::endl;
            std::cerr << "tl != tr: " << (tl != tr) << std::endl;
            std::cerr << "tl.root: " << tl.top() << std::endl;
            tl.detailOut = true;
            std::cerr <<  tl << std::endl;
            BSTreeInt ttt = tl;
            std::cerr << "tl == ttt: " << (tl == ttt) << std::endl;
            std::cerr << "tr == ttt: " << (tr == ttt) << std::endl;
            std::cerr << "ttt.root: " << ttt.top() << std::endl;
            ttt.detailOut = true;
            std::cerr << ttt << std::endl;
        }
        void testPublicBSTreeClone(void) {
            BSTreeInt t0(nullptr);
            TS_ASSERT( t0.isEmpty() );

            BSTreeInt t7;
            fillTree7(t7);
            BSTreeInt t7n(t7.clone());
            TS_ASSERT( t7.coincides(t7n) );

            BSTreeInt t9;
            fillTree9(t9);
            BSTreeInt t9n(t9.clone());
            TS_ASSERT( t9.coincides(t9n) );
        }
        void testPublicMerge(void) {
            fillTree7(*this);
            int max = findMax();
            BSTreeInt tn8;
            tn8.insert(8);
            toMerge(tn8);
            TS_ASSERT_EQUALS(root->key(), max);
            TS_ASSERT_EQUALS(findMax(), 8);

            delete root;
            root = nullptr;

            BSTreeInt t7;
            fillTree7(t7);

#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t7.root: " << t7.top() << std::endl;
            t7.detailOut = true;
            std::cerr << std::endl << t7 << std::endl;
#endif

            BSTreeInt t70;
            fillTree70(t70);

#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t70.root: " << t70.top() << std::endl;
            t70.detailOut = true;
            std::cerr << std::endl << t70 << std::endl;
#endif

            t7.toMerge(t70);
            TS_ASSERT_EQUALS(t70.top(), nullptr);

#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t7.root: " << t7.top() << std::endl;
            t7.detailOut = true;
            std::cerr << std::endl << t7 << std::endl;
#endif

            BSTreeInt t9;
            fillTree9(t9);
#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t9.root: " << t9.top() << std::endl;
            t9.detailOut = true;
            std::cerr << std::endl << t9 << std::endl;
#endif
            fillTree70(t70);
#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t70.root: " << t70.top() << std::endl;
            t70.detailOut = true;
            std::cerr << std::endl << t70 << std::endl;
#endif
            t9.toMerge(t70);
            TS_ASSERT_EQUALS(t70.top(), nullptr);
#ifdef DETAIL_OUT_CERR
            std::cerr << std::endl << "t9.root: " << t9.top() << std::endl;
            t9.detailOut = true;
            std::cerr << std::endl << t9 << std::endl;
#endif
        }
        void testPublicSplit(void) {
            BSTreeInt t9, t70;
            fillTree9(t9);
            fillTree70(t70);
            t9.toMerge(t70);
            BSTreeInt b, c;
            t9.splitTo(b, c, 14);
            for (int i = 0; i < 71; ++i) {
                t9.splitTo(b, c, i);
#ifdef DETAIL_OUT_CERR
                std::cerr << std::endl << "split by: " << i << std::endl;
                std::cerr << "b.root: " << b.top() << " " << nodeKeyToString(b.top()) << std::endl;
                b.detailOut = true;
                std::cerr << b << std::endl;
                std::cerr << "c.root: " << c.top() << " " << nodeKeyToString(b.top()) << std::endl;
                c.detailOut = true;
                std::cerr << std::endl << c << std::endl;
#endif
                t9.toMerge(b);
                t9.toMerge(c);
#ifdef DETAIL_OUT_CERR
                std::cerr << std::endl << "merge a and b to t9" << std::endl;
                std::cerr << std::endl << "t9.root: " << t9.top() << " " << nodeKeyToString(t9.top())  << std::endl;
                t9.detailOut = true;
                std::cerr << std::endl << t9 << std::endl;
#endif
            }
        }

};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=98:ts=4:sw=4:sts=4:et
 * EOF */
