 # (leading space required for Xenix /bin/sh)

#
# Determine the type of *ix operating system that we're
# running on, and echo an appropriate value.
# This script is intended to be used in Makefiles.
# (This is a kludge.  Gotta be a better way.)
#

case `uname -m` in
"amd64")
	BITS="64"
	;;
"x86_64")
	BITS="64"
	;;
"i686")
	BITS="32"
	;;
"i686-pc")
	BITS="32"
	;;
*)
	echo "Unknown machine" >&2
	exit 1
esac
echo $BITS
exit 0
