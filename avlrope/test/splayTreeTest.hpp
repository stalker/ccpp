////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#define BIG 1

#include "../splaytree.hpp"
// #include "../bstree.hpp"
#include "../ifacebst.hpp"
#include "../basictree.hpp"
#include "../node.hpp"

#include <cfloat>  // DBL_MAX
#include <climits> // INT_MIN INT_MAX
#include <cstddef>
#include <cstdint>
#include <cstdlib> // std::rand
#include <ctime>   // std::time
#include <unistd.h>

#include <algorithm>
#include <sstream>
#include <vector>

#undef  DEBUG
#define DEBUG 2
#ifdef  DEBUG
# include "mydebug.hpp"
#endif

#define SIZE5  32
#define SIZE10 16
// #define SIZE10 1024
#define SIZE15 32768
// #define SIZE20 131072+51200
// #define SIZE20 262144+51200
// #define SIZE20 313342
// #define SIZE20 262031
#define SIZE19 524288
#define SIZE20 1048576
#define SIZE21 2097152
#define SIZE22 4194304
#define SIZE23 8388608
#define SIZE24 16777216

typedef svn::StoreNode<int>          NodeInt;
typedef svn::SplayTree<int, NodeInt> SplayTreeInt;

// TODO: Don't user Node<T>!!!
class SplayTreeTest : public CxxTest::TestSuite {
    public:
        const int sequance9[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
        const int * sequance9end = sequance9 + 9;
        const int sequance70[7] = {40, 20, 30, 15, 60, 50, 70};
        const int * sequance70end = sequance70 + 7;
        std::vector<int> randomInts;

        void debugOUT(std::ostream & os, SplayTreeInt & t) {
            t.detailOut = true;
            os << std::endl << "root: " << t.top();
            os << std::endl << t << std::endl;
            t.detailOut = false;
        }
#define DEBUG_OUT(o, t) do{o<<#t;debugOUT(o,t);}while(0);

        void fillTree7(svn::IfaceBSTree<int> & t) {
            const int sequance7[] = {3, 1, 2, 0, 5, 4, 6};
            for (size_t i = 0; i < sizeof(sequance7) / sizeof(int); ++i) {
                // std::cerr << "insert: " << sequance7[i] << " to ";
                TS_ASSERT( t.insert(sequance7[i]) );
                // DEBUG_OUT(std::cerr, t);
                TS_ASSERT( not t.insert(sequance7[i]) );
            }
        }
        void fillTree9(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance9) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance9[i]) );
                TS_ASSERT( not t.insert(sequance9[i]) );
            }
        }
        void fillTree70(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance70) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance70[i]) );
                TS_ASSERT( not t.insert(sequance70[i]) );
            }
        }
        void fillTreeSize1024(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE10; ++i) {
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void fillTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE20; ++i) {
                std::cerr << "insert: " << i << std::endl;
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void randomTreeSize1Kib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE10);
            for (size_t i = 0; i < SIZE10; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (10*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1KibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Kib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }

        void treeSize1Mib(svn::IfaceBSTree<int> & t) {
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < randomInts.size(); ++i) {
                randomInts[i] = static_cast<int>(i);
                TS_ASSERT( t.insert(randomInts[i]) );
            }
        }

        void randomTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < SIZE20; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (99*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1MibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Mib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }
        void randomIntsOut(std::ostream & os) {
            size_t i = 0; for (; i < randomInts.size() - 1; ++i)
               os << randomInts[i] << " ";
            os << randomInts[i];
        }

        void test00SplayTreeInt(void) {
#if defined(DEBUG) && defined(PRINTM)
            std::cerr << std::endl;
            printd("ok: this:", this) << std::endl;
#endif  
        }
        void test01PublicInsert(void) {
            SplayTreeInt test01Size8(INT_MAX);
            for (int i = 1; i < 9; ++i) {
                 TS_ASSERT( test01Size8.insert(i) );
                 TS_ASSERT_EQUALS( test01Size8.top()->key(), i );
            }
            for (int i = 1; i < 9; ++i) {
                 TS_ASSERT( not test01Size8.insert(i) );
                 TS_ASSERT_EQUALS( test01Size8.top()->key(), i );
            }
            // std::cout << std::endl << "test 01 Done" << std::endl;
        }
        void test02PublicInsert(void) {
            SplayTreeInt test02Size5(INT_MAX);
            for (int i = 0; i < SIZE5; ++i) {
                // std::cerr << "test02Size5.insert(" << i << ")" << std::endl;
                test02Size5.insert(i);
                // DEBUG_OUT(std::cerr, test02Size5)
                TS_ASSERT_EQUALS( test02Size5.top()->key(), i );
            }
            for (int i = 0; i < SIZE5; ++i) {
                TS_ASSERT( not test02Size5.insert(i) );
                // DEBUG_OUT(std::cout, test02Size5)
                TS_ASSERT_EQUALS( test02Size5.top()->key(), i );
            } 
            SplayTreeInt test02Size10;
            for (int i = 0; i < SIZE10; ++i) {
                test02Size10.insert(i);
            }
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( not test02Size10.insert(i) );
                TS_ASSERT_EQUALS( test02Size10.top()->key(), i );
            }
            SplayTreeInt test02Size15;
            for (int i = 0; i < SIZE15; ++i) {
                test02Size15.insert(i);
            }
            for (int i = 0; i < SIZE15; ++i) {
                // std::cout << "test02Size5.insert(" << i << ")" << std::endl;
                TS_ASSERT( not test02Size15.insert(i) );
                TS_ASSERT_EQUALS( test02Size15.top()->key(), i );
            }
            // std::cout << std::endl << "test 02 Done" << std::endl;
        }
        void test03PublicTreeOutInOrder(void) {
            SplayTreeInt test03Tree0;
            std::ostringstream oss0;
            test03Tree0.treeOutInOrder(oss0);
            TS_ASSERT_EQUALS( oss0.str(), "" );

            SplayTreeInt test03Tree7;
            fillTree7(test03Tree7);
            std::ostringstream oss7;
            test03Tree7.treeOutInOrder(oss7);
            TS_ASSERT_EQUALS( oss7.str(), "0 1 2 3 4 5 6" );


            SplayTreeInt test03Tree70;
            fillTree70(test03Tree70);
            std::ostringstream oss70;
            test03Tree70.treeOutInOrder(oss70);
            TS_ASSERT_EQUALS( oss70.str(), "15 20 30 40 50 60 70" );

            SplayTreeInt test03TreeRandom1Kib;
            randomTreeSize1KibSort(test03TreeRandom1Kib);

            std::ostringstream treeRandom1Kib;
            test03TreeRandom1Kib.treeOutInOrder(treeRandom1Kib);
            std::ostringstream arrayRandom1Kib;

            randomIntsOut(arrayRandom1Kib);
            TS_ASSERT_EQUALS( arrayRandom1Kib.str(), treeRandom1Kib.str() );
            // std::cout << std::endl << "test 03 Done" << std::endl;
        }
        void test04PublicFindSplay(void) {
            SplayTreeInt test04Tree0;
            TS_ASSERT( not test04Tree0.find(INT_MIN) );

            SplayTreeInt test04Tree7;
            fillTree7(test04Tree7);
            for (int i = -1; i < 15; ++i) {
                if (-1 < i && i < 7) {
                    TS_ASSERT( test04Tree7.findSplay(i) );
                    TS_ASSERT_EQUALS( test04Tree7.top()->key(), i );
                } else
                    TS_ASSERT( not test04Tree7.findSplay(i) );
            }

            SplayTreeInt test04Tree9;
            fillTree9(test04Tree9);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance9, sequance9end, i);
                if (sequance9end != p) {
                    TS_ASSERT( test04Tree9.findSplay(i) );
                    TS_ASSERT_EQUALS( test04Tree9.top()->key(), i );
                } else
                    TS_ASSERT( not test04Tree9.find(i) );
            }

            SplayTreeInt test04Tree70;
            fillTree70(test04Tree70);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance70, sequance70end, i);
                if (sequance70end != p) {
                    TS_ASSERT( test04Tree70.findSplay(i) );
                    TS_ASSERT_EQUALS( test04Tree70.top()->key(), i );
                } else
                    TS_ASSERT( not test04Tree70.findSplay(i) );
            }

            SplayTreeInt test04Size1024;
            fillTreeSize1024(test04Size1024);
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( test04Size1024.findSplay(i) );
            }
#ifdef BIG
            SplayTreeInt test04Size1Mib;
            for (size_t i = 0; i < SIZE20; ++i) {
                test04Size1Mib.insert(i);
                TS_ASSERT( not test04Size1Mib.insert(i) );
            }
            for (int i = 0; i < SIZE20; ++i) {
               TS_ASSERT( test04Size1Mib.findSplay(i) );
               TS_ASSERT_EQUALS( test04Size1Mib.top()->key(), i );
            }
#endif
            // std::cout << std::endl << "test 04 Done" << std::endl;
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=91:ts=4:sw=4:sts=4:et
 * EOF */
