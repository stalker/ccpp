////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#define BIG 1


#include "../avltree.hpp"
#include "../bstree.hpp"
#include "../ifacebst.hpp"
#include "../basictree.hpp"
#include "../node.hpp"

#include <cfloat>  // DBL_MAX
#include <climits> // INT_MIN INT_MAX
#include <cstddef>
#include <cstdint>
#include <cstdlib> // std::rand
#include <ctime>   // std::time
#include <unistd.h>

#include <algorithm>
#include <sstream>
#include <vector>

#define DEBUG 1
#ifdef DEBUG
# include "mydebug.hpp"
#endif

#define SIZE5  32
#define SIZE10 16
// #define SIZE10 1024
#define SIZE15 32768
#define SIZE20 1048576
#define SIZE21 2097152
#define SIZE22 4194304
#define SIZE23 8388608
#define SIZE24 16777216

typedef svn::Node<int>              NodeInt;
typedef svn::AVLTree<int, NodeInt>  AVLTreeInt;

class AVLTreeTest : public CxxTest::TestSuite, virtual public AVLTreeInt {
    public:
        const int sequance9[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
        const int * sequance9end = sequance9 + 9;
        const int sequance70[7] = {40, 20, 30, 15, 60, 50, 70};
        const int * sequance70end = sequance70 + 7;
        std::vector<int> randomInts;

        void debugOUT(std::ostream & os, AVLTreeInt & t) {
            t.detailOut = true;
            os << std::endl << "root: " << t.top();
            os << std::endl << t << std::endl;
            t.detailOut = false;
        }
#define DEBUG_OUT(o, t) do{o<<#t;debugOUT(o,t);}while(0);

        void fillTree7(svn::IfaceBSTree<int> & t) {
            const int sequance7[] = {3, 1, 2, 0, 5, 4, 6};
            for (size_t i = 0; i < sizeof(sequance7) / sizeof(int); ++i) {
                // std::cerr << "insert: " << sequance7[i] << " to ";
                TS_ASSERT( t.insert(sequance7[i]) );
                // DEBUG_OUT(std::cerr, t);
                TS_ASSERT( not t.insert(sequance7[i]) );
            }
        }
        void fillTree9(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance9) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance9[i]) );
                TS_ASSERT( not t.insert(sequance9[i]) );
            }
        }
        void fillTree70(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance70) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance70[i]) );
                TS_ASSERT( not t.insert(sequance70[i]) );
            }
        }
        void fillTreeSize1024(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE10; ++i) {
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void fillTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE20; ++i) {
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void randomTreeSize1Kib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE10);
            for (size_t i = 0; i < SIZE10; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (10*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1KibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Kib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }

        void treeSize1Mib(svn::IfaceBSTree<int> & t) {
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < randomInts.size(); ++i) {
                randomInts[i] = static_cast<int>(i);
                TS_ASSERT( t.insert(randomInts[i]) );
            }
        }

        void randomTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < SIZE20; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (99*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1MibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Mib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }
        void randomIntsOut(std::ostream & os) {
            size_t i = 0; for (; i < randomInts.size() - 1; ++i)
               os << randomInts[i] << " ";
            os << randomInts[i];
        }

        void test00AVLTreeInt(void) {
#if defined(DEBUG) && defined(PRINTM)
            std::cerr << std::endl;
            printd("ok: this:", this) << std::endl;
#endif
            TS_ASSERT_EQUALS( top(), nullptr );
            TS_ASSERT_EQUALS( detailOut, false );
        }
        void test01Protectedadd(void) {
            bool found = false;
            root = add(root, found, 3);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->key(), 3 );
            root = add(root, found, 3);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 1);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->key(), 1 );
            root = add(root, found, 1);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 0);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->key(), 0 );
            TS_ASSERT_EQUALS( top()->key(), 1 );
            TS_ASSERT_EQUALS( top()->right()->key(), 3 );
            root = add(root, found, 0);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 2);
            TS_ASSERT( not found );
            root = add(root, found, 2);
            TS_ASSERT( found );

            delete root;
            root = nullptr;
        }
        void test02PublicInsert(void) {
            AVLTreeInt test02Size5;
            for (int i = 0; i < SIZE5; ++i) {
                test02Size5.insert(i);
            }
            for (int i = 0; i < SIZE5; ++i) {
                TS_ASSERT( not test02Size5.insert(i) );
            } 
            AVLTreeInt test02Size10;
            for (int i = 0; i < SIZE10; ++i) {
                test02Size10.insert(i);
            }
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( not test02Size10.insert(i) );
            }
            AVLTreeInt test02Size15;
            for (int i = 0; i < SIZE15; ++i) {
                test02Size15.insert(i);
            }
            for (int i = 0; i < SIZE15; ++i) {
                TS_ASSERT( not test02Size15.insert(i) );
            }
        }
        void test03PublicTreeOutInOrder(void) {
            AVLTreeInt test03Tree0;
            std::ostringstream oss0;
            test03Tree0.treeOutInOrder(oss0);
            TS_ASSERT_EQUALS( oss0.str(), "" );

            AVLTreeInt test03Tree7;
            fillTree7(test03Tree7);

            // DEBUG_OUT(std::cerr, test03Tree7);

            std::ostringstream oss7;
            test03Tree7.treeOutInOrder(oss7);
            TS_ASSERT_EQUALS( oss7.str(), "0 1 2 3 4 5 6" );

            AVLTreeInt test03Tree9;
            fillTree9(test03Tree9);
            std::ostringstream oss9;
            test03Tree9.treeOutInOrder(oss9);
            TS_ASSERT_EQUALS( oss9.str(), "1 3 4 6 7 8 10 13 14" );

            AVLTreeInt test03Tree70;
            fillTree70(test03Tree70);
            std::ostringstream oss70;
            test03Tree70.treeOutInOrder(oss70);
            TS_ASSERT_EQUALS( oss70.str(), "15 20 30 40 50 60 70" );

            AVLTreeInt test03TreeRandom1Kib;
            randomTreeSize1KibSort(test03TreeRandom1Kib);
            std::ostringstream treeRandom1Kib;
            test03TreeRandom1Kib.treeOutInOrder(treeRandom1Kib);
            std::ostringstream arrayRandom1Kib;

            randomIntsOut(arrayRandom1Kib);
            TS_ASSERT_EQUALS( arrayRandom1Kib.str(), treeRandom1Kib.str() );
        }
        void test04PublicFind(void) {
            AVLTreeInt test04Tree0;
            TS_ASSERT( not test04Tree0.find(INT_MIN) );

            AVLTreeInt test04Tree7;
            fillTree7(test04Tree7);
            for (int i = -1; i < 15; ++i) {
                if (-1 < i && i < 7) {
                    TS_ASSERT( test04Tree7.find(i) );
                } else
                    TS_ASSERT( not test04Tree7.find(i) );
            }

            AVLTreeInt test04Tree9;
            fillTree9(test04Tree9);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance9, sequance9end, i);
                if (sequance9end != p) {
                    TS_ASSERT( test04Tree9.find(i) );
                } else
                    TS_ASSERT( not test04Tree9.find(i) );
            }

            AVLTreeInt test04Tree70;
            fillTree70(test04Tree70);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance70, sequance70end, i);
                if (sequance70end != p) {
                    TS_ASSERT( test04Tree70.find(i) );
                } else
                    TS_ASSERT( not test04Tree70.find(i) );
            }

            AVLTreeInt test04Size1024;
            fillTreeSize1024(test04Size1024);
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( test04Size1024.find(i) );
            }
#ifdef BIG
            AVLTreeInt test04Size1Mib;
            fillTreeSize1Mib(test04Size1Mib);
            for (int i = 0; i < SIZE20; ++i) {
                TS_ASSERT( test04Size1Mib.find(i) );
            }
#endif
        }
        void test05PublicFindMin(void) {
            AVLTreeInt test05Tree0;
            try {
                test05Tree0.findMin();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            AVLTreeInt test05Tree9;
            fillTree9(test05Tree9);
            TS_ASSERT_EQUALS( test05Tree9.findMin(), 1);

            AVLTreeInt test05Tree1;
            test05Tree1.insert(1);
            TS_ASSERT_EQUALS( test05Tree1.findMin(), 1);

            AVLTreeInt test05Tree2;
            fillTree9(test05Tree2);
            test05Tree2.insert(2);
            TS_ASSERT_EQUALS( test05Tree2.findMin(), 1);

            AVLTreeInt test05Tree7IntMin;
            fillTree7(test05Tree7IntMin);
            test05Tree7IntMin.insert(INT_MIN);
            TS_ASSERT_EQUALS( test05Tree7IntMin.findMin(), INT_MIN);

            AVLTreeInt test05Tree9IntMin;
            fillTree9(test05Tree9IntMin);
            test05Tree9IntMin.insert(INT_MIN);
            TS_ASSERT_EQUALS( test05Tree9IntMin.findMin(), INT_MIN);

            AVLTreeInt test05TreeRandom1Kib;
            randomTreeSize1Kib(test05TreeRandom1Kib);
            int min = *std::min_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test05TreeRandom1Kib.findMin(), min );

#ifdef BIG
            AVLTreeInt test05TreeRandom1Mib;
            randomTreeSize1Mib(test05TreeRandom1Mib);
            min = *std::min_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test05TreeRandom1Mib.findMin(), min );
#endif
        }
        void test06PublicFindMax(void) {
            AVLTreeInt test06Tree0;
            try {
                test06Tree0.findMax();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            AVLTreeInt test06Tree9;
            fillTree9(test06Tree9);
            TS_ASSERT_EQUALS( test06Tree9.findMax(), 14);

            AVLTreeInt test06Tree1;
            test06Tree1.insert(1);
            TS_ASSERT_EQUALS( test06Tree1.findMax(), 1);

            AVLTreeInt test06Tree2;
            fillTree9(test06Tree2);
            test06Tree2.insert(15);
            TS_ASSERT_EQUALS( test06Tree2.findMax(), 15);

            AVLTreeInt test06Tree7IntMax;
            fillTree7(test06Tree7IntMax);
            test06Tree7IntMax.insert(INT_MAX);
            TS_ASSERT_EQUALS( test06Tree7IntMax.findMax(), INT_MAX);

            AVLTreeInt test06Tree9IntMax;
            fillTree9(test06Tree9IntMax);
            test06Tree9IntMax.insert(INT_MAX);
            TS_ASSERT_EQUALS( test06Tree9IntMax.findMax(), INT_MAX);

            AVLTreeInt test06TreeRandom1Kib;
            randomTreeSize1Kib(test06TreeRandom1Kib);
            int max = *std::max_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test06TreeRandom1Kib.findMax(), max );
#ifdef BIG
            AVLTreeInt test06TreeRandom1Mib;
            randomTreeSize1MibSort(test06TreeRandom1Mib);
            max = *std::max_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test06TreeRandom1Mib.findMax(), max );
#endif
        }
        void test07PublicNext(void) {
            int zero = 0;
            int intMax = INT_MAX;
            AVLTreeInt test07Tree0;
            const int & foundZero = test07Tree0.next(zero);
            TS_ASSERT_EQUALS( &zero , &foundZero);
            const int & foundIntMax = test07Tree0.next(intMax);
            TS_ASSERT_EQUALS( &intMax , &foundIntMax);

            AVLTreeInt test07TreeRandom1Kib;
            randomTreeSize1KibSort(test07TreeRandom1Kib);
            for (size_t i = 0, j = 1; i < randomInts.size() - 1; ++i, ++j) {
                TS_ASSERT_EQUALS( 
                    test07TreeRandom1Kib.next(randomInts[i]), randomInts[j] 
                );
            }
        }
        void test08PublicPrevious(void) {
            int zero = 0;
            int intMin = INT_MIN;
            AVLTreeInt test07Tree0;
            const int & foundZero = test07Tree0.previous(zero);
            TS_ASSERT_EQUALS( &zero , &foundZero);
            const int & foundIntMin = test07Tree0.previous(intMin);
            TS_ASSERT_EQUALS( &intMin , &foundIntMin);

            AVLTreeInt test08TreeRandom1Kib;
            randomTreeSize1KibSort(test08TreeRandom1Kib);
            for (int i = 1, j = 0; i < SIZE10; ++i, ++j) {
                TS_ASSERT_EQUALS(
                    test08TreeRandom1Kib.previous(randomInts[i]), randomInts[j]
                );
            }
        }
        void test09PublicSearchNode(void) {
            AVLTreeInt test09Tree0;
            TS_ASSERT_EQUALS( test09Tree0.searchNode(INT_MIN), nullptr );
        }
        void test10PublicExtractNode(void) {
            AVLTreeInt test10Tree0;
            TS_ASSERT_EQUALS( test10Tree0.extractNode(INT_MAX), nullptr );
            TS_ASSERT_EQUALS( test10Tree0.top(), nullptr );

            AVLTreeInt test10Tree9IntMax;
            fillTree9(test10Tree9IntMax);

            AVLTreeInt::node_t * n14 = test10Tree9IntMax.extractNode(14);
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;

            AVLTreeInt::node_t * n13 = test10Tree9IntMax.extractNode(13);
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            AVLTreeInt::node_t * n3 = test10Tree9IntMax.extractNode(3);
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            AVLTreeInt::node_t * n6 = test10Tree9IntMax.extractNode(6);
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            AVLTreeInt::node_t * n7 = test10Tree9IntMax.extractNode(7);
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            AVLTreeInt::node_t * n8 = test10Tree9IntMax.extractNode(8);
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            AVLTreeInt::node_t * n4 = test10Tree9IntMax.extractNode(4);
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            AVLTreeInt::node_t * n10 = test10Tree9IntMax.extractNode(10);
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            AVLTreeInt::node_t * n1 = test10Tree9IntMax.extractNode(1);
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);
            test10Tree9IntMax.detailOut = true;
            TS_ASSERT( isNull(test10Tree9IntMax.top()) );
            delete n1;
        }
        void test11PublicExtractMinimum(void) {
            AVLTreeInt test11Tree0;
            TS_ASSERT_EQUALS( test11Tree0.extractMinimum(), nullptr );
            TS_ASSERT_EQUALS( test11Tree0.top(), nullptr );

            AVLTreeInt test11Tree9IntMax;
            fillTree9(test11Tree9IntMax);

            AVLTreeInt::node_t * n1 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);
            delete n1;

            AVLTreeInt::node_t * n3 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            AVLTreeInt::node_t * n4 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            AVLTreeInt::node_t * n6 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            AVLTreeInt::node_t * n7 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            AVLTreeInt::node_t * n8 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            AVLTreeInt::node_t * n10 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            AVLTreeInt::node_t * n13 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            AVLTreeInt::node_t * n14 = test11Tree9IntMax.extractMinimum();
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;
            TS_ASSERT( isNull(test11Tree9IntMax.top()) );
        }
        void test12PublicExtractMaximum(void) {
            AVLTreeInt test12Tree0;
            TS_ASSERT_EQUALS( test12Tree0.extractMaximum(), nullptr );
            TS_ASSERT_EQUALS( test12Tree0.top(), nullptr );

            AVLTreeInt test12Tree9IntMax;
            fillTree9(test12Tree9IntMax);

            AVLTreeInt::node_t * n14 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;

            AVLTreeInt::node_t * n13 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            AVLTreeInt::node_t * n10 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            AVLTreeInt::node_t * n8 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            AVLTreeInt::node_t * n7 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            AVLTreeInt::node_t * n6 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            AVLTreeInt::node_t * n4 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            AVLTreeInt::node_t * n3 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            AVLTreeInt::node_t * n1 = test12Tree9IntMax.extractMaximum();
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);
            delete n1;
            TS_ASSERT( isNull(test12Tree9IntMax.top()) );
        }
        void test13PublicRemove(void) {
            AVLTreeInt test13Tree0;
            TS_ASSERT( not test13Tree0.remove(0));
            TS_ASSERT( not test13Tree0.remove(INT_MIN));
            TS_ASSERT( not test13Tree0.remove(INT_MAX));

            AVLTreeInt test13TreeRandom1Kib;
            randomTreeSize1Kib(test13TreeRandom1Kib);
            while (not randomInts.empty()) {
                int e = randomInts.back();
                randomInts.pop_back();
                TS_ASSERT( test13TreeRandom1Kib.remove(e) );
            }
        }
        void test14PublicCoincides(void) {
            AVLTreeInt test13Tree01;
            AVLTreeInt test13Tree02;
            TS_ASSERT( test13Tree01.coincides(test13Tree02) );

            fillTree7(test13Tree01);
            TS_ASSERT( not test13Tree01.coincides(test13Tree02) );

            fillTree7(test13Tree02);
            TS_ASSERT( test13Tree01.coincides(test13Tree02) );

            AVLTreeInt test13Tree91;
            AVLTreeInt test13Tree92;

            fillTree9(test13Tree91);
            TS_ASSERT( not test13Tree91.coincides(test13Tree92) );

            fillTree9(test13Tree92);
            TS_ASSERT( test13Tree91.coincides(test13Tree92) );
        }
        void test15PublicEquals(void) {
            AVLTreeInt test15Tree01;
            AVLTreeInt test15Tree02;
            TS_ASSERT( test15Tree01.equals(test15Tree02) );

            AVLTreeInt test15TreeRandom1Kib01;
            randomTreeSize1Kib(test15TreeRandom1Kib01);
            std::vector<int> randomInts2(randomInts);

            AVLTreeInt test15TreeRandom1Kib02;
            std::srand(std::time(NULL)-getpid());
            while (not randomInts2.empty()) {
                int rnd = static_cast<int>(std::rand()) % randomInts2.size();
                auto it = randomInts2.begin() + rnd;
                test15TreeRandom1Kib02.insert(*it);
                randomInts2.erase(it);
            }
            // DEBUG_OUT(std::cerr, test15TreeRandom1Kib01);
            // DEBUG_OUT(std::cerr, test15TreeRandom1Kib02);
            TS_ASSERT( test15TreeRandom1Kib01.equals(test15TreeRandom1Kib02) );
            TS_ASSERT( test15TreeRandom1Kib01 == test15TreeRandom1Kib02 );
            // std::cerr << std::boolalpha << std::endl
            //           << (test15TreeRandom1Kib01 == test15TreeRandom1Kib02)
            //           << std::endl;
        }
        void test16PublicIsBinarySearchTree(void) {
            AVLTreeInt test16Tree0;
            TS_ASSERT( test16Tree0.isBinarySearchTree() );

            AVLTreeInt test16Tree7;
            fillTree7(test16Tree7);
            TS_ASSERT( test16Tree7.isBinarySearchTree() );

            AVLTreeInt test16Tree9;
            fillTree9(test16Tree9);
            TS_ASSERT( test16Tree9.isBinarySearchTree() );
            
            AVLTreeInt test16TreeRandom1Kib;
            randomTreeSize1Kib(test16TreeRandom1Kib);
            TS_ASSERT( test16TreeRandom1Kib.isBinarySearchTree() );
        }
        void test17PublicAVLCheck(void) {
            AVLTreeInt test17Tree0;
            TS_ASSERT( test17Tree0.avlCheck() );

            AVLTreeInt test17Tree7;
            fillTree7(test17Tree7);
            TS_ASSERT( test17Tree7.avlCheck() );

            AVLTreeInt test17Tree9;
            fillTree9(test17Tree9);
            TS_ASSERT( test17Tree9.avlCheck() );
            
            AVLTreeInt test17TreeRandom1Kib;
            randomTreeSize1Kib(test17TreeRandom1Kib);
            TS_ASSERT( test17TreeRandom1Kib.avlCheck() );

#ifdef BIG
            AVLTreeInt test17TreeRandom1Mib;
            randomTreeSize1MibSort(test17TreeRandom1Mib);
            TS_ASSERT( test17TreeRandom1Mib.avlCheck() );
#endif
        }
        void test18PublicAt(void) {
            AVLTreeInt test18Tree0;

            try {
                int zero = test18Tree0[0];
                TS_ASSERT( false );
                TS_ASSERT_EQUALS( zero, 0);
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            AVLTreeInt test18Tree9;
            fillTree9(test18Tree9);
            std::vector<int> test18Vector9(sequance9, sequance9end);
            std::sort(test18Vector9.begin(), test18Vector9.end());
            for (size_t i = 0; i < 9; ++i) {
                TS_ASSERT_EQUALS( test18Tree9[i], test18Vector9[i] );
            }
            TS_ASSERT( test18Tree9[0] != test18Vector9[1] );

            AVLTreeInt test18TreeRandom1Kib;
            randomTreeSize1KibSort(test18TreeRandom1Kib);
            for (size_t i = 0; i < randomInts.size(); ++i) {
                TS_ASSERT_EQUALS( test18TreeRandom1Kib[i], randomInts[i] );
            }

#ifdef BIG
            AVLTreeInt test18Tree1Mib;
            treeSize1Mib(test18Tree1Mib);
            for (size_t i = 0; i < randomInts.size(); ++i) {
                TS_ASSERT_EQUALS( test18Tree1Mib[i], randomInts[i] );
            }
#endif
        }
        void test19ProtectedKeyByIndex(void) {
            treeSize1Mib(*this);
            size_t middle = this->size() / 2;
            this->keyByIndex(middle) = 0;
            TS_ASSERT_EQUALS( this->at(middle), 0 );
            TS_ASSERT( not this->isBinarySearchTree() );
            delete root;
            root = nullptr;
        }
        void test20PublicAVLToMerge(void) {
            AVLTreeInt test20Tree01;
            AVLTreeInt test20Tree02;

            test20Tree01.avlToMerge(test20Tree02);
            TS_ASSERT_EQUALS( test20Tree01.top(), nullptr );
            TS_ASSERT_EQUALS( test20Tree02.top(), nullptr );

            AVLTreeInt test20Tree03;
            test20Tree01.insert(1);
            test20Tree03.insert(1);
            test20Tree02.insert(2);
            test20Tree03.insert(2);
            test20Tree02.insert(3);
            test20Tree03.insert(3);
            test20Tree01.avlToMerge(test20Tree02);
            TS_ASSERT_EQUALS( test20Tree02.top(), nullptr );
            TS_ASSERT_EQUALS( test20Tree01, test20Tree03 );
            TS_ASSERT( test20Tree01.avlCheck() );
            AVLTreeInt test20Tree9;
            fillTree9(test20Tree9);
            AVLTreeInt test20Tree70;
            fillTree70(test20Tree70);
            test20Tree9.avlToMerge(test20Tree70);
            TS_ASSERT_EQUALS( test20Tree70.top(), nullptr );
            TS_ASSERT( test20Tree9.avlCheck() );

            AVLTreeInt test20Tree9Plus70;
            for (size_t i = 0; i < 9; ++i) {
                test20Tree9Plus70.insert(sequance9[i]);
            }
            for (size_t i = 0; i < 7; ++i) {
                test20Tree9Plus70.insert(sequance70[i]);
            }
            TS_ASSERT_EQUALS( test20Tree9, test20Tree9Plus70 );
        }
        void test21PublicAVLSplitTo(void) {
            AVLTreeInt test21Tree00;
            AVLTreeInt test21Tree01;
            AVLTreeInt test21Tree02;

            test21Tree00.avlSplitTo(test21Tree01, test21Tree02, 0);
            TS_ASSERT_EQUALS( test21Tree00.top(), nullptr );
            TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );
            TS_ASSERT_EQUALS( test21Tree02.top(), nullptr );

            test21Tree00.insert(1);
            test21Tree00.avlSplitTo(test21Tree01, test21Tree02, 1);
            TS_ASSERT_EQUALS( test21Tree00.top(), nullptr );
            TS_ASSERT_EQUALS( test21Tree01[0], 1 );
            TS_ASSERT_EQUALS( test21Tree02.top(), nullptr );
            test21Tree00.avlToMerge(test21Tree01);
            TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );

            test21Tree00.avlSplitTo(test21Tree01, test21Tree02, 0);
            TS_ASSERT_EQUALS( test21Tree00.top(), nullptr );
            TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );
            TS_ASSERT_EQUALS( test21Tree02[0], 1 );
            test21Tree00.avlToMerge(test21Tree02);
            TS_ASSERT_EQUALS( test21Tree02.top(), nullptr ); 

            AVLTreeInt test21Tree9;
            fillTree9(test21Tree9);
            // DEBUG_OUT(std::cerr, test21Tree9 );
            test21Tree9.avlSplitTo(test21Tree01, test21Tree02, 8);
            TS_ASSERT_EQUALS( test21Tree9.top(), nullptr );
            TS_ASSERT( test21Tree01.top() != nullptr );
            TS_ASSERT( test21Tree02.top() != nullptr );
            TS_ASSERT( test21Tree01.avlCheck() );
            TS_ASSERT( test21Tree02.avlCheck() );

            std::ostringstream oss91;
            test21Tree01.treeOutInOrder(oss91);
            TS_ASSERT_EQUALS( oss91.str(), "1 3 4 6 7 8" );
            std::ostringstream oss92;
            test21Tree02.treeOutInOrder(oss92);
            TS_ASSERT_EQUALS( oss92.str(), "10 13 14" );


            test21Tree9.avlToMerge(test21Tree01);
            TS_ASSERT( (test21Tree9.top() != nullptr) );
            TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );
            test21Tree9.avlToMerge(test21Tree02);
            TS_ASSERT_EQUALS( test21Tree02.top(), nullptr );
            TS_ASSERT( test21Tree9.top() != nullptr );

            std::ostringstream oss9;
            test21Tree9.treeOutInOrder(oss9);
            TS_ASSERT_EQUALS( oss9.str(), "1 3 4 6 7 8 10 13 14" );
            TS_ASSERT( test21Tree01.avlCheck() );

            // DEBUG_OUT(std::cerr, test21Tree01 );
            // DEBUG_OUT(std::cerr, test21Tree02 );
            //
            AVLTreeInt test21TreeRandom1Kib;
            randomTreeSize1KibSort(test21TreeRandom1Kib);
            for (size_t i = 0; i < randomInts.size() - 1; ++i) {
                // §DEBUG_OUT(std::cerr, test21TreeRandom1Kib );
                test21TreeRandom1Kib.avlSplitTo(test21Tree01, test21Tree02, randomInts[i]);
                TS_ASSERT_EQUALS( test21TreeRandom1Kib.top(), nullptr );
                TS_ASSERT( (test21Tree01.top() != nullptr) );
                TS_ASSERT( (test21Tree02.top() != nullptr) );

                // DEBUG_OUT(std::cerr, test21Tree01 );
                // DEBUG_OUT(std::cerr, test21Tree02 );
                test21TreeRandom1Kib.avlToMerge(test21Tree01);
                TS_ASSERT( (test21TreeRandom1Kib.top() != nullptr) );
                test21TreeRandom1Kib.avlToMerge(test21Tree02);
                TS_ASSERT( (test21TreeRandom1Kib.top() != nullptr) );
                TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );
                TS_ASSERT_EQUALS( test21Tree01.top(), nullptr );

            }
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=91:ts=1:sw=1:sts=1:et
 * EOF */
