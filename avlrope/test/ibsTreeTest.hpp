////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#define BIG 1

#include "../ibstree.hpp"
// #include "../bstree.hpp"
#include "../ifacebst.hpp"
#include "../basictree.hpp"
#include "../node.hpp"

#include <cfloat>  // DBL_MAX
#include <climits> // INT_MIN INT_MAX
#include <cstddef>
#include <cstdint>
#include <cstdlib> // std::rand
#include <ctime>   // std::time
#include <unistd.h>

#include <algorithm>
#include <sstream>
#include <vector>

#undef  DEBUG
#define DEBUG 2
#ifdef  DEBUG
# include "mydebug.hpp"
#endif

#define SIZE5  32
#define SIZE10 40
// #define SIZE10 1024
#define SIZE15 32768
// #define SIZE20 131072+51200
// #define SIZE20 262144+51200
// #define SIZE20 313342
// #define SIZE20 262031
#define SIZE19 524288
#define SIZE20 1048576
#define SIZE21 2097152
#define SIZE22 4194304
#define SIZE23 8388608
#define SIZE24 16777216

typedef svn::StoreNode<int>          NodeInt;
typedef svn::IBSTree<int, NodeInt> IBSTreeInt;

// TODO: Don't user Node<T>!!!
class IbsTreeTest : public CxxTest::TestSuite , public IBSTreeInt {
    public:
        const int sequance9[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
        const int * sequance9end = sequance9 + 9;
        const int sequance70[7] = {40, 20, 30, 15, 60, 50, 70};
        const int * sequance70end = sequance70 + 7;
        std::vector<int> randomInts;

        void debugOUT(std::ostream & os, IBSTreeInt & t) {
            t.detailOut = true;
            os << std::endl << "root: " << t.top();
            os << std::endl << t << std::endl;
            t.detailOut = false;
        }
#define DEBUG_OUT(o, t) do{o<<#t;debugOUT(o,t);}while(0);

        void fillTree7(svn::IfaceBSTree<int> & t) {
            const int sequance7[] = {3, 1, 2, 0, 5, 4, 6};
            for (size_t i = 0; i < sizeof(sequance7) / sizeof(int); ++i) {
                // std::cerr << "insert: " << sequance7[i] << " to ";
                TS_ASSERT( t.insert(sequance7[i]) );
                // DEBUG_OUT(std::cerr, t);
                TS_ASSERT( not t.insert(sequance7[i]) );
            }
        }
        void fillTree9(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance9) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance9[i]) );
                TS_ASSERT( not t.insert(sequance9[i]) );
            }
        }
        /**
         *                              16  
         *               +---------------+---------------+
         *               8                              24
         *       +-------+-------+               +-------+-------+
         *       4              12              20              28
         *   +---+---+       +---+---+       +---+---+       +---+---+
         *   2       6      10      14      18      22      26      30
         * +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+
         * 1   3   5   7   9  11  13  15  17  19  21  23  25  27  29  31
         *
         *               8
         *       +-------+-------+
         *       4              10
         *   +---+---+       +---+---+
         *   2       6       9      11
         * +-+-+   +-+-+     
         * 1   3   5   7      
         *
         *                 16  
         *  +---------------+---------------+
         * 12                              24
         *  +-------+               +-------+-------+
         *         14              20              28
         *      +---+---+       +---+---+       +---+---+
         *     13      15      18      22      26      30
         *                    +-+-+   +-+-+   +-+-+   +-+-+
         *                   17  19  21  23  25  27  29  31
         */
        void fillTree31(svn::IfaceBSTree<int> & t) {
            t.insert(16);
            for (int step = 16; step > 1; step /= 2) {
                for (int value = step/2; value < 32; value += step) {
                    TS_ASSERT( t.insert(value) );
                    TS_ASSERT( not t.insert(value) );
                }
            }
        }
        void fillTree313(svn::IfaceBSTree<int> & t) {
            t.insert(16);
            for (int step = 16; step > 1; step /= 2) {
                for (int value = step/2; value < 32; value += step) {
                    TS_ASSERT( t.insert(3*value) );
                    TS_ASSERT( not t.insert(3*value) );
                }
            }
        }
        void fillTree70(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < sizeof(sequance70) / sizeof(int); ++i) {
                TS_ASSERT( t.insert(sequance70[i]) );
                TS_ASSERT( not t.insert(sequance70[i]) );
            }
        }
        void fillTreeSize1024(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE10; ++i) {
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void fillTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            for (size_t i = 0; i < SIZE20; ++i) {
                std::cerr << "insert: " << i << std::endl;
                TS_ASSERT( t.insert(i) );
                TS_ASSERT( not t.insert(i) );
            }
        }
        void randomTreeSize1Kib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE10);
            for (size_t i = 0; i < SIZE10; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (10*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1KibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Kib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }

        void treeSize1Mib(svn::IfaceBSTree<int> & t) {
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < randomInts.size(); ++i) {
                randomInts[i] = static_cast<int>(i);
                TS_ASSERT( t.insert(randomInts[i]) );
            }
        }

        void randomTreeSize1Mib(svn::IfaceBSTree<int> & t) {
            std::srand(std::time(NULL));
            randomInts.clear();
            randomInts.resize(SIZE20);
            for (size_t i = 0; i < SIZE20; /* None */ ) {
                int rnd = static_cast<int>(std::rand()) % (99*randomInts.size());
                if (t.insert(rnd)) {
                    randomInts[i] = rnd;
                    TS_ASSERT( not t.insert(rnd) );
                    i++;
                }
            }
        }
        void randomTreeSize1MibSort(svn::IfaceBSTree<int> & t) {
            randomTreeSize1Mib(t);
            std::sort(randomInts.begin(), randomInts.end());
        }
        void randomIntsOut(std::ostream & os) {
            size_t i = 0; for (; i < randomInts.size() - 1; ++i)
               os << randomInts[i] << " ";
            os << randomInts[i];
        }

        void tezd00IBSTreeInt(void) {
#if defined(DEBUG) && defined(PRINTM)
            std::cerr << std::endl;
            printd("ok: this:", this) << std::endl;
#endif  
        }
        void tezd01PublicInsert(void) {
            IBSTreeInt test01Size8;
            for (int i = 1; i < 9; ++i) {
                 TS_ASSERT( test01Size8.insert(i) );
            }
            for (int i = 1; i < 9; ++i) {
                 TS_ASSERT( not test01Size8.insert(i) );
            }
        }
        void tezd00ProtectedAdd(void) {
            bool found = false;
            root = add(root, found, 3);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->key(), 3 );
            root = add(root, found, 3);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 1);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->key(), 1 );
            root = add(root, found, 1);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 0);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->left()->key(), 0 );
            root = add(root, found, 0);
            TS_ASSERT( found );            

            found = false;
            root = add(root, found, 2);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->left()->right()->key(), 2 );
            root = add(root, found, 2);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 5);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->key(), 5 );
            root = add(root, found, 5);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 4);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->left()->key(), 4 );
            root = add(root, found, 4);
            TS_ASSERT( found );

            found = false;
            root = add(root, found, 6);
            TS_ASSERT( not found );
            TS_ASSERT_EQUALS( top()->right()->right()->key(), 6 );
            root = add(root, found, 6);
            TS_ASSERT( found );

            delete root;
            root = nullptr;

            fillTree7(*this);
            TS_ASSERT_EQUALS( top()->key(), 3 );
            TS_ASSERT_EQUALS( top()->left()->key(), 1 );
            TS_ASSERT_EQUALS( top()->left()->left()->key(), 0 );
            TS_ASSERT_EQUALS( top()->left()->right()->key(), 2 );
            TS_ASSERT_EQUALS( top()->right()->key(), 5 );
            TS_ASSERT_EQUALS( top()->right()->left()->key(), 4 );
            TS_ASSERT_EQUALS( top()->right()->right()->key(), 6 );

            delete root;
            root = nullptr;
        }
        void tezd02PublicInsert(void) {
            IBSTreeInt test02Size5;
            for (int i = 0; i < SIZE5; ++i) {
                TS_ASSERT( test02Size5.insert(i) );
            }
            for (int i = 0; i < SIZE5; ++i) {
                TS_ASSERT( not test02Size5.insert(i) );
            } 
            IBSTreeInt test02Size10;
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( test02Size10.insert(i) );
            }
            for (int i = 0; i < SIZE10; ++i) {
                TS_ASSERT( not test02Size10.insert(i) );
            }
            IBSTreeInt test02Size15;
            for (int i = 0; i < SIZE15; ++i) {
                TS_ASSERT( test02Size15.insert(i) );
            }
            for (int i = 0; i < SIZE15; ++i) {
                TS_ASSERT( not test02Size15.insert(i) );
            }
        }
        void tezd03PublicTreeOutInOrder(void) {
            IBSTreeInt test03Tree0;
            std::ostringstream oss0;
            test03Tree0.treeOutInOrder(oss0);
            TS_ASSERT_EQUALS( oss0.str(), "" );

            IBSTreeInt test03Tree7;
            fillTree7(test03Tree7);
            std::ostringstream oss7;
            test03Tree7.treeOutInOrder(oss7);
            TS_ASSERT_EQUALS( oss7.str(), "0 1 2 3 4 5 6" );


            IBSTreeInt test03Tree70;
            fillTree70(test03Tree70);
            std::ostringstream oss70;
            test03Tree70.treeOutInOrder(oss70);
            TS_ASSERT_EQUALS( oss70.str(), "15 20 30 40 50 60 70" );

            IBSTreeInt test03TreeRandom1Kib;
            randomTreeSize1KibSort(test03TreeRandom1Kib);

            std::ostringstream treeRandom1Kib;
            test03TreeRandom1Kib.treeOutInOrder(treeRandom1Kib);
            std::ostringstream arrayRandom1Kib;

            randomIntsOut(arrayRandom1Kib);
            TS_ASSERT_EQUALS( arrayRandom1Kib.str(), treeRandom1Kib.str() );
        }
        void tezd04PublicFind(void) {
            IBSTreeInt test04Tree0;
            TS_ASSERT( not test04Tree0.find(INT_MIN) );

            IBSTreeInt test04Tree7;
            fillTree7(test04Tree7);
            for (int i = -1; i < 15; ++i) {
                if (-1 < i && i < 7) {
                    TS_ASSERT( test04Tree7.find(i) );
                } else
                    TS_ASSERT( not test04Tree7.find(i) );
            }
            IBSTreeInt test04Tree9;
            fillTree9(test04Tree9);
            for (int i = -1; i < 16; ++i) {
                const int * p = std::find(sequance9, sequance9end, i);
                if (sequance9end != p) {
                    TS_ASSERT( test04Tree9.find(i) );
                } else
                    TS_ASSERT( not test04Tree9.find(i) );
            }
        }
        void tezd05PublicFindMin(void) {
            IBSTreeInt test05Tree0;
            try {
                test05Tree0.findMin();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            IBSTreeInt test05Tree9;
            fillTree9(test05Tree9);
            TS_ASSERT_EQUALS( test05Tree9.findMin(), 1);

            IBSTreeInt test05Tree1;
            test05Tree1.insert(1);
            TS_ASSERT_EQUALS( test05Tree1.findMin(), 1);

            IBSTreeInt test05Tree2;
            fillTree9(test05Tree2);
            test05Tree2.insert(2);
            TS_ASSERT_EQUALS( test05Tree2.findMin(), 1);

            IBSTreeInt test05Tree3;
            fillTree9(test05Tree3);
            test05Tree3.insert(INT_MIN);
            TS_ASSERT_EQUALS( test05Tree3.findMin(), INT_MIN);

            IBSTreeInt test05TreeRandom1Kib;
            randomTreeSize1Kib(test05TreeRandom1Kib);
            int min = *std::min_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test05TreeRandom1Kib.findMin(), min );

#ifdef BIG
            IBSTreeInt test05TreeRandom1Mib;
            randomTreeSize1Mib(test05TreeRandom1Mib);
            min = *std::min_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test05TreeRandom1Mib.findMin(), min );
#endif
        }
        void tezd06PublicFindMax(void) {
            IBSTreeInt test06Tree0;
            try {
                test06Tree0.findMax();
                TS_ASSERT( false );
            } catch (std::range_error & e) {
                TS_ASSERT( true );
            }

            IBSTreeInt test06Tree1;
            fillTree9(test06Tree1);
            TS_ASSERT_EQUALS( test06Tree1.findMax(), 14);

            IBSTreeInt test06Tree2;
            test06Tree2.insert(1);
            TS_ASSERT_EQUALS( test06Tree2.findMax(), 1);

            IBSTreeInt test06Tree3;
            fillTree9(test06Tree3);
            test06Tree3.insert(15);
            TS_ASSERT_EQUALS( test06Tree3.findMax(), 15);

            IBSTreeInt test06Tree9;
            fillTree9(test06Tree9);
            test06Tree9.insert(INT_MAX);
            TS_ASSERT_EQUALS( test06Tree9.findMax(), INT_MAX);

            IBSTreeInt test06TreeRandom1Kib;
            randomTreeSize1Kib(test06TreeRandom1Kib);
            int max = *std::max_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test06TreeRandom1Kib.findMax(), max );
#ifdef BIG
            IBSTreeInt test06TreeRandom1Mib;
            randomTreeSize1MibSort(test06TreeRandom1Mib);
            max = *std::max_element(randomInts.begin(), randomInts.end());
            TS_ASSERT_EQUALS( test06TreeRandom1Mib.findMax(), max );
#endif
        }
        void tezd07PublicNext(void) {
            int zero = 0;
            int intMax = INT_MAX;
            IBSTreeInt test07Tree0;
            const int & foundZero = test07Tree0.next(zero);
            TS_ASSERT_EQUALS( &zero , &foundZero);
            const int & foundIntMax = test07Tree0.next(intMax);
            TS_ASSERT_EQUALS( &intMax , &foundIntMax);

            IBSTreeInt test07TreeRandom1Kib;
            randomTreeSize1KibSort(test07TreeRandom1Kib);
            for (size_t i = 0, j = 1; i < randomInts.size() - 1; ++i, ++j) {
                TS_ASSERT_EQUALS(
                    test07TreeRandom1Kib.next(randomInts[i]), randomInts[j]
                );
            }
        }
        void tezd08PublicPrevious(void) {
            int zero = 0;
            int intMin = INT_MIN;
            IBSTreeInt test07Tree0;
            const int & foundZero = test07Tree0.previous(zero);
            TS_ASSERT_EQUALS( &zero , &foundZero);
            const int & foundIntMin = test07Tree0.previous(intMin);
            TS_ASSERT_EQUALS( &intMin , &foundIntMin);

            IBSTreeInt test08TreeRandom1Kib;
            randomTreeSize1KibSort(test08TreeRandom1Kib);
            for (int i = 1, j = 0; i < SIZE10; ++i, ++j) {
                TS_ASSERT_EQUALS(
                    test08TreeRandom1Kib.previous(randomInts[i]), randomInts[j]
                );
            }
        }
        void tezd09PublicExtractMinimum(void) {
            IBSTreeInt test09Tree0;
            TS_ASSERT_EQUALS( test09Tree0.extractMinimum(), nullptr );
            TS_ASSERT_EQUALS( test09Tree0.top(), nullptr );

            IBSTreeInt test09Tree9;
            fillTree9(test09Tree9);

            IBSTreeInt::node_t * n1 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);
            delete n1;

            IBSTreeInt::node_t * n3 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            IBSTreeInt::node_t * n4 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            IBSTreeInt::node_t * n6 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            IBSTreeInt::node_t * n7 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            IBSTreeInt::node_t * n8 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            IBSTreeInt::node_t * n10 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            IBSTreeInt::node_t * n13 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            IBSTreeInt::node_t * n14 = test09Tree9.extractMinimum();
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;
            TS_ASSERT( isNull(test09Tree9.top()) );
        }
        void tezd10PublicExtractMaximum(void) {
            IBSTreeInt test10Tree0;
            TS_ASSERT_EQUALS( test10Tree0.extractMaximum(), nullptr );
            TS_ASSERT_EQUALS( test10Tree0.top(), nullptr );

            IBSTreeInt test10Tree9;
            fillTree9(test10Tree9);

            IBSTreeInt::node_t * n14 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;

            IBSTreeInt::node_t * n13 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            IBSTreeInt::node_t * n10 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            IBSTreeInt::node_t * n8 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            IBSTreeInt::node_t * n7 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            IBSTreeInt::node_t * n6 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            IBSTreeInt::node_t * n4 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            IBSTreeInt::node_t * n3 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            IBSTreeInt::node_t * n1 = test10Tree9.extractMaximum();
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);
            delete n1;
            TS_ASSERT_EQUALS( test10Tree9.extractMaximum(), nullptr );
            TS_ASSERT( isNull(test10Tree9.top()) );
        }
        void test11PublicExtractNode(void) {
            IBSTreeInt test11Tree0;
            TS_ASSERT_EQUALS( test11Tree0.extractNode(INT_MAX), nullptr );
            TS_ASSERT_EQUALS( test11Tree0.top(), nullptr );
            // printVertical(std::cerr << std::endl, this->top());
            printHorizontal(std::cerr << std::endl, test11Tree0.top()) << std::endl;

            IBSTreeInt test11Tree3;
            test11Tree3.insert(2);
            test11Tree3.insert(1);
            test11Tree3.insert(3);
            printVertical(std::cerr << std::endl, test11Tree3.top());
            printHorizontal(std::cerr << std::endl, test11Tree3.top()) << std::endl;

            IBSTreeInt::node_t * n2 = test11Tree3.extractNode(2);
            TS_ASSERT( isNotNull(n2) );
            TS_ASSERT_EQUALS(n2->key() , 2);
            delete n2;

            IBSTreeInt test11Tree9;
            fillTree9(test11Tree9);
            printVertical(std::cerr << std::endl, test11Tree9.top());
            printHorizontal(std::cerr << std::endl, test11Tree9.top()) << std::endl;

            IBSTreeInt::node_t * n14 = test11Tree9.extractNode(14);
            TS_ASSERT( isNotNull(n14) );
            TS_ASSERT_EQUALS(n14->key() , 14);
            delete n14;

            IBSTreeInt::node_t * n13 = test11Tree9.extractNode(13);
            TS_ASSERT( isNotNull(n13) );
            TS_ASSERT_EQUALS(n13->key() , 13);
            delete n13;

            IBSTreeInt::node_t * n3 = test11Tree9.extractNode(3);
            TS_ASSERT( isNotNull(n3) );
            TS_ASSERT_EQUALS(n3->key() , 3);
            delete n3;

            IBSTreeInt::node_t * n6 = test11Tree9.extractNode(6);
            TS_ASSERT( isNotNull(n6) );
            TS_ASSERT_EQUALS(n6->key() , 6);
            delete n6;

            IBSTreeInt::node_t * n7 = test11Tree9.extractNode(7);
            TS_ASSERT( isNotNull(n7) );
            TS_ASSERT_EQUALS(n7->key() , 7);
            delete n7;

            IBSTreeInt::node_t * n8 = test11Tree9.extractNode(8);
            TS_ASSERT( isNotNull(n8) );
            TS_ASSERT_EQUALS(n8->key() , 8);
            delete n8;

            IBSTreeInt::node_t * n4 = test11Tree9.extractNode(4);
            TS_ASSERT( isNotNull(n4) );
            TS_ASSERT_EQUALS(n4->key() , 4);
            delete n4;

            IBSTreeInt::node_t * n10 = test11Tree9.extractNode(10);
            TS_ASSERT( isNotNull(n10) );
            TS_ASSERT_EQUALS(n10->key() , 10);
            delete n10;

            IBSTreeInt::node_t * n1 = test11Tree9.extractNode(1);
            TS_ASSERT( isNotNull(n1) );
            TS_ASSERT_EQUALS(n1->key() , 1);

            TS_ASSERT( isNull(test11Tree9.top()) );
            TS_ASSERT_EQUALS( test11Tree9.extractNode(INT_MAX), nullptr );
            delete n1;
        }
        void test00ProtectedSplit(void) {
            delete root;
            fillTree31(*this);
            // DEBUG_OUT(std::cerr, *this);
            printVertical(std::cerr << std::endl, this->top());
            printHorizontal(std::cerr << std::endl, this->top()) << std::endl;
            node_t * root1 = nullptr;
            node_t * root2 = nullptr;
        /**
         *                              16  
         *               +---------------+---------------+
         *               8                              24
         *       +-------+-------+               +-------+-------+
         *       4              12              20              28
         *   +---+---+       +---+---+       +---+---+       +---+---+
         *   2       6      10      14      18      22      26      30
         * +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+   +-+-+
         * 1   3   5   7   9  11  13  15  17  19  21  23  25  27  29  31
         *
         *               8
         *       +-------+-------+
         *       4              10
         *   +---+---+       +---+---+
         *   2       6       9      11
         * +-+-+   +-+-+     
         * 1   3   5   7      
         *
         *                 16  
         *  +---------------+---------------+
         * 12                              24
         *  +-------+               +-------+-------+
         *         14              20              28
         *      +---+---+       +---+---+       +---+---+
         *     13      15      18      22      26      30
         *                    +-+-+   +-+-+   +-+-+   +-+-+
         *                   17  19  21  23  25  27  29  31
         *





                                     16
               +----------------------+----------------------+
               8                                             24
       +-------+---------+                        +----------+-----------+
       4                 12                      20                      28
   +---+---+        +-----+----+            +-----+----+            +-----+----+
   2       6       10          14          18          22          26          30
 +-+-+   +-+-+   +--+-+      +--+-+      +--+-+      +--+-+      +--+-+      +--+-+
 1   3   5   7   9    11    13    15    17    19    21    23    25    27    29    31






         */
            split(root1, root2, root, 11);
            root = nullptr;

            IBSTreeInt other1(root1);

            std::ostringstream oss1;
            other1.treeOutInOrder(oss1);
            TS_ASSERT_EQUALS( oss1.str(), "1 2 3 4 5 6 7 8 9 10 11" );

            IBSTreeInt other2(root2);
            std::ostringstream oss2;
            other2.treeOutInOrder(oss2);
            TS_ASSERT_EQUALS( oss2.str(),
                "12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31"
            );
        }
        void test99PublicSplitTo(void) {
            for (int i = 0 ; i < 95; ++i) {
                IBSTreeInt test99Tree31;
                fillTree313(test99Tree31);
                // DEBUG_OUT(std::cerr, test99Tree31);
                IBSTreeInt test99Tree31a;
                IBSTreeInt test99Tree31b;
                test99Tree31.splitTo(test99Tree31a, test99Tree31b, i);
                // DEBUG_OUT(std::cerr, test99Tree31a);
                // DEBUG_OUT(std::cerr, test99Tree31b);
            }
            IBSTreeInt test99TreeRandom1Kib;
            randomTreeSize1KibSort(test99TreeRandom1Kib);
            printVertical(std::cerr << std::endl, test99TreeRandom1Kib.top());
            printHorizontal(std::cerr << std::endl, test99TreeRandom1Kib.top()) << std::endl;
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=91:ts=4:sw=4:sts=4:et
 * EOF */
