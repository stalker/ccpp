################################################################################
## GNUmakefile for GNU Make and gcc/clang or
## Microsoft Visual Stutio with GNU Make
# requare GNU Make version 3.81
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#

ROOT=.
PLATFORM=$(shell $(ROOT)/systype.sh)
include $(ROOT)/Make.defines.$(PLATFORM).mk

MAIN        = runner
PROGS       = 
CSOURCES    = 
CPPSOURCES  = $(MAIN).cpp
INCDIR      = $(CURDIR)/include
HEADER      = nodeTest.hpp
HDRS        = 

################################################################################
### all ###
all    : build

################################################################################
### debug ### /MDd
ifdef DBG
ifeq ($(OS), Windows_NT)
  LFLAGS   +=  /MTd
  DCFLAGS   = DEBUG=$(DBG) /Od /ZI
else
  DCFLAGS   = -DDEBUG=$(DBG) -g
endif
else
ifeq ($(OS), Windows_NT)
  LFLAGS   += /MD
  DCFLAGS   = NDEBUG=1 /Ox
else
  DCFLAGS   = -DNDEBUG -O3
endif
endif

################################################################################
### Default for OS GNU/Linux ###
# COMPILE.c  = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
# COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
# LINK.c     = $(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
# LINK.cc    = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
################################################################################
### detect OS version ###
ifeq ($(OS), Windows_NT)
  CHCP      = chcp 866 1>nul && 
  CCSTD     =
  CXXSTD    =
  _CC       = $(CHCP) cl
  _LD       = $(CHCP) cl /o
  _CPP      = $(CHCP) cl /E
  _CXX      = $(CHCP) cl
  _CFLAGS   = /I$(INCDIR) /D$(DCFLAGS) /c
  _CXXFLAGS = /EHsc /I$(INCDIR) /D$(DCFLAGS) /c
  _LDFLAGS  = /link /MAP:$(MAIN).map /DEBUG /PDB:$(MAIN).pdb
  GDEL      = $(CHCP) del
  GEEXT     = .exe
  GHEXT     = h
  GOEXT     = obj
  GWINFTMP  = *.idb *.ilk *.map *.pdb
hello  :
	cmd /c echo Hello world.
else
  CCSTD     = -std=c99
  CXXSTD    = -std=c++11
  _CC       = gcc
  _LD       = g++ -o
  _CPP      = $(_CC) -E
  _CXX      = g++
  _CFLAGS   = -c $(CCSTD) -I$(INCDIR) $(DCFLAGS)
  _CXXFLAGS = -c $(CXXSTD) -I$(INCDIR) $(DCFLAGS)
  _LDFLAGS  =
  GDEL      = rm -f
  GEEXT     =
  GHEXT     = h
  GOEXT     = o
hello  :
	echo  Hello world.
endif
ifeq ($(_system_name), OSX)
  CFLAGS   += -I/opt/local/include
  CXXFLAGS += -I/opt/local/include
  LFLAGS   += -L/opt/local/lib
endif

################################################################################
### Compilators, linker and options ###
CC          = $(_CC)
CPP         = $(_CPP)
CXX         = $(_CXX)
LD          = $(_LD)
CFLAGS     += $(_CFLAGS)
CXXFLAGS   += $(_CXXFLAGS)
LDFLAGS    += $(_LDFLAGS)
RM          = $(GDEL)
EEXT        = $(GEEXT)
HEXT        = $(GHEXT)
OEXT        = $(GOEXT)

OBJECTS     = $(CPPSOURCES:.cpp=.$(OEXT)) $(CSOURCES:.c=.$(OEXT))
TARGETS     = $(PROGS:=$(EEXT))

################################################################################
### build ###
build  : $(MAIN)$(EEXT) $(TARGETS)

$(MAIN)$(EEXT) : $(OBJECTS) $(HDRS) $(HEADER)
	$(LD) $(MAIN)$(EEXT) $(OBJECTS) $(LDFLAGS)

$(TARGETS): $(OBJECTS) $(HDRS) $(HEADER)
	$(LD) $@ $(OBJECTS) $(LDFLAGS)

$(MAIN).$(OEXT) : $(HEADER) $(HDRS)

ifeq ($(OS), Windows_NT)
$(OBJECTS): %.$(OEXT): %.cpp $(HDRS)
	$(CC) $(CFLAGS) $< /o $@
#else
#$(MAIN).$(OEXT): %.$(OEXT): %.c $(HDRS) %.$(HEXT)
#	$(CC) $(CFLAGS) $< -o $@
endif

clean  :
	$(RM) $(MAIN)$(EEXT) $(TARGETS) *.$(OEXT) $(GWINFTMP) *.bak

################################################################################
#EOF
