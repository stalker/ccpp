////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _NODETEST_HPP_
#define _NODETEST_HPP_

#include "../node.hpp"

#include <cstddef>
#include <cstdint>

class BasicNodeTest : public CxxTest::TestSuite {
    public:
        void testIsNull(void) {
            svn::BasicNode * a = nullptr;
            TS_ASSERT(svn::isNull(a));
            TS_ASSERT(not svn::isNotNull(a));
        }
        void testLink(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            delete a;
        }
        void testLinkLeft(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            a->linkLeft(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete a;
        }
        void testLinkRight(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            a->linkRight(b);
            TS_ASSERT(not svn::isNull(a));
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            delete a;
        }
        void testUnLink(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLink();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(a->isRightNull());
            delete a;
            delete b;
            delete c;
        }
        void testUnLinkLeft(void) {
            svn::BasicNode * a = new svn::BasicNode;
            svn::BasicNode * b = new svn::BasicNode;
            svn::BasicNode * c = new svn::BasicNode;
            a->link(b, c);
            TS_ASSERT(not a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            a->unLinkLeft();
            TS_ASSERT(a->isLeftNull());
            TS_ASSERT(not a->isRightNull());
            delete a;
            delete b;
            delete c;
        }
};

#endif // _NODETEST_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
