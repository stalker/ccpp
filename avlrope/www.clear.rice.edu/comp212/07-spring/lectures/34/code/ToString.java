
package brs;

/**
 * Computes a String representation of the binary tree host so that it can be
 * printed vertically.
 * @author Dung X. Nguyen - Copyright 2000 - All rights reserved.
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 */
public class ToString<T> implements IVisitor<T,String,Void> {
    private ToStringHelp<T> _toStringHelp = new ToStringHelp<T>();
    /**
     * Returns "[]", a String representation of an empty binary tree.
     * @param host an empty binary tree.
     * @param nu not used.
     * @return String
    */
    public String emptyCase(BiTree<T> host, Void... nu) {
        return "[]";
    }

    /**
     * Computes a String representation of the binary tree host so that it can
     * be printed vertically. There is no '\n' at the end of the String.  Passes
     * appropriate leftmost leading String to a helper visitor to compute the
     * String representations of the left and right subtrees.
     * @param host a non-empty binary tree.
     * @param nu not used.
     * @return String
     */
    public String nonEmptyCase(BiTree<T> host, Void... nu) {
        String ls = host.getLeftSubTree().execute(_toStringHelp, "|  ");
        String rs = host.getRightSubTree().execute(_toStringHelp, "   ");
        return (host.getRootDat() + "\n" + ls + "\n" + rs);
        // EXERCISE FOR STUDENTS: Rewrite using anonymous inner classes.
    }
}
