
package brs;

/**
 * Computes a String representation of the binary tree host so that it can be
 * printed vertically, given a leftmost leading string for the two subtrees.
 * Called by ToString.
 * Should be implemented as an anonymous inner class in the call by ToString.
 * @author Dung X. Nguyen - Copyright 2001 - All rights reserved.
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 */
public class ToStringHelp<T> implements IVisitor<T,String,String> {
    /**
    * Returns "|_[]" to denote an empty tree subtree.
    * @param host an empty binary (sub)tree.
    * @param nu not used.
    * @return String
    */
    public String emptyCase(BiTree<T> host, String... nu) {
        return "|_ []";
    }

    /**
    * Computes a String representation of the binary tree host so that it
    * can be printed vertically.
    * There is no '\n' at the end of the String.
    * @param host a non-empty binary (sub)tree.
    * @param leftLead appropriate leftmost leading String to help compute the
    * String representations of the left and right subtrees.
    * @return String
    */
    public String nonEmptyCase(BiTree<T> host, String... leftLead) {
        String ls = host.getLeftSubTree().execute(this, leftLead[0] + "|  ");
        String rs = host.getRightSubTree().execute(this, leftLead[0] + "   ");
        return ("|_ " + host.getRootDat()+ "\n" +
                leftLead[0] + ls + "\n" +
                leftLead[0] + rs);
    }
}
