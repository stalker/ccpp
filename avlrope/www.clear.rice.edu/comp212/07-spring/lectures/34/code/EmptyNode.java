
package brs;

/**
 * Represents the empty state of a BiTree.  Uses the singleton pattern to model
 * the uniqueness of "emptiness".
 * @author Dung X. Nguyen - Copyright 2002 - All rights reserved.
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 * @since 03/11/02
 */
class EmptyNode<T> extends ANode<T> {
    /**
    * Throws java.util.NoSuchElementException.
    * @param owner the BiTree holding this EmptyNode.
    */
    T getRootDat (BiTree<T> owner) {
        throw new java.util.NoSuchElementException ("EmptyNode.getRootDat()");
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param dat a data Object.
    * @param owner the BiTree holding this EmptyNode.
    */
    void setRootDat (BiTree<T> owner, T dat) {
        throw new java.util.NoSuchElementException ("EmptyNode.setRootDat()");
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param owner the BiTree holding this EmptyNode.
    */
    BiTree<T> getLeftSubTree (BiTree<T> owner) {
        throw new java.util.NoSuchElementException ("EmptyNode.getLeftSubTree()");
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param owner the BiTree holding this EmptyNode.
    */
    BiTree<T> getRightSubTree(BiTree<T> owner) {
        throw new java.util.NoSuchElementException ("EmptyNode.getRightSubTree()");
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param biTree a given BiTree.
    * @param owner the BiTree holding this EmptyNode.
    */
    void setLeftSubTree(BiTree<T> owner, BiTree<T> biTree) {
        throw new java.util.NoSuchElementException ("EmptyNode.setLeftSubTree()");
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param biTree a given BiTree.
    * @param owner the BiTree holding this EmptyNode.
    */
    void setRightSubTree(BiTree<T> owner, BiTree<T> biTree) {
        throw new java.util.NoSuchElementException ("EmptyNode.setRightSubTree()");
    }

    /**
    * Asks the owner tree to set the root node to a new DatNode containing dat,
    * resulting in a state change from empty to non-empty.
    * @param dat a given data Object.
    * @param owner the context of this state.
    */
    void insertRoot(BiTree<T> owner, T dat) {
        owner.setRootNode(new DatNode<T>(dat));
    }

    /**
    * Throws java.util.NoSuchElementException.
    * @param owner the BiTree holding this EmptyNode.
    */
    T remRoot (BiTree<T> owner) {
        throw new java.util.NoSuchElementException ("EmptyNode.remRoot()");
    }

    /**
    * Calls algo's emptyCase () method to execute the algorithm algo.
    * @param owner the BiTree holding this EmptyNode.
    * @param algo the visiting algorithm
    * @param inp the vararg input algo needs.
    * @return the output for the emptyCase() of algo.
    */
    <R,P> R execute(BiTree<T> owner, IVisitor<T,R,P> algo, P... inp) {
        return algo.emptyCase(owner, inp);
    }
}

