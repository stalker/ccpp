
package brs;

/**
 * Represents all extrinsic algorithms on a <code>BiTree<code> as a visitor to
 * the BiTree host structure.  The BiTree host will make the appropriate call on
 * this IVisitor's methods.
 * Since BiTree is a mutable data structure, we require an exact match for
 * the data type (BiTree<T>, not BiTree<? extends T>).
 * @author Dung X. Nguyen - Copyright 2001 - All rights reserved.
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 */
public abstract interface IVisitor<T,R,P> {
    /**
    * Called by the host when the host is empty.
    * @param host an empty BiTree on which this IVisitor operates.
    * @param inp the vararg input needed by this IVisitor to perform its task.
    * @return Object the output of this algorithm on the host.
    */
    public abstract R emptyCase(BiTree<T> host, P... inp);

    /**
    * Called by the host when the host is not empty.
    * @param host a non-empty BiTree on which this IVisitor operates.
    * @param inp the vararg input needed by this IVisitor to perform its task.
    * @return Object the output of this algorithm on the host.
    */
    public abstract R nonEmptyCase(BiTree<T> host, P... inp);
}

