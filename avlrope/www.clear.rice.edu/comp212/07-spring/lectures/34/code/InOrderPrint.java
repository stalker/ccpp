package brs.visitor;

import brs.*;

/**
 * Print all the numbers in a tree in in-order, line by line
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 */
public class InOrderPrint<T> implements IVisitor<T,Void,Void> {
    public Void emptyCase(BiTree<T> host, Void... nu) {
        return null; 
    }

    public Void nonEmptyCase(BiTree<T> host, Void... nu) {
        host.getLeftSubTree().execute(this);
        System.err.println(host.getRootDat());
        host.getRightSubTree().execute(this);
        return null;
    }

    public static void main(String[] args) {
        BiTree<Integer> bt = new BiTree<Integer>();
        InOrderPrint<Integer> inOrderPrint = new InOrderPrint<Integer>();
        
        System.out.println(bt);
        bt.execute(inOrderPrint);
        System.out.println("==============================");
        
        bt.insertRoot(5);
        System.out.println(bt);
        bt.execute(inOrderPrint);
        System.out.println("==============================");
        
        bt.getLeftSubTree().insertRoot(-2);
        System.out.println(bt);
        bt.execute(inOrderPrint);
        System.out.println("==============================");
        
        bt.getRightSubTree().insertRoot(10);
        System.out.println(bt);
        bt.execute(inOrderPrint);
        System.out.println("==============================");
        
        bt.getRightSubTree().getLeftSubTree().insertRoot(-9);
        System.out.println(bt);
        bt.execute(inOrderPrint);
    }
}