
package brs;

/**
 * Represents the state of the owner binary tree structure.  Union pattern
 * @author Dung X. Nguyen - Copyright 2001 - All rights reserved.
 * @author Mathias Ricken - Copyright 2008 - All rights reserved.
 */
abstract class ANode<T> {
    /**
    * Gets the root data of the owner tree if it exists.
    * @param owner the BiTree that holds this node.
    * @return the data element of this node if it exists.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract T getRootDat(BiTree<T> owner);

    /**
    * Sets the root element of the owner tree to a given data object.
    * @param dat
    * @param owner the BiTree that holds this node.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract void setRootDat(BiTree<T> owner, T dat);

    /**
    * Gets the left subtree of the owner tree.
    * @param owner the BiTree that holds this node.
    * @return the left subtree of this node if it exists.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract BiTree<T> getLeftSubTree(BiTree<T> owner);

    /**
    * Gets the right subtree of the owner tree.
    * @param owner the BiTree that holds this node.
    * @return the right subtree of this node if it exists.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract BiTree<T> getRightSubTree(BiTree<T> owner);

    /**
    * Sets the left subtree of the owner tree to a given tree.
    * @param biTree != null.
    * @param owner the BiTree that holds this node.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract void setLeftSubTree(BiTree<T> owner, BiTree<T> biTree);

    /**
    * Sets the right subtree of the owner tree to a given tree.
    * @param biTree != null.
    * @param owner the BiTree that holds this node.
    * @exception NoSuchElementException if the owner is empty.
    */
    abstract void setRightSubTree(BiTree<T> owner, BiTree<T> biTree);

    /**
    * Inserts a root element to the owner tree. Allows the owner tree to change
    * state from empty to non-empty.
    * @param dat
    * @param owner the BiTree that holds this node.
    * @exception IllegaStateException if the owner is not empty.
    */
    abstract void insertRoot(BiTree<T> owner, T dat);

    /**
    * Removes and returns the root element from the owner tree.  Allows the
    * owner tree to change state from non-empty to empty.
    * @param dat
    * @param owner the BiTree that holds this node.
    * @exception IllegaStateException if the owner has more than one element.
    */
    abstract T remRoot(BiTree<T> owner);

    /**
    * Calls the appropriate visitor's method to execute the visiting algorithm.
    * @param owner the BiTree that holds this node.
    * @param algo the visiting algorithm
    * @param inp the vararg input the algorithm needs.
    * @return the output for the algorithm.
    */
    abstract <R,P> R execute(BiTree<T> owner, IVisitor<T,R,P> algo, P... inp);
}

