
package brs;

/**
* Stores data and represents a non-empty state.
* @author Dung X. Nguyen - Copyright 2001 - All rights reserved.
* @author Mathias Ricken - Copyright 2008 - All rights reserved.
*/
class DatNode<T> extends ANode<T> {
    /**
     * Data Invariant: != null.
     * @SBGen Variable (,left,,64)
     */
    private BiTree<T> _leftTree = new BiTree<T> ();
    /**
    * the stored data element.
    */
    private T _dat;
    /**
    * Data Invariant: != null.
    * @SBGen Variable (,right,,64)
    */
    private BiTree<T> _rightTree = new BiTree<T> ();

    /**
    * Initialize the data element to a given object.
    * @param dat a given data object.
    */
    DatNode(T dat) {
        _dat = dat;
    }


    /**
    * Gets the root data of the owner Tree.
    * @param owner the BiTree holding this DatNode.
    * @return the data element of this DatNode.
    */
    T getRootDat(BiTree<T> owner) {
        return _dat;
    }

    /**
    * Sets the data element of this node to a given data object.
    * @param dat a given data object.
    * @param owner the BiTree holding this DatNode.
    */
    void setRootDat(BiTree<T> owner, T dat) {
        _dat = dat;
    }

    /**
    * Gets the left subtree of the owner tree.
    * @param owner the BiTree holding this DatNode.
    * @return the left subtree of this DatNode.
    */
    BiTree<T> getLeftSubTree(BiTree<T> owner) {
        return _leftTree;
    }

    /**
    * Gets the right subtree of the owner tree.
    * @param owner the BiTree holding this DatNode.
    * @return the right subtree of this DatNode.
    */
    BiTree<T> getRightSubTree(BiTree<T> owner) {
        return _rightTree;
    }

    /**
    * Sets the left subtree of this Datnode to a given tree.
    * Allows for growing the owner tree.
    * @param biTree != null.  Does not check for null!
    * @param owner the BiTree holding this DatNode.
    */
    void setLeftSubTree(BiTree<T> owner, BiTree<T> biTree)  {
        _leftTree = biTree;
    }

    /**
    * Sets the right subtree of this node to a given tree.
    * Allows for growing the owner tree.
    * @param biTree != null.  Does not check for null!
    * @param owner the BiTree holding this DatNode.
    */
    void setRightSubTree(BiTree<T> owner, BiTree<T> biTree) {
        _rightTree = biTree;
    }

    /**
    * Throws an IllegalStateException because the owner tree is not empty.
    * @exception IllegaStateException.
    */
    void insertRoot(BiTree<T> owner, T dat) {
        throw new IllegalStateException ("DatNode.insertRoot().");
    }

    /**
    * Removes and returns the root element from the owner tree by asking the
    * left subtree and, if necessary, the right subtree to help do the job.
    * The subtrees help determine whether or not the root element can be removed
    * by executing appropriate anonymous visitors.
    * @param owner the BiTree holding this DatNode.  Why is it final?
    * @exception IllegaStateException if both subtrees of owner are non-empty.
    */
    T remRoot(final BiTree<T> owner) {
        return _leftTree.execute(new IVisitor<T,T,Void>() {
            /**
             * The left subtree is empty. The parent tree can simply become the
             * right subtree.
             */
            public T emptyCase(BiTree<T> host, Void... notUsed) {
                owner.setRootNode(_rightTree.getRootNode());
                return _dat;

            }

            /**
             * The left subtree is not empty!  The right subtree must determine
             * whether or not the parent root can be removed.
             */
            public T nonEmptyCase(BiTree<T> host, Void... inp) {
                 return _rightTree.execute(new IVisitor<T,T,Void>() {
                    /**
                     * At this point both the left and right subtrees are empty.
                     */
                    public T emptyCase(BiTree<T> h, Void... notUsed) {
                        owner.setRootNode(_leftTree.getRootNode());
                        return _dat;
                    }

                    /**
                    * Both left and right subtrees are not empty!
                    * Cannot remove root!
                    */
                    public T nonEmptyCase(BiTree<T> h, Void... notUsed) {
                        throw new IllegalStateException ("Not a leaf.");
                    }
                });            
            }
        });
    }

    /**
    * Calls algo's nonEmptyCase() method to execute the algorithm algo.
    * @param owner the BiTree holding this DatNode.
    * @param algo an algorithm on owner.
    * @param inp the vararg input algo needs.
    * @return the output for the nonEmptyCase() of algo.
    */
    <R,P> R execute(BiTree<T> owner, IVisitor<T,R,P> algo, P... inp) {
        return algo.nonEmptyCase (owner, inp);
    }
}

