################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using cc and CC.

.SUFFIXES: .o .c .cpp

RANLIB      = echo
AR          = ar
AWK         = awk

CCSTD       =
CXXSTD      =
CC          = cc -m$(BITS)
CCC         = CC -m$(BITS)
LD          = CC -m$(BITS) -o
CFLAGS      = -c $(CCSTD)
CCFLAGS     = -c $(CXXSTD)
CFLAGS     += -DMSOLARIS -D__EXTENSIONS__
CFLAGS     += -D_XOPEN_SOURCE_EXTENDED=1
CFLAGS     += -D_XPG4_2=1 $(EXTRA)
#CFLAGS    += -D_XOPEN_SOURCE=1 -D_XOPEN_SOURCE_EXTENDED=1
CCFLAGS    += -DMSOLARIS -D__EXTENSIONS__
CCFLAGS    += -D_XOPEN_SOURCE_EXTENDED=1
CCFLAGS    += -D_XPG4_2=1 $(EXTRA)
#CCFLAGS   += -D_XOPEN_SOURCE=1 -D_XOPEN_SOURCE_EXTENDED=1
NAMEMAX     = -DNAME_MAX=_XOPEN_NAME_MAX

################################################################################
### modified built-in check make -p ###
#  recipe to execute (modified built-in):

.cpp:
	$(LINK.C) -o $@ $< $(LDLIBS)
.cpp.o:
	$(COMPILE.C) $(OUTPUT_OPTION) $<

#EOF
