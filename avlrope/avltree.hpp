////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////
// Compilation:
// Execution:
// Dependencies: node.hpp basictree.hpp bstree.hpp

#ifndef _AVLTREE_HPP_
#define _AVLTREE_HPP_

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "node.hpp"
#include "basictree.hpp"
#include "bstree.hpp"

#define DEBUG 1
#ifdef DEBUG
# include "mydebug.hpp"
#endif

namespace svn {

////////////////////////////////////////////////////////////////////////////////
// Adelson-Velsky and Landis, Binary Search Tree class.
// An AVL tree is a self-balancing binary search tree.  It was the first such
// data structure to be invented.  In an AVL tree, the  heights  of  the  two
// child subtrees of any node differ by at most one;  if  at  any  time  they
// differ by more than one, rebalancing is done  to  restore  this  property.
// Lookup, insertion, and deletion all take O(log n) time in both the average
// and worst cases, where n is the number of nodes in the tree prior  to  the
// operation.  Insertions and deletions may require the tree to be rebalanced
// by one or more tree rotations.
// For  lookup-intensive applications, AVL trees  are faster  than  red–black
// trees because they are more strictly balanced.
//
// Operations:
// Read-only operations of an AVL tree involve carrying out the same  actions
// as would  be  carried  out  on  an  unbalanced  binary  search  tree,  but
// modifications have to observe  and  restore  the  height  balance  of  the
// subtrees.
// When inserting an element into an AVL tree, you initially follow the  same
// process as inserting into a Binary Search Tree. After inserting a node, it
// is necessary to check each of the node’s ancestors  for  consistency  with
// the invariants of AVL trees: this is called "retracing".  This is achieved
// by considering the balance factor of each node
//
// Since with a single insertion the height of an AVL subtree cannot increase
// by more than one,  the  temporary  balance  factor  of  a  node  after  an
// insertion will be in the range [–2,+2].  For each  node  checked,  if  the
// temporary balance factor remains in the range from –1 to +1 then  only  an
// update of the balance factor and no rotation is necessary. However, if the
// temporary balance factor becomes less than –1  or  greater  than  +1,  the
// subtree rooted at this node is AVL unbalanced, and a rotation is needed.
// The time required is O(log n) for lookup,  plus  a  maximum  of  O(log  n)
// retracing levels (O(1) on average) on the way back to  the  root,  so  the
// operation can be completed in O(log n) time.
//
// Since with a single deletion the height of an AVL subtree cannot  decrease
// by more than one, the temporary balance factor of a node will  be  in  the
// range from −2 to +2. If the balance factor remains in the range from −1 to
// +1 it can be adjusted in accord with the AVL rules.  If it becomes ±2 then
// the subtree is unbalanced and needs to be rotated.
//
//  @author Victor Skurikhin
//
template<typename T, class NODE = Node<T> >
class AVLTree : public BSTree<T, NODE> {
    friend class BSTreeIterator<T, NODE>;
    template <typename Y, class N>
    friend bool operator==(const BSTree<Y,N> &, const BSTree<Y,N> &);
    template <typename Y, class N>
    friend bool operator!=(const BSTree<Y,N> &, const BSTree<Y,N> &);
public:
    typedef NODE node_t;
    typedef BasicT<NODE> basict_t;
    typedef BSTree<T, NODE> bstree_t;
    typedef BSTreeIterator<T, NODE> iterator;
    typedef BSTreeIterator<T, const NODE> const_iterator;
    using basict_t::root;
    using basict_t::detailOut;

    ////////////////////////////////////////////////////////////////////////////

    AVLTree()          { /* None */ }    // Initializes an empty set.
    virtual ~AVLTree() { /* None */ }    // Virtual destructor.

    // Copy constructor and operator-function =.
    AVLTree(const AVLTree & other) : bstree_t(other) { /* None */ }
    AVLTree & operator=(const AVLTree & rhs) {
        if (&rhs == this) return *this;
        auto newRoot = rhs.bstree_t::clone();
        delete root;
        root = newRoot;
        return *this;
    }

    AVLTree(AVLTree && other) { basict_t::move(std::move(other)); }
    AVLTree & operator=(AVLTree && o) { return bstree_t::operator=(o); }

    const T & at(size_t i) const throw(std::range_error) {
        checkRootAndIndex(i, __LINE__, __FILE__);
        return orderStatistics(root, i + 1)->key();
    }

    const T & operator[](size_t i) const throw(std::range_error) {
        return at(i);
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Adds the key to the set if it is not already present.
    // @param key the key to add
    // @return false if the set contains key and true otherwise
    // @throws std::bad_alloc if can't allocated a memory
    virtual bool insert(const T & key) throw(std::bad_alloc) {
        bool found = false;
        root = add(root, found, key);
        return not found;
    }

    // Does the set contain the given key?
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool find(const T & key) const {
        return isNull(bstree_t::searchNode(root, key)) ? false : true;
    }

    // Returns the smallest key in the set.
    // @return the smallest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMin() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return bstree_t::searchMinimum(root)->key();
    }

    // Returns the largest key in the set.
    // @return the largest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMax() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return bstree_t::searchMaximum(root)->key();
    }

    // Returns  the smallest key in  the set greater than  to key  or if this
    // set is empty returns the key.
    // @param key the key
    // @return the next key in the set greater than to key
    virtual const T & next(const T & key) const {
        node_t * successor = bstree_t::nextNode(key);
        return isNull(successor) ? key : successor->key();
    }

    // Returns the largest key in the set less than or equal to key.
    // @param key the key
    // @return the largest key in the set table less than or equal to key
    virtual const T & previous(const T & key) const {
        node_t * predecessor = bstree_t::prevNode(key);
        return isNull(predecessor) ? key : predecessor->key();
    }

    // Does the set contain the given key?
    // @param key the key
    // @return a pointer to the node_t that contains key
    virtual const node_t * const searchNode(const T & key) const {
        return bstree_t::searchNode(root, key);
    }

    // @param key the key to extracting
    // @return a pointer to the extracted (by key) node,
    //         if tagret set is empty returns the NULL
    node_t * extractNode(const T & key) { return extractNodeFrom(root, key); }
    node_t * extractMinimum()           { return extractMinimumFrom(root); }
    node_t * extractMaximum()           { return extractMaximumFrom(root); }

    size_t size()          const { return root->size(); }
    virtual bool isEmpty() const { return isNull(root); } // Is the set empty?

    // Removes the key from the set if the key is present.
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool remove(const T & key) {
        bool found = false;
        const node_t * deletedNode = extractNode(key);
        found = isNull(deletedNode) ? false : true;
        delete deletedNode;
        return found;
    }

    bool avlCheck() { return (nullptr == avlSubTreeCheck(root)); }

    virtual void avlToMerge(AVLTree & o) throw(std::range_error) {
        if (isNull(o.root))
            return;
        else if (isNull(root)) {
            root = o.root;
            o.root = nullptr;
            return;
        } else if (findMax() < o.findMin()) {
            root = avlSubMerge(o.root);
            o.root = nullptr;
            return;
        }
        throw std::range_error("AVL Merge Error at: " +
              std::to_string(__LINE__) + std::string(" in file: ") + __FILE__);
    }

    virtual void avlSplitTo(AVLTree & b, AVLTree & c, const T & key) {
        if (isNull(root)) return;
        avlSubSplit(b.root, c.root, root, key);
        root = nullptr;
    }

    // Returns a stream representation of this set.
    // @return a stream representation of this set, with the keys separated
    std::ostream & treeOutInOrder(std::ostream & os) const {
        size_t index = 0;
        const char * sep = detailOut ? nl : sp;
        this->subTreeOutInOrder(os, root, index, sep);
        return os;
    }
protected:
    T & keyByIndex(size_t i) throw(std::range_error) {
        checkRootAndIndex(i, __LINE__, __FILE__);
        return const_cast<T &>(orderStatistics(root, i + 1)->key());
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * Simple rotation smallLeftRotate.
     * Figure 4 shows a Right Right situation. In its upper half, node x has
     * two child trees with a balance factor of  +2.   Moreover,  the  inner
     * child B of z is not higher than its sibling C.  This can happen by  a
     * height increase of subtree C or by a height decrease of subtree A. In
     * the latter case, also the pale situation where B has the same  height
     * as C may occur. figure 4:
     *                  A<x<B<z<C        A<x<B<z<C
     *                    x                  z
     *                   / \                / \
     *                  +   \ ^            x   \
     *                 /A\   z            / \   \
     *                 ---  / \          +   +   +
     *                     +   \   ==>  /A\ /B\ /C\
     *                    /B\   +       --- --- ---
     *                    ---  /C\
     *                         ---
     * The result of the left rotation is shown in the  lower  half  of  the
     * figure.  Three links (thick edges) and two balance factors are to  be
     * updated.
     */
    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty returns the NULL
    //         if right child of x is NULL returns x
    node_t * smallLeftRotate(node_t * x) {
        if (isNull(x)) return nullptr;
        DEBUG_FUNC1(x);
        // z is by 2 higher than its sibling
        node_t * z = x->right();     // Node  z = x.right
        if (isNotNull(z)) {
            x->linkRight(z->left()); // x.right = z.left # Inner child of z
            z->linkLeft(x);          // z.left  = x
        } else
          return x;
        updateNode(x);
        updateNode(z);
        return z;
    }

    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty returns the NULL
    //         if left child of x is NULL returns x
    node_t * smallRightRotate(node_t * x) {
        if (isNull(x)) return nullptr;
        DEBUG_FUNC1(x);
        node_t * z = x->left();      // Node z  = x.left
        if (isNotNull(z)) {
            x->linkLeft(z->right()); // x.left  = z.right
            z->linkRight(x);         // z.right = x
        } else
          return x;
        updateNode(x);
        updateNode(z);
        return z;
    }

    /**
     * Double rotation rotate_RightLeft.
     * A Right Left situation.  In its upper third, node  x  has  two  child
     * trees with a balance factor of +2.  But unlike figure  4,  the  inner
     * child y of z is higher than its sibling D.   This  can  happen  by  a
     * height increase of subtree B or C (with the consequence that they are
     * of different height) or by a height decrease of subtree  A.   In  the
     * latter case, it may also occur that B and C are of same height.
     *
     * The result of the first, the right, rotation is shown in  the  middle
     * third of the figure.  (With respect  to  the  balance  factors,  this
     * rotation is not of the same kind as the other AVL  single  rotations,
     * because the height difference between y and D is only 1.) The  result
     * of the final left rotation is shown in the lower third of the figure.
     * Five links (thick edges in figure 5) and three balance factors are to
     * be updated.
     *
     * As the figure shows, before an insertion, the leaf layer was at level
     * h+1, temporarily at level h+2 and after the double rotation again  at
     * level h+1. In case of a deletion, the leaf layer was at level h+2 and
     * after the double rotation it is at level h+1, so that the  height  of
     * the rotated tree decreases. figure 5:
     *     A<x<B<y<C<z<D                            A<x<B<y<C<z<D
     *           x                   x                    y
     *          / \                 / \                  / \
     *         /   \               /   \                /   \
     *        /     z     (1)     /     y      (2)     /     \
     *       +     / \    ==>    +     / \     ==>    x       z
     *      /A\   /   +         /A\   +   \          / \     / \
     *      ---  y   /D\        ---  /B\   z        +   +   +   +
     *          / \  ---             ---  / \      /A\ /B\ /C\ /D\
     *         /   \                     +   +     --- --- --- ---
     *        +     +                   /C\ /D\
     *       /B\   /C\                  --- ---
     *       ---   ---
     */
    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty returns the NULL
    node_t * bigRightLeftRotate(node_t * x) {
        DEBUG_FUNC1(x);
        x->linkRight(smallRightRotate(x->right()));  // (1) x.right == z => y
        return smallLeftRotate(x);                   // (2) x => y on top
    }

    // @param  x is a pointer to node with a disbalance factor
    // @return pointer to the root node of changed subtree
    //         if tagret set is empty returns the NULL
    node_t * bigLeftRightRotate(node_t * x) {
        DEBUG_FUNC1(x);
        x->linkLeft(smallLeftRotate(x->left()));
        return smallRightRotate(x);
    }

    // May be applied to any node of a binary search tree to restore balance
    // to the tree.
    // @param  x is a pointer to node for check a balance factor
    // @return pointer to the root node of subtree with restored balance
    //         if tagret set is empty returns the NULL
    node_t * avlRestoreBalance(node_t * x) {
        if (isNull(x)) return nullptr;
        DEBUG_FUNC1(x);
        updateNode(x);
        if (2 == getBalance(x)) {             // x is right-heavy
            // Rebalancing is required.
            if (getBalance(x->right()) < 0)   // Right Left Case (see figure 5)
            // Double rotation: Right(z) then Left(x)
                return bigRightLeftRotate(x);
            // Right Right Case (see figure 4)
            return smallLeftRotate(x);        // Single rotation Left(x)
        }
        if (-2 == getBalance(x)) {            // x is left-heavy
            // Rebalancing is required.
            if (0 < getBalance(x->left()))    // Left Right Case
            // Double rotation: Left(z) then Right(z)
                return bigLeftRightRotate(x);
            // Left Left Case
            return smallRightRotate(x);      // Single rotation Right(x)
        }
        return x; // Ничего делать не нужно. Возвращаем текущую ноду.
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Adds the key to the set if it is not already present.
    // @param  c (current) is a pointer to the root of current subtree
    // @param  found the boolean attribute for return
    // @param  k the key
    // @return pointer to the root of current subtree
    // @return found feature is set to true if key already present
    //         otherwise found is set to false
    node_t * add(node_t * c, bool & found, const T & k)
      throw(std::bad_alloc) {
        if (isNull(c)) return new node_t(k);
        DEBUG_FUNC3(c, found, k);

        if (k < c->key())
            c->linkLeft(add(c->left(), found, k));
        else if (c->key() < k)
            c->linkRight(add(c->right(), found, k));
        else
            found = true;

        return avlRestoreBalance(c); // назад по пути поиска и ...
    }

    // @param  current is a pointer to the root node of subtree
    // @param  parent is a pointer to the root of previous subtree,
    //         parent for node current
    // @return pointer to the root node of changed tree
    //         if tagret set is empty returns the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMinimum(node_t *& result, node_t * current) {
        if (isNull(current)) return nullptr;
        DEBUG_FUNC2(result, current);

        if (current->isLeftNull()) {
            result = current;
            if (current->isRightNull())
                return nullptr;
            node_t * right = current->right();
            current->unLinkRight();
            return right;
        }

        current->linkLeft(extractMinimum(result, current->left()));
        return avlRestoreBalance(current);
    }

    // Wrapper function for extractMinimum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with minimum key,
    //         if tagret set is empty returns the NULL
    node_t * extractMinimumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMinimum(result, fromRoot);
        return result;
    }

    // @param  current the pointer to root node of subtree
    // @return pointer to the root node of changed tree
    //         if tagret set is empty returns the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMaximum(node_t *& result, node_t * current) {
        if (isNull(current)) return nullptr;
        DEBUG_FUNC2(result, current);

        if (current->isRightNull()) {
            result = current;
            if (current->isLeftNull())
                return nullptr;
            node_t * left = current->left();
            current->unLinkLeft();
            return left;
        }

        current->linkRight(extractMaximum(result, current->right()));
        return avlRestoreBalance(current);
    }

    // Wrapper function for extractMaximum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with maximum key,
    //         if tagret set is empty returns the NULL
    virtual node_t * extractMaximumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMaximum(result, fromRoot);
        return result;
    }

    // @param  result is a pointer to extracted node,
    //         place for returning node
    // @param  current is a pointer to root node of subtree
    // @param  key the key to extracting
    // @return pointer to the root of current subtree
    //         if tagret set is empty returns the NULL
    // @return result is a pointer indicates the found and extracted node
    //         if key not found or if tagret set is empty result unchanged!
    node_t * extractNode(node_t *& result, node_t * current, const T & key) {
        if (isNull(current)) return nullptr;
        DEBUG_FUNC3(result, current, key);

        if (key < current->key()) {
            current->linkLeft(  extractNode(result, current->left(), key) );
        } else if (current->key() < key) {
            current->linkRight( extractNode(result, current->right(), key) );
        }   // (3)
        else if (current->left() != nullptr and current->right() != nullptr ) {
            result = current;
            node_t * left  = result->left();
            node_t * right = result->right(); // result->right() Must be change!
            current = extractMinimumFrom(right);                  // after this
            result->unLink();
            current->link(left, right);
        } else {
            // (1), (2)
            if (not current->isLeftNull()) {
                result = current;
                current = current->left();
                result->unLinkLeft();
            } else {
                result = current;
                current = current->right();
                // if (result == root) ???
                //     root = current; ???
                result->unLinkRight();
            }
        }

        return avlRestoreBalance(current);
    }

    // Wrapper function for extractNode.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @param  key the key to extracting
    // @return the pointer to the node with key, if key not found or if
    //         tagret set is empty returns the NULL
    node_t * extractNodeFrom(node_t *& fromRoot, const T & key) {
        node_t * result = nullptr;
        root = extractNode(result, root, key);
        return result;
    }

    node_t * avlSubTreeCheck(node_t * c) {
        if (nullptr == c)
            return nullptr;
        node_t * l = avlSubTreeCheck(c->left());
        if (nullptr != l) return l;
        int8_t lh = leftHight(c);
        int8_t rh = rightHight(c);
        if (1 < std::abs(lh - rh))
            return c;
        return avlSubTreeCheck(c->right());
    }

    node_t * orderStatistics(node_t * current, size_t index) const {
        if (isNull(current)) return nullptr;
        size_t subIdx = leftSize(current) + 1;
        if (index == subIdx) return current;
        if (index <  subIdx)
            return orderStatistics(current->left(), index);
        else
            return orderStatistics(current->right(), index - subIdx);
        return nullptr;
    }

    node_t * avlMergeWithRoot(node_t * v1, node_t * v2, node_t * t) {
        if (isNull(t)) return nullptr;

        if (std::abs(hight(v1) - hight(v2)) <= 1) {
            bstree_t::mergeWithRoot(v1, v2, t);
            updateNode(t);
            return t;
        } else if (hight(v2) < hight(v1)) {
            node_t * t1 = avlMergeWithRoot(v1->right(), v2, t);
            if (isNotNull(v1)) v1->linkRight(t1);
            return avlRestoreBalance(v1);
        } else if (hight(v1) < hight(v2)) {
            node_t * t2 = avlMergeWithRoot(v1, v2->left(), t);
            if (isNotNull(v2)) v2->linkLeft(t2);
            return avlRestoreBalance(v2);
        }
        return nullptr;
    }

    node_t * avlSubMerge(node_t * v2) {
        node_t * t = extractMaximumFrom(root);
        return avlMergeWithRoot(root, v2, t);
    }

    void avlSubSplit(node_t *& res1, node_t *& res2, node_t * v, const T & k) {
        if (isNull(v)) return;

        if (k < v->key()) {
            node_t * v1 = nullptr, * v2 = nullptr;
            avlSubSplit(v1, v2, v->left(), k);
            res1 = v1;
            res2 = avlMergeWithRoot(v2, v->right(), v);
        } else {
            node_t * v1 = nullptr, * v2 = nullptr;
            avlSubSplit(v1, v2, v->right(), k);
            res1 = avlMergeWithRoot(v->left(), v1, v);
            res2 = v2;
        }
    }

    void checkRootAndIndex(size_t i, int l, const char * f) const
      throw(std::range_error) {
        if (nullptr == root || root->size() <= i || (SIZE_MAX - 2) < i)
            throw std::range_error("AVL Range Error Index at: " +
                  std::to_string(l) + std::string(" in file: ") + f);
    }
};

template<typename T,class N> bool
operator==(const svn::AVLTree<T,N> & left, const svn::AVLTree<T,N> & right) {
    return left.equals(right);
}

template<typename T, class N> bool
operator!=(const svn::AVLTree<T,N> & left, const svn::AVLTree<T,N> & right) {
    return !(left == right);
}

} // namespace svn

#endif // _AVLTREE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
