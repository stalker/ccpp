////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 1.1$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////
// Compilation:
// Execution:
// Dependencies: node.hpp basictree.hpp

#ifndef _IBSTREE_HPP_
#define _IBSTREE_HPP_

#include <iomanip>
#include <iostream>
#include <stack>
#include <sstream>

#include "node.hpp"
#include "basictree.hpp"
#include "ifacebst.hpp"

#undef DEBUG
#define DEBUG 3
#ifdef DEBUG
# include "mydebug.hpp"
#endif

namespace svn {

template<typename T, class NODE>
class IBSTreeIterator;

////////////////////////////////////////////////////////////////////////////////
// Iterative Binary Search Tree class.
//
// The IBSTree class represents an ordered set of comparable keys. It supports
// the usual add, contains, and delete methods.   It  also  provides  ordered
// methods for finding the minimum,  maximum,  floor,  and  ceiling  and  set
// methods for union, intersection, and equality.
//
// Even though this implementation include the method equals(), it  does  not
// support the method hashCode() because sets are mutable.
//
// The insert (add), find (contains),  remove  (delete),  findMin  (minimum),
// findMax (maximum), next, previous  and  remove  methods  take  linear time
// in the worst case.
//
// The isEmpty operations take constant time.
// Standart Construction takes constant time.
// Explicit Construction takes constant time.
// Clone operation takes linear time.
// Verification (isBinarySearchTree) takes linear time.
//
//  @author Victor Skurikhin
//
template<typename T, class NODE = StoreNode<T> >
class IBSTree : public BasicT<NODE>, virtual public IfaceBSTree<T> {
    friend class IBSTreeIterator<T, NODE>;
    template <typename Y, class N>
    friend bool operator==(const IBSTree<Y,N> &, const IBSTree<Y,N> &);
    template <typename Y, class N>
    friend bool operator!=(const IBSTree<Y,N> &, const IBSTree<Y,N> &);

    // An iterative function to do postorder traversal of a given binary tree
    void postOrderIterativeFree() {
        // Check for empty tree
        if (isNull(root)) return;
        node_t * current = root;
        node_t * lastNodeVisited = nullptr;
        std::stack<node_t *> stack;
        while (not stack.empty() or isNotNull(current)) {
            if (isNotNull(current)) {
                stack.push(current);
                current = current->left();
            } else {
                node_t * peekNode = stack.top();
                // if right child exists and traversing node
                // from left child, then move right
                if (not peekNode->isRightNull()
                    and lastNodeVisited != peekNode->right())
                    current = peekNode->right();
                else {
                    // visit peekNode nodeOutDetail(std::cerr, peekNode)
                    // << std::endl;
                    peekNode->unLink();
                    delete peekNode;
                    peekNode = nullptr;
                    // end visit current
                    lastNodeVisited = stack.top(); stack.pop();
                }
            }
        }
        root = nullptr;
    }
public:
    typedef NODE node_t;
    typedef BasicT<NODE> basict_t;
    typedef IBSTreeIterator<T, NODE> iterator;
    typedef IBSTreeIterator<T, const NODE> const_iterator;
    using basict_t::root;
    using basict_t::detailOut;

    ////////////////////////////////////////////////////////////////////////////

    IBSTree()          { /* None */ } // Initializes an empty set.
    virtual ~IBSTree() { postOrderIterativeFree(); } // Virtual destructor.

    // Copy constructor and operator-function =.
    IBSTree(const IBSTree & other) : basict_t(other) { /* None */ }
    IBSTree & operator=(const IBSTree & rhs) {
        if (&rhs == this) return *this;
        auto newRoot = rhs.clone();
        postOrderIterativeFree(); // delete root;
        root = newRoot;
        return *this;
    }

    IBSTree(IBSTree && other) { basict_t::move(std::move(other)); }
    IBSTree & operator=(IBSTree && o) { return basict_t::operator=(o); }

    explicit IBSTree(node_t * p) : basict_t(p) { /* None */ }
    ////////////////////////////////////////////////////////////////////////////
    //
    // Adds the key to the set if it is not already present.
    // @param key the key to add
    // @return false if the set contains key and true otherwise
    // @throws std::bad_alloc if can't allocated a memory
    virtual bool insert(const T & key) throw(std::bad_alloc) {
        bool found = false;
        root = add(root, found, key);
        return not found;
    }

    // Does the set contain the given key?
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool find(const T & key) const {
        return isNull(searchNode(root, key)) ? false : true;
    }

    // Returns the smallest key in the set.
    // @return the smallest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMin() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return searchMinimum(root)->key();
    }

    // Returns the largest key in the set.
    // @return the largest key in the set
    // @throws std::range_error if the set is empty
    virtual const T & findMax() const throw(std::range_error) {
        basict_t::checkRoot(__LINE__, __FILE__);
        return searchMaximum(root)->key();
    }

    // Returns  the smallest key in  the set greater than  to key  or if this
    // set is empty returns the key.
    // @param key the key
    // @return the next key in the set greater than to key
    virtual const T & next(const T & key) const {
        node_t * successor = nextNode(key);
        return isNull(successor) ? key : successor->key();
    }

    // Returns the largest key in the set less than or equal to key.
    // @param key the key
    // @return the largest key in the set table less than or equal to key
    virtual const T & previous(const T & key) const {
        node_t * predecessor = prevNode(key);
        return isNull(predecessor) ? key : predecessor->key();
    }

    // Does the set contain the given key?
    // @param key the key
    // @return a pointer to the node_t that contains key
    const node_t * const searchNode(const T & key) const {
        return searchNode(root, key);
    }

    // @param key the key to extracting
    // @return a pointer to the extracted (by key) node,
    //         if tagret set is empty return the NULL
    node_t * extractNode(const T & key) { return extractNodeFrom(root, key); }
    node_t * extractMinimum()           { return extractMinimumFrom(root); }
    node_t * extractMaximum()           { return extractMaximumFrom(root); }

    size_t size()          const { return root->size(); }
    virtual bool isEmpty() const { return isNull(root); } // Is the set empty?

    // Removes the key from the set if the key is present.
    // @param key the key
    // @return true if the set contains key and false otherwise
    virtual bool remove(const T & key) {
        bool found = false;
        const node_t * deletedNode = extractNode(key);
        found = isNull(deletedNode) ? false : true;
        delete deletedNode;
        return found;
    }

    // Does this IBSTree equal to other and the structure coincides  to other?
    // @param  o the other BStree
    // @return true  if  the two sets  are equal and structure  of both trees
    //         equal; false otherwise
    bool coincides(const IBSTree & o) const {
        if (isNull(root))
            return isNull(o.root) ? true : false;
        return coincides(root, o.root);
    }

    // Does this IBSTree equals o?
    // @param o the other BStree
    // @return true if the two sets are equal; false otherwise
    bool equals(const IBSTree & o) const {
        iterator it = const_cast<IBSTree &>(o).begin();
        return (isNull(root) and isNotNull(it)) ? false : equals(root, it);
    }

    // Does this binary tree satisfy symmetric order?
    // Note: this test also ensures that data structure is a binary tree since
    // order is strict
    virtual bool isBinarySearchTree() const {
        return binarySearchTreeCheck(std::numeric_limits<T>::lowest(),
                                     std::numeric_limits<T>::max());
    }

    bool binarySearchTreeCheck(T min, T max) const {
        return isNull(getIncorrectNodeInSearchTree(root, min, max));
    }

    // @param  c is a pointer to the root node of subtree
    // @param  min
    // @param  max
    const node_t * getIncorrectNodeInSearchTree(node_t * c, T min, T max) const
    {
        // Check for empty tree
        if (isNull(c)) return nullptr;
        node_t * current = root;
        node_t * lastNodeVisited = nullptr;
        std::stack<node_t *> stack;
        while (not stack.empty() or isNotNull(current)) {
            if (isNotNull(current)) {
                stack.push(current);
                current = current->left();
            } else {
                node_t * peekNode = stack.top();
                // if right child exists and traversing node
                // from left child, then move right
                if (not peekNode->isRightNull()
                    and lastNodeVisited != peekNode->right())
                    current = peekNode->right();
                else {
                    // visit peekNode nodeOutDetail(std::cerr, peekNode)
                    // << std::endl;
                    if (peekNode->key() <= min or max <= peekNode->key())
                        return peekNode;
                    else
                        min = peekNode->key();
                    // end visit current
                    lastNodeVisited = stack.top(); stack.pop();
                }
            }
        }
        return nullptr;
    }

    /**
     * The Join proceudre takes this Binary Search  Tree  and  other  Binary
     * Search Tree as input, such that o, and any keyword in  this  is  less
     * than any keyword in o.  It unions of this and  other.   Other  Binary
     * Search Tree o will be empty.
     */
    // @param  o is a other IBSTree class
    virtual void toMerge(IBSTree & o) {
        root = merge(root, o.root);
        o.root = nullptr;
    }

    // @param  key the key
    // @return b is a IBSTree class
    // @return c is a IBSTree class
    virtual void splitTo(IBSTree & b, IBSTree & c, const T & key) {
        if (isNotNull(b.root) or isNotNull(c.root)) return;
        split(b.root, c.root, root, key);
        root = nullptr;
    }
    // Returns a stream representation of this set.
    // @return a stream representation of this set, with the keys separated
    virtual std::ostream & treeOutInOrder(std::ostream & os) const {
        size_t index = 0;
        const char * sep = detailOut ? nl : sp;
        inOrderIterativeOut(os, root, index, sep);
        return os;
    }

    ////////////////////////////////////////////////////////////////////////////

    iterator begin() { return iterator(*this, searchMinimum(root)); }
    iterator end()   { return iterator(*this, nullptr); }

    const_iterator begin() const {
        return iterator(*this, searchMinimum(root));
    }
    const_iterator end() const { return iterator(*this, nullptr); }

    iterator rbegin() { return iterator(*this, searchMaximum(root)); }
    iterator rend()   { return iterator(*this, nullptr); }

protected:
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Insertion begins as a search would begin; if the key is not equal  to
     * that of the root, we search the left or  right  subtrees  as  before.
     * Eventually, we will reach an external node and add the new  key-value
     * pair (here encoded as a record 'new node_t(k)') as its right  or left
     * child, * depending on the node's key.  In other words, we examine the
     * root and recursively insert the new node to  the left  subtree if its
     * key is less  than  that of  the root, or  the  right  subtree  if its
     * key is greater than or equal to the root.
     * The part that is rebuilt uses O(log n) space in  the average case and
     * O(n) in the worst case.
     */
    // Adds the key to the set if it is not already present.
    // @param  c (current) is a pointer to the root of current subtree
    // @param  found the boolean attribute for return
    // @param  k the key
    // @return pointer to the root of current subtree
    // @return found feature is set to true if key already present
    //         otherwise found is set to false
    node_t * add(node_t * c, bool & found, const T & k) throw(std::bad_alloc) {
        if (isNull(c)) return new node_t(k);

        node_t * parent = nullptr, * x = c;
        while (isNotNull(x)) {
            parent = x;
            if (k < x->key())
                x = x->left();
            else if (x->key() < k)
                x = x->right();
            else {
                found = true;
                return c;
            }
        }
        if (k < parent->key())
            parent->linkLeft(new node_t(k));
        else
            parent->linkRight(new node_t(k));
        return c;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * We begin by examining the root node.  If the tree is null, the key we
     * are searching for does not exist in the tree.  Otherwise, if the  key
     * equals that of the root, the search is successful and we  return  the
     * node.  If the key is less than that of the root, we search  the  left
     * subtree.  Similarly, if the key is greater than that of the root,  we
     * search the right subtree.  This process is repeated until the key  is
     * found or the remaining subtree is null.  If the searched key  is  not
     * found after a null subtree is reached, then the key is not present in
     * the tree.
     * On average, binary search trees with n nodes have O(log n) height.
     * However, in the worst case, binary search trees can have O(n) height.
     *
     * Reference:
     *  http://www.cs.cmu.edu/~pattis/15-1XX/15-200/lectures/treeprocessing/
     */
    // Does the set contain the given key?
    // @param  current is a pointer to root node of subtree
    // @param  key the key
    // @return pointer to the searched node,  if tagret set is empty  return 
    //         the NULL and if node with key doesn't exist return NULL
    node_t * searchNode(const node_t * current, const T & key) const {
        node_t * c = const_cast<node_t *>(current);
        for (; isNotNull(c); c = key < c->key() ? c->left() : c->right()) {
            if (key == c->key())
                return c;
        }

        return nullptr;
    }

    /* Just traverse the node from root  to left  recursively  until  left is
     * NULL. The node whose left is NULL is the node with minimum value.
     */
    // @param  current is a pointer to root node of subtree
    // @return a pointer to the node  with minimum,
    //         if tagret set is empty return the NULL
    node_t * searchMinimum(const node_t * current) const {
        // if (isNull(current)) return nullptr;
        node_t * c = const_cast<node_t *>(current), * result = nullptr;
        for (; isNotNull(c); c = c->left())
            result = c;
        return result;
    }

    /* Just traverse the node from root  to right recursively  until right is
     * NULL. The node whose right is NULL is the node with maximum value.
     */
    // @param  current is a pointer to the root node of subtree
    // @return a pointer to the node with maximum,
    //         if tagret set is empty return the NULL
    node_t * searchMaximum(const node_t * current) const {
        // if (isNull(current)) return nullptr;
        node_t * c = const_cast<node_t *>(current), * result = nullptr;
        for (; isNotNull(c); c = c->right())
            result = c;
        return result;
    }

    // Finding the successor node of a node with key.
    // @param  key the key
    // @return a pointer to the node with successor key,
    //         if tagret set is empty return the NULL
    node_t * nextNode(const T & key) const {
        node_t * current = root, * successor = nullptr;

        while (isNotNull(current)) {
            if (key < current->key()) {
                successor = current;
                current = current->left();
            } else
                current = current->right();
        }

        return successor;
    }

    // Finding the successor node of a given node.
    // @param  x is a pointer to given node
    // @return a pointer to the node with successor key,
    //         if tagret set is empty return the NULL
    node_t * nextNode(const node_t * x) const {
        if (isNull(x)) return nullptr;
        return nextNode(x->key());
    }

    // Finding the predecessor node of a node with key.
    // @param  key the key
    // @return the pointer to the node with predecessor key,
    //         if tagret set is empty return the NULL
    node_t * prevNode(const T & key) const {
        node_t * current = root, * predecessor = nullptr;

        while (isNotNull(current)) {
            if (current->key() < key) {
                predecessor = current;
                current = current->right();
            } else
                current = current->left();
        }

        return predecessor;
    }

    // Finding the predecessor node of a given node.
    // @param x is a pointer to a given node
    // @return the pointer to the node with predecessor key,
    //         if tagret set is empty return the NULL
    node_t * prevNode(const node_t * x) const {
        if (isNull(x)) return nullptr;
        return prevNode(x->key());
    }

    /**
     * Just traverse the node from root  to left  recursively  until  left is
     * NULL. The node whose left is NULL is the node with minimum value.
     * The  procedure   for  deleting   the  minimum  of  from   the  subtree
     * (effectively extracting the minimum element)
     */
    // @param  current is a pointer to the root node of subtree
    // @param  parent is a pointer to the root of previous subtree,
    //         parent for node current
    // @return pointer to the root node of changed tree
    //         if tagret set is empty return the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMinimum(node_t *& result, node_t * current) {
        result = current;
        if (isNull(current)) return nullptr;
        node_t * parent = nullptr;

        if (result->isLeftNull()) {
            if (result->isRightNull())
                return nullptr;
            else {
                node_t * right = result->right();
                result->unLinkRight();
                return right;
            }
        } else do {
            parent  = result;
            result  = result->left();
        } while (not result->isLeftNull());

        if (result->isRightNull())
            parent->unLinkLeft();
        else {
            parent->linkLeft(result->right());
            result->unLinkRight();
        }

        return current;
    }

    // Wrapper function for extractMinimum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with minimum key,
    //         if tagret set is empty return the NULL
    node_t * extractMinimumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMinimum(result, fromRoot);
        return result;
    }

    /**
     * Just traverse the node from root  to right  recursively until right is
     * NULL. The node whose right is NULL is the node with maximum value.
     * Time Complexity: O(n) Worst case happens for right skewed trees.
     */
    // @param  current the pointer to root node of subtree
    // @return pointer to the root node of changed tree
    //         if tagret set is empty return the NULL
    // @return result is a pointer to the found and extracted node
    //         place for returning node
    node_t * extractMaximum(node_t *& result, node_t * current) {
        result = current;
        if (isNull(current)) return nullptr;
        node_t * parent = nullptr;

        if (result->isRightNull()) {
            if (result->isLeftNull())
                return nullptr;
            else {
                node_t * left = result->left();
                result->unLinkLeft();
                return left;
            }
        } else do {
            parent  = result;
            result  = result->right();
        } while (not result->isRightNull());

        if (result->isLeftNull())
            parent->unLinkRight();
        else {
            parent->linkRight(result->left());
            result->unLinkLeft();
        }

        return current;
    }

    // Wrapper function for extractMaximum.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @return the pointer to the node with maximum key, 
    //         if tagret set is empty return the NULL
    node_t * extractMaximumFrom(node_t *& fromRoot) {
        node_t * result = nullptr;
        fromRoot = extractMaximum(result, fromRoot);
        return result;
    }

    /**
     * When removing a node from a binary search tree  it  is  mandatory  to
     * maintain  the  in-order  sequence  of  the  nodes.   There  are  many
     * possibilities to do this.  However, the following  method  which  has
     * been proposed by T.  Hibbard in 1962 guarantees that the  heights  of
     * the subject subtrees are changed by at most  one.   There  are  three
     * possible cases to consider:
     * (1) Deleting  a node with  no children:  simply remove  the node from 
     *     the tree.
     * (2) Deleting  a node with one child:  remove the node  and replace it
     *     with its child.
     * (3) Deleting a node with two children: call the node to be deleted D.
     *     Do  not  delete  D.    Instead,  choose   either   its   in-order
     *     predecessor node or its in-order successor  node  as  replacement
     *     node E.  Copy the user values of E to D.  If E does  not  have  a
     *     child simply remove E from its previous parent G.   If  E  has  a
     *     child, say F, it is a right child. Replace E with F at E's
     *     parent.
     *                  D                  E                  E
     *                 / \                / \                / \
     *                /   +              /   +              /   +
     *               /   / \            /   / \            /   / \
     *              /   G  ...  ==>    /   G  ...  ==>    /   G  ...
     *             +   / \            +     \            +   / \
     *            / \ E   \          / \     \          / \ F   \
     *            ---  \   +         ---  F   +         ---      +
     *                  F / \                / \                / \
     *                    ---                ---                ---
     */
    // @param  result is a pointer to extracted node,
    //         place for returning node
    // @param  current is a pointer to root node of subtree
    // @param  key the key to extracting
    // @return pointer to the root of current subtree
    //         if tagret set is empty return the NULL
    // @return result is a pointer indicates the found and extracted node
    //         if key not found or if tagret set is empty result unchanged!
    node_t * extractNode(node_t *& result, node_t * peak, const T & key) {
        result = peak;
        if (isNull(peak)) return nullptr;
        node_t * parent = nullptr;

        do {
            if (key == result->key()) {
                if (result->left() != nullptr and result->right() != nullptr) {
                    node_t * left  = result->left();  // (3)
                    node_t * right = result->right(); // result->right() Must be
                    node_t * current = extractMinimumFrom(right); // change!
                    result->unLink();
                    current->link(left, right);
                    if (isNull(parent))
                        return current;
                    parentReLink(parent, result, current);
                } else if (result->isLeftNull()) {         // (1), (2)
                    node_t * right = result->right();
                    result->unLinkRight();
                    if (isNull(parent))
                        return right;
                    parentReLink(parent, result, right);
                } else {
                    node_t * left  = result->left();
                    result->unLinkLeft();
                    if (isNull(parent))
                        return left;
                    parentReLink(parent, result, left);
                }

                return peak;
            }
            parent  = result; // Search the parent and result
            result = key < result->key() ? result->left() : result->right();
        } while (isNotNull(result));

        return peak;
    }

    // Wrapper function for extractNode.
    // @param  fromRoot the place of the pointer to root node of subtree
    //         place for returning node
    // @param  key the key to extracting
    // @return the pointer to the node with key, if key not found or if
    //         tagret set is empty return the NULL
    node_t * extractNodeFrom(node_t *& fromRoot, const T & key) {
        node_t * result = nullptr;
        root = extractNode(result, root, key);
        return result;
    }

    // @param  l the pointer to root node of first subtree
    // @param  r the pointer to root node of second subtree
    // @return true if the two sets are equal and the structure of both trees
    //         is the same; false otherwise
    bool coincides(const node_t * l, const node_t * r) const { //TODO
        if (isNull(l))
            return isNull(r) ? true : false;
        if (isNull(r)) return  false;
        return coincides(l->left(),  r->left())
           and l->key() == r->key()
           and coincides(l->right(), r->right());
    }

    // Does this set equals to other?
    // @param  current the pointer to root node of subtree
    // @param  it the iterator from other set
    // @return true if the two sets are equal; false otherwise
    bool equals(const node_t * c, iterator & it) const { //TODO
        if (isNull(it))
            return isNull(c) ? true : false;
        if (isNull(c)) return  true;
        return equals(c->left(), it)
           and c->key() == it++->key()
           and equals(c->right(), it);
    }

    // @param  v1 is a pointer to top (root) node of left tree
    // @param  v2 is a pointer to top (root) node of right tree
    // @param  t is a pointer to the root node of the unifying tree
    // @return a pointer to the root of merged tree
    node_t * mergeWithRoot(node_t * v1, node_t * v2, node_t * t) {
        if (isNull(t)) return nullptr;
        t->linkLeft(v1);
        t->linkRight(v2);
        return t;
    }

    /**
     * The Join proceudre takes v1 and v2 as input, such that v1 and v2  are
     * Binary Search trees, and any keyword in v1 is less than  any  keyword
     * in v2. It returns a Binary tree T that is the union of v1 and v2.  We
     * claim that this can be done in O(|ht(T1) − ht(T2)|), where ht denotes
     * the height.
     */
    // @param  v1 is a pointer to top (root) node of left tree
    // @param  v2 is a pointer to top (root) node of right tree
    // @return a pointer to the root of merged tree
    node_t * merge(node_t * v1, node_t * v2) {
        if (isNull(v1)) return v2;
        node_t * t = extractMaximumFrom(v1);
        t = mergeWithRoot(v1, v2, t);
        return t;
    }

    /**
     * The Split procedure takes T and k as inputs, such that T is an Binary
     * Search tree, and k is a keyword of T.  It returns Binary Search trees
     * T1, T2, such that T1 contains the elements that is less than k in  T,
     * and T2 contains those larger than k.  We claim that this can be  done
     * in O(ht(T)).
     * The algorithm to split would in general not only need to cut  an edge
     * (making  two  trees), but  also  repeat  this  at  deeper  levels  of 
     * the cut-off tree, since there  may be subtrees there  that  should be 
     * attached at the place the cut-off happened.
     */
    // @param  v is a pointer to root node of tree
    // @param  key the keyword
    // @return result1 a pointer to the root of left tree
    // @return result2 a pointer to the root of right tree
    void split(node_t *& result1, node_t *& result2, node_t * v, const T & key)
    {
        if (isNull(v)) return;
        // DEBUG2_FUNC4(result1, result1, v, key);

        node_t * root1 = v;
        node_t * root2 = nullptr;
        node_t * parent1 = nullptr;
        node_t * parent2 = nullptr;

        // Determine at which side of the root we will travel
        bool toLeft = key < root1->key();
        // DEBUG2_FUNC2("toLeft = ", toLeft);

        while (isNotNull(root1)) {
            // DEBUG2_FUNC3("while ( root1 = ", root1, ")");
            while (isNotNull(root1) && (key < root1->key()) == toLeft) {
                // DEBUG2_FUNC4("while ... key < [root1->key() = ", root1->key(), "] == ", toLeft);
                parent1 = root1;
                root1 = toLeft ? root1->left() : root1->right();
                // DEBUG2_FUNC4("parent1 = ", parent1, " parent1->key = ", keyToString(parent1));
                // DEBUG2_FUNC4("root1   = ", root1,   "   root1->key = ", keyToString(root1));
            }

            // DEBUG2_FUNC3("Cut out the edge parent1 = ", parent1, toLeft);
            // Cut out the edge. root is now detached
            setChild<node_t>(parent1, toLeft, nullptr);
            toLeft = not toLeft; // toggle direction

            // DEBUG2_FUNC2("root1 = ", root1);
            if (isNull(root2)) {
                // DEBUG2_FUNC1("root2 = root1");
                root2 = root1;   // This is the root of the other tree.
            } else if (isNotNull(parent2)) {
                // re-attach the detached subtree
                setChild(parent2, toLeft, root1);
                // DEBUG2_FUNC4("parent2 = ", parent2, " toLeft = ", toLeft);
            }

            parent2 = parent1; // Sift down.
            parent1 = nullptr; // Because they cut off.
            // DEBUG2_FUNC4("parent1 = ", parent1, " parent1->key = ", keyToString(parent1));
            // DEBUG2_FUNC4("root1   = ", root1,   "   root1->key = ", keyToString(root1));
            // DEBUG2_FUNC4("parent2 = ", parent2, " parent2->key = ", keyToString(parent2));
            // DEBUG2_FUNC4("root2   = ", root2,   "   root2->key = ", keyToString(root2));
        }

        if (root1 == root2) {
            if ((toLeft = (key < v->key()))) {
                result1 = nullptr;
                result2 = v;
            } else {
                result1 = v;
                result2 = nullptr;
            }
        } else if (toLeft) {
                result1 = root2;
                result2 = v;
        } else {
                result1 = v;
                result2 = root2;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Returns a string representation of key.
    // @return a string representation of key
    virtual std::string keyToString(const node_t * current) const {
        return nodeKeyToString<node_t>(current);
    }

    // Returns a string representation of node_t.
    // @return a string representation of node_t
    virtual std::string nodeToString(const node_t * current) const {
        return nodeKeyToString<node_t>(current);
    }

    ////////////////////////////////////////////////////////////////////////////
    virtual void subTreeOutInOrder(
      std::ostream & os, const node_t * c, size_t & i, const char * sep
      ) const {
        inOrderIterativeOut(os, const_cast<node_t *>(c), i, sep);
    }

    void inOrderIterativeOut(
      std::ostream & os, node_t * current, size_t & i, const char * sep
      ) const {
        // Check for empty tree
        if (isNull(current)) return;
        /* Initialize stack */
        std::stack<node_t *> stack;
        while (not stack.empty() or isNotNull(current)) {
            if (isNotNull(current)) {
                stack.push(current);
                current = current->left();
            } else {
                current = stack.top(); stack.pop();
                // begin visit current
                if (detailOut) {
                    nodeOutDetail(os, current);
                    os << " index:" << i << sep;
                } else if (0 == i)
                    os << keyToString(current);
                else
                    os << sep << keyToString(current);
                i++;
                // end visit current
                current = current->right();
            }
        }
    }
};

} // namespace svn

#endif // _BSTREE_HPP_
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
