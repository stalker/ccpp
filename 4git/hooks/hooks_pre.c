/* hooks_pre.c */
/* $Date$ //
 * $Id$                          //
 * $Version: 0.1.6$                                                         //
 * $Revision: 5$                                                            //
 * $Author: Victor Skurikhin <stalker@quake.ru>$                            //
 */

#define DEBUG 5
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "hooks_pre.h"
#ifdef WIN32
# include <process.h>
# if defined(_MSC_VER)
#  ifndef __func__
#   define __func__ __FUNCTION__
#  endif
#  ifndef snprintf
#   define snprintf(buf,len,fmt,...) _snprintf_s(buf,len,len,fmt,__VA_ARGS__)
#  endif
# endif
#else
# include <stdlib.h>
# include <unistd.h>
#endif

#if defined(__DMC__)
# error Don't adopt for __DMC__
#endif

char build_date[] = "__DATE__";

int is_err_print(void);
int FileExists(const char *path);
int FileExistsA(const char *path);

typedef struct _arg {
    int argc;
    char ** argv;
} Targ;
Targ arg;

#ifdef WIN32
char pre_cmd[] = "sh -c \"find . -type f "
                 "\\( -name '*.c' -or -name '*.h' \\) "
                 "| ./hooks_fil | ./hooks_rev | ./hooks_sed\"";
char fmt_fil_cmd[] = "sh -c \"exit $(git diff %s|wc -l)\"";
char fmt_rev_cmd[] = "cmd /c 'printf \"%s %%d\\n\" "
                     "\"$(git log -- %s|grep ^commit|wc -l)\"'";
char fmt_sed_cmd1[] = "copy %s %s_%d.bak";
char fmt_sed_cmd2[] = "sed -e 's/%s %s/' %s_%d.bak > %s";
char hooks_sed_pat[] = "\\($Revision:\\) \\+[0-9]\\+/\\1";
#else
char pre_cmd[] = "sh -c \"find . -type f "
                 "\\( -name '*.c' -or -name '*.h' \\) "
                 "| ./hooks_fil | ./hooks_rev | ./hooks_sed\"";
char fmt_fil_cmd[] = "sh -c \"exit $(git diff %s|wc -l)\"";
char fmt_rev_cmd[] = "sh -c 'printf \"%s %%d\\n\" "
                     "\"$(git log -- %s|grep ^commit|wc -l)\"'";
char fmt_sed_cmd1[] = "cp %s %s_%d.bak";
char fmt_sed_cmd2[] = "sed -b -e 's/%s %s/' %s_%d.bak > %s;";
char hooks_sed_pat[] = "\\($Revision:\\) \\+[0-9]\\+/\\1";
#endif

int main(int argc, char ** argv) {
    Tint result, len, i, n;
#ifdef WIN32
    setvbuf(stdout, NULL, _IONBF, 0);
    result = setmode(fileno(stdin), O_BINARY );
    if (result == -1)
       fprintf(stderr, "Cannot set mode" );
#if defined(DEBUG) && DEBUG > 2
    else
       fprintf(stderr, "'stdin' successfully changed to binary mode\n" );
#endif
    result = setmode(fileno(stdout), O_BINARY );
    if (result == -1)
       fprintf(stderr, "Cannot set mode" );
#if defined(DEBUG) && DEBUG > 2
    else
       fprintf(stderr, "'stdout' successfully changed to binary mode\n" );
#endif
#endif
    prog = basename(argv[0]);
    arg.argc = argc;
    arg.argv = argv;
    len = strlen(argv[0]);

    for (n = 0; n < MY_FUNCS_NKEYS; n++) {
#if defined(DEBUG) && DEBUG > 2
        fprintf(stderr, "%s: ", my_funcs[n].name);
#endif
        if (0 == strncmp(prog, my_funcs[n].name, len)) {
#if defined(DEBUG) && DEBUG > 2
            fprintf(stderr, " Ok found: %p\n", my_funcs[n].pfunc);
#endif
            my_funcs[n].pfunc();
        }
#if defined(DEBUG) && DEBUG > 2
        else
            fputs(" Next", stderr);
        putc('\n', stderr);
#endif
    }

    return 0;
}

/* имя: hooks_pre
 */
int hooks_pre(void) {
#if defined(DEBUG) && DEBUG > 2
    fprintf(stderr, "Ok\n");
    fprintf(stderr, "%s:%s:\n", __FILE__, __func__);
#endif
    int ret = system(pre_cmd);

    return true;
}

/* имя: hooks_fil
 */
int hooks_fil(void) {
#if defined(DEBUG) && DEBUG > 2
    fprintf(stderr, "Ok\n");
    fprintf(stderr, "%s:%s:\n", __FILE__, __func__);
#endif
    int read;
    char buf[1024];
	char line[1024];
	while ((read = my_getline(line, 1024)) != -1) {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';
        sprintf(buf, fmt_fil_cmd, line);
#if defined(DEBUG) && DEBUG > 2
        fprintf(stderr, "system: %s\n", buf);
#endif
        int rc = system(buf);
        if (rc) {
#if defined(DEBUG) && DEBUG > 2
            fprintf(stderr, "Retrieved line of length %d :\n", read);
            fprintf(stderr, "rc: %d\n", rc);
#endif
            printf("%s\n", line);
        }
    }
    return true;
}

/* имя: hooks_rev
 */
int hooks_rev(void) {
#if defined(DEBUG) && DEBUG > 2
    fprintf(stderr, "Ok\n");
    fprintf(stderr, "%s:%s:\n", __FILE__, __func__);
#endif
    int read;
    char buf[1024];
	char line[1024];
	while ((read = my_getline(line, 1024)) != -1) {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';
		snprintf(buf, 1024, fmt_rev_cmd, line, line);
#if defined(DEBUG) && DEBUG > 2
        fprintf(stderr, "system: %s\n", buf);
#endif
        int rc = system(buf);
        if (!rc) {
#if defined(DEBUG) && DEBUG > 2
            fprintf(stderr, "Retrieved line of length %d :\n", read);
            fprintf(stderr, "rc: %d\n", rc);
#endif
        }
    }
    return true;
}

/* имя: hooks_sed
 */
int hooks_sed(void) {
    Tint i, fd;
#ifdef WIN32
    Tint pid = _getpid();
#else
    Tint pid = getpid();
#endif
#if defined(DEBUG) && DEBUG > 2
    fprintf(stderr, "%s:%s:\n", __FILE__, __func__);
#endif
	char line[1024];
    char buf[1024];
	int read;
	while ((read = my_getline(line, 1024)) != -1) {
        char *pos;
        if ((pos = strchr(line, '\n')) != NULL)
            *pos = '\0';
        if ((pos = strchr(line,  ' ')) != NULL) {
            *pos = '\0';
            pos++;
        }
		if (!FileExistsA(line)) {
            if (is_err_print()) {
                fprintf(stderr, "%s errno: %d = 0x%X\n", line, errno, errno);
            }
        } else {
			snprintf(buf, 1024, fmt_sed_cmd1, line, line, pid);
#if defined(DEBUG) && DEBUG > 2
			fprintf(stderr, "system: %s\n", buf);
#endif
			int rc = system(buf);
#if defined(DEBUG) && DEBUG > 2
			fprintf(stderr, "rc: %d\n", rc);
#endif
			if (rc) {
#if defined(DEBUG) && DEBUG > 2
                fprintf(stderr, "!system: %s\n", buf);
                fprintf(stderr, "!rc: %d\n", rc);
#endif
				return false;
            }
			// char fmt_sed_cmd2[] = "sed -e 's/%s %s/' %s_%d.bak > %s";
			snprintf(buf, 1024, fmt_sed_cmd2, hooks_sed_pat, pos, line, pid, line);
#if defined(DEBUG) && DEBUG > 2
			fprintf(stderr, "system: %s\n", buf);
#endif
			rc = system(buf);
            if (rc) {
#if defined(DEBUG) && DEBUG > 2
                fprintf(stderr, "system: %s\n", buf);
                fprintf(stderr, "rc: %d\n", rc);
#endif
				return false;
			}
        }
    }
    return true;
}

int is_err_print(void) {
    switch (errno) {
// EACCES The requested access to the file is not allowed, or search permission is denied for one of the directories in
//        the path prefix of pathname, or the file did not exist yet and write access to the parent  directory  is  not
//        allowed.  (See also path_resolution(7).)
      case EACCES:
          fprintf(stderr, "EACCES");
          return true;

// EEXIST pathname already exists and O_CREAT and O_EXCL were used.

// EFAULT pathname points outside your accessible address space.
      case EFAULT:
          fprintf(stderr, "EFAULT");
          return true;

// EFBIG  See EOVERFLOW.

// EINTR  While  blocked  waiting to complete an open of a slow device (e.g., a FIFO; see fifo(7)), the call was inter-
//        rupted by a signal handler; see signal(7).
      case EINTR:
          fprintf(stderr, "EINTR");
          return true;

// EISDIR pathname refers to a directory and the access requested involved writing (that  is,  O_WRONLY  or  O_RDWR  is
//        set).
      case EISDIR:
          fprintf(stderr, "EISDIR");
          return true;

// ELOOP  Too  many symbolic links were encountered in resolving pathname, or O_NOFOLLOW was specified but pathname was
//        a symbolic link.

// EMFILE The process already has the maximum number of files open.
      case EMFILE:
          fprintf(stderr, "EMFILE");
          return true;

// ENAMETOOLONG
//        pathname was too long.

// ENFILE The system limit on the total number of open files has been reached.
      case ENFILE:
          fprintf(stderr, "ENFILE");
          return true;

// ENODEV pathname refers to a device special file and no corresponding device exists.  (This is a Linux kernel bug; in
//        this situation ENXIO must be returned.)
      case ENODEV:
          fprintf(stderr, "EN1DEV");
          return true;

// ENOENT O_CREAT  is  not set and the named file does not exist.  Or, a directory component in pathname does not exist
//        or is a dangling symbolic link.
      case ENOENT:
          fprintf(stderr, "ENOENT");
          return true;

// ENOMEM Insufficient kernel memory was available.

// ENOSPC pathname was to be created but the device containing pathname has no room for the new file.

// ENOTDIR
//        A component used as a directory in pathname is not, in fact, a directory, or O_DIRECTORY  was  specified  and
//        pathname was not a directory.

// ENXIO  O_NONBLOCK | O_WRONLY is set, the named file is a FIFO and no process has the file open for reading.  Or, the
//        file is a device special file and no corresponding device exists.

// EOVERFLOW
//        pathname refers to a regular file that is too large to be opened.  The usual scenario here is that an  appli-
//        cation  compiled  on a 32-bit platform without -D_FILE_OFFSET_BITS=64 tried to open a file whose size exceeds
//        (2<<31)-1 bits; see also O_LARGEFILE above.  This is the error specified by POSIX.1-2001; in  kernels  before
//        2.6.24, Linux gave the error EFBIG for this case.

// EPERM  The O_NOATIME flag was specified, but the effective user ID of the caller did not match the owner of the file
//        and the caller was not privileged (CAP_FOWNER).
        default:
            fprintf(stderr, "default");
            return false;
    }
}

int FileExists(const char *path) {
    FILE *fp;
    fpos_t fsize = 0;

#ifdef WIN32
    if ( !fopen_s(&fp, path, "r") ) {
#else
    if ( !(fp = fopen(path, "r")) ) {
#endif
        fseek(fp, 0, SEEK_END);
        fgetpos(fp, &fsize);
        fclose(fp);
    }
    return fsize > 0;
}

int FileExistsA(const char *path) {
	FILE *fp;
	fpos_t fsize = 0;

#ifdef WIN32
	if (!fopen_s(&fp, path, "a")) {
#else
	if (!(fp = fopen(path, "a"))) {
#endif
		fseek(fp, 0, SEEK_END);
		fgetpos(fp, &fsize);
		fclose(fp);
	}
	return fsize > 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
