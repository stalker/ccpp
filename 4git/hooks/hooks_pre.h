// hooks_pre.h  //
/* $Date$ //
 * $Id$                          //
 * $Version: 0.1.0$                                                         //
 * $Revision: 0$                                                            //
 */

#pragma once

#ifndef DATERC_H__
#define DATERC_H__

#if defined(__WIN32__) || defined(__NT__) || defined(_WIN32)
# if !defined(WIN32)
#  define WIN32
# endif
# include <io.h>
# include <fcntl.h>
# include "my_basename.h"
#else
# include <libgen.h> /* basename */
enum my_bool {false, true};
#endif
#if defined(_MSC_VER) || defined(__WATCOMC__) || defined(__TINYC__)
enum my_bool {false, true};
#endif
#include "my_getline.h"

#if !defined(fileno)
# define fileno(fp) ((fp)->_file)
#endif


int hooks_fil(void);
int hooks_pre(void);
int hooks_rev(void);
int hooks_sed(void);

struct _my_funcs {
    char * name;
    int (*pfunc)(void);
} my_funcs[] = {
#ifdef _WIN32
    "hooks_fil.exe", hooks_fil,
    "hooks_pre.exe", hooks_pre,
    "hooks_rev.exe", hooks_rev,
    "hooks_sed.exe", hooks_sed,
    "pre-commit", hooks_pre
#else
    "hooks_fil", hooks_fil,
    "hooks_pre", hooks_pre,
    "hooks_rev", hooks_rev,
    "hooks_sed", hooks_sed
#endif
};

#define MY_FUNCS_NKEYS (sizeof my_funcs / sizeof(my_funcs[0]))

typedef signed int Tint;

/* The name this program was invoked by.  */
char *prog;

#endif /* DATERC_H__ */

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
