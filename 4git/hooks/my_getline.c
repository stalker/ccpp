/* my_getline.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 5$
 */

#include <stdio.h>
#include <stdlib.h>
#include "my_getline.h"

size_t my_puts(char * s) {
    for(; *s != '\0'; ++s)
        fputc(*s, stdout);
    return 0;
}

/* my_getline: get line into s, return length */
/* ckr_getline: читает строку в s, возвращает длину */
int my_getline(char * s, int lim)
{
    int c;
    char * i;

    i = s;
    while (--lim > 0 && (c = my_getch()) != EOF && c != '\n')
        *s++ = c;
    if (i == s && c == EOF) return EOF;
    if (c == '\n')
        *s++ = c;
    *s = '\0';

    return s - i;
}

/* buffer for ungetch */
static int buf[BUFSIZE]; /* буфер для ungetch */

/* next free position in buf */
static int bufp = 0;  /* след, свободная позиция в буфере */

/* get a (possibly pushed-back) character */
/* взять (возможно возвращенный) символ */
int my_getch(void)
{
    return (bufp > 0) ? buf[--bufp] : fgetc(stdin);
}

/* push character back on input */
/* вернуть символ на ввод */
void my_ungetch(int c)
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
