/* my_getline.h */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#pragma once

#ifndef MY_GETLINE_H__
#define MY_GETLINE_H__

#define BUFSIZE 256

size_t my_puts(char * s);

/* my_getline: get line into s, return length */
/* ckr_getline: читает строку в s, возвращает длину */
int my_getline(char * s, int lim);

/* get a (possibly pushed-back) character */
/* взять (возможно возвращенный) символ */
int my_getch(void);

/* push character back on input */
/* вернуть символ на ввод */
void my_ungetch(int c);

#endif /* MY_GETLINE_H__ */

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
