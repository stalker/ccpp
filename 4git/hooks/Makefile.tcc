## Digital Mars C/C++ makefile for windows with tcc
#
# $Date$
# $Id: 68c4f6f4f6879041b79e3a2748313f4498f84d1f $
# $Version: 0.7$
# $Revision: 1$
#

MAKEFILE= Makefile.tcc
MAIN    = daterc
SOURCES = $(MAIN).c
TARGETS = $(MAIN).exe $(MAIN)r.exe
OEXT    = obj
OBJ1    = my_getline.$(OEXT)
OBJ2    = my_basename.$(OEXT)
OBJS    = $(OBJ1) $(OBJ2)
HDRS    = my_getline.h my_basename.h
HEADER  = $(MAIN).h
LIBS    = 
TEMPS   = main.map $(MAIN).map
CFLAGS  = -I$(INC)
LDFLAGS = $(CFLAGS)
RM      = del /F
CP      = copy /B /Y
CC      = tcc

.PHONY: all

# Set Environment Variable:
all :
	+ make -f$(MAKEFILE) $(TARGETS) INC=%CD%

hooks_pre : $(TARGETS)

hooks_pre.exe : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)

hooks_sed.exe : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)


$(OBJS) : $(HDRS)

$(MAIN).$(OEXT) : $(HEADER)

$(TARGETS) : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)

.c.obj :
	$(CC) $(CFLAGS) $(DEFINES) -c $< -o $@

install :
	echo install

clean :
	$(RM) main.exe $(TARGETS) $(MAIN).$(OEXT) $(OBJS) $(TEMPS)
#
cleano :
	$(RM) *.obj

# vim: syntax=c:paste:ff=dos:textwidth=76:ts=4:sw=4:sts=4:et
#EOF
