// daterc.c //
/* $Date$ //
 * $Id$                          //
 * $Version: 0.7.1$                                                         //
 * $Revision: 1$                                                            //
 * $Author: Victor Skurikhin <stalker@quake.ru>$                            //
 */

#define DEBUG 0
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "daterc.h"

#if defined(__DMC__)
#error Don't adopt for __DMC__
#endif

int main(int argc, char ** argv) {
    int len, result;
    register int n;
#ifdef WIN32
    setvbuf(stdout, NULL, _IONBF, 0);
    result = setmode(fileno(stdin), O_BINARY );
    if (result == -1)
       fprintf(stderr, "Cannot set mode" );
#if defined(DEBUG) && DEBUG > 1
    else
       fprintf(stderr, "'stdin' successfully changed to binary mode\n" );
#endif
    result = setmode(fileno(stdout), O_BINARY );
    if (result == -1)
       fprintf(stderr, "Cannot set mode" );
#if defined(DEBUG) && DEBUG > 1
    else
       fprintf(stderr, "'stdout' successfully changed to binary mode\n" );
#endif
#endif
    prog = basename(argv[0]);
    len = strlen(prog);
    curr_data = time_string();
    curr_data_len = strlen(curr_data);
#if defined(DEBUG) && DEBUG > 1
    fprintf(stderr, "strlen(curr_data = %s) = %d\n", curr_data, curr_data_len);
    fprintf(stderr, "I find ...: %s\n", prog);
#endif
    for (n = 0; n < MY_FUNCS_NKEYS; n++) {
#if defined(DEBUG) && DEBUG > 1
        fprintf(stderr, "%s: ", my_funcs[n].name);
#endif
        if (0 == strncmp(prog, my_funcs[n].name, len)) {
#if defined(DEBUG) && DEBUG > 1
            fprintf(stderr, " Ok found: %p", my_funcs[n].pfunc);
#endif
            my_funcs[n].pfunc();
        }
#if defined(DEBUG) && DEBUG > 1
        else
            fputs(" Next", stderr);
        putc('\n', stderr);
#endif
    }

    return 0;
}

static Tint num_line = 0; /* текущая строка в файле */
/*                                                                          //
 * tr_comm:  тригер 1 если в крайней прочитанной строке был обнаружен       //
 *           комментарий иначе 0.                                           //
 * tr_comm0: тригер0 0 если "мы в комментарии"                              //
 *             1 если "мы не в комментарии".                                //
 * tr_comm1: тригер1 0 если "мы в комментарии"                              //
 *             1 если "мы не в комментарии".                                //
 */
Tint tr_comm  = false;
Tint tr_comm0 = false;
Tint tr_comm1 = false;
/*                                                                          //
 * tr_stng: тригер2 0 если "мы в строке ограниченной символом(") "          //
 *             1 если "мы не в строке".                                     //
 */
Tint tr_stng = false;
static char buf[SHRT_MAX];

/* имя: daterc                                                              //
 * ф-ция читает входной поток помещает текущую процитанную строку в буффер  //
 * buf, вызывает ф-цию find_comm_daterc для каждой прочитанной строки.      //
 */
int daterc(void) {
    Tint len, pos;
    while ((len = my_getline(buf, SHRT_MAX)) > 0) {
        ++num_line;
        pos = find_comm_daterc(buf, len);
#if defined(DEBUG) && DEBUG > 1
        fprintf(stderr, "%4d,%-3d,%1d,%2d: %s",
                num_line, len, tr_comm, pos, buf);
#endif
        my_puts(buf);
        if (!(tr_comm0 || tr_comm1))
            tr_comm = false;
    }
    return true;
}

/* имя: datercr                                                             //
 * ф-ция читает входной поток помещает текущую процитанную строку в буффер  //
 * buf, вызывает ф-цию find_comm_daterc для каждой прочитанной строки.      //
 */
int datercr(void) {
    Tint len, pos;
    while ((len = my_getline(buf, SHRT_MAX)) > 0) {
        ++num_line;
        pos = find_comm_datercr(buf, len);
#if defined(DEBUG) && DEBUG > 0
        fprintf(stderr, "%4d,%-3d,%1d,%2d: %s",
                num_line, len, tr_comm, pos, buf);
#endif
        my_puts(buf);
        if (!(tr_comm0 || tr_comm1))
            tr_comm = false;
    }
    return true;
}

/* имя: is_start_comm0                                                      //
 * параметры: b указатель на начало текущей строки.                         */
// ф-ция поиска начала комм-рия первого типа (/* */) в текущей строке       //
Tint is_start_comm0(char * b) {
    return !(tr_comm0 || tr_comm1 || tr_stng)
            && ('/' == *b && '*' == *(b + 1));
}

/* имя: is_start_comm1                                                      //
 * параметры: b указатель на начало текущей строки.                         //
 * ф-ция поиска начала комм-рия второго типа (//) в текущей строке.         //
 */
Tint is_start_comm1(char * b) {
    return !(tr_comm0 || tr_comm1 || tr_stng)
            && ('/' == *b && '/' == *(b + 1));
}

/* имя: is_end_comm                                                         //
 * параметры: b указатель на начало текущей строки.                         //
 * ф-ция поиска окончиния комм-рия первого или второго типа в текущей       //
 * строке.                                                                  //
 */
Tint is_end_comm(char * b) {
    return !tr_stng && ( (tr_comm1 && '\n' == *b) ||
                (tr_comm0 && '*' == *b && '/' == *(b + 1))
            );
}

/* имя: find_comm_daterc                                                    //
 * параметры: b - указатель на начало текущей строки.                       //
 *            len - длинна текуще строки.                                   //
 * ф-ция                                                                    //
 */
Tint find_comm_daterc(char * b, Tint len) {
    Tint r = 0;
    char * p = b;
    char * s = b;
    if (tr_comm && NULL != (p = strstr(b, pat1_date)))
        substitution_date_label(p, len);
    for (; len >= 0; ++b, --len)
        if (is_start_comm0(b)) {
            r = b - s + 1;
            tr_comm = tr_comm0 = true;
            if (1 == r && NULL != (p = strstr(b, pat1_date)))
                substitution_date_label(p, len - (p - s));
        } else if (is_start_comm1(b)) {
            r = b - s + 1;
            tr_comm = tr_comm1 = true;
            if (1 == r && NULL != (p = strstr(b, pat1_date)))
                substitution_date_label(p, len - (p - s));
        } else if (is_end_comm(b))
            tr_comm0 = tr_comm1 = false;
    return r;
}

/* имя: substitution_date_label                                             //
 * параметры: b - указатель на начало подстроки содержащей паттерн          //
                  pat1_date                                                 //
 *            len - длинна подстроки.                                       //
 * ф-ция                                                                    //
 */
Tint substitution_date_label(char * b, Tint len) {
    char * q;
    char * s = b;
    register Tint i, j, pat1_date_len;

    if (NULL == (q = strchr(b + 1, '$')))
        return false;
    remove_date_label(b, q);
    pat1_date_len = strlen(pat1_date);
    for (i = len; i >= pat1_date_len - 1; --i)
        *(b + i + curr_data_len + 1) = *(b + i);
    *(b + pat1_date_len) = ':';
    *(b + pat1_date_len + 1) = ' ';
    *(b + len + curr_data_len + 1) = '\0';
    for (i = pat1_date_len + 2, j = 0;
         i < (pat1_date_len + curr_data_len + 2)
         && *(curr_data + j) != '\n'; ++i, ++j)
        *(b + i) = *(curr_data + j);
    return true;
}

/* имя: find_comm_datercr                                                   //
 * параметры: b - указатель на начало текущей строки.                       //
 *            len - длинна текуще строки.                                   //
 * ф-ция                                                                    //
 */
Tint find_comm_datercr(char * b, Tint len) {
    char * q;
    Tint r = 0;
    char * p = b;
    char * s = b;
    if (tr_comm && NULL != (p = strstr(b, pat1_date))) {
      if (NULL != (q = strchr((const char *)p + 1, (int)'$')))
          remove_date_label(p, q);
    }
    for (; len >= 0; ++b, --len)
        if (is_start_comm0(b)) {
            r = b - s + 1;
            tr_comm = tr_comm0 = true;
            if (1 == r && NULL != (p = strstr(b, pat1_date)))
                if (NULL != (q = strchr(p + 1, '$')))
                    remove_date_label(p, q);
        } else if (is_start_comm1(b)) {
            r = b - s + 1;
            tr_comm = tr_comm1 = true;
            if (1 == r && NULL != (p = strstr(b, pat1_date)))
                if (NULL != (q = strchr(p + 1, '$')))
                    remove_date_label(p, q);
        } else if (is_end_comm(b))
            tr_comm0 = tr_comm1 = false;
    return r;
}

/* имя: substitution_date_label                                             //
 * параметры: b - указатель на начало подстроки содержащей паттерн          //
                  pat1_date                                                 //
 *            len - длинна подстроки.                                       //
 * ф-ция                                                                    //
 */
Tint remove_date_label(char * b, char * e) {
    char * s;
    register Tint i, j, pat1_date_len;

#if DEBUG > 1
        fprintf(stderr, "\n%p,%p\n: %s %s\n", b, e, b, e);
#endif
    s = b;
    pat1_date_len = strlen(pat1_date);
    for (i = pat1_date_len, j = 0; *(e + j) != '\0'; ++i, ++j)
        *(b + i) = *(e + j);
    *(b + i) = '\0';
    return true;
}

char *time_string(void) {
    struct tm *ptr;
    time_t lt;
    lt = time(NULL);
    ptr = localtime(&lt);
    return asctime(ptr);
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
