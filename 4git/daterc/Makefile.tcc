## Digital Mars C/C++ makefile for windows with tcc
#
# $Date$
# $Id: 0d8ffd044ec0a4dace44b95780904ed813402dff $
# $Version: 0.7$
# $Revision: 1$
#

MAKEFILE= Makefile.tcc
MAIN    = daterc
SOURCES = $(MAIN).c
TARGETS = $(MAIN).exe $(MAIN)r.exe
OEXT    = obj
OBJ1    = my_getline.$(OEXT)
OBJ2    = my_basename.$(OEXT)
OBJS    = $(OBJ1) $(OBJ2)
HDRS    = my_getline.h my_basename.h
HEADER  = $(MAIN).h
LIBS    = 
TEMPS   = main.map $(MAIN).map
CFLAGS  = -I$(INC)
LDFLAGS = $(CFLAGS)
RM      = del /F
CP      = copy /B /Y
CC      = tcc

.PHONY: all

# Set Environment Variable:
all :
	+ make -f$(MAKEFILE) $(TARGETS) INC=%CD%

daterc : $(TARGETS)

daterc.exe : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)

datercr.exe : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)


$(OBJS) : $(HDRS)

$(MAIN).$(OEXT) : $(HEADER)

$(TARGETS) : $(MAIN).$(OEXT) $(OBJS) $(HDRS) $(HEADER)
	$(CC) $(LDFLAGS) -o$@ $^ $(LIBS) $(OBJS) $(MAIN).$(OEXT)

.c.obj :
	$(CC) $(CFLAGS) $(DEFINES) -c $< -o $@

clean :
	$(RM) main.exe $(TARGETS) $(MAIN).$(OEXT) $(OBJS) $(TEMPS)
#
cleano :
	$(RM) *.obj

#EOF
