/* my_basename.h */
/* $Date$
 * $Id$
 * $Version: 0.7.1$
 * $Revision: 1$
 */

#pragma once

#ifndef MY_BASENAME_H__
#define MY_BASENAME_H__

#include <stdio.h>
#include <stdlib.h>

#if defined _WIN32 && (defined(__WATCOMC__) || defined(__TINYC__))
char * basename(char const *name);
#endif // defined _WIN32 && __DMC__

#endif // MY_BASENAME_H__ 

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
