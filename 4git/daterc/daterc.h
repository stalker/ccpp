// daterc.h //
/* $Date$ //
 * $Id$                          //
 * $Version: 0.7.1$                                                         //
 * $Revision: 1$                                                            //
 */

#pragma once

#ifndef DATERC_H__
#define DATERC_H__

#if defined(__WIN32__) || defined(__NT__) || defined(_WIN32)
# if !defined(WIN32)
#  define WIN32
# endif
# include <io.h>
# include <fcntl.h>
# include "my_basename.h"
#else
# include <libgen.h> /* basename */
enum my_bool {false, true};
#endif
#if defined(_MSC_VER) || defined(__WATCOMC__) || defined(__TINYC__)
enum my_bool {false, true};
#endif
#include "my_getline.h"

#if !defined(fileno)
# define fileno(fp) ((fp)->_file)
#endif


int daterc(void);
int datercr(void);

struct _my_funcs {
    char * name;
    int (*pfunc)(void);
} my_funcs[] = {
#ifdef _WIN32
    "daterc.exe", daterc,
    "datercr.exe", datercr
#else
    "daterc", daterc,
    "datercr", datercr
#endif
};

#define MY_FUNCS_NKEYS (sizeof my_funcs / sizeof(my_funcs[0]))

const char pat1_date[] = "$Date";

typedef signed int Tint;

/* The name this program was invoked by.  */
char *prog;
static char * curr_data;
static size_t curr_data_len = 9;

Tint find_comm_daterc(char * buf, Tint len);
Tint find_comm_datercr(char * buf, Tint len);
Tint substitution_date_label(char * buf, Tint len);
Tint remove_date_label(char * b, char * e);
char * time_string(void);

#endif /* DATERC_H__ */

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
