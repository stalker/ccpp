// exer_1_20.cc

#include <iostream>

int main()
{
    int a;
    std::cout << "Enter two numbers:" << std::endl;
    int i, v1, v2;
    std::cin >> v1 >> v2; // read input
    int lower = v2, sum = v2;
    for (i = (v1 - v2); i > 0 || i != 0 && (i = -i, lower = sum = v1); --i)
        sum += ++lower;
    std::cout << "sum = "
              << sum
              << std::endl;
    return 0;
}

// vim: syntax=cpp:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
// EOF
