// truth.c -- какие значения обозначают истину?
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    int n = 3;

    while (n)
        printf("%2d есть true\n", n--);
    printf("%2d есть false\n", n);
    n = -3;
    while (n)
        printf("%2d есть true\n", n++);
    printf("%2d есть false\n", n);

    return 0;
}
/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
