/* put2.c -- печатает строку и подсчитывает выведенные символы */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int put2(const char * string)
{
    int count = 0;

    while (*string)              /* общая идиома */
    {
        putchar(*string++);
        count++;
    }
    putchar('\n');               /* символ новой строки не подсчитывается */
    return(count);
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
