/* lesser.c -- из двух зол она выбирает меньшую */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int imin(int, int);

int main(void)
{
    int evil1, evil2;

    printf("Введите два целых числа (или q для завершения программы):\n");
    while (scanf("%d %d", &evil1, &evil2) == 2)
    {
        printf("Меньшим из двух чисел %d и %d является %d.\n",
           evil1, evil2, imin(evil1,evil2));
        printf("Введите два целых числа (или q для завершения программы):\n");
    }
    printf("Программа завершена.\n");
    return 0;
}

int imin(int n,int m)
{
    int min;

    if (n < m)
        min = n;
    else
        min = m;
    return min;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
