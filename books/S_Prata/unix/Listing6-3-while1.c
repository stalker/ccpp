/* while1.c -- правильно расставляйте фигурные скобки */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

/* Неправильное кодирование может стать причиной появления бесконечных циклов*/

#include <stdio.h>

int main(void)
{
    int n = 0;

    while (n < 3)
        printf("n равно %d\n", n);
        n++;
    printf("Это все, на что способна данная программа\n");

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
