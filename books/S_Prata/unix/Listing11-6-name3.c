/* name3.c -- программа считывает имена, пользуясь функцией fgets() */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

#define MAX 81

int main(void)
{
    char name[MAX];
    char * ptr;

    printf("Как вас зовут?\n");
    ptr = fgets(name, MAX, stdin);
    printf("%s? А! %s!\n", name, ptr);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
