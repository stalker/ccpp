/* hello.c -- преобразует аргумент командной строки в число */ 
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h> 
#include <stdlib.h> 

int main(int argc, char *argv[]) 
{
    int i, times; 
    if (argc < 2 || (times = atoi(argv[1])) < 1) 
        printf("Использование: %s положительное-число\n", argv[0]); 
    else 
        for (i = 0; i < times; i++) 
            puts("Всего хорошего!"); 
    return 0; 
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
