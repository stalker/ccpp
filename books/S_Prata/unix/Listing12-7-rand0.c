/* rand0.c -- генерация случайных чисел */
/* используется переносимый алгоритм ANSI C */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

static unsigned long int next = 1;         /* начальное число */

int rand0(void)
{
   /* магическая формула генерации псевдослучайных чисел */
   next = next * 1103515245 + 12345;
   return (unsigned int) (next/65536) % 32768;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
