// cmpflt.c -- сравнение чисел с плавающей запятой
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <math.h>
#include <stdio.h>

int main(void)
{
    const double ANSWER = 3.14159;
    double response;

    printf("Каково значение числа pi?\n");
    scanf("%lf", &response);
    while (fabs(response - ANSWER) > 0.0001)
    {
        printf("Введите значение повторно!\n");
        scanf("%lf", &response);
    }
    printf("Требуемая точность достигнута!\n");

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
