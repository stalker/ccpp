/* r_drive0.c -- тестирование функции rand0() */
/* компилируется с файлом rand0.c */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

extern int rand0(void);

int main(void)
{
    int count;
    for (count = 0; count < 5; count++)
        printf("%hd\n", rand0());
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
