/* recur.c -- иллюстрация рекурсии */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

void up_and_down(int);

int main(void)
{
    up_and_down(1);
    return 0;
}

void up_and_down(int n)
{
    printf("Уровень %d: ячейка n %p\n", n, &n);        /* 1 */
    if (n < 4)
        up_and_down(n+1);
    printf("УРОВЕНЬ %d: ячейка n %p\n", n, &n);        /* 2 */
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
