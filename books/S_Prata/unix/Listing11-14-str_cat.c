/* str_cat.c -- объединяет две строки */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>
#include <string.h>               /* объявление функции strcat()*/

#define SIZE 80

int main(void)
{
    char flower[SIZE];
    char addon[] = " пахнет как старые валенки.";

    puts("Какой ваш любимый цветок?");
    gets(flower);
    strcat(flower, addon);
    puts(flower);
    puts(addon);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
