/* Программа nono.c -- не делайте так! */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    char side_a[] = "Сторона А";
    char dont[] = {'В', 'А', 'У', '!' };
    char side_b[] = "Сторона Б";

    puts(dont);          /* dont не является строкой */
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
