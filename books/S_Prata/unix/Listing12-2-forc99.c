// forc99.c -- правила нового стандарта C99, касающиеся блоков
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main()
{
    int n = 10;

    printf("Первоначально n = %d\n", n);
    for (int n = 1; n < 3; n++)
        printf("цикл 1: n = %d\n", n);
    printf("По завершении цикла 1 n = %d\n", n);
    for (int n = 1; n < 3; n++)
    {
        printf("индекс цикла 2 n = %d\n", n);
        int n = 30;
        printf("цикл 2: n = %d\n", n);
        n++;
    }
    printf("По завершении цикла 2 n = %d\n", n);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
