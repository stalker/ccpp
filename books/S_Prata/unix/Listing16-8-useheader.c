// useheader.c -- использование структуры names_st
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>
#include "names_st.h"

// компилировать вместе с файлом names_st.c
int main(void)
{
    names candidate;
    get_names(&candidate);
    printf("Добро пожаловать в программу, ");
    show_names(&candidate);
    printf(" !\n");
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
