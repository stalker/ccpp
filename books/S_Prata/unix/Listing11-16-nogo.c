/* nogo.c -- будет ли это работать? */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

#define ANSWER "Грант"

int main(void)
{
    char try[40];

    puts("Кто похоронен в могиле Гранта?");
    gets(try);
    while (try != ANSWER)
    {
       puts("Нет, неправильно. Попытайтесь еще раз.");
       gets(try);
    }
    puts("Теперь правильно!");
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
