/* put1.c -- печатает строку без добавления символа \n */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

void put1(const char * string)         /* строка не меняется */
{
    while (*string != '\0')
        putchar(*string++);
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
