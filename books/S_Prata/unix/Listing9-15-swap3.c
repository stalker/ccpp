/* swap3.c -- использование указателей для обмена значениями переменных */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

void interchange(int * u, int * v);

int main(void)
{
    int x = 5, y = 10;

    printf("Первоначально x = %d и y = %d.\n", x, y);
    interchange(&x, &y);                  /* передача адресов в функцию */
    printf("Теперь x = %d и y = %d.\n", x, y);
    return 0;
}

void interchange(int * u, int * v)
{
    int temp;

    temp = *u; /*переменная temp получает значение, на которое указывает u*/
    *u = *v;
    *v = temp;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
