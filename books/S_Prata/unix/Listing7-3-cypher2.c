// cypher2.c -- меняет символы входных данных, оставляя неизменными символы,
// не являющиеся буквами
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>
#include <ctype.h>           // для функции isalpha()

int main(void)
{
    char ch;
    while ((ch = getchar()) != '\n')
    {
        if (isalpha(ch))     // если это буква,
            putchar(ch + 1); // изменить ее
        else                 // в противном случае
            putchar(ch);     // вывести символ таким, каким он есть
    }
    putchar(ch);             // печатать символ новой строки
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
