/* name1.c -- программа считывает имя */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

#define MAX 81

int main(void)
{
    char name[MAX];            /* выделить пространство памяти */

    printf("Как вас зовут?\n");
    gets(name);                /* поместить строку в массив name */
    printf("Прекрасное имя, %s.\n", name);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
