/* for_cube.c -- использование цикла for для построения таблицы кубов целых чисел */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    int num;

    printf("    n    n в кубе\n");
    for (num = 1; num <= 6; num++)
       printf("%5d %5d\n", num, num*num*num);

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
