/* loccheck.c -- проверка с целью выяснения, где хранятся переменные */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

void mikado(int);           /* объявление функции */

int main(void)
{
    int pooh = 2, bah = 5;   /* переменные, локальные в функции main() */

    printf("В функции main() pooh = %d и &pooh = %p\n",
            pooh, &pooh);
    printf("В функции main() bah = %d и &bah = %p\n",
            bah, &bah);
    mikado(pooh);
    return 0;
}

void mikado(int bah)       /* объявление функции */
{
    int pooh = 10;          /* переменные, локальные в функции mikado() */

    printf("В функции mikado() pooh = %d и &pooh = %p\n", pooh, &pooh);
    printf("В функции mikado() bah = %d и &bah = %p\n", bah, &bah);
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
