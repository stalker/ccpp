// doubincl.c -- двойное включение заголовочного файла
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>
#include "names.h"
#include "names.h" // непреднамеренное повторное включение

int main()
{
    names winner = {"Иван", "Иванов"};
    printf("Победителем стал %s %s.\n", winner.first,
           winner.last);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
