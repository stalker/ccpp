/* rows1.c -- использование вложенных циклов */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

#define ROWS 6
#define CHARS 10

int main(void)
{
    int row;
    char ch;

    for (row = 0; row < ROWS; row++)               /* строка 10 */
    {
        for (ch = 'A'; ch < ('A' + CHARS); ch++)    /* строка 12 */
           printf("%c", ch);
        printf("\n");
    }

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
