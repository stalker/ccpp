// diceroll.h
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 0$
 */

extern int roll_count;
int roll_n_dice(int dice, int sides);

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
