// names_st.h -- заголовочный файл для структуры names_st
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 0$
 */
// константы

#define SLEN 32

// объявление структуры
struct names_st
{
    char first[SLEN];
    char last[SLEN];
};

// определения типов
typedef struct names_st names;

// прототипы функций
void get_names(names *);
void show_names(const names *);

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
