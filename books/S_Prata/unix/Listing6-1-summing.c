/* summing.c -- суммирует целые числа, вводимые в интерактивном режиме */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    long num;
    long sum = 0L; /* инициализация переменной sum нулем */
    int status;

    printf("Введите целое число для последующего суммирования ");
    printf("(или q для завершения программы): ");
    status = scanf("%ld", &num);
    while (status == 1) /* == обозначает равенство */
    {
        sum = sum + num;
        printf("Введите следующее целое число (или q для завершения программы): ");
        status = scanf("%ld", &num);
    }
    printf("Сумма введенных целых чисел равна %ld.\n", sum);

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
