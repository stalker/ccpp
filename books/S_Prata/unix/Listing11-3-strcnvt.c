/* p_and_s.c -- указатели и строки */ 
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h> 

int main(void) 
{ 
    const char * mesg = "Не позволяйте себя запутать!"; 
    const char * copy; 

    copy = mesg; 
    printf("%s\n", copy); 
    printf("mesg = %s; &mesg = %p; value = %p\n", 
            mesg, &mesg, mesg); 
    printf("copy = %s; &copy = %p; value = %p\n", 
            copy, &copy, copy); 
    return 0; 
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
