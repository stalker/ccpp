/* scan_str.c -- использование функции scanf() */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    char name1[11], name2[11];
    int count;

    printf("Введите, пожалуйста, два имени.\n");
    count = scanf("%5s %10s",name1, name2);
    printf("Прочитано %d имени: %s и %s.\n",
           count, name1, name2);
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
