/* s_and_r.c -- файл функций rand1() и srand1() */
/* использует переносимый алгоритм ANSI C */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

static unsigned long int next = 1;         /* начальное число */

int rand1(void)
{
    /* магическая формула для генерации псевдослучайных чисел */
    next = next * 1103515245 + 12345;
    return (unsigned int) (next/65536) % 32768;
}

void srand1(unsigned int seed)
{
    next = seed;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
