/* compare.c -- эта программа будет работать */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>
#include <string.h> /* объявляет функцию strcmp() */

#define ANSWER "Грант"
#define MAX 40

int main(void)
{
    char try[MAX];

    puts("Кто похоронен в могиле Гранта?");
    gets(try);
    while (strcmp(try,ANSWER) != 0)
    {
       puts("Нет, неправильно. Попытайтесь еще раз.");
       gets(try);
    }
    puts("Теперь правильно!");
    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
