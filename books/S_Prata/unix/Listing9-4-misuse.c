/* misuse.c -- неправильное использование функции */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int imax();               /* объявление в старом стиле */

int main(void)
{
    printf("Наибольшим значением из %d и %d является %d.\n",
           3, 5, imax(3));
    printf("Наибольшим значением из %d и %d является %d.\n",
           3, 5, imax(3.0, 5.0));
    return 0;
}

int imax(n, m)
int n, m;
{
    int max;
    if (n > m)
        max = n;
    else
        max = m;
    return max;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
