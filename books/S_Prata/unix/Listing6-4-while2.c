/* while2.c -- правильно расставляйте точки с запятой */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    int n = 0;

    while (n++ < 3);                 /* строка 7 */
        printf("n равно %d\n", n);    /* строка 8 */
    printf("Это все, что может сделать данная программа.\n");

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
