/* floatcnv.c -- несогласованные преобразования с плавающей запятой */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

int main(void)
{
    float n1 = 3.0;
    double n2 = 3.0;
    long n3 = 2000000000;
    long n4 = 1234567890;

    printf("%.1e %. 1e %.1e %.1e\n", n1, n2, n3, n4);
    printf("%ld  %ld\n", n3, n4);
    printf("%ld  %ld  %ld   %ld\n", n1, n2, n3, n4);

    return 0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
