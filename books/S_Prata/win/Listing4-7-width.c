/* width.c -- ������ ���� */
/* $Date$
 * $Id$
 * $Version: 1$
 * $Revision: 1$
 */

#include <stdio.h>

#define PAGES 931

int main(void)
{
    printf("*%d*\n", PAGES);
    printf{"*%2d*\n", PAGES);
    printf("*%10d*\n", PAGES);
    printf("*%-10d*\n", PAGES);

    return 0;
}

/* vim: syntax=c:paste:ff=dos:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
