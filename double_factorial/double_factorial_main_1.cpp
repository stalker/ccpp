/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/math/constants/constants.hpp>
#include "double_factorial.h"
#include "mydebug.h"
#include "factorial65536.hpp"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using namespace boost::multiprecision;

using namespace boost::multiprecision;

namespace mp = boost::multiprecision;
using BF = mp::number<mp::cpp_dec_float_100::backend_type, mp::et_off>;
using BI = mp::number<mp::cpp_int::backend_type, mp::et_off>;

cpp_int factorial(unsigned  n) {
    assert(n != ~0U);
    if (n < 16384) {
        cpp_int factorial = 1;
        for (unsigned i = 1; i < (n + 1); ++i) {
            factorial *= i;
        }
        return factorial;
    } else if (n == 16384) {
        return factorial16384();
    } else if (n < 24576) {
        cpp_int factorial = factorial16384();
        for (unsigned i = 16385; i < (n + 1); ++i) {
            factorial *= i;
        }
        return factorial;
    } else if (n == 24576) {
        return factorial24576();
    } else if (n < 32768) {
        cpp_int factorial = factorial24576();
        for (unsigned i = 24577; i < (n + 1); ++i) {
            factorial *= i;
        }
        return factorial;
    } else if (n == 24576) {
        return factorial24576();
    } else if (n < 65536) {
        cpp_int factorial = factorial32768();
        for (unsigned i = 32769; i < (n + 1); ++i) {
            factorial *= i;
        }
        return factorial;
    } else if (n == 65536) {
        return factorial65536();
    } else {
        cpp_int factorial = factorial65536();
        for (unsigned i = 65537; i < (n + 1); ++i) {
            factorial *= i;
        }
        return factorial;
    }
}

template<typename T>
inline T stirling_factorial(T n)
{
   using boost::math::constants::e;
   using boost::math::constants::pi;
   using boost::multiprecision::pow;
   using boost::multiprecision::sqrt;
   return sqrt(2*pi<T>()*n) * pow(n/e<T>(), n) * (1 + 1/(12*n) + 1/(288*n*n));
}

BI stirling_factorial_prec100(unsigned  n) {
    BF fn = n;
    BF r1 = ceil(stirling_factorial(fn));
    BI r0(r1.convert_to<BI>());
    return r0;
}

BF stirling_factorial_float_prec100(unsigned  n) {
    BF fn = n;
    return ceil(stirling_factorial(fn));
}

/* let's go */
int main(int argc, char *argv[]) {
    /* cout << "8!     = " << factorial(8) << endl;
    cout << "20!    = " << factorial(20) << endl;
    cout << "21!    = " << factorial(21) << endl;
    cout << "70!    = " << factorial(70) << endl;
    cout << "80!    = " << factorial(80) << endl;
    cout << "128!   = " << factorial(128) << endl;
    cout << "1024!  = " << factorial(1024) << endl;
    cout << "8192!  = " 
         << std::hex
         << factorial(8192)
         << endl; */
    cout << "16384! = "
         << factorial(16384)
         << endl;
    cout << "16385! = "
         << factorial(16385)
         << endl;
    cout << "24576! = "
         << factorial(24576)
         << endl;
    cout << "24577! = "
         << factorial(24577)
         << endl;
    cout << "32768! = "
         << factorial(32768)
         << endl;
    cout << "32769! = "
         << factorial(32769)
         << endl;
    cout << "8192!  = " << FF_11_6
         << stirling_factorial_float_prec100(8192)
         << endl;
    cout << "16384! = " << FF_11_6
         << stirling_factorial_float_prec100(16384)
         << endl;
    cout << "24576! = " << FF_11_6
         << stirling_factorial_float_prec100(65536)
         << endl;
    cout << "32768! = " << FF_11_6
         << stirling_factorial_float_prec100(32768)
         << endl;
    /*
    cout << "65536! = " << factorial(65536) << endl;
    cout << "65537! = " << factorial(65537) << endl;
    cout << "8!     = " << stirling_factorial_prec100(8) << endl;
    cout << "20!    = " << stirling_factorial_prec100(20) << endl;
    cout << "21!    = " << stirling_factorial_prec100(21) << endl;
    cout << "80!    = " << stirling_factorial_prec100(80) << endl;
    cout << "128!   = " << FF_11_6
         << stirling_factorial_float_prec100(128)
         << endl;
    cout << "1024!  = " << FF_11_6
         << stirling_factorial_float_prec100(1024)
         << endl;
    cout << "8192!  = " << FF_11_6
         << stirling_factorial_float_prec100(8192)
         << endl;
    cout << "65536! = " << FF_11_6
         << stirling_factorial_float_prec100(65536)
         << endl;
    cout << "65537! = " << FF_11_6
         << stirling_factorial_float_prec100(65537)
         << endl; */
    return EXIT_SUCCESS;
}



/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
