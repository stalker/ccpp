#ifndef _FACTORIAL_65536_HPP_
#define _FACTORIAL_65536_HPP_
#include <boost/multiprecision/cpp_int.hpp>
boost::multiprecision::cpp_int factorial16384();
boost::multiprecision::cpp_int factorial24576();
boost::multiprecision::cpp_int factorial32768();
boost::multiprecision::cpp_int factorial65536();
#endif
