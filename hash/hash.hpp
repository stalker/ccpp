////////////////////////////////////////////////////////////////////////////////
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef _HASH_HPP_
#define _HASH_HPP_

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iostream>
#include <iomanip>
#include <list>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <string>

#ifndef SINT64
# define SINT64 int_fast64_t
#endif
#ifndef UINT64
# define UINT64 uint_fast64_t
#endif
#ifndef CAST_SINT64
# define CAST_SINT64(x) static_cast<SINT64>(x)
#endif
#ifndef CAST_UINT64
# define CAST_UINT64(x) static_cast<UINT64>(x)
#endif

// const char * sp = " ";
#define A000040_56         263
#define A000040_216816     2999999
#define A000040_216817     3000017
#define A000040_283146     3999971
#define A000040_283147     4000037
#define A000040_664579     9999991
#define A000040_664580     10000019
#define A000040_EULER_1772 2147483647

const size_t prime_3m  = A000040_216816;
const size_t prime_4m  = A000040_283146;
const UINT64 prime_mod = 1000000007;
const UINT64 prime_x   = A000040_56;

template <class I = SINT64>
inline I signed_mod(I a, I b) { // (corresponding to Euclidian division)
    I const r = a % b;
    return (r < 0 ? r + std::abs(b) : r);
}

template <class I = UINT64>
inline I sum_mod(I a, I b, I m) {
    return ((a % m) + (b % m)) % m;
}

template <class I = UINT64>
inline I product_mod(I a, I b, I m) {
    return ((a % m) * (b % m)) % m;
}

// base^p mod prime_mod
// O(log m) : m = prime_mod
UINT64 bin_pow_mod(UINT64 base, SINT64 p) {
    if (p == 0)
        return 1;

    if (p == 1)
        return base;

    if (p % 2 == 0) {
        UINT64 t = bin_pow_mod(base, p / 2);
        return t * t % prime_mod;
    } else
        return bin_pow_mod(base, p - 1) * base % prime_mod;
}

template <class I = UINT64>
inline I next_factor(I f, I x, I m) {
    return (f * x) % m;
}

template <class I = UINT64>
inline I monom_mod(I a, I f, I m) {
    return (CAST_UINT64(a) * f) % m;
}

template <class T>
class hash {
public:
    typedef typename std::list<T>        list_t;
    typedef typename list_t::iterator    list_iterator_t;
    typedef typename list_t::value_type  list_value_type_t;
    typedef typename std::vector<list_t> vector_t;
    typedef typename vector_t::iterator  vector_iterator_t;

    const UINT64 m, p, x;

    hash(UINT64 mmod = prime_3m, UINT64 a = prime_x, UINT64 pmod = prime_mod)
    : m(mmod), p(pmod), x(a), _data(m) { /* None */ }

    void add(T value) {
        auto & lst = getLst(value);
        auto it = std::find(lst.begin(), lst.end(), value);
        if (it == lst.end())
            lst.push_front(value);
    }

    bool find(T value) {
        auto & lst = getLst(value);
        auto it = std::find(lst.begin(), lst.end(), value);
        return (it != lst.end());
    }

    void erase(T value) {
        auto & lst = getLst(value);
        auto it = std::find(lst.begin(), lst.end(), value);
        if (it != lst.end())
            lst.erase(it);
    }

    friend class iterator;
    friend class indexIterator;

    class indexIterator {
        friend class hash;
    private:
        hash::list_t &         _list;
        hash::list_iterator_t  _itr;
        indexIterator(hash::list_t & list)
        : _list(list), _itr(_list.begin()) { /* Empty */ }
    public:
        bool hasNext() {
            return _itr != _list.end();
        }
        list_value_type_t next(void) {
            list_value_type_t value = (*_itr);
            ++_itr;
            return value;
        }
    };

    indexIterator getIndexIterator(ptrdiff_t idx)
    throw(std::range_error) {
        check_index(idx, "getIndexIterator(idx)", __LINE__);
        return indexIterator(_data[idx]);
    }

    class iterator {
        friend class hash;
    private:
        hash::vector_t &        _vector;
        hash::vector_iterator_t _vitert;
        hash::list_iterator_t   _litert;
        iterator(hash::vector_t & v)
        : _vector(v), _vitert(_vector.begin())
        , _litert(_vitert->begin())
        { /* Empty */ }
    public:
        bool hasNext() {
            if (_litert != _vitert->end()) return true;
            size_t size = 0;
            for (auto it = _vitert + 1; it != _vector.end(); ++it)
                size += it->size();
            return size > 0;
        }

        list_value_type_t next(void)
        throw(std::range_error) {
            if (!hasNext())
                throw std::range_error("Hash Range Error index: in: "
                      " iterator::next() on line: " + std::to_string(__LINE__));
            if (_litert != _vitert->end()) {
                list_value_type_t value = (*_litert);
                ++_litert;
                return value;
            }
            ++_vitert;
            _litert = _vitert->begin();
            return next();
        }
    };

    iterator getIterator()
    throw(std::range_error) {
        return iterator(_data);
    }
protected:
    const std::vector<T>& operator[](ptrdiff_t idx) const
    throw(std::range_error) {
        check_index(idx, "operator[]", __LINE__);
        return _data[idx];
    }

    UINT64 hashFunction(const T & s, ptrdiff_t lim = PTRDIFF_MAX) {
        auto it = s.begin();
        UINT64 i = 0, factor = 1, sum = monom_mod<UINT64>(*it, factor, p);
        for (++it, --lim; it != s.end() && lim > 0; ++i, ++it, --lim) {
            factor = next_factor<UINT64>(factor, x, p);
            sum = (sum + monom_mod<UINT64>(*it, factor, p)) % p;
        }
        return sum % m;
    }
private:
    list_t & getLst(T & value) {
        return _data[hashFunction(value)];
    }

    void check_index(ptrdiff_t idx, const std::string sfun, int line)
    throw(std::range_error) {
        if (0 > idx || static_cast<size_t>(idx) > _data.size())
            throw std::range_error("Hash Range Error index: " +
                  std::to_string(idx) + " in: " + sfun + " on line: " +
                  std::to_string(line));
    }

    vector_t _data;
};

#endif
////////////////////////////////////////////////////////////////////////////////
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
