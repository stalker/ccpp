// 3_2_Step_03_main_1.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#define DEBUG 4

#ifdef DEBUG
# include "mydebug.hpp"
#endif

/**
 * Поиск образца в тексте
 * Найти все вхождения строки Pattern в строку Text.
 * 
 * Вход. Строки Pattern и Text.
 * Выход. Все индексы i строки Text, начиная с которых стро ка Pattern входит
 * в Text:
 * Text[i..i + |Pattern| − 1] = Pattern.
 *
 * Реализуйте алгоритм Карпа–Рабина.
 *
 * Формат входа. Образец Pattern и текст Text.
 *
 * Формат  выхода.   Индексы  вхождений  строки  Pattern  в  строку  Text   в
 * возрастающем порядке, используя индексацию с нуля.
 *
 * Ограничения.  1 ≤ |Pattern|  ≤  |Text|  ≤  5·10⁵.   Суммарная  длина  всех
 * вхождений образца в текста не превосходит 10⁸.  Обе строки содержат  буквы
 * латинского алфавита.
 *
 */

#include "hash.hpp"
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <sstream>
#include <string>

template <class T>
void h_iterate_index(hash<T> & h, ptrdiff_t index, std::ostream & outstm) {
    auto it = h.getIndexIterator(index);
    while (it.hasNext()) {
        auto item = it.next();
        outstm << item;
        if (it.hasNext()) outstm << sp;
    }
    outstm << std::endl;
}

template <class T>
void h_iterate(hash<T> & h, std::ostream & outstm) {
    auto it = h.getIterator();
    while (it.hasNext()) {
        auto item = it.next();
        outstm << item;
        if (it.hasNext()) outstm << sp;
    }
    outstm << std::endl;
}

int go(std::istream & instm, std::ostream & outstm) try {
    int m = 0, n = 0;
    instm >> m >> n;
    hash<std::string> h(m);
    for (int i = 0; i < n; ++i) {
        std::string cmd;
        instm >> cmd;
        std::string name;
        if (cmd == "add") {
            instm >> name;
            h.add(name);
        } else if (cmd == "del") {
            instm >> name;
            h.erase(name);
        } else if (cmd == "find") {
            instm >> name;
            if (h.find(name))
                outstm << "yes" << std::endl;
            else
                outstm << "no" << std::endl;
        } else if (cmd == "check") {
            int index;
            instm >> index;
            h_iterate_index(h, index, outstm);
        } else
            throw std::logic_error("Bad command");
    }
    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

int test_hash_string() {
    hash<std::string> h(5);
    std::string world = "world";
    std::cout << "h.add(\"world\", 5)" << std::endl;
    h.add("world");
    std::cout << "h.add(\"HellO\", 5)" << std::endl;
    h.add("HellO");
    std::cout << "h.add(\"GooD\", 5)" << std::endl;
    h.add("GooD");
    std::cout << "h.add(\"luck\", 5)" << std::endl;
    h.add("luck");
    h_iterate(h, std::cout);
    for (int i = 0; i < 5; ++i) {
        std::cout << i << ": ";
        h_iterate_index(h, i, std::cout);
    }
    return EXIT_SUCCESS;
}

/* let's go */
int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(0);
#if defined(DEBUG) && defined(PRINTM)
    printd("e argc: ", argc, "; argv: ", argv) << std::endl;
#endif
    // return go(std::cin, std::cout);
    return test_hash_string();
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
