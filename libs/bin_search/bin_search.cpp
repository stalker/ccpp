/* bin_search.cpp */
/* $Date$
 * $Id$
 * $Version: 1.0$
 * $Revision: 1$
 */
// compl. example: g++ -std=c++11 bin_search.cpp -o bin_search
//                 icc -std=c++11 bin_search.cpp -o bin_search

#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# include "stdafx.h"
#endif
#include <cctype>
#include <string>
#include <vector>
#include <iostream>

using std::string;
using namespace std;

template <class T>
T bin_search(size_t & i, vector<T> v, T e) {
    i = -1;
    if (v.empty())
        return nullptr;
    auto v_beg = v.begin(), v_end = v.end();
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    if (3 > (v_end - v_beg)) {
        vector<T>::iterator p;
        for (p = v.begin(); p != v.end(); ++p)
            if (e == *p) {
                i = v_beg - p;
                return *p;
            }
        return *v_beg;
    }
#endif
    auto v_mid = v_beg + (v_end - v_beg)/2;
    while (v_mid != v_end && *v_mid != e) {
        if (e < *v_mid)
            v_end = v_mid;
        else
            v_beg = v_mid + 1;
        v_mid = v_beg + (v_end - v_beg)/2;
    }
    if (v_mid != v_end) {
        i = v_mid - v_beg;
        return *v_mid;
    }
    return e;
}

#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# define main _tmain
#else
# define _TCHAR char
#endif
int main(int argc, _TCHAR * argv[])
{
    string s("Hello World!!!");
    for (auto &c : s)
        c = toupper(c);
    cout << s << endl;
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    articles.push_back("a");
    articles.push_back("an");
    articles.push_back("the");
#else
    vector<string> articles = {"a", "an", "the"};
#endif
    size_t i;
    string b("b");
    auto res = bin_search(i, articles, string("b"));
    if (-1 != i) {
        cout << "res = "
           << res << endl
           << "index of res = "
           << i
           << endl;
    } else
        cout << "res not found!" << endl
             << "return index = " << i
             << endl;
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */

