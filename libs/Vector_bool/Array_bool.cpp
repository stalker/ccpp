#include <cstdlib>
#include <cstring>
#include <iostream>

#define SIZE 10

using namespace std;

template < class T >
struct Array {
/* ... */
    T * data_;
};

template <>
struct Array <bool > {
    static int const INTBITS = 8 * sizeof ( int );
    explicit Array( size_t size)
                  : size_(size)
                  , data_( new int [size_ / INTBITS + 1])
    {
        memset(data_, 0, sizeof(int) * (size_ / INTBITS + 1));
    }
    bool operator []( size_t i) const {
        return data_[i / INTBITS] & (1 << (i % INTBITS ));
    }
    bool operator()(size_t i, bool b) const {
        return data_[i / INTBITS] |= (1 << (i % INTBITS ));
    }

private :
    size_t size_;
    int * data_;
};

int main(void) {
    size_t i;
    Array<bool> b(SIZE);
    b(0, true);
    b(3, true);
    for (i = 0; i < SIZE; ++i)
        cout << b[i] << " ";
    for(auto e : b) {
      cout << "ok" << endl;
    }
    cout << endl;
    return 0;
}
