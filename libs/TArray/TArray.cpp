// конструкторы
template <class T>
TArray<T>::TArray(const TArray<T> &t)
:Size(t.Size),
 Count(t.Count),
 elems(new T[Size])                         // новый массив
{   for(size_type i = 0; i<Count; ++i)      // копируем
       elems[i] = t.elems[i];
}
template <class T>
TArray<T>::TArray(const size_type& n)
:Size((n > minsize)?n:minsize),
 Count(n),                                  // массив пустой
 elems(new T[Size])                         // новый массив
{ for(size_type i = 0; i<Size; ++i)         // обнуляем
     elems[i] = T();
}
template <class T>
template <class Iterator>
TArray<T>::TArray(Iterator begin, Iterator end)
{ if (!(begin > end))
  { Size  = (end - begin);                  // количество элементов
    Count = Size;                           // текущий размер
    elems = new T[Size];                    // создаем массив
    for(size_type i = 0; i<Count; ++i)      // заполняем массив
       elems[i] = *(begin+i);               // копируем из массива
  }
  else                                      // неправильные параметры
  throw std::invalid_argument("array<>: invalid_argument (begin > end)!");
}
// добавление элементов
template <class T>
void TArray<T>::push_back(const value_type& v)
{ if (Count == Size)                        // места нет
    resize(Size * 2);                       // увеличили «мощность»
  elems[Count++] = v;                       // присвоили
}
template <class T>
void TArray<T>::resize(size_type newsize)
{ if (newsize > capacity())
  { T *data = new T[newsize];               // новый массив
    for(size_type i = 0; i<Count; ++i)      // копируем
       data[i] = elems[i];
    delete[] elems;                         // возвращаем память
    elems = data;                           // вступили во владение
    Size = newsize;                         // «увеличили «мощность»
  }
}
template <class T>
template <class U>
TArray<T>& TArray<T>::operator=(const TArray<U>& other)
{ TArray<T>tmp(other.begin(), other.end());
  tmp.resize(other.capacity());
  swap(tmp);
  return *this;
}
template <class T>
TArray<T>& TArray<T>::operator=(const TArray<T>& other)
{ TArray<T>tmp(other.begin(), other.end());
  tmp.resize(other.capacity());
  swap(tmp);
  return *this;
}
