/* AVL_tree.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * http://rosettacode.org/wiki/Compare_a_list_of_strings#C.2B.2B
 * Given a list of arbitrarily many strings, show how to:
 *
 *     test if they are all lexically equal
 *     test if every string is lexically less than the one after it
 *     (i.e. whether the list is in strict ascending order) 
 *
 * Each of those two tests should result in a single true or false value,
 * which could be used as the condition of an if statement or similar.
 * If the input list has less than two elements, the tests should always
 * return true.
 *
 * There is no need to provide a complete program & output: Assume that
 * the strings are already stored in an array/list/sequence/tuple variable
 * (whatever is most idiomatic) with the name strings, and just show the
 * expressions for performing those two tests on it (plus of course any
 * includes and custom functions etc. that it needs), with as little
 * distractions as possible.
 *
 * Try to write your solution in a way that does not modify the original
 * list, but if it does then please add a note to make that clear to readers.
 *
 */

#include <stdio.h>
#include <string.h>

int strings_are_equal(char * * strings, int nstrings)
{
  int result = 1;

  while (result && (--nstrings > 0))
  {
    result = !strcmp(*strings, *(strings+nstrings));
  }

  return result;
}

int strings_are_in_ascending_order(char * * strings, int nstrings)
{
  int result = 1;
  int k = 0;

  while (result && (++k < nstrings))
  {
    result = (0 >= strcmp(*(strings+k-1), *(strings+k)));
  }

  return result;
}

/* vim: syntax=c:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
