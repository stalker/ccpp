// Реализуйте шаблонную функцию minimum, которая находит минимальный элемент, который хранится в экземпляре шаблонного класса Array, при этом типовой параметр шаблона Array может быть произвольным. Чтобы сравнивать объекты произвольного типа, на вход функции так же будет передаваться компаратор, в качестве компаратора может выступать функция или объект класса с перегруженным оператором "()". Примеры вызова функции minimum:
//
// Sample Input:
// There are no tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

#include <cstddef>
#include <iostream>
#include "Array.h"

bool less(int a, int b) {
    return a < b;
}
struct Greater {
    bool operator()(int a, int b) {
        return b < a;
    }
};

template<class T, class Comp>
T minimum(Array<T> & p, Comp lesscmp);

int main(void) {
    Array<int> ints(3);
    ints[0] = 10;
    ints[1] = 2;
    ints[2] = 15;
    int min = minimum(ints, less); // в min должно попасть число 2
    int max = minimum(ints, Greater()); // в max должно попасть число 15
    std::cout << "min=" << min << std::endl;
    std::cout << "max=" << max << std::endl;
}

template<class T, class Comp>
T minimum(Array<T> & p, Comp lesscmp) {
    T t = T(p[0]);
    size_t i;
    for (i = 0; i < p.size(); i++)
        if (lesscmp(p[i], t))
            t = p[i];
    return t;
}

/*
template <typename T>
class Array
{
public:
	explicit Array(size_t size = 0, const T& value = T());
	Array(const Array& other);
	~Array();
	Array& operator=(Array other);
	void swap(Array &other);
	size_t size() const;
	T& operator[](size_t idx);
	const T& operator[](size_t idx) const;

private:
	size_t size_;
	T *data_;
};
*/
// put your code here
