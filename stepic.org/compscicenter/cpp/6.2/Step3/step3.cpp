// C++
void sort(int * p, int * q);
void sort( double * p, double * q);

// C++ + OOP
struct IComparable {
    virtual int compare(IComparable * comp) const = 0;
    virtual ~IComparable() {}
};

void sort(IComparable ** p, IComparable ** q);

// C++ + templates
template <typename Type >
void sort(Type * p, Type * q);
