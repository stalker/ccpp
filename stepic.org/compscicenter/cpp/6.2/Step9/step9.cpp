#include <cstddef>
#include <iostream>
// data_->~T();
template <typename T>
class Array
{
public:
    // Список операций:
    //
    // Array(size_t size, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    Array(size_t size, const T& value = T())
                     : size_(size)
    {
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        for (size_t i = 0; i < size_; ++i)
            new (data_ + i) T(value);
    }
    //
    // Array()
    //   конструктор класса, который можно вызвать
    //   без параметров. Должен создавать пустой
    //   Array.
    Array() {
        size_ = 0;
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        // data_ = 0;
    }
    //
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Для типа T оператор
    //   присвивания не определен.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        for (size_t i = 0; i < size_; ++i)
            new (data_ + i) T(v[i]);
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()  {
        for (size_t i = 0; i < size_; i++) {
              data_[i].~T();
        }
        operator delete[](data_);
    }
    //
    // Array& operator=(...)
    //   оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            for (size_t i = 0; i < size_; i++) {
                  data_[i].~T();
            }
            operator delete[](data_);
            size_ = v.size_;
            T* buff = static_cast<T*>(operator new[] (size_ * sizeof(T)));
            for (size_t i = 0; i < size_; ++i)
                new (buff + i) T(v[i]);
        }
        return *this;
    }
    //
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элемнтов).
    size_t size() const { return size_; }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
private:
    T    * data_;
    size_t size_;
};

int l = 0;
template <typename T>
void flatten(const Array<T>& array, std::ostream& out)
{
    l++;
    // out << "level++ " << l++ << std::endl;
    size_t i;
    for (i = 0; i < array.size(); ++i)
        out << array[i] << " ";
    // out << "level-- " << l-- << std::endl;
    l--;
    if (l == 0)
        out << std::endl;
}

template <typename T>
void flatten(const Array< Array<T> > & array, std::ostream& out)
{
    l++;
    // out << "level++ " << l++ << std::endl;
    size_t i;
    for (i = 0; i < array.size(); ++i)
        flatten(array[i], out);
    // out << "level-- " << l-- << std::endl;
    l--;
    if (l == 0)
        out << std::endl;
}

int main() {
    Array<int> ints(2, 0);
    ints[0] = 10;
    ints[1] = 20;

    Array< Array<int> > array_of_ints(2, ints);

    // std::cout << "level   " << l << std::endl;
    flatten(ints, std::cout);
    flatten(array_of_ints, std::cout);
    // std::cout << "level   " << l << std::endl;

/*
    int i, j;
    for (i = 0; i < array_of_ints.size(); ++i)
        for (j = 0; j < array_of_ints[i].size(); ++j)
            cout << array_of_ints[i][j] << " ";
    cout << endl;
*/
  return 0;
}
