#include <iostream>
// В первом уроке вы реализовали простой шаблон ValueHolder, в этом задании
// мы используем его чтобы написать класс Any (интересно, что не шаблонный),
// который позволяет хранить значения любого типа! Например, вы сможете создать
// массив объектов типа Any, и сохранять в них int-ы, double-ы или даже объекты
// Array. Подробности в шаблоне кода. Hint: в нешаблонном классе Any могут быть
// шаблонные методы, например, шаблонный конструктор.
//
// Sample Input:
// There are no tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

struct ICloneable
{
    virtual ICloneable* clone() const = 0;
    virtual ~ICloneable() { }
};

// Шаблон ValueHolder с типовым параметром T,
// должен содержать одно открытое поле data_
// типа T.
//
// В шаблоне ValueHolder должен быть определен
// конструктор от одного параметра типа T,
// который инициализирует поле data_.
//
// Шаблон ValueHolder должен реализовывать
// интерфейс ICloneable, и возвращать указатель
// на копию объекта, созданную в куче, из метода
// clone.

template <typename T>
struct ValueHolder : ICloneable {
    ValueHolder(const T& data): data_(data){}
    T data_;
    ValueHolder * clone() const {
        return new ValueHolder(*this);
    }
};

// Это класс, который вам нужно реализовать
class Any
{
    ICloneable * ptr_;
public:
    // В классе Any должен быть конструктор,
    // который можно вызвать без параметров,
    // чтобы работал следующий код:
    //    Any empty; // empty ничего не хранит
    Any() : ptr_(0) { }

    // В классе Any должен быть шаблонный
    // конструктор от одного параметра, чтобы
    // можно было создавать объекты типа Any,
    // например, следующим образом:
    //    Any i(10); // i хранит значение 10

    template <class Other>
    Any(const Other & other)
    {
        ptr_ = new ValueHolder<Other>(other);
    }

    // Не забудьте про деструктор. Все выделенные
    // ресурсы нужно освободить.
    ~Any()
    {
        delete ptr_;
    }

    // В классе Any также должен быть конструктор
    // копирования (вам поможет метод clone
    // интерфейса ICloneable)
    Any(const Any & other) : ptr_(other.ptr_ ? other.ptr_->clone() : 0) { }
    // Any(const Any & a) {
    // }

    // В классе должен быть оператор присваивания и/или
    // шаблонный оператор присваивания, чтобы работал
    // следующий код:
    //    Any copy(i); // copy хранит 10, как и i
    //    empty = copy; // empty хранит 10, как и copy
    //    empty = 0; // а теперь empty хранит 0
    template <class Other>
    Any& operator=(const Other & other) {
        delete ptr_;
        ptr_ = new ValueHolder<Other>(other);
        return *this;
    }

    // Ну и наконец, мы хотим уметь получать хранимое
    // значение, для этого определите в классе Any
    // шаблонный метод cast, который возвращает
    // указатель на хранимое значение, или нулевой
    // указатель в случае несоответствия типов или
    // если объект Any ничего не хранит:
    //    int *iptr = i.cast<int>(); // *iptr == 10
    //    char *cptr = i.cast<char>(); // cptr == 0,
    //        // потому что i хранит int, а не char
    //    Any empty2;
    //    int *p = empty2.cast<int>(); // p == 0
    // При реализации используйте dynamic_cast,
    // который мы уже обсуждали ранее.
    template <class Other>
    Other * cast(void) {
        ValueHolder<T> * vh = dynamic_cast<ValueHolder<T>*>(ptr);
        if (!vh)
            return 0;
        return &(vh->data_);
    }
};

using namespace std;

int main()
{

    Any empty;
    Any i(10);
    int *iptr = i.cast<int>();
    cout << "[1] i=" << *iptr << endl;
    return 0;
}
