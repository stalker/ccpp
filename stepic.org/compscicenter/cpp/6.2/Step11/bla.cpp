// http://www.linux.org.ru/forum/development/10968185
#include <iostream>

using namespace std;

struct ICloneable
{
    virtual ICloneable* clone() const = 0;
    virtual ~ICloneable() { }
};

template <typename T>
struct ValueHolder : ICloneable {
    ValueHolder(const T& data): data_(data){}
    T data_;
    ValueHolder * clone() const {
        return new ValueHolder(*this);
    }
};

class Any
{
    ICloneable * ptr;

public:
    Any() : ptr(0) { }

    template <class value_t>
    Any(const value_t& v_) : ptr(new ValueHolder<value_t>(v_)) { }

    Any(Any const & other) : ptr(other.ptr ? other.ptr->clone() : 0) {}

    Any& operator=(Any const & other)
    {
        if(this->ptr)
            delete this->ptr;
        this->ptr = NULL;
        if (other.ptr)
        {
            this->ptr=other.ptr->clone();
        }
        return *this;
    }
    template <class A>
    Any& operator=(A const& other)
    {
        if(this->ptr)
            delete this->ptr;
        this->ptr = NULL;
        if (other)
        {
            this->ptr=new ValueHolder<A>(other);
        }
        return *this;
    }

    ~Any() { delete this->ptr; }
    template <class T>
    T* cast()
    {
        if (dynamic_cast<ValueHolder<T>*>(this->ptr))
        {
            return (T*)dynamic_cast<ValueHolder<T>*>(this->ptr);
        }
        else
        {
            return 0;
        }
    }
};


int main()
{

    Any empty;
    Any i(10);
    cout << "[1] i=" << i.cast<int>() << endl;
    Any copy(i);
    cout << "[2] copy=" << copy.cast<int>() << endl;
    empty = copy;
    cout << "[3] empty=" << empty.cast<int>() << endl;
    empty = 0;
    cout << "[4] empty=" << empty.cast<int>() << endl;
    int *iptr = i.cast<int>();
    cout << "[5] *iptr=" << iptr << endl;
    char *cptr = i.cast<char>();
    cout << "[6] *cptr=" << cptr << endl;
    Any empty2;
    int *p = empty2.cast<int>();
    cout << "[7] *p=" << p << endl;
    Any a = 20;
    cout << "[8] a=" << a.cast<int>() << endl;
    a=0;
    cout << "[9] a=" << a.cast<int>() << endl;
    a='w';
    cout << "[10] a=" << a.cast<char>() << endl;
    return 0;
}
