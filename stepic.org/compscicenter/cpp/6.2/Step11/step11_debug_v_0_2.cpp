#include <iostream>
#include <typeinfo>
// В первом уроке вы реализовали простой шаблон ValueHolder, в этом задании
// мы используем его чтобы написать класс Any (интересно, что не шаблонный),
// который позволяет хранить значения любого типа! Например, вы сможете создать
// массив объектов типа Any, и сохранять в них int-ы, double-ы или даже объекты
// Array. Подробности в шаблоне кода. Hint: в нешаблонном классе Any могут быть
// шаблонные методы, например, шаблонный конструктор.
//
// Sample Input:
// There are no tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

using namespace std;

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl

#define cerr_X_V(x,v) cerr<<#x<<(v)
#define cerr_X_V_nl(x,v) cerr_X_V(x,v)<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':';cerr_X_V(x,v)
#define cerr_func_X_V_nl(x,v) cerr_func_X_V(x,v)<<endl

#define cerr_X_V_Y(x,v,y) cerr<<#x<<(v)<<#y
#define cerr_X_V_Y_nl(x,v,y) cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)<<endl

#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

struct ICloneable
{
    virtual ICloneable* clone() const = 0;
    virtual ~ICloneable() { }
};

// Шаблон ValueHolder с типовым параметром T,
// должен содержать одно открытое поле data_
// типа T.
//
// В шаблоне ValueHolder должен быть определен
// конструктор от одного параметра типа T,
// который инициализирует поле data_.
//
// Шаблон ValueHolder должен реализовывать
// интерфейс ICloneable, и возвращать указатель
// на копию объекта, созданную в куче, из метода
// clone.

template <typename T>
struct ValueHolder : ICloneable {
    ValueHolder(const T& data): data_(data) {
        // cerr << "data_=" << data_ << endl;
        // cerr << "data=" << data << endl;
    }
    T data_;
    ValueHolder * clone() const {
        return new ValueHolder(*this);
    }
};

// Это класс, который вам нужно реализовать
class Any
{
    ICloneable * ptr_;
public:
    // В классе Any должен быть конструктор,
    // который можно вызвать без параметров,
    // чтобы работал следующий код:
    //    Any empty; // empty ничего не хранит
    Any() : ptr_(0) { }

    // В классе Any должен быть шаблонный
    // конструктор от одного параметра, чтобы
    // можно было создавать объекты типа Any,
    // например, следующим образом:
    //    Any i(10); // i хранит значение 10

    template <class Other>
    Any(const Other & other)
    {
        cerr_FILE_LINE;
        cerr_func_X_V_nl(other=, other);
        ptr_ = new ValueHolder<Other>(other);
        cerr_FILE_LINE;
        cerr_func_X_V_nl(other=, ptr_);
    }

    // Не забудьте про деструктор. Все выделенные
    // ресурсы нужно освободить.
    ~Any()
    {
        cerr_FILE_LINE;
        cerr_func_X_V_nl(ptr_=, ptr_);
        delete ptr_;
    }

    // В классе Any также должен быть конструктор
    // копирования (вам поможет метод clone
    // интерфейса ICloneable)
    Any(const Any & other) : ptr_(other.ptr_ ? other.ptr_->clone() : 0) {
        cerr_FILE_LINE;
        cerr_func_X_V_nl(ptr_=, ptr_);
    }
    // Any(const Any & a) {
    // }

    // В классе должен быть оператор присваивания и/или
    // шаблонный оператор присваивания, чтобы работал
    // следующий код:
    //    Any copy(i); // copy хранит 10, как и i
    //    empty = copy; // empty хранит 10, как и copy
    //    empty = 0; // а теперь empty хранит 0
    Any& operator=(const Any & other)
    {
        cerr_FILE_LINE;
        cerr_func_X_nl(simple operator=);
        if (ptr_ != 0)
            delete ptr_;
        delete ptr_;
        ptr_ = 0;
        if (other.ptr_) {
            cerr_FILE_LINE;
            cerr_func_X_nl(if (other.ptr_));
            ptr_ = other.ptr_->clone();
        }
        cerr_FILE_LINE;
        cerr_func_X_V_nl(ptr_=, ptr_);
        return *this;
    }
    template <class Other>
    Any& operator=(const Other & other) {
        cerr_FILE_LINE;
        cerr_func_X_nl(template operator=);
        if (ptr_ != 0)
        delete ptr_;
        ptr_ = new ValueHolder<Other>(other);
        cerr_FILE_LINE;
        cerr_func_X_V_nl(ptr_=, ptr_);
        return *this;
    }
    // Ну и наконец, мы хотим уметь получать хранимое
    // значение, для этого определите в классе Any
    // шаблонный метод cast, который возвращает
    // указатель на хранимое значение, или нулевой
    // указатель в случае несоответствия типов или
    // если объект Any ничего не хранит:
    //    int *iptr = i.cast<int>(); // *iptr == 10
    //    char *cptr = i.cast<char>(); // cptr == 0,
    //        // потому что i хранит int, а не char
    //    Any empty2;
    //    int *p = empty2.cast<int>(); // p == 0
    // При реализации используйте dynamic_cast,
    // который мы уже обсуждали ранее.
    template <class Other>
    Other * cast(void) {
        ValueHolder<Other> * vh = dynamic_cast<ValueHolder<Other>*>(ptr_);
        if (!vh) {
            cerr_FILE_LINE;
            cerr_func_X_V_nl(if (!vh) =, vh);
            return 0;
        }
        if (!vh->data_)
            return 0;
        return &(vh->data_);
    }
};


int main()
{
    Any empty;
    Any i(10);
    cout << "[1] i=" << i.cast<int>() << endl;
    cout << "[1] *i=" << *i.cast<int>() << endl;
    Any copy(i);
    cout << "[2] copy=" << copy.cast<int>() << endl;
    cout << "[2] *copy=" << *copy.cast<int>() << endl;
    empty = copy;
    cout << "[3] empty=" << empty.cast<int>() << endl;
    cout << "[3] *empty=" << *empty.cast<int>() << endl;
    cout << "[2] copy=" << copy.cast<int>() << endl;
    cout << "[2] *copy=" << *copy.cast<int>() << endl;
    empty = 0;
    cout << "[4] empty=" << empty.cast<int>() << endl;
    int *iptr = i.cast<int>();
    cout << "[5] iptr=" << iptr << endl;
    cout << "[5] *iptr=" << *iptr << endl;
    char *cptr = i.cast<char>();
    cout << "[6] cptr=" << ( cptr == NULL ? "NULL" : "ERROR" ) << endl;

    Any empty2;
    int *p = empty2.cast<int>();
    cout << "[7] p=" << ( p == NULL ? "NULL" : "ERROR" ) << endl;

    Any a = 20;
    cout << "[8] a=" << a.cast<int>() << endl;
    cout << "[8] *a=" << *a.cast<int>() << endl;

    a = 0;
    cout << "[9] a=" << a.cast<int>() << endl;

    a = 'w';
    cout << "[10] a=" << a.cast<char>() << endl;
/*
    Any empty2;
    int *p = empty2.cast<int>();
    cout << "[7] *p=" << p << endl;
    Any a = 20;
    cout << "[8] a=" << a.cast<int>() << endl;
    a=0;
    cout << "[9] a=" << a.cast<int>() << endl;
*/
    return 0;
}
