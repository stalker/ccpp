#include <cstddef>
#include <iostream>
// data_->~T();
template <typename T>
class Array
{
public:
    // Список операций:
    //
    // Array(size_t size, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    Array(size_t size, const T& value = T())
                     : size_(size)
    {
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        for (size_t i = 0; i < size_; ++i)
            new (data_ + i) T(value);
    }
    //
    // Array()
    //   конструктор класса, который можно вызвать
    //   без параметров. Должен создавать пустой
    //   Array.
    Array() {
        size_ = 0;
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        // data_ = 0;
    }
    //
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Для типа T оператор
    //   присвивания не определен.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = static_cast<T*>(operator new[] (size_ * sizeof(T)));
        for (size_t i = 0; i < size_; ++i)
            new (data_ + i) T(v[i]);
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()  {
        for (size_t i = 0; i < size_; i++) {
              data_[i].~T();
        }
        operator delete[](data_);
    }
    //
    // Array& operator=(...)
    //   оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            for (size_t i = 0; i < size_; i++) {
                  data_[i].~T();
            }
            operator delete[](data_);
            size_ = v.size_;
            T* buff = static_cast<T*>(operator new[] (size_ * sizeof(T)));
            for (size_t i = 0; i < size_; ++i)
                new (buff + i) T(v[i]);
        }
        return *this;
    }
    //
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элемнтов).
    size_t size() const { return size_; }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
private:
    T    * data_;
    size_t size_;
};
