#include <string>  // std::string
#include <cassert> // assert
#include <cmath>   // sqrt и fabs

struct Expression
{
// put your code here
    virtual ~Expression() {}
    virtual double evaluate() const = 0;
};

struct Transformer
{
    virtual ~Transformer() {}
    virtual Expression *transformNumber(Number const *) = 0;
    virtual Expression *transformBinaryOperation(BinaryOperation const *) = 0;
    virtual Expression *transformFunctionCall(FunctionCall const *) = 0;
    virtual Expression *transformVariable(Variable const *) = 0;
};

struct Number : Expression
{
    Number(double value)
        : value_(value)
    {}

    double value() const
    { return value_; }

    double evaluate() const
    { return value_; }

    Expression * transform(Transformer *tr) const
    {
        return tr->transformNumber(this);
    }
private:
    double value_;
};

struct BinaryOperation : Expression
{
    enum {
        PLUS = '+',
        MINUS = '-',
        DIV = '/',
        MUL = '*'
    };

    BinaryOperation(Expression const *left, int op, Expression const *right)
        : left_(left), op_(op), right_(right)
    { assert(left_ && right_); }

    ~BinaryOperation()
    {
        delete left_;
        delete right_;
    }

    Expression const *left() const
    { return left_; }

    Expression const *right() const
    { return right_; }

    int operation() const
    { return op_; }

    double evaluate() const
    {
        double left = left_->evaluate();
        double right = right_->evaluate();
        switch (op_)
        {
        case PLUS: return left + right;
        case MINUS: return left - right;
        case DIV: return left / right;
        case MUL: return left * right;
        }
        assert(0);
        return 0.0;
    }

    Expression * transform(Transformer *tr) const
    {
        return tr->transformNumber(this);
    }
private:
    Expression const *left_;
    Expression const *right_;
    int op_;
};

struct FunctionCall : Expression
{
    /**
     * @name имя функции, возможные варианты
     *       "sqrt" и "abs".
     *
     *       Объекты, std::string можно
     *       сравнивать с C-строками используя
     *       обычный синтаксис ==.
     *
     * @arg  выражение аргумент функции
     */
    FunctionCall(std::string const &name, Expression const *arg)
        : name_(name), arg_(arg)
    {
        assert(arg_);
        assert(name_ == "sqrt" || name_ == "abs");
    }

    // реализуйте оставшие методы из
    // интерфейса Expression и не забудьте
    // удалить arg_, как это сделано в классе
    // BinaryOperation

    std::string const & name() const
    {
        return name_;
        // put your code here
    }

    Expression const *arg() const
    {
        return arg_;
        // here
    }

    ~FunctionCall() {
        delete arg_;
    }
    // and here
    double evaluate() const
    {
        if (name_ == "sqrt") {
            return sqrt(arg_->evaluate());
        } else if (name_ == "abs") {
            return abs(arg_->evaluate());
        }
        assert(0);
        return 0.0;
    }

    Expression * transform(Transformer *tr) const
    {
        return tr->transformNumber(this);
    }
private:
    std::string const name_;
    Expression const *arg_;
};


struct Variable : Expression
{
    Variable(std::string const &name) : name_(name) { }
    std::string const &name() const { return name_; }
    double evaluate() const
    {
        assert(0);
        return 0.0;
    }
    Expression *transform(Transformer *tr) const { return tr->transformVariable(this); }
private:
    std::string const name_;
};

