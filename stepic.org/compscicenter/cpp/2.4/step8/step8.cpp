#include <stdio.h>
#include <iostream>
using namespace std;

#define MAXLINE 65536

int strstr(const char *str, const char *pattern);
int getline(char s[], int lim);

int main(){
    int i;
    char s1[65536];
    char s2[65536];

    getline(s1, MAXLINE);
    getline(s2, MAXLINE);
    i = strstr(s1, s2);
    cout << i;
    cout << endl;

    return 0;
}

int bad_strstr(const char *str, const char *pattern) {
    int k;
    char * r;
    char * s1;
    char * t1;

    s1 = (char *)str;
    for (; *s1 != '\0';) {
        t1 = (char *)pattern;
        if (*s1 == *t1) r = s1;
        for (k = 0; *s1++ == *t1++ &&  *t1 != '\0'; k++)
            ;
        if (k > 0 && *t1 == '\0')
            return r - str;
        else
           r = NULL;
    }

    return -1;
}

int strstr (const char *str, const char *pattern)
/* int strindex_l(char s[], char t[]) */
{
  int i, j, k;
  char * s = (char *)str;
  char * t = (char *)pattern;
  for (i = 0; s[i] != '\0'; i++) {
    for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
      ;
    if (k > 0 && t[k] == '\0')
      return i;
  }
  return -1;
}

/* ui_getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
    s[i] = c;
  }
  /*
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  */
  s[i] = '\0';
  return i;
}
