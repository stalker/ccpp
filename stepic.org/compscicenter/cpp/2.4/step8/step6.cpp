#include <stdio.h>
#include <iostream>
using namespace std;

#define MAXLINE 65536

unsigned strlen(const char *str);
int getline(char s[], int lim);

int main(){
    int l;
    // char s[] = "C-style string";
    char s[65536];

    getline(s, MAXLINE);
    l = strlen(s);
    cout << l;
    cout << endl;

    return 0;
}

unsigned strlen(const char *str) {
  char * p = (char *)str;

  while (*p++ != '\0') ;

  return p - str - 1;
}


/* ui_getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
    s[i] = c;
  }
  /*
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  */
  s[i] = '\0';
  return i;
}
