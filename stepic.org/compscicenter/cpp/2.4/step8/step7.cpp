#include <stdio.h>
#include <iostream>
using namespace std;

#define MAXLINE 65536

void strcat(char *to, const char *from);
int getline(char s[], int lim);

int main(){
    char s1[65536];
    char s2[65536];

    getline(s1, MAXLINE);
    getline(s2, MAXLINE);
    strcat(s1, s2);
    cout << s1;
    cout << endl;

    return 0;
}
void strcat(char *to, const char *from) {
    char * s = (char *)to;
    char * t = (char *)from;

    while (*s++)
        ;
    s--;
    while (*s++ = *t++)
        ;
    *s = '\0';
}

/* ui_getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
    s[i] = c;
  }
  /*
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  */
  s[i] = '\0';
  return i;
}
