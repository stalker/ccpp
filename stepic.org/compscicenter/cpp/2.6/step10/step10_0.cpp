#include <iostream>
using namespace std;
// #define MAXLINE 65536

char * getline(void);

int main(){
    char * r;

    // while ((r = getline()) && !(r[0] == '1' && r[1] == ':'  && r[2] == '~') && !std::cin.eof()) {
    while (!cin.eof()) {
        r = getline();
        cout << r;
        delete[] r;
    }
    return 0;
}

char * getline(void) {
    int n;
    char * s1 = new char[65535];
    char * b1 = s1;

    for(; cin.get(*s1) && *s1 != '\n' && *s1 != '\0' && s1 - b1 < 65535; s1++)
        ;
    *s1++ = '\0';
    n = s1 - b1;

    char * s2 = new char[n];
    char * b2 = s2;

    s1 = b1;
    while (*s2++ = *s1++)
        ;
    *s2 = '\0';
    delete[] b1;

    return b2;
}
