#include <iostream>
using namespace std;

char * getline(void) {
    int l;
    char a[99999];
    char * p1;
    char * p2;
    char * s;

    p1 = a;
    do {
        cin.get(*p1);
    } while (!cin.eof() && *p1++ != '\n');
    if (*(p1 - 1) == '\n')
        p1--;
    *p1 = '\0';
    l = p1 - a;
    s = new char[l+1];
    p1 = s;
    p2 = a;
    while ((*p1++ = *p2++) != '\0')
      ;
    return s;
}
