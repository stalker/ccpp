#include <iostream>
using namespace std;
// #define MAXLINE 65536

char * getline(void);

int main(){
    char * r;

    // while ((r = getline()) && !(r[0] == '1' && r[1] == ':'  && r[2] == '~') && !std::cin.eof()) {
    while (!cin.eof()) {
        r = getline();
        cout << r;
        cout << endl;
        delete[] r;
    }
    return 0;
}

char * getline(void) {
    int l;
    char a[99999];
    char * p1;
    char * p2;
    char * s;

    p1 = a;
    do {
        cin.get(*p1);
    } while (!cin.eof() && *p1++ != '\n');
    if (*(p1 - 1) == '\n')
        p1--;
    *p1 = '\0';
    l = p1 - a;
    s = new char[l+1];
    p1 = s;
    p2 = a;
    while ((*p1++ = *p2++) != '\0')
      ;
    return s;
}
