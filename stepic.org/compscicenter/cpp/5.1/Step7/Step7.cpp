struct BigNum {
    BigNum & operator++() { // prefix
        // increment
        // ...
        return *this;
    }
    BigNum & operator++(int) { // postfix
        BigNum tmp(*this);
        ++(*this);
        return tmp;
    }
}
