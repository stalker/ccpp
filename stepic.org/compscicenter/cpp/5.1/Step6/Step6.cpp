#include <cstdlib>

struct Vector {
    Vector(double xx, double yy)
          :x(xx), y(yy)
    { }
    Vector operator-() const {
        return Vector(-x, -y);
    }
    Vector operator-(Vector const& p) const {
        return Vector(x - p.x, y - p.y);
    }
    Vector operator*=(double d) {
        x *= d;
        y *= d;
        return *this;
    }
    Vector operator+(Vector const& w) {
        return Vector(x + w.x, y + w.y);
    }
    Vector operator*(double d) {
        return Vector(x * d, y * d);
    }
    double operator[](size_t i) const {
        return (i == 0) ? x : y;
    }
    bool operator()(double d) {}
    bool operator()(double a, double b) {}
    double x, y;
};
/*
Vector operator*(Vector const& v, double d) {
    return Vector(v.x * d, v.y * d);
}
Vector operator*(double d, Vector const& v) {
    return v * d;
}
*/
