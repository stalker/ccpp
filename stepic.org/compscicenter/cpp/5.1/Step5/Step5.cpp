Vector operator-(Vector const& v) {
    return Vector(-v.x, -v.y);
}

Vector operator+(Vector const& v, Vector const& w) {
    return Vector(v.x + w.x, v.y + w.y);
}

Vector operator*(Vector const& v, double d) {
    return Vector(v.x * d, v.y * d);
}

Vector operator*(double d, Vector const& v) {
    return v * d;
}
