struct SmartPtr {
    SmartPtr(int n = 1) {
        data_ = new int[n];
    }
    ~SmartPtr() {
        delete [] data_;
    }
    int & operator*()  const {return *data_;}
    int * operator->() const {return  data_;}
    int * get()        const {return  data_;}
private:
    int * data_;
    int size;
};

bool operator==(SmartPtr const& p1, SmartPtr const& p2) {
    return p1.get() == p2.get();
}
