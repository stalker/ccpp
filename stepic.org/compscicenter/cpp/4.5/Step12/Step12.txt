Теперь посмотрим на новый класс Variable:

struct Variable : Expression
{
    Variable(std::string const &name) : name_(name) { }
    std::string const &name() const { return name_; }
    double evaluate() const
    {
        assert(0);
        return 0.0;
    }
    Expression *transform(Transformer *tr) const { return tr->transformVariable(this); }
private:
    std::string const name_;
};

Класс не представляет из себя ничего особенного, за исключением метода evaluate. Мы пока не знаем, откуда взять значение  переменной, поэтому вызов evaluate у переменной считаем ошибкой и используем assert (мы исправим это в будущем).
