// Step13.cpp
// Version 0.1
// Реализуйте класс CopySyntaxTree, который, используя шаблон Visitor, выполняет
// копирование AST. Интерфейсы всех используемых классов приведены для удобства
// — не изменяйте их. Шаблон класса CopySyntaxTree находится в самом низу.
//
// Sample Input:
// There is not tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

#include <cmath>
#include <string>
#include <cassert> // assert
#include <iostream>

using namespace std;

struct Transformer;
struct Number;
struct BinaryOperation;
struct FunctionCall;
struct Variable;

struct Expression
{
    virtual ~Expression() { }
    virtual double evaluate() const = 0;
    virtual Expression *transform(Transformer *tr) const = 0;
};

struct Transformer
{
    virtual ~Transformer() { }
    virtual Expression *transformNumber(Number const *) = 0;
    virtual Expression *transformBinaryOperation(BinaryOperation const *) = 0;
    virtual Expression *transformFunctionCall(FunctionCall const *) = 0;
    virtual Expression *transformVariable(Variable const *) = 0;
};

// Begin Number
struct Number : Expression
{
    Number(double value);
    double value() const;
    double evaluate() const;
    Expression *transform(Transformer *tr) const;

private:
    double value_;
};
// End Number

// Begin BinaryOperation
struct BinaryOperation : Expression
{
    enum {
        PLUS = '+',
        MINUS = '-',
        DIV = '/',
        MUL = '*'
    };
    BinaryOperation(Expression const *left, int op, Expression const *right);
    ~BinaryOperation();
    double evaluate() const;
    Expression *transform(Transformer *tr) const;
    Expression const *left() const;
    Expression const *right() const;
    int operation() const;

private:
    Expression const *left_;
    Expression const *right_;
    int op_;
};
// End BinaryOperation

// Begin FunctionCall
struct FunctionCall : Expression
{
    FunctionCall(std::string const &name, Expression const *arg);
    ~FunctionCall();
    double evaluate() const;
    Expression *transform(Transformer *tr) const;
    std::string const &name() const;
    Expression const *arg() const;

private:
    std::string const name_;
    Expression const *arg_;
};
// FunctionCall

// Begin Variable
struct Variable : Expression
{
    Variable(std::string const &name);
    // Variable(std::string const &name) : name_(name) { }
    std::string const & name() const;
    double evaluate() const;
    Expression *transform(Transformer *tr) const;

private:
    std::string const name_;
};
// End Variable

/**
 * реализуйте все необходимые методы класса
 * вы можете определять любые вспомогательные
 * методы, если хотите
 */
struct CopySyntaxTree : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        return new BinaryOperation(binop->left()->transform(this), binop->operation(), binop->right()->transform(this));
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    { return new FunctionCall(fcall->name(), fcall->arg()->transform(this)); }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};

Expression * Number::transform(Transformer *tr) const
{
    return tr->transformNumber(this);
}


int main() {
  Expression *expression = new Number(10.0);
  Transformer *transformer = new CopySyntaxTree();
  Expression *new_expression = expression->transform(transformer);

  delete expression;
  delete transformer;
  cout<<"number: "<<new_expression->evaluate()<<endl;

  delete new_expression;

  expression = new Variable("x");
  transformer = new CopySyntaxTree();
  new_expression = expression->transform(transformer);
  delete expression;
  delete transformer;
  // cout<<"new variable: "<<new_expression->evaluate()<<endl;
  delete new_expression;
  enum { PLUS = '+', MINUS = '-', DIV = '/', MUL = '*' };
  struct Number *x = new Number(2);
  struct Number *y = new Number(3);

  expression = new BinaryOperation(x, MUL, y);
  transformer = new CopySyntaxTree();
  new_expression = expression->transform(transformer);

  delete expression;
  delete transformer;
  cout<<"binary operation: "<<new_expression->evaluate()<<endl;
  delete new_expression;

  x = new Number(2);
  expression = new FunctionCall("sqrt",x);
  transformer = new CopySyntaxTree();
  new_expression = expression->transform(transformer);

  delete expression;
  delete transformer;
  cout<<"function call: "<<new_expression->evaluate()<<endl;
  delete new_expression;

  //system("pause");
  return 0;
}

// Begin Number
Number::Number(double value)
              : value_(value)
{}

double Number::value() const
{ return value_; }

double Number::evaluate() const
{ return value_; }
// End Number

// Begin BinaryOperation
BinaryOperation::BinaryOperation(Expression const *left, int op, Expression const *right)
        : left_(left), op_(op), right_(right)
{ assert(left_ && right_); }

BinaryOperation::~BinaryOperation()
{
    delete left_;
    delete right_;
}

double BinaryOperation::evaluate() const
{
    double left = left_->evaluate();
    double right = right_->evaluate();
    switch (op_)
    {
    case PLUS: return left + right;
    case MINUS: return left - right;
    case DIV: return left / right;
    case MUL: return left * right;
    }
    assert(0);
    return 0.0;
}

Expression * BinaryOperation::transform(Transformer *tr) const
{
    return tr->transformBinaryOperation(this);
}

Expression const *BinaryOperation::left() const
{ return left_; }

Expression const *BinaryOperation::right() const
{ return right_; }

int BinaryOperation::operation() const
{ return op_; }
// End BinaryOperation

// Begin FunctionCall
//    std::string const &name() const;
//    Expression const *arg() const;
//
FunctionCall::FunctionCall(std::string const &name, Expression const *arg)
        : name_(name), arg_(arg)
{
    assert(arg_);
    assert(name_ == "sqrt" || name_ == "abs");
}

FunctionCall::~FunctionCall() {
    delete arg_;
}

double FunctionCall::evaluate() const
{
    if (name_ == "sqrt") {
        return sqrt(arg_->evaluate());
    } else if (name_ == "abs") {
        return abs(arg_->evaluate());
    }
    assert(0);
    return 0.0;
}

Expression * FunctionCall::transform(Transformer *tr) const
{
    return tr->transformFunctionCall(this);
}

std::string const & FunctionCall::name() const
{
    return name_;
    // put your code here
}

Expression const *FunctionCall::arg() const
{
    return arg_;
    // here
}
// Begin FunctionCall

// Begin Variable
Variable::Variable(std::string const &name) : name_(name) { }

std::string const &Variable::name() const { return name_; }

double Variable::evaluate() const
{
    assert(0);
    return 0.0;
}

Expression * Variable::transform(Transformer *tr) const
{
    return tr->transformVariable(this);
}
// End Variable
