#include <iostream>
#include <string>
using namespace std;
 
class Foo;
class Bar;
class Baz;
 
class Visitor{
public:
  virtual void visit(Foo&ref) = 0;
  virtual void visit(Bar&ref) = 0;
  virtual void visit(Baz&ref) = 0;
};

class Element{
public:
  virtual void accept(Visitor&v) = 0;
};

class Foo:public Element{
public:
  void accept(Visitor&v){v.visit(*this);}
};

class Bar:public Element{
public:
  void accept(Visitor&v){v.visit(*this);}
};

class Baz:public Element{
public:
  void accept(Visitor&v){v.visit(*this);}
};

class GetType:public Visitor{
public:
  string value;
public:
  void visit(Foo &){value="Foo";}
  void visit(Bar &){value="Bar";}
  void visit(Baz &){value="Baz";}
};

int main()
{
  Foo foo;
  Bar bar;
  Baz baz;
  // An array of elements
  Element *elements[] = {&foo,&bar,&baz,0};

  // Iterate over elements and get their type
  for(Element **it = &elements[0]; *it; ++it)
  {
    GetType visitor;
    (*it)->accept(visitor);
    cout << visitor.value << endl;
  }
  return 0;
}
