// Step7.cpp
// Version 0.2
// Задача на программирование. Дан неориентированный граф, содержащий n вершин
// и m рёбер (1≤n≤1000, 0≤m≤1000). Необходимо вывести количество компонент
// связности в заданном графе.
// Сроки сдачи задания:
//
// Soft дедлайн 27.10.2014 23:59 MSK
//
// Hard дедлайн 03.11.2014 23:59 MSK
//
// Sample Input:
//
// 4 2
// 1 2
// 3 2
//
// Sample Output:
//
// 2
//
// Memory Limit: 256 MB
//
// Time Limit: 5 seconds

#include <iostream>
#define SIZE 2002
using namespace std;
int m, n, nn, ccnum = 0;
int  visited[SIZE];
bool matrix[SIZE][SIZE];
void set_path(int a, int b) {
    matrix[a][b] = true;
}
int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = ccnum;
    if (matrix[v][target] == true || matrix[target][v] == true) {
        visited[target] = ccnum;
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true && visited[i] == 0) {
            if(explore(i, target) == 1)
                return 1;
        }
        if (matrix[i][v] == true && visited[i] == 0) {
            if(explore(i, target) == 1)
                return 1;
        }
    }
    return result;
}
int dfs(int v, int target) {
    int register i;
    int register j;
    visited[v] = ccnum;
    if (matrix[v][target] == true || matrix[target][v] == true) {
          visited[target] = ccnum;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true)
            if(explore(i, target) == 1)
                return 1;
        if (matrix[i][v] == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        set_path(b, a);
    }
    for (i = 1; i < nn; i++)
        visited[i] = 0;
    ccnum=1;
    dfs(1, 2001);
    for (i = 2; i < nn; i++) {
        if (visited[i] == 0) {
            for (j = 1; j < nn; j++) {
                if (visited[j] != 0)
                    notfound = true;
                else {
                    notfound = false;
                    break;
                }
            }
            if (notfound)
                break;
            else
                ccnum++;
            dfs(i, 2001);
        }
    }
    cout << ccnum << endl;
    return 0;
}
