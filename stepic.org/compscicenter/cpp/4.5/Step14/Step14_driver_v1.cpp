// Step13.cpp
// Version 0.1
// Реализуйте класс CopySyntaxTree, который, используя шаблон Visitor, выполняет
// копирование AST. Интерфейсы всех используемых классов приведены для удобства
// — не изменяйте их. Шаблон класса CopySyntaxTree находится в самом низу.
//
// Sample Input:
// There is not tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

#include <cmath>
#include <string>
#include <cassert> // assert
#include <iostream>

using namespace std;

#define DEBUG 2

struct Transformer;
struct Number;
struct BinaryOperation;
struct FunctionCall;
struct Variable;

struct Expression
{
    virtual ~Expression() { }
    virtual double evaluate() const = 0;
    virtual Expression *transform(Transformer *tr) const = 0;
};

struct Transformer
{
    virtual ~Transformer() { }
    virtual Expression *transformNumber(Number const *) = 0;
    virtual Expression *transformBinaryOperation(BinaryOperation const *) = 0;
    virtual Expression *transformFunctionCall(FunctionCall const *) = 0;
    virtual Expression *transformVariable(Variable const *) = 0;
};

// Begin Number
struct Number : Expression
{
    Number(double value);
    double value() const;
    double evaluate() const;
    Expression *transform(Transformer *tr) const;

private:
    double value_;
};
struct Number *x3 = new Number(10);
struct Number *y3 = new Number(6);
struct Number *x2 = new Number(10);
struct Number *y2 = new Number(6);
struct Number *pi = new Number(3.14);
// End Number

// Begin BinaryOperation
struct BinaryOperation : Expression
{
    enum {
        PLUS = '+',
        MINUS = '-',
        DIV = '/',
        MUL = '*'
    };
    BinaryOperation(Expression const *left, int op, Expression const *right);
    ~BinaryOperation();
    double evaluate() const;
    Expression *transform(Transformer *tr) const;
    Expression const *left() const;
    Expression const *right() const;
    int operation() const;

private:
    Expression const *left_;
    Expression const *right_;
    int op_;
};
// End BinaryOperation

// Begin FunctionCall
struct FunctionCall : Expression
{
    FunctionCall(std::string const &name, Expression const *arg);
    ~FunctionCall();
    double evaluate() const;
    Expression *transform(Transformer *tr) const;
    std::string const &name() const;
    Expression const *arg() const;

private:
    std::string const name_;
    Expression const *arg_;
};
// FunctionCall

// Begin Variable
struct Variable : Expression
{
    Variable(std::string const &name);
    // Variable(std::string const &name) : name_(name) { }
    std::string const & name() const;
    double evaluate() const;
    Expression *transform(Transformer *tr) const;

private:
    std::string const name_;
};
// End Variable

/**
 * реализуйте все необходимые методы класса
 * вы можете определять любые вспомогательные
 * методы, если хотите
 */
struct CopySyntaxTree : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        return new BinaryOperation(binop->left()->transform(this), binop->operation(), binop->right()->transform(this));
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    { return new FunctionCall(fcall->name(), fcall->arg()->transform(this)); }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};

Expression * Number::transform(Transformer *tr) const
{
    return tr->transformNumber(this);
}

/**
 * реализуйте все необходимые методы
 * если считаете нужным, то можете
 * заводить любые вспомогательные
 * методы
 */
struct FoldConstants : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        // Expression *expression = parse(code);
        Expression *e1_right, *e2_left;
        Number *number1 = dynamic_cast<Number *>((Expression *)binop->left());
        Number *number2 = dynamic_cast<Number *>((Expression *)binop->right());
#if DEBUG > 1
        if (number1)
            cout << "transformBinaryOperation 1: binop->left()       It's a number" << endl;
        else
            cout << "transformBinaryOperation 1: binop->left()  It is not a number" << endl;

        if (number2)
            cout << "transformBinaryOperation 1: binop->right()      It's a number" << endl;
        else
            cout << "transformBinaryOperation 1: binop->right() It is not a number" << endl;
#endif
        if (number1 == NULL) {
            e2_left = binop->left()->transform(this);
            number1 = dynamic_cast<Number *>(e2_left);
            if (number1) {
#if DEBUG > 1
                cout<<"transformBinaryOperation 2: "<<e2_left->evaluate()<<endl;
#endif
            }
        } else
            e2_left = number1;
        if (number2 == NULL) {
            e1_right = binop->right()->transform(this);
            number2 = dynamic_cast<Number *>(e1_right);
            if (number2) {
#if DEBUG > 1
                cout<<"transformBinaryOperation 3: "<<e1_right->evaluate()<<endl;
#endif
            }
        } else
            e1_right = number2;
#if DEBUG > 1
        if (number1)
            cout << "transformBinaryOperation 4: binop->left()       It's a number" << endl;
        else
            cout << "transformBinaryOperation 4: binop->left()  It is not a number" << endl;

        if (number2)
            cout << "transformBinaryOperation 4: binop->right()      It's a number" << endl;
        else
            cout << "transformBinaryOperation 4: binop->right() It is not a number" << endl;
#endif
        if (number1 != NULL && number2 != NULL) {
            Number *numberr = new Number(binop->evaluate());
#if DEBUG > 1
            cout<<"transformBinaryOperation 5: "<<binop->evaluate()<<numberr<<endl;
#endif
            return numberr;
        }
        // return (Expression *)binop;
        BinaryOperation *bo = new BinaryOperation(e2_left,
                                      binop->operation(), e1_right
                                  );
        return bo;
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    { }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};


// int main() {
int main() {

  Expression *x_plus_y = new BinaryOperation(x3, BinaryOperation::PLUS, y3);
  cout<<"x_plus_y: "<<x_plus_y->evaluate()<<endl;
  Expression *x_minus_y = new BinaryOperation(x2, BinaryOperation::MINUS, y2);
  cout<<"x_minus_y: "<<x_minus_y->evaluate()<<endl;
  Expression *expression0 = new BinaryOperation(pi, BinaryOperation::PLUS, x_plus_y);
  cout<<"expression0: "<<expression0->evaluate()<<endl;
  Expression *expression1 = new BinaryOperation(expression0, BinaryOperation::MINUS, x_minus_y);
  Transformer *transformer1 = new FoldConstants();
  Expression *new_expression1 = expression1->transform(transformer1);
  delete transformer1;
  // delete x_plus_y;
  // delete x_minus_y;
  delete expression1;
  // delete expression0;
  cout<<"binary operation 1: "<<new_expression1->evaluate()<<endl;
  return 0;

  Number *number1 = dynamic_cast<Number *>((Expression *)expression1);
  BinaryOperation *bo1 = dynamic_cast<BinaryOperation *>((Expression *)expression1);
  if (number1) {
      cout<<"yes"<<endl;
      delete number1;
  }
  if (bo1) {
      delete bo1;
  }


  delete expression0;
  delete x_minus_y;
  delete x_plus_y;
  cout<<"binary operation 1: "<<new_expression1->evaluate()<<endl;
// Begin My
  struct Variable *z = new Variable("z");
  // Expression *myexpression1 = new BinaryOperation(z, BinaryOperation::PLUS, y1);

//  cout<<"myexpression1: "<<myexpression1->evaluate()<<endl;

// End My
/*
  Expression *expression2 = new FunctionCall("sqrt", new_expression1);
  Transformer *transformer2 = new FoldConstants();
  Expression *new_expression2 = expression2->transform(transformer1);

  cout<<"function call 1: "<<new_expression2->evaluate()<<endl;

  Expression *expression3 = new FunctionCall("sqrt", new_expression2);
  Transformer *transformer3 = new FoldConstants();
  Expression *new_expression3 = expression3->transform(transformer1);

  cout<<"function call 2: "<<new_expression3->evaluate()<<endl;

  Expression *expression4 = new BinaryOperation(new_expression3,BinaryOperation::DIV,y);
  Transformer *transformer4 = new FoldConstants();
  Expression *new_expression4 = expression4->transform(transformer1);

  cout<<"binary operation 2: "<<new_expression4->evaluate()<<endl;
  //system("pause");
*/
  return 0;
}
//  return 0;

// Begin Number
Number::Number(double value)
              : value_(value)
{}

double Number::value() const
{ return value_; }

double Number::evaluate() const
{ return value_; }
// End Number

// Begin BinaryOperation
BinaryOperation::BinaryOperation(Expression const *left, int op, Expression const *right)
        : left_(left), op_(op), right_(right)
{ assert(left_ && right_); }

BinaryOperation::~BinaryOperation()
{
    delete left_;
    delete right_;
}

double BinaryOperation::evaluate() const
{
    double left = left_->evaluate();
    double right = right_->evaluate();
    switch (op_)
    {
    case PLUS: return left + right;
    case MINUS: return left - right;
    case DIV: return left / right;
    case MUL: return left * right;
    }
    assert(0);
    return 0.0;
}

Expression * BinaryOperation::transform(Transformer *tr) const
{
    return tr->transformBinaryOperation(this);
}

Expression const *BinaryOperation::left() const
{ return left_; }

Expression const *BinaryOperation::right() const
{ return right_; }

int BinaryOperation::operation() const
{ return op_; }
// End BinaryOperation

// Begin FunctionCall
//    std::string const &name() const;
//    Expression const *arg() const;
//
FunctionCall::FunctionCall(std::string const &name, Expression const *arg)
        : name_(name), arg_(arg)
{
    assert(arg_);
    assert(name_ == "sqrt" || name_ == "abs");
}

FunctionCall::~FunctionCall() {
    delete arg_;
}

double FunctionCall::evaluate() const
{
    if (name_ == "sqrt") {
        return sqrt(arg_->evaluate());
    } else if (name_ == "abs") {
        return abs(arg_->evaluate());
    }
    assert(0);
    return 0.0;
}

Expression * FunctionCall::transform(Transformer *tr) const
{
    return tr->transformFunctionCall(this);
}

std::string const & FunctionCall::name() const
{
    return name_;
    // put your code here
}

Expression const *FunctionCall::arg() const
{
    return arg_;
    // here
}
// Begin FunctionCall

// Begin Variable
Variable::Variable(std::string const &name) : name_(name) { }

std::string const &Variable::name() const { return name_; }

double Variable::evaluate() const
{
    assert(0);
    return 0.0;
}

Expression * Variable::transform(Transformer *tr) const
{
    return tr->transformVariable(this);
}
// End Variable
