#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

#include <cstddef>
#include <cstring>

struct String {
    String(const char *str = "");
    String(size_t n, char c);
    ~String();

    size_t size();
    char &at(size_t idx);
    const char *c_str();

    int compare(String &str) {
        return strcmp(str_, str.c_str());
    }

    size_t size_;
    char *str_;
};

char t1[] = "There is no test input";
char t2[] = "There is no test inpu";
char t3[] = "There is no test inpua";
char t4[] = "There is no test inpuz";

int main(void) {
    String s1(t1);
    String s2(t2);
    String s3(t3);
    String s4(t4);
    cout << "t1 > t2 => " << s1.compare(s2) << endl;
    cout << "t1 > t3 => " << s1.compare(s3) << endl;
    cout << "t1 > t4 => " << s1.compare(s4) << endl;

    return 0;
}

String::String(size_t n, char c) {
    size_t i = 0;
    size_ = n;
    str_ = new char[size_ + 1];
    for(i = 0; i < size_; i++)
      str_[i] = c;
    str_[i] = '\0';
}

String::String(const char *str) {
    char * p1, *p2;
    p1 = (char *)str;
    while (*p1 != '\0')
        p1++;
    size_ = p1 - str;
    str_ = new char[size_];
    p1 = (char *)str;
    p2 = str_;
    while (*p1 != '\0')
        *p2++ = *p1++;
    *p2 = '\0';
}

String::~String() {
    delete [] str_;
}

size_t String::size() {
    return size_;
}

char &String::at(size_t idx) {
    str_[idx];
}

    const char *String::c_str() {
    return str_;
}

