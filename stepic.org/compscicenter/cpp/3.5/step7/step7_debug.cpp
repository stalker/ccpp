#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstdio>
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String {
    String(const char *str = "");
    String(size_t n, char c);
    ~String();

    size_t size();
    char &at(size_t idx);
    const char *c_str();
    int compare(String &str);

    int append(String &str) {
        size_t i, j;
        size_t nsize = size_ + str.size();
        size_t n = nsize > size_ ? nsize : size_;
        char * nstr = new char[n];
        for(i = 0; i < size_; i++)
            nstr[i] = str_[i];
        nstr[i] = '\0';
        delete [] str_;
        str_ = nstr;
        for(j = 0; i < n; i++, j++)
            str_[i] = str.at(j);
        str_[i] = '\0';
        size_ = nsize;
    }

private:
    size_t size_;
    char *str_;
};

char t1[] = "There is no test input";
char t2[] = "There is no test inpu ";
char t3[] = "There is no test inpua";
char t4[] = "There is no test inpuz";

int main(void) {
    String s1(t1);
    String s2(t2);
    String s3(t3);
    String s4(t4);
    cout << "s1 = " << s1.c_str() << endl;
    cout << "s2 = " << s2.c_str() << endl;
    s2.append(s1);
    cout << "s2 + s1 => " << s2.c_str() << endl;

    return 0;
}

String::String(size_t n, char c) {
    size_t i = 0;
    size_ = n;
    str_ = new char[size_ + 1];
    for(i = 0; i < size_; i++)
      str_[i] = c;
    str_[i] = '\0';
}

String::String(const char *str) {
    char * p1, *p2;
    p1 = (char *)str;
    while (*p1 != '\0')
        p1++;
    size_ = p1 - str;
    str_ = new char[size_];
    p1 = (char *)str;
    p2 = str_;
    while (*p1 != '\0')
        *p2++ = *p1++;
    *p2 = '\0';
}

String::~String() {
    delete [] str_;
}

size_t String::size() {
    return size_;
}

char &String::at(size_t idx) {
    return str_[idx];
}

const char * String::c_str() {
    return str_;
}

int String::compare(String &str) {
    return strcmp(str_, str.c_str());
}

