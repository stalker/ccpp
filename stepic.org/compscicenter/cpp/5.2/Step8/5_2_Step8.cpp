#include <iostream>

using namespace std;

struct Rational
{
    Rational(int numerator = 0, int denominator = 1);

    void add(Rational rational);
    void sub(Rational rational);
    void mul(Rational rational);
    void div(Rational rational);

    void neg();
    void inv();
    double to_double() const;


    Rational operator-() const { return Rational(-numerator_, denominator_); }
    Rational operator+() const { return Rational(+numerator_, denominator_); }

    Rational& operator+=(Rational const& r) { add(r); return *this; }
    Rational& operator-=(Rational const& r) { sub(r); return *this; }
    Rational& operator*=(Rational const& r) { mul(r); return *this; }
    Rational& operator/=(Rational const& r) { div(r); return *this; }

    Rational& operator+=(int n) { add(Rational(n)); return *this; }
    Rational& operator-=(int n) { sub(Rational(n)); return *this; }
    Rational& operator*=(int n) { mul(Rational(n)); return *this; }
    Rational& operator/=(int n) { div(Rational(n)); return *this; }
    Rational operator-(Rational const& r2) const {
        Rational r1(numerator_, denominator_);
        return r1 -= r2;
    }
/*
    Rational operator+(Rational const& r2) const {
        Rational r1(numerator_, denominator_);
        return r1 += r2;
    }
    Rational operator*(Rational const& r2) const {
        Rational r1(numerator_, denominator_);
        return r1 *= r2;
    }
*/
    Rational operator/(Rational const& r2) const {
        Rational r1(numerator_, denominator_);
        return r1 /= r2;
    }
    operator double () const {
        return to_double();
    }


private:
    int numerator_;
    unsigned denominator_;
};

Rational::Rational(int numerator, int denominator)
: numerator_(numerator), denominator_(denominator)
{ }

void Rational::add(Rational rational) {numerator_ += rational.to_double();}
void Rational::sub(Rational rational) {numerator_ -= rational.to_double();}
void Rational::mul(Rational rational) {numerator_ *= rational.to_double();}
void Rational::div(Rational rational) {numerator_ /= rational.to_double();}

void Rational::neg() {numerator_ = -numerator_;}
void Rational::inv() {numerator_ = -numerator_;}
double Rational::to_double() const {return (const double)numerator_;}


Rational const operator-(const int n, Rational const& r) {
    Rational r1(n);
    return r1 -= r;
}
/*
Rational const operator+(const int n, Rational const& r) {
    Rational r1(n);
    return r1 += r;
}
*/
Rational const operator*(const int n, Rational const& r) {
    Rational r1(n);
    return r1 *= r;
}

Rational const operator/(const int n, Rational const& r) {
    Rational r1(n);
    return r1 /= r;
}

bool operator==(Rational const& a, Rational const& b) {
    return (a.to_double() == b.to_double());
}

bool operator!=(Rational const& a, Rational const& b) {
    return !(a == b);
}

bool operator<(Rational const& a, Rational const& b) {
    return (a.to_double() < b.to_double());
}

bool operator>(Rational const& a, Rational const& b) {
    return b < a;
}

bool operator<=(Rational const& a, Rational const& b) {
    return !(b < a);
}

bool operator>=(Rational const& a, Rational const& b) {
    return !(a < b);
}

/*
Rational& Rational::operator-(Rational const& r2) {
    Rational r1(*this);
    return r1 -= r2;
}

Rational& Rational::operator+(Rational const& r2) {
    Rational r1(*this);
    return r1 += r2;
}

Rational& Rational::operator*(Rational const& r2) {
    Rational r1(*this);
    return r1 *= r2;
}

Rational& Rational::operator/(Rational const& r2) {
    Rational r1(*this);
    return r1 /= r2;
}
*/

int main(void) {
    int z = 0;
    int i = 1;
    Rational r0(-1);
    Rational r1(1);
    Rational r2(2);
    Rational rx;
    cout << -r0.to_double() << endl;
    cout << -r1.to_double() << endl;
    r2 += 1;
    cout << r2.to_double() << endl;
    r2 -= 1;
    cout << r2.to_double() << endl;
    r2 *= 2;
    cout << r2.to_double() << endl;
    r2 /= 2;
    cout << r2.to_double() << endl;

    rx = r1 + r2;
    cout << rx.to_double() << endl;
    rx = r1 - r2;
    cout << rx.to_double() << endl;

    rx = r1 * z;
    cout << rx.to_double() << endl;

    rx = r1 + i;
    cout << rx.to_double() << endl;

    rx = 10 + r1;
    cout << rx.to_double() << endl;

    cout << (double)rx << endl;

    return 0;
}
