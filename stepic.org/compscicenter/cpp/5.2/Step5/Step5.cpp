bool operator==(String const& a, String const& b) {
    return true;
}

bool operator!=(String const& a, String const& b) {
    return !(a == b);
}

bool operator<(String const& a, String const& b) {
    return true;
}

bool operator>(String const& a, String const& b) {
    return b < a;
}

bool operator<=(String const& a, String const& b) {
    return !(b < a);
}

bool operator>=(String const& a, String const& b) {
    return !(a < b);
}
