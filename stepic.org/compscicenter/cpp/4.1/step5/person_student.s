	.file	"person_student.cpp"
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZN6PersonC2ESsi,"axG",@progbits,_ZN6PersonC5ESsi,comdat
	.align 2
	.weak	_ZN6PersonC2ESsi
	.type	_ZN6PersonC2ESsi, @function
_ZN6PersonC2ESsi:
.LFB970:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSsC1ERKSs
	movq	-8(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, 8(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE970:
	.size	_ZN6PersonC2ESsi, .-_ZN6PersonC2ESsi
	.weak	_ZN6PersonC1ESsi
	.set	_ZN6PersonC1ESsi,_ZN6PersonC2ESsi
	.section	.text._ZNK6Person4nameEv,"axG",@progbits,_ZNK6Person4nameEv,comdat
	.align 2
	.weak	_ZNK6Person4nameEv
	.type	_ZNK6Person4nameEv, @function
_ZNK6Person4nameEv:
.LFB972:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSsC1ERKSs
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE972:
	.size	_ZNK6Person4nameEv, .-_ZNK6Person4nameEv
	.section	.text._ZNK6Person3ageEv,"axG",@progbits,_ZNK6Person3ageEv,comdat
	.align 2
	.weak	_ZNK6Person3ageEv
	.type	_ZNK6Person3ageEv, @function
_ZNK6Person3ageEv:
.LFB973:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	8(%rax), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE973:
	.size	_ZNK6Person3ageEv, .-_ZNK6Person3ageEv
	.section	.text._ZN6PersonD2Ev,"axG",@progbits,_ZN6PersonD5Ev,comdat
	.align 2
	.weak	_ZN6PersonD2Ev
	.type	_ZN6PersonD2Ev, @function
_ZN6PersonD2Ev:
.LFB976:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE976:
	.size	_ZN6PersonD2Ev, .-_ZN6PersonD2Ev
	.weak	_ZN6PersonD1Ev
	.set	_ZN6PersonD1Ev,_ZN6PersonD2Ev
	.section	.text._ZN7StudentC2ESsiSs,"axG",@progbits,_ZN7StudentC5ESsiSs,comdat
	.align 2
	.weak	_ZN7StudentC2ESsiSs
	.type	_ZN7StudentC2ESsiSs, @function
_ZN7StudentC2ESsiSs:
.LFB978:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA978
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-48(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB0:
	call	_ZNSsC1ERKSs
.LEHE0:
	movq	-40(%rbp), %rax
	movl	-52(%rbp), %edx
	leaq	-32(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB1:
	call	_ZN6PersonC2ESsi
.LEHE1:
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
.LEHB2:
	call	_ZNSsD1Ev
.LEHE2:
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-64(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
.LEHB3:
	call	_ZNSsC1ERKSs
.LEHE3:
	jmp	.L13
.L11:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB4:
	call	_Unwind_Resume
.LEHE4:
.L12:
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6PersonD2Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB5:
	call	_Unwind_Resume
.LEHE5:
.L13:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE978:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA978:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE978-.LLSDACSB978
.LLSDACSB978:
	.uleb128 .LEHB0-.LFB978
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB978
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L11-.LFB978
	.uleb128 0
	.uleb128 .LEHB2-.LFB978
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB978
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L12-.LFB978
	.uleb128 0
	.uleb128 .LEHB4-.LFB978
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB978
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE978:
	.section	.text._ZN7StudentC2ESsiSs,"axG",@progbits,_ZN7StudentC5ESsiSs,comdat
	.size	_ZN7StudentC2ESsiSs, .-_ZN7StudentC2ESsiSs
	.weak	_ZN7StudentC1ESsiSs
	.set	_ZN7StudentC1ESsiSs,_ZN7StudentC2ESsiSs
	.section	.text._ZNK7Student10universityEv,"axG",@progbits,_ZNK7Student10universityEv,comdat
	.align 2
	.weak	_ZNK7Student10universityEv
	.type	_ZNK7Student10universityEv, @function
_ZNK7Student10universityEv:
.LFB980:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSsC1ERKSs
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE980:
	.size	_ZNK7Student10universityEv, .-_ZNK7Student10universityEv
	.globl	e
	.bss
	.align 16
	.type	e, @object
	.size	e, 24
e:
	.zero	24
	.globl	s
	.align 16
	.type	s, @object
	.size	s, 24
s:
	.zero	24
	.section	.text._ZN6PersonC2ERKS_,"axG",@progbits,_ZN6PersonC5ERKS_,comdat
	.align 2
	.weak	_ZN6PersonC2ERKS_
	.type	_ZN6PersonC2ERKS_, @function
_ZN6PersonC2ERKS_:
.LFB983:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSsC1ERKSs
	movq	-16(%rbp), %rax
	movl	8(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 8(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE983:
	.size	_ZN6PersonC2ERKS_, .-_ZN6PersonC2ERKS_
	.weak	_ZN6PersonC1ERKS_
	.set	_ZN6PersonC1ERKS_,_ZN6PersonC2ERKS_
	.section	.rodata
.LC0:
	.string	"Student s:"
.LC1:
	.string	"Person l:"
.LC2:
	.string	"Person r:"
.LC3:
	.string	"Person p:"
	.text
	.globl	main
	.type	main, @function
main:
.LFB981:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA981
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -24
	movq	$s, -24(%rbp)
	movq	$s, -32(%rbp)
	leaq	-128(%rbp), %rax
	movl	$s, %esi
	movq	%rax, %rdi
.LEHB6:
	call	_ZN6PersonC1ERKS_
.LEHE6:
	movl	$.LC0, %esi
	movl	$_ZSt4cout, %edi
.LEHB7:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	leaq	-112(%rbp), %rax
	movl	$s, %esi
	movq	%rax, %rdi
	call	_ZNK7Student10universityEv
.LEHE7:
	movl	$s, %edi
	call	_ZNK6Person3ageEv
	movl	%eax, %ebx
	leaq	-96(%rbp), %rax
	movl	$s, %esi
	movq	%rax, %rdi
.LEHB8:
	call	_ZNK6Person4nameEv
.LEHE8:
	leaq	-96(%rbp), %rax
	movq	%rax, %rsi
	movl	$_ZSt4cout, %edi
.LEHB9:
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	leaq	-112(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE9:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
.LEHB10:
	call	_ZNSsD1Ev
.LEHE10:
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
.LEHB11:
	call	_ZNSsD1Ev
	movl	$.LC1, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK6Person3ageEv
	movl	%eax, %ebx
	leaq	-80(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNK6Person4nameEv
.LEHE11:
	leaq	-80(%rbp), %rax
	movq	%rax, %rsi
	movl	$_ZSt4cout, %edi
.LEHB12:
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE12:
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
.LEHB13:
	call	_ZNSsD1Ev
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK6Person3ageEv
	movl	%eax, %ebx
	leaq	-64(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNK6Person4nameEv
.LEHE13:
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	movl	$_ZSt4cout, %edi
.LEHB14:
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE14:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
.LEHB15:
	call	_ZNSsD1Ev
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK6Person3ageEv
	movl	%eax, %ebx
	leaq	-48(%rbp), %rax
	leaq	-128(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNK6Person4nameEv
.LEHE15:
	leaq	-48(%rbp), %rax
	movq	%rax, %rsi
	movl	$_ZSt4cout, %edi
.LEHB16:
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE16:
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
.LEHB17:
	call	_ZNSsD1Ev
.LEHE17:
	movl	$0, %ebx
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
.LEHB18:
	call	_ZN6PersonD1Ev
.LEHE18:
	movl	%ebx, %eax
	jmp	.L33
.L29:
	movq	%rax, %rbx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L20
.L28:
	movq	%rax, %rbx
.L20:
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L21
.L30:
	movq	%rax, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L21
.L31:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L21
.L32:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L21
.L27:
	movq	%rax, %rbx
.L21:
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6PersonD1Ev
	movq	%rbx, %rax
	jmp	.L25
.L26:
.L25:
	movq	%rax, %rdi
.LEHB19:
	call	_Unwind_Resume
.LEHE19:
.L33:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE981:
	.section	.gcc_except_table
.LLSDA981:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE981-.LLSDACSB981
.LLSDACSB981:
	.uleb128 .LEHB6-.LFB981
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L26-.LFB981
	.uleb128 0
	.uleb128 .LEHB7-.LFB981
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L27-.LFB981
	.uleb128 0
	.uleb128 .LEHB8-.LFB981
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L28-.LFB981
	.uleb128 0
	.uleb128 .LEHB9-.LFB981
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L29-.LFB981
	.uleb128 0
	.uleb128 .LEHB10-.LFB981
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L28-.LFB981
	.uleb128 0
	.uleb128 .LEHB11-.LFB981
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L27-.LFB981
	.uleb128 0
	.uleb128 .LEHB12-.LFB981
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L30-.LFB981
	.uleb128 0
	.uleb128 .LEHB13-.LFB981
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L27-.LFB981
	.uleb128 0
	.uleb128 .LEHB14-.LFB981
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L31-.LFB981
	.uleb128 0
	.uleb128 .LEHB15-.LFB981
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L27-.LFB981
	.uleb128 0
	.uleb128 .LEHB16-.LFB981
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L32-.LFB981
	.uleb128 0
	.uleb128 .LEHB17-.LFB981
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L27-.LFB981
	.uleb128 0
	.uleb128 .LEHB18-.LFB981
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L26-.LFB981
	.uleb128 0
	.uleb128 .LEHB19-.LFB981
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE981:
	.text
	.size	main, .-main
	.section	.text._ZN7StudentD2Ev,"axG",@progbits,_ZN7StudentD5Ev,comdat
	.align 2
	.weak	_ZN7StudentD2Ev
	.type	_ZN7StudentD2Ev, @function
_ZN7StudentD2Ev:
.LFB1027:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1027
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
.LEHB20:
	call	_ZNSsD1Ev
.LEHE20:
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
.LEHB21:
	call	_ZN6PersonD2Ev
.LEHE21:
	jmp	.L38
.L37:
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6PersonD2Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB22:
	call	_Unwind_Resume
.LEHE22:
.L38:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1027:
	.section	.gcc_except_table
.LLSDA1027:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1027-.LLSDACSB1027
.LLSDACSB1027:
	.uleb128 .LEHB20-.LFB1027
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L37-.LFB1027
	.uleb128 0
	.uleb128 .LEHB21-.LFB1027
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB1027
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE1027:
	.section	.text._ZN7StudentD2Ev,"axG",@progbits,_ZN7StudentD5Ev,comdat
	.size	_ZN7StudentD2Ev, .-_ZN7StudentD2Ev
	.weak	_ZN7StudentD1Ev
	.set	_ZN7StudentD1Ev,_ZN7StudentD2Ev
	.section	.rodata
.LC4:
	.string	"University"
.LC5:
	.string	"Name"
.LC6:
	.string	"Oxford"
.LC7:
	.string	"Alex"
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB1025:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1025
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -24
	movl	%edi, -84(%rbp)
	movl	%esi, -88(%rbp)
	cmpl	$1, -84(%rbp)
	jne	.L39
	cmpl	$65535, -88(%rbp)
	jne	.L39
	movl	$_ZStL8__ioinit, %edi
.LEHB23:
	call	_ZNSt8ios_base4InitC1Ev
.LEHE23:
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	call	__cxa_atexit
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-65(%rbp), %rdx
	leaq	-80(%rbp), %rax
	movl	$.LC4, %esi
	movq	%rax, %rdi
.LEHB24:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE24:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-49(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movl	$.LC5, %esi
	movq	%rax, %rdi
.LEHB25:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE25:
	leaq	-80(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movq	%rdx, %rcx
	movl	$20, %edx
	movq	%rax, %rsi
	movl	$e, %edi
.LEHB26:
	call	_ZN7StudentC1ESsiSs
.LEHE26:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
.LEHB27:
	call	_ZNSsD1Ev
.LEHE27:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
.LEHB28:
	call	_ZNSsD1Ev
.LEHE28:
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	movl	$__dso_handle, %edx
	movl	$e, %esi
	movl	$_ZN7StudentD1Ev, %edi
	call	__cxa_atexit
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-33(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movl	$.LC6, %esi
	movq	%rax, %rdi
.LEHB29:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE29:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-17(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movl	$.LC7, %esi
	movq	%rax, %rdi
.LEHB30:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE30:
	leaq	-48(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rcx
	movl	$21, %edx
	movq	%rax, %rsi
	movl	$s, %edi
.LEHB31:
	call	_ZN7StudentC1ESsiSs
.LEHE31:
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
.LEHB32:
	call	_ZNSsD1Ev
.LEHE32:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
.LEHB33:
	call	_ZNSsD1Ev
.LEHE33:
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	movl	$__dso_handle, %edx
	movl	$s, %esi
	movl	$_ZN7StudentD1Ev, %edi
	call	__cxa_atexit
	jmp	.L39
.L49:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L42
.L48:
	movq	%rax, %rbx
.L42:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L43
.L47:
	movq	%rax, %rbx
.L43:
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB34:
	call	_Unwind_Resume
.LEHE34:
.L52:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L45
.L51:
	movq	%rax, %rbx
.L45:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L46
.L50:
	movq	%rax, %rbx
.L46:
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB35:
	call	_Unwind_Resume
.LEHE35:
.L39:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1025:
	.section	.gcc_except_table
.LLSDA1025:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1025-.LLSDACSB1025
.LLSDACSB1025:
	.uleb128 .LEHB23-.LFB1025
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB1025
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L47-.LFB1025
	.uleb128 0
	.uleb128 .LEHB25-.LFB1025
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L48-.LFB1025
	.uleb128 0
	.uleb128 .LEHB26-.LFB1025
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L49-.LFB1025
	.uleb128 0
	.uleb128 .LEHB27-.LFB1025
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L48-.LFB1025
	.uleb128 0
	.uleb128 .LEHB28-.LFB1025
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L47-.LFB1025
	.uleb128 0
	.uleb128 .LEHB29-.LFB1025
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L50-.LFB1025
	.uleb128 0
	.uleb128 .LEHB30-.LFB1025
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L51-.LFB1025
	.uleb128 0
	.uleb128 .LEHB31-.LFB1025
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L52-.LFB1025
	.uleb128 0
	.uleb128 .LEHB32-.LFB1025
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L51-.LFB1025
	.uleb128 0
	.uleb128 .LEHB33-.LFB1025
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L50-.LFB1025
	.uleb128 0
	.uleb128 .LEHB34-.LFB1025
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB35-.LFB1025
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
.LLSDACSE1025:
	.text
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_e, @function
_GLOBAL__sub_I_e:
.LFB1029:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1029:
	.size	_GLOBAL__sub_I_e, .-_GLOBAL__sub_I_e
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_e
	.section	.rodata
	.align 8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL22__gthrw_pthread_cancelm
	.weakref	_ZL22__gthrw_pthread_cancelm,pthread_cancel
	.hidden	__dso_handle
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
