#include <string>
#include <iostream>
#include <iostream>
using namespace std;

struct Person {
    Person(string name, int age)
          : name_(name), age_(age)
    {}
    string name() const { return name_; }
    int age() const { return age_; }
protected: // break incaps.
    string name_;
    int age_;
};

// protected interface

struct Student : Person {
    Student(string name, int age, string uni)
           : Person(name, age), uni_(uni)
    {}
    string university() const { return uni_; }
private:
    string uni_;
};

Student e("Name", 20, "University");
Student s("Alex", 21, "Oxford");

int main(void)
{
    Person & l = s;  // Student & -> Person & ! Person & <!- Student &
    Person * r = &s; // Student * -> Person * ! Person * <!- Student *
    Person p = s;    // Person("Alex", 21);
    cout << "Student s:" << endl;
    cout << s.name() << endl
         << s.age() << endl
         << s.university() << endl << endl;

    cout << "Person l:" << endl;
    cout << l.name() << endl
         << l.age() << endl << endl;

    cout << "Person r:" << endl;
    cout << r->name() << endl
         << r->age() << endl << endl;

    cout << "Person p:" << endl;
    cout << p.name() << endl
         << p.age() << endl << endl;
    return 0;
}
