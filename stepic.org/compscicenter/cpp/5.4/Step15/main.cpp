#include <iostream>

using namespace std;

int foo(void);
int bar(void);
int zoo(void);

int main(void) {
    foo();
    bar();
    bar();
    zoo();
    zoo();
    zoo();

    cout << zoo() << endl;
    cout << bar() << endl;
    cout << foo() << endl;

    return 0;
}
