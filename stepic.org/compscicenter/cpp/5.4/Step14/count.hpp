inline int count() {
    static int counter = 0;
    return ++counter;
}
