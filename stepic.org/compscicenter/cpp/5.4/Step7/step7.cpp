#include <cstdlib>
#include <iostream>

struct User {
    //
    static size_t count() { return instances_; }
private:
    static size_t instances_;
};

size_t User::instances_ = 1;

int main(void) {

    std::cout << User::count() << std::endl;

    return 0;
}
