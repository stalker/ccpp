// Step16.cpp
// Version 0.1
// Реализуйте класс CopySyntaxTree, который, используя шаблон Visitor, выполняет
// копирование AST. Интерфейсы всех используемых классов приведены для удобства
// — не изменяйте их. Шаблон класса CopySyntaxTree находится в самом низу.
//
// Sample Input:
// There is not tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

#include <cmath>
#include <string>
#include <cassert> // assert
#include <iostream>

using namespace std;

#define DEBUG 2

struct Transformer;
struct Number;
struct BinaryOperation;
struct FunctionCall;
struct Variable;
/*
struct ScopedPtr
{
    explicit ScopedPtr(Expression *ptr = 0)
                      : ptr_(ptr) { }
    ~ScopedPtr() {
        delete ptr_;
    }
    Expression* get()        const {return  ptr_;}
    Expression* release() {
        Expression *temp;
        temp = ptr_;
        ptr_ = 0;
        return temp;
    }
    void reset(Expression *ptr = 0) {
        if (ptr_ != 0)
            delete ptr_;
        ptr_ = ptr;
    }
    Expression& operator*()  const {return *ptr_;}
    Expression* operator->() const {return  ptr_;}

private:
    // запрещаем копирование ScopedPtr
    ScopedPtr(const ScopedPtr&);
    ScopedPtr& operator=(const ScopedPtr&);

    Expression *ptr_;
};
*/

struct SharedPtr
{
    // реализуйте следующие методы
    //
    // explicit SharedPtr(Expression *ptr = 0)
    // ~SharedPtr()
    // SharedPtr(const SharedPtr &)
    // SharedPtr& operator=(const SharedPtr &)
    // Expression* get() const
    // void reset(Expression *ptr = 0)
    // Expression& operator*() const
    // Expression* operator->() const
private:
    Expression *ptr_;
};

void reset(Expression *ptr = 0)
{
    delete ptr;
}

struct Scope
{
    virtual ~Scope() { }
    virtual double variableValue(std::string const &name) const = 0;
};

struct MyScope : Scope
{
    MyScope() { }
    ~MyScope() { }
    double variableValue(std::string const &name) { };
} *_s;

struct Expression
{
    virtual ~Expression() { }
    virtual double evaluate(Scope const *ptr) const = 0;
    virtual Expression *transform(Transformer *tr) const = 0;
};

struct Transformer
{
    virtual ~Transformer() { }
    virtual Expression *transformNumber(Number const *) = 0;
    virtual Expression *transformBinaryOperation(BinaryOperation const *) = 0;
    virtual Expression *transformFunctionCall(FunctionCall const *) = 0;
    virtual Expression *transformVariable(Variable const *) = 0;
};


// Begin Number
struct Number : Expression
{
    Number(double value);
    double value() const;
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;

private:
    double value_;
};
struct Number *x3 = new Number(10);
struct Number *y3 = new Number(6);
struct Number *x2 = new Number(10);
struct Number *y2 = new Number(6);
struct Number *y4 = new Number(6);
struct Number *pi = new Number(3.14);
// End Number

// Begin BinaryOperation
struct BinaryOperation : Expression
{
    enum {
        PLUS = '+',
        MINUS = '-',
        DIV = '/',
        MUL = '*'
    };
    BinaryOperation(Expression const *left, int op, Expression const *right);
    ~BinaryOperation();
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;
    Expression const *left() const;
    Expression const *right() const;
    int operation() const;

private:
    Expression const *left_;
    Expression const *right_;
    int op_;
};
// End BinaryOperation

// Begin FunctionCall
struct FunctionCall : Expression
{
    FunctionCall(std::string const &name, Expression const *arg);
    ~FunctionCall();
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;
    std::string const &name() const;
    Expression const *arg() const;

private:
    std::string const name_;
    Expression const *arg_;
};
// FunctionCall

// Begin Variable
struct Variable : Expression
{
    Variable(std::string const &name);
    std::string const & name() const;
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;

private:
    std::string const name_;
};
// End Variable

/**
 * реализуйте все необходимые методы класса
 * вы можете определять любые вспомогательные
 * методы, если хотите
 */
struct CopySyntaxTree : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        return new BinaryOperation(binop->left()->transform(this), binop->operation(), binop->right()->transform(this));
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    { return new FunctionCall(fcall->name(), fcall->arg()->transform(this)); }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};

Expression * Number::transform(Transformer *tr) const
{
    return tr->transformNumber(this);
}

/**
 * реализуйте все необходимые методы
 * если считаете нужным, то можете
 * заводить любые вспомогательные
 * методы
 */
struct FoldConstants : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        Expression *e1_left, *e2_right;
        e1_left = binop->left()->transform(this);
        e2_right = binop->right()->transform(this);
        Number *number1 = dynamic_cast<Number *>((Expression *)e1_left);
        Number *number2 = dynamic_cast<Number *>((Expression *)e2_right);
        if (number1 != NULL && number2 != NULL) {
            delete e1_left;
            delete e2_right;
            return new Number(binop->evaluate(_s));
        }
        return new BinaryOperation(e1_left, binop->operation(), e2_right);
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    {
        Expression *exp = fcall->arg()->transform(this);
        Number *number1 = dynamic_cast<Number *>((Expression *)exp);
        if (number1 != NULL) {
            delete exp;
            return new Number(fcall->evaluate(_s));
        }
        return new FunctionCall(fcall->name(), exp);
    }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};

// int main() {
int main() {

  Expression *x_plus_y = new BinaryOperation(x3, BinaryOperation::PLUS, y3);
  cout<<"x_plus_y: "<<x_plus_y->evaluate(_s)<<endl;
  Expression *x_minus_y = new BinaryOperation(x2, BinaryOperation::MINUS, y2);
  cout<<"x_minus_y: "<<x_minus_y->evaluate(_s)<<endl;
  Expression *expression0 = new BinaryOperation(pi, BinaryOperation::PLUS, x_plus_y);
  cout<<"expression0: "<<expression0->evaluate(_s)<<endl;
  Expression *expression1 = new BinaryOperation(expression0, BinaryOperation::MINUS, x_minus_y);
  Transformer *transformer1 = new FoldConstants();
  Expression *new_expression1 = expression1->transform(transformer1);
/*
  delete transformer1;
  // delete x_plus_y;
  // delete x_minus_y;
  cout<<"expression0:"<<expression0<<endl;
  delete expression1;
  cout<<"expression0:"<<expression0<<endl;
  // delete expression0;
*/
  cout<<"binary operation 1: "<<new_expression1->evaluate(_s)<<endl;

  Expression *expression2 = new FunctionCall("sqrt", new_expression1);
  Transformer *transformer2 = new FoldConstants();
  Expression *new_expression2 = expression2->transform(transformer1);

  cout<<"function call 1: "<<new_expression2->evaluate(_s)<<endl;

  Expression *expression3 = new FunctionCall("sqrt", new_expression2);
  Transformer *transformer3 = new FoldConstants();
  Expression *new_expression3 = expression3->transform(transformer1);

  cout<<"function call 2: "<<new_expression3->evaluate(_s)<<endl;

  Expression *expression4 = new BinaryOperation(new_expression3,BinaryOperation::DIV,y4);
  Transformer *transformer4 = new FoldConstants();
  Expression *new_expression4 = expression4->transform(transformer1);

  cout<<"binary operation 2: "<<new_expression4->evaluate(_s)<<endl;
  //system("pause");
  return 0;
}
//  return 0;

// Begin Number
Number::Number(double value)
              : value_(value)
{}

double Number::value() const
{ return value_; }

double Number::evaluate(Scope const *ptr) const
{ return value_; }
// End Number

// Begin BinaryOperation
BinaryOperation::BinaryOperation(Expression const *left, int op, Expression const *right)
        : left_(left), op_(op), right_(right)
{ assert(left_ && right_); }

BinaryOperation::~BinaryOperation()
{
#if DEBUG > 9
    cout<<"delete: ~BinaryOperation("<<this<<"): "<<(char)op_<<endl;
#endif
    delete left_;
    delete right_;
}

double BinaryOperation::evaluate(Scope const *ptr) const
{
    double left = left_->evaluate(_s);
    double right = right_->evaluate(_s);
    switch (op_)
    {
    case PLUS: return left + right;
    case MINUS: return left - right;
    case DIV: return left / right;
    case MUL: return left * right;
    }
    assert(0);
    return 0.0;
}

Expression * BinaryOperation::transform(Transformer *tr) const
{
    return tr->transformBinaryOperation(this);
}

Expression const *BinaryOperation::left() const
{ return left_; }

Expression const *BinaryOperation::right() const
{ return right_; }

int BinaryOperation::operation() const
{ return op_; }
// End BinaryOperation

// Begin FunctionCall
//    std::string const &name() const;
//    Expression const *arg() const;
//
FunctionCall::FunctionCall(std::string const &name, Expression const *arg)
        : name_(name), arg_(arg)
{
    assert(arg_);
    assert(name_ == "sqrt" || name_ == "abs");
}

FunctionCall::~FunctionCall() {
    delete arg_;
}

double FunctionCall::evaluate(Scope const *ptr) const
{
    if (name_ == "sqrt") {
        return sqrt(arg_->evaluate(_s));
    } else if (name_ == "abs") {
        return abs(arg_->evaluate(_s));
    }
    assert(0);
    return 0.0;
}

Expression * FunctionCall::transform(Transformer *tr) const
{
    return tr->transformFunctionCall(this);
}

std::string const & FunctionCall::name() const
{
    return name_;
    // put your code here
}

Expression const *FunctionCall::arg() const
{
    return arg_;
    // here
}
// Begin FunctionCall

// Begin Variable
Variable::Variable(std::string const &name) : name_(name) { }

std::string const &Variable::name() const { return name_; }

double Variable::evaluate(Scope const *ptr) const
{
    assert(ptr != 0);
    return ptr->variableValue(name_);
}

Expression * Variable::transform(Transformer *tr) const
{
    return tr->transformVariable(this);
}
// End Variable

// Begin Scope
// virtual Scope::~Scope() { }
// End Scope



void test(){
    Number *n32 = new Number(32.0);
    Number *n16 = new Number(16.0);
    Number *n42 = new Number(42.0);
    SharedPtr p1(n32);
    SharedPtr p2 = p1;
    SharedPtr p3(p1);
    p3.reset(n16);
    p3.reset(n32);
    p1.reset(n42);
    SharedPtr p4(n32);
}

/*
     std::cout<<"------------------"<<std::endl;
     std::cout<<"SharedPtr p1(n32)"<<std::endl;
     SharedPtr p1(n32);
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"SharedPtr p2 = p1"<<std::endl;
     SharedPtr p2 = p1;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"SharedPtr p3(p1)"<<std::endl;
     SharedPtr p3(p1);
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p3 = "<<p3.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
     
     std::cout<<"p3.reset(n16)"<<std::endl;
     p3.reset(n16);
     std::cout<<"p3 = "<<p3.count()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p3.reset(n32)"<<std::endl;
     p3.reset(n32);
     std::cout<<"p3 = "<<p3.count()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p3.~SharedPtr()"<<std::endl;
     p3.~SharedPtr();
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p1.reset(n42)"<<std::endl;
     p1.reset(n42);
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"SharedPtr p4(n32)"<<std::endl;
     SharedPtr p4(n32);
     std::cout<<"p4 = "<<p4.count()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p1.~SharedPtr()"<<std::endl;
     p1.~SharedPtr();
     std::cout<<"p2 = "<<p2.count()<<std::endl;
     std::cout<<"p4 = "<<p4.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p2.~SharedPtr()"<<std::endl;
     p2.~SharedPtr();
     std::cout<<"p4 = "<<p4.count()<<std::endl;
     std::cout<<"------------------"<<std::endl;

SharedPtr p1(n32)
p1 = 1
------------------
SharedPtr p2 = p1
p2 = 2
p1 = 2
------------------
SharedPtr p3(p1)
p1 = 3
p2 = 3
p3 = 3
------------------
p3.reset(n16)
p3 = 1
p2 = 2
p1 = 2
------------------
p3.reset(n32)
p3 = 3
p2 = 3
p1 = 3
------------------
p3.~SharedPtr()
p2 = 2
p1 = 2
------------------
p1.reset(n42)
p1 = 1
p2 = 1
------------------
SharedPtr p4(n32)
p4 = 2
p2 = 2
p1 = 1
------------------
p1.~SharedPtr()
p2 = 2
p4 = 2
------------------
p2.~SharedPtr()
p4 = 1

==687== HEAP SUMMARY:
==687==     in use at exit: 25,260 bytes in 376 blocks
==687==   total heap usage: 477 allocs, 101 frees, 31,928 bytes allocated
==687== 
==687== LEAK SUMMARY:
==687==    definitely lost: 0 bytes in 0 blocks
==687==    indirectly lost: 0 bytes in 0 blocks
==687==      possibly lost: 0 bytes in 0 blocks
==687==    still reachable: 0 bytes in 0 blocks
==687==         suppressed: 25,260 bytes in 376 blocks
==687== 
==687== For counts of detected and suppressed errors, rerun with: -v
==687== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 14 from 14)\



*/
int count(){
        if(ptr_){return cp_->count(ptr_);}
        return -1;
 } 
