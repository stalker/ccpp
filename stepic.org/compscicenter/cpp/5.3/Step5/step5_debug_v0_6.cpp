// step5_debug_v0_4_etalon_v2_ok.cpp
// Version: 0.4
#include <iostream>
#include "SharedPtr.h"

using namespace std;

int main(void) {
  {
    cerr_FILE_LINE_nl;
     Number *n16 = new Number(16.0);
  std::cout<<"SharedPtr p4(NULL)"<<std::endl;
  SharedPtr p4(NULL);
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"SharedPtr p5(NULL)"<<std::endl;
  SharedPtr p5(NULL);
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"p5 = p4"<<std::endl;
  p5 = p4;
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"SharedPtr p6(p4)"<<std::endl;
  SharedPtr p6(p4);
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"p6 = "<<p6.count()<<", p6.val="<<p6.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"p6.reset(n16)"<<std::endl;
    p6.reset(n16);
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"p6 = "<<p6.count()<<", p6.val="<<p6.val()<<std::endl;
    std::cout<<"------------------"<<std::endl;

  std::cout<<"p5 = p6"<<std::endl;
  p5 = p6;
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"p6 = "<<p6.count()<<", p6.val="<<p6.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"p6 = p4"<<std::endl;
  p6 = p4;
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"p6 = "<<p6.count()<<", p6.val="<<p6.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;

  std::cout<<"p5.reset(NULL)"<<std::endl;
  p5.reset(NULL);
  std::cout<<"p4 = "<<p4.count()<<", p4.val="<<p4.val()<<std::endl;
  std::cout<<"p5 = "<<p5.count()<<", p5.val="<<p5.val()<<std::endl;
  std::cout<<"p6 = "<<p6.count()<<", p6.val="<<p6.val()<<std::endl;
  std::cout<<"------------------"<<std::endl;
  }
    // assert(Expression::count == 0);
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
// EOF



