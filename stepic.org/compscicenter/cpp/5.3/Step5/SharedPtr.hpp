// SharedPtr.h
// Version: 0.7
#include <iostream>
#include "Expression.h"
#pragma once

using namespace std;


struct SharedPtr
{
    // реализуйте следующие методы
    //
    //+explicit SharedPtr(Expression *ptr = 0)
    //+~SharedPtr()
    //+SharedPtr(const SharedPtr &)
    //+SharedPtr& operator=(const SharedPtr &)
    //+Expression* get() const
    //+void reset(Expression *ptr = 0)
    // Expression& operator*() const
    //+Expression* operator->() const

    explicit SharedPtr(Expression *ptr = 0)
    {
        if (ptr != 0)
            stor_ = new Stor(ptr);
        else
            stor_ = 0;
    }
    ~SharedPtr() {
        if (stor_ != 0 && stor_->count_ != 0)
            decr();
        else if (stor_ != 0) {
            delete stor_;
            stor_ = 0;
        }
    }
    SharedPtr(const SharedPtr & oth)
             : stor_(oth.stor_)
    {
        if (stor_ != 0 && stor_->ptr_ != 0)
            incr();
    }
    SharedPtr& operator=(const SharedPtr & ptr) {
        if (stor_ != ptr.stor_) {
            if (stor_ != 0)
                decr();
            stor_ = ptr.stor_;
            if (stor_ != 0 && stor_->ptr_ != 0)
                incr();
        }
        return *this;
    }
    Expression* get() const {
        if (stor_ != 0) {
            return stor_->ptr_;
        }
        return 0;
    }
    Expression* operator->() const {
        if (stor_ != 0) {
            return stor_->ptr_;
        }
        return 0;
    }
    void reset(Expression *ptr = 0) {
        if (stor_ != 0)
            decr();
        // else {
            if(ptr != 0)
                stor_ = new Stor(ptr);
            else
                stor_ = 0;
        // }
    }
    Expression& operator*() const {
      return *get();
    }
    int count() {
        if (stor_ != 0)
            return stor_->count_;
        else
            return -10;
    }
    double val() {
        Number * n;
        if (stor_ != 0)
            n = (Number*)stor_->ptr_;
        if (n != 0)
          return n->evaluate(0);
        else
          return 0;
    }

private:
    void incr() {
        if (stor_ != 0) {
            ++stor_->count_;
        }
    }
    void decr() {
        if (stor_ == 0) return;
        if (stor_->count_ == 0) return;
        --stor_->count_;
        if (stor_->count_ == 0) {
            if (stor_->ptr_ != 0) {
                delete stor_->ptr_;
            }
            delete stor_;
            stor_ = 0;
        }
    }
    struct Stor {
        explicit Stor(Expression * ptr)
        {
            if (ptr == 0) {
                ptr_ = 0;
                count_ = 0;
            } else {
                ptr_ = ptr;
                count_ = 1;
            }
        }
        Expression * ptr_;
        std::size_t  count_;
    };
    Stor * stor_;
};
// EOF
