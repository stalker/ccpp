// step5_debug_v0_4_etalon_v2_ok.cpp
// Version: 0.4
#include <iostream>
#include "Expression.h"

using namespace std;

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

struct SharedPtr
{
    // реализуйте следующие методы
    //
    //+explicit SharedPtr(Expression *ptr = 0)
    //+~SharedPtr()
    //+SharedPtr(const SharedPtr &)
    //+SharedPtr& operator=(const SharedPtr &)
    //+Expression* get() const
    //+void reset(Expression *ptr = 0)
    // Expression& operator*() const
    //+Expression* operator->() const

    explicit SharedPtr(Expression *ptr = 0)
                      : stor_(new Stor(ptr))
    {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(Expression *ptr = 0));
        cerr_FILE_LINE;
        cerr_X(:SharedPtr(Expression *ptr = 0));
        cerr_X(:stor=);
        cerr<<stor_<<" new stor_ "<<endl;
    }
    ~SharedPtr() {
        cerr_FILE_LINE;
        cerr_X_nl(:~SharedPtr());
        if (stor_ != 0 && stor_->count_ != 0)
            decr();
        else if (stor_ != 0) {
            cerr_FILE_LINE;
            cerr_X(:~SharedPtr());
            cerr_X(:stor_=);
            cerr<<stor_<<" deleting..."<<endl;
            delete stor_;
            stor_ = 0;
        }
    }
    SharedPtr(const SharedPtr & oth)
             : stor_(oth.stor_)
    {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(const SharedPtr &));
        cerr_FILE_LINE;
        cerr_X(:SharedPtr(const SharedPtr &));
        cerr_X(:stor=);
        cerr<<stor_<<" new stor_ "<<endl;
        if (stor_ != 0 && stor_->ptr_ != 0)
            incr();
    }
    SharedPtr& operator=(const SharedPtr & ptr) {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr& operator=(const SharedPtr & ptr));
        if (stor_ != ptr.stor_) {
            cerr_FILE_LINE;
            cerr_X(:SharedPtr& operator=(const SharedPtr & ptr));
            cerr_X(:stor_ != ptr.stor_);
            cerr_X(:stor_->count_=);
            cerr<<stor_->count_<<endl;
            if (stor_ != 0)
                decr();
            stor_ = ptr.stor_;
            cerr_FILE_LINE;
            cerr_X(:SharedPtr& operator=(const SharedPtr & ptr));
            cerr_X(:stor_=);
            cerr<<stor_<<" new stor_ "<<endl;
            if (stor_ != 0 && stor_->ptr_ != 0)
                incr();
            cerr_FILE_LINE;
            cerr_X(:SharedPtr& operator=(const SharedPtr & ptr));
            cerr_X(:stor_ != ptr.stor_);
            cerr_X(:stor_->count_=);
            cerr<<stor_->count_<<endl;
        }
        return *this;
    }
    Expression* get() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* get() const);
        if (stor_ != 0) {
            cerr_FILE_LINE;
            cerr_X(:Expression* get() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    Expression* operator->() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* operator->() const);
        if (stor_ != 0) {
            cerr_FILE_LINE;
            cerr_X(:Expression* operator->() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    void reset(Expression *ptr = 0) {
        cerr_FILE_LINE;
        cerr_X_nl(:void reset(Expression *ptr = 0));
        if (stor_ != 0)
            decr();
        if (ptr != 0) {
            stor_ = new Stor(ptr);
            cerr_FILE_LINE;
            cerr_X(:void reset(Expression *ptr = 0));
            cerr_X(:stor_=);
            cerr<<stor_<<" new stor_ "<<endl;
        } else
            stor_ = 0;
    }
    Expression& operator*() const {
      return *get();
    }
    int count() {
        if (stor_ != 0)
            return stor_->count_;
        else
            return -1;
    }
    double val() {
        Number * n;
        if (stor_ != 0)
            n = (Number*)stor_->ptr_;
        if (n != 0)
          return n->evaluate(0);
        else
          return 0;
    }

private:
    void incr() {
        cerr_FILE_LINE;
        cerr_X_nl(:incr());
        cerr_FILE_LINE;
        cerr_X(:incr());
        cerr_X(:incr():stor_);
        cerr<<'='<<stor_<<endl;
        if (stor_ != 0) {
            cerr_FILE_LINE;
            cerr_X(:incr());
            cerr_X(:incr():++stor_->count_);
            ++stor_->count_;
            cerr<<'='<<stor_->count_<<endl;
        }
    }
    void decr() {
        cerr_FILE_LINE;
        cerr_X_nl(:decr());
        --stor_->count_;
        if (stor_ != 0 && (stor_->count_ == 0)) {
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X_nl(:stor_->count_ == 0);
            if (stor_->ptr_ != 0) {
                cerr_FILE_LINE;
                cerr_X(:decr());
                cerr_X(:_stor_->ptr_=);
                cerr<<stor_->ptr_<<" deleting..."<<endl;
                delete stor_->ptr_;
            }
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X(:_stor_=);
            cerr<<stor_<<" deleting..."<<endl;
            delete stor_;
            stor_ = 0;
        }
    }
    struct Stor {
        explicit Stor(Expression * ptr)
        {
            if (ptr == 0) {
                ptr_ = 0;
                count_ = 0;
            } else {
                ptr_ = ptr;
                count_ = 1;
                cerr_FILE_LINE;
                cerr_X(:Stor::Stor);
                cerr_X(:stor_->ptr_=);
                cerr<<ptr_<<" new ptr_ "<<endl;
            }
        }
        Expression * ptr_;
        std::size_t  count_;
    };
    Stor * stor_;
};

int main(void) {
  {
    cerr_FILE_LINE_nl;
        SharedPtr ptr(new Number(42));
        assert(ptr.count() == 1);
        assert(ptr->evaluate(NULL) == 42);

        SharedPtr copy(ptr);
        assert(copy.count() == 2);
        assert(copy->evaluate(NULL) == 42);

        SharedPtr empty;
        assert(empty.empty());

        empty = copy;
        assert(empty.count() == 3);
        assert(empty->evaluate(NULL) == 42);

        empty = SharedPtr();
        assert(empty.empty());

        SharedPtr emptyCopy(empty);
        assert(emptyCopy.empty());
  }
    // assert(Expression::count == 0);
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
// EOF



