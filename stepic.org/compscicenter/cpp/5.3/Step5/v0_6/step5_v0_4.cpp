// step5_debug_v0_4_etalon_v2_ok.cpp
// Version: 0.4
#include <iostream>
#include "Expression.h"

using namespace std;


struct SharedPtr
{
    // реализуйте следующие методы
    //
    //+explicit SharedPtr(Expression *ptr = 0)
    //+~SharedPtr()
    //+SharedPtr(const SharedPtr &)
    //+SharedPtr& operator=(const SharedPtr &)
    //+Expression* get() const
    //+void reset(Expression *ptr = 0)
    // Expression& operator*() const
    //+Expression* operator->() const

    explicit SharedPtr(Expression *ptr = 0)
                      : stor_(new Stor(ptr))
    {
    }
    ~SharedPtr() {
        if (stor_ != 0 && stor_->count_ != 0)
            decr();
        else if (stor_ != 0) {
            delete stor_;
            stor_ = 0;
        }
    }
    SharedPtr(const SharedPtr & oth)
             : stor_(oth.stor_)
    {
        if (stor_ != 0 && stor_->ptr_ != 0)
            incr();
    }
    SharedPtr& operator=(const SharedPtr & ptr) {
        if (stor_ != ptr.stor_) {
            if (stor_ != 0)
                decr();
            stor_ = ptr.stor_;
            if (stor_ != 0 && stor_->ptr_ != 0)
                incr();
        }
        return *this;
    }
    Expression* get() const {
        if (stor_ != 0) {
            return stor_->ptr_;
        }
        return 0;
    }
    Expression* operator->() const {
        if (stor_ != 0) {
            return stor_->ptr_;
        }
        return 0;
    }
    void reset(Expression *ptr = 0) {
        if (stor_ != 0)
            decr();
        if (ptr != 0)
            stor_ = new Stor(ptr);
        else
            stor_ = 0;
    }
    Expression& operator*() const {
      return *get();
    }
    int count() {
        if (stor_ != 0)
            return stor_->count_;
        else
            return -1;
    }
    double val() {
        Number * n;
        if (stor_ != 0)
            n = (Number*)stor_->ptr_;
        if (n != 0)
          return n->evaluate(0);
        else
          return 0;
    }

private:
    void incr() {
        if (stor_ != 0) {
            ++stor_->count_;
        }
    }
    void decr() {
        --stor_->count_;
        if (stor_ != 0 && (stor_->count_ == 0)) {
            if (stor_->ptr_ != 0)
                delete stor_->ptr_;
            delete stor_;
            stor_ = 0;
        }
    }
    struct Stor {
        explicit Stor(Expression * ptr)
        {
            if (ptr == 0) {
                ptr_ = 0;
                count_ = 0;
            } else {
                ptr_ = ptr;
                count_ = 1;
            }
        }
        Expression * ptr_;
        std::size_t  count_;
    };
    Stor * stor_;
};

int main(void) {
  {
    std::cout<<"---Example 1-------------------------------"<<std::endl;
    std::cout<<"SharedPtr p = SharedPtr(new Expression(42))"<<std::endl;
    SharedPtr p = SharedPtr(new Number(42));
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    SharedPtr p2(p);
    std::cout<<"SharedPtr p2(p)"<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    SharedPtr p3(p);
    std::cout<<"SharedPtr p3(p)"<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 2-------------------------------"<<std::endl;
    p.reset(new Number(1912));
    std::cout<<"p.reset(new Expression(1912))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;

    p2.reset(0);
    std::cout<<"p2.reset(0)"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 3-------------------------------"<<std::endl;
    p = p3;
    std::cout<<"p = p3"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    p3.reset();
    std::cout<<"p3.reset()"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 4-------------------------------"<<std::endl;
    p3 = p2;
    std::cout<<"p3 = p2"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 5-------------------------------"<<std::endl;
    p3.reset(new Number(1));
    std::cout<<"p3.reset(new Number(1))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    p3.reset(new Number(2));
    std::cout<<"p3.reset(new Number(2))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

  }
    return 0;
}
// EOF
