// step5_debug_v0_4_etalon_v2_ok.cpp
// Version: 0.4
#include <iostream>
#include "SharedPtr.h"

using namespace std;

int main(void) {
  {
    cerr_FILE_LINE_nl;
     Number *n32 = new Number(32.0);
     Number *n16 = new Number(16.0);
     Number *n42 = new Number(42.0);
     std::cout<<"------------------"<<std::endl;
     std::cout<<"SharedPtr p1(n32)"<<std::endl;
     SharedPtr p1(n32);
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"SharedPtr p2 = p1"<<std::endl;
     SharedPtr p2 = p1;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"SharedPtr p3(p1)"<<std::endl;
     SharedPtr p3(p1);
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p3 = "<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
     
     std::cout<<"p3.reset(n16)"<<std::endl;
     p3.reset(n16);
     std::cout<<"p3 = "<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p3.reset(0)"<<std::endl;
     p3.reset(0);
     std::cout<<"p3 = "<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
     
     
     std::cout<<"p1 = p1"<<std::endl;
     p1 = p1;
     std::cout<<"p3 = "<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
    
     std::cout<<"p3.~SharedPtr()"<<std::endl;
     p3.~SharedPtr();
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p1.reset(n42)"<<std::endl;
     p1.reset(n42);
     std::cout<<"p1 = "<<p1.count()<<", p1.val="<<p1.val()<<std::endl;
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p1.~SharedPtr()"<<std::endl;
     p1.~SharedPtr();
     std::cout<<"p2 = "<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
     std::cout<<"------------------"<<std::endl;
    
     std::cout<<"p2.~SharedPtr()"<<std::endl;
     p2.~SharedPtr();
  }
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
// EOF



