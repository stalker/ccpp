// step5_debug_v0_4_etalon_v2_ok.cpp
// Version: 0.4
#include <iostream>
#include "SharedPtr.h"

using namespace std;

int main(void) {
  {
    cerr_FILE_LINE_nl;
    std::cout<<"---Example 1-------------------------------"<<std::endl;
    std::cout<<"SharedPtr p = SharedPtr(new Expression(42))"<<std::endl;
    SharedPtr p = SharedPtr(new Number(42));
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    SharedPtr p2(p);
    std::cout<<"SharedPtr p2(p)"<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    SharedPtr p3(p);
    std::cout<<"SharedPtr p3(p)"<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 2-------------------------------"<<std::endl;
    p.reset(new Number(1912));
    std::cout<<"p.reset(new Expression(1912))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;

    p2.reset(0);
    std::cout<<"p2.reset(0)"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 3-------------------------------"<<std::endl;
    p = p3;
    std::cout<<"p = p3"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    p3.reset();
    std::cout<<"p3.reset()"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 4-------------------------------"<<std::endl;
    p3 = p2;
    std::cout<<"p3 = p2"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    std::cout<<"---Example 5-------------------------------"<<std::endl;
    p3.reset(new Number(1));
    std::cout<<"p3.reset(new Number(1))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    p3.reset(new Number(2));
    std::cout<<"p3.reset(new Number(2))"<<std::endl;
    std::cout<<"p.count="<<p.count()<<", p.val="<<p.val()<<std::endl;
    std::cout<<"p2.count="<<p2.count()<<", p2.val="<<p2.val()<<std::endl;
    std::cout<<"p3.count="<<p3.count()<<", p3.val="<<p3.val()<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

  }
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
// EOF
