#include <cmath>
#include <string>
#include <cassert> // assert
#include <iostream>
#pragma once

using namespace std;

#define DEBUG 2

struct Transformer;
struct Number;
struct BinaryOperation;
struct FunctionCall;
struct Variable;

struct Scope
{
    virtual ~Scope() { }
    virtual double variableValue(std::string const &name) const = 0;
};

struct MyScope : Scope
{
    MyScope() { }
    ~MyScope() { }
    double variableValue(std::string const &name) { return 0; }
} *_s;

struct Expression
{
    virtual ~Expression() { }
    virtual double evaluate(Scope const *ptr) const = 0;
    virtual Expression *transform(Transformer *tr) const = 0;
};

struct Transformer
{
    virtual ~Transformer() { }
    virtual Expression *transformNumber(Number const *) = 0;
    virtual Expression *transformBinaryOperation(BinaryOperation const *) = 0;
    virtual Expression *transformFunctionCall(FunctionCall const *) = 0;
    virtual Expression *transformVariable(Variable const *) = 0;
};


// Begin Number
struct Number : Expression
{
    Number(double value);
    double value() const;
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;

private:
    double value_;
};
struct Number *x3 = new Number(10);
struct Number *y3 = new Number(6);
struct Number *x2 = new Number(10);
struct Number *y2 = new Number(6);
struct Number *y4 = new Number(6);
struct Number *pi = new Number(3.14);
// End Number

// Begin BinaryOperation
struct BinaryOperation : Expression
{
    enum {
        PLUS = '+',
        MINUS = '-',
        DIV = '/',
        MUL = '*'
    };
    BinaryOperation(Expression const *left, int op, Expression const *right);
    ~BinaryOperation();
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;
    Expression const *left() const;
    Expression const *right() const;
    int operation() const;

private:
    Expression const *left_;
    Expression const *right_;
    int op_;
};
// End BinaryOperation

// Begin FunctionCall
struct FunctionCall : Expression
{
    FunctionCall(std::string const &name, Expression const *arg);
    ~FunctionCall();
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;
    std::string const &name() const;
    Expression const *arg() const;

private:
    std::string const name_;
    Expression const *arg_;
};
// FunctionCall

// Begin Variable
struct Variable : Expression
{
    Variable(std::string const &name);
    std::string const & name() const;
    // double evaluate() const;
    double evaluate(Scope const *ptr) const;
    Expression *transform(Transformer *tr) const;

private:
    std::string const name_;
};
// End Variable

/**
 * реализуйте все необходимые методы класса
 * вы можете определять любые вспомогательные
 * методы, если хотите
 */
struct CopySyntaxTree : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        return new BinaryOperation(binop->left()->transform(this), binop->operation(), binop->right()->transform(this));
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    { return new FunctionCall(fcall->name(), fcall->arg()->transform(this)); }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};

Expression * Number::transform(Transformer *tr) const
{
    return tr->transformNumber(this);
}

/**
 * реализуйте все необходимые методы
 * если считаете нужным, то можете
 * заводить любые вспомогательные
 * методы
 */
struct FoldConstants : Transformer
{
    Expression *transformNumber(Number const *number)
    { return new Number(number->value()); }

    Expression *transformBinaryOperation(BinaryOperation const *binop)
    {
        Expression *e1_left, *e2_right;
        e1_left = binop->left()->transform(this);
        e2_right = binop->right()->transform(this);
        Number *number1 = dynamic_cast<Number *>((Expression *)e1_left);
        Number *number2 = dynamic_cast<Number *>((Expression *)e2_right);
        if (number1 != NULL && number2 != NULL) {
            delete e1_left;
            delete e2_right;
            return new Number(binop->evaluate(_s));
        }
        return new BinaryOperation(e1_left, binop->operation(), e2_right);
    }

    Expression *transformFunctionCall(FunctionCall const *fcall)
    {
        Expression *exp = fcall->arg()->transform(this);
        Number *number1 = dynamic_cast<Number *>((Expression *)exp);
        if (number1 != NULL) {
            delete exp;
            return new Number(fcall->evaluate(_s));
        }
        return new FunctionCall(fcall->name(), exp);
    }

    Expression *transformVariable(Variable const *var)
    { return new Variable(var->name()); }
};


// Begin Number
Number::Number(double value)
              : value_(value)
{}

double Number::value() const
{ return value_; }

double Number::evaluate(Scope const *ptr) const
{ return value_; }
// End Number

// Begin BinaryOperation
BinaryOperation::BinaryOperation(Expression const *left, int op, Expression const *right)
        : left_(left), op_(op), right_(right)
{ assert(left_ && right_); }

BinaryOperation::~BinaryOperation()
{
#if DEBUG > 9
    cout<<"delete: ~BinaryOperation("<<this<<"): "<<(char)op_<<endl;
#endif
    delete left_;
    delete right_;
}

double BinaryOperation::evaluate(Scope const *ptr) const
{
    double left = left_->evaluate(_s);
    double right = right_->evaluate(_s);
    switch (op_)
    {
    case PLUS: return left + right;
    case MINUS: return left - right;
    case DIV: return left / right;
    case MUL: return left * right;
    }
    assert(0);
    return 0.0;
}

Expression * BinaryOperation::transform(Transformer *tr) const
{
    return tr->transformBinaryOperation(this);
}

Expression const *BinaryOperation::left() const
{ return left_; }

Expression const *BinaryOperation::right() const
{ return right_; }

int BinaryOperation::operation() const
{ return op_; }
// End BinaryOperation

// Begin FunctionCall
//    std::string const &name() const;
//    Expression const *arg() const;
//
FunctionCall::FunctionCall(std::string const &name, Expression const *arg)
        : name_(name), arg_(arg)
{
    assert(arg_);
    assert(name_ == "sqrt" || name_ == "abs");
}

FunctionCall::~FunctionCall() {
    delete arg_;
}

double FunctionCall::evaluate(Scope const *ptr) const
{
    if (name_ == "sqrt") {
        return sqrt(arg_->evaluate(_s));
    } else if (name_ == "abs") {
        return abs(arg_->evaluate(_s));
    }
    assert(0);
    return 0.0;
}

Expression * FunctionCall::transform(Transformer *tr) const
{
    return tr->transformFunctionCall(this);
}

std::string const & FunctionCall::name() const
{
    return name_;
    // put your code here
}

Expression const *FunctionCall::arg() const
{
    return arg_;
    // here
}
// Begin FunctionCall

// Begin Variable
Variable::Variable(std::string const &name) : name_(name) { }

std::string const &Variable::name() const { return name_; }

double Variable::evaluate(Scope const *ptr) const
{
    assert(ptr != 0);
    return ptr->variableValue(name_);
}

Expression * Variable::transform(Transformer *tr) const
{
    return tr->transformVariable(this);
}
// End Variable

// Begin Scope
// virtual Scope::~Scope() { }
// End Scope
