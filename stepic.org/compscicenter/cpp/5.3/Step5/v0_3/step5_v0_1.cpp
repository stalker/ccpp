#include <iostream>
#include "Expression.h"

using namespace std;

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

struct SharedPtr
{
    // реализуйте следующие методы
    //
    //+explicit SharedPtr(Expression *ptr = 0)
    //+~SharedPtr()
    //+SharedPtr(const SharedPtr &)
    //+SharedPtr& operator=(const SharedPtr &)
    //+Expression* get() const
    //+void reset(Expression *ptr = 0)
    // Expression& operator*() const
    //+Expression* operator->() const

    explicit SharedPtr(Expression *ptr = 0) 
                      : stor_(new Stor(ptr))
    { 
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(Expression *ptr = 0));
    }
    ~SharedPtr() {
        cerr_FILE_LINE;
        cerr_X_nl(:~SharedPtr());
        decr();
    }
    SharedPtr(const SharedPtr & oth)
             : stor_(oth.stor_)
    {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(const SharedPtr &));
        incr();
    }
    SharedPtr& operator=(const SharedPtr & ptr) {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr& operator=(const SharedPtr & ptr));
        if (stor_ != ptr.stor_) {
            cerr_FILE_LINE;
            cerr_X(:SharedPtr& operator=(const SharedPtr & ptr));
            cerr_X(:stor_ != ptr.stor_);
            decr();
            stor_ = ptr.stor_;
            incr();
        }
        return *this;
    }
    Expression* get() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* get() const);
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:Expression* get() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    Expression* operator->() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* operator->() const);
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:Expression* operator->() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    void reset(Expression *ptr = 0) {
        cerr_FILE_LINE;
        cerr_X_nl(:void reset(Expression *ptr = 0));
        if (stor_->ptr_ != 0 && (stor_->count_ - 1) == 0) {
            stor_->count_--;
            cerr_FILE_LINE;
            cerr_X(:void reset(Expression *ptr = 0));
            cerr_X_nl(ptr_ != 0);
            delete stor_->ptr_;
        }
        stor_->ptr_ = ptr;
    }
    Expression& operator*() const {
      return *get();
    }

private:
    void incr() {
        cerr_FILE_LINE;
        cerr_X_nl(:incr());
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:incr());
            cerr_X(:incr():++stor_->count_);
            ++stor_->count_;
            cerr<<++stor_->count_<<endl;
        }
    }
    void decr() {
        cerr_FILE_LINE;
        cerr_X_nl(:decr());
        if (stor_ && (--stor_->count_ == 0)) {
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X(:stor_);
            cerr<<stor_<<endl;
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X_nl(:--stor_->count_ == 0);
            delete stor_->ptr_;
            delete stor_;
            stor_ = 0;
        }
    }
    struct Stor {
        explicit Stor(Expression * ptr)
                     : ptr_(ptr), count_(1) { }
        Expression * ptr_;
        std::size_t  count_;
    };
    Stor * stor_;
};

int main(void) {
  {
    cerr_FILE_LINE_nl;
    Number *n32 = new Number(32.0);
    Number *n16 = new Number(16.0);
    Number *n42 = new Number(42.0);
    SharedPtr p1(n32);
    SharedPtr p2 = p1;
    SharedPtr p3(p1);
    p3.reset(n16);
    p3.reset(n32);
    p1.reset(n42);
    SharedPtr p4(n32);
  }
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
