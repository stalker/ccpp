// step5_debug_v0_3.cpp
// Version: 0.3
#include <iostream>
#include "Expression.h"

using namespace std;

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

struct SharedPtr
{
    // реализуйте следующие методы
    //
    //+explicit SharedPtr(Expression *ptr = 0)
    //+~SharedPtr()
    //+SharedPtr(const SharedPtr &)
    //+SharedPtr& operator=(const SharedPtr &)
    //+Expression* get() const
    //+void reset(Expression *ptr = 0)
    // Expression& operator*() const
    //+Expression* operator->() const

    explicit SharedPtr(Expression *ptr = 0)
                      : stor_(new Stor(ptr))
    {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(Expression *ptr = 0));
    }
    ~SharedPtr() {
        cerr_FILE_LINE;
        cerr_X_nl(:~SharedPtr());
        if (stor_ != 0 && stor_->count_ != 0)
            decr();
        else if (stor_ != 0) {
            cerr_FILE_LINE;
            cerr_X(:~SharedPtr());
            cerr_X(:stor_=);
            cerr<<stor_<<endl;
            delete stor_;
            stor_ = 0;
        }
    }
    SharedPtr(const SharedPtr & oth)
             : stor_(oth.stor_)
    {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr(const SharedPtr &));
        incr();
    }
    SharedPtr& operator=(const SharedPtr & ptr) {
        cerr_FILE_LINE;
        cerr_X_nl(:SharedPtr& operator=(const SharedPtr & ptr));
        if (stor_ != ptr.stor_) {
            cerr_FILE_LINE;
            cerr_X(:SharedPtr& operator=(const SharedPtr & ptr));
            cerr_X(:stor_ != ptr.stor_);
            decr();
            stor_ = ptr.stor_;
            incr();
        }
        return *this;
    }
    Expression* get() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* get() const);
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:Expression* get() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    Expression* operator->() const {
        cerr_FILE_LINE;
        cerr_X_nl(:Expression* operator->() const);
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:Expression* operator->() const);
            cerr<<stor_<<endl;
            return stor_->ptr_;
        }
        return 0;
    }
    void reset(Expression *ptr = 0) {
        cerr_FILE_LINE;
        cerr_X_nl(:void reset(Expression *ptr = 0));
        decr();
        stor_ = new Stor(ptr);
    }
    Expression& operator*() const {
      return *get();
    }
    int count() {
        return stor_->count_;
    }

private:
    void incr() {
        cerr_FILE_LINE;
        cerr_X_nl(:incr());
        cerr_FILE_LINE;
        cerr_X(:incr());
        cerr_X(:incr():stor_);
        cerr<<'='<<stor_<<endl;
        if (stor_) {
            cerr_FILE_LINE;
            cerr_X(:incr());
            cerr_X(:incr():++stor_->count_);
            ++stor_->count_;
            cerr<<'='<<stor_->count_<<endl;
        }
    }
    void decr() {
        cerr_FILE_LINE;
        cerr_X_nl(:decr());
        if (stor_ && (stor_->count_ == 0)) {
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X(:stor_);
            cerr<<stor_<<endl;
            cerr_FILE_LINE;
            cerr_X(:decr());
            cerr_X_nl(:stor_->count_ == 0);
            delete stor_->ptr_;
            delete stor_;
            stor_ = 0;
        } else
            --stor_->count_;
    }
    struct Stor {
        explicit Stor(Expression * ptr)
        {
            if (ptr == 0) {
                ptr_ = 0;
                count_ = 0;
            } else {
                ptr_ = ptr;
                count_ = 1;
            }
        }
        Expression * ptr_;
        std::size_t  count_;
    };
    Stor * stor_;
};

int main(void) {
  {
    cerr_FILE_LINE_nl;
    Number *n32 = new Number(32.0);
    Number *n16 = new Number(16.0);
    Number *n42 = new Number(42.0);
    std::cout<<"------------------"<<std::endl;
    std::cout<<"SharedPtr p1(n32)"<<std::endl;
    SharedPtr p1(n32);
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"SharedPtr p2 = p1"<<std::endl;
    SharedPtr p2 = p1;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"SharedPtr p3(p1)"<<std::endl;
    SharedPtr p3(p1);
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p3 = "<<p3.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p3.reset(n16)"<<std::endl;
    p3.reset(n16);
    std::cout<<"p3 = "<<p3.count()<<std::endl;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p3.reset(0)"<<std::endl;
    p3.reset(0);
    std::cout<<"p3 = "<<p3.count()<<std::endl;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p1 = p1"<<std::endl;
    p1 = p1;
    std::cout<<"p3 = "<<p3.count()<<std::endl;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p3.~SharedPtr()"<<std::endl;
    p3.~SharedPtr();
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p1.reset(n42)"<<std::endl;
    p1.reset(n42);
    std::cout<<"p1 = "<<p1.count()<<std::endl;
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p1.~SharedPtr()"<<std::endl;
    p1.~SharedPtr();
    std::cout<<"p2 = "<<p2.count()<<std::endl;
    std::cout<<"------------------"<<std::endl;

    std::cout<<"p2.~SharedPtr()"<<std::endl;
    p2.~SharedPtr();
  }
    cerr_FILE_LINE;
    cerr_X_nl(:return 0;);
    return 0;
}
// EOF
