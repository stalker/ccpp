/* films2.c -- ������������� �������� ������ �������� */
#include <stdio.h>
#include <stdlib.h> /* �������� �������� ������� malloc()   */
#include <string.h> /* �������� �������� ������� strcpy()   */
#define TSIZE 45    /* ������ ������� ��� �������� �������� */

struct film {
	char title[TSIZE];
	int rating;
	struct film * next; /* ��������� �� ��������� ��������� � ������ */
};

int main(void)
{
	struct film * head = NULL;
	struct film * prev, * current;
	char input[TSIZE];

/* ���� � ���������� ���������� */
	puts("������� �������� ������� ������:");
	while (gets(input) != NULL && input[0] != '\0')
	{
		current = (struct film *) malloc(sizeof(struct film));
		if (head == NULL) /* ������ ���������      */
			head = current;
		else              /* ����������� ��������� */
			prev->next = current;
		current->next = NULL;
		strcpy(current->title, input);
		puts("������� ���� �������� �������� <0-10>:");
		scanf("%d", &current->rating);
		while(getchar() != '\n')
			continue;
		puts("������� �������� ���������� ������ (��� ������ ������ ��� ����������� ����� ����������):");
		prev = current;
	}
/* ����������� ������ ������� */
	if (head == NULL)
		printf("������ �� ���� �������. ");
	else
		printf ("������ �������:\n");
	current = head;
	while (current != NULL)
	{
		printf("�����: %s �������: %d\n",
			current->title, current->rating);
		current = current->next;
	}

/* ��������� ���������, ������� ����� ���������� ������ */
	current = head;
	while (current != NULL)
	{
		free(current);
		current = current->next;
	}
	printf("��������� ���������.\n");
	return 0;
}
