/* films1.c -- ������������� ������� �������� */
#include <stdio.h>
#define TSIZE 45 /* ������ ������� ��� �������� ��������     */
#define FMAX 5   /* ������������ ���������� �������� ������� */

struct film {
	char title[TSIZE];
	int rating;
};
int main(void)
{
	struct film movies[FMAX];
	int i = 0;
	int j;
	puts("������� �������� ������� ������:");
	while (i < FMAX && gets(movies[i].title) != NULL &&
			movies[i].title[0] != '\0')
	{
		puts("������� ���� �������� �������� <0-10>:");
		scanf("%d", &movies[i++].rating);
		while(getchar() != '\n')
			continue;
		puts("������� �������� ���������� ������ (��� ������ ������ ��� ����������� �����):");
	}
	if (i == 0)
		printf("������ �� ���� �������. ");
	else
		printf ("������ �������:\n");
	for (j = 0; j < i; j++)
		printf("�����: %s �������: %d\n", movies[j].title,
			movies[j].rating);
	printf("��������� ���������.\n");
	return 0;
}
