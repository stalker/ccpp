/* films3.c -- ������������� �������� ������ � ����� ADT */
/* ������������� ������ � list.c */
#include <stdio.h>
#include <stdlib.h> /* �������� ������� exit()      */
#include "list.h"   /* ����������� ����� List, Item */
void showmovies(Item item);

int main(void)
{
	List movies;
	Item temp;


/* ������������� */
	InitializeList(&movies);
	if (ListIsFull(&movies))
	{
		fprintf(stderr,"��������� ������ �����������! ��������� ���������.\n");
		exit(1);
	}

/* ���� � ���������� ���������� */
	puts("������� �������� ������� ������:");
	while (gets(temp.title) != NULL && temp.title[0] != '\0')
	{
		puts("������� ���� �������� �������� <0-10>:");
		scanf("%d", &temp.rating);
		while(getchar() != '\n')
			continue;
		if (AddItem(temp, &movies)==false)
		{
			fprintf(stderr,"�������� � ��������������� ������\n");
			break;
		}
		if (ListIsFull(&movies))
		{
			puts("������ �����.");
			break;
		}
		puts("������� �������� ���������� ������ (��� ������ ������ ��� ����������� �����):");
	}

/* ����������� */
	if (ListIsEmpty(&movies))
		printf("������ �� ���� �������. ");
	else
	{
		printf ("������ �������:\n");
		Traverse(&movies, showmovies);
	}
	printf("�� ����� %d �������.\n", ListItemCount(&movies));

/* ������� */
	EmptyTheList(&movies);
	printf("��������� ���������.\n");

	return 0;
}

void showmovies(Item item)
{
	printf("�����: %s �������: %d\n", item.title,
		item.rating);
}
