// Реализуйте ScopedPtr, как было описано ранее (реализуйте методы get и
// release, операторы * и ->, а так же конструктор от указателя на Expression).
// Hint: в качестве признака того, что ScopedPtr не хранит никакого указателя
// (после вызова release), используйте нулевой указатель, при этом вы можете
// явно проверить указатель в деструкторе, но это не обязательно, так как
// delete от нулевого указателя ничего не делает.
// Sample Input:
// There is no tests for this task
// Sample Output:
// OK
// Memory Limit: 256 MB
// Time Limit: 5 seconds

struct Expression;
struct Number;
struct BinaryOperation;
struct FunctionCall;
struct Variable;

struct ScopedPtr
{
    explicit ScopedPtr(Expression *ptr = 0)
                      : ptr_(ptr) { }
    ~ScopedPtr() {
        delete ptr_;
    }
    Expression* get()        const {return  ptr_;}
    Expression* release() {
        Expression *temp;
        temp = ptr_;
        ptr_ = 0;
        return temp;
    }
    void reset(Expression *ptr = 0) {
        if (ptr_ != 0)
            delete ptr_;
        ptr_ = ptr;
    }
    Expression& operator*()  const {return *ptr_;}
    Expression* operator->() const {return  ptr_;}

private:
    // запрещаем копирование ScopedPtr
    ScopedPtr(const ScopedPtr&);
    ScopedPtr& operator=(const ScopedPtr&);

    Expression *ptr_;
};
