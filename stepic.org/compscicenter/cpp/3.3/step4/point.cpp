struct Point {
    Point(double x = 0, double y = 0)
        : x(x), y(y) {}
    double x;
    double y;
};

Point p1;
Point p2(2);
Point p3(3,4);
