	.file	"point.cpp"
	.section	.text._ZN5PointC2Edd,"axG",@progbits,_ZN5PointC5Edd,comdat
	.align 2
	.weak	_ZN5PointC2Edd
	.type	_ZN5PointC2Edd, @function
_ZN5PointC2Edd:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movsd	%xmm0, -16(%rbp)
	movsd	%xmm1, -24(%rbp)
	movq	-8(%rbp), %rdx
	movq	-16(%rbp), %rax
	movq	%rax, (%rdx)
	movq	-8(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rax, 8(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	_ZN5PointC2Edd, .-_ZN5PointC2Edd
	.weak	_ZN5PointC1Edd
	.set	_ZN5PointC1Edd,_ZN5PointC2Edd
	.globl	p1
	.bss
	.align 16
	.type	p1, @object
	.size	p1, 16
p1:
	.zero	16
	.globl	p2
	.align 16
	.type	p2, @object
	.size	p2, 16
p2:
	.zero	16
	.globl	p3
	.align 16
	.type	p3, @object
	.size	p3, 16
p3:
	.zero	16
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L2
	cmpl	$65535, -8(%rbp)
	jne	.L2
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	movl	$p1, %edi
	call	_ZN5PointC1Edd
	movabsq	$4611686018427387904, %rax
	xorpd	%xmm1, %xmm1
	movq	%rax, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movl	$p2, %edi
	call	_ZN5PointC1Edd
	movabsq	$4616189618054758400, %rdx
	movabsq	$4613937818241073152, %rax
	movq	%rdx, -16(%rbp)
	movsd	-16(%rbp), %xmm1
	movq	%rax, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movl	$p3, %edi
	call	_ZN5PointC1Edd
.L2:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_p1, @function
_GLOBAL__sub_I_p1:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	_GLOBAL__sub_I_p1, .-_GLOBAL__sub_I_p1
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_p1
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
