#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String {
    String(const char *str = "");
    String(size_t n, char c);
    ~String();

    size_t size() {
        return size;
    }

    char &at(size_t idx) {
        str_[idx];
    }

    const char *c_str() {
        return str_;
    }

    size_t size_;
    char *str_;
};

char t[] = "There is no test input";

int main(void) {
    String s(t);

    return 0;
}

/*
#include <cstddef>
#include <cstring>

struct String {
    String(const char *str = "");
    String(size_t n, char c);
    ~String();

    size_t size() {
        // put your code here
    }

    char &at(size_t idx) {
        // here
    }

    const char *c_str() {
        // and here
    }

    size_t size_;
    char *str_;
};


struct String {
    String(size_t n, char c) {
        size_t i = 0;
        size_ = n;
        str_ = new char[size_ + 1];
        for(i = 0; i < size_; i++)
          str_[i] = c;
        str_[i] = '\0';
    }

    String(const char *str = "") {
        char * p1, *p2;
        p1 = (char *)str;
        while (*p1 != '\0')
            p1++;
        size_ = p1 - str;
        cout << "size_ = " << size_ << endl;
        str_ = new char[size_];
        p1 = (char *)str;
        p2 = str_;
        while (*p1 != '\0')
            *p2++ = *p1++;
        *p2 = '\0';
        cout << "str_ = " << str_ << endl;
    }

    // для освобождения памяти не используйте free
    // иначе ваша программа может не пройти
    // тестирование
    ~String() {
        delete [] str_;
    }

    size_t size_;
    char *str_;
};

*/
