#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String {
    // для аллокации памяти не используйте malloc
    // иначе ваша программа может не пройти
    // тестирование
    String(const char *str = "") {
        char * p1, *p2;
        p1 = (char *)str;
        while (*p1 != '\0')
            p1++;
        size_ = p1 - str;
        cout << "size_ = " << size_ << endl;
        str_ = new char[size_];
        p1 = (char *)str;
        p2 = str_;
        while (*p1 != '\0')
            *p2++ = *p1++;
        *p2 = '\0';
        cout << "str_ = " << str_ << endl;
    }

    // не изменяйте эти имена, иначе ваша программа
    // не пройдет тестирование
    size_t size_;
    char *str_;
};

char t[] = "There is no test input";

int main(void) {
    String s(t);

    return 0;
}
