#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String {
    String(const char *str = "");
    String(size_t n, char c);

    // для освобождения памяти не используйте free
    // иначе ваша программа может не пройти
    // тестирование
    ~String() {
        delete [] str_;
    }

    size_t size_;
    char *str_;
};

char t[] = "There is no test input";

int main(void) {
    String s(t);

    return 0;
}
/*
#include <cstddef>
#include <cstring>

struct String {
    String(const char *str = "");
    String(size_t n, char c);

    // для освобождения памяти не используйте free
    // иначе ваша программа может не пройти
    // тестирование
    ~String() {
        // put your code here
    }

    size_t size_;
    char *str_;
};
*/
