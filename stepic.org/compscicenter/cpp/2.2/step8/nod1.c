#include <math.h>

int nod(int a,int b)
{
  //Алгоритм Евклида: если a = bq + r, то НОД(а,b) = НОД(b,r)
  int max,min;

  //Находим большее по модулю число
  if(a > b) {
    max = abs(a);
    min = abs(b);
  } else {
    max = abs(b);
    min = abs(a);
  }
  //находим остаток от деления большего числа на меньшее
  int r = max % min;
  //если остаток равен нулю
  if(r == 0)
    //то вернуть меньшее число
    return min;
  else
    //иначе вычислить НОД меньшего числа и остатка
    return nod(b,r);
}
