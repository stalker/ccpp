#include <stdio.h>

int getch(void);

main(){
    int a,b;

    scanf("%d%d",&a,&b);
    while (b) b^=a^=b^=a%=b;
    printf("%d",a);
    getch();

    return 0;
}

int getch(void)
{
    return getchar();
}
