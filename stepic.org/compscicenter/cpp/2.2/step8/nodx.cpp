#include <iostream>
using namespace std;

int gcd(int x, int y);

int main(){
    int a, b, r;

    cin >> a;
    cin >> b;
    r = gcd(a, b);
    cout << r;
    cout << endl;

    return 0;
}

int gcd(int x, int y)
{
  if (x % y == 0)
    return y;
  else
    return gcd(y, x % y);
}
