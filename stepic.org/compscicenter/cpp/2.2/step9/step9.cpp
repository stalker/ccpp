#include <stdio.h>
#include <iostream>
using namespace std;

int foo(int n, int * s);

int b = 0;
int c = 0;
int l = 0;
int r = 0;

int main(){
    printf("&b=%p, &l=%p, &r=%p\n", &b, &l, &r);
    foo(3, &b);
    printf("b=%d, c=%d, l=%d, r=%d\n", b, c, l, r);

    return 0;
}

int foo(int n, int * s) {
    printf("foo(%d): c=%d, &s=%p\n", n, c, s);
    c++;
    (*s)++;
    if (n <= 0)
        return 2;
    return foo(n - 1, &l) + foo(n / 2, &r);
}