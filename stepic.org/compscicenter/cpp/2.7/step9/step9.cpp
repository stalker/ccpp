#include <limits.h>
#include <iostream>
using namespace std;

int main(){
    int m, n, i, j;
    int mini, minj;
    int min = 2147483647;

    cin >> m >> n;

    int a[m][n];

    for (i = 0 ; i < m; i++) {
        for (j = 0; j < n; j++) {
            cin >> a[i][j];
            if (a[i][j] <= min) {
                min = a[i][j];
                mini = i;
                minj = j;
            }
        }
    }

    for (j = 0; j < n; j++) {
        int c;
        c = a[0][j], a[0][j] = a[mini][j], a[mini][j] = c;
    }

    for (i = 0 ; i < m; i++) {
        for (j = 0; j < n - 1; j++) {
            cout << a[i][j] << " ";
        }
        cout << a[i][j] << endl;
    }

    return 0;
}
