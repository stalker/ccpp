#include <cstdlib>
#include <iostream>

using namespace std;

template < class T , size_t N , size_t M >
struct Matrix {
    /* ... */
    T & operator ()( size_t i , size_t j)
    { return data_[M * j + i]; }

private :
    T data_[N * M];
};

template < class T , size_t N , size_t M , size_t K >
    Matrix <T, N, K> operator *(Matrix <T, N, M> const & a,
    Matrix <T, M, K> const & b);

// log - это глобальная переменная

template <ofstream & log >
struct FileLogger { /* ... */ };
