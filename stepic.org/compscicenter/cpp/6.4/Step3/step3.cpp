// Реализуйте функцию array_size, которая возвращает размер массива, переданного в качестве параметра. Функция должна работать только для массивов! Т. е. если функции передать указатель, должна произойти ошибка компиляции. Примеры:
//
// int ints[] = {1, 2, 3, 4};
// int *iptr = ints;
// double doubles[] = {3.14};
// array_size(ints); // вернет 4
// array_size(doubles); // вернет 1
// array_size(iptr); // тут должна произойти ошибка компиляции
// Hint: в одной из первых недель мы вам показывали трюк с передачей массивов только заданного размера в функцию (передача массива по ссылке), совместите его с вашими знаниями о шаблонах.
//
// Sample Input:
// There are no tests for this task
// Sample Output:
// OK
/// Memory Limit: 256 MB
/// Time Limit: 5 seconds

#include <cstdlib>
#include <iostream>
using namespace std;
/*
template <class T ,size_t N>
struct Matrix {
private:
    T data_[N];
};
*/

// template<size_t N>
// int array_size(int a[N]) {
// }

template <class Type, size_t size>
Type array_size( const Type (&r_array)[size] ) {
    return size;
}


int main(void) {
    int ints[] = {1, 2, 3, 4};
    int *iptr = ints;
    double doubles[] = {3.14};
    cout << "array_size(ints) = " << array_size(ints) << endl; // вернет 4
    cout << "array_size(doubles) = " << array_size(doubles) << endl; // вернет 4
    // array_size(doubles); // вернет 1
    // array_size(iptr); // тут должна произойти ошибка компиляции
}
