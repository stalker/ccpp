#include <iostream>
using namespace std;

char * getline(void);

char a[99999];

int main(){
    while (!cin.eof()) {
        cin.getline(a, 99999);
        getline();
        if (a[0] != '\0')
            cout << a << endl;
    }

    return 0;
}

char * getline(void) {
    char * p1;
    char * p2;
    int  count = 0;
    bool opening_sp = true;

    p1 = a;
    p2 = a;
    while (*p2 != '\0') {
        if (*p2 != ' ') {
            opening_sp = false;
            while (*p2 != ' ' && *p2 != '\0')
                *p1++ = *p2++;
        } else {
            while (*p2 == ' ') {
                count++;
                p2++;
            }
            if (*p2 == '\0')
                break;
            if (count > 0 && !opening_sp) {
                *p1++ = ' ';
                count = 0;
                continue;
            }
        }
    }
    *p1 = '\0';
    return a;
}
