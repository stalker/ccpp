#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
#include <cstdio>
#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String {
    String(const char *str = "");
    String(size_t n, char c);

    String(String const & str)
            : size_(str.size_), str_(new char[size_])
    {
        for (size_t i = 0; i != size_; i++)
            str_[i] = str.at(i);
    }
    void swap(String & str) {
        size_t const t1 = size_;
        size_ = str.size_;
        str.size_ = t1;

        char * const t2 = str_;
        str_ = str.str_;
        str.str_ = t2;
    }
    // Перегрузка оператора
    String & operator=(String const & str) {
        if (this != &str)
            String(str).swap(*this);
        return *this;
    }
    ~String();

    size_t size();
    const char *c_str();
    int compare(String &str);
    int append(String &str);

    char at(size_t idx) const;
    char & at(size_t idx);
private:
    size_t size_;
    char *str_;
};

int main(void)
{
    // String greet("Hello");
    // char ch1 = greet.at(0);

    String const const_greet("Hello, Const!");
    char const &ch2 = const_greet.at(0);

    return 0;
}

String::String(size_t n, char c) {
    size_t i = 0;
    size_ = n;
    str_ = new char[size_ + 1];
    for(i = 0; i < size_; i++)
      str_[i] = c;
    str_[i] = '\0';
}

String::String(const char *str) {
    char * p1, *p2;
    p1 = (char *)str;
    while (*p1 != '\0')
        p1++;
    size_ = p1 - str;
    str_ = new char[size_];
    p1 = (char *)str;
    p2 = str_;
    while (*p1 != '\0')
        *p2++ = *p1++;
    *p2 = '\0';
}

String::~String() {
    delete [] str_;
}

size_t String::size() {
    return size_;
}

const char * String::c_str() {
    return str_;
}

int String::compare(String &str) {
    return strcmp(str_, str.c_str());
}

int String::append(String &str) {
    size_t i, j;
    size_t nsize = size_ + str.size();
    size_t n = nsize > size_ ? nsize : size_;
    char * nstr = new char[n];
    for(i = 0; i < size_; i++)
        nstr[i] = str_[i];
    nstr[i] = '\0';
    delete [] str_;
    str_ = nstr;
    for(j = 0; i < n; i++, j++)
        str_[i] = str.at(j);
    str_[i] = '\0';
    size_ = nsize;
}

char String::at(size_t idx) const {
    cout << "char String::at(size_t idx) const" << endl;
    return str_[idx];
}

char & String::at(size_t idx) {
    cout << "char & String::at(size_t idx)" << endl;
    return str_[idx];
}
