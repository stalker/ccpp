struct Vector2D {
    Vector2D(double x, double y) : x(x), y(y) {}
    Vector2D mult(double d) const {
        return Vector2D(x * d, y * d);
    }
    double mult(Vector2D const& p) const {
        return x * p.x + y * p.y;
    }
    double x, y;
};

Vector2D p(1, 2);
Vector2D q = p.mult(10); // (10, 20)
double r = p.mult(q); // 50
