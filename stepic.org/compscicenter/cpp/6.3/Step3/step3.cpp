template < class T >
void swap(T & a, T & b)
{
    T tmp(a);
    a = b;
    b = tmp;
}

template <>
void swap <Database >(Database & a, Database & b)
{
    a.swap(b);
}

template < class T >
void swap(Array <T> & a, Array <T> & b)
{
    a.swap(b);
}
