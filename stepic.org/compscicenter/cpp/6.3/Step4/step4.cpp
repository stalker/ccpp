#include <iostream>

using namespace std;

template < class T >
void foo(T a, T b) {
    cout << "same types" << endl;
}

template < class T , class V >
void foo(T a, V b) {
    cout << "different types" << endl;
}

template <>
void foo <int , int >( int a , int b) {
    cout << "both parameters are int" << endl;
}

int main() {
    foo(3, 4);
    return 0;
}
