#include <cstdlib>
template < class T >
struct Array {
    T & operator []( size_t i) { return data_[i]; }
    /* ... */
private :
    T * data_;
};

template < class T >
struct Array <T *> {
    explicit Array( size_t size)
    : size_(size)
    , data_( new T *[size_])
    {}

    T & operator []( size_t i) { return *data_[i]; }

private :
    size_t size_;
    T ** data_;
};
