#include <cstddef> // size_t

struct IntArray {
    explicit IntArray(size_t size)
            : size_(size)
            , data_(new int[size])
    { }
    IntArray(IntArray const & a)
            : size_(a.size_), data_(new int[size_])
    {
        for (size_t i = 0; i != size_; i++)
            data_[i] = a.data_[i];
    }
    ~IntArray() {
        delete [] data_;
    }

    // Перегрузка оператора
    IntArray & operator=(IntArray const & a) {
        if (this != &a) {
            delete [] data_;
            size_ = a.size_;
            data_ = new int[size_];
            for (size_t i = 0; i != size_; i++)
                data_[i] = a.data_[i];
        }
        return *this;
    }
    // Перегрузка оператора
    int & operator[](size_t i) {
        return data_[i];
    }
    int & get(size_t i) {
        return data_[i];
    }
    size_t size(void) {
        return size_;
    }
    void resize(size_t nsize) {
        int * ndata = new int[nsize];
        size_t n = nsize > size_ ? size_ : nsize;
        for(size_t i = 0; i != n; ++i)
            ndata[i] = data_[i];
        delete [] data_;
        data_ = ndata;
        size_ = nsize;
    }
private:
    size_t size_;
    int *  data_;
};
