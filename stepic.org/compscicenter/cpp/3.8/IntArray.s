	.file	"IntArray.cpp"
	.section	.text._ZN8IntArrayC2Em,"axG",@progbits,_ZN8IntArrayC5Em,comdat
	.align 2
	.weak	_ZN8IntArrayC2Em
	.type	_ZN8IntArrayC2Em, @function
_ZN8IntArrayC2Em:
.LFB401:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-32(%rbp), %rax
	salq	$2, %rax
	movq	%rax, %rdi
	call	_Znam
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	$0, -8(%rbp)
	jmp	.L2
.L3:
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	-8(%rbp), %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movl	$0, (%rax)
	addq	$1, -8(%rbp)
.L2:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	setne	%al
	testb	%al, %al
	jne	.L3
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE401:
	.size	_ZN8IntArrayC2Em, .-_ZN8IntArrayC2Em
	.weak	_ZN8IntArrayC1Em
	.set	_ZN8IntArrayC1Em,_ZN8IntArrayC2Em
	.section	.text._ZN8IntArrayD2Ev,"axG",@progbits,_ZN8IntArrayD5Ev,comdat
	.align 2
	.weak	_ZN8IntArrayD2Ev
	.type	_ZN8IntArrayD2Ev, @function
_ZN8IntArrayD2Ev:
.LFB407:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L4
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rdi
	call	_ZdaPv
.L4:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE407:
	.size	_ZN8IntArrayD2Ev, .-_ZN8IntArrayD2Ev
	.weak	_ZN8IntArrayD1Ev
	.set	_ZN8IntArrayD1Ev,_ZN8IntArrayD2Ev
	.globl	a
	.bss
	.align 16
	.type	a, @object
	.size	a, 16
a:
	.zero	16
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L6
	cmpl	$65535, -8(%rbp)
	jne	.L6
	movl	$10, %esi
	movl	$a, %edi
	call	_ZN8IntArrayC1Em
	movl	$__dso_handle, %edx
	movl	$a, %esi
	movl	$_ZN8IntArrayD1Ev, %edi
	call	__cxa_atexit
.L6:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE418:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_a, @function
_GLOBAL__sub_I_a:
.LFB419:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	_GLOBAL__sub_I_a, .-_GLOBAL__sub_I_a
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_a
	.hidden	__dso_handle
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
