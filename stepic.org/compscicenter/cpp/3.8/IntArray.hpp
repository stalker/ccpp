#ifndef MYINTARRAY_HPP
#define MYINTARRAY_HPP

#include <algorithm> // std::swap
#include <cstddef> // size_t

struct IntArray {
    explicit IntArray(size_t size)
            : size_(size)
            , data_(new int[size])
    {
        for (size_t i = 0; i != size_; i++)
            data_[i] = 0;
    }
    IntArray(IntArray const & a)
            : size_(a.size_), data_(new int[size_])
    {
        for (size_t i = 0; i != size_; i++)
            data_[i] = a.data_[i];
    }
    ~IntArray() {
        delete [] data_;
    }
    // Перегрузка оператора
    IntArray & operator=(IntArray const & a) {
        if (this != &a)
            IntArray(a).swap(*this);
        return *this;
    }
    // Перегрузка оператора
    int & operator[](size_t i) {
        return data_[i];
    }
    void swap(IntArray & a) {
        std::swap(size_, a.size_);
        std::swap(data_, a.data_);
    }
    int get(size_t i) const {
        return data_[i];
    }
    int & get(size_t i) {
        return data_[i];
    }
    size_t size(void) const {
        return size_;
    }
    void resize(size_t nsize) {
        IntArray t(nsize);
        size_t n = nsize > size_ ? size_ : nsize;
        for(size_t i = 0; i != n; ++i)
            t.data_[i] = data_[i];
        swap(t);
    }
private:
    size_t size_;
    int *  data_;
};

#endif