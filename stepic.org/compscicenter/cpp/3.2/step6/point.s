	.file	"point.cpp"
	.text
	.align 2
	.globl	_ZN5Point5shiftEdd
	.type	_ZN5Point5shiftEdd, @function
_ZN5Point5shiftEdd:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movsd	%xmm0, -16(%rbp)
	movsd	%xmm1, -24(%rbp)
	movq	-8(%rbp), %rax
	movsd	(%rax), %xmm0
	addsd	-16(%rbp), %xmm0
	movq	-8(%rbp), %rax
	movsd	%xmm0, (%rax)
	movq	-8(%rbp), %rax
	movsd	8(%rax), %xmm0
	addsd	-24(%rbp), %xmm0
	movq	-8(%rbp), %rax
	movsd	%xmm0, 8(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	_ZN5Point5shiftEdd, .-_ZN5Point5shiftEdd
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
