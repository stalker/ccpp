#include <iostream>
using namespace std;

char * getline(void);

char a[99999];

int main(){
    while (!cin.eof()) {
        getline();
        std::cout << a;
    }

    return 0;
}

char * getline(void) {
    char * p1;
    int  count = 0;
    bool opening_sp = true;

    p1 = a;
    do {
        cin.get(*p1);
        if (*p1 == '\n')
            break;
        if (*p1 != ' ') {
            p1++;
            opening_sp = false;
        } else {
            while (*p1 == ' ') {
                count++;
                cin.get(*p1);
            }
            if (count > 0 && !(opening_sp || *p1 == '\n')) {
                *(p1 + 1) = *p1;
                *p1++ = ' ';
            }
            if (*p1 == '\n')
                break;
            p1++;
        }
    } while (!cin.eof());
    *(p1 + 1) = '\0';

    return a;
}
