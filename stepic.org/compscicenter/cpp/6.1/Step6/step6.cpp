#include <cstddef>
#include <iostream>

template <typename T>
class Array
{
public:
    // Список операций:
    //
    // explicit Array(size_t size = 0, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    explicit Array(size_t size = 0, const T& value = T())
                     : size_(size)
                     , data_(new T[size])
    {
        for (size_t i = 0; i < size; i++)
            data_[i] = value;
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()  { delete [] data_; data_ = 0; }
    //
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Считайте, что для типа
    //   T определен оператор присваивания.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = new T[size_];
        for (size_t i = 0; i < size_; i++)
            data_[i] = v.data_[i];
    }
    //
    // Array& operator=(...)
    //   оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            delete [] data_;
            size_ = v.size_;
            data_ = new T[size_];
            for (size_t i = 0; i < size_; i++)
                data_[i] = v.data_[i];
        }
        return *this;
    }
    //
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элементов).
    size_t size() const { return size_; }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
    T    * data_;
    size_t size_;
};


using namespace std;
int main() {
  Array<int> *arr1 = new Array<int>(10, 7);
  Array<float> *arr2 = new Array<float>(5, 8.88f);
  Array<float> arr4;
  Array<float> arr3 = Array<float>(*arr2);
  arr4 = arr3;
  //my_arr1->~Array();
  //my_arr2->~Array();

  for (size_t i = 0; i < arr1->size(); i++)
    cout << "Value of " << i << " element is " << (*arr1)[i] << endl;

  cout<<endl;

  for (size_t i = 0; i < arr4.size(); i++)
    cout << "Value of " << i << " element is " << arr4[i] << endl;

  return 0;
}
