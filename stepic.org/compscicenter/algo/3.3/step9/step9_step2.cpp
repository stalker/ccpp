#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#define DEBUG 2
#include "myintervalTree.hpp"
using namespace std;

int a[MAXSIZE];

int main(void)
{
    int i, k, m, n;
    char line[255];
    char * stopstring, *p;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);

#ifdef DEBUG
    fprintf(stderr, "n = %d, m = %d\n", n, m);
#endif

    for (i = 0; i < n; i++) {
        cin >> a[i];
    }

#ifdef DEBUG
    for (i = 0; i < n; i++) {
        cout << "Input: " << a[i] << endl;
    }
#endif

    intervalTree<int> itree;
    itree.build(a, n);

    cout << "itree.treesize = " << itree.treesize << endl;
    size_t b, e;
    for (i = 0; i < m; i++) {
        cin.getline(line, 255);
#ifdef DEBUG
        fprintf(stderr, "i = %d, getline = %s\n", i, line);
#endif
        if (strncmp("Min", line, 3) == 0) {
            b = strtol(&line[3], &stopstring, 10);
            p = stopstring;
            e = strtol(p, &stopstring, 10);
#ifdef DEBUG
            cout << "Input: Min b = " << b << " e = " << e << endl;
#endif
            cout << itree.find_m(b, e);
            cout << endl;
#ifdef DEBUG
            for (i = 1; i < itree.arraysize; i++) {
                cout << "itree[" << i << "] = " << itree[i];
                printf("\n");
                // cout << endl;
            }
#endif
        } else if (strncmp("Set", line, 3) == 0) {
            b = strtol(&line[3], &stopstring, 10);
            p = stopstring;
            k = strtol(p, &stopstring, 10);
#ifdef DEBUG
            cout << "Input: Set b = " << b << " k = " << k << endl;
            cout << endl;
#endif
            itree.update(e, k);
#ifdef DEBUG
            for (i = 1; i < itree.treesize; i++) {
                cout << "itree[" << i << "] = " << itree[i];
                printf("\n");
                //cout << endl;
            }
#endif
        }
    }

    return 0;
}
