#include <iostream>
#include <cstring>
#include <cstdlib>
#include <climits>
#include <cstdio>
#include <cmath>

using namespace std;

#define MAXSIZE 1000000

int a[MAXSIZE];
const int INF = INT_MAX;
enum treeType { Maxtree, Mintree };

size_t size_t_log2(size_t n) {
    return (size_t)((log(n))/(log(2)));
}

template <class type> class intervalTree
{
public:
    type * data;
    treeType treeT;
    size_t length;
    size_t arraysize;
    size_t treesize;
    intervalTree(treeType ht = Mintree, size_t sz = MAXSIZE) {
        size_t logsize = (1 << (size_t_log2(sz - 1) + 1));
        length = logsize;
        data = new type[length];
        memset(data, 0, sizeof(type) * length);
        treeT = ht;
    }
    ~intervalTree(void) { delete [] data; }
    type& operator[](size_t node) { return data[node]; }
    type min(type v1, size_t v2) { return v1 < v2 ? v1 : v2; }
    type min_by_index(size_t i1, size_t i2) {
        return data[i1] < data[i2] ? data[i1] : data[i2];
    }
    type max(type v1, size_t v2) { return v1 > v2 ? v1 : v2; }
    type max_by_index(size_t i1, size_t i2) {
        return data[i1] > data[i2] ? data[i1] : data[i2];
    }
    void build(type a[], size_t size) {
        if (size < length) {
            arraysize = size;
            size_t n = (1 << (size_t_log2(size - 1) + 1));
            for (size_t i = 0; i < n; i++)
                data[i] = INF;
            for (size_t i = n; i < n + size; i++) {
                data[i] = a[i - n];
            }
            for (size_t i = n - 1; i > 0; i--)
                data[i] = (treeT == Mintree) ?
                          min_by_index(2 * i, 2 * i + 1) :
                          max_by_index(2 * i, 2 * i + 1);
            treesize = 2*n;
            for (size_t i = n + size; i < treesize; i++)
                data[i] = INF;
        }
    }
    size_t find_m(size_t l, size_t r) {
        type ans = INF;
        size_t n = treesize / 2;
        l += n - 1, r += n - 1;
        while (l <= r) {
            if (l & 1)
                ans = (treeT == Mintree) ?
                      min(ans, data[l]) :
                      max(ans, data[l]);
            if (!(r & 1))
                ans = (treeT == Mintree) ?
                      min(ans, data[r]) :
                      max(ans, data[r]);
            l = (l + 1) / 2, r = (r - 1) / 2;
        }
        return ans;
    }
    void update(size_t i, type x) {
        size_t n = treesize / 2;
        i += n - 1;
        data[i] = x;
        while (i /= 2)
            data[i] = (treeT == Mintree) ?
                      min_by_index(2 * i, 2 * i + 1) :
                      max_by_index(2 * i, 2 * i + 1);
    }
};

int main(void)
{
    int i, k, m, n;
    char line[255];
    char * stopstring, *p;
    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);
    for (i = 0; i < n; i++) {
        cin >> a[i];
    }
    intervalTree<int> itree;
    itree.build(a, n);
    size_t b, e;
    for (i = 0; i < m;) {
        cin.getline(line, 255);
        if (strncmp("Min", line, 3) == 0) {
            i++;
            b = strtol(&line[3], &stopstring, 10);
            p = stopstring;
            e = strtol(p, &stopstring, 10);
            cout << itree.find_m(b, e);
            cout << endl;
        } else if (strncmp("Set", line, 3) == 0) {
            i++;
            b = strtol(&line[3], &stopstring, 10);
            p = stopstring;
            k = strtol(p, &stopstring, 10);
            itree.update(b, k);
        }
        if (i > MAXSIZE - 1)
            break;
    }

    return 0;
}
