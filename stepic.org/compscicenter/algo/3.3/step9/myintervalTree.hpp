#ifndef MYINTERVALTREE_H_INCLUDED
#define MYINTERVALTREE_H_INCLUDED
#include <cmath>
#include <climits>
#ifdef DEBUG
#include <cstdio>
#endif

const int INF = INT_MAX;

#define MAXSIZE 1000000

// Перечесление типов кучи
enum treeType { Maxtree, Mintree };

size_t size_t_log2(size_t n)
{
    return (size_t)((log(n))/(log(2)));
}

template <class type> class intervalTree
{
public:
    // Массив кучи
    type * data;
    // Тип кучи
    treeType treeT;
    // Длина кучи
    size_t length;
    size_t arraysize;
    size_t treesize;

    // Конструктор
    intervalTree(treeType ht = Mintree, size_t sz = MAXSIZE)
    {
#if DEBUG > 1
        fprintf(stderr, "intervalTree::Constructor\n");
#endif
        // size_t logsize = 2 * (size_t_log2(100) + 1);
        size_t logsize = (1 << (size_t_log2(sz - 1) + 1));
        data = new type[logsize];
#if DEBUG > 1
        fprintf(stderr, "intervalTree::Constructor logsize = %lu\n", logsize);
#endif
        length = logsize;
        treeT = ht;
    }

    // Деструктор

    ~intervalTree(void)
    {
#if DEBUG > 1
        fprintf(stderr, "intervalTree::Destructor\n");
#endif
        delete [] data;
    }

    // Перегрузка оператора
    type& operator[](size_t node)
    {
        return data[node];
    }

    size_t size(void)
    {
        return length;
    }
    void swap(size_t i1, size_t i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }

    type min(type v1, size_t v2)
    {
        return v1 < v2 ? v1 : v2;
    }

    type min_by_index(size_t i1, size_t i2)
    {
        return data[i1] < data[i2] ? data[i1] : data[i2];
    }

    type max(type v1, size_t v2)
    {
        return v1 > v2 ? v1 : v2;
    }

    type max_by_index(size_t i1, size_t i2)
    {
        return data[i1] > data[i2] ? data[i1] : data[i2];
    }

    // Процедура построения дерева отрезков
    // Из основной программы вызывать эту функцию следует с параметрами:
    // v=1, tl=0, tr=n-1.
    void build(type a[], size_t size)
    {
        if (size < length) {
            arraysize = size;
            // size_t n = (1 << (int_log2(size - 1) + 1));
            size_t n = (1 << (size_t_log2(size - 1) + 1));
            for (size_t i = 0; i < n; i++)
                data[i] = INF;
#if DEBUG > 1
            fprintf(stderr, "intervalTree::build size = %lu, n = %lu,\n", size, n);
#endif
            for (size_t i = n; i < n + size; i++) {
                data[i] = a[i - n];
#if DEBUG > 1
                fprintf(stderr, "intervalTree::build data[%lu] = a[%lu] = %d,\n", i, i - n, data[i]);
#endif
            }
            for (size_t i = n - 1; i > 0; i--)
                data[i] = (treeT == Mintree) ? 
                          min_by_index(2 * i, 2 * i + 1) : 
                          max_by_index(2 * i, 2 * i + 1);
            treesize = 2*n;
            for (size_t i = n + size; i < treesize; i++)
                data[i] = INF;
#if DEBUG > 1
            fprintf(stderr, "intervalTree::build treesize = %lu\n", treesize);
#endif
        }
    }
    size_t find_m(size_t l, size_t r)
    {
        type ans = INF;
        size_t n = treesize / 2;
#if DEBUG > 1
        fprintf(stderr, "intervalTree::find_m(%lu, %lu) n = %lu\n", l, r, n);
#endif
        l += n - 1, r += n - 1;
#if DEBUG > 1
        fprintf(stderr, "intervalTree::find_m l = %lu, r = %lu\n", l, r);
#endif
        while (l <= r)
        {
            // если l - правый сын своего родителя,
            // учитываем его фундаментальный отрезок
            if (l & 1)
                ans = (treeT == Mintree) ?
                      min(ans, data[l]) :
                      max(ans, data[l]);
            // если r - левый сын своего родителя,
            // учитываем его фундаментальный отрезок
            if (!(r & 1))
                ans = (treeT == Mintree) ?
                      min(ans, data[r]) :
                      max(ans, data[r]);
            // сдвигаем указатели на уровень выше
            l = (l + 1) / 2, r = (r - 1) / 2;
        }
        return ans;
    }

    void update(size_t i, type x)
    {
        size_t n = treesize / 2;
        i += n - 1;
        data[i] = x;
        while (i /= 2)
            data[i] = (treeT == Mintree) ?
                      min_by_index(2 * i, 2 * i + 1) :
                      max_by_index(2 * i, 2 * i + 1);
    }
};

#endif // MYINTERVALTREE
