#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#define DEBUG 1
#include "myintervalTree.hpp"
using namespace std;

int a[] = {3, 8, 6, 4, 2, 5, 9, 0, 7, 1};

int main(void)
{
    int i, k, m, n;
    char line[255];
    char * stopstring, *p;

    n = (sizeof a)/(sizeof a[0]);
    printf("n = %d, log2(%d) + 1 = %d\n", n, n, int_log2(n) + 1);

    intervalTree<int> tree;
    tree.build(a, n);

    for (i = 0; i < tree.treesize; i++)
        cout << "tree[" << i << "] = " << tree[i] << endl;

    m = 1;
    n = 8;
    cout << "min(" << m << "," << n << ") = " << tree.find_m(m, n) << endl;

    tree.update(8, 1);

    m = 1;
    n = 8;
    cout << "min(" << m << "," << n << ") = " << tree.find_m(m, n) << endl;
/*
    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);
    k = int_log2(n) + 1;
    printf("n = %d, log2(%d) + 1 = %d\n", n, n, int_log2(n) + 1);
*/
    return 0;
}
