// #include <cstdio>
// #include <cstdlib>
// #include <cstring>
#include <iostream>
using namespace std;

template <class type> class heap
{
    type data[100000];
public:
    int length;
    heap(void)
    {
        length = 0;
    }
    void swap(int i1, int i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }
    int siftUp(int node)
    {
        while (node > 1 && data[node/2] < data[node]) {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }
    int siftDown(int index)
    {
        bool finish = false;

        while (2 * index <= length && !finish)
        {
            int temp = index;
            if (data[2 * index] > data[temp])
                temp = 2 * index;
            if (2 * index + 1 <= length && (data[2 * index + 1] > data[temp]))
                temp = 2 * index + 1;
            if (index == temp)
                finish = true;
            else
                swap(index, temp);
            index = temp;
        }
        return index;
    }

    int add(int value)
    {
        int i;
        length++;
        data[length] = value;
        i = siftUp(length);
        return i;
    }
    int remove(int index)
    {
          data[index] = data[1] + 1;
          siftUp(index);
          data[1] = data[length];
          length--;
          if (length)
              return siftDown(1);
          return 0;
    }
    type first(void)
    {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else
            return 0;
    }
};

int main(void)
{
    heap<int> heap;
    int e, i, n;
    char line[255];
    char * stopstring;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    for (i = 0; i < n; i++) {
        cin.getline(line, 255);
        if (strncmp("Insert", line, 5) == 0) {
            e = strtol(&line[6], &stopstring, 10);
            heap.add(e);
        } else if (strncmp("Extract", line, 6) == 0) {
            e = heap.first();
            cout << e << endl;
        }
    }
    return 0;
}
