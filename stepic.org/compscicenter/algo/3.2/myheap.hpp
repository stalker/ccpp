#ifndef MYHEAP_H_INCLUDED
#define MYHEAP_H_INCLUDED
#ifdef DEBUG
#include <cstdio>
#endif

// Перечесление типов кучи
enum heapType { MaxHeap, MinHeap };

template <class type> class heap
{
    // Массив кучи
    type * data;
    //type data[100000];
    // Тип кучи
    heapType heapT;
    // Длина кучи
    size_t length;
public:

    // Конструктор
    heap(heapType ht = MinHeap, size_t sz = 100000)
    {
#ifdef DEBUG
        fprintf(stderr, "heap::Constructor\n");
#endif
        data = new type[sz];
        length = 0;
        heapT = ht;
    }

    // Деструктор
    ~heap(void)
    {
#ifdef DEBUG
        fprintf(stderr, "heap::Destructor\n");
#endif
        delete [] data;
    }
    // Перегрузка оператора
    type& operator[](size_t node)
    {
        return data[node];
    }

    size_t size(void)
    {
        return length;
    }
    void swap(size_t i1, size_t i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }

    // Всплытие элемента под номером. Возвращает новый номер этого же элемента
    size_t siftUp(size_t node)
    {
        while ((node > 1 && data[node / 2] < data[node] && heapT == MaxHeap) \
               || (node > 1 && data[node / 2] > data[node] \
                   && heapT == MinHeap))
        {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }

    // Потопление элемента под номером. Возвращает новый номер этого же элемента
    size_t siftDown(size_t node)
    {
        bool finish = false;

        while (2 * node <= length && !finish) {
            size_t temp = node;
            if ((data[2 * node] > data[temp] && heapT == MaxHeap) ||
                (data[2 * node] < data[temp] && heapT == MinHeap))
                temp = 2 * node;
            if ((2 * node + 1 <= length && (data[2 * node + 1] > data[temp] &&
                 heapT == MaxHeap)) ||
                (data[2 * node + 1] < data[temp] && heapT == MinHeap) )
                temp = 2 * node + 1;
            if (node == temp)
                finish = true;
            else
                swap(node, temp);
            node = temp;
        }
        return node;
    }

    // Преобразование массива в кучу
    void build()
    {
        for (size_t i = length / 2; i > 0; i--)
             siftDown(i);
    }

    // Сортирует кучу по неубыванию
    void sort()
    {
        build();
        size_t t = length;
        while (length > 1) {
            swap(data[1], data[length]);
            length--;
            siftDown(1);
        }
        length = t;
    }

    // Добовляет к этой куче кучу heap
    void concat(heap heap)
    {
        for (size_t i = 1; i <= heap.length; i++)
            add(heap[i]);
    }

    // Добовление value в кучу. Возвращает новый номер этого же элемента
    size_t add(type value)
    {
        size_t i;
        length++;
        data[length] = value;
        i = siftUp(length);
        return i;
    }

    // Удаляет элемент под номером node из кучи
    size_t remove(size_t node)
    {
        data[node] = data[1] + 1;
        siftUp(node);
        data[1] = data[length];
        length--;
        if (length)
            return siftDown(1);
        return 0;
    }

    // Возвращает первый элемент и удаляет его
    type first(void)
    {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else
            return 0;
    }

    // Ставит элемент под номером node на своё место. Возвращает новый номер этого же элемента
    size_t test(size_t node)
    {
        size_t f = siftUp(node);
        if (f != node)
            return f;
        return siftDown(node);
    }
};

#endif // MinHeap
