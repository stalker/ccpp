#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;
enum heapType { MaxHeap, MinHeap };
 
template <class type> class heap
{
    // Массив кучи
    type data[100000];
    // Длина кучи
    int length;
    // Тип кучи
    heapType heapT;
public:
    // Конструктор
    heap(heapType ht = MaxHeap)
    {
        length = 0;
        heapT = ht;
    }
    // Перегрузка оператора
    type& operator[](int index)
    {
        return data[index];
    }
    int size(void)
    {
        return length;
    }
    void swap(int i1, int i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }
    void SiftDown(int node)
    {
    }
    // Просеивание вверх
    // Heap_SiftUp(A, i)
    //   while i > 1 и A[⌊i/2⌋] < A[i]
    //     do Обменять A[i] ↔ A[⌊i/2⌋]
    //       i ← ⌊i/2⌋
    int SiftUp(int node)
    {
        // cout << "inter  SiftUp(" << node << ")" << endl;
        while (node > 1 && data[node/2] < data[node]) {
            // cout << "       SiftUp: while node = " << node << endl;
            // cout << "       SiftUp: while data[" << node << "] = " << data[node] << endl;
            swap(node, node/2);
            node = node / 2;
        }
        // cout << "return SiftUp(" << node << ")" << endl;
        return node;
    }
    // Добовление value в кучу. Возвращает новый номер этого же элемента
    // Heap_add(A, key)
    //   A.heap_size ← A.heap_size + 1
    //   A[A.heap_size] ← -∞
    //   Heap_SiftUp(A, A.heap_size, key)
    int add(int value)
    {
        length++;
        data[length] = value;
        return SiftUp(length);
    }
    type extract(int node)
    {
        if (length < 1) {
            return 0;
        }
        SiftUp(node);
        type max = data[1];
        data[1] = data[length];
        length--;
        Heapify(1);
        return max;
    }
    // Heap_Extract_Max(A)
    //   if A.heap_size[A] < 1
    //     then error "Куча пуста"
    //   max ← A[1]
    //   A[1] ← A[A.heap_size]
    //   A.heap_size ← A.heap_size-1
    //   Heapify(A, 1)
    //   return max
    type first(void)
    {
        if (length) {
            type temp = data[1];
            extract(1);
            return temp;
        }
        else
            return 0;
    }
    // Heapify(A, i)
    //   left ← 2i
    //   right ← 2i+1
    //   heap_size - количество элементов в куче
    //   largest ← i
    //   if left ≤ A.heap_size и A[left] > A[i]
    //     then largest ← left
    //   if right ≤ A.heap_size и A[right] > A[largest]
    //     then largest ← right
    //   if largest ≠ i
    //     then Обменять A[i] ↔ A[largest]
    //          Heapify(A, largest)
    void Heapify(int i) {
        int l = 2 * i;
        int r = 2 * i + 2;
        int largest = i;
        if (l <= length && data[l] > data[largest])
            largest = l;
        if (r <= length && data[r] > data[largest])
            largest = r;
        if (largest != i) {
            swap(i, largest);
            Heapify(largest);
        }
    }
};

int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

#define NKEYS (sizeof a / sizeof a[0])

int main(void)
{
    heap<int> heap;
    int e, i, n;
    char line[255];
    char * stopstring;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    cout << "n = " << n << endl;
/*
    cout << NKEYS << endl;
    for (i = 0; i < NKEYS; i++)
        heap.add(a[i]);
    for (i = 1; i < NKEYS; i++) {
        cout << heap[i] << " ";
    }
    cout << heap[i] << endl;
    heap.extract(3);
*/
    for (i = 0; i < n; i++) {
        cin.getline(line, 255);
        // cout << "getline " << line << endl;
        if (strncmp("Insert", line, 5) == 0) {
            e = strtol(&line[6], &stopstring, 10);
            heap.add(e);
            cout << "Insert " << line << " e = " << e << endl;
        } else if (strncmp("Extract", line, 6) == 0) {
            cout << "Extract" << endl;
            e = heap.first();
            cout << "Result " << e << endl;
        }
    }
    // for (i = 1; i < heap.size(); i++) {
    //     cout << heap[i] << " ";
    // }
    cout << heap[i] << endl;
    return 0;
}
