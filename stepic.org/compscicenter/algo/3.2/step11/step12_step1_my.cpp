#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

template <class type> class heap
{
public:
    type data[100000];
    int length;
    // Конструктор
    heap(void)
    {
        length = 0;
    }
    void swap(int i1, int i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }
    int siftUp(int node)
    {
        while (node > 1 && data[node/2] < data[node]) {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }
    // Потопление элемента под номером. Возвращает новый номер этого же элемента
    int siftDown(int index)
    {
        bool finish = false;
    
        while (2 * index <= length && !finish)
        {
            int temp = index;
            if (data[2 * index] > data[temp])
                temp = 2 * index;
            if (2 * index + 1 <= length && (data[2 * index + 1] > data[temp]))
                temp = 2 * index + 1;
            if (index == temp)
                finish = true;
            else
                swap(index, temp);
            index = temp;
        }
        return index;
    }

    int add(int value)
    {
        int i;
        length++;
        data[length] = value;
        i = siftUp(length);
        Heapify(1);
        return i;
    }
    type extract(int node)
    {
        if (length < 1) {
            return 0;
        }
        siftUp(node);
        type max = data[1];
        data[1] = data[length];
        length--;
        Heapify(1);
        return max;
    }
    // Удаляет элемент под номером index из кучи
    int remove(int index)
    {
          data[index] = data[1] + 1;
          siftUp(index);
          data[1] = data[length];
          length--;
          if (length)
              return siftDown(1);
          return 0;
    }
    type first(void)
    {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else
            return 0;
    }
    void Heapify(int i) {
        int l = 2 * i;
        int r = 2 * i + 2;
        int largest = i;
        if (l <= length && data[l] > data[largest])
            largest = l;
        if (r <= length && data[r] > data[largest])
            largest = r;
        if (largest != i) {
            swap(i, largest);
            Heapify(largest);
        }
    }
};

int main(void)
{
    heap<int> heap;
    int e, i, n;
    char line[255];
    char * stopstring;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    for (i = 0; i < n; i++) {
        cin.getline(line, 255);
        if (strncmp("Insert", line, 5) == 0) {
            e = strtol(&line[6], &stopstring, 10);
            heap.add(e);
        } else if (strncmp("Extract", line, 6) == 0) {
            for (i = 1; i < heap.length; i++) {
                fprintf(stderr, "%d ", heap.data[i]);
            }
            fprintf(stderr, "%d\n", heap.data[i]);
            fprintf(stderr, "%s = ", line);
            e = heap.first();
            fprintf(stderr, "%d\n", e);
            for (i = 1; i < heap.length; i++) {
                fprintf(stderr, "%d ", heap.data[i]);
            }
            fprintf(stderr, "%d\n", heap.data[i]);
            cout << e << endl;
        }
    }
    return 0;
}
