// #include <cstdio>
// #include <cstdlib>
// #include <cstring>
#include <iostream>
using namespace std;

// Перечесление типов кучи
enum heapType { MaxHeap, MinHeap };

template <class type> class heap
{
    // Массив кучи
    type data[100000];
    // Тип кучи
    heapType heapT;
    // Длина кучи
    int length;
public:

    // Конструктор
    heap(heapType ht = MinHeap)
    {
            length = 0;
            heapT = ht;
    }

    // Перегрузка оператора
    type& operator[](int index)
    {
            return data[index];
    }

    int size(void)
    {
        return length;
    }
    void swap(int i1, int i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }

    // Всплытие элемента под номером. Возвращает новый номер этого же элемента
    int siftUp(int node)
    {
        while ((node > 1 && data[node / 2] < data[node] && heapT == MaxHeap) || \
               (node > 1 && data[node / 2] > data[node] && heapT == MinHeap)) {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }

    // Потопление элемента под номером. Возвращает новый номер этого же элемента
    int siftDown(int index)
    {
        bool finish = false;

        while (2 * index <= length && !finish)
        {
            int temp = index;
            if ((data[2 * index] > data[temp] && heapT == MaxHeap) ||
                (data[2 * index] < data[temp] && heapT == MinHeap))
                temp = 2 * index;
            if ((2 * index + 1 <= length && (data[2 * index + 1] > data[temp] && 
                 heapT == MaxHeap)) ||
                (data[2 * index + 1] < data[temp] && heapT == MinHeap) ) 
                temp = 2 * index + 1;
            if (index == temp)
                finish = true;
            else
                swap(index, temp);
            index = temp;
        }
        return index;
    }

    // Преобразование массива в кучу
    void build()
    {
        for (int i = length / 2; i > 0; i--)
             siftDown(i);
    }
  
    // Сортирует кучу по неубыванию
    void sort()
    {
        build();
        int t = length;
        while (length > 1) {
            swap(data[1], data[length]);
            length--;
            siftDown(1);
        }
        length = t;
    }

    // Добовляет к этой куче кучу heap
    void concat(heap heap)
    {
        for (int i = 1; i <= heap.length; i++)
                add(heap[i]);
    }
 
    // Добовление value в кучу. Возвращает новый номер этого же элемента
    int add(int value)
    {
        int i;
        length++;
        data[length] = value;
        i = siftUp(length);
        return i;
    }

    // Удаляет элемент под номером index из кучи
    int remove(int index)
    {
          data[index] = data[1] + 1;
          siftUp(index);
          data[1] = data[length];
          length--;
          if (length)
              return siftDown(1);
          return 0;
    }

    // Возвращает первый элемент и удаляет его
    type first(void)
    {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else
            return 0;
    }

    // Ставит элемент под номером index на своё место. Возвращает новый номер этого же элемента
    int test(int index)
    {
        int f = siftUp(index);
        if (f != index)
            return f;
        return siftDown(index);
    }
};

int a[] = {2, 10, 3, 14, 20, 5, 6, 18, 11, 27, 21};

#define N (sizeof a)/(sizeof a[0])

int main(void)
{
    heap<int> heap;
    int e, i, n;
    char line[255];
    char * stopstring;

    // cin.getline(line, 255);
    // n = strtol(line, &stopstring, 10);
    for (i = 0; i < N; i++) {
        heap.add(a[i]);
    }
    for (i = 1; i < N; i++) {
        cout << heap[i] << " " << endl;
    }
    cout << heap[i] << endl;
    return 0;
}
