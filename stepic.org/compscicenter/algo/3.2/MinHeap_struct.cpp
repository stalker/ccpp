// #include <cstdio>
// #include <cstdlib>
// #include <cstring>
#define DEBUG 1
#include <iostream>
#include "myheap.hpp"
using namespace std;

int a[] = {2, 10, 3, 14, 20, 5, 6, 18, 11, 27, 21};


struct mystruct {
  int pri;
  void * ptr;
};

typedef struct mystruct MYSTRUCT;

MYSTRUCT b[] = { {2, NULL}, {10, NULL}, {3, NULL}, {14, NULL}, {20, NULL}, {5, NULL}, {6, NULL}, {18, NULL} , {11, NULL}, {27, NULL} };


#define N (sizeof a)/(sizeof a[0])

int main(void)
{
    heap<int> heap(MinHeap);
    int e, i, n;
    char line[255];
    char * stopstring;

    // cin.getline(line, 255);
    // n = strtol(line, &stopstring, 10);
    for (i = 0; i < N; i++) {
        heap.add(a[i]);
    }
    for (i = 1; i < N; i++) {
        cout << heap[i] << " " << endl;
    }
    cout << heap[i] << endl;
    return 0;
}
