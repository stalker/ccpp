#include <stdio.h>
unsigned long long pow_2(int i) {
    return 1<<i;
}
int main(void) {
  int i;
  printf("введите степень двойки: ");
  scanf("%d", &i);
  puts("");
  printf("2^i = %llu\n", pow_2(i));
  
  return 0;
}
