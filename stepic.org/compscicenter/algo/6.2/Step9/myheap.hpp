#ifndef MYHEAP_H_INCLUDED
#define MYHEAP_H_INCLUDED
#ifdef DEBUG
#include <cstdio>
#include <iostream>
using namespace std;
#endif

inline unsigned long long pow2(int i) {
    return 1<<i;
}

// Перечесление типов кучи
enum heapType { MaxHeap, MinHeap };

template <class type> class heap
{
    // Массив кучи
    type * data;
    //type data[100000];
    // Тип кучи
    heapType heapT;
    // Длина кучи
    size_t length;
public:

    // Конструктор
    heap(heapType ht = MinHeap, size_t sz = 100000)
    {
#ifdef DEBUG
        fprintf(stderr, "heap::Constructor\n");
#endif
        data = new type[sz];
        memset(data, 0xFF, sizeof(type) * sz);
        length = 0;
        heapT = ht;
    }

    // Деструктор
    ~heap(void)
    {
#ifdef DEBUG
        fprintf(stderr, "heap::Destructor\n");
#endif
        delete [] data;
    }
    // Перегрузка оператора
    type& operator[](size_t node)
    {
        return data[node].n;
    }

    size_t size(void)
    {
        return length;
    }
    void swap(size_t i1, size_t i2)
    {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }

    // Всплытие элемента под номером. Возвращает новый номер этого же элемента
    size_t siftUp(size_t node)
    {
        while ((node > 1 && data[node / 2].c < data[node].c && heapT == MaxHeap) \
               || (node > 1 && data[node / 2].c > data[node].c \
                   && heapT == MinHeap))
        {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }

    // Потопление элемента под номером. Возвращает новый номер этого же элемента
    size_t siftDown(size_t node)
    {
        bool finish = false;

        while (2 * node <= length && !finish) {
            size_t temp = node;
            if ((data[2 * node].c > data[temp].c && heapT == MaxHeap) ||
                (data[2 * node].c < data[temp].c && heapT == MinHeap))
                temp = 2 * node;
            if ((2 * node + 1 <= length && (data[2 * node + 1].c > data[temp].c &&
                 heapT == MaxHeap)) ||
                (data[2 * node + 1].c < data[temp].c && heapT == MinHeap) )
                temp = 2 * node + 1;
            if (node == temp)
                finish = true;
            else
                swap(node, temp);
            node = temp;
        }
        return node;
    }

    // Преобразование массива в кучу
    void build()
    {
        for (size_t i = length / 2; i > 0; i--)
             siftDown(i);
    }

    // Сортирует кучу по неубыванию
    void sort()
    {
        build();
        size_t t = length;
        while (length > 1) {
            swap(data[1], data[length]);
            length--;
            siftDown(1);
        }
        length = t;
    }

    // Добовляет к этой куче кучу heap
    void concat(heap heap)
    {
        for (size_t i = 1; i <= heap.length; i++)
            add(heap[i]);
    }

    // Добовление value в кучу. Возвращает новый номер этого же элемента
    size_t add(type value)
    {
        size_t i;
        length++;
        data[length] = value;
        i = siftUp(length);
        return i;
    }

    // Удаляет элемент под номером node из кучи
    size_t remove(size_t node)
    {
        data[node].c = data[1].c + 1;
        siftUp(node);
        data[1] = data[length];
        length--;
        if (length)
            return siftDown(1);
        return 0;
    }

    // Возвращает первый элемент и удаляет его
    type first(void)
    {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else {
            type r;
            r.c = INT_MAX;
            r.n = 0;
            return r;
        }
    }

    // Ставит элемент под номером node на своё место. Возвращает новый номер этого же элемента
    size_t test(size_t node)
    {
        size_t f = siftUp(node);
        if (f != node)
            return f;
        return siftDown(node);
    }
    size_t find(size_t num)
    {
        for (size_t i = 1; i <= length; i++)
            if (data[i].n == num)
                return i;
        return 0;
    }
    size_t ChangePriority(size_t num, unsigned int w)
    {
        size_t i = find(num);
        if (i) {
            data[i].c = w;
            return test(i);
        }
        return 0;
    }
#ifdef DEBUG
    void print(void)
    {
        cerr<<"heap {"<<endl;
        size_t k;
        for (size_t i = 0, k = 1; k <= length; i++) {
            cerr<<"i="<<i;
            for (size_t j = 0 ; j < pow2(i) - 1; j++) {
                cerr<<" data["<<k<<"].c="<<data[k].c;
                cerr<<" data["<<k<<"].n="<<data[k].n<<" ";
                k++;
            }
            cerr<<" data["<<k<<"].c="<<data[k].c;
            cerr<<" data["<<k<<"].n="<<data[k].n<<endl;
            k++;
        }
        cerr<<"}"<<endl;
    }
#endif
};

#endif // MinHeap
