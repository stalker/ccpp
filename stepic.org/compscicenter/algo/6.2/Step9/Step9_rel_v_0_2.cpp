// Задача на программирование. Дан ориентированный взвешенный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤100000). Вес ребра — натуральное
// число, не превышающее 1000. Последняя строка содержит номера двух
// вершин u и v. Выведите кратчайшее расстояние между вершинами u и v
// или -1, если в графе нет пути из u в v.
//
// Сроки сдачи задания:
// Soft дедлайн 03.11.2014 23:59 MSK
// Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 1 3 5
// 1 3
//
// Sample Output:
// 3
//
// Memory Limit: 256 MB
// Time Limit: 5 seconds
//
// Version 0.2
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>

enum heapType { MaxHeap, MinHeap };
template <class type> class heap {
    type * data;
    heapType heapT;
    size_t length;
public:
    heap(heapType ht = MinHeap, size_t sz = 100000) {
        data = new type[sz];
        memset(data, 0xFF, sizeof(type) * sz);
        length = 0;
        heapT = ht;
    }
    ~heap(void) { delete [] data; }
    type& operator[](size_t node) { return data[node].n; }
    size_t size(void) { return length; }
    void swap(size_t i1, size_t i2) {
        type t;
        t = data[i1], data[i1] = data[i2], data[i2] = t;
    }
    size_t siftUp(size_t node) {
        while ((node > 1 && data[node / 2].c < data[node].c && heapT == MaxHeap) \
               || (node > 1 && data[node / 2].c > data[node].c \
                   && heapT == MinHeap)) {
            swap(node, node/2);
            node = node / 2;
        }
        return node;
    }
    size_t siftDown(size_t node) {
        bool finish = false;
        while (2 * node <= length && !finish) {
            size_t temp = node;
            if ((data[2 * node].c > data[temp].c && heapT == MaxHeap) ||
                (data[2 * node].c < data[temp].c && heapT == MinHeap))
                temp = 2 * node;
            if ((2 * node + 1 <= length && (data[2 * node + 1].c > data[temp].c &&
                 heapT == MaxHeap)) ||
                (data[2 * node + 1].c < data[temp].c && heapT == MinHeap) )
                temp = 2 * node + 1;
            if (node == temp)
                finish = true;
            else
                swap(node, temp);
            node = temp;
        }
        return node;
    }
    void build() {
        for (size_t i = length / 2; i > 0; i--)
             siftDown(i);
    }
    void sort() {
        build();
        size_t t = length;
        while (length > 1) {
            swap(data[1], data[length]);
            length--;
            siftDown(1);
        }
        length = t;
    }
    void concat(heap heap) {
        for (size_t i = 1; i <= heap.length; i++)
            add(heap[i]);
    }
    size_t add(type value) {
        size_t i;
        length++;
        data[length] = value;
        i = siftUp(length);
        return i;
    }
    size_t remove(size_t node) {
        data[node].c = data[1].c + 1;
        siftUp(node);
        data[1] = data[length];
        length--;
        if (length)
            return siftDown(1);
        return 0;
    }
    type first(void) {
        if (length > 0) {
            type temp = data[1];
            remove(1);
            return temp;
        }
        else {
            type r;
            r.c = LONG_LONG_MAX;
            r.n = 0;
            return r;
        }
    }
    size_t test(size_t node) {
        size_t f = siftUp(node);
        if (f != node)
            return f;
        return siftDown(node);
    }
    size_t find(size_t num) {
        for (size_t i = 1; i <= length; i++)
            if (data[i].n == num)
                return i;
        return 0;
    }
    size_t ChangePriority(size_t num, unsigned int w) {
        size_t i = find(num);
        if (i) {
            data[i].c = w;
            return test(i);
        }
        return 0;
    }
};

#define PATHSTART 128

using namespace std;
long long m, n, nn;
typedef struct path_ {
    long long n; // node
    unsigned long long c; // cost
} Path;
typedef struct graph {
    long long node;
    Path * path;
    long long path_lim;
} Graph;
long long binsearch(long long x, Path v[], long long n) {
    long long low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}
inline void paths_swap(Path * p, long long a, long long b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}
void qsort1(Path * paths, long long l, long long u) {
    long long register i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}
inline bool get_path(Graph * g, long long a, long long b) {
    if (g[a].path != NULL)
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    return false;
}
long long set_path(Graph * g, long long a, long long b, unsigned long long c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        // if (get_path(g, a, b))
        //    return -1;
        long long e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            long long new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}
void Dijkstra(Graph * g, long long * dist, long long * prev, long long s) {
    long long register i;
    long long register j;
    Path u;
    heap<Path> heap(MinHeap);
    for (i = 1; i < nn; i++) {
        if (i != s) {
            u.c = dist[i] = LONG_LONG_MAX;
            prev[i] = -1;
        } else {
            u.c = dist[i] = 0;
            prev[i] = i;
        }
        u.n = i;
        heap.add(u);
    }
    while(heap.size() > 0) { 
        u = heap.first();
        if (g[u.n].path != NULL) {
            for (i = 1; i <= g[u.n].path[0].n; i++) {
                long long v = g[u.n].path[i].n;
                unsigned long long w = dist[u.n] + g[u.n].path[i].c;
                if (dist[v] > w) {
                    dist[v] = w;
                    prev[v] = u.n;
                    heap.ChangePriority(v, w);
                }
            }
        }
    }
    return;
}
int main(void) {
    long long register i;
    long long a, b;
    unsigned long long c;
    long long u, v;
    long long * dist;
    long long * prev;
    Graph * g;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new long long[nn + 2];
    prev = new long long[nn + 2];
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
    }
    cin >> u;
    cin >> v;
    for (i = 0; i < nn; i++)
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
    Dijkstra(g, dist, prev, u);
    if (dist[v] == LONG_LONG_MAX)
        cout << -1 << endl;
    else
        cout << dist[v] << endl;
    return 0;
}
