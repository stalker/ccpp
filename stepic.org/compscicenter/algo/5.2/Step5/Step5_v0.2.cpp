// Step5.cpp
// Version 0.2
// Задача на программирование. Дан неориентированный граф в следующем
// стандартном формате. Первая строка содержит два числа n и m (1≤n,m≤1000)
// — количества вершин и рёбер в графе. Следующие m строк содержат описания
// ребер — номера вершин, соединенных ребром. Вершины нумеруются от 1 до n.
// Последняя строка содержит пару чисел u и v — номера вершин. Если из u
// достижима v (другими словами, u и v лежат в одной компоненте связности),
// необходимо вывести 1, иначе — 0.
//
// Sample Input:
// 4 3
// 1 2
// 3 2
// 4 3
// 1 4
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// Graph Theory.

#include <iostream>
#define SIZE 2001
using namespace std;
int m, n, nn;
bool visited[SIZE];
bool matrix[SIZE][SIZE];
void set_path(int a, int b) {
    matrix[a][b] = true;
}
int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = true;
    if (matrix[v][target] == true || matrix[target][v] == true) {
        visited[target] = true;
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true && visited[i] == false) {
            if(explore(i, target) == 1)
                return 1;
        }
        if (matrix[i][v] == true && visited[i] == false) {
            if(explore(i, target) == 1)
                return 1;
        }
    }
    return result;
}
int dfs(int v, int target) {
    int register i;
    for (i = 1; i < nn; i++)
        visited[i] = false;
    visited[v] = true;
    if (matrix[v][target] == true || matrix[target][v] == true) {
          visited[target] = true;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true)
            if(explore(i, target) == 1)
                return 1;
        if (matrix[i][v] == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        set_path(b, a);
    }
    cin >> u;
    cin >> v;
    cout << dfs(u, v) << endl;
    return 0;
}
