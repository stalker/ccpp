// Step5.cpp
// Version 0.1
// Задача на программирование. Дан неориентированный граф в следующем
// стандартном формате. Первая строка содержит два числа n и m (1≤n,m≤1000)
// — количества вершин и рёбер в графе. Следующие m строк содержат описания
// ребер — номера вершин, соединенных ребром. Вершины нумеруются от 1 до n.
// Последняя строка содержит пару чисел u и v — номера вершин. Если из u
// достижима v (другими словами, u и v лежат в одной компоненте связности),
// необходимо вывести 1, иначе — 0.
//
// Sample Input:
// 4 3
// 1 2
// 3 2
// 4 3
// 1 4
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// Graph Theory.

#include <iostream>

// #define DEBUG 1
#define SIZE 1001

using namespace std;

int m, n, nn;

bool visited[SIZE];
bool matrix[SIZE][SIZE];

void set_path(int a, int b) {
    matrix[a][b] = true;
}

int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = true;
#ifdef DEBUG
    cerr << "explore(" << v << ", " << target << ")" << endl;
    cerr << "explore: visited[" << v << "]=" << visited[v] << endl;
#endif
    if (matrix[v][target] == true || matrix[target][v] == true) {
        visited[target] = true;
#if DEBUG > 9
        cerr << "explore: FOUND: ma[" << v << "][" << target << "]=" << matrix[v][target] << endl;
        cerr << "explore: FOUND: ma[" << target << "][" << v << "]=" << matrix[target][v] << endl;
#endif
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true && visited[i] == false) {
#if DEBUG > 9
            cerr << "explore: 1: matrix[" << v << ", " << i << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
        if (matrix[i][v] == true && visited[i] == false) {
#if DEBUG > 9
            cerr << "explore: 2: matrix[" << i << ", " << v << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
    }
#if DEBUG > 9
    cerr << "explore(" << v << ", " << target << ") = " << result << endl;
#endif
    return result;
}

/*
int explore(Graph g[], bool visited[], int v, int t) {
    int i;
    int index = node_exists_in_graph(g, v);

    visited[v] = true;
    for (i = 1; i <= g[index].path[0]; i++)
        if (t == g[index].path[i]) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
    for (i = 1; i <= g[index].path[0]; i++) {
        int u = g[index].path[i];
        if (u == t) {
            visited[t] = true;
            return 1;
        }
        if (visited[u] == false)
            if(explore(g, visited, u, t) == 1)
                return 1;
    }
    return 0;
}
*/

int dfs(int v, int target) {
    int register i;

    for (i = 1; i < nn; i++)
        visited[i] = false;

    visited[v] = true;
    if (matrix[v][target] == true || matrix[target][v] == true) {
          visited[target] = true;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true)
            if(explore(i, target) == 1)
                return 1;
        if (matrix[i][v] == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}

int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;

    ios_base::sync_with_stdio(0);
#if DEBUG > 99
    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE - 1; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    cin >> n; nn = n + 1;
    cin >> m;
#if DEBUG > 0
    cerr << "main: n=" << n << "; m=" << m << endl;
#endif
    // for (i = 0; i < n; i++)
    //    visited[i] = false;
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
#if DEBUG > 1
        cerr << "main: a=" << a << "; b=" << b << endl;
#endif
        set_path(a, b);
        set_path(b, a);
    }
    cin >> u;
    cin >> v;
#if DEBUG > 0
    cerr << "main: u=" << u << "; v=" << v << endl;
#endif
#if DEBUG > 9
    for (i = 1; i < nn; i++) {
        for (j = 1; j < n; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    cout <<
#ifdef DEBUG
        " dfs=" <<
#endif
        dfs(u, v) << endl;
    return 0;
}




/*
typedef struct graph {
    int node;
    int * path;
    bool * visited;
} Graph;

int node_exists_in_graph(Graph g[], int v) {
#if DEBUG > 7
    cerr << "node_exists_in_graph: " << v << endl;
#endif
    for (int i = 0; i < n; i++)
        if (g[i].node == v)
            return i;
    return -1;
}

// int c = -1; // крайний индекс в g
int set_path(Graph g[], int * i, int a, int b) {
    int k = node_exists_in_graph(g, a);
#if DEBUG > 7
    cerr << "set_path: *c=" << *i << "; a=" << a << "; b=" << b << "; k=" << k << endl;
#endif
    if (k == -1) {
        int c = ++(*i);
        g[c].node = a;
        g[c].path = new int[l + 2];
        g[c].path[0] = 1;
        g[c].path[1] = b;
#if DEBUG > 7
        cerr << "set_path: c=" << c << "; g[c].node=" << g[c].node;
        cerr << "; g[c].path[0]=" << g[c].path[0];
        cerr << "; g[c].path[1]=" << g[c].path[1] << endl;
#endif
        return 0;
    } else {
        for (int i = 1; i <= g[k].path[0]; i++)
            if (g[k].path[i] == b)
                return -1;
        int e = g[k].path[0] + 1;
        g[k].path[e] = b;
        g[k].path[0] = e;
#if DEBUG > 7
        cerr << "set_path: e=" << e << "; g[e].node=" << g[k].node;
        cerr << "; g[c].path[" << e << "]=" << g[k].path[e] << endl;
#endif
        return 1;
    }
}

int explore(Graph g[], bool visited[], int v, int t) {
    int i;
    int index = node_exists_in_graph(g, v);

    visited[v] = true;
#ifdef DEBUG
    cerr << "explore: visited[" << v << "]=" << visited[v] << ", index=" << index << endl;
    cerr << "explore: g[" << index << "].path[0]=" << g[index].path[0] << endl;
#endif
    for (i = 1; i <= g[index].path[0]; i++)
        if (t == g[index].path[i]) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
    for (i = 1; i <= g[index].path[0]; i++) {
        int u = g[index].path[i];
#ifdef DEBUG
        cerr << "explore: u=" << u << endl;
        if (u == t) {
            cerr << "explore: u=" << u << " == t="<< t << endl;
            visited[t] = true;
            return 1;
        }
        if (visited[u] == false) {
            cerr << "explore: explore(g, visited, " << u << ");" << endl;
            if(explore(g, visited, u, t) == 1)
                return 1;
        }
#else
        if (u == t) {
            visited[t] = true;
            return 1;
        }
        if (visited[u] == false)
            if(explore(g, visited, u, t) == 1)
                return 1;
#endif
    }
    return 0;
}

int dfs(Graph g[], int n, bool visited[], int v, int t) {
    int i;
    int index = node_exists_in_graph(g, v);

#if DEBUG > 0
    cerr << "dfs: v=" << v << ", index=" << index << endl;
    cerr << "dfs: g[index].path[0]=" << g[index].path[0] << endl;
#endif
    for (i = 0; i < n; i++)
        visited[i] = false;
    for (i = 1; i <= g[index].path[0]; i++)
        if (t == g[index].path[i]) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
    for (i = 1; i <= g[index].path[0]; i++) {
        int u = g[index].path[i];
        if (u == t) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
#if DEBUG > 2
        cerr << "dfs: u=" << u << endl;
#endif
        visited[v] = true;
#ifdef DEBUG
        if (visited[u] == false) {
            cerr << "dfs: explore(g, visited, " << u << ");" << endl;
            if(explore(g, visited, u, t) == 1)
                return 1;
        }
#else
        if (visited[u] == false)
            if(explore(g, visited, u, t) == 1)
                return 1;
#endif
    }
    return 0;
}
*/
