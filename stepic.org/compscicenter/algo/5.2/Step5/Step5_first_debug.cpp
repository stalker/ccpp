// Step5.cpp
// Version 0.1
// Задача на программирование. Дан неориентированный граф в следующем
// стандартном формате. Первая строка содержит два числа n и m (1≤n,m≤1000)
// — количества вершин и рёбер в графе. Следующие m строк содержат описания
// ребер — номера вершин, соединенных ребром. Вершины нумеруются от 1 до n.
// Последняя строка содержит пару чисел u и v — номера вершин. Если из u
// достижима v (другими словами, u и v лежат в одной компоненте связности),
// необходимо вывести 1, иначе — 0.
//
// Sample Input:
// 4 3
// 1 2
// 3 2
// 4 3
// 1 4
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// Graph Theory.

#include <iostream>
#define DEBUG 1

using namespace std;

int m, n;

/* локальный тип данных */
typedef struct graph {
    int node;
    int * path;
    bool * visited;
} Graph;

int node_exists_in_graph(Graph g[], int v) {
#ifdef DEBUG
    cerr << "node_exists_in_graph: " << v << endl;
#endif
    for (int i = 0; i < n; i++)
        if (g[i].node == v)
            return i;
    return -1;
}

// int c = -1; // крайний индекс в g
int set_path(Graph g[], int * i, int a, int b) {
    int k = node_exists_in_graph(g, a);
#ifdef DEBUG
    cerr << "set_path: *c=" << *i << "; a=" << a << "; b=" << b << "; k=" << k << endl;
#endif
    if (k == -1) {
        int c = ++(*i);
        g[c].node = a;
        g[c].path = new int[m + 1];
        g[c].path[0] = 1;
        g[c].path[1] = b;
#ifdef DEBUG
        cerr << "set_path: c=" << c << "; g[c].node=" << g[c].node;
        cerr << "; g[c].path[0]=" << g[c].path[0];
        cerr << "; g[c].path[1]=" << g[c].path[1] << endl;
#endif
        return 0;
    } else {
        int e = g[k].path[0] + 1;
        g[k].path[e] = b;
        g[k].path[0] = e;
#ifdef DEBUG
        cerr << "set_path: e=" << e << "; g[e].node=" << g[k].node;
        cerr << "; g[c].path[" << e << "]=" << g[k].path[e] << endl;
#endif
        return 1;
    }
}

int main(void) {
    int a, b, c, i, j, u, v;

    cin >> n;
    cin >> m;

    Graph * g = new Graph[n];
    bool * visited = new bool[n];

    for (i = 0; i < n; i++) {
        visited[i] = false;
        g[i].node = -1;
        g[i].path = NULL;
    }
    for (c = -1, i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
#if DEBUG > 8
        cerr << "main: a=" << a << "; b=" << b << endl;
#endif
        set_path(g, &c, a, b);
        set_path(g, &c, b, a);
    }
    cin >> u;
    cin >> v;
#if DEBUG > 0
    cerr << "main: u=" << u << "; v=" << v << endl;
#endif
#ifdef DEBUG
    for (i = 0; i < n; i++) {
        if (g[i].path != NULL) {
            cerr << "vertice: " << g[i].node << " paths: ";
            for (j = 1; j < g[i].path[0]; j++) {
                cerr << g[i].path[j] << ", ";
            }
            cerr << g[i].path[j] << ";" << endl;
        }
    }
#endif
    j = 0;
    c = node_exists_in_graph(g, u);
    if (c != -1) {
#ifdef DEBUG
        cout << "vertice: " << g[c].node << " paths: ";
#endif
#ifdef DEBUG
        for (i = 1; i < g[c].path[0]; i++) {
            cout << g[i].path[j] << ", ";
            if (g[c].path[i] == v)
                j = 1;
        }
        cout << g[c].path[i] << ";" << endl;
#else
        for (i = 1; i < g[c].path[0]; i++)
            if (g[c].path[i] == v) {
                j = 1;
                break;
            }
#endif
        cerr << g[i].path[j] << ";" << endl;
    }
    cout << j << endl;
    for (i = 0; i < n; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;

    delete [] visited;
    delete [] g;
    return 0;
}
