// Step5.cpp
// Version 0.1
// Задача на программирование. Дан неориентированный граф в следующем
// стандартном формате. Первая строка содержит два числа n и m (1≤n,m≤1000)
// — количества вершин и рёбер в графе. Следующие m строк содержат описания
// ребер — номера вершин, соединенных ребром. Вершины нумеруются от 1 до n.
// Последняя строка содержит пару чисел u и v — номера вершин. Если из u
// достижима v (другими словами, u и v лежат в одной компоненте связности),
// необходимо вывести 1, иначе — 0.
//
// Sample Input:
// 4 3
// 1 2
// 3 2
// 4 3
// 1 4
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// Graph Theory.

#include <iostream>

// #define DEBUG 1
#define SIZE 2002

using namespace std;

int m, n, nn;

int  visited[SIZE];
bool matrix[SIZE][SIZE];

void set_path(int a, int b) {
    matrix[a][b] = true;
}

int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = true;
#ifdef DEBUG
    cerr << "explore(" << v << ", " << target << ")" << endl;
    cerr << "explore: visited[" << v << "]=" << visited[v] << endl;
#endif
    if (matrix[v][target] == true || matrix[target][v] == true) {
        visited[target] = true;
#if DEBUG > 9
        cerr << "explore: FOUND: ma[" << v << "][" << target << "]=" << matrix[v][target] << endl;
        cerr << "explore: FOUND: ma[" << target << "][" << v << "]=" << matrix[target][v] << endl;
#endif
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true && visited[i] == false) {
#if DEBUG > 9
            cerr << "explore: 1: matrix[" << v << ", " << i << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
        if (matrix[i][v] == true && visited[i] == false) {
#if DEBUG > 9
            cerr << "explore: 2: matrix[" << i << ", " << v << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
    }
#if DEBUG > 9
    cerr << "explore(" << v << ", " << target << ") = " << result << endl;
#endif
    return result;
}

int dfs(int v, int target) {
    int register i;

    for (i = 1; i < nn; i++)
        visited[i] = false;

    visited[v] = true;
    if (matrix[v][target] == true || matrix[target][v] == true) {
          visited[target] = true;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true)
            if(explore(i, target) == 1)
                return 1;
        if (matrix[i][v] == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}

int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;

    ios_base::sync_with_stdio(0);
#if DEBUG > 99
    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE - 1; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    cin >> n; nn = n + 1;
    cin >> m;
#if DEBUG > 0
    cerr << "main: n=" << n << "; m=" << m << endl;
#endif
    // for (i = 0; i < n; i++)
    //    visited[i] = false;
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
#if DEBUG > 1
        cerr << "main: a=" << a << "; b=" << b << endl;
#endif
        set_path(a, b);
        set_path(b, a);
    }
    cin >> u;
    cin >> v;
#if DEBUG > 0
    cerr << "main: u=" << u << "; v=" << v << endl;
#endif
#if DEBUG > 99
    for (i = 1; i < nn; i++) {
        for (j = 1; j < n; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    cout <<
#ifdef DEBUG
        " dfs=" <<
#endif
        dfs(u, v) << endl;
    return 0;
}
