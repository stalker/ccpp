// Step7.cpp
// Version 0.3
// Задача на программирование. Дан неориентированный граф, содержащий n вершин
// и m рёбер (1≤n≤1000, 0≤m≤1000). Необходимо вывести количество компонент
// связности в заданном графе.
// Сроки сдачи задания:
//
// Soft дедлайн 27.10.2014 23:59 MSK
//
// Hard дедлайн 03.11.2014 23:59 MSK
//
// Sample Input:
//
// 4 2
// 1 2
// 3 2
//
// Sample Output:
//
// 2
//
// Memory Limit: 256 MB
//
// Time Limit: 5 seconds

#include <cstdio>
#include <cstring>
#include <iostream>
#define DEBUG 2
using namespace std;
int m, n, nn, ccnum = 0;
// bool matrix[SIZE][SIZE];

/* локальный тип данных */
typedef struct graph {
    int node;
    int * path;
    int path_lim;
} Graph;

Graph * g;
int * visited;

bool get_path(int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}

int set_path(int a, int b) {
#if DEBUG > 1
    cerr << "set_path: a=" << a << "; b=" << b << endl;
#endif
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[8];
        memset(g[a].path, 0, sizeof(int) * 8);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
#if DEBUG > 1
        cerr << "set_path: g[c].node=" << g[a].node;
        cerr << "; g[c].path[0]=" << g[a].path[0];
        cerr << "; g[c].path[1]=" << g[a].path[1] << endl;
#endif
        return 0;
    } else {
        //for (int i = 1; i <= g[a].path[0]; i++)
        if (get_path(a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
#if DEBUG > 1
            cerr << "set_path: g[a].path_lim=" << g[a].path_lim;
            cerr << "; e=" << e << endl;
#endif
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
#if DEBUG > 1
        cerr << "set_path: g[a].node=" << g[a].node;
        cerr << "; g[a].path[0]=" << g[a].path[0];
        cerr << "; g[a].path[1]=" << g[a].path[1] << endl;
#endif
        return 1;
    }
}

int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = ccnum;
    if (get_path(v, target) == true || get_path(target, v) == true) {
        visited[target] = ccnum;
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true && visited[i] == 0) {
            if(explore(i, target) == 1)
                return 1;
        }
        if (get_path(i, v) == true && visited[i] == 0) {
            if(explore(i, target) == 1)
                return 1;
        }
    }
    return result;
}

int dfs(int v, int target) {
    int register i;
    int register j;
    visited[v] = ccnum;
    if (get_path(v, target) == true || get_path(target, v) == true) {
          visited[target] = ccnum;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true)
            if(explore(i, target) == 1)
                return 1;
        if (get_path(i, v) == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;
#ifndef DEBUG
    ios_base::sync_with_stdio(0);
#endif
    cin >> n; nn = n + 1;
    cin >> m;
    visited = new int[nn];
    memset(visited, 0, sizeof(int) * nn);
    g = new Graph[nn];
    memset(g, 0, sizeof(Graph) * nn);
#if DEBUG > 1
    for (i = 0; i < nn; i++) {
        cerr << "main: g[" << i << "]=" << g[i].node << endl;
        fprintf(stderr, "main: g[%d] = %p\n", i, g[i].path);
    }
#endif
    // return 0;
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        set_path(b, a);
    }
    ccnum=1;
    dfs(1, nn + 1);
    for (i = 2; i < nn; i++) {
        if (visited[i] == 0) {
            for (j = 1; j < nn; j++) {
                if (visited[j] != 0)
                    notfound = true;
                else {
                    notfound = false;
                    break;
                }
            }
            if (notfound)
                break;
            else
                ccnum++;
            dfs(i, nn + 1);
        }
    }
    cout << ccnum << endl;
    return 0;
}
