// Step7.cpp
// Version 0.4

#include <ctime>
#include <cstdio>
#include <cstring>
#include <iostream>

#define DEBUG 2

using namespace std;

int m, n, nn, ccnum = 0;

typedef struct graph {
    int node;
    int * path;
    int path_lim;
    int visited;
    time_t start_t;
    time_t end_t;
} Graph;
Graph * g;

bool get_path(int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}

void set_visited(int v) {
    g[v].visited = ccnum;
    time(&g[v].start_t);
}

void set_end_t(int v) {
    time(&g[v].end_t);
}

int get_visited(int v) {
    return g[v].visited;
}

int set_path(int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[8];
        memset(g[a].path, 0, sizeof(int) * 8);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        if (get_path(a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}

int explore(int v, int target) {
    int result = 0;
    int register i;

    set_visited(v);
    if (get_path(v, target) == true || get_path(target, v) == true) {
        set_visited(target);
        set_end_t(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true && get_visited(i) == 0) {
            if(explore(i, target) == 1) {
                set_end_t(v);
                return 1;
            }
        }
        if (get_path(i, v) == true && get_visited(i) == 0) {
            if(explore(i, target) == 1) {
                set_end_t(v);
                return 1;
            }
        }
    }
    set_end_t(v);
    return result;
}

int dfs(int v, int target) {
    int register i;
    int register j;

    set_visited(v);
    if (get_path(v, target) == true || get_path(target, v) == true) {
        set_visited(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true)
            if(explore(i, target) == 1)
                return 1;
        if (get_path(i, v) == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}

int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;

#ifndef DEBUG
    ios_base::sync_with_stdio(0);
#endif
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
#if DEBUG > 2
    for (i = 0; i < nn; i++) {
        cerr << "main: g[" << i << "].node=" << g[i].node << endl;
        fprintf(stderr, "main: g[%d].path= %p\n", i, g[i].path);
        cerr << "main: g[" << i << "].start_t=" << ctime(g[i].start_t) << endl;
        cerr << "main: g[" << i << "].end_t=" << g[i].end_t << endl;
    }
#endif
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        // set_path(b, a);
    }
    ccnum=1;
    dfs(1, nn + 1);
    for (i = 2; i < nn; i++) {
        if (get_visited(i) == 0) {
            for (j = 1; j < nn; j++) {
                if (get_visited(j) != 0)
                    notfound = true;
                else {
                    notfound = false;
                    break;
                }
            }
            if (notfound)
                break;
            else
                ccnum++;
            dfs(i, nn + 1);
        }
    }
#if DEBUG > 1
    for (i = 0; i < nn; i++) {
        cerr << "main: g[" << i << "].node=" << g[i].node << endl;
        fprintf(stderr, "main: g[%d].path = %p\n", i, g[i].path);
        cerr << "main: g[" << i << "].start_t=" << ctime(&g[i].start_t);
        cerr << "main: g[" << i << "].end_t=" << ctime(&g[i].end_t) << endl;
    }
#endif
    cout << ccnum << endl;
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;
    delete [] g;
    return 0;
}
// EOF
