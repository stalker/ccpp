// Step5.cpp
// Version 0.6
// Задача на программирование. Дан неориентированный граф в следующем
// стандартном формате. Первая строка содержит два числа n и m (1≤n,m≤1000)
// — количества вершин и рёбер в графе. Следующие m строк содержат описания
// ребер — номера вершин, соединенных ребром. Вершины нумеруются от 1 до n.
// Последняя строка содержит пару чисел u и v — номера вершин. Если из u
// достижима v (другими словами, u и v лежат в одной компоненте связности),
// необходимо вывести 1, иначе — 0.
//
// Sample Input:
// 4 3
// 1 2
// 3 2
// 4 3
// 1 4
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// Graph Theory.

#include <iostream>

using namespace std;

int m, n;

/* локальный тип данных */
typedef struct graph {
    int node;
    int * path;
    bool * visited;
} Graph;

int node_exists_in_graph(Graph g[], int v) {
    for (int i = 0; i < n; i++)
        if (g[i].node == v)
            return i;
    return -1;
}

// int c = -1; // крайний индекс в g
int set_path(Graph g[], int * i, int a, int b) {
    int k = node_exists_in_graph(g, a);
    if (k == -1) {
        int c = ++(*i);
        g[c].node = a;
        g[c].path = new int[m + 2];
        g[c].path[0] = 1;
        g[c].path[1] = b;
        return 0;
    } else {
        for (int i = 1; i <= g[k].path[0]; i++)
            if (g[k].path[i] == b)
                return -1;
        int e = g[k].path[0] + 1;
        g[k].path[e] = b;
        g[k].path[0] = e;
        return 1;
    }
}

int explore(Graph g[], bool visited[], int v, int t) {
    int i;
    int index = node_exists_in_graph(g, v);

    visited[v] = true;
    for (i = 1; i <= g[index].path[0]; i++)
        if (t == g[index].path[i]) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
    for (i = 1; i <= g[index].path[0]; i++) {
        int u = g[index].path[i];
        if (u == t) {
            visited[t] = true;
            return 1;
        }
        if (visited[u] == false)
            if(explore(g, visited, u, t) == 1)
                return 1;
    }
    return 0;
}

int dfs(Graph g[], int n, bool visited[], int v, int t) {
    int i;
    int index = node_exists_in_graph(g, v);

    for (i = 0; i < n; i++)
        visited[i] = false;
    for (i = 1; i <= g[index].path[0]; i++)
        if (t == g[index].path[i]) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
    for (i = 1; i <= g[index].path[0]; i++) {
        int u = g[index].path[i];
        if (u == t) {
            visited[v] = true;
            visited[t] = true;
            return 1;
        }
        visited[v] = true;
        if (visited[u] == false)
            if(explore(g, visited, u, t) == 1)
                return 1;
    }
    return 0;
}

int main(void) {
    int a, b, c, i, j, u, v;

    cin >> n;
    cin >> m;

    Graph * g = new Graph[n];
    bool * visited = new bool[n + 1];

    for (i = 0; i < n; i++) {
        visited[i] = false;
        g[i].node = -1;
        g[i].path = NULL;
    }
    for (c = -1, i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, &c, a, b);
        set_path(g, &c, b, a);
    }
    cin >> u;
    cin >> v;
    cout << dfs(g, n, visited, u, v) << endl;
    for (i = 0; i < n; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;

    delete [] visited;
    delete [] g;
    return 0;
}
