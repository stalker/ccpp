// Step7.cpp
// Version 0.1
// Задача на программирование. Дан неориентированный граф, содержащий n вершин
// и m рёбер (1≤n≤1000, 0≤m≤1000). Необходимо вывести количество компонент
// связности в заданном графе.
// Сроки сдачи задания:
//
// Soft дедлайн 27.10.2014 23:59 MSK
//
// Hard дедлайн 03.11.2014 23:59 MSK
//
// Sample Input:
//
// 4 2
// 1 2
// 3 2
//
// Sample Output:
//
// 2
//
// Memory Limit: 256 MB
//
// Time Limit: 5 seconds

#include <iostream>

#define DEBUG 1
#define SIZE 2002

using namespace std;

int m, n, nn, ccnum = 0;

int  visited[SIZE];
bool matrix[SIZE][SIZE];

void set_path(int a, int b) {
    matrix[a][b] = true;
}

int explore(int v, int target) {
    int result = 0;
    int register i;
    visited[v] = ccnum;
#ifdef DEBUG
    cerr << "explore(" << v << ", " << target << ")" << endl;
    cerr << "explore: visited[" << v << "]=" << visited[v] << endl;
#endif
    if (matrix[v][target] == true || matrix[target][v] == true) {
        visited[target] = ccnum;
#if DEBUG > 9
        cerr << "explore: FOUND: ma[" << v << "][" << target << "]=" << matrix[v][target] << endl;
        cerr << "explore: FOUND: ma[" << target << "][" << v << "]=" << matrix[target][v] << endl;
#endif
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true && visited[i] == 0) {
#if DEBUG > 9
            cerr << "explore: 1: matrix[" << v << ", " << i << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
        if (matrix[i][v] == true && visited[i] == 0) {
#if DEBUG > 9
            cerr << "explore: 2: matrix[" << i << ", " << v << "] = " << matrix[v][i] << endl;
#endif
            if(explore(i, target) == 1)
                return 1;
        }
    }
#if DEBUG > 9
    cerr << "explore(" << v << ", " << target << ") = " << result << endl;
#endif
    return result;
}

int dfs(int v, int target) {
    int register i;
    int register j;
    visited[v] = ccnum;
    if (matrix[v][target] == true || matrix[target][v] == true) {
          visited[target] = ccnum;
          return 1;
    }
    for (i = 1; i < nn; i++) {
        if (matrix[v][i] == true)
            if(explore(i, target) == 1)
                return 1;
        if (matrix[i][v] == true)
            if(explore(i, target) == 1)
                return 1;
    }
    return 0;
}

int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;

    ios_base::sync_with_stdio(0);
#if DEBUG > 99
    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE - 1; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    cin >> n; nn = n + 1;
    cin >> m;
#if DEBUG > 0
    cerr << "main: n=" << n << "; m=" << m << endl;
#endif
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
#if DEBUG > 1
        cerr << "main: a=" << a << "; b=" << b << endl;
#endif
        set_path(a, b);
        set_path(b, a);
    }
#if DEBUG > 99
    for (i = 1; i < nn; i++) {
        for (j = 1; j < n; j++)
            cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j]  << ", ";
        cerr << "main: ma[" << i << "][" << j << "]=" << matrix[i][j] << endl;
    }
#endif
    for (i = 1; i < nn; i++)
        visited[i] = 0;

    ccnum=1;
    dfs(2, 2001);
    for (i = 2; i < nn; i++) {
        cerr << "visited[" << i << "]=" << visited[i] << endl;
        if (visited[i] == 0) {
            cerr << "visited[" << i << "]=" << visited[i] << endl;
            for (j = i + 1; j < nn; j++) {
                if (visited[j] != 0)
                    notfound = true;
                else {
                    notfound = false;
                    break;
                }
            }
            if (notfound)
                break;
            else
                ccnum++;
            dfs(i, 2001);
        }
    }
#ifdef DEBUG
    cerr << "main: visited[] = { ";
    for (i = 1; i < n; i++) {
        cerr << visited[i] << ", ";
    }
    cerr << visited[i] << " };" << endl;
#endif
    cout <<
#ifdef DEBUG
            "ccnum=" <<
#endif
             ccnum << endl;
    return 0;
}

/*
int dfs(int v, int target) {
    int register i;
    int register j;
    bool notfound = false;

    for (i = 1; i < nn; i++)
        visited[i] = 0;

    for (j = 1; i < nn; i++) {
        if (visited[v] == 0) {
            notfound = true;
            for (i = i + 1; i < nn; i++)
                if (visited[v] == 0) {
                    notfound = false;
                    break;
                }
            if (notfound)
                break;
            else
                ccnum++;
            visited[v] = true;
            if (matrix[v][target] == true || matrix[target][v] == true) {
                  visited[target] = ccnum;
                  return 1;
            }
            for (i = 1; i < nn; i++) {
                if (matrix[v][i] == true)
                    if(explore(i, target) == 1)
                        return 1;
                if (matrix[i][v] == true)
                    if(explore(i, target) == 1)
                        return 1;
            }
        }
    }
    return 0;
}
*/
