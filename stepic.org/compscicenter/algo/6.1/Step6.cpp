// Version 0.4
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define USE_BINSEARCH 1
#define PATHSTART 128
using namespace std;
int m, n, nn;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
} Graph;
int binsearch(int x, int v[], int n)
{
    int low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid])
            high = mid - 1;
        else if (x > v[mid])
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}
inline bool get_path(Graph * g, int a, int b) {
    int register i;
    if (g[a].path != NULL) {
#ifdef USE_BINSEARCH
        return (binsearch(b, &g[a].path[1], g[a].path[0]) != -1);
#else
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
#endif
    }
    return false;
}
int set_path(Graph * g, int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
#ifdef USE_BINSEARCH
        if (get_path(g, a, b))
            return -1;
#endif
        int e = g[a].path[0] + 1;
        if (e >= (g[a].path_lim - 1)) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int bfs(Graph * g, int * dist, int s) {
    int register i;
    queue <int> q;
    for (i = 0; i < nn; i++)
        dist[i] = INT_MAX;
    dist[s] = 0;
    q.push(s);
    while (!q.empty()) {
        int register u;
        int register v;
        u = q.front();
        q.pop();
        if (g[u].path != NULL) {
          for (i = 1; i <= g[u].path[0]; i++) {
            v = g[u].path[i];
            if (dist[v] == INT_MAX) {
                q.push(v);
                dist[v] = dist[u] + 1;
            }
          }
        }
    }
    return 0;
}
Graph * g;
int * dist;
int main(void) {
    int register i;
    int a, b;
    int u, v;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new int[nn + 2];
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, a, b);
        set_path(g, b, a);
    }
    cin >> u;
    cin >> v;
#define USE_BINSEARCH 1
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            sort(&g[i].path[1], &g[i].path[1] + g[i].path[0]);
#endif
    bfs(g, dist, u);
    if (dist[v] == INT_MAX)
        cout << -1 << endl;
    else
        cout << dist[v] << endl;
    return 0;
}
