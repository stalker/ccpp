#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#define PATHSTART 256
using namespace std;
int m, n, nn;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
} Graph;
typedef int Item;
#define MAXQUEUE 100001
typedef struct node {
    Item item;
    struct node * next;
} Node;
typedef struct queue {
    Node * front;
    Node * rear;
    int items;
} Queue;
static void CopyToNode(Item item, Node * pn);
static void CopyToItem(Node * pn, Item * pi);
void InitializeQueue(Queue * pq) {
    pq->front = pq->rear = NULL;
    pq->items = 0;
}
bool QueueIsFull(const Queue * pq) { return pq->items == MAXQUEUE; }
bool QueueIsEmpty(const Queue * pq) { return pq->items == 0; }
int QueueItemCount(const Queue * pq) { return pq->items; }
bool EnQueue(Item item, Queue * pq) {
    Node * pnew;
    if (QueueIsFull(pq))
        return false;
    pnew = (Node *) malloc( sizeof(Node));
    if (pnew == NULL) {
       exit(1);
    }
    CopyToNode(item, pnew);
    pnew->next = NULL;
    if (QueueIsEmpty(pq))
        pq->front = pnew;
    else
        pq->rear->next = pnew;
    pq->rear = pnew;
    pq->items++;
    return true;
}
bool DeQueue(Item * pitem, Queue * pq) {
    Node * pt;
    if (QueueIsEmpty(pq))
        return false;
    CopyToItem(pq->front, pitem);
    pt = pq->front;
    pq->front = pq->front->next;
    free(pt);
    pq->items--;
    if (pq->items == 0)
        pq->rear = NULL;
    return true;
}
void EmptyTheQueue(Queue * pq) {
    Item dummy;
    while (!QueueIsEmpty(pq))
        DeQueue(&dummy, pq);
}
static void CopyToNode(Item item, Node * pn) { pn->item = item; }
static void CopyToItem(Node * pn, Item * pi) { *pi = pn->item; }
inline bool get_path(Graph * g, int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}
int set_path(Graph * g, int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[PATHSTART];
        memset(g[a].path, 0, sizeof(int) * PATHSTART);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        if (get_path(g, a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int bfs(Graph * g, Queue * q, int * dist, int s) {
    int register i;
    for (i = 0; i < nn; i++) {
        dist[i] = INT_MAX;
    }
    dist[s] = 0;
    if (!QueueIsFull(q)) {
        EnQueue(s, q);
        while (!QueueIsEmpty(q)) {
            int register u;
            int register v;
            DeQueue(&u, q);
            for (i = 1; i <= g[u].path[0]; i++) {
                v = g[u].path[i];
                if (dist[v] == INT_MAX) {
                    if (QueueIsFull(q)) {
                        return -2;
                    }
                    EnQueue(v, q);
                    dist[v] = dist[u] + 1;
                }
            }
        }
    } else {
        return -2;
    }
    return 0;
}
Queue q;
Graph * g;
int * dist;
int * prew;
int main(void) {
    int register i;
    int register j;
    int a, b;
    int u, v;
    bool zero = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new int[nn + 2];
    for (i = 0; i < nn; i++) {
    }
    prew = new int[nn + 2];
    memset(prew, 0, sizeof(int) * (nn + 2));
    InitializeQueue(&q);
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, a, b);
        set_path(g, b, a);
    }
    cin >> u;
    cin >> v;
    for (i = 0; i < nn; i++) {
        if (g[i].path != NULL) {
            sort(&g[i].path[1], &g[i].path[1] + g[i].path[0]);
        }
    }
    bfs(g, &q, dist, u);
    if (dist[v] == INT_MAX) {
        cout << -1 << endl;
    } else {
        cout << dist[v] << endl;
    }
    for (i = 0; i < nn; i++) {
    }
    return 0;
}
