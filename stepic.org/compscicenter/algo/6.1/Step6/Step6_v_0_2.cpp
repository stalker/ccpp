// Step6.cpp
// Version 0.2
// Задача на программирование. Дан неориентированный граф на n вершинах и m
// рёбрах (1≤n,m≤100000), а также номера вершин u и v. Выведите количество
// рёбер на кратчайшем пути между вершинами u и v или -1, если пути между ними
// в графе нет.
//
// Сроки сдачи задания:
// Soft дедлайн 03.11.2014 23:59 MSK
// Hard дедлайн 10.11.2014 23:59 MSK
// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// 2 4
// Sample Output:
// 2
#include <ctime>
#include <queue>
#include <vector>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define DEBUG 1
// #define USE_FIND 1
#define USE_BINSEARCH 1
#define PATHSTART 128
#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':'<<#x<<(v)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':'<<#x<<(v)<<#y
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':'<<#x<<(v)<<#y<<endl
#define cerr_func_X_V_nl(x,v) cerr<<':'<<__func__<<':'<<#x<<(v)<<endl
#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl
#ifdef DEBUG
int cnt = 0;
#endif
using namespace std;
int m, n, nn;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
} Graph;
int binsearch(int x, int v[], int n)
{
    int low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid])
            high = mid - 1;
        else if (x > v[mid])
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}
inline bool get_path(Graph * g, int a, int b) {
    int register i;
    if (g[a].path != NULL) {
#ifdef USE_BINSEARCH
        return (binsearch(b, &g[a].path[1], g[a].path[0]) != -1);
#else
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
#endif
    }
    return false;
}
int set_path(Graph * g, int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[PATHSTART];
        // memset(g[a].path, 0, sizeof(int) * PATHSTART);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
#ifdef USE_BINSEARCH
        if (get_path(g, a, b)) {
            cerr_FILE_LINE;
            cerr_func_X_V_nl(get_path(g, a, b)=-1:a=, a);
            cerr_FILE_LINE;
            cerr_func_X_V_nl(get_path(g, a, b)=-1:b=, b);
            return -1;
        }
#endif
        int e = g[a].path[0] + 1;
        if (e >= (g[a].path_lim - 1)) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            // memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
#ifdef DEBUG
            cnt++;
            cerr_FILE_LINE;
            cerr_func_X_V_nl(cnt=, cnt);
#endif
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int bfs(Graph * g, int * dist, int s) {
    int register i;
    queue <int> q;
    for (i = 0; i < nn; i++) {
        dist[i] = INT_MAX;
    }
    dist[s] = 0;
    q.push(s);
    while (!q.empty()) {
        int register u;
        int register v;
        u = q.front();
        q.pop();
        if (g[u].path != NULL) {
          for (i = 1; i <= g[u].path[0]; i++) {
            v = g[u].path[i];
            if (dist[v] == INT_MAX) {
                q.push(v);
                dist[v] = dist[u] + 1;
            }
          }
        }
    }
    return 0;
}
Graph * g;
int * dist;
time_t Start, End;
double difTime;
int main(void) {
    int register i;
    int a, b;
    int u, v;
#ifdef DEBUG
    time(&Start);
#endif
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new int[nn + 2];
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, a, b);
        set_path(g, b, a);
    }
    cin >> u;
    cin >> v;
#ifdef DEBUG
    time(&End);
    difTime = difftime(End, Start);
    cerr << "main: cin: " << difTime << endl;
    time(&Start);
#endif
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            sort(&g[i].path[1], &g[i].path[1] + g[i].path[0]);
#ifdef DEBUG
    time(&End);
    difTime = difftime(End, Start);
    cerr << "main: sort: " << difTime << endl;
    time(&Start);
#endif
    bfs(g, dist, u);
    if (dist[v] == INT_MAX)
        cout << -1 << endl;
    else
        cout << dist[v] << endl;
    return 0;
}
