// Step11.cpp
// Version 0.2
// Задача на программирование. Дан ациклический ориентированный граф на n
// вершинах и m рёбрах (1≤n≤100000, 0≤m≤100000). Необходимо вывести n чисел
// — номера вершин в порядке любой топологической сортировки.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK 
// Hard дедлайн 03.11.2014 23:59 MSK
//
// Sample Input:
// 4 3
// 1 2
// 4 1
// 3 1
// Sample Output:
// 4 3 1 2

#include <ctime>
#include <stack>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
int m, n, nn;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
    int visited;
    int indegree;
    time_t start_t;
    time_t end_t;
} Graph;
Graph * g;
int toposize = 0;
int * topolofy;
int remove(int v);
bool get_path(int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}
int set_path(int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[8];
        memset(g[a].path, 0, sizeof(int) * 8);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        // if (get_path(a, b))
        //    return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;
    bool loop = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    topolofy = new int[nn + 2];
    memset(topolofy, 0, sizeof(int) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        // set_path(b, a);
    }
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL) {
            sort(&g[i].path[1], &g[i].path[1] + g[i].path[0]);
        }
    for (i = 1; i < nn; i++) {
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0]; j++) {
                int k = g[i].path[j];
                g[k].indegree++;
            }
        }
    }
    while (toposize < n) {
        stack <int> st;
        for (i = 1; i < nn; i++) {
            if (g[i].indegree == 0) {
                g[i].indegree--;
                st.push(i);
            }
        }
        while (!(st.empty())) {
            remove(st.top());
            topolofy[toposize++] = st.top();
            st.pop();
        }
    }
    for (j = 0; j < toposize - 1; j++)
        cout << topolofy[j] << " ";
    cout << topolofy[j] << endl;
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;
    delete [] g;
    return 0;
}
int remove(int v) {
    int register i;
    int register j;
    if (g[v].path != NULL) {
        for (j = 1; j <= g[v].path[0]; j++) {
            i = g[v].path[j];
            if (i != v)
                g[i].indegree--;
        }
    }
    return 0;
}
// EOF

