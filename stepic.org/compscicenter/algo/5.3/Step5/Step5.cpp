// Step5.cpp
// Version 0.2
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n,m≤1000). Необходимо вывести 1, если граф содержит цикл, и 0 в противном
// случае.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK
// Hard дедлайн 03.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 5 seconds

#include <ctime>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
int m, n, nn, ccnum = 0;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
    int visited;
    time_t start_t;
    time_t end_t;
} Graph;
Graph * g;
bool get_path(int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}
void set_visited(int v) {
    g[v].visited = ccnum;
    time(&g[v].start_t);
}
void set_end_t(int v) {
    time(&g[v].end_t);
}
int get_visited(int v) {
    return g[v].visited;
}
int set_path(int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[8];
        memset(g[a].path, 0, sizeof(int) * 8);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        if (get_path(a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int explore(int v, int target) {
    int result = 0;
    int register i;
    set_visited(v);
    if (get_path(v, target) == true) {
        set_visited(target);
        set_end_t(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true && (result = get_visited(i)) == ccnum
            && g[i].end_t == 0)
            return -1;
        if (get_path(v, i) == true && get_visited(i) == 0) {
            if((result = explore(i, target)) == 1) {
                set_end_t(v);
                return 1;
            }
            else if (result == -1)
                return -1;
        }
    }
    set_end_t(v);
    return result;
}
int dfs(int v, int target) {
    int result;
    int register i;
    int register j;
    set_visited(v);
    if (get_path(v, target) == true) {
        set_visited(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_path(v, i) == true) {
            if((result = explore(i, target)) == 1)
                return 1;
            else if(result == -1)
                return -1;
        }
    }
    return 0;
}
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool notfound = false;
    bool loop = false;
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(a, b);
        // set_path(b, a);
    }
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL) {
            sort(&g[i].path[1], &g[i].path[1] + g[i].path[0]);
        }
    ccnum=1;
    if (dfs(1, nn + 1) == -1) {
        loop = true;
    } else {
        for (i = 2; i < nn; i++) {
            if (get_visited(i) == 0) {
                for (j = 1; j < nn; j++) {
                    if (get_visited(j) != 0)
                        notfound = true;
                    else {
                        notfound = false;
                        break;
                    }
                }
                if (notfound)
                    break;
                else
                    ccnum++;
                if (dfs(i, nn + 1) == -1) {
                    loop = true;
                    break;
                }
            }
        }
    }
    if (loop)
        cout << "1" << endl;
    else
        cout << "0" << endl;
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;
    delete [] g;
    return 0;
}
// EOF
