//
// Задача на программирование. Дан взвешенный ориентированный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤10000). Вес ребра — целое число,
// по модулю не превышающее 1000. Выведите 1, если в графе есть цикл
// отрицательного веса, и 0 в противном случае.
//
// Сроки сдачи задания:
//
//Soft дедлайн 03.11.2014 23:59 MSK
//
//Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 3 1 -5
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 10 seconds
// Version 0.4
//
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define PATHSTART 128
using namespace std;
typedef long long Tint;
typedef unsigned long long Tuint;
Tint m, n, nn;
typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;
typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
} Graph;
Tint * relx;
bool over[10001][10001];
Tint binsearch(Tint x, Path v[], Tint n) {
    Tint low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}
void paths_swap(Path * p, Tint a, Tint b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}
void qsort1(Path * paths, Tint l, Tint u) {
    Tint i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}
inline bool get_path(Graph * g, Tint a, Tint b) {
    if (g[a].path != NULL) {
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    }
    return false;
}

Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        if (get_path(g, a, b))
           return -1;
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}
bool Relax(Tint * dist, Tint * prev, Tint u, Tint v, Tint w, Tint i) {
    bool relax = false;
                /* Tint w = dist[u] + g[u].path[j].c; */
                /* */
    if (dist[v] > w) {
        dist[v] = w;
        prev[v] = u;
        relax = true;
        if (i > n) {
            relx[v] += 1;
            over[u][v] = true;
        }
    }
    return relax;
}
bool BellmanFord(Graph * g, Tint * dist, Tint * prev, Tint s) {
    Tint register i;
    Tint register j;
    Tint register u;
    bool cont = false;
    bool relax = false;
    Path p;
    for (i = 1; i < nn; i++) {
        if (i != s) {
            p.c = dist[i] = (LLONG_MAX - 2000);
            prev[i] = -1;
        } else {
            p.c = dist[i] = 0;
            prev[i] = i;
        }
        p.n = i;
    }
    for (i = 1; i < nn*nn; i++) {
        if (i > n)
            cont = false;
        for (u = 1; u < nn; u++) {
            if (g[u].path == NULL) {
                continue;
            }
            for (j = 1; j <= g[u].path[0].n; j++) {
                Tint v = g[u].path[j].n;
                if (i > n && over[u][v])
                    continue;
                Tint w;
                if ((LLONG_MAX - 2000) != dist[u])
                    w = dist[u] + g[u].path[j].c;
                else
                    w = dist[u];
                // if ( u != v)
                //    relax = (Relax(dist, prev, u, v, w, i) or relax);
                // relax = (Relax(dist, prev, u, v, w, i) || relax);
                if (Relax(dist, prev, u, v, w, i))
                    cont = true;
            }
        }
        if (!cont)
            break;
    }
    return relax;
}

int main(void) {
    Tint register i;
    Tint a, b, c;
    Tint u, v, s;
    Tint * dist;
    Tint * prev;
    Graph * g;
    bool relax = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    cin >> s;
    if (0 == m) {
        for (i = 0; i < nn; ++i) {
            if (s == i)
                cout << "0" << endl;
            else
                cout << "*" << endl;
        }
        return 0;
    }
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new Tint[nn + 2];
    prev = new Tint[nn + 2];
    relx = new Tint[nn + 2];
    memset(relx, 0, sizeof(Tint) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
    }
    for (i = 0; i < nn; i++)
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
    relax = BellmanFord(g, dist, prev, s);
    for (i = 1; i < nn; i++) {
        if ((LLONG_MAX - 2000) == dist[i])
          cout << "*" << endl;
        else if (0 != relx[i])
          cout << "-" << endl;
        else
           cout << dist[i] << endl;
    }
        // else if (dist[i] < 0)
        //   cout << "-" << endl;

    return 0;
}
