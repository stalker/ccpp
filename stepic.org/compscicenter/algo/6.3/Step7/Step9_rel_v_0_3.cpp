//
// Задача на программирование. Дан взвешенный ориентированный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤10000). Вес ребра — целое число,
// по модулю не превышающее 1000. Выведите 1, если в графе есть цикл
// отрицательного веса, и 0 в противном случае.
//
// Сроки сдачи задания:
//
//Soft дедлайн 03.11.2014 23:59 MSK
//
//Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 3 1 -5
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 10 seconds
// Version 0.3
//
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define PATHSTART 128
using namespace std;
typedef long long Tint;
typedef unsigned long long Tuint;
Tint m, n, nn;
typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;
typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
} Graph;
Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}
inline bool Relax(Tint * dist, Tint u, Tint v, Tint w) {
    bool relax = false;
    if (dist[v] > w) {
        dist[v] = w;
        relax = true;
    }
    return relax;
}

bool BellmanFord(Graph * g, Tint * dist, Tint * prev, Tint s) {
    Tint register i;
    Tint register j;
    Tint register u;
    bool relax = false;
    Path p;
    for (i = 1; i < nn; i++) {
        if (i != s)
            p.c = dist[i] = INT_MAX;
        else
            p.c = dist[i] = 0;
        p.n = i;
    }
    for (i = 1; i < nn; i++) {
        if (i == n)
            relax = false;
        for (u = 1; u < nn; u++) {
            if (g[u].path == NULL)
                continue;
            for (j = 1; j <= g[u].path[0].n; j++) {
                Tint v = g[u].path[j].n;
                Tint w = dist[u] + g[u].path[j].c;
                relax = (Relax(dist, u, v, w) || relax);
            }
        }
    }
    return relax;
}
int main(void) {
    Tint register i;
    Tint a, b, c;
    Tint u, v;
    Tint * dist;
    Tint * prev;
    Graph * g;
    bool relax = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new Tint[nn + 2];
    prev = new Tint[nn + 2];
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
    }
    relax = BellmanFord(g, dist, prev, 1);
    if (relax)
        cout << 1 << endl;
    else
        cout << 0 << endl;

    return 0;
}
