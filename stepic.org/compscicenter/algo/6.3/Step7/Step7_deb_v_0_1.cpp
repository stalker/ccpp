// Задача на программирование. Дан ориентированный взвешенный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤100000). Вес ребра — натуральное
// число, не превышающее 1000. Последняя строка содержит номера двух
// вершин u и v. Выведите кратчайшее расстояние между вершинами u и v
// или -1, если в графе нет пути из u в v.
//
// Сроки сдачи задания:
// Soft дедлайн 03.11.2014 23:59 MSK
// Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 1 3 5
// 1 3
//
// Sample Output:
// 3
//
// Memory Limit: 256 MB
// Time Limit: 5 seconds
//
// Version 0.2
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>

#define DEBUG 1
#include "myheap.hpp"

#define USE_BINSEARCH 1
#define PATHSTART 128

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl

#define cerr_X_V(x,v) cerr<<#x<<(v)
#define cerr_X_V_nl(x,v) cerr_X_V(x,v)<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':';cerr_X_V(x,v)
#define cerr_func_X_V_nl(x,v) cerr_func_X_V(x,v)<<endl

#define cerr_X_V_Y(x,v,y) cerr<<#x<<(v)<<#y
#define cerr_X_V_Y_nl(x,v,y) cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)<<endl

#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

using namespace std;

int m, n, nn;

typedef struct path_ {
    int n; // node
    unsigned int c; // cost
} Path;

typedef struct graph {
    int node;
    Path * path;
    int path_lim;
} Graph;

void cerr_graph(Graph * g, const char * func, const char * var);

int binsearch(int x, Path v[], int n) {
    int low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

void paths_swap(Path * p, int a, int b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}

void qsort1(Path * paths, int l, int u) {
    int i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}

inline bool get_path(Graph * g, int a, int b) {
    if (g[a].path != NULL) {
#ifdef USE_BINSEARCH
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
#else
        int register i;
        for (i = 1; i <= g[a].path[0].n; i++)
            if (g[a].path[i].n == b)
                return true;
#endif
    }
    return false;
}

int set_path(Graph * g, int a, int b, int c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
#ifdef USE_BINSEARCH
        if (get_path(g, a, b))
            return -1;
#endif
        int e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            int new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}

/*
процедура Dijkstra(G, s)
для всех вершин u ∈ V:
    dist[u] ← ∞
    prev[u] ← nil
dist[s] ← 0
H ← MakeQueue(V) { dist в качестве ключей }
пока H не пусто:
    u ← ExtrackMin(H)
    для всех рёбер (u, v) ∈ E:
        если dist[v] > dist[u] + w(u, v):
            dist[v] ← dist[u] + w(u, v)
            prev[v] ← u
            ChangePriority(H, v, dist[v])
*/
void Dijkstra(Graph * g, int * dist, int * prev, int s) {
    int register i;
    int register j;
    Path u;
    heap<Path> heap(MinHeap);

    for (i = 1; i < nn; i++) {
        if (i != s) {
            u.c = dist[i] = INT_MAX;
            prev[i] = -1;
        } else {
            u.c = dist[i] = 0;
            prev[i] = i;
        }
        u.n = i;
        heap.add(u);
    }
#if DEBUG > 9
    for (j = 1; j < nn; j++) {
        Path t = heap.first();
        cerr_FILE_LINE;
        cerr_func_X_V(heap.first()=>p.n=, t.n);
        cerr_func_X_V_nl( p.c=, t.c);
    }
#endif 
#ifdef DEBUG
    heap.print();
#endif
   
    while(heap.size() > 0) { 
        u = heap.first();
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V(u.n=, u.n);
        cerr_X_V(; u.c=, u.c);
        cerr_X_V_nl(; g[u.n].path=, g[u.n].path);
#endif
        if (g[u.n].path != NULL) {
            for (i = 1; i <= g[u.n].path[0].n; i++) {
                int v = g[u.n].path[i].n;
                unsigned int w = dist[u.n] + g[u.n].path[i].c;
                cerr_FILE_LINE;
                cerr_func_X_V(i=, i);
                cerr_X_V(; v=, v);
                cerr_X_V(; w=, w);
                cerr_X_V(; g[u.n].path[i].n=, g[u.n].path[i].n);
                cerr_X_V_nl(; g[u.n].path[i].c=, g[u.n].path[i].c);
                if (dist[v] > w) {
                    dist[v] = w;
                    prev[v] = u.n;
                    heap.ChangePriority(v, w);
#ifdef DEBUG
                    cerr_FILE_LINE;
                    cerr_func_X_V(heap.ChangePriority(v, w) u.n=, u.n);
                    cerr_X_V(; v=, v);
                    cerr_X_V_nl(; w=, w);
                    heap.print();
#endif
                }
            }
        }
    }
    return;
}


int main(void) {
    int register i;
    int a, b, c;
    int u, v;
    int * dist;
    int * prev;
    Graph * g;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new int[nn + 2];
    prev = new int[nn + 2];
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
    }
    cin >> u;
    cin >> v;
    cerr_graph(g, __func__, "g");
    cerr_FILE_LINE;
    cerr_func_X_V_nl(sizeof Path=, g[1].path[0].n * sizeof(Path));
#ifdef USE_BINSEARCH
    for (i = 0; i < nn; i++)
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
#endif
#ifdef DEBUG
    cerr_graph(g, __func__, "g");
#endif
    Dijkstra(g, dist, prev, u);
#ifdef DEBUG
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(dist[, i,]);
        cerr_X_V_nl(=, dist[i]);
    }
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(prev[, i,]);
        cerr_X_V_nl(=, prev[i]);
    }
#endif
    if (dist[v] == INT_MAX)
        cout << -1 << endl;
    else
        cout << dist[v] << endl;
    return 0;
}

void cerr_graph(Graph * g, const char * func, const char * var) {
    int register i;
    int register j;
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr << ':' << func << ": " << var << "[" << i << "].node=" << g[i].node << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, i, g[i].path);
#endif
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0].n; j++) {
                cerr_FILE_LINE;
                cerr << ':'<< func << ": " << var << "[" << i << "].path[" << j << "].n=" << g[i].path[j].n;
                cerr << ": " << var << "[" << i << "].path[" << j << "].c=" << g[i].path[j].c << endl;
            }
        }
        cerr << endl;
    }
}
/*
int bfs(Graph * g, int * dist, int s) {
    int register i;
    queue <int> q;
    for (i = 0; i < nn; i++)
        dist[i] = INT_MAX;
    dist[s] = 0;
    q.push(s);
    while (!q.empty()) {
        int register u;
        int register v;
        u = q.front();
        q.pop();
        if (g[u].path != NULL) {
          for (i = 1; i <= g[u].path[0]; i++) {
            v = g[u].path[i];
            if (dist[v] == INT_MAX) {
                q.push(v);
                dist[v] = dist[u] + 1;
            }
          }
        }
    }
    return 0;
}
*/
