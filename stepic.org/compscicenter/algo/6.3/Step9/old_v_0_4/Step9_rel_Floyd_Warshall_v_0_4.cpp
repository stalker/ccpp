//
// Задача на программирование. Дан взвешенный ориентированный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤10000). Вес ребра — целое число,
// по модулю не превышающее 1000. Выведите 1, если в графе есть цикл
// отрицательного веса, и 0 в противном случае.
//
// Сроки сдачи задания:
//
//Soft дедлайн 03.11.2014 23:59 MSK
//
//Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 3 1 -5
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 10 seconds
// Version 0.4
//
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define F_W 1
#define MINF 1999999999999999999
using namespace std;
typedef long long Tint;
typedef unsigned long long Tuint;
Tint m, n, nn;
Tint w[1001][1001];
inline void set_path(Tint a, Tint b, Tint c) {
    if (c < w[a][b])
    w[a][b] = c;
}
inline Tint min(Tint a, Tint b) {
    return (a < b ? a : b);
}
int main(void) {
    int register i;
    int register j;
    int register k;
    int register t;
    Tint a, b, c;
    Tint u, v, s;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    cin >> s;
    for (i = 1; i < nn; ++i)
        for (j = 1; j < nn; ++j)
            if (i == j)
                w[i][j] = 0;
            else
                w[i][j] = MINF;
    if (0 == m) {
        for (i = 1; i < nn; ++i) {
            if (s == i)
                cout << "0" << endl;
            else
                cout << "*" << endl;
        }
        return 0;
    }
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(a, b, c);
    }
    for (k = 1; k < nn; ++k)
        for (i = 1; i < nn; ++i)
            for (j = 1; j < nn; ++j)
                if (w[i][k] < MINF && w[k][j] < MINF)
                    w[i][j] = min(w[i][j], w[i][k] + w[k][j]);
    for (i = 1; i < nn; ++i)
        for (j = 1; j < nn; ++j)
            for (t = 1; t < nn; ++t)
                if (w[i][t] < MINF && w[t][t] < 0 && w[t][j] < MINF)
                    w[i][j] = -MINF;
    for (i = 1; i < nn; ++i)
        if (s == i)
          cout << "0" << endl;
        else if (MINF == w[s][i])
          cout << "*" << endl;
        else if (-MINF == w[s][i])
          cout << "-" << endl;
        else
          cout << w[s][i] << endl;
    return 0;
}
