#include <iostream>
#include <vector>
using namespace std;
const int inf = 1555555555;
struct edge {
    int a, b, cost;
};

int n, m, v, i, s;
vector<edge> e;
void solve() {
    vector<int> d (n, inf);
    d[0] = 0;
    for (;;) {
        bool any = false;
        for (int j=0; j<m; ++j)
            if (d[e[j].a] < inf)
                if (d[e[j].b] > d[e[j].a] + e[j].cost) {
                    d[e[j].b] = d[e[j].a] + e[j].cost;
                    any = true;
                }
        if (!any)  break;
    }
    for(int i=1; i<n; i++)
        if(d[i]<inf)
            cout<<d[i]<<endl;
        else
            cout<<"NO"<<endl;

}

int main(){
    cin>>n>>m>>s;
    edge t;
    for(i=0; i<m; i++)
    {
        cin>>t.a>>t.b>>t.cost;
        t.a--; t.b--;
        e.push_back(t);
    }
    solve();
    return 0;
}
