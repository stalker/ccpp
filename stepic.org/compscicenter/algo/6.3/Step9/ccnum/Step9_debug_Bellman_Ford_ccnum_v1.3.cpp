// Step9.cpp
// Version 1.4
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n≤100000, 0≤m≤100000). Выведите количество компонент сильной связности в
// данном графе.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK
// Hard дедлайн 03.11.2014 23:59 MSK

// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 2
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// #include <ctime>

#include <stack>
#include <vector>
#include <cstdio>
#include <cstring>
#include <climits>
#include <iostream>
#include <algorithm>
#define PATHSTART 128
#define MAXSIZE 1001
#define MINF LLONG_MAX/10000

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl

#define cerr_X_V(x,v) cerr<<#x<<(v)
#define cerr_X_V_nl(x,v) cerr_X_V(x,v)<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':';cerr_X_V(x,v)
#define cerr_func_X_V_nl(x,v) cerr_func_X_V(x,v)<<endl

#define cerr_X_V_Y(x,v,y) cerr<<#x<<(v)<<#y
#define cerr_X_V_Y_nl(x,v,y) cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)<<endl

#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

using namespace std;

typedef long long Tint;
typedef unsigned long long Tuint;

vector<bool> used;
vector<Tint> order, component;

Tint * relx;
bool over[MAXSIZE][MAXSIZE];

Tint m, n, nn;

typedef struct cc_ {
    Tint n; // number
    bool i; // inf
    Tint v;
} Cc;

Tint ccnum = 1;
Cc * cc[MAXSIZE];

typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;

typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
    Tint visited;
} Graph;

#define DEBUG 1
void cerr_graph(Graph * g, const char * func, const char * var);

Tint binsearch(Tint x, Path v[], Tint n) {
    Tint low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

void paths_swap(Path * p, Tint a, Tint b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}

void qsort1(Path * paths, Tint l, Tint u) {
    Tint i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}

bool zero_visited(Graph * g, Tint i = 1) {
    bool found = false;
    Tint register j;
    for (j = i; j < nn; j++) {
        if (g[j].visited == 0) {
            found = true;
            break;
        }
    }
    return found;
}

inline bool get_path(Graph * g, Tint a, Tint b) {
    if (g[a].path != NULL)
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    return false;
}

inline void set_visited(Graph * g, Tint v) {
    g[v].visited = ccnum;
}

inline Tint get_visited(Graph * g, Tint v) {
    return g[v].visited;
}

Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        // if (get_path(g, a, b))
        //     return -1;
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}

void dfs1 (Graph * g, Tint v) {
    used[v] = true;
    if (NULL != g[v].path)
        for (size_t i = 1; i <= g[v].path[0].n; ++i)
            if (!used[g[v].path[i].n])
                dfs1(g, g[v].path[i].n);
    order.push_back(v);
}

void dfs2 (Graph * g, Tint v) {
    used[v] = true;
    component.push_back(v);
    if (NULL != g[v].path)
        for (size_t i = 1; i <= g[v].path[0].n; ++i)
            if (!used[g[v].path[i].n])
                dfs2(g, g[v].path[i].n);
}

bool dfs3 (Graph * g, Tint v, Tint u) {
    used[v] = true;
    if (v == u)
        return true;
    if (NULL != g[v].path)
        for (size_t i = 1; i <= g[v].path[0].n; ++i)
            if (!used[g[v].path[i].n])
                if (dfs3(g, g[v].path[i].n, u))
                     return true;
    return false;
}

bool Relax(Tint * dist, Tint u, Tint v, Tint w, Tint i) {
    bool relax = false;
    if (dist[v] > w) {
        dist[v] = w;
        relax = true;
        if (i > n) {
            relx[v] += 1;
            over[u][v] = true;
        }
    }
    return relax;
}
bool BellmanFord(Graph * g, Tint * dist, Tint s) {
    Tint register i;
    Tint register j;
    Tint register u;
    bool cont = false;
    bool relax = false;
    Path p;
    for (i = 1; i < nn; i++) {
        if (i != s) {
            p.c = dist[i] = MINF;
        } else {
            p.c = dist[i] = 0;
        }
        p.n = i;
    }
    for (i = 1;; i++) {
        if (i > n)
            cont = false;
        for (u = 1; u < nn; u++) {
            if (g[u].path == NULL) {
                continue;
            }
            for (j = 1; j <= g[u].path[0].n; j++) {
                Tint v = g[u].path[j].n;
                if (i > n && over[u][v])
                    continue;
                Tint w;
                if (MINF != dist[u])
                    w = dist[u] + g[u].path[j].c;
                else
                    w = dist[u];
                if (Relax(dist, u, v, w, i))
                    cont = true;
            }
        }
        if (!cont)
            break;
    }
    return relax;
}

Graph * g, * r;

int main(void) {
    Tint a, b, c = 1, s;
    Tint register i;
    Tint register j;
    Tint u, v;
    Tint ccar[MAXSIZE];
    Tint * dist;
    bool loop = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    cin >> s;
    if (0 == m) {
        for (i = 1; i < nn; ++i) {
            if (s == i)
                cout << "0" << endl;
            else
                cout << "*" << endl;
        }
        return 0;
    }
    g = new Graph[nn + 2];
    r = new Graph[nn + 2];
    dist = new Tint[nn + 2];
    relx = new Tint[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    memset(r, 0, sizeof(Graph) * (nn + 2));
    memset(relx, 0, sizeof(Tint) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
        set_path(r, b, a, c);
    }
    for (i = 0; i < nn; i++) {
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
       if (r[i].path != NULL)
            qsort1(r[i].path, 1, r[i].path[0].n);
    }
    BellmanFord(g, dist, s);
    /* TODO */
    used.assign(nn, false);
    for (i = 1; i < nn; ++i)
        if (!used[i])
            dfs1(g, i);
    used.assign(nn, false);
    for (i = 1; i < nn; ++i) {
        Tint v = order[n - i];
        if (!used[v]) {
            dfs2(r, v);
            vector<Tint>::iterator I;
            for(I = component.begin(); I != component.end(); I++) {
                ccar[*I] = ccnum;
                if (relx[*I]) {
                    if (NULL == cc[ccnum])
                        cc[ccnum] = new Cc;
                    cc[ccnum]->n = ccnum;
                    cc[ccnum]->i = true;
                    cc[ccnum]->v = *I;
                }
            }
            ccnum++;
            component.clear();
        }
    }
    /* TODO */
    // cout << ccnum - 1 << endl;
#if DEBUG > 2
    for (i = 0; i < nn; i++)
        cerr << "relx[" << i << "]=" << relx[i] << endl;
#endif
#if DEBUG > 0
    for (j = 1; j < nn; ++j) {
        if (NULL != cc[j]) {
            cerr << "cc[" << j << "]->n=" << cc[j]->n << " ";
            cerr << "cc[" << j << "]->i=" << cc[j]->i << " ";
            cerr << "cc[" << j << "]->v=" << cc[j]->v << endl;
        }
    }
#endif
    for (i = 1; i < nn; ++i) {
        if (s == i) {
          loop = false;
          if (NULL != g[s].path)
            for (j = 1; j <= g[s].path[0].n; ++j) {
              if (g[s].path[j].n == s && g[s].path[j].c < 0) {
                loop = true;
                break;
              }
            }
          if (loop)
            cout << "-" << endl;
          else {
            loop = false;
            for (j = 1; j < ccnum; ++j)
              if (NULL != cc[j]) {
                used.assign(nn, false);
                loop = (dfs3(g, cc[j]->v, i) || loop);
#if DEBUG > 0
                cerr << "cc[" << j << "]->n=" << cc[j]->n << " ";
                cerr << "cc[" << j << "]->i=" << cc[j]->i << " ";
                cerr << "cc[" << j << "]->v=" << cc[j]->v << endl;
                cerr << "loop=" << loop << endl;
#endif
            }
            if (loop)
              cout << "-" << endl;
            else
              cout << "0" << endl;
          }
        }
        else if (MINF == dist[i])
          cout << "*" << endl;
        else if (0 != relx[i])
          cout << "-" << endl;
        else {
          loop = false;
          for (j = 1; j < ccnum; ++j)
            if (NULL != cc[j]) {
                used.assign(nn, false);
                loop = (dfs3(g, cc[j]->v, i) || loop);
#if DEBUG > 0
                cerr << "cc[" << j << "]->n=" << cc[j]->n << " ";
                cerr << "cc[" << j << "]->i=" << cc[j]->i << " ";
                cerr << "cc[" << j << "]->v=" << cc[j]->v << endl;
                cerr << "loop=" << loop << endl;
#endif
            }
          if (loop)
            cout << "-" << endl;
          else
            cout << dist[i] << endl;
        }
/*
        for (j = 1; j < ccnum; ++j)
            k = lower_bound(ccar, 1, ccar + n, j);
                if (explore(k, j))
                    cout << "-" << endl;
*/
    }
    // for (i = 0; i < nn; i++)
    //     if (g[i].path != NULL)
    //         delete [] g[i].path;
    // delete [] g;
    return 0;
}

void cerr_graph(Graph * g, const char * func, const char * var) {
    int register i;
    int register j;
    for (i = 1; i < nn; i++) {
        cerr << func << ": " << var << "[" << i << "].node=" << g[i].node << endl;
        cerr << func << ": " << var << "[" << i << "].visited=" << g[i].visited << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, i, g[i].path);
#endif
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0].n; j++) {
                cerr << func << ": " << var << "[" << i << "].path[" << j << "]=" << g[i].path[j].c << endl;
            }
        }
        cerr << endl;
    }
}
