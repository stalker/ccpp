//
// Задача на программирование. Дан взвешенный ориентированный граф на n
// вершинах и m рёбрах (1≤n≤1000, 0≤m≤10000). Вес ребра — целое число,
// по модулю не превышающее 1000. Выведите 1, если в графе есть цикл
// отрицательного веса, и 0 в противном случае.
//
// Сроки сдачи задания:
//
//Soft дедлайн 03.11.2014 23:59 MSK
//
//Hard дедлайн 10.11.2014 23:59 MSK
//
// Sample Input:
// 4 4
// 1 2 1
// 4 1 2
// 2 3 2
// 3 1 -5
// Sample Output:
// 1
// Memory Limit: 256 MB
// Time Limit: 10 seconds
// Version 0.5
//
#include <stack>
#include <queue>
#include <cstdio>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>

#define DEBUG 1

// #define USE_BINSEARCH 1
#define PATHSTART 128
#define MAX_VERT 1001
#define MINF LLONG_MAX/10000

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl

#define cerr_X_V(x,v) cerr<<#x<<(v)
#define cerr_X_V_nl(x,v) cerr_X_V(x,v)<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':';cerr_X_V(x,v)
#define cerr_func_X_V_nl(x,v) cerr_func_X_V(x,v)<<endl

#define cerr_X_V_Y(x,v,y) cerr<<#x<<(v)<<#y
#define cerr_X_V_Y_nl(x,v,y) cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)<<endl

#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

using namespace std;

typedef long long Tint;
typedef unsigned long long Tuint;

Tint m, n, nn, ssss;

typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;

typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
    Tint visited;
} Graph;

Tint * relx;
bool over[MAX_VERT][MAX_VERT];
Tint cost[MAX_VERT][MAX_VERT];

void cerr_graph(Graph * g, const char * func, const char * var);

Tint binsearch(Tint x, Path v[], Tint n) {
    Tint low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

void paths_swap(Path * p, Tint a, Tint b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}

void qsort1(Path * paths, Tint l, Tint u) {
    Tint i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}

inline bool get_path(Graph * g, Tint a, Tint b) {
    if (g[a].path != NULL)
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    return false;
}

Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        // if (get_path(g, a, b))
        //    return -1;
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}

bool Relax(Tint * dist, Tint * prev, Tint u, Tint v, Tint w, Tint i) {
    bool relax = false;
    if (dist[v] > w) {
        dist[v] = w;
        prev[v] = u;
        relax = true;
        if (i > n) {
            relx[v] += 1;
            over[u][v] = true;
        }
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V(if (dist[v] > w) u=, u);
        cerr_X_V(; v=, v);
        cerr_X_V_nl(; w=, w);
#endif
    }
    return relax;
}

bool BellmanFord(Graph * g, Tint * dist, Tint * prev, Tint s) {
    Tint register i;
    Tint register j;
    Tint register u;
    bool cont = false;
    bool relax = false;
    Path p;

    for (i = 1; i < nn; i++) {
        if (i != s) {
            p.c = dist[i] = MINF;
            prev[i] = -1;
        } else {
            p.c = dist[i] = 0;
            prev[i] = i;
        }
        p.n = i;
    }
    for (i = 1; i < nn*nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_nl(START  i=, i);
        if (i > n)
            cont = false;
        for (u = 1; u < nn; u++) {
            cerr_FILE_LINE;
            cerr_func_X_V_nl(u=, u);
            cerr_FILE_LINE;
            cerr_func_X_V_Y(dist[, u,]);
            cerr_X_V_nl(=, dist[u]);
            if (g[u].path == NULL) {
                cerr_FILE_LINE;
                cerr_func_X_nl(g[u].path == NULL);
                continue;
            }
            for (j = 1; j <= g[u].path[0].n; j++) {
                Tint v = g[u].path[j].n;
                if (i > n && over[u][v]) {
                    cerr_FILE_LINE;
                    cerr_func_X_nl(g[u].path == NULL);
                    continue;
                }
                Tint w;
                if (MINF != dist[u])
                    w = dist[u] + g[u].path[j].c;
                else
                    w = dist[u];
                cerr_FILE_LINE;
                cerr_func_X_V(j=, j);
                cerr_X_V(; w=, w);
                cerr_X_V_nl(; v=, v);
                cerr_FILE_LINE;
                cerr_func_X_V_Y(dist[, v,]);
                cerr_X_V_nl(=, dist[v]);
                // if ( u != v)
                //    relax = (Relax(dist, prev, u, v, w, i) or relax);
                // relax = (Relax(dist, prev, u, v, w, i) || relax);
                if (Relax(dist, prev, u, v, w, i))
                    cont = true;
                cerr_FILE_LINE;
                cerr_func_X_V_Y(dist[, v,]);
                cerr_X_V_nl(=, dist[v]);
                cerr_FILE_LINE;
                cerr_func_X_V(; g[u].path[j].n=, g[u].path[j].n);
                cerr_X_V_nl(; g[u].path[j].c=, g[u].path[j].c);
                cerr_FILE_LINE;
                cerr_func_X_V_nl(   relax=, relax);
            }
        }
#ifdef DEBUG
        if ( i > n ) {
            for (j = 1; j < nn; j++) {
                cerr_FILE_LINE;
                cerr_func_X_V_Y(dist[, j,]);
                cerr_X_V_nl(=, dist[j]);
            }
            for (j = 1; j < nn; j++) {
                cerr_FILE_LINE;
                cerr_func_X_V_Y(prev[, j,]);
                cerr_X_V_nl(=, prev[j]);
            }
        }
#endif
        cerr_FILE_LINE;
        cerr_func_X_V_nl(FINISH i=, i);
        cerr << endl;
        if (!cont) {
            break;
        }
    }
    return relax;
}

bool inf_loop = false;

inline void set_visited(Graph * g, int v) {
    g[v].visited = ssss;
    if (relx[v])
        inf_loop = true;
}
inline Tint get_visited(Graph * g, int v) {
    return g[v].visited;
}

bool zero_visited(Graph * g, int i = 1) {
    bool found = false;
    Tint register j;
    for (j = i; j < nn; j++) {
        if (g[j].visited != ssss) {
            found = true;
            break;
        }
    }
    return found;
}

Tint explore(Graph * g, Tint v, Tint target, stack <Tint> & st) {
    Tint result = 0;
    Tint register i;
#if DEBUG > 0
    cerr << "explore: v=" << v << ", target=" << target << endl;
#endif
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
        return 1;
    }
    int k;
    if (g[v].path != NULL && (k = g[v].path[0].n) > 0)
        for (i = 1; i <= k; i++) {
            Tint l = g[v].path[i].n;
            if (get_visited(g, l) != ssss) {
                if((result = explore(g, l, target, st)) == 1) {
                    st.push(v);
                    return 1;
                }
            }
        }
    st.push(v);
    return result;
}

bool Loop_Detect(Graph * g, Tint s, Tint target) {
    inf_loop = false;
    ssss = target;
    stack <Tint> stg;
    explore(g, s, nn + 1, stg);
    return get_visited(g, target) && inf_loop;
}

int main(void) {
    Tint register i;
    Tint register j;
    Tint a, b, c;
    Tint u, v, s;
    Tint * dist;
    Tint * prev;
    Graph * g;
    bool relax = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    cin >> s; ssss = s;
    if (0 == m) {
        for (i = 0; i < nn; ++i) {
            if (s == i)
                cout << "0" << endl;
            else
                cout << "*" << endl;
        }
        return 0;
    }
    g = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    dist = new Tint[nn + 2];
    prev = new Tint[nn + 2];
    relx = new Tint[nn + 2];
    memset(relx, 0, sizeof(Tint) * (nn + 2));
    for (i = 1; i < nn; ++i)
        for (j = 1; j < nn; ++j)
            if (i == j)
                cost[i][j] = 0;
            else
                cost[j][i] = MINF;

    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        set_path(g, a, b, c);
    }
    cerr_graph(g, __func__, "g");
    cerr_FILE_LINE;
    cerr_func_X_V_nl(sizeof Path=, g[1].path[0].n * sizeof(Path));
    for (i = 0; i < nn; i++)
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
#ifdef DEBUG
    cerr_graph(g, __func__, "g");
#endif
    relax = BellmanFord(g, dist, prev, s);
#ifdef DEBUG
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(dist[, i,]);
        cerr_X_V_nl(=, dist[i]);
    }
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(prev[, i,]);
        cerr_X_V_nl(=, prev[i]);
    }
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(relx[, i,]);
        cerr_X_V_nl(=, relx[i]);
    }
#endif
    for (i = 1; i < nn; i++) {
        if (MINF == dist[i])
          cout << "*" << endl;
        else if (0 != relx[i])
          cout << "-" << endl;
        else if (Loop_Detect(g, s, i))
          cout << "-" << endl;
        else
          cout << dist[i] << endl;
    }
    // else if (dist[i] < 0)
    //   cout << "-" << endl;

    return 0;
}

void cerr_graph(Graph * g, const char * func, const char * var) {
    Tint register i;
    Tint register j;
    for (i = 1; i < nn; i++) {
        cerr_FILE_LINE;
        cerr << ':' << func << ": " << var << "[" << i << "].node=" << g[i].node << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, i, g[i].path);
#endif
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0].n; j++) {
                cerr_FILE_LINE;
                cerr << ':'<< func << ": " << var << "[" << i << "].path[" << j << "].n=" << g[i].path[j].n;
                cerr << ": " << var << "[" << i << "].path[" << j << "].c=" << g[i].path[j].c << endl;
            }
        }
        cerr << endl;
    }
}
