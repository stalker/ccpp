#include <iostream>
// для структуры pair:
#include <utility>
#include <vector>
#include <deque>
// для значения INT_MAX:
#include <climits>
// для функции reverse:
#include <algorithm>
 
using namespace std;
 
typedef pair<int,int> rib;
typedef vector < vector<rib> > graph;
 
const int INFINITY = INT_MAX;
 
// Значения по умолчанию для работы алгоритма (число вершин графа, индексы начальной и конечной вершин пути)
int defaultNumber = 10, 
    defaultStart = 1, 
    defaultFinish = 3;
 
int main()
{
    int numberOfVertices = defaultNumber,  
        startVertex = defaultStart, 
	finishVertex = 4, m;
 
    cin>>numberOfVertices>>m>>startVertex;
    graph g (numberOfVertices);
    for (int i=0; i<numberOfVertices; ++i)
    {
        int a, b, c;
        cin>>a>>b>>c;
        rib r(b,c);
        g[a].push_back(r);
    }
 
	// Здесь считываем структуру графа (откуда-либо, например, из файла).
	// К слову, размерность и номера вершин для поиска скорее всего
	// необходимо получать из того же источника.
 
    vector<int> d (numberOfVertices, INFINITY);
    d[startVertex] = 0;
 
    vector<int> state (numberOfVertices, 2);
    state[startVertex] = 1;
 
    deque<int> q;
    q.push_back (startVertex);
 
    vector<int> p (numberOfVertices, -1);
 
    while (!q.empty())
    {
        int vertex = q.front();  
        q.pop_front();
        state[vertex] = 0;
        for (size_t i = 0; i < g[vertex].size(); ++i)
        {
            int to = g[vertex][i].first, 
                length = g[vertex][i].second;
            if (d[to] > d[vertex] + length)
            {
                d[to] = d[vertex] + length;
                if (state[to] == 2)
                    q.push_back (to);
                else if (state[to] == 0)
                    q.push_front (to);
                p[to] = vertex;
                state[to] = 1;
            }
        }
    }
    if (p[finishVertex] == -1)
    {
        cout << "No solution" << endl;
    }
    else
    {
        vector<int> path;
        for (int vertex = finishVertex; vertex != -1; vertex = p[vertex])
            path.push_back (vertex);
        reverse (path.begin(), path.end());
        for (size_t i = 0; i < path.size(); ++i)
            cout << path[i] + 1 << ' ';
    }
 
    // для запуска не из командной строки (чтобы была возможность увидеть результат)
    cin.get();
    return 0;
}
