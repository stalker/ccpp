/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

/*
 * На доске написано n чисел. С ними n−1 раз повторяется следующая простая
 * операция: стираются текущий минимум и текущий максимум, выписывается их
 * полусумма. Ваша задача — определить, какое число останется в конце.
 *
 * Во входе дано целое число 1 ≤ n ≤ 10⁵ и начальные целые числа
 * −10⁵ ≤ a₁, …, aₙ ≤ 10⁵. Выведите итоговое число с точностью 10⁻⁶.
 */
#include <cstring>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

#include <heap.hpp>

#define DEBUG 1

const char * sp = " ";

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setprecision(6)

using std::string;
using std::vector;
using namespace std;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n; /*
    vector<double> vd;
    for (int i = 0; i < n; ++i) {
        double d;
        cin >> d;
        vd.push_back(d);
    }
    heap<double> hpmax(vd);
    hpmax.build_heap();
    heap<double> hpmin(vd, MinHeap);
    hpmin.build_heap(); */
    heap<double> hpmax;
    heap<double> hpmin(MinHeap);
    for (int i = 0; i < n; ++i) {
        double d;
        cin >> d;
        hpmax.add(d);
        hpmin.add(d);
    } 
    if (1 == n) {
        cout << FF_11_6 << hpmax.extract_top() << endl;
        return EXIT_SUCCESS;
    }
    hpmax.print(cout) << endl;
    hpmin.print(cout) << endl;
    double half_sum;
    for (int i = 1; i < n; ++i) {
        double max = hpmax.extract_top();
        double min = hpmin.extract_top();
        half_sum = (max + min) / 2;
        if (half_sum < min) min = half_sum;
        hpmax.add(half_sum);
        hpmin.add(half_sum);
    }
    // cout << FF_11_6 << hpmin.first() << endl;
    // cout << FF_11_6 << hpmax.first() << endl;
    cout << FF_11_6 << half_sum << endl;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
