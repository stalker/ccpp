/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 2$
 */

#ifndef _HEAP_HPP_
#define _HEAP_HPP_

#include <cstring>
#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <algorithm>

#define DEBUG 1

static const char * __sp = " ";

// std::string operator ""_s(const char * str, std::size_t len) {
//     return std::string(str, str + len);
// }

enum heap_type { MaxHeap, MinHeap };

inline size_t parent(size_t idx) { return idx / 2;   }
inline size_t left(size_t idx)   { return 2*idx;     }
inline size_t right(size_t idx)  { return 2*idx + 1; }

template <class T>
bool min_cmp(const std::vector<T> & arr, size_t left_idx, size_t right_idx) {
    return arr[right_idx] < arr[left_idx];
}

template <class T>
bool max_cmp(const std::vector<T> & arr, size_t left_idx, size_t right_idx) {
    return arr[left_idx] < arr[right_idx];
}

template <class T> class heap {
public:
    heap(heap_type heaptype = MaxHeap)
    : _ht(heaptype), len(0), _data(1) {
        if (_ht == MaxHeap) {
            this->cmp = &max_cmp;
        } else 
            this->cmp = &min_cmp;
    }
    typedef typename std::vector<T>::iterator itr_vec;
    heap(const std::vector<T> v, heap_type heaptype = MaxHeap) {
        T e;
        this->_data.push_back(e);
        for (itr_vec i = v.begin();
             i != v.end(); ++i
        ) {
            this->_data.push_back(*i);
        }
        this->_ht = heaptype;
        if (this->_ht == MaxHeap) {
            this->cmp = &max_cmp;
        } else {
            this->cmp = &min_cmp;
        }
        this->len = this->_data.size() - 1;
    }
    // typedef bool (*)(const std::vector<T> &, size_t , size_t) compataror;
    heap(bool (*acmp)(const std::vector<T> &, size_t, size_t), heap_type ht)
    : _ht(ht), len(0), _data(1), cmp(acmp) { /* None */ }
    const T& operator[](size_t idx) const {
        if (1 > idx || idx > this->len) {
            std::string errorMessage = std::string("list index out of range! in ")
                                       + __func__;
            throw std::out_of_range(errorMessage);
        }
        // throw
        // std::out_of_range(" list index out of range! in '"_s+__func__+"'"_s);
        return this->_data[idx];
    }
    // T& operator[](size_t index) {
    //     return this->_data[index];
    // }
    size_t size(void) const { return this->len; }
    void swap(size_t idx1, size_t idx2) {
        T t; t = this->_data[idx1];
        this->_data[idx1] = this->_data[idx2];
        this->_data[idx2] = t;
    }
    void set_heap_type(heap_type heaptype = MaxHeap) {
        this->_ht = heaptype;
        if (this->_ht == MaxHeap) {
            this->cmp = &max_cmp;
        } else {
            this->cmp = &min_cmp;
        }
        build_heap();
    }
    heap_type get_heap_type() const {
        return this->_ht;
    }
    size_t sift_up(size_t idx) {
        if (idx > this->len) return 0;
        // default MaxHeap cmp: _data[parent(idx)] < _data[idx]
        while (idx > 1 && cmp(this->_data, parent(idx), idx)) {
            swap(idx, parent(idx));
            idx = parent(idx);
        }
        return idx;
    }
    size_t add(T value) {
        this->_data.push_back(value);
        ++this->len;
        // TODO check len < _data.size()
        return sift_up(this->len);
    }
    // TODO need test
    size_t remove(size_t idx) {
        if (2 > idx || idx > this->len) return 0;
        // this->_data[idx] = this->_data[1] + 1; ???
        this->_data[idx] = this->_data[1];
        sift_up(idx);
        extract_top();
        /* this->_data[1] = this->_data[this->len];
        this->_data.pop_back();
        // TODO check len  < _data.size()
        --this->len;
        if (this->len > 1) {
            return sift_down(1);
        } */
        return 1;
    }
    T top(void) const {
        if (this->len > 0) {
            T t = this->_data[1];
            return t;
        }
        // TODO assert or exception
        std::cerr << "Error: in top! Empty heap." << std::endl;
        return this->_data[0];
    }
    T extract_top() {
        T top;
        if (this->len == 0) {
            // TODO assert
            return top;
        }
        top = this->_data[1];
        this->_data[1] = this->_data[this->len];
        this->_data.pop_back();
        // TODO check len  < _data.size()
        --this->len;
        if (this->len > 0) heapify(1);
        return top;
    }
    T get_opposite() const {
        size_t start = this->len / 2;
        size_t end   = this->len + 1;
        if (this->len <= 1) return this->_data[1];
        T opposite = this->_data[1];
        for (size_t idx = start; idx < end; ++idx) {
            if (this->_ht == MaxHeap) {
              if (this->_data[idx] < opposite)
                  opposite = this->_data[idx];
            } else if (opposite < this->_data[idx])
                  opposite = this->_data[idx];
        }
        return opposite;
    }
    // other heapify no recursive
    size_t sift_down(size_t idx) {
        bool finish = false;
        while (left(idx) <= this->len && !finish) {
            size_t cur_left = left(idx);
            size_t cur_right = right(idx);
            size_t extremum = idx;
            // default MaxHeap cmp: _data[temp] < _data[left(idx)]
            if (cmp(this->_data, extremum, cur_left)) {
                extremum = cur_left;
            }
            // default MaxHeap cmp: _data[temp] < _data[right(idx)]
            if (cur_right <= this->len
                && cmp(this->_data, extremum, cur_right)) {
                extremum = cur_right;
            }
            if (idx == extremum) {
                finish = true;
            } else {
                swap(idx, extremum);
            }
            idx = extremum;
        }
        return idx;
    }
    void heapify_recursive(size_t idx) {
        size_t cur_left  = left(idx);
        size_t cur_right = right(idx);
        size_t extremum  = idx;

        // default MaxHeap cmp: _data[extremum] < _data[cur_left]
        if (cur_left  <= this->len && cmp(this->_data, extremum, cur_left))
            extremum = cur_left;
        // default MaxHeap cmp: _data[extremum] < _data[cur_right]
        if (cur_right <= this->len && cmp(this->_data, extremum, cur_right))
            extremum = cur_right;
        if (extremum != idx) {
            swap(idx, extremum);
            heapify_recursive(extremum);
        }
    }
    void heapify(size_t idx) {
        heapify_recursive(idx);
    }
    void build_heap() {
        for (size_t idx = this->len / 2 ; idx > 0; --idx) {
            heapify(idx);
        }
    }
    std::ostream& print(std::ostream& stm, const char * sp = __sp) const {
        size_t idx = 1; for (; idx < this->len; ++idx) {
            stm << this->_data[idx] << sp;
        }
        if (this->len > 0) {
            stm << this->_data[idx];
        }
        return stm;
    }
private:
    heap_type _ht;
    size_t len;
    std::vector<T> _data; // Массив кучи
    bool (*cmp)(const std::vector<T> & v, size_t left_idx, size_t right_idx);
};

#endif
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
