#include <cstring>
#include <cstdlib>
#include <iostream>

using namespace std;
const int MAXN = 1000000;
int p[MAXN], rank[MAXN];

void make_set(int x) {
    p[x] = x;
    rank[x] = 0;
}

int find(int x) {
    return ( x == p[x] ? x : p[x] = find(p[x]) );
}

void Union(int x, int y) {
    if ( (x = find(x)) == (y = find(y)) ) return;
    if ( rank[x] <  rank[y] ) p[x] = y;
    else p[y] = x;
    if ( rank[x] == rank[y] ) ++rank[x];
}

int main(void)
{
    int i, m, n;
    char line[255];
    char * stopstring, *p;
    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);
    for (i = 0; i <= n; i++)
        make_set(i);
    size_t a, b;
    for (i = 0; i < m;) {
        cin.getline(line, 255);
        if (strncmp("Check", line, 5) == 0) {
            i++;
            a = strtol(&line[5], &stopstring, 10);
            p = stopstring;
            b = strtol(p, &stopstring, 10);
            if (find(a) == find(b))
                cout << "True" << endl;
            else
                cout << "False" << endl;
        } else if (strncmp("Union", line, 5) == 0) {
            i++;
            a = strtol(&line[5], &stopstring, 10);
            p = stopstring;
            b = strtol(p, &stopstring, 10);
            Union(a, b);
        }
    }
    return 0;
}
