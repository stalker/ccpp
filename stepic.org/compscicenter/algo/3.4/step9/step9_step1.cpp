#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

// #define DEBUG 2
using namespace std;


const int MAXN = 1000000;

int p[MAXN], rank[MAXN];

void make_set(int x)
{
    p[x] = x;
    rank[x] = 0;
}

int find(int x)
{
    return ( x == p[x] ? x : p[x] = find(p[x]) );
}

void Union(int x, int y)
{
    if ( (x = find(x)) == (y = find(y)) ) {
#if DEBUG > 1
        fprintf(stderr, "Union: (x = find(x)) == (y = find(y))\n");
        fprintf(stderr, "Union: (%d = find(%d)) == (%d = find(%d))\n", x, x, y, y);
#endif
        return;
    }
    if ( rank[x] <  rank[y] ) {
        p[x] = y;
#if DEBUG > 1
        fprintf(stderr, "Union: p[%d] = %d\n", x, y);
#endif
    } else {
        p[y] = x;
#if DEBUG > 1
        fprintf(stderr, "Union: p[%d] = %d\n", y, x);
#endif
    }
    if ( rank[x] == rank[y] ) {
        ++rank[x];
#if DEBUG > 1
        fprintf(stderr, "Union: ++rank[%d] = %d\n", x, rank[x]);
#endif
    }
}

int main(void)
{
    int i, m, n;
    char line[255];
    char * stopstring, *p;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);

    // memset(p, n, (sizeof(int) * MAXN - sizeof(int) * n) - 1);
    // memset(rank, n, sizeof(int) * MAXN - sizeof(int) * n - 1);
    for (i = 0; i <= n; i++)
        make_set(i);

#if DEBUG > 2
    fprintf(stderr, "n = %d, m = %d\n", n, m);
#endif

    size_t a, b;
    for (i = 0; i < m;) {
        cin.getline(line, 255);
#if DEBUG > 2
        fprintf(stderr, "i = %d, getline = %s\n", i, line);
#endif
        if (strncmp("Check", line, 5) == 0) {
            i++;
            a = strtol(&line[5], &stopstring, 10);
            p = stopstring;
            b = strtol(p, &stopstring, 10);
#if DEBUG > 0
            fprintf(stderr, "Input: Check a = %d b = %d\n", a, b);
            fprintf(stderr, "find(%d) = %d\n", a, find(a));
            fprintf(stderr, "find(%d) = %d\n", b, find(b));
            fprintf(stderr, "find(%d) == find(%d)\n", a, b, find(a) == find(b));
#endif
            if (find(a) == find(b)) {
                cout << "True" << endl;
            } else {
                cout << "False" << endl;
            }
        } else if (strncmp("Union", line, 5) == 0) {
            i++;
            a = strtol(&line[5], &stopstring, 10);
            p = stopstring;
            b = strtol(p, &stopstring, 10);
#if DEBUG > 0
            fprintf(stderr, "Input: Union a = %d b = %d\n", a, b);
#endif
            Union(a, b);
        }
    }

/*
    int i, k, m, n;
    char line[255];
    char * stopstring, *p;

    n = (sizeof a)/(sizeof a[0]);
    printf("n = %d, log2(%d) + 1 = %d\n", n, n, int_log2(n) + 1);

    intervalTree<int> tree;
    tree.build(a, n);

    for (i = 0; i < tree.treesize; i++)
        cout << "tree[" << i << "] = " << tree[i] << endl;

    m = 1;
    n = 8;
    cout << "min(" << m << "," << n << ") = " << tree.find_m(m, n) << endl;

    tree.update(8, 1);

    m = 1;
    n = 8;
    cout << "min(" << m << "," << n << ") = " << tree.find_m(m, n) << endl;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    p = stopstring;
    m = strtol(p, &stopstring, 10);
    k = int_log2(n) + 1;
    printf("n = %d, log2(%d) + 1 = %d\n", n, n, int_log2(n) + 1);
*/
    return 0;
}
