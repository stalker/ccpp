int findOrderStatistic(int[] array, int k) {
  int left = 0, right = array.length;
  while (true) {
    int mid = partition(array, left, right);

    if (mid == k) {
      return array[mid];
    }
    else if (k < mid) {
      right = mid;
    }
    else {
      k -= mid + 1;
      left = mid + 1;
    }
  }
}
