#include <iostream>

using namespace std;

#define DEBUG 1

int a[] = {2, 7, 8, 6, 0, 4, 1, 9, 3, 5};
int size = (sizeof a)/(sizeof a[0]) - 1;

int search(int a[], int k, int l, int r) {
    int s, m, i, j, tmp;
#ifdef DEBUG
        int z;
        for (z = 0; z < size - 1; z++)
            cout << a[z] << ", ";
        cout << a[z] << endl;
#endif
    if ( l == r )
        return a[r];
    else {
        i = l;
        j = r;
        s = (l + r) / 2;
        m = a[s];
        while ( i < j ) {
            while (a[i] < m)
                i++;
            while (a[j] > m)
                j--;
            if (i < j) {
                tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
                i++;
                j--;
            }
        }
        if (k < j)
            return search(a, k, l, j);
        else
            return search(a, k, i, r);
    }
}


int main(void) {
    cout << search(a, 9, 0, size) << endl;
    return 0;
}
