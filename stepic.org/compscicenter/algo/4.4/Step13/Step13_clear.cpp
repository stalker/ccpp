#include <iostream>
#include <algorithm>

using namespace std;

typedef struct line {
    int a;
    int b;
} Lines;

bool in_line(Lines & l, int point) {
    return (l.a <= point && point <= l.b);
}

int m, n;

int count_point1(Lines * l, int point) {
    int j, r = 0;

    for (j = n - 1; j >= 0; j--)
        if (point > l[j].b) {
            return r;
        } else if (in_line(l[j], point))
            r++;
    return r;
}

void lines_swap(Lines * l, int a, int b) {
    Lines t = l[a];
    l[a] = l[b];
    l[b] = t;
}

void qsort1(Lines * lines, int l, int u)
{
    int i, m;

    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (lines[i].b < lines[l].b) {
            ++m;
            lines_swap(lines, m, i);
        }
    lines_swap(lines, l, m);
    qsort1(lines, l, m - 1);
    qsort1(lines, m + 1, u);
}

int main() {
    int i, j, r;

    cin >> n;
    cin >> m;

    int * p = new int[m];
    Lines * l = new Lines[n];

    for (i = 0; i < n; i++) {
        cin >> l[i].a;
        cin >> l[i].b;
    }
    for (i = 0; i < m; i++)
        cin >> p[i];
    qsort1(l, 0, n - 1);
    for (i = 0; i < m - 1; i++) {
        r = count_point1(l, p[i]);
        cout << r << " ";
    }
    r = count_point1(l, p[i]);
    cout << r << endl;
    delete [] l;
    delete [] p;
    return 0;
}
