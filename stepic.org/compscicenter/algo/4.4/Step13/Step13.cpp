#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

#define DEBUG 1
/* локальный тип данных */
typedef struct line {
    int a;
    int b;
} Lines;

bool in_line(Lines & l, int point) {
    return (l.a <= point && point <= l.b);
}

int m, n;

int count_point1(Lines * l, int point) {
    int j, r = 0;

    for (j = n - 1; j >= 0; j--)
        if (point > l[j].b) {
#if DEBUG > 1
            fprintf(stderr, "%d -> l[%d].b = %d\n", point, j, l[j].b);
#endif
            return r;
        } else if (in_line(l[j], point))
            r++;
    return r;
}

void myqsort(Lines * l, int left, int right)
{
    int i, last;

    if (left >= right)
        swap(l[left], l[(left + right)/2]);

    last = left;

    for (i = left + 1; i <= right; i++)
        if (l[i].b < l[left].b)
            swap(l[++last], l[i]);
    swap(l[left], l[last]);
    myqsort(l, left, last-1);
    myqsort(l, last+1, right);
}

void lines_swap(Lines * l, int a, int b) {
    Lines t = l[a];
    l[a] = l[b];
    l[b] = t;
}

void qsort1(Lines * lines, int l, int u)
{    int i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (lines[i].b < lines[l].b) {
            ++m;
            lines_swap(lines, m, i);
        }
    lines_swap(lines, l, m);
    qsort1(lines, l, m - 1);
    qsort1(lines, m + 1, u);
}

int main() {
    int i, j, r;

    cin >> n;
    cin >> m;

    int * p = new int[m];
    Lines * l = new Lines[n];

    for (i = 0; i < n; i++) {
        cin >> l[i].a;
        cin >> l[i].b;
    }
    for (i = 0; i < m; i++)
        cin >> p[i];
    qsort1(l, 0, n - 1);
#ifdef DEBUG
    for (i = 0; i < n; i++) {
        cout << "l[" << i << "].a = " << l[i].a << ";" \
             << "l[" << i << "].b = " << l[i].b << ";" \
        << endl;
    }
    for (i = 0; i < m; i++)
        cout << "p[" << i << "] = " << p[i] << endl;
#endif
    for (i = 0; i < m - 1; i++) {
        r = count_point1(l, p[i]);
        cout << r << " ";
        // cout << i << ":" << r << " ";
    }
    r = count_point1(l, p[i]);
    cout << r << endl;
    delete [] l;
    delete [] p;
    return 0;
}
