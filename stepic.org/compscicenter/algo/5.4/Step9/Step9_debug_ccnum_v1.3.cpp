// Step9.cpp
// Version 1.3
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n≤100000, 0≤m≤100000). Выведите количество компонент сильной связности в
// данном графе.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK
// Hard дедлайн 03.11.2014 23:59 MSK

// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 2
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// #include <ctime>

#include <stack>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define PATHSTART 256
#define MAXSIZE 100000

using namespace std;

typedef long long Tint;
typedef unsigned long long Tuint;

vector<bool> used;
vector<Tint> order, component;

Tint m, n, nn, ccnum = 1;

typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;

typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
    Tint visited;
} Graph;

#define DEBUG 1
void cerr_graph(Graph * g, const char * func, const char * var);

Tint binsearch(Tint x, Path v[], Tint n) {
    Tint low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

void paths_swap(Path * p, Tint a, Tint b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}

void qsort1(Path * paths, Tint l, Tint u) {
    Tint i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}

bool zero_visited(Graph * g, Tint i = 1) {
    bool found = false;
    Tint register j;
    for (j = i; j < nn; j++) {
        if (g[j].visited == 0) {
            found = true;
            break;
        }
    }
    return found;
}

inline bool get_path(Graph * g, Tint a, Tint b) {
    if (g[a].path != NULL)
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    return false;
}

inline void set_visited(Graph * g, Tint v) {
    g[v].visited = ccnum;
}

inline Tint get_visited(Graph * g, Tint v) {
    return g[v].visited;
}

Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        if (get_path(g, a, b))
            return -1;
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}

void dfs1 (Graph * g, Tint v) {
    used[v] = true;
    if (NULL != g[v].path)
        for (size_t i = 1; i <= g[v].path[0].n; ++i)
            if (!used[g[v].path[i].n])
                dfs1(g, g[v].path[i].n);
    order.push_back(v);
}

void dfs2 (Graph * g, int v) {
    used[v] = true;
    component.push_back(v);
    if (NULL != g[v].path)
        for (size_t i = 1; i <= g[v].path[0].n; ++i)
            if (!used[g[v].path[i].n])
                dfs2(g, g[v].path[i].n);
}

Graph * g, * r;

int main(void) {
    Tint a, b, c = 1;
    Tint register i;
    Tint register j;
    Tint u, v;
    Tint res[MAXSIZE];
    bool zero = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    if (m == 0) {
        cout << n << endl;
        return 0;
    }
    g = new Graph[nn + 2];
    r = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    memset(r, 0, sizeof(Graph) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        // cin >> c;
        set_path(g, a, b, c);
        set_path(r, b, a, c);
    }
    for (i = 0; i < nn; i++) {
       if (g[i].path != NULL)
            qsort1(g[i].path, 1, g[i].path[0].n);
       if (r[i].path != NULL)
            qsort1(r[i].path, 1, r[i].path[0].n);
    }

    used.assign(nn, false);
    for (i = 1; i < nn; ++i)
        if (!used[i])
            dfs1(g, i);
    used.assign(nn, false);
    for (i = 1; i < nn; ++i) {
        Tint v = order[n - i];
        if (!used[v]) {
            dfs2(r, v);
            vector<Tint>::iterator I;
            for(I = component.begin(); I != component.end(); I++) {
                cerr << "*I=" << *I << endl;
                res[*I] = ccnum;
            }
            ccnum++;
            component.clear();
        }
    }

    cout << ccnum - 1 << endl;
    // for (i = 0; i < nn; i++)
    //     if (g[i].path != NULL)
    //         delete [] g[i].path;
    // delete [] g;
    return 0;
}

void cerr_graph(Graph * g, const char * func, const char * var) {
    int register i;
    int register j;
    for (i = 1; i < nn; i++) {
        cerr << func << ": " << var << "[" << i << "].node=" << g[i].node << endl;
        cerr << func << ": " << var << "[" << i << "].visited=" << g[i].visited << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, i, g[i].path);
#endif
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0].n; j++) {
                cerr << func << ": " << var << "[" << i << "].path[" << j << "]=" << g[i].path[j].c << endl;
            }
        }
        cerr << endl;
    }
}
