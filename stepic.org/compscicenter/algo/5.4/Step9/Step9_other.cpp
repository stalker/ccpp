#include <iostream>
#include <vector>
#define MAXSIZE 100000
using namespace std;
 
vector<int> g[MAXSIZE], gr[MAXSIZE];
vector<bool> used;
vector<int> order, component;
 
void dfs1 (int v) {
    used[v] = true;
    for (size_t i = 0; i < g[v].size(); ++i)
        if (!used[g[v][i]])
            dfs1(g[v][i]);
    order.push_back(v);
}

void dfs2 (int v) {
    used[v] = true;
    component.push_back(v);
    for (size_t i = 0; i < gr[v].size(); ++i)
        if (!used[gr[v][i]])
            dfs2(gr[v][i]);
}

int main() {
  int n, m, i, a, b, res[MAXSIZE], ccnum=1;
    cin >> n >> m;
    for (i = 0; i < m; i++) {
        cin >> a >> b;
        g[a-1].push_back(b-1);
        gr[b-1].push_back(a-1);
    }
    used.assign(n, false);
    for (int i = 0; i < n; ++i)
        if (!used[i])
            dfs1(i);
    used.assign(n, false);
    for (int i = 0; i < n; ++i) {
        int v = order[n - 1 - i];
        if (!used[v]) {
            dfs2(v);
            vector<int>::iterator I;
            for(I = component.begin(); I != component.end(); I++)
                res[*I] = ccnum;
            ccnum++;
            component.clear();
        }
    }
    cout << ccnum - 1 << endl;
    return 0;
}
