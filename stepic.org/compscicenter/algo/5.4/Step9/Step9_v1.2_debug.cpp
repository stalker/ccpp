// Step9.cpp
// Version 1.2
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n≤100000, 0≤m≤100000). Выведите количество компонент сильной связности в
// данном графе.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK
// Hard дедлайн 03.11.2014 23:59 MSK

// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 2
// Memory Limit: 256 MB
// Time Limit: 5 seconds
// #include <ctime>
#include <stack>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define DEBUG 1
#define PATHSTART 256
using namespace std;

typedef long long Tint;
typedef unsigned long long Tuint;

Tint m, n, nn, ccnum = 0;

typedef struct path_ {
    Tint n; // node
    Tint c; // cost
} Path;

typedef struct graph {
    Tint node;
    Path * path;
    Tint path_lim;
    Tint visited;
    Tint indegree;
} Graph;

void cerr_node(Graph * g, Tint v, const char * func, const char * var);
void cerr_graph(Graph * g, const char * func, const char * var);

Tint binsearch(Tint x, Path v[], Tint n);
void qsort1(Path * paths, Tint l, Tint u);

bool zero_visited(Graph * g, Tint i = 1) {
    bool found = false;
    Tint register j;
    for (j = i; j < nn; j++) {
        if (g[j].visited == 0) {
            found = true;
            break;
        }
    }
    return found;
}

inline bool get_path(Graph * g, Tint a, Tint b) {
    if (g[a].path != NULL)
        return (binsearch(b, &g[a].path[1], g[a].path[0].n) != -1);
    return false;
}
inline void set_visited(Graph * g, Tint v) {
    g[v].visited = ccnum;
}
inline Tint get_visited(Graph * g, Tint v) {
    return g[v].visited;
}
Tint set_path(Graph * g, Tint a, Tint b, Tint c) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new Path[PATHSTART];
        g[a].path_lim = PATHSTART - 1;
        g[a].path[0].n = 1;
        g[a].path[1].n = b;
        g[a].path[1].c = c;
        return 0;
    } else {
        // if (get_path(g, a, b))
        //    return -1;
        Tint e = g[a].path[0].n + 1;
        if (e >= (g[a].path_lim - 1)) {
            Tint new_lim = g[a].path_lim * 2;
            Path * new_path = new Path[new_lim];
            memcpy(new_path, g[a].path, sizeof(Path) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e].n = b;
        g[a].path[e].c = c;
        g[a].path[0].n = e;
        return 1;
    }
}

Tint explore(Graph * g, Tint v, Tint target, stack <int> & st) {
    Tint result = 0;
    Tint register i;
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
        return 1;
    }
    Tint k;
    if (g[v].path != NULL && (k = g[v].path[0].n) > 0)
        for (i = 1; i <= k; i++) {
            Tint l = g[v].path[i].n;
            if (get_visited(g, l) == 0) {
                if((result = explore(g, l, target, st)) == 1) {
                    st.push(v);
                    return 1;
                }
            }
        }
    st.push(v);
    return result;
}
Tint dfs(Graph * g, Tint v, Tint target, stack <int> & st) {
    Tint result;
    Tint register i;
    Tint register j;
    ccnum=1;
    explore(g, 1, nn + 1, st);
    for (i = 2; i < nn; i++) {
        if (zero_visited(g, i)) {
            ccnum++;
            explore(g, i, nn + 1, st);
        }
    }
    return 0;
}
Graph * g, * r;
int main(void) {
    Tint a, b, c = 1;
    Tint register i;
    Tint register j;
    Tint u, v;
    bool zero = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    if (m == 0) {
        cout << n << endl;
        return 0;
    }
    g = new Graph[nn + 2];
    r = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    memset(r, 0, sizeof(Graph) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        // cin >> c;
        set_path(g, a, b, c);
        set_path(r, b, a, c);
    }
    stack <int> str;
    dfs(r, 1, nn + 1, str);
    ccnum=0;
    while (!(str.empty())) {
        stack <int> stg;
        Tint k = str.top();
        if (g[k].visited == 0) {
            ccnum++;
            explore(g, k, nn + 1, stg);
        }
        str.pop();
    }
    cout << ccnum << endl;
    // for (i = 0; i < nn; i++)
    //     if (g[i].path != NULL)
    //         delete [] g[i].path;
    // delete [] g;
    return 0;
}

Tint binsearch(Tint x, Path v[], Tint n) {
    Tint low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid].n)
            high = mid - 1;
        else if (x > v[mid].n)
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

void paths_swap(Path * p, Tint a, Tint b) {
    Path t = p[a];
    p[a] = p[b];
    p[b] = t;
}

void qsort1(Path * paths, Tint l, Tint u) {
    Tint i, m;
    if (l >= u)
        return;
    m = l;
    for (i = l + 1; i <= u; i++)
        if (paths[i].n < paths[l].n) {
            ++m;
            paths_swap(paths, m, i);
        }
    paths_swap(paths, l, m);
    qsort1(paths, l, m - 1);
    qsort1(paths, m + 1, u);
}
