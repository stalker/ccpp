// Step9.cpp
// Version 0.5
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n≤100000, 0≤m≤100000). Выведите количество компонент сильной связности в
// данном графе.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK 
// Hard дедлайн 03.11.2014 23:59 MSK

// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 2
// Memory Limit: 256 MB
// Time Limit: 5 seconds
#include <ctime>
#include <stack>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define DEBUG 1
#define PATHSTART 256
using namespace std;
int m, n, nn, ccnum = 0;
time_t Start, End;
double difTime;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
    int visited;
    int indegree;
} Graph;

bool zero_visited(Graph * g);
inline bool get_path(Graph * g, int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}
inline void set_visited(Graph * g, int v) {
    g[v].visited = ccnum;
}
inline int get_visited(Graph * g, int v) {
    return g[v].visited;
}
int set_path(Graph * g, int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[8];
        memset(g[a].path, 0, sizeof(int) * 8);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        if (get_path(g, a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            // memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int explore(Graph * g, int v, int target, stack <int> & st) {
    int result = 0;
    int register i;
#if DEBUG > 0
    time_t Start, End;
    double difTime;
    time(&Start);
#endif
#if DEBUG > 1
    cerr << "explore: v=" << v << ", target=" << target << endl;
#endif
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
#if DEBUG > 0
        time(&End);
        difTime = difftime(End, Start);
        cerr << "explore(" << v << ") i=" << i << ": " << difTime << endl;
#endif
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_visited(g, i) == 0 && get_path(g, v, i)) {
            if((result = explore(g, i, target, st)) == 1) {
                st.push(v);
#if DEBUG > 0
                time(&End);
                difTime = difftime(End, Start);
                cerr << "explore(" << v << ") i=" << i << ": " << difTime << endl;
#endif
                return 1;
            }
        }
    }
    st.push(v);
#if DEBUG > 0
    time(&End);
    difTime = difftime(End, Start);
    cerr << "explore(" << v << "): " << difTime << endl;
#endif
    return result;
}
int dfs(Graph * g, int v, int target, stack <int> & st) {
    int result;
    int register i;
    int register j;
#if DEBUG > 0
    time_t Start, End;
    double difTime;
    time(&Start);
#endif
#if DEBUG > 1
    cerr << "dfs: v=" << v << ", target=" << target << endl;
#endif
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_visited(g, i) == 0 && get_path(g, v, i)) {
            if((result = explore(g, i, target, st)) == 1) {
                st.push(v);
#if DEBUG > 0
                    time(&End);
                    difTime = difftime(End, Start);
                    cerr << "dfs(" << v <<"): i=" << i << " " << difTime << endl;
#endif
                return 1;
            }
        }
    }
//#if DEBUG > 0
//                    time(&End);
//                    difTime = difftime(End, Start);
//                    cerr << "dfs(): v=" << v << " " << difTime << endl;
//#endif
    st.push(v);
    return 0;
}
Graph * g, * r;
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool zero = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    if (m == 0) {
        cout << n << endl;
    }
#if DEBUG > 0
    time(&Start);
#endif
    g = new Graph[nn + 2];
    r = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    memset(r, 0, sizeof(Graph) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, a, b);
        set_path(r, b, a);
    }
#if DEBUG > 0
    time(&End);
    difTime = difftime(End, Start);
    cerr << "main: cin: " << difTime << endl;
#endif
#if DEBUG > 1
    cerr_graph(g, "main", "r");
#endif
    ccnum=1;
#if DEBUG > 0
    time(&Start);
#endif
    stack <int> str;
    dfs(r, 1, nn + 1, str);
    for (i = 2; i < nn; i++) {
        zero = true;
        zero = zero_visited(r);
        int visited = get_visited(r, i);
#if DEBUG > 1
        cerr << "main: i=" << i;
        cerr << " zero=" << zero;
        cerr << " visited=" << visited;
        cerr << endl;
#endif
        if (visited == 0 && zero) {
            ccnum++;
            dfs(r, i, nn + 1, str);
        } else if(!zero)
          break;
    }
#if DEBUG > 0
    time(&End);
    difTime = difftime(End, Start);
    cerr << "main: dfs(r): " << difTime << endl;
    time(&Start);
#endif
#if DEBUG > 1
    cerr_graph(r, "main", "r");
    cerr << "stack , topology(r): ";
#endif
    ccnum=0;
    while (!(str.empty())) {
        stack <int> stg;
        int k = str.top();
        if (get_visited(g, k) == 0 && zero_visited(g)) {
            ccnum++;
            dfs(g, k, nn + 1, stg);
        }
        str.pop();
    }
#if DEBUG > 1
    cerr << endl << endl;
#endif

#if DEBUG > 0
    time(&End);
    difTime = difftime(End, Start);
    cerr << "main: dfs(g): " << difTime << endl;
#endif
#if DEBUG > 1
    cerr_graph(g, "main", "g");
#endif
    cout << ccnum << endl;
    for (i = 0; i < nn; i++)
        if (g[i].path != NULL)
            delete [] g[i].path;
    delete [] g;
    return 0;
}
bool zero_visited(Graph * g) {
    bool found = false;
    int register j;

    for (j = 1; j < nn; j++) {
        if (g[j].visited == 0) {
            found = true;
            break;
        }
    }
    return found;
}

void cerr_node(Graph * g, int v, const char * func, const char * var) {
    int register j;
        cerr << func << ": " << var << "[" << v << "].node=" << g[v].node
             << endl;
        cerr << func << ": " << var << "[" << v << "].indegree="
             << g[v].indegree << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, v, g[v].path);
#endif
        if (g[v].path != NULL) {
            for (j = 1; j <= g[v].path[0]; j++) {
                cerr << func << ": " << var << "[" << v << "].path[" << j
                     << "]=" << g[v].path[j] << endl;
            }
        }
        cerr << endl;
}

void cerr_graph(Graph * g, const char * func, const char * var) {
    int register i;
    int register j;
    for (i = 1; i < nn; i++) {
        cerr << func << ": " << var << "[" << i << "].node=" << g[i].node << endl;
        cerr << func << ": " << var << "[" << i << "].visited=" << g[i].visited << endl;
        cerr << func << ": " << var << "[" << i << "].indegree=" << g[i].indegree << endl;
#if DEBUG > 2
        fprintf(stderr, "%s: %s[%d].path=%p\n", func, var, i, g[i].path);
#endif
        if (g[i].path != NULL) {
            for (j = 1; j <= g[i].path[0]; j++) {
                cerr << func << ": " << var << "[" << i << "].path[" << j << "]=" << g[i].path[j] << endl;
            }
        }
        cerr << endl;
    }
}
