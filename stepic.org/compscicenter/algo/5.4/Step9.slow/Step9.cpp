// Step9.cpp
// Version 0.5
// Задача на программирование. Дан ориентированный граф на n вершинах и m рёбрах
// (1≤n≤100000, 0≤m≤100000). Выведите количество компонент сильной связности в
// данном графе.
//
// Сроки сдачи задания:
// Soft дедлайн 27.10.2014 23:59 MSK 
// Hard дедлайн 03.11.2014 23:59 MSK

// Sample Input:
// 4 4
// 1 2
// 4 1
// 2 3
// 3 1
// Sample Output:
// 2
// Memory Limit: 256 MB
// Time Limit: 5 seconds
#include <ctime>
#include <stack>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define PATHSTART 256
using namespace std;
int m, n, nn, ccnum = 0;
time_t Start, End;
double difTime;
typedef struct graph {
    int node;
    int * path;
    int path_lim;
    int visited;
    int indegree;
} Graph;
bool zero_visited(Graph * g, int i = 1) {
    bool found = false;
    int register j;

    for (j = i; j < nn; j++) {
        if (g[j].visited == 0) {
            found = true;
            break;
        }
    }
    return found;
}
inline bool get_path(Graph * g, int a, int b) {
    int register i;
    if (g[a].path != NULL)
        for (i = 1; i <= g[a].path[0]; i++)
            if (g[a].path[i] == b)
                return true;
    return false;
}
inline void set_visited(Graph * g, int v) {
    g[v].visited = ccnum;
}
inline int get_visited(Graph * g, int v) {
    return g[v].visited;
}
int set_path(Graph * g, int a, int b) {
    if (g[a].path == NULL) {
        g[a].node = a;
        g[a].path = new int[PATHSTART];
        memset(g[a].path, 0, sizeof(int) * PATHSTART);
        g[a].path_lim = 8;
        g[a].path[0] = 1;
        g[a].path[1] = b;
        return 0;
    } else {
        if (get_path(g, a, b))
            return -1;
        int e = g[a].path[0] + 1;
        if (e >= g[a].path_lim) {
            int new_lim = g[a].path_lim * 2;
            int * new_path = new int[new_lim];
            // memset(new_path, 0, sizeof(int) * new_lim);
            memcpy(new_path, g[a].path, sizeof(int) * g[a].path_lim);
            g[a].path_lim = new_lim;
            delete g[a].path;
            g[a].path = new_path;
        }
        g[a].path[e] = b;
        g[a].path[0] = e;
        return 1;
    }
}
int explore(Graph * g, int v, int target, stack <int> & st) {
    int result = 0;
    int register i;
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
        return 1;
    }
    int k = g[v].path[0];
    for (i = 1; i <= k; i++) {
        if (get_visited(g, i) == 0) {
            if((result = explore(g, i, target, st)) == 1) {
                st.push(v);
                return 1;
            }
        }
    }
    st.push(v);
    return result;
}
int dfs(Graph * g, int v, int target, stack <int> & st) {
    int result;
    int register i;
    int register j;
    set_visited(g, v);
    if (get_path(g, v, target) == true) {
        set_visited(g, target);
        st.push(target);
        return 1;
    }
    for (i = 1; i < nn; i++) {
        if (get_visited(g, i) == 0 && get_path(g, v, i)) {
            if((result = explore(g, i, target, st)) == 1) {
                st.push(v);
                return 1;
            }
            if (zero_visited(r, i))
                break;
        }
    }
    st.push(v);
    return 0;
}
Graph * g, * r;
int main(void) {
    int a, b;
    int register i;
    int register j;
    int u, v;
    bool zero = false;
    ios_base::sync_with_stdio(0);
    cin >> n; nn = n + 1;
    cin >> m;
    if (m == 0) {
        cout << n << endl;
        return 0;
    }
    g = new Graph[nn + 2];
    r = new Graph[nn + 2];
    memset(g, 0, sizeof(Graph) * (nn + 2));
    memset(r, 0, sizeof(Graph) * (nn + 2));
    for (i = 0; i < m; i++) {
        cin >> a;
        cin >> b;
        set_path(g, a, b);
        set_path(r, b, a);
    }
    ccnum=1;
    stack <int> str;
    dfs(r, 1, nn + 1, str);
    for (i = 2; i < nn; i++) {
        zero = true;
        if (get_visited(r, i) == 0 && (zero = zero_visited(r, i))) {
            ccnum++;
            dfs(r, i, nn + 1, str);
        } else if(!zero)
          break;
    }
    ccnum=0;
    while (!(str.empty())) {
        stack <int> stg;
        int k = str.top();
        if (get_visited(g, k) == 0 && zero_visited(g)) {
            ccnum++;
            dfs(g, k, nn + 1, stg);
        }
        str.pop();
    }
    cout << ccnum << endl;
    // for (i = 0; i < nn; i++)
    //     if (g[i].path != NULL)
    //         delete [] g[i].path;
    // delete [] g;
    return 0;
}
