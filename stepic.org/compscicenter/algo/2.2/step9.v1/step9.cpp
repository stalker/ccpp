#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

#define BASE 10
#define MAXSIZE 256
// #define DEBUG 1

char emsg[MAXSIZE];

struct bigint {
  int b;
  int c;  // count
  char s; // sign
  char v[MAXSIZE]; // value
};

struct bigint * allocated[MAXSIZE];
int allocated_idx = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y);

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2);
void sum_bigint(struct bigint * pr, struct bigint & v1, struct bigint & v2);
void add_bigint(struct bigint * r, struct bigint & v);
void sub_bigint(struct bigint * r, struct bigint & v);
void mul_pow10_bigint(struct bigint * r, int p);
void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X);

void normalize_bigint(struct bigint * x, struct bigint * y);
struct bigint read_bigint(void);
void print_bigint(struct bigint & p);
void fprint_stderr_bigint(struct bigint & p);
void reverse_bigint(struct bigint * p);

void free_allocated(void);
void my_error(char * emsg);

struct bigint X, Y, Z;

#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (x), (y)) > -1)\
                               my_error(emsg);\
                               allocated[allocated_idx++] = (x);\
                             }
#define shift_level(x) for(int i=0; i < (x); i++){fprintf(stderr, "   ");}
int main(void){
    struct bigint T;
    struct bigint * pT;
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pres;
// #ifdef DEBUG
    char fncname[] = "main";
// #endif

    Z.c = 0;
    X = read_bigint();
    Y = read_bigint();
// #ifdef DEBUG
    print_bigint(X);
    cout << endl;
    print_bigint(Y);
    cout << endl;
    cout << endl;
// #endif
/*
    X.v[0] = 0; X.v[1] = 8; X.c = 2;
    Y.v[0] = 0; Y.v[1] = 9; Y.c = 2;
*/
    allocate_bigint(pres, "pres");
    Karatsuba(pres, X, Y);
    print_bigint(*pres);
    cout << endl;
    return 0;
}

static int level = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y) {
    char t;
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n_max = X.c > Y.c ? X.c : Y.c;
    int n_min = X.c < Y.c ? X.c : Y.c;
    int n2l = n_max / 2 + n_max % 2;
    int n2r = n_max - n2l;
    int n2up = n2l;
    int n2down = n_max / 2;
// #ifdef DEBUG
    char fncname[] = "Karatsuba";
// #endif

    normalize_bigint(&X, &Y);
    level++;
    if (r != NULL) {
        if (n_max == 1) {
            t = X.v[0] * Y.v[0];
            if (t >= BASE) {
                r->v[0] = t % BASE;
                r->v[1] = t / BASE;
                r->c = 2;
            } else {
                r->v[0] = t;
                r->c = 1;
            }
            return;
        } else if (n_min == 1) {
            if (X.c == 1) {
                if (X.v[0] == 0) {
                    r->c = 1;
                    r->v[0] = 0;
                    return;
                }
                if (X.v[0] == 1) {
                    *r = Y;
                    return;
                }
                int i;
                *r = Y;
                for (i = 1; i < X.v[0]; i++)
                    add_bigint(r, Y);
                return;
            }
            if (Y.c == 1) {
                if (Y.v[0] == 0) {
                    r->c = 1;
                    r->v[0] = 0;
                    return;
                }
                if (Y.v[0] == 1) {
                    *r = X;
                    return;
                }
                int i;
                *r = X;
                for (i = 1; i < Y.v[0]; i++)
                    add_bigint(r, X);
                return;
            }
        }
        allocate_bigint(pXl, "pXl");
        allocate_bigint(pXr, "pXr");
        allocate_bigint(pYl, "pYl");
        allocate_bigint(pYr, "pYr");
        split_bigint(pXl, pXr, X);
        split_bigint(pYl, pYr, Y);
        struct bigint P1 = Z;
        struct bigint P2 = Z;
        struct bigint P3 = Z;
        struct bigint R = Z;
        struct bigint Xl_plus_Xr = Z;
        struct bigint Yl_plus_Yr = Z;

#ifdef DEBUG
          fprintf(stderr, "\nlevel = %d\n", level);
          shift_level(level);
          fprintf(stderr, "P1 = Karatsuba( ");
          fprint_stderr_bigint(*pXl);
          fprintf(stderr, " , ");
          fprint_stderr_bigint(*pYl);
#endif
        Karatsuba(&P1, *pXl, *pYl);
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(P1);
          fprintf(stderr, "\n");
          shift_level(level);

          fprintf(stderr, "P2 = Karatsuba( ");
          fprint_stderr_bigint(*pXr);
          fprintf(stderr, " , ");
          fprint_stderr_bigint(*pYr);
#endif
        Karatsuba(&P2, *pXr, *pYr);
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(P2);
          fprintf(stderr, "\n");
          shift_level(level);
#endif
        sum_bigint(&Xl_plus_Xr, *pXl, *pXr);
        sum_bigint(&Yl_plus_Yr, *pYl, *pYr);
#ifdef DEBUG
          fprintf(stderr, "P3 = Karatsuba( ");
          fprint_stderr_bigint(Xl_plus_Xr);
          fprintf(stderr, " , ");
          fprint_stderr_bigint(Yl_plus_Yr);
#endif
        Karatsuba(&P3, Xl_plus_Xr, Yl_plus_Yr);
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(P3);
          fprintf(stderr, "\n");
          shift_level(level);
#endif
        sub_bigint(&P3, P1);
        sub_bigint(&P3, P2);
        mul_pow10_bigint(&P3, n2down);
#ifdef DEBUG
          fprintf(stderr, "\n");
          shift_level(level);
          fprintf(stderr, "(P3 - P1 - P2)*10^%d = ", n2down);
          fprint_stderr_bigint(P3);
          fprintf(stderr, "\n");
          shift_level(level);
#endif
        mul_pow10_bigint(&P1, 2*n2down);
#ifdef DEBUG
          fprintf(stderr, "\n");
          shift_level(level);
          fprintf(stderr, "P1*10^(2*%d) = ", n2down);
          fprint_stderr_bigint(P1);
          fprintf(stderr, "\n");
          shift_level(level);
#endif
        add_bigint(&P1, P3);
        add_bigint(&P1, P2);
        *r = P1;
    } else {
        sprintf(emsg, "in %s: r == %p\n", fncname, r);
        my_error(emsg);
    }
    level--;
}

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2){
    char s;
    int i, max, p;
    struct bigint r = Z;
// #ifdef DEBUG
    char fncname[] = "sum_bigint";
// #endif

    p = 0;
    max = (v1.c > v2.c) ? v1.c : v2.c ;
    for (i = 0; i < max; i++) {
        s = v1.v[i] + v2.v[i] + p;
        p = (s >= BASE) ? 1 : 0;
        r.v[i] = s % BASE;
    }
    if (p)
        if(i < MAXSIZE)
            r.v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    r.c = i;
    if (r.c > r.b)
        r.b = r.c;

    return r;
}

void sum_bigint(struct bigint * r, struct bigint & v1, struct bigint & v2) {
    char s;
    int i, max, p;
// #ifdef DEBUG
    char fncname[] = "sum_bigint";
// #endif

    p = 0;
    if ( r != NULL) {
        *r = Z;
        max = (v1.c > v2.c) ? v1.c : v2.c;
        for (i = 0; i < max; i++) {
            s = v1.v[i] + v2.v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        }
        if (p) {
            if(i < MAXSIZE)
                r->v[i++] = p;
            else {
                sprintf(emsg, "in %s: oversize: i = %\n",
                        fncname, i);
                my_error(emsg);
            }
        }
        r->c = i;
    } else {
        fprintf(stderr, "in sum_bigint r = %p\n", r);
    }
    if (r->c > r->b)
        r->b = r->c;
}

void add_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, max, p;
// #ifdef DEBUG
    char fncname[] = "add_bigint";
// #endif

    p = 0;
    max = (r->c > v.c) ? r->c : v.c;
    for (i = 0; i < max; i++) {
        if (i <= r->c) {
            s = v.v[i] + r->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        } else
            r->v[i] = v.v[i];
    }
    if (p) {
        if(i < MAXSIZE)
            r->v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    }
    r->c = i;
    if (r->c > r->b)
        r->b = r->c;
}


void sub_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, min, p;
// #ifdef DEBUG
    char fncname[] = "sub_bigint";
// #endif


    if (r->c < v.c) {
        sprintf(emsg, "in %s: r->c = %d < v.c = %d\n",
                fncname, r->c, v.c);
        my_error(emsg);
    } else if (r->c == v.c && r->v[r->c - 1] < v.v[v.c - 1]) {
        sprintf(emsg, "in %s: r->c = %d == v.c = %d && rc->v < v.v\n",
                fncname, r->c, v.c);
        fprintf(stderr, "\n r->v = ");
        fprint_stderr_bigint(*r);
        fprintf(stderr, "\n v.v = ");
        fprint_stderr_bigint(v);
        fprintf(stderr, "\n");
        my_error(emsg);
    }
    p = 0;
    min = (r->c < v.c) ? r->c : v.c;
    for (i = 0; i < min; i++) {
        if (r->v[i] < v.v[i] && i < (r->c - 1)) {
            p = i + 1;
            while(p < r->c) {
                if (r->v[p] > 0) {
                    r->v[p]--;
                    break;
                } else
                    r->v[p] = BASE - 1;
                p++;
            }
            r->v[i] = (r->v[i] + BASE) - v.v[i];
        } else
            r->v[i] -= v.v[i];
    }
    p = r->c - 1;
    while (r->v[p--] == 0 && p >= 0)
        r->c--;
}

void mul_pow10_bigint(struct bigint * r, int p) {
    int i;

    for (i = r->c - 1; i >= 0; i--)
        r->v[i + p] = r->v[i];
    for (i = 0; i < p; i++)
        r->v[i] = 0;
    r->c += p;
    if (r->c > r->b)
        r->b = r->c;
}

void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X) {
    int l, r;
    int n2l = X.b / 2 + X.b % 2;
    int n2r = X.b - n2l;

#ifdef DEBUG
    fprintf(stderr, "in split_bigint n2l = %d || n2r = %d\n", n2l, n2r);
#endif
    if ( Xl != NULL || Xr != NULL) {
        *Xl = Z;
        *Xr = Z;
        /*
        for (i = 0, j = 0; i < n2l; i++) {
            if (j < n2r) {
                Xr->v[j] = X.v[j];
                j++;
            }
            Xl->v[i] = X.v[i + n2r];
        }
        */
        Xl->b = n2l;
        Xr->b = n2r;
        X.c > n2r ? Xr->c = n2r : Xr->c = X.c;
        if (X.c < X.b) {
            int s;
            if ((s = X.c - Xr->c) > 0)
                Xl->c = s;
            else {
                Xl->c = 1;
                Xl->v[0] = 0;
            }
        } else
            Xl->c = n2l;
        // Xl->c = n2l;
        // n2r == 0 ? Xr->c = 1 : Xr->c = n2r;
        for (l = 0, r = 0; l < Xl->b; l++, r++) {
            // Xr->v[r] = X.v[r];
            (r < Xr->c) ? Xr->v[r] = X.v[r] : Xr->v[r] = 0;
            // Xl->v[l] = X.v[Xr.c + l];
            (l < Xl->c) ? Xl->v[l] = X.v[Xr->c + l] : Xl->v[l] = 0;
        }
    } else {
        fprintf(stderr, "in split_bigint Xl = %p || Xr = %p\n", Xl, Xr);
    }
    normalize_bigint(Xl, Xr);
}

void normalize_bigint(struct bigint * x, struct bigint * y) {
    int i, s, n_max;

    if ((s = x->c - y->c) > 0) {
        n_max = x->c;
        for (i = y->c; i < n_max; i++)
            y->v[i] = 0;
    } else if (s < 0) {
        n_max = y->c;
        for (i = x->c; i < n_max; i++)
            x->v[i] = 0;
    } else
        n_max = x->c;
    x->b = n_max;
    y->b = n_max;
}

struct bigint read_bigint(void) {
    char c;
    int i = 0;
    struct bigint t = Z;

    while (cin.get(c) && isspace(c)) {
    }
    cin.unget();
    while (cin.get(c) && isdigit(c)) {
        t.v[i++] = (char)(c - '0');
    }
    t.c = i;
    reverse_bigint(&t);

    if (!isdigit(c))
        cin.unget();

    return t;
}

void print_bigint(struct bigint & p) {
    int i;

    for (i = p.c - 1; i >= 0; i--)
        cout << (char)(p.v[i] + '0');
}

void fprint_stderr_bigint(struct bigint & p) {
    int i;
    char s[MAXSIZE];

    s[0] = '\0';

    for (i = p.c - 1; i >= 0; i--)
        sprintf(s, "%s%c", s, (char)(p.v[i] + '0'));
    fprintf(stderr, "%s", s);
}

void reverse_bigint(struct bigint * p) {
    char c;
    int i, j;

    for (i = 0, j = p->c - 1; i < j; i++, j--)
        c = p->v[i], p->v[i] = p->v[j], p->v[j] = c;
}

void free_allocated(void){
    int i;

    for (i = 0; i < allocated_idx; i++) {
        delete allocated[i];
        allocated[i] = NULL;
    }
}

void my_error(char * emsg) {
    fprintf(stderr, "FATAL ERROR: %s\n", emsg);
    exit(1);
}
