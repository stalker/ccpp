//
#include <bitset>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

#define BASE 10
#define MAXSIZE 256

char emsg[MAXSIZE];

struct bigint {
  int b;
  int c;  // count
  char s; // sign
  char v[MAXSIZE]; // value
};

struct bigint * allocated[MAXSIZE];
int allocated_idx = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y);

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2);
void sum_bigint(struct bigint * pr, struct bigint & v1, struct bigint & v2);
void add_bigint(struct bigint * r, struct bigint & v);
void sub_bigint(struct bigint * r, struct bigint & v);
void mul_pow10_bigint(struct bigint * r, int p);
void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X);

struct bigint read_bigint(void);
void print_bigint(struct bigint & p);
void fprint_stderr_bigint(struct bigint & p);
void reverse_bigint(struct bigint * p);

void free_allocated(void);
void my_error(char * emsg);

struct bigint X, Y, Z;

#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (x), (y)) > -1)\
                               my_error(emsg);\
                               allocated[allocated_idx++] = (x);\
                             }
#define shift_level(x) for(int i=0; i < (x); i++){fprintf(stderr, "   ");}
int main(void){
    struct bigint T;
    char fncname[] = "main";

    Z.c = 0;
    X = read_bigint();
    /*
    Y = read_bigint();
    */
    print_bigint(X);
    cout << endl;

    return 0;
}

void print_bigint(struct bigint & p) {
    int i;

    for (i = p.c - 1; i >= 0; i--)
        
    //    std::cout << std::bitset<8>(p.v[i]);
}

struct bigint read_bigint(void) {
    char c;
    int i = 0, x = 0, y = 0;
    struct bigint t = Z;

    while (cin.get(c) && isspace(c)) {
    }
    cin.unget();
    while (cin.get(c) && isdigit(c)) {
        y = x << 3;
#ifdef DEBUG > 9
        std::cout << "y = " << std::bitset<32>(x) << endl;
#endif
        x = x << 1;
#ifdef DEBUG > 9
        std::cout << "x = " << std::bitset<32>(x) << endl;
#endif
        x = (x + y) + (c - '0');
#ifdef DEBUG > 9
        std::cout << "x = " << std::bitset<32>(x) << endl;
#endif
        if (x > 255) {
#ifdef DEBUG > 9
            printf("i = %d, x = %d\n", i, x);
            std::cout << "x = " << std::bitset<32>(x) << endl;
#endif
            t.v[i] = (char)x;
            std::cout << "x = " << std::bitset<8>(t.v[i]) << endl;
            i++;
            x = x >> 8;
#ifdef DEBUG > 9
            printf("i = %d, x = %d\n", i, x);
            std::cout << "x = " << std::bitset<32>(x) << endl;
#endif
        }
    }
    t.v[i] = (char)x;
    std::cout << "x = " << std::bitset<8>(t.v[i]) << endl;
    i++;
    x = x >> 8;
    
    t.c = i;

    if (!isdigit(c))
        cin.unget();

    return t;
}

static int level = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y) {
    char t;
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n_max = X.c > Y.c ? X.c : Y.c;
    int n_min = X.c < Y.c ? X.c : Y.c;
    int n2l = n_max / 2 + n_max % 2;
    int n2r = n_max - n2l;
    int n2up = n2l;
    int n2down = n_max / 2;
    char fncname[] = "Karatsuba";

    level++;
    if (r != NULL) {
        if (n_max == 1) {
            t = X.v[0] * Y.v[0];
            if (t >= BASE) {
                r->v[0] = t % BASE;
                r->v[1] = t / BASE;
                r->c = 2;
            } else {
                r->v[0] = t;
                r->c = 1;
            }
            return;
        } else if (n_min == 1) {
            if (X.c == 1) {
                if (X.v[0] == 0) {
                    r->c = 1;
                    r->v[0] = 0;
                    return;
                }
                if (X.v[0] == 1) {
                    *r = Y;
                    return;
                }
                int i;
                *r = Y;
                for (i = 1; i < X.v[0]; i++)
                    add_bigint(r, Y);
                return;
            }
            if (Y.c == 1) {
                if (Y.v[0] == 0) {
                    r->c = 1;
                    r->v[0] = 0;
                    return;
                }
                if (Y.v[0] == 1) {
                    *r = X;
                    return;
                }
                int i;
                *r = X;
                for (i = 1; i < Y.v[0]; i++)
                    add_bigint(r, X);
                return;
            }
        }
        allocate_bigint(pXl, "pXl");
        allocate_bigint(pXr, "pXr");
        allocate_bigint(pYl, "pYl");
        allocate_bigint(pYr, "pYr");
        split_bigint(pXl, pXr, X);
        split_bigint(pYl, pYr, Y);
        struct bigint P1 = Z;
        struct bigint P2 = Z;
        struct bigint P3 = Z;
        struct bigint R = Z;
        struct bigint Xl_plus_Xr = Z;
        struct bigint Yl_plus_Yr = Z;

        fprintf(stderr, "\nlevel = %d\n", level);
        shift_level(level);
        fprintf(stderr, "P1 = Karatsuba( ");
        fprint_stderr_bigint(*pXl);
        fprintf(stderr, " , ");
        fprint_stderr_bigint(*pYl);
        Karatsuba(&P1, *pXl, *pYl);
        fprintf(stderr, ") = ");
        fprint_stderr_bigint(P1);
        fprintf(stderr, "\n");
        shift_level(level);

        fprintf(stderr, "P2 = Karatsuba( ");
        fprint_stderr_bigint(*pXr);
        fprintf(stderr, " , ");
        fprint_stderr_bigint(*pYr);
        Karatsuba(&P2, *pXr, *pYr);
        fprintf(stderr, ") = ");
        fprint_stderr_bigint(P2);
        fprintf(stderr, "\n");
        shift_level(level);

        sum_bigint(&Xl_plus_Xr, *pXl, *pXr);
        sum_bigint(&Yl_plus_Yr, *pYl, *pYr);

        fprintf(stderr, "P3 = Karatsuba( ");
        fprint_stderr_bigint(Xl_plus_Xr);
        fprintf(stderr, " , ");
        fprint_stderr_bigint(Yl_plus_Yr);
        Karatsuba(&P3, Xl_plus_Xr, Yl_plus_Yr);
        fprintf(stderr, ") = ");
        fprint_stderr_bigint(P3);
        fprintf(stderr, "\n");
        shift_level(level);

        sub_bigint(&P3, P1);
        sub_bigint(&P3, P2);
        mul_pow10_bigint(&P3, n2down);
        fprintf(stderr, "\n");
        shift_level(level);
        fprintf(stderr, "(P3 - P1 - P2)*10^%d = ", n2down);
        fprint_stderr_bigint(P3);
        fprintf(stderr, "\n");
        shift_level(level);

        mul_pow10_bigint(&P1, 2*n2down);
        fprintf(stderr, "\n");
        shift_level(level);
        fprintf(stderr, "P1*10^(2*%d) = ", n2down);
        fprint_stderr_bigint(P1);
        fprintf(stderr, "\n");
        shift_level(level);

        add_bigint(&P1, P3);
        add_bigint(&P1, P2);
        *r = P1;
    } else {
        sprintf(emsg, "in %s: r == %p\n", fncname, r);
        my_error(emsg);
    }
    level--;
}

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2){
    char s;
    int i, max, p;
    struct bigint r = Z;
    char fncname[] = "sum_bigint";

    p = 0;
    max = (v1.c > v2.c) ? v1.c : v2.c ;
    for (i = 0; i < max; i++) {
        s = v1.v[i] + v2.v[i] + p;
        p = (s >= BASE) ? 1 : 0;
        r.v[i] = s % BASE;
    }
    if (p)
        if(i < MAXSIZE)
            r.v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    r.c = i;

    return r;
}

void sum_bigint(struct bigint * r, struct bigint & v1, struct bigint & v2) {
    char s;
    int i, max, p;
    char fncname[] = "sum_bigint";

    p = 0;
    if ( r != NULL) {
        *r = Z;
        max = (v1.c > v2.c) ? v1.c : v2.c;
        for (i = 0; i < max; i++) {
            s = v1.v[i] + v2.v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        }
        if (p) {
            if(i < MAXSIZE)
                r->v[i++] = p;
            else {
                sprintf(emsg, "in %s: oversize: i = %\n",
                        fncname, i);
                my_error(emsg);
            }
        }
        r->c = i;
    } else {
        fprintf(stderr, "in sum_bigint r = %p\n", r);
    }
}

void add_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, max, p;
    char fncname[] = "add_bigint";

    p = 0;
    max = (r->c > v.c) ? r->c : v.c;
    for (i = 0; i < max; i++) {
        if (i <= r->c) {
            s = v.v[i] + r->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        } else
            r->v[i] = v.v[i];
    }
    if (p) {
        if(i < MAXSIZE)
            r->v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    }
    r->c = i;
}


void sub_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, min, p;
    char fncname[] = "sub_bigint";


    if (r->c < v.c) {
        sprintf(emsg, "in %s: r->c = %d < v.c = %d\n",
                fncname, r->c, v.c);
        my_error(emsg);
    } else if (r->c == v.c && r->v[r->c - 1] < v.v[v.c - 1]) {
        sprintf(emsg, "in %s: r->c = %d == v.c = %d && rc->v < v.v\n",
                fncname, r->c, v.c);
        my_error(emsg);
    }
    p = 0;
    min = (r->c < v.c) ? r->c : v.c;
    for (i = 0; i < min; i++) {
        if (r->v[i] < v.v[i] && i < (r->c - 1)) {
            p = i + 1;
            while(p < r->c) {
                if (r->v[p] > 0) {
                    r->v[p]--;
                    break;
                } else
                    r->v[p] = BASE - 1;
                p++;
            }
            r->v[i] = (r->v[i] + BASE) - v.v[i];
        } else
            r->v[i] -= v.v[i];
    }
    p = r->c - 1;
    while (r->v[p--] == 0 && p >= 0)
        r->c--;
}

void mul_pow10_bigint(struct bigint * r, int p) {
    int i;

    for (i = r->c - 1; i >= 0; i--)
        r->v[i + p] = r->v[i];
    for (i = 0; i < p; i++)
        r->v[i] = 0;
    r->c += p;
}

void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X) {
    int i, j;
    int n2l = X.c / 2 + X.c % 2;
    int n2r = X.c - n2l;

    fprintf(stderr, "in split_bigint n2l = %d || n2r = %d\n", n2l, n2r);
    if ( Xl != NULL || Xr != NULL) {
        *Xl = Z;
        *Xr = Z;
        for (i = 0, j = 0; i < n2l; i++) {
            if (j < n2r) {
                Xr->v[j] = X.v[j];
                j++;
            }
            Xl->v[i] = X.v[i + n2r];
        }
        Xl->c = n2l;
        n2r == 0 ? Xr->c = 1 : Xr->c = n2r;
    } else {
        fprintf(stderr, "in split_bigint Xl = %p || Xr = %p\n", Xl, Xr);
    }
}

void fprint_stderr_bigint(struct bigint & p) {
    int i;
    char s[MAXSIZE];

    s[0] = '\0';

    for (i = p.c - 1; i >= 0; i--)
        sprintf(s, "%s%c", s, (char)(p.v[i] + '0'));
    fprintf(stderr, "%s", s);
}

void reverse_bigint(struct bigint * p) {
    char c;
    int i, j;

    for (i = 0, j = p->c - 1; i < j; i++, j--)
        c = p->v[i], p->v[i] = p->v[j], p->v[j] = c;
}

void free_allocated(void){
    int i;

    for (i = 0; i < allocated_idx; i++) {
        delete allocated[i];
        allocated[i] = NULL;
    }
}

void my_error(char * emsg) {
    fprintf(stderr, "FATAL ERROR: %s\n", emsg);
    exit(1);
}


/* reverse3: reverse string s in place */
void reverse3(const char * p)
{
    char c, * r, * s;
    s = (char *)p;
    r = s + strlen(s) - 1;
    while (s < r) {
        c = *s;
        *s++ = *r;
        *r-- = c;
    }
}
/* itoa: convert n to characters in s */
/* itoa: преобразование n в строку s */
void my_itoa3(signed int n, const char * p)
{
    char * s;
    signed int sign, t;
    unsigned int z;
    /*DEBUG
    char msg[MAXLINE]; */

    s = (char *)p;
    /* z = ~((~0)>>1); */
    z = ~0; z >>= 1; z = ~z;
    /*DEBUG
    sprintf(msg, "z = %3d => ", z);
    my_puts_uchar_bits_msg(msg, z); */
    if ((sign = n) < 0) {        /* record sign */
        if (z == (unsigned int)n) {    /* C - K/R Exercise 3-4. */
            t = -(n + 1) % 10; /* fix sign overflow */
            *s++ = (9 != t) ? (t + '1') : '0';
            n = n + t + 1;
            n /= 10;
        }
        n = -n;
    }
    do {    /* generate digits in reverse order */
        *s++ = n % 10 + '0';  /* get next digit */
    } while ((n /= 10) > 0);       /* delete it */
    if (sign < 0)
        *s++ = '-';
    *s = '\0';
    reverse3(p);
}
