// step9.cpp
/* $Date$
 * $Id$
 * $Version: 1.16.2$
 * $Revision: 1$
 */
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

#define BASE 10
#define MAXSIZE 131072
//#define DEBUG 1

char emsg[MAXSIZE];

struct bigint {
  unsigned int b;
  unsigned int c;  // count
  char s; // sign
  char * v; // value
};

struct bigint_f {
  unsigned int b;
  unsigned int c;  // count
  char s; // sign
  char v[MAXSIZE]; // value
};

struct k_stack {
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n_max;
    int n_min;
    int n2l;
    int n2r;
    int n2up;
    int n2down;
    struct bigint *pP1;
    struct bigint *pP2;
    struct bigint *pP3;
    struct bigint *pXl_plus_Xr;
    struct bigint *pYl_plus_Yr;
};

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y);

struct bigint mul_bigint(struct bigint & v1, struct bigint & v2);
void add_bigint(struct bigint * r, struct bigint & v);
void sub_bigint(struct bigint * r, struct bigint & v);
void mul_pow10_bigint(struct bigint * r, int p);
void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X);

void normalize_bigint(struct bigint * x, struct bigint * y);
struct bigint read_bigint(void);
void print_bigint(struct bigint & p);
void fprint_stderr_bigint(struct bigint & p);
void reverse_bigint(struct bigint * p);

void free_allocated(void);
void my_error(char * emsg);

struct bigint_f X, Y, Z;
int in_size = 0
int out_size = 0

#ifdef DEBUG
#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#define allocate_bigint_v(x,y,z) { (x).v = new struct char[z];\
                                   if ((x).v == NULL &&\
                                   sprintf(emsg, "in %s: (%s) == %p\n",\
                                          fncname, (y), (x).v) > -1)\
                                   my_error(emsg);\
                                 }
#define allocate_k_stack(x,y) { (x) = new struct k_stack;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (x), (y)) > -1)\
                               my_error(emsg);\
                             }
#else
#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "(%s) == %p\n", (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#define allocate_bigint_v(x,y,z) { (x).v = new struct char[z];\
                                   if ((x).v == NULL &&\
                                   sprintf(emsg, "in %s: (%s) == %p\n",\
                                          fncname, (y), (x).v) > -1)\
                                   my_error(emsg);\
                                 }
#define allocate_k_stack(x,y) { (x) = new struct k_stack;\
                               if ((x) == NULL &&\
                               sprintf(emsg, " (%s) == %p\n",\
                                       (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#endif
#define shift_level(x) for(int i=0; i < (x); i++){fprintf(stderr, "   ");}
int main(void){
    struct bigint X;
    struct bigint Y;
#ifdef DEBUG
    char fncname[] = "main";
#endif
    fZ.c = 0;
    fX = read_bigint();
    fY = read_bigint();
    in_size = fX.c > fY.c ? fX.c : fY.c;
    out_size = in_size*2 + 1;
    X.v = new char[in_size];
    Y.v = new char[in_size];
    copy_bigint_f_to_bigint(X, fX);
    copy_bigint_f_to_bigint(Y, fY);
    return 0;
#ifdef DEBUG
    //print_bigint(X);
    //cout << endl;
    //print_bigint(Y);
    //cout << endl;
    //cout << endl;
#endif
    allocate_bigint(pres, "pres");
    Karatsuba(pres, X, Y);
    print_bigint(*pres);
    cout << endl;
    return 0;
}

static int level = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y) {
    // char t;
    /*
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n_max = X.c > Y.c ? X.c : Y.c;
    int n_min = X.c < Y.c ? X.c : Y.c;
    int n2l = n_max / 2 + n_max % 2;
    int n2r = n_max - n2l;
    int n2up = n2l;
    int n2down = n_max / 2;
    */
    struct k_stack * stack;
    allocate_k_stack(stack, "stack");
    if (stack == NULL) {
#ifdef DEBUG
        sprintf(emsg, "in %s: stack == %p\n", fncname, r);
#else
        sprintf(emsg, "in Karatsuba: stack == %p\n", r);
#endif
        my_error(emsg);
    }
    stack->n_max = X.c > Y.c ? X.c : Y.c;
    stack->n_min = X.c < Y.c ? X.c : Y.c;
    stack->n2l = stack->n_max / 2 + stack->n_max % 2;
    stack->n2r = stack->n_max - stack->n2l;
    stack->n2up = stack->n2l;
    stack->n2down = stack->n_max / 2;
#ifdef DEBUG
    char fncname[] = "Karatsuba";
#endif
    normalize_bigint(&X, &Y);
    level++;
    if (r != NULL) {
        if (stack->n_max < 10) {
            *r = mul_bigint(X,Y);
            delete stack;
            return;
        }
        if(X.c == 1 && X.v[0] == 0) {
            *r = Z;
            r->b = 1;
            r->c = 1;
            delete stack;
            return;
        }
        if(Y.c == 1 && Y.v[0] == 0) {
            *r = Z;
            r->b = 1;
            r->c = 1;
            delete stack;
            return;
        }
        allocate_bigint(stack->pXl, "pXl");
        allocate_bigint(stack->pXr, "pXr");
        allocate_bigint(stack->pYl, "pYl");
        allocate_bigint(stack->pYr, "pYr");
        split_bigint(stack->pXl, stack->pXr, X);
        split_bigint(stack->pYl, stack->pYr, Y);
        // struct bigint *pP1;
        allocate_bigint(stack->pP1, "pP1");
        allocate_bigint_v(stack->pP1.v, "pP1.v", out_size);
        *(stack->pP1) = Z;
        // struct bigint *pP2;
        allocate_bigint(stack->pP2, "pP2");
        *(stack->pP2) = Z;
        // struct bigint *pP3;
        allocate_bigint(stack->pP3, "pP3");
        *(stack->pP3) = Z;
        // struct bigint *pXl_plus_Xr;
        // struct bigint *pYl_plus_Yr;

#ifdef DEBUG
#if DEBUG > 9
          fprintf(stderr, "\nlevel = %d\n", level);
          shift_level(level);
#endif
          fprintf(stderr, "\nP1 = Karatsuba( ");
          fprint_stderr_bigint(*(stack->pXl));
          fprintf(stderr, " , ");
          fprint_stderr_bigint(*(stack->pYl));
#endif
        Karatsuba(stack->pP1, *(stack->pXl), *(stack->pYl));
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(*(stack->pP1));
          fprintf(stderr, "\n");
#if DEBUG > 9
          shift_level(level);
#endif

          fprintf(stderr, "P2 = Karatsuba( ");
          fprint_stderr_bigint(*(stack->pXr));
          fprintf(stderr, " , ");
          fprint_stderr_bigint(*(stack->pYr));
#endif
        Karatsuba(stack->pP2, *(stack->pXr), *(stack->pYr));
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(*(stack->pP2));
          fprintf(stderr, "\n");
#if DEBUG > 9
          shift_level(level);
#endif
#endif
        add_bigint(stack->pXl, *(stack->pXr));
        stack->pXl_plus_Xr = stack->pXl;
        add_bigint(stack->pYl, *(stack->pYr));
        stack->pYl_plus_Yr = stack->pYl;
#ifdef DEBUG
          fprintf(stderr, "P3 = Karatsuba( ");
          fprint_stderr_bigint(*pXl_plus_Xr);
          fprintf(stderr, " , ");
          fprint_stderr_bigint(*pYl_plus_Yr);
#endif
        Karatsuba(stack->pP3, *(stack->pXl_plus_Xr), *(stack->pYl_plus_Yr));
#ifdef DEBUG
          fprintf(stderr, ") = ");
          fprint_stderr_bigint(*(stack->pP3));
          fprintf(stderr, "\n");
#if DEBUG > 9
          shift_level(level);
#endif
#endif
        sub_bigint(stack->pP3, *(stack->pP1));
        sub_bigint(stack->pP3, *(stack->pP2));
        mul_pow10_bigint(stack->pP3, stack->n2down);
#ifdef DEBUG
#if DEBUG > 9
          fprintf(stderr, "\n");
          shift_level(level);
#endif
          fprintf(stderr, "(P3 - P1 - P2)*10^%d = ", stack->n2down);
          fprint_stderr_bigint(*(stack->pP3));
          fprintf(stderr, "\n");
#if DEBUG > 9
          shift_level(level);
#endif
#endif
        mul_pow10_bigint(stack->pP1, 2*stack->n2down);
#ifdef DEBUG
#if DEBUG > 9
          fprintf(stderr, "\n");
          shift_level(level);
#endif
          fprintf(stderr, "P1*10^(2*%d) = ", stack->n2down);
          fprint_stderr_bigint(*(stack->pP1));
          fprintf(stderr, "\n");
#if DEBUG > 9
          shift_level(level);
#endif
#endif
        add_bigint(stack->pP1, *(stack->pP3));
#ifdef DEBUG
          fprintf(stderr, "P1 + P3 = ");
          fprint_stderr_bigint(*(stack->pP1));
#endif
        add_bigint(stack->pP1, *(stack->pP2));
        *r = *(stack->pP1);
#ifdef DEBUG
          fprintf(stderr, "\n");
          fprintf(stderr, "P1 + P2 + P3 = ");
          fprint_stderr_bigint(*(stack->pP1));
          fprintf(stderr, " = ");
          fprint_stderr_bigint(*r);
          fprintf(stderr, "\n");
#endif
        delete stack->pP3;
        delete stack->pP2;
        delete stack->pP1;
        delete stack->pYr;
        delete stack->pYl;
        delete stack->pXr;
        delete stack->pXl;
    } else {
#ifdef DEBUG
        sprintf(emsg, "in %s: r == %p\n", fncname, r);
#else
        sprintf(emsg, "in Karatsuba: r == %p\n", r);
#endif
        my_error(emsg);
    }
    level--;
    delete stack;
}

struct bigint mul_bigint(struct bigint & v1, struct bigint & v2){
    struct bigint r;
    r = Z;
    char s[MAXSIZE + 1];
    if (v1.c < 10 && v2.c < 10) {
        int i, j;
        for (j = 0, i = v1.c - 1;i >=0; i--, j++)
            s[j] = v1.v[i] + '0';
        s[j] = '\0';
        long int li1 = atol(s);
        for (j = 0, i = v2.c - 1;i >=0; i--, j++)
            s[j] = v2.v[i] + '0';
        s[j] = '\0';
        long int li2 = atol(s);
        long long int lir = li1*li2;
        sprintf(s, "%lld", lir);
        i = strlen(s);
#if DEBUG > 8
        fprintf(stderr, "\nin mul_bigint: lir = li1 * li2 = %ld * %ld = %ld, s = %s, i = %d\n", li1, li2, lir, s ,i);
#endif
        r.c = i;
        r.b = i;
        i--;
        for (j = 0; i >= 0; i--, j++)
            r.v[i] = s[j] - '0';
        return r;
    } else
        return r;
}

void add_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, max, p;
#ifdef DEBUG
    char fncname[] = "add_bigint";
#endif
    struct bigint * v1;
    struct bigint * v2;

#if DEBUG > 1
    fprintf(stderr, "\nin %s: r->c = %u, v.c = %u r = ", fncname, r->c, v.c);
    fprint_stderr_bigint(*r);
    fprintf(stderr, ", v = ");
    fprint_stderr_bigint(v);
#endif
    p = 0;
    if (r->c >= v.c) {
        v1 = r;
        v2 = &v;
        max = r->c;
    } else {
        v1 = &v;
        v2 = r;
        max = v.c;
    }
    for (i = 0; i < max; i++) {
        if (i < v2->c) {
            s = v1->v[i] + v2->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        } else {
            s = v1->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        }
#if DEBUG > 2
        fprintf(stderr, "\nin %s: r->c = %u, i = %d, p = %d ", fncname, r->c, i, p);
#endif
    }
    r->c = i;
    if (p == 1 && i == r->c) {
        if (r->c == MAXSIZE) {
#ifdef DEBUG
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
#else
            sprintf(emsg, "oversize3: i = %\n", i);
#endif
            my_error(emsg);
        } else
            r->v[r->c++] = p;
    }
    if (r->c > r->b)
        r->b = r->c;
#if DEBUG > 1
    fprintf(stderr, ", r += v = ");
    fprint_stderr_bigint(*r);
    fprintf(stderr, ", r->c = %u, v.c = %u", r->c, v.c);
    fprintf(stderr, "\n");
#endif
}


void sub_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, min, p;
// #ifdef DEBUG
    char fncname[] = "sub_bigint";
// #endif


    if (r->c < v.c) {
        sprintf(emsg, "in %s: r->c = %d < v.c = %d\n",
                fncname, r->c, v.c);
        fprintf(stderr, "\n r->v = ");
        fprint_stderr_bigint(*r);
        fprintf(stderr, "\n v.v = ");
        fprint_stderr_bigint(v);
        fprintf(stderr, "\n");
        my_error(emsg);
    } else if (r->c == v.c && r->v[r->c - 1] < v.v[v.c - 1]) {
        sprintf(emsg, "in %s: r->c = %d == v.c = %d && rc->v < v.v\n",
                fncname, r->c, v.c);
        fprintf(stderr, "\n r->v = ");
        fprint_stderr_bigint(*r);
        fprintf(stderr, "\n v.v = ");
        fprint_stderr_bigint(v);
        fprintf(stderr, "\n");
        my_error(emsg);
    }
    p = 0;
    min = (r->c < v.c) ? r->c : v.c;
    for (i = 0; i < min; i++) {
        if (r->v[i] < v.v[i] && i < (r->c - 1)) {
            p = i + 1;
            while(p < r->c) {
                if (r->v[p] > 0) {
                    r->v[p]--;
                    break;
                } else
                    r->v[p] = BASE - 1;
                p++;
            }
            r->v[i] = (r->v[i] + BASE) - v.v[i];
        } else
            r->v[i] -= v.v[i];
    }
    p = r->c - 1;
    for (p = r->c - 1; r->v[p] == 0 && p > 0; p--)
        r->c--;
}

void mul_pow10_bigint(struct bigint * r, int p) {
    int i;

    if (r->c == 1 && r->v[0] == 0)
        return;
    for (i = r->c - 1; i >= 0; i--)
        r->v[i + p] = r->v[i];
    for (i = 0; i < p; i++)
        r->v[i] = 0;
    r->c += p;
    if (r->c > r->b)
        r->b = r->c;
}

void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X) {
    int l, r;
    int n2l = X.b / 2 + X.b % 2;
    int n2r = X.b - n2l;

#if DEBUG > 3
    fprintf(stderr, "\nin split_bigint n2l = %d || n2r = %d\n", n2l, n2r);
#endif
    if ( Xl != NULL || Xr != NULL) {
        *Xl = Z;
        *Xr = Z;
        Xl->b = n2l;
        Xr->b = n2r;
        X.c > n2r ? Xr->c = n2r : Xr->c = X.c;
        if (X.c < X.b) {
            int s;
            if ((s = X.c - Xr->c) > 0)
                Xl->c = s;
            else {
                Xl->c = 1;
                Xl->v[0] = 0;
            }
        } else
            Xl->c = n2l;
        for (l = 0, r = 0; l < Xl->b; l++, r++) {
            (r < Xr->c) ? Xr->v[r] = X.v[r] : Xr->v[r] = 0;
            (l < Xl->c) ? Xl->v[l] = X.v[Xr->c + l] : Xl->v[l] = 0;
        }
    } else {
        fprintf(stderr, "in split_bigint Xl = %p || Xr = %p\n", Xl, Xr);
    }
    normalize_bigint(Xl, Xr);
}

void normalize_bigint(struct bigint * x, struct bigint * y) {
    int i, s, n_max;

    if ((s = x->c - y->c) > 0) {
        n_max = x->c;
        for (i = y->c; i < n_max; i++)
            y->v[i] = 0;
    } else if (s < 0) {
        n_max = y->c;
        for (i = x->c; i < n_max; i++)
            x->v[i] = 0;
    } else
        n_max = x->c;
    x->b = n_max;
    y->b = n_max;
}

struct bigint read_bigint(void) {
    char c;
    int i = 0;
    struct bigint t = Z;

    while (cin.get(c) && isspace(c)) {
    }
    cin.unget();
    while (cin.get(c) && isdigit(c)) {
        t.v[i++] = (char)(c - '0');
    }
    t.b = i;
    t.c = i;
    reverse_bigint(&t);

    if (!isdigit(c))
        cin.unget();

    return t;
}

void print_bigint(struct bigint & p) {
    int i;

    for (i = p.c - 1; i >= 0; i--)
        cout << (char)(p.v[i] + '0');
}

void fprint_stderr_bigint(struct bigint & p) {
    int i;
    char s[MAXSIZE];

    s[0] = '\0';

    for (i = p.c - 1; i >= 0; i--)
        sprintf(s, "%s%c", s, (char)(p.v[i] + '0'));
    fprintf(stderr, "%s", s);
}

void reverse_bigint(struct bigint * p) {
    char c;
    int i, j;

    for (i = 0, j = p->c - 1; i < j; i++, j--)
        c = p->v[i], p->v[i] = p->v[j], p->v[j] = c;
}

void free_allocated(void){
    int i;

    for (i = 0; i < allocated_idx; i++) {
        delete allocated[i];
        allocated[i] = NULL;
    }
}

void my_error(char * emsg) {
    fprintf(stderr, "FATAL ERROR: %s\n", emsg);
    exit(1);
}

