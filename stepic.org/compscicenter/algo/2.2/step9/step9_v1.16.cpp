// step9.cpp
/* $Date$
 * $Id$
 * $Version: 1.16.13$
 * $Revision: 1$
 */
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

#define BASE 10
#define MAXSIZE 600000

char emsg[MAXSIZE];

struct bigint {
  unsigned int b;
  unsigned int c;  // count
  char s; // sign
  char v[MAXSIZE]; // value
};

struct k_stack {
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n_max;
    int n_min;
    int n2l;
    int n2r;
    int n2up;
    int n2down;
    struct bigint *pP1;
    struct bigint *pP2;
    struct bigint *pP3;
    struct bigint *pXl_plus_Xr;
    struct bigint *pYl_plus_Yr;
};

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y);

struct bigint mul_bigint(struct bigint & v1, struct bigint & v2);
void add_bigint(struct bigint * r, struct bigint & v);
void sub_bigint(struct bigint * r, struct bigint & v);
void mul_pow10_bigint(struct bigint * r, int p);
void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X);

void normalize_bigint(struct bigint * x, struct bigint * y);
struct bigint read_bigint(void);
void print_bigint(struct bigint & p);
void fprint_stderr_bigint(struct bigint & p);
void reverse_bigint(struct bigint * p);

void free_allocated(void);
void my_error(char * emsg);

struct bigint X, Y, Z;

#ifdef DEBUG
#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#define allocate_k_stack(x,y) { (x) = new struct k_stack;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#else
#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "(%s) == %p\n", (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#define allocate_k_stack(x,y) { (x) = new struct k_stack;\
                               if ((x) == NULL &&\
                               sprintf(emsg, " (%s) == %p\n",\
                                       (y), (x)) > -1)\
                               my_error(emsg);\
                             }
#endif
#define shift_level(x) for(int i=0; i < (x); i++){fprintf(stderr, "   ");}
int main(void){
    struct bigint * pres;
    Z.c = 0;
    X = read_bigint();
    Y = read_bigint();
    allocate_bigint(pres, "pres");
    Karatsuba(pres, X, Y);
    print_bigint(*pres);
    cout << endl;
    delete pres;
    return 0;
}

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y) {
    struct k_stack * stack;
    allocate_k_stack(stack, "stack");
    if (stack == NULL) {
        sprintf(emsg, "in Karatsuba: stack == %p\n", r);
        my_error(emsg);
    }
    stack->n_max = X.c > Y.c ? X.c : Y.c;
    stack->n_min = X.c < Y.c ? X.c : Y.c;
    stack->n2l = stack->n_max / 2 + stack->n_max % 2;
    stack->n2r = stack->n_max - stack->n2l;
    stack->n2up = stack->n2l;
    stack->n2down = stack->n_max / 2;
    normalize_bigint(&X, &Y);
    if (r != NULL) {
        if (stack->n_max < 10) {
            *r = mul_bigint(X,Y);
            delete stack;
            return;
        }
        if(X.c == 1 && X.v[0] == 0) {
            *r = Z;
            r->b = 1;
            r->c = 1;
            delete stack;
            return;
        }
        if(Y.c == 1 && Y.v[0] == 0) {
            *r = Z;
            r->b = 1;
            r->c = 1;
            delete stack;
            return;
        }
        allocate_bigint(stack->pXl, "pXl");
        allocate_bigint(stack->pXr, "pXr");
        allocate_bigint(stack->pYl, "pYl");
        allocate_bigint(stack->pYr, "pYr");
        split_bigint(stack->pXl, stack->pXr, X);
        split_bigint(stack->pYl, stack->pYr, Y);
        allocate_bigint(stack->pP1, "pP1");
        *(stack->pP1) = Z;
        allocate_bigint(stack->pP2, "pP2");
        *(stack->pP2) = Z;
        allocate_bigint(stack->pP3, "pP3");
        *(stack->pP3) = Z;
        Karatsuba(stack->pP1, *(stack->pXl), *(stack->pYl));
        Karatsuba(stack->pP2, *(stack->pXr), *(stack->pYr));
        add_bigint(stack->pXl, *(stack->pXr));
        stack->pXl_plus_Xr = stack->pXl;
        add_bigint(stack->pYl, *(stack->pYr));
        stack->pYl_plus_Yr = stack->pYl;
        Karatsuba(stack->pP3, *(stack->pXl_plus_Xr), *(stack->pYl_plus_Yr));
        sub_bigint(stack->pP3, *(stack->pP1));
        sub_bigint(stack->pP3, *(stack->pP2));
        mul_pow10_bigint(stack->pP3, stack->n2down);
        mul_pow10_bigint(stack->pP1, 2*stack->n2down);
        add_bigint(stack->pP1, *(stack->pP3));
        add_bigint(stack->pP1, *(stack->pP2));
        *r = *(stack->pP1);
        delete stack->pP3;
        delete stack->pP2;
        delete stack->pP1;
        delete stack->pYr;
        delete stack->pYl;
        delete stack->pXr;
        delete stack->pXl;
    } else {
        sprintf(emsg, "in Karatsuba: r == %p\n", r);
        my_error(emsg);
    }
    delete stack;
}

struct bigint mul_bigint(struct bigint & v1, struct bigint & v2){
    struct bigint r;
    r = Z;
    char s[MAXSIZE + 1];
    if (v1.c < 10 && v2.c < 10) {
        int i, j;
        for (j = 0, i = v1.c - 1;i >=0; i--, j++)
            s[j] = v1.v[i] + '0';
        s[j] = '\0';
        long int li1 = atol(s);
        for (j = 0, i = v2.c - 1;i >=0; i--, j++)
            s[j] = v2.v[i] + '0';
        s[j] = '\0';
        long int li2 = atol(s);
        long long int lir = li1*li2;
        sprintf(s, "%lld", lir);
        i = strlen(s);
        r.c = i;
        r.b = i;
        i--;
        for (j = 0; i >= 0; i--, j++)
            r.v[i] = s[j] - '0';
        return r;
    } else
        return r;
}

void add_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, max, p;
    struct bigint * v1;
    struct bigint * v2;

    p = 0;
    if (r->c >= v.c) {
        v1 = r;
        v2 = &v;
        max = r->c;
    } else {
        v1 = &v;
        v2 = r;
        max = v.c;
    }
    for (i = 0; i < max; i++) {
        if (i < v2->c) {
            s = v1->v[i] + v2->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        } else {
            s = v1->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        }
    }
    r->c = i;
    if (p == 1 && i == r->c) {
        if (r->c == MAXSIZE) {
            sprintf(emsg, "oversize3: i = %\n", i);
            my_error(emsg);
        } else
            r->v[r->c++] = p;
    }
    if (r->c > r->b)
        r->b = r->c;
}


void sub_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, min, p;

    if (r->c < v.c) {
        sprintf(emsg, "in sub_bigint: r->c = %d < v.c = %d\n",
                r->c, v.c);
        fprintf(stderr, "\n r->v = ");
        fprint_stderr_bigint(*r);
        fprintf(stderr, "\n v.v = ");
        fprint_stderr_bigint(v);
        fprintf(stderr, "\n");
        my_error(emsg);
    } else if (r->c == v.c && r->v[r->c - 1] < v.v[v.c - 1]) {
        sprintf(emsg, "in sub_bigint: r->c = %d == v.c = %d && rc->v < v.v\n",
                r->c, v.c);
        fprintf(stderr, "\n r->v = ");
        fprint_stderr_bigint(*r);
        fprintf(stderr, "\n v.v = ");
        fprint_stderr_bigint(v);
        fprintf(stderr, "\n");
        my_error(emsg);
    }
    p = 0;
    min = (r->c < v.c) ? r->c : v.c;
    for (i = 0; i < min; i++) {
        if (r->v[i] < v.v[i] && i < (r->c - 1)) {
            p = i + 1;
            while(p < r->c) {
                if (r->v[p] > 0) {
                    r->v[p]--;
                    break;
                } else
                    r->v[p] = BASE - 1;
                p++;
            }
            r->v[i] = (r->v[i] + BASE) - v.v[i];
        } else
            r->v[i] -= v.v[i];
    }
    p = r->c - 1;
    for (p = r->c - 1; r->v[p] == 0 && p > 0; p--)
        r->c--;
}

void mul_pow10_bigint(struct bigint * r, int p) {
    int i;

    if (r->c == 1 && r->v[0] == 0)
        return;
    for (i = r->c - 1; i >= 0; i--)
        r->v[i + p] = r->v[i];
    for (i = 0; i < p; i++)
        r->v[i] = 0;
    r->c += p;
    if (r->c > r->b)
        r->b = r->c;
}

void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X) {
    int l, r;
    int n2l = X.b / 2 + X.b % 2;
    int n2r = X.b - n2l;

    if ( Xl != NULL || Xr != NULL) {
        *Xl = Z;
        *Xr = Z;
        Xl->b = n2l;
        Xr->b = n2r;
        X.c > n2r ? Xr->c = n2r : Xr->c = X.c;
        if (X.c < X.b) {
            int s;
            if ((s = X.c - Xr->c) > 0)
                Xl->c = s;
            else {
                Xl->c = 1;
                Xl->v[0] = 0;
            }
        } else
            Xl->c = n2l;
        for (l = 0, r = 0; l < Xl->b; l++, r++) {
            (r < Xr->c) ? Xr->v[r] = X.v[r] : Xr->v[r] = 0;
            (l < Xl->c) ? Xl->v[l] = X.v[Xr->c + l] : Xl->v[l] = 0;
        }
    } else {
        fprintf(stderr, "in split_bigint Xl = %p || Xr = %p\n", Xl, Xr);
    }
    normalize_bigint(Xl, Xr);
}

void normalize_bigint(struct bigint * x, struct bigint * y) {
    int i, s, n_max;

    if ((s = x->c - y->c) > 0) {
        n_max = x->c;
        for (i = y->c; i < n_max; i++)
            y->v[i] = 0;
    } else if (s < 0) {
        n_max = y->c;
        for (i = x->c; i < n_max; i++)
            x->v[i] = 0;
    } else
        n_max = x->c;
    x->b = n_max;
    y->b = n_max;
}


void print_bigint(struct bigint & p) {
    int i;

    for (i = p.c - 1; i >= 0; i--)
        cout << (char)(p.v[i] + '0');
}

void fprint_stderr_bigint(struct bigint & p) {
    int i;
    char s[MAXSIZE];

    s[0] = '\0';

    for (i = p.c - 1; i >= 0; i--)
        sprintf(s, "%s%c", s, (char)(p.v[i] + '0'));
    fprintf(stderr, "%s", s);
}

struct bigint read_bigint(void) {
    char c;
    int i = 0;
    struct bigint t = Z;

    while (cin.get(c) && isspace(c)) {
    }
    cin.unget();
    while (cin.get(c) && isdigit(c)) {
        t.v[i++] = (char)(c - '0');
    }
    t.b = i;
    t.c = i;
    reverse_bigint(&t);

    if (!isdigit(c))
        cin.unget();

    return t;
}

void reverse_bigint(struct bigint * p) {
    char c;
    int i, j;

    for (i = 0, j = p->c - 1; i < j; i++, j--)
        c = p->v[i], p->v[i] = p->v[j], p->v[j] = c;
}

void my_error(char * emsg) {
    fprintf(stderr, "FATAL ERROR: %s\n", emsg);
    exit(1);
}
