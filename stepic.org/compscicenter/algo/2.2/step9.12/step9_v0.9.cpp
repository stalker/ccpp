#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

#define BASE 10
#define MAXSIZE 256

char emsg[MAXSIZE];

struct bigint {
  int b;
  int c;  // count
  char s; // sign
  char v[MAXSIZE]; // value
};

struct bigint * allocated[MAXSIZE];
int allocated_idx = 0;

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y);

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2);
void sum_bigint(struct bigint * pr, struct bigint & v1, struct bigint & v2);
void add_bigint(struct bigint * r, struct bigint & v);
void sub_bigint(struct bigint * r, struct bigint & v);
void mul_pow10_bigint(struct bigint * r, int p);
void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X);

struct bigint read_bigint(void);
void print_bigint(struct bigint & p);
void reverse_bigint(struct bigint * p);

void free_allocated(void);
void my_error(char * emsg);

struct bigint X, Y, Z;

#define allocate_bigint(x,y) { (x) = new struct bigint;\
                               if ((x) == NULL &&\
                               sprintf(emsg, "in %s: (%s) == %p\n",\
                                      fncname, (x), (y)) > -1)\
                               my_error(emsg);\
                               allocated[allocated_idx++] = (x);\
                             }
int main(void){
    struct bigint T;
    struct bigint * pT;
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pres;
    char fncname[] = "main";

    Z.c = 0;
    X = read_bigint();
    Y = read_bigint();

    print_bigint(X);
    cout << endl;
    print_bigint(Y);
    cout << endl;
    cout << endl;

    sub_bigint(&X, Y);
    print_bigint(X);
    cout << endl;
/*
    add_bigint(&X, Y);
    print_bigint(X);
    cout << endl;

    mul_pow10_bigint(&X, 30);
    print_bigint(X);
    cout << endl;
    // T = sum_bigint(X, Y);

    pT = new struct bigint;
    allocated[allocated_idx++] = pT;
    sum_bigint(pT, X, Y);
    if (pT != NULL) {
        print_bigint(*pT);
        cout << endl;
    } else {
        fprintf(stderr, "in main pT = %p\n", pT);
    }
    pXl = new struct bigint;
    allocated[allocated_idx++] = pXl;
    pXr = new struct bigint;
    allocated[allocated_idx++] = pXr;
    split_bigint(pXl, pXr, X);
    print_bigint(*pXl);
    cout << endl;
    print_bigint(*pXr);
    cout << endl;
    X.v[0] = 8; X.c = 1;
    Y.v[0] = 9; Y.c = 1;
    allocate_bigint(pres, "pres");
    Karatsuba(pres, X, Y);
    mul_pow10_bigint(pres, 3);
    print_bigint(*pres);
    cout << endl;
*/
    return 0;
}

void Karatsuba(struct bigint * r, struct bigint & X, struct bigint & Y) {
    char t;
    struct bigint * pXl;
    struct bigint * pXr;
    struct bigint * pYl;
    struct bigint * pYr;
    int n = X.c > Y.c ? X.c : Y.c;
    int n2l = n / 2 + n % 2;
    int n2r = n - n2l;
    char fncname[] = "Karatsuba";

    if (r != NULL) {
        if (n == 1) {
            t = X.v[0] * Y.v[0];
            if (t >= BASE) {
                r->v[0] = t % BASE;
                r->v[1] = t / BASE;
                r->c = 2;
            } else {
                r->v[0] = t;
                r->c = 1;
            }
            return;
        }
/*
        pXl = new struct bigint;
        if (pXl == NULL &&
            sprintf(emsg, "in %s: pXl == %p\n", fncname, pXl) > -1)
            my_error(emsg);
        allocated[allocated_idx++] = pXl;
*/
        allocate_bigint(pXl, "pXl");
        allocate_bigint(pXr, "pXr");
        allocate_bigint(pYl, "pYl");
        allocate_bigint(pYr, "pYr");
        split_bigint(pXl, pXr, X);
        split_bigint(pYl, pYr, Y);
        struct bigint P1 = Z;
        struct bigint P2 = Z;
        struct bigint P3 = Z;
        struct bigint Xl_plus_Xr = Z;
        struct bigint Yl_plus_Yr = Z;
        Karatsuba(&P1, *pXl, *pYl);
        Karatsuba(&P2, *pXr, *pYr);
        sum_bigint(&Xl_plus_Xr, *pXl, *pXr);
        sum_bigint(&Yl_plus_Yr, *pYl, *pYr);
    } else {
        sprintf(emsg, "in %s: r == %p\n", fncname, r);
        my_error(emsg);
    }
}

struct bigint sum_bigint(struct bigint & v1, struct bigint & v2){
    char s;
    int i, max, p;
    struct bigint r = Z;
    char fncname[] = "sum_bigint";

    p = 0;
    max = (v1.c > v2.c) ? v1.c : v2.c ;
    for (i = 0; i < max; i++) {
        s = v1.v[i] + v2.v[i] + p;
        p = (s >= BASE) ? 1 : 0;
        r.v[i] = s % BASE;
    }
    if (p)
        if(i < MAXSIZE)
            r.v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    r.c = i;

    return r;
}

void sum_bigint(struct bigint * r, struct bigint & v1, struct bigint & v2) {
    char s;
    int i, max, p;
    char fncname[] = "sum_bigint";

    p = 0;
    if ( r != NULL) {
        *r = Z;
        max = (v1.c > v2.c) ? v1.c : v2.c;
        for (i = 0; i < max; i++) {
            s = v1.v[i] + v2.v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        }
        if (p) {
            if(i < MAXSIZE)
                r->v[i++] = p;
            else {
                sprintf(emsg, "in %s: oversize: i = %\n",
                        fncname, i);
                my_error(emsg);
            }
        }
        r->c = i;
    } else {
        fprintf(stderr, "in sum_bigint r = %p\n", r);
    }
}

void add_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, max, p;

    p = 0;
    max = (r->c > v.c) ? r->c : v.c;
    for (i = 0; i < max; i++) {
        if (i <= r->c) {
            s = v.v[i] + r->v[i] + p;
            p = (s >= BASE) ? 1 : 0;
            r->v[i] = s % BASE;
        } else
            r->v[i] = v.v[i];
    }
    if (p) {
        if(i < MAXSIZE)
            r->v[i++] = p;
        else {
            sprintf(emsg, "in %s: oversize: i = %\n",
                    fncname, i);
            my_error(emsg);
        }
    }
    r->c = i;
}


void sub_bigint(struct bigint * r, struct bigint & v) {
    char s;
    int i, min, p;
    char fncname[] = "sub_bigint";


    if (r->c < v.c) {
        sprintf(emsg, "in %s: r->c = %d < v.c = %d\n",
                fncname, r->c, v.c);
        my_error(emsg);
    } else if (r->c == v.c && r->v[r->c - 1] < v.v[v.c - 1]) {
        sprintf(emsg, "in %s: r->c = %d == v.c = %d && rc->v < v.v\n",
                fncname, r->c, v.c);
        my_error(emsg);
    }
    p = 0;
    min = (r->c < v.c) ? r->c : v.c;
    for (i = 0; i < min; i++) {
        if (r->v[i] < v.v[i] && i < (r->c - 1)) {
            p = i + 1;
            while(p < r->c) {
                if (r->v[p] > 0) {
                    r->v[p]--;
                    break;
                } else
                    r->v[p] = BASE - 1;
                p++;
            }
            r->v[i] = (r->v[i] + BASE) - v.v[i];
        } else
            r->v[i] -= v.v[i];
    }
    p = r->c - 1;
    while (r->v[p--] == 0 && p >= 0)
        r->c--;
}

void mul_pow10_bigint(struct bigint * r, int p) {
    int i;

    for (i = r->c - 1; i >= 0; i--)
        r->v[i + p] = r->v[i];
    for (i = 0; i < p; i++)
        r->v[i] = 0;
    r->c += p;
}

void split_bigint(struct bigint * Xl, struct bigint * Xr, struct bigint & X) {
    int i, j;
    int n2l = X.c / 2 + X.c % 2;
    int n2r = X.c - n2l;

    // cout << "split_bigint: n2l = " << n2l << endl;
    // cout << "split_bigint: n2r = " << n2r << endl;
    if ( Xl != NULL || Xr != NULL) {
        *Xl = Z;
        *Xr = Z;
        for (i = 0, j = 0; i < n2l; i++) {
            // fprintf(stderr, "split_bigint: i = %d, j = %d, i + n2r = %d\n", i, j, i + n2r);
            if (j < n2r) {
                Xr->v[j] = X.v[j];
                j++;
            }
            Xl->v[i] = X.v[i + n2r];
        }
        Xl->c = n2l;
        Xr->c = n2r;
    } else {
        fprintf(stderr, "in split_bigint Xl = %p || Xr = %p\n", Xl, Xr);
    }
}

struct bigint read_bigint(void) {
    char c;
    int i = 0;
    struct bigint t = Z;

    while (cin.get(c) && isspace(c)) {
    }
    cin.unget();
    while (cin.get(c) && isdigit(c)) {
        t.v[i++] = (char)(c - '0');
    }
    t.c = i;
    reverse_bigint(&t);

    if (!isdigit(c))
        cin.unget();

    return t;
}

void print_bigint(struct bigint & p) {
    int i;

    for (i = p.c - 1; i >= 0; i--)
        cout << (char)(p.v[i] + '0');
}

void reverse_bigint(struct bigint * p) {
    char c;
    int i, j;

    for (i = 0, j = p->c - 1; i < j; i++, j--)
        c = p->v[i], p->v[i] = p->v[j], p->v[j] = c;
}

void free_allocated(void){
    int i;

    for (i = 0; i < allocated_idx; i++) {
        delete allocated[i];
        allocated[i] = NULL;
    }
}

void my_error(char * emsg) {
    fprintf(stderr, "FATAL ERROR: %s\n", emsg);
    exit(1);
}
