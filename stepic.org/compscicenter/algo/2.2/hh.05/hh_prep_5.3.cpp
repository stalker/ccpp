
/* $Date$
 * $Id$
 * $Version: 0.5.3$
 * $Revision: 1$
 */
#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
 
int length = 100002;
#define BASE 10000
#define STRLEN 100008
#define MIN_LENGTH_KARATSUBA 4
typedef int digit;
typedef unsigned long int size_length;

using namespace std;

struct bigint {
  digit *vals;
  size_length length;
};

bigint sum(bigint a, bigint b) {
  bigint s;
  s.length = a.length + 1;
  s.vals = new digit[s.length];

  s.vals[a.length - 1] = a.vals[a.length - 1];
  s.vals[a.length] = 0;
  for (size_length i = 0; i < b.length; ++i)
    s.vals[i] = a.vals[i] + b.vals[i];
  return s;
}

bigint &sub(bigint &a, bigint b) {
  for (size_length i = 0; i < b.length; ++i)
    a.vals[i] -= b.vals[i];
  return a;
}

void normalize(bigint l) {
  for (size_length i = 0; i < l.length - 1; ++i) {
    if (l.vals[i] >= BASE) {
      digit carryover = l.vals[i] / BASE;
      l.vals[i + 1] += carryover;
      l.vals[i] -= carryover * BASE;

    } else if (l.vals[i] < 0) {
      digit carryover = (l.vals[i] + 1) / BASE - 1;
      l.vals[i + 1] += carryover;
      l.vals[i] -= carryover * BASE;

    }
  }
}

bigint karatsuba(bigint a, bigint b) {
  bigint product;
  product.length = a.length + b.length;
  product.vals = new digit[product.length];

  if (a.length < MIN_LENGTH_KARATSUBA) {
    memset(product.vals, 0, sizeof(digit) * product.length);
    for (size_length i = 0; i < a.length; ++i)
      for (size_length j = 0; j < b.length; ++j) {
        product.vals[i + j] += a.vals[i] * b.vals[j];
      }
  } else {
    bigint a_part1;
    a_part1.vals = a.vals;
    a_part1.length = (a.length + 1) / 2;

    bigint a_part2;
    a_part2.vals = a.vals + a_part1.length;
    a_part2.length = a.length / 2;

    bigint b_part1;
    b_part1.vals = b.vals;
    b_part1.length = (b.length + 1) / 2;

    bigint b_part2;
    b_part2.vals = b.vals + b_part1.length;
    b_part2.length = b.length / 2;

    bigint sum_of_a_parts = sum(a_part1, a_part2);
    normalize(sum_of_a_parts);
    bigint sum_of_b_parts = sum(b_part1, b_part2);
    normalize(sum_of_b_parts);
    bigint prodof_sums_of_parts = karatsuba(sum_of_a_parts, sum_of_b_parts);


    bigint prodof_first_parts = karatsuba(a_part1, b_part1);
    bigint prodof_second_parts = karatsuba(a_part2, b_part2);
    bigint sum_of_middle_terms = sub(sub(prodof_sums_of_parts, prodof_first_parts), prodof_second_parts);

    memcpy(product.vals, prodof_first_parts.vals,
      prodof_first_parts.length * sizeof(digit));
    memcpy(product.vals + prodof_first_parts.length,
      prodof_second_parts.vals, prodof_second_parts.length
      * sizeof(digit));
    for (size_length i = 0; i < sum_of_middle_terms.length; ++i)
      product.vals[a_part1.length + i] += sum_of_middle_terms.vals[i];

    delete [] sum_of_a_parts.vals;
    delete [] sum_of_b_parts.vals;
    delete [] prodof_sums_of_parts.vals;
    delete [] prodof_first_parts.vals;
    delete [] prodof_second_parts.vals;
  }

  normalize(product);

  return product;
}

int strlength = STRLEN;
char tmp1[STRLEN + 1];
char tmp2[STRLEN + 1];

struct bigint * read_bigint(void) {
    char c, * p1, *p2;
    int i = 0, j = 0, k = 1, x = 0, y = 0;;
    struct bigint * t;

    t = new struct bigint;

    t->length = length + 1;
    t->vals = new digit[length];

    cin.getline(tmp1, strlength);

    p1 = tmp1;
    p2 = tmp2;
    while (isspace(*p1)) {
        p1++;
    }
    if ((p1 - tmp1) > 0)p1--;
    while (isdigit(*p1)) {
        *p2++ = *p1++;
    }
    *p2 = '\0';
    x = 1;
    for (j = length; j >=(p2 - tmp2 - 1); j--)
        t->vals[j] = 0;
    for (j = (p2 - tmp2 - 1); j >=0; j--, k++) {

        t->vals[i] = t->vals[i] + x*(tmp2[j] - '0');
        if (k != 0 && (k % 4) == 0){
            i++;
            x = 1;
        } else {
            y = x << 3;
            x = x << 1;
            x = (x + y);
        }
    }
    return t;
}

void print_bigint(struct bigint & p) {
    int i;
    int c;
    char s[20];

    for (i = p.length - 1; p.vals[i] == 0; --i) {
    }
    sprintf(s,"%d", p.vals[i--]);
    cout << s;
    for (; i >= 0; i--) {
        sprintf(s,"%04d", p.vals[i]);
        cout << s;
    }
}

struct bigint * x;
struct bigint * y;
struct bigint r;

int main(void){

    x = read_bigint();
    y = read_bigint();
    r = karatsuba(*x, *y);

    print_bigint(r);
    cout << endl;
    delete [] y->vals;
    delete [] x->vals;
    delete y;
    delete x;
    return 0;
}
