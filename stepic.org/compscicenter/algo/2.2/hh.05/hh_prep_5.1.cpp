
/* $Date$
 * $Id$
 * $Version: 0.5$
 * $Revision: 1$
 */
#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
 
int length = 100002;
#define BASE 10000
#define STRLEN 100008
#define MIN_LENGTH_FOR_KARATSUBA 4
typedef int digit;
typedef unsigned long int size_length;

using namespace std;

struct long_value {
  digit *values;
  size_length length;
};

long_value sum(long_value a, long_value b) {
  long_value s;
  s.length = a.length + 1;
  s.values = new digit[s.length];

  s.values[a.length - 1] = a.values[a.length - 1];
  s.values[a.length] = 0;
  for (size_length i = 0; i < b.length; ++i)
    s.values[i] = a.values[i] + b.values[i];
  return s;
}

long_value &sub(long_value &a, long_value b) {
  for (size_length i = 0; i < b.length; ++i)
    a.values[i] -= b.values[i];
  return a;
}

void normalize(long_value l) {
  for (size_length i = 0; i < l.length - 1; ++i) {
    if (l.values[i] >= BASE) {
      digit carryover = l.values[i] / BASE;
      l.values[i + 1] += carryover;
      l.values[i] -= carryover * BASE;

    } else if (l.values[i] < 0) {
      digit carryover = (l.values[i] + 1) / BASE - 1;
      l.values[i + 1] += carryover;
      l.values[i] -= carryover * BASE;

    }
  }
}

long_value karatsuba(long_value a, long_value b) {
  long_value product;
  product.length = a.length + b.length;
  product.values = new digit[product.length];

  if (a.length < MIN_LENGTH_FOR_KARATSUBA) {
    memset(product.values, 0, sizeof(digit) * product.length);
    for (size_length i = 0; i < a.length; ++i)
      for (size_length j = 0; j < b.length; ++j) {
        product.values[i + j] += a.values[i] * b.values[j];
      }
  } else {
    long_value a_part1;
    a_part1.values = a.values;
    a_part1.length = (a.length + 1) / 2;

    long_value a_part2;
    a_part2.values = a.values + a_part1.length;
    a_part2.length = a.length / 2;

    long_value b_part1;
    b_part1.values = b.values;
    b_part1.length = (b.length + 1) / 2;

    long_value b_part2;
    b_part2.values = b.values + b_part1.length;
    b_part2.length = b.length / 2;

    long_value sum_of_a_parts = sum(a_part1, a_part2);
    normalize(sum_of_a_parts);
    long_value sum_of_b_parts = sum(b_part1, b_part2);
    normalize(sum_of_b_parts);
    long_value product_of_sums_of_parts = karatsuba(sum_of_a_parts, sum_of_b_parts);


    long_value product_of_first_parts = karatsuba(a_part1, b_part1);
    long_value product_of_second_parts = karatsuba(a_part2, b_part2);
    long_value sum_of_middle_terms = sub(sub(product_of_sums_of_parts, product_of_first_parts), product_of_second_parts);

    memcpy(product.values, product_of_first_parts.values,
      product_of_first_parts.length * sizeof(digit));
    memcpy(product.values + product_of_first_parts.length,
      product_of_second_parts.values, product_of_second_parts.length
      * sizeof(digit));
    for (size_length i = 0; i < sum_of_middle_terms.length; ++i)
      product.values[a_part1.length + i] += sum_of_middle_terms.values[i];

    delete [] sum_of_a_parts.values;
    delete [] sum_of_b_parts.values;
    delete [] product_of_sums_of_parts.values;
    delete [] product_of_first_parts.values;
    delete [] product_of_second_parts.values;
  }

  normalize(product);

  return product;
}

int strlength = STRLEN;
char tmp1[STRLEN + 1];
char tmp2[STRLEN + 1];

struct long_value * read_bigint(void) {
    char c, * p1, *p2;
    int i = 0, j = 0, k = 1, x = 0, y = 0;;
    struct long_value * t;

    t = new struct long_value;

    t->length = length + 1;
    t->values = new digit[length];

    cin.getline(tmp1, strlength);

    p1 = tmp1;
    p2 = tmp2;
    while (isspace(*p1)) {
        p1++;
    }
    if ((p1 - tmp1) > 0)p1--;
    while (isdigit(*p1)) {
        *p2++ = *p1++;
    }
    *p2 = '\0';
    x = 1;
    for (j = length; j >=(p2 - tmp2 - 1); j--)
        t->values[j] = 0;
    for (j = (p2 - tmp2 - 1); j >=0; j--, k++) {

        t->values[i] = t->values[i] + x*(tmp2[j] - '0');
        if (k != 0 && (k % 4) == 0){
            i++;
            x = 1;
        } else {
            y = x << 3;
            x = x << 1;
            x = (x + y);
        }
    }
    return t;
}

void print_bigint(struct long_value & p) {
    int i;
    int c;
    char s[20];

    for (i = p.length - 1; p.values[i] == 0; --i) {
    }
    sprintf(s,"%d", p.values[i--]);
    cout << s;
    for (; i >= 0; i--) {
        sprintf(s,"%04d", p.values[i]);
        cout << s;
    }
}

struct long_value * x;
struct long_value * y;
struct long_value r;

int main(void){

    x = read_bigint();
    y = read_bigint();
    r = karatsuba(*x, *y);

    print_bigint(r);
    cout << endl;
    delete [] y->values;
    delete [] x->values;
    delete y;
    delete x;
    return 0;
}
