#!/usr/bin/perl

my $i;


sub log2 {
    my $n = shift;
    return log($n)/log(2);
}

$i = 10**50000;
printf "log2($i) = %d\n", log2($i)