#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

#define SIZE 1000001

int n;
int a[SIZE];

void myswap(int i, int j)
{
    int temp;

    temp = a[i];
    a[i] = a[j];
    a[j] = temp;
}

void myqsort(int left, int right)
{
    int i, last;

    if (left >= right)
        myswap(left, (left + right)/2);

    last = left;

    for (i = left + 1; i <= right; i++)
        if (a[i] < a[left])
            myswap(++last, i);
    myswap(left, last);
    myqsort(left, last-1);
    myqsort(last+1, right);
}


int main(void){
    int i, m;

    cin >> n;

    for (i = 0; i < n; i++)
        cin >> a[i];

    myqsort(0, n - 1);

    for (i = 0; i < (n / 2) + 1; i++) {
        m = i + n/2;
        if(a[i] == a[m]) {
            cout << "1" << endl;
            return 1;
        }
    }
    cout << "0" << endl;

    return 0;
}
