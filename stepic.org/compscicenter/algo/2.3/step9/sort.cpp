#include <iostream>
#include <typeinfo>
#include <cstdlib>

#include "sort.h"

using namespace std;


//----------------- Functions Declaration --------------------
unsigned cSort_t::recursionDepth = 0;


//----------------- Default Constractor --------------------
cSort_t::cSort_t()
{

}


//----------------- Copy Constractor --------------------
cSort_t::cSort_t (const cSort_t& ref)
{

}


//----------------- Destractor --------------------
cSort_t::~cSort_t()
{

}

//----------------- Assigment Operator --------------------
const cSort_t& cSort_t::operator= (const cSort_t& ref)
{
    if (this == &ref)
        return *this;

    return *this;
}


//----------------- Choose Pivot Function --------------------
unsigned cSort_t::choosePivot (const unsigned size)
{
    if (size < 3)
        return 0;

    unsigned pivot[] = {0,0,0};

    srand(time(NULL));

    for (int i=0; i<3; ++i)
    {
        pivot[i] = rand() % (size);
    }

    if (pivot[2] < pivot[0])
        swap (pivot, pivot+2, sizeof(unsigned));
    if (pivot[1] < pivot[0])
        swap (pivot, pivot+1, sizeof(unsigned));
    if (pivot[2] < pivot[1])
        swap (pivot+1, pivot+2, sizeof(unsigned));

    return pivot[1];
}
