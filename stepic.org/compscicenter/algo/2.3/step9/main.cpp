#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>

#include "sort.h"

using namespace std;

int main()
{
    time_t STLStart, STLEnd, introStart, introEnd;
    unsigned size = 59999999;
    double difTimeSTL, difTimeIntro;

    cin >> size;

    vector<int> vec1;

    vec1.reserve((size_t)size);

    unsigned*  d = new unsigned [size];

    srand(time(NULL));

/*
    for (unsigned i=0; i<size; ++i)
    {
        d[i] = (rand()%9999999);
        vec1.push_back (d[i]);
    }
*/

    for (unsigned i = 0; i < size; i++) {
        cin >> d[i];
        vec1.push_back (d[i]);
    }

    cout << "Entering of numbers is finished" << endl;
    cout << "Starting Intro Sort" << endl;

    time (&introStart);

    cSort_t::introSort(d, (unsigned )size);

    time (&introEnd);
    difTimeIntro = difftime (introEnd, introStart);

    cout << "Intro Sort is finished" << endl;
    cout << "Starting STL sort" << endl;

    time (&STLStart);

    sort (vec1.begin(), vec1.end());

    time (&STLEnd);
    difTimeSTL = difftime (STLEnd, STLStart);

    cout << "STL Sort is finished" << endl;
    cout << endl << "STL Sort: " << difTimeSTL << "  Intro Sort: " << difTimeIntro << endl;

    for (unsigned i=0; i<size; ++i)
        if (vec1[i]!=d[i])
            cout << "Sorted arrays by STL and Intro Sort aren't identical" << endl;

    delete [] d;

    return 0;
}
