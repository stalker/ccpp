#include <iostream>

int main(void){
    int x;
    unsigned int n, max_i;
    std::cin >> n;
    int r[100000];
    // memset(r, 0, sizeof(unsigned int) * 2000000000);
    for (unsigned int i = 0; i < n; i++) {
        std::cin >> r[i];
        max_i = 1;
        for (unsigned int j = 0; j < i; j++) {
            if (r[i] == r[j]) {
                max_i++;
            }
            if (max_i > (n/2)) {
                std::cout << "1" << std::endl;
                return 0;
            }
        }
    }
    std::cout << "0" << std::endl;
    return 0;
}
