#include <cstdio>
#include <cctype>
#include <cstdlib>

int count = 0;

int* merge_sort(int *up, int *down, unsigned int left, unsigned int right) {
    if (left == right) {
        down[left] = up[left];
        return down;
    }
    unsigned int middle = (unsigned int)((left + right) * 0.5f);
    int *l_buff = merge_sort(up, down, left, middle);
    int *r_buff = merge_sort(up, down, middle + 1, right);
    int *target = l_buff == up ? down : up;
    unsigned int width = right - left, l_cur = left, r_cur = middle + 1;
    for (unsigned int i = left; i <= right; i++) {
        if (l_cur <= middle && r_cur <= right) {
            if (l_buff[l_cur] < r_buff[r_cur]) {
                target[i] = l_buff[l_cur];
                l_cur++;
            } else {
                count++;
                target[i] = r_buff[r_cur];
                r_cur++;
            }
        } else if (l_cur <= middle) {
            target[i] = l_buff[l_cur];
            l_cur++;
        } else {
            target[i] = r_buff[r_cur];
            r_cur++;
        }
    }
    return target;
}

int main(void) {
    int i, n;
    int * a;
    int * b;
    char line[255];
    char * ss;

    fgets(line, 255, stdin);
    n = strtol(line, &ss, 10);
    a = (int *)calloc(n, sizeof(int));
    b = (int *)calloc(n, sizeof(int));
    for (i = 0; i < n; ) {
        int isd, eol;
        eol = (*ss == '\0') || (*ss == '\n');
        isd = isdigit(*ss);
        while (!isd && !eol) {
            ss++;
            eol = (*ss == '\0') || (*ss == '\n');
            isd = isdigit(*ss);
        }
        if (eol)
            fgets(ss = line, 255, stdin);
        else if (isd)
            a[i++] = strtol(ss, &ss, 10);
    }
    merge_sort(a, b, 0, n - 1);
    printf("%d\n", count);
    return 0;
}
