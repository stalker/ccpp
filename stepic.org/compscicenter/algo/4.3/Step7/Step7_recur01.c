#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

/*
 * Сортирует массив используя рекурсивную сортировку слиянием
 * up - указатель на массив который нужно сортировать
 * down - указатель на массив с, как минимум, таким же размерои как у 'up',
 *        используется как буфер
 * left - левая граница массива, передайте 0 чтобы сортировать массив с начала
 * right - правая граница массива, передайте длинну массива - 1 чтобы
 *         сортировать массив до последнего элемента
 * возвращает: указатель на отсортированный массив. Из за особенностей работы
 *             данной имплементации, отсортированная версия массива может
 *             оказаться либо в 'up' либо в 'down
 */
int* merge_sort(int *up, int *down, unsigned int left, unsigned int right)
{
    if (left == right)
    {
        down[left] = up[left];
        return down;
    }

    unsigned int middle = (unsigned int)((left + right) * 0.5f);

    // разделяй и сортируй
    int *l_buff = merge_sort(up, down, left, middle);
    int *r_buff = merge_sort(up, down, middle + 1, right);

    // слияние двух отсортированных половин
    int *target = l_buff == up ? down : up;

    unsigned int width = right - left, l_cur = left, r_cur = middle + 1;
    for (unsigned int i = left; i <= right; i++) {
        if (l_cur <= middle && r_cur <= right) {
            if (l_buff[l_cur] < r_buff[r_cur]) {
                    target[i] = l_buff[l_cur];
                    l_cur++;
            } else {
                    target[i] = r_buff[r_cur];
                    r_cur++;
            }
        } else if (l_cur <= middle) {
            target[i] = l_buff[l_cur];
            l_cur++;
        } else {
            target[i] = r_buff[r_cur];
            r_cur++;
        }
    }
    return target;
}

int main(void) {
    int i, n;
    int * a;
    int * b;
    char line[255];
    char * stopstring, *p;

    fgets(line, 255, stdin);
    n = strtol(line, &stopstring, 10);
    fprintf(stderr, "n = %d\n", n);
    a = calloc(n, sizeof(int));
    b = calloc(n, sizeof(int));
    for (i = 0; i < n; ) {
        int isd, eol;
        while (!(isd = isdigit(*stopstring))
               && !(eol = (*stopstring == '\0') || (*stopstring == '\n'))) {
            fprintf(stderr, "isd = %d, eol = %d\n", isd, eol);
            fprintf(stderr, "*stopstring = %d, stopstring = %s\n", *stopstring, stopstring);
            if(eol)
                fgets(stopstring = line, 255, stdin);
            else
                stopstring++;
        }
        if(eol)
            fgets(stopstring = line, 255, stdin);
        p = stopstring;
        a[i] = strtol(p, &stopstring, 10);
        if (isd)
            i++;
    }
    for (i = 0; i < n - 1; ) {
        printf("&a[%d] = %p, a[%d] = %d\n", i, (a + i), i, a[i]);
    }
    printf("&a[%d] = %p, a[%d] = %d\n", i, (a + i), i, a[i]);
    printf("&b = %p\n", b);

    return 0;
}
