#include <cstdio>
#include <cctype>
#include <cstdlib>

long long merge(int a[], int left[], int right[], int l, int r) {
    long long count = 0;
    int i = 0, j = 0;
    while (i < l || j < r) {
        if (i == l) {
            a[i+j] = right[j];
            j++;
        } else if (j == r) {
            a[i+j] = left[i];
            i++;
        } else if (left[i] <= right[j]) {
            a[i+j] = left[i];
            i++;
        } else {
            a[i+j] = right[j];
            count += l - i;
            j++;
        }
    }
    return count;
}
long long inversions(int a[], int high) {
    if (high < 1)
        return 0;
    int mid = (high + 1) / 2;
    int left[mid];
    int right[high-mid+1];
    int i,j;
    for(i = 0;i < mid; i++)
        left[i] = a[i];
    for(i = high - mid, j = high; j >= mid; i--, j--)
        right[i] = a[j];
    return inversions(left, mid - 1) 
         + inversions(right, high - mid) 
         + merge(a, left, right, mid, high - mid + 1);
}
int main(void) {
    int i, n;
    int * a;
    int * b;
    char line[255];
    char * ss;
    fgets(line, 255, stdin);
    n = strtol(line, &ss, 10);
    a = (int *)calloc(n, sizeof(int));
    for (i = 0; i < n; ) {
        int isd, eol;
        eol = (*ss == '\0') || (*ss == '\n');
        isd = isdigit(*ss);
        while (!isd && !eol) {
            ss++;
            eol = (*ss == '\0') || (*ss == '\n');
            isd = isdigit(*ss);
        }
        if (eol)
            fgets(ss = line, 255, stdin);
        else if (isd)
            a[i++] = strtol(ss, &ss, 10);
    }
    printf("%lld\n",inversions(a,n-1));
    free(a);
    return 0;
}
