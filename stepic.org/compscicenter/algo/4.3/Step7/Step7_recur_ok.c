#include <cctype>
#include <cstdlib>
#include <iostream>

using namespace std;

long long merge(int a[], int left[], int right[], long l, long r) {
    long long count = 0;
    long i = 0, j = 0;
    while (i < l || j < r) {
        if (i == l) {
            a[i + j] = right[j];
            j++;
        } else if (j == r) {
            a[i + j] = left[i];
            i++;
        } else if (left[i] <= right[j]) {
            a[i + j] = left[i];
            i++;
        } else {
            a[i + j] = right[j];
            count += l - i;
            j++;
        }
    }
    return count;
}
long long inversions(int a[], long high) {
    if (high < 1)
        return 0;
    long mid = (high + 1) / 2;
    int left[mid];
    int right[high - mid + 1];
    long i, j;
    for (i = 0; i < mid; i++)
        left[i] = a[i];
    for (i = high - mid, j = high; j >= mid; i--, j--)
        right[i] = a[j];
    return inversions(left, mid - 1) 
         + inversions(right, high - mid) 
         + merge(a, left, right, mid, high - mid + 1);
}
int main(void) {
    long i, n;
    long long r;
    cin >> n;
    int * a = new int[n];
    for (i = 0; i < n; i++)
        cin >> a[i];
    r = inversions(a, n - 1);
    cout << r << endl;
    delete [] a;
    return 0;
}
