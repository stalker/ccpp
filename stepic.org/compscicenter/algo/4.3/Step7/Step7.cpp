#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <algorithm>
using namespace std;
int main(void) {
    unsigned long long i, n, count = 0;
    int * a;
    char line[255];
    char * ss;

    fgets(line, 255, stdin);
    n = strtol(line, &ss, 10);
    a = (int *)calloc(n + 1, sizeof(long long));
    for (i = 0; i < n; ) {
        int isd, eol;
        eol = (*ss == '\0') || (*ss == '\n');
        isd = isdigit(*ss);
        while (!isd && !eol) {
            ss++;
            eol = (*ss == '\0') || (*ss == '\n');
            isd = isdigit(*ss);
        }
        if (eol)
            fgets(ss = line, 255, stdin);
        else if (isd)
            a[i++] = strtol(ss, &ss, 10);
    }
    for (i = 0; i < n; i++)
        for (long long j = i + 1; j < n; j++)
            if (a[i] > a[j]) {
                // swap(a[j], a[j + 1]);
                count++;
            }
    printf("%llu\n", count);
    free(a);
    return 0;
}
