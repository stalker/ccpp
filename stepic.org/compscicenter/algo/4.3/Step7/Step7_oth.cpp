#include <iostream>
#include <clocale>
#include <iomanip>

using namespace std;

void print (int[], int);

int main()
{
	setlocale (LC_CTYPE,"Russian");
	const int arraySize = 9;
	int array[arraySize] = {11, 22, 33, 44, 55, 66, 77, 88, 99};
	int temp, index;
	cout << "Неотсортированный массив: ";
	print(array, arraySize);
	cout << endl;
	for (int i = 0; i < arraySize/2; i++)
	{
		for (int j = arraySize-1; j > arraySize/2; j--)
		{
			temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
	}
	cout << "Отсортированный массив: ";
	print(array, arraySize);
	cout << endl;
	return 0;
}

void print (int massive[], int size)
{
	for (int q = 0; q < size; q++)
	{
		cout << setw(4) << massive[q];
	}
}
