#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<string>
#include<vector>
#include<map>
typedef long long i64;
using namespace std;
class FenwickTree{
    public:
        vector<int> data;
        FenwickTree(int n) : data(n+1 , 0)
        {
        }
        inline void add(int numb,int val){
            for(int i = numb;i<data.size(); i+=i&(-i)){
                data[i]+=val;
            }
        }
        inline int sum_p(int end){
            int res=0;
            for(int i = end;i>0;i-=i&(-i)){
                res+=data[i];
            }
            return res;
        }
        inline int sum_d(int l, int r){
            return sum_p(r)-sum_p(l-1);
        }
};
int main(){
    int i, n;
    char line[255];
    char * stopstring, *p;

    cin.getline(line, 255);
    n = strtol(line, &stopstring, 10);
    while (!isdigit(*stopstring)) {
        stopstring++;
    }
    vector<int> numbs(n);
    for (i = 0; i < n; i++) {
        cin >> numbs[i];
    }
    for(int co=0;co<n;co++){ // количество тестов
        int numb_t,cur;
        i64 res=0;
        vector<int> numb_s=numbs;
        sort(numb_s.begin(),numb_s.end());
        map<int,int> rev;
        for (int i=0;i<numb_t;i++) rev[numb_s[i]] = i+1;
        for (int i=0;i<numb_t;i++) numbs[i] = rev[numbs[i]];
        FenwickTree x(numb_t);
        for (int i=0;i<numb_t;i++){
            int cur = numb_t + 1 - numbs[i];
            res+=x.sum_p(cur - 1);
            x.add(cur,1);
        }
        printf("%lld\n", res);
    }
    return 0;
}
