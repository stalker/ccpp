#ifndef UNSBITSMATRIX_H_INCLUDED
#define UNSBITSMATRIX_H_INCLUDED
#include <climits>

#define MAXSIZE 1000001

class UnsBitsMatrix
{
public:
    // Массив
    char ** data;
    char ** lines;
    // Длина кучи
    int length;
    int data_length;

    // Конструктор
    UnsBitsMatrix(int sz = MAXSIZE)
    {
#if DEBUG > 1
        fprintf(stderr, "UnsBitsMatrix::Constructor\n");
#endif
        data_length = sz / 8 + 1;
        data = new type[data_length*data_length];
    }

    // Деструктор
    ~UnsBitsMatrix(void)
    {
#if DEBUG > 1
        fprintf(stderr, "UnsBitsMatrix::Destructor\n");
#endif
        delete [] data;
    }

    // Перегрузка оператора
    bool& operator[](size_t node)
    {
        return data[node];
    }

    int size(void)
    {
        return length;
    }
};
#endif // UNSBITSMATRIX_H_INCLUDED
