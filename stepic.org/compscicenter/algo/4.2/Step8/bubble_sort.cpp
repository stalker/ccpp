#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int i, n;

    cin >> n;

    int a[n];
    for (i = 0; i < n; i++)
        cin >> a[i];
    for (i = 0; i < n; i++)
        for (int j = 0; j < n - i - 1; j++)
            if (a[j] > a[j + 1])
                swap(a[j], a[j + 1]);

    for (i = 0 ; i < n - 1; i++)
        cout << a[i] << " ";
    cout << a[i] << endl;
}
