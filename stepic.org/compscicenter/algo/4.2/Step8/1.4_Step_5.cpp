#include <math.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#define UP 11

int get_and_work(int n);

int main() {
    int n;
    cin >> n;
    get_and_work(n);
 
  return 0;
}

int get_and_work(int n) {
    int i, j, k, m;
    int a[n];
    int b[UP];

    for (i = 0; i < n; i++)
        cin >> a[i];
    for (j = 0; j < UP; j++)
        b[j] = 0;

    for (j = 0; j < n; j++)
        b[a[j]]++;

    k = 0;
    for (i = 0; i < UP ; i++)
        for (j = 0; j < b[i]; j++)
            a[k++] = i;

    for (i = 0; i < n - 1; i++)
        printf("%d ", a[i]);
    printf("%d\n", a[n - 1]);

    return 0;
}

