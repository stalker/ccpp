#!/usr/bin/perl

my $i;


sub log2 {
    my $n = shift;
    return log($n)/log(2);
}

sub log3 {
    my $n = shift;
    return log($n)/log(3);
}

sub log4 {
    my $n = shift;
    return log($n)/log(4);
}

sub log5 {
    my $n = shift;
    return log($n)/log(5);
}

sub log10 {
    my $n = shift;
    return log($n)/log(10);
}

sub f1 {
    my ($n) = @_;
    return 3**log2($n);
}

sub f2 {
    my ($n) = @_;
    return $n**log2($n);
}

sub f3 {
    my ($n) = @_;
    return 2**$n;
}

sub f4 {
    my ($n) = @_;
    return 4**$n;
}

sub f5 {
    my ($n) = @_;
    return log3($n);
}

sub f6 {
    my ($n) = @_;
    return sqrt($n);
}

sub fact {
    my $i, $r = 1, ($n) = @_;
    for ($i = 1; $i <= $n; $i++) {
        $r = $r * $i;
    }
    return $r;
}

sub f7 {
    my ($n) = @_;
    return log2(fact($n));
}

sub f8 {
    my ($n) = @_;
    return $n**2;
}

sub f9 {
    my ($n) = @_;
    return 7**log2($n);
}
sub f10 {
    my ($n) = @_;
    return 2**(2**$n);
}

sub f11 {
    my ($n) = @_;
    return log2(log2($n));
}

sub f12 {
    my ($n) = @_;
    return (log2($n))**2;
}

sub f13 {
    my ($n) = @_;
    return (log2($n))**(log2($n));
}

sub f14 {
    my ($n) = @_;
    return sqrt(log4($n));
}

sub f15 {
    my ($n) = @_;
    return $n/log5($n);
}

sub f16 {
    my ($n) = @_;
    return fact($n);
}

sub f17 {
    my ($n) = @_;
    return $n**(sqrt($n));
}

for ($i = 9223372036854775000; $i < 9223372036854775806; $i+=1) {
#    printf("%2d: f1=%4.2e f2=%4.2e f3=%4.2e f4=%4.2e f5=%4.2e f6=%4.2e f7=%4.2e f8=%4.2e"
#          ." f9=%4.2e f10=%9.2e f11=%4.2e f12=%4.2e f13=%4.2e f14=%4.2e f15=%4.2e f16=%4.2e f17=%4.2e\n",
#            $i, f1($i), f2($i), 2**$i, 4**$i, f5($i), sqrt($i), f7($i), $i**2,
#            f9($i), f10($i), f11($i), f12($i), f13($i), f14($i), f15($i), fact($i), f17($i));
  printf("%2d: ".
         "f6=%8.2e ".
         "f15=%8.2e".
         "\n",
         $i, f6($i), f15($i));

#9999: f14=2.58e+00 f11=3.73e+00 f5=8.38e+00 f6=1.00e+02 f12=1.77e+02 f15=1.75e+03 f7=     inf f1=2.19e+06 f8=1.00e+08 f9=1.70e+11 f13=8.47e+14 f2=1.41e+53 f17=     inf f3=     inf f4=     inf f16=     inf f10=     inf

# f14=2.58e+00 f11=3.73e+00 f5=8.38e+00 f6=1.00e+02 f12=1.77e+02 f15=1.75e+03 f7=     inf f1=2.19e+06 f8=1.00e+08 f9=1.70e+11 f13=8.47e+14 f2=1.41e+53 f17=     inf f3=     inf f4=     inf f16=     inf f10=     inf

# f14= f11= f5= f6= f12= f15= f7= f1= f8= f9= f13= f2= f17= f3= f4= f16= f10=

# f14= f11= f5= f6= f12= f15= f7= f1=
# f8= f9= f13= f2= f17= f3= f4= f16= f10=

# 14 11 5 6 12 15 7 1 8 9 13 2 17 3 4 16 10

# 14 11 5 6 12 15 7 1
# 8 9 13 2 17 3 4 16 10

}

__END__

1 +
2 +
3 +
4 +
5 +
6 +
7 +
8 +
9 +
10 +
11 +
12 +
13 +
14 +
15 +
16 +
17 +

 f1  3**log2($n);
 f2  $n**log2($n);
 f3  2**$n;
 f4  4**$n;
 f5  log3($n);
 f6  sqrt($n);
 f7  log2(fact($n));
 f8  $n**2;
 f9  7**log2($n);
f10  2**(2**$n);
f11  log2(log2($n));
f12  (log2($n))**2;
f13  (log2($n))**(log2($n));
f14  sqrt(log4($n));
f15  $n/(log5($n));
f16  fact($n);
f17  $n**(sqrt($n));

   2: f14=7.07e-01 f11=0.00e+00 f5=6.31e-01 f6=1.41e+00 f15=4.64e+00 f12=1.00e+00 f7=1.00e+00 f1=3.00e+00 f8=4.00e+00 f9=7.00e+00 f13=1.00e+00 f2=2.00e+00 f17=2.67e+00 f3=4.00e+00 f4=1.60e+01 f16=2.00e+00 f10=1.60e+01
  99: f14=1.82e+00 f11=2.73e+00 f5=4.18e+00 f6=9.95e+00 f15=3.47e+01 f12=4.39e+01 f7=5.18e+02 f1=1.46e+03 f8=9.80e+03 f9=4.00e+05 f13=2.79e+05 f2=1.70e+13 f17=7.18e+19 f3=6.34e+29 f4=4.02e+59 f16=9.33e+155 f10=     inf
 101: f14=1.82e+00 f11=2.74e+00 f5=4.20e+00 f6=1.00e+01 f15=3.52e+01 f12=4.43e+01 f7=5.31e+02 f1=1.50e+03 f8=1.02e+04 f9=4.23e+05 f13=3.03e+05 f2=2.21e+13 f17=1.39e+20 f3=2.54e+30 f4=6.43e+60 f16=9.43e+159 f10=     inf
 170: f14=1.92e+00 f11=2.89e+00 f5=4.67e+00 f6=1.30e+01 f15=5.33e+01 f12=5.49e+01 f7=1.02e+03 f1=3.43e+03 f8=2.89e+04 f9=1.83e+06 f13=2.78e+06 f2=3.36e+16 f17=1.21e+29 f3=1.50e+51 f4=2.24e+102 f16=7.26e+306 f10=     inf
 199: f14=1.95e+00 f11=2.93e+00 f5=4.82e+00 f6=1.41e+01 f15=6.05e+01 f12=5.83e+01 f7=     inf f1=4.40e+03 f8=3.96e+04 f9=2.84e+06 f13=5.53e+06 f2=3.59e+17 f17=2.69e+32 f3=8.03e+59 f4=6.46e+119 f16=     inf f10=     inf
 201: f14=1.96e+00 f11=2.94e+00 f5=4.83e+00 f6=1.42e+01 f12=5.85e+01 f15=6.10e+01 f7=     inf f1=4.47e+03 f8=4.04e+04 f9=2.92e+06 f13=5.77e+06 f2=4.19e+17 f17=4.50e+32 f3=3.21e+60 f4=1.03e+121 f16=     inf f10=     inf
 999: f14=2.23e+00 f11=3.32e+00 f5=6.29e+00 f6=3.16e+01 f12=9.93e+01 f15=2.33e+02 f7=     inf f1=5.68e+04 f8=9.98e+05 f9=2.64e+08 f13=8.89e+09 f2=7.74e+29 f17=6.41e+94 f3=5.36e+300 f4=     inf f16=     inf f10=     inf
1000: f14=2.23e+00 f11=3.32e+00 f5=6.29e+00 f6=3.16e+01 f12=9.93e+01 f15=2.33e+02 f7=     inf f1=5.69e+04 f8=1.00e+06 f9=2.64e+08 f13=8.93e+09 f2=7.90e+29 f17=7.38e+94 f3=1.07e+301 f4=     inf f16=     inf f10=     inf
9999: f14=2.58e+00 f11=3.73e+00 f5=8.38e+00 f6=1.00e+02 f12=1.77e+02 f15=1.75e+03 f7=     inf f1=2.19e+06 f8=1.00e+08 f9=1.70e+11 f13=8.47e+14 f2=1.41e+53 f17=     inf f3=     inf f4=     inf f16=     inf f10=     inf
