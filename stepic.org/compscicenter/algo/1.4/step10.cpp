#include <stdio.h>
#include <iostream>
using namespace std;

int get_and_work(int n);
int binsearch(int x, int v[], int n);

int main() {
    int n;
    cin >> n;
    get_and_work(n);
    return 0;
}

int get_and_work(int n) {
    int a[n];
    int i, k, m;
    for (i = 0; i < n; i++)
        cin >> a[i];
    cin >> k;
    int b[k];
    for (i = 0; i < k; i++)
        cin >> b[i];
    for (i = 0; i < k - 1; i++) {
        if ((m = binsearch(b[i], a, n)) != -1)
            printf("%d ", m + 1);
        else
            printf("%d ", m);
    }
    if ((m = binsearch(b[k - 1], a, n)) != -1)
        printf("%d\n", m + 1);
    else
        printf("%d\n", m);
    return 0;
}

int binsearch(int x, int v[], int n)
{
  int low, high, mid;
  low = 0;
  high = n - 1;
  while (low <= high) {
    mid = (low + high) / 2;
    if (x < v[mid])
      high = mid - 1;
    else if (x > v[mid])
      low = mid + 1;
    else
      return mid;  /* совпадение найдено */
  }
  return -1;       /* совпадений нет */
}
