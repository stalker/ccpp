#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int main() {
    int i, n;

    cin >> n;

    int a;
    unordered_map<int, int> result;
    for (i = 0; i < n; i++)
        cin >> a[i], result[a] = i;
    sort(a, a + n);
  
    int k;
    cin >> k;
    for (i = 0; i < k; i++) {
        int b;
        cin >> b;
        if (result.count(b))
            cout << result[b] + 1 << endl;
        else
            cout << -1 << endl;
    }
    return 0;
}
