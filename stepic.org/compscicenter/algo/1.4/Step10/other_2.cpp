#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int i, n;

    cin >> n;

    int a[n];
    for (i = 0; i < n; i++)
        cin >> a[i];
    sort(a, a + n);
  
    int k;
    cin >> k;
    for (i = 0; i < k; i++) {
        int b;
        cin >> b;
        
        int j = lower_bound(a, a + n, b) - a;
        cout << (j == n || a[j] != b ? -1 : j + 1) << endl;
    }
    return 0;
}
