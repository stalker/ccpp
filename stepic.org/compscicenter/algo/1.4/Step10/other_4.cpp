#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int main() {
    int a, b, i, k, n;

    ios_base::sync_with_stdio(0);
    cin >> n;
    unordered_map<int, int> result;
    for (i = 0; i < n; i++)
        cin >> a, result[a] = i;
  
    cin >> k;
    for (i = 0; i < k; i++) {
        cin >> b;
        if (result.count(b))
            cout << result[b] + 1 << endl;
        else
            cout << -1 << endl;
    }
    return 0;
}
