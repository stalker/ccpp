/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */

/**
 * Найдите минимальное вершинное покрытие в двудольном графе.
 *
 * На вход подается описание двудольного графа, в котором доли уже выделены
 * явно. Первая строка содержит три натуральных числа: v1 < 100 — число вершин
 * первой доли, v2 < 100 — число вершин второй доли, e ≤ v1∗v2 — число рёбер.
 * Подразумевается, что первая доля состоит из вершин с номерами от 0 до v1 − 1,
 * вторая — из вершин с номерами от v1 до v1 + v2 − 1.
 * Следующие e строк описывают рёбра: каждая из этих строк содержит два числа:
 * 0 ≤ ui < v1 и v1 ≤ wi < v1 + v2, что означает, что между вершинами ui и wi
 * есть ребро.
 *
 * Скопируйте описание графа из входа на выход и выведите единственную
 * дополнительную строку — список номеров вершин, составляющих минимальное
 * вершинное покрытие. Если таких покрытий несколько, выведите любое.
 */

#include <cstdlib>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <vector>
#include <iostream>
#include <algorithm>

// #define DEBUG 1

typedef int t_timer;
typedef int t_vertex;
#define NOVERTEX -1

typedef std::set< t_vertex > set_vertex;
typedef std::set< t_vertex >::iterator itr_set_vertex;

typedef std::stack< t_vertex > stack_vertex;
typedef std::deque< t_vertex > deque_vertex;
typedef std::deque< t_vertex >::iterator itr_deque_vertex;
typedef std::deque< t_vertex >::reverse_iterator rit_deque_vertex;

typedef std::map< t_vertex, bool > map_vertex_bool;
typedef std::map< t_vertex, bool >::iterator itr_map_vertex_bool;
typedef std::map< t_vertex, char > map_vertex_char;
typedef std::map< t_vertex, char >::iterator itr_map_vertex_char;
typedef std::map< t_vertex, size_t > map_vertex_sizet;
typedef std::map< t_vertex, t_timer > map_vertex_timer;
typedef std::map< t_vertex, std::set<t_vertex> > map_vertex_set;
typedef std::map< t_vertex, std::set<t_vertex> >::iterator
            itr_map_vertex_set;
typedef std::set<t_vertex>::iterator itr_set_vertex;

typedef std::vector<t_vertex> vector_vertex;
typedef std::vector<t_vertex>::iterator itr_vector_vertex;
typedef std::map< t_vertex, vector_vertex> map_vertex_vector;
typedef std::map< t_vertex, vector_vertex>::iterator
            itr_map_vertex_vector;

typedef map_vertex_vector       adjacency_lst;
typedef itr_map_vertex_vector   adjacency_itr;
#define adj_itr                 adjacency_itr
typedef itr_vector_vertex       adjacency_lst_itr;
#define adjlst_itr              adjacency_lst_itr
// set typedef itr_set_vertex   vertex_lst_itr;
typedef itr_vector_vertex       vertex_lst_itr;
typedef map_vertex_sizet        sizet_lst;
typedef map_vertex_timer        timer_lst;
typedef map_vertex_char         part_lst;
typedef itr_map_vertex_char     part_itr;
typedef map_vertex_bool         used_lst;
typedef itr_map_vertex_bool     used_itr;
typedef std::vector<bool>       marked_lst;

#define Beg(X) (X).begin()
#define End(X) (X).end()

const bool WHITE = false;
const bool BLACK = true;
const char * sp = " ";

namespace graph {

using namespace std;

adjacency_lst& create(istream& stm, adjacency_lst& g, used_lst& u) {
    t_vertex a, b;
    while (stm >> a >> b) {
        if (a != b && find(Beg(g[a]), End(g[a]), b) == End(g[a])) {
            g[a].push_back(b);
            g[b].push_back(a);
            u[a] = u[b] = false;
        }
        std::cout << a << sp << b << std::endl;
    }
    return g;
}

class Graph {
public:
    virtual ~Graph() {}
};


class Visited : public Graph {
protected:
    used_lst used;
};

class Adjacency_list : public Visited {
public:
    Adjacency_list(istream& stm) {
        create(stm, this->g, this->used);
    }
protected:
    using Visited::used;
    adjacency_lst g;
};

class Dfs_adjacency_list : public Adjacency_list {
public:
    virtual void dfs() {
        for (used_itr use = Beg(used); use != End(used); ++use) {
            if (not use->second) {
#if defined(NORECURSION)
                dfs_norecursion(use->first);
#else
                dfs_recursion(use->first, NOVERTEX);
#endif
            }
        }
    }
protected:
    t_vertex not_used_vertex(t_vertex v) {
        for (vertex_lst_itr to = Beg(this->g[v]); to != End(this->g[v]); ++to) {
            if (not this->used[*to]) {
                return *to;
            }
        }
        return NOVERTEX;
    }
    virtual void dfs_recursion(t_vertex, t_vertex);
    virtual void dfs_norecursion(t_vertex);
    using Visited::used;
    using Adjacency_list::g;
};

void Dfs_adjacency_list::dfs_recursion(t_vertex cur, t_vertex prev) {
    // cerr << "AdjacencyLst::dfs_recursion" << endl;
    if (this->used[cur]) {
        return;
    }
    used[cur] = true;
    for (adjlst_itr to = Beg(this->g[cur]); to != End(this->g[cur]); ++to) {
        if (*to == prev) {
            continue;
        }
        if (not this->used[*to]) {
            dfs_recursion(*to, cur);
        }
    }
}

void Dfs_adjacency_list::dfs_norecursion(t_vertex start) {
    // cerr << "AdjacencyLst::dfs_norecursion" << endl;
    if (this->used[start]) {
        return;
    }
    this->used[start] = true;
    stack_vertex stck;
    stck.push(start);
    while (not stck.empty()) {
        t_vertex curr = stck.top(), next;
        if ((next = not_used_vertex(curr)) != NOVERTEX) {
            if (curr != next) {
                curr = next;
                stck.push(curr);
                this->used[curr] = true;
            }
        } else {
            stck.pop();
        }
    }
}

/**
 *  The <tt>Bipartite_matching</tt> class represents a data type for computing
 *  a <em>maximum (cardinality) matching</em> and
 *  a <em>minimum (cardinality) vertex cover</em> in a bipartite graph.
 *  A <em>bipartite graph</em> in a graph whose vertices can be partitioned
 *  into two disjoint sets such that every edge has one endpoint in either set.
 *  A <em>matching</em> in a graph is a subset of its edges with no common
 *  vertices. A <em>maximum matching</em> is a matching with the maximum number
 *  of edges.
 *  A <em>perfect matching</em> is a matching which matches all vertices in
 *  the graph.
 *  A <em>vertex cover</em> in a graph is a subset of its vertices such that
 *  every edge is incident to at least one vertex.
 *  A <em>minimum vertex cover</em> is a vertex cover with the minimum number
 *  of vertices.
 *  By Konig's theorem, in any biparite
 *  graph, the maximum number of edges in matching equals the minimum number
 *  of vertices in a vertex cover.
 *  The maximum matching problem in <em>nonbipartite</em> graphs is
 *  also important, but all known algorithms for this more general problem
 *  are substantially more complicated.
 *  <p>
 *  This implementation uses the <em>alternating path algorithm</em>.
 *  It is equivalent to reducing to the maximum flow problem and running
 *  the augmenting path algorithm on the resulting flow network, but it
 *  does so with less overhead.
 *  The order of growth of the running time in the worst case is
 *  (<em>E</em> + <em>V</em>) <em>V</em>,
 *  where <em>E</em> is the number of edges and <em>V</em> is the number
 *  of vertices in the graph. It uses extra space (not including the graph)
 *  proportional to <em>V</em>.
 *  <p>
 *  See also {@link HopcroftKarp}, which solves the problem in
 *  O(<em>E</em> <em>V</em>^1/2)
 *  using the Hopcroft-Karp algorithm and
 *  {@link BipartiteMatchingToMaxflow}, which solves the problem in
 *  O(<em>E V</em>) time via a reduction to maxflow.
 *  <p>
 *  For additional documentation, see
 *  <a href="http://algs4.cs.princeton.edu/65reductions">Section 6.5</a>
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 */
class Bipartite_matching : Adjacency_list {
public:
    Bipartite_matching(istream& stm, t_vertex n, t_vertex m = 0)
    : Adjacency_list(stm), v1(n), v2(m), V(n + m), q(V), mate(V, NOVERTEX)
      , edge_to(V, NOVERTEX), marked(V, false), in_min_vertex_cover(V, false)
    {
        is_bipart = false;
        cardinality = 0;
        for (adj_itr it = Beg(g); it != End(g); ++it) {
            this->part[it->first] = NOVERTEX;
        }
    }

    /**
     * BFS.
     * По окончании работы алгоритма мы либо обнаружим, что граф не двудолен,
     * либо найдём разбиение вершин графа на две доли в part.
     */
    void bipartition();

    /**
     * Returns true if the graph is bipartite.
     * @return true if the graph is bipartite; false otherwise
     */
    bool is_bipartite() {
        return this->is_bipart;
    }

    /**
     * Returns the side of the bipartite that vertex v is on.
     * @param  v the vertex
     * @return the side of the bipartition that vertex v is on; two vertices
     *         are in the same side of the bipartition if and only if they have
     *         the same color.
     */
    bool color(t_vertex v) {
        return this->part[v] != 0;
    }

    /**
     * Returns true if the specified vertex is matched in the maximum matching.
     * @param  v the vertex
     * @return true if vertex v is matched in maximum matching; false otherwise
     */
    bool is_matched(t_vertex v) {
        return this->mate[v] != NOVERTEX;
    }

    // is the edge v-w a forward edge not in the matching or a reverse edge
    // in the matching?
    bool is_residual_graph_edge(t_vertex v, t_vertex w) {
        if ((this->mate[v] != w) && color(v)) return true;
        if ((this->mate[v] == w) && not color(v)) return true;
        return false;
    }

    /**
     * This implementation finds a shortest augmenting path
     * (fewest number of edges), though there
     * is no particular advantage to do so here.
     */
    bool has_augmenting_path();

    /**
     * Determines a maximum matching (and a minimum vertex cover)
     * in a bipartite graph.
     * @param  G the bipartite graph
     */
    void bipartite_matching();

    void out_min_vertex_cover(ostream& stm);
private:
    t_vertex v1, v2;
    const t_vertex V;
    vector_vertex q;
    vector_vertex mate;
    vector_vertex edge_to;
    marked_lst marked;
    marked_lst in_min_vertex_cover;
    part_lst part;
    bool is_bipart;
    int cardinality;
};

/**
 * BFS.
 * Произведём серию поисков в ширину. Т.е. будем запускать поиск в ширину
 * из каждой непосещённой вершины. Ту вершину, из которой мы начинаем идти,
 * мы помещаем в первую долю. В процессе поиска в ширину, если мы идём в
 * какую-то новую вершину, то мы помещаем её в долю, отличную от доли
 * текущей вершину. Если же мы пытаемся пройти по ребру в вершину, которая
 * уже посещена, то мы проверяем, чтобы эта вершина и текущая вершина
 * находились в разных долях. В противном случае граф двудольным
 * не является.
 *
 * По окончании работы алгоритма мы либо обнаружим, что граф не двудолен,
 * либо найдём разбиение вершин графа на две доли.
 */
void Bipartite_matching::bipartition() {
    this->is_bipart = true;
    for (part_itr st = Beg(this->part); st != End(this->part); ++st) {
        t_vertex part_idx = st->first;
        char & part_value = st->second;
        if (part_value == NOVERTEX) {
            t_vertex h = 0, t = 0;
            q[t++] = part_idx;
            this->part[part_idx] = 0;
            while (h < t) {
                t_vertex v = q[h++];
                for (adjlst_itr to = Beg(this->g[v]);
                                to != End(this->g[v]); ++to
                ) {
                    if (this->part[*to] == NOVERTEX) {
                        this->part[*to] = not this->part[v], q[t++] = *to;
                    } else {
                        this->is_bipart &= this->part[*to] != this->part[v];
                    }
                } // for adjacency_lst_itr to
            } // while h < t
        }
    } // for part_itr st
    // std::cout << (this->is_bipart ? "YES" : "NO") << std::endl;
}

// is there an augmenting path?
// an alternating path is a path whose edges belong alternately to
// the matching and not to the matching an augmenting path is
// an alternating path that starts and ends at unmatched vertices
//
// if so, upon termination edgeTo[] contains a parent-link representation
// of such a path if not, upon terminatation marked[] specifies the subset
// of vertices reachable via an alternating path from one side of
// the bipartition
//
// this implementation finds a shortest augmenting path
// (fewest number of edges), though there
// is no particular advantage to do so here
bool Bipartite_matching::has_augmenting_path() {
    deque_vertex que;
    // breadth-first search
    // starting from all unmatched vertices on one side of bipartition
    for (t_vertex v = 0; v < this->V; ++v) {
        this->edge_to[v] = NOVERTEX;
        this->marked[v] = false;
        // either (1) forward edge not in matching or (2) backward edge
        // in matching
        if (color(v) && not is_matched(v)) {
            que.push_back(v);
            this->marked[v] = true;
        }
    }
    // run BFS, stopping as soon as an alternating path is found
    while (not que.empty()) {
        t_vertex v = que.back(); que.pop_back();
        for (adjlst_itr w = Beg(this->g[v]); w != End(this->g[v]); ++w) {
            if (is_residual_graph_edge(v, *w)) {
                if (not this->marked[*w]) {
                    this->edge_to[*w] = v;
                    this->marked[*w] = true;
                    if (not is_matched(*w)) return true;
                    que.push_back(*w);
                }
            } // if is_residual_graph_edge v, w
        } // for w in g[v]
    } // while not que.empty()
    return false;
}

/**
 * Determines a maximum matching (and a minimum vertex cover)
 * in a bipartite graph.
 * @param  G the bipartite graph
 */
void  Bipartite_matching::bipartite_matching() {
    if (not this->is_bipart) return;
    // alternating path algorithm
    while (has_augmenting_path()) {
        // find one endpoint t in alternating path
        t_vertex t = NOVERTEX;
        for (t_vertex v = 0; v < V; ++v) {
            if (!is_matched(v) && this->edge_to[v] != NOVERTEX) {
                t = v;
                break;
            }
        } // for v in g
        // update the matching according to alternating path
        // in edge_to[] array
        for (t_vertex v = t; v != NOVERTEX; v = this->edge_to[this->edge_to[v]])
        {
            t_vertex w = edge_to[v];
            this->mate[v] = w;
            this->mate[w] = v;
        } // for v from t while v != NOVERTEX
        this->cardinality++;
    } // while is_residual_graph_edge() == true
    for (t_vertex v = 0; v < V; ++v) {
        if (color(v) && not this->marked[v]) {
            this->in_min_vertex_cover[v] = true;
        }
        if (not color(v) && this->marked[v]) {
            this->in_min_vertex_cover[v] = true;
        }
    }
}

void Bipartite_matching::out_min_vertex_cover(ostream& stm) {
    for (t_vertex v = 0; v < V; ++v) {
        if (this->in_min_vertex_cover[v]) {
            stm << v << sp;
        }
    }
    stm << std::endl;
}

} // namespace graph

/* let's go */
int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(0);
    int a, b, c;
    std::cin >> a >> b >> c;
    std::cout << a << sp << b << sp << c << std::endl;
    using namespace graph;
    Bipartite_matching* g = new Bipartite_matching(std::cin, a, b);
    g->bipartition();
    g->bipartite_matching();
    g->out_min_vertex_cover(std::cout);
    delete g;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
