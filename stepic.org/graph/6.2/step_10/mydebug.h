#ifndef MYDEBUG_H
#define MYDEBUG_H

#define cerr_FILE_LINE cerr<<__FILE__<<':'<<__LINE__
#define cerr_FILE_LINE_nl cerr_FILE_LINE<<endl
#define cerr_func_X(x) cerr<<':'<<__func__<<':'<<#x
#define cerr_func_X_nl(x) cerr<<':'<<__func__<<':'<<#x<<endl

#define cerr_X_V(x,v) cerr<<#x<<(v)
#define cerr_X_V_nl(x,v) cerr_X_V(x,v)<<endl
#define cerr_func_X_V(x,v) cerr<<':'<<__func__<<':';cerr_X_V(x,v)
#define cerr_func_X_V_nl(x,v) cerr_func_X_V(x,v)<<endl

#define cerr_X_V_Y(x,v,y) cerr<<#x<<(v)<<#y
#define cerr_X_V_Y_nl(x,v,y) cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)
#define cerr_func_X_V_Y_nl(x,v,y) cerr<<':'<<__func__<<':';cerr_X_V_Y(x,v,y)<<endl

#define cerr_func_V_X(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x
#define cerr_func_V_X_nl(v,x) cerr<<':'<<__func__<<':'<<(v)<<#x<<endl
#define cerr_X(x) cerr<<#x
#define cerr_X_nl(x) cerr<<#x<<endl

#endif // MYDEBUG_H
