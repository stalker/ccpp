/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

/**
 * Докажите, что k-мерный гиперкуб k-связен с помощью теоремы Менгера.
 *
 * k-мерным гиперкубом называется граф, в котором вершины — битовые строки
 * длины k, а рёбра проведены между теми парами вершин, которые отличаются
 * ровно в одном бите. На вход подаётся две различные битовых строки A и B
 * длины k<100 через пробел. Выведите k простых путей в k-мерном гиперкубе
 * из A в B, не пересекающихся по внутренним вершинам. Формат вывода: один
 * путь на строку; путь – последовательности битовых строк, разделённая
 * пробелами.
 *
 * Sample Input 1:
 * 1010 0010
 *
 * Sample Output 1:
 * 1010 0010
 * 1010 1011 0011 0010
 * 1010 1000 0000 0010
 * 1010 1110 0110 0010
 *
 * Sample Input 2:
 * 0 1
 *
 * Sample Output 2:
 * 0 1
 */

#include <cstdlib>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstddef>
#include <cstdio>
#include <string>
#include <map>
#include <stack>
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>

// #define DEBUG 8

typedef std::pair<int, int> ipair;
typedef std::stack<ipair> pstack;
typedef std::vector<bool> array_bool;
typedef std::vector<array_bool> matrix_bool;

typedef array_bool vertex;
typedef matrix_bool array_vertex;
typedef std::vector<array_vertex> result_matrix;

using std::string;
using std::vector;
using namespace std;

pstack * vertex_map;

ostream& print_vertex(ostream& stm, vertex& ab) {
    vertex out = ab;
    if (NULL != vertex_map) {
#if defined(DEBUG) && DEBUG > 7
        cerr << __func__ << " ";
        for (int i = 0; i < ab.size(); ++i) {
            cerr << out[i];
        }
#endif
        pstack stk = (*vertex_map);
        while (not stk.empty())
        {
            ipair i = stk.top();
#if defined(DEBUG) && DEBUG > 7
            cerr << " swap: " << i.first << " " << i.second << " ";
#endif
            bool t = out[i.first];
            out[i.first] = out[i.second];
            out[i.second] = t;
            stk.pop();
        }
#if defined(DEBUG) && DEBUG > 7
        for (int i = 0; i < ab.size(); ++i) {
            cerr << out[i];
        }
        cerr << " " << endl;
#endif
    }
    for (int i = 0; i < ab.size(); ++i) {
        stm << out[i];
    }
    return stm;
}

ostream& print_result_matrix(ostream& stm, result_matrix& r) {
    for (int i = 0; i < r.size(); ++i) {
        for (int j = 0; j < r[i].size(); ++j) {
            print_vertex(stm, r[i][j]) << " ";
        }
        stm << endl;
    }
    return stm;
}

bool find_in_result(result_matrix& result, vertex& curr, int k) {
    for (int i = 0; i < result.size(); ++i) {
        for (int j = 0; j < k; ++j) {
            if (result[i][j] == curr)
                return true;
        }
    }
    return false;
}

void bit_reverse(array_vertex& arr, int idx_vertex, int idx_bit,
                 string sep = " ") {
#if defined(DEBUG) && DEBUG > 8
    cerr << __func__ << ": ";
    print_vertex(cerr, arr[idx_vertex]) << " ";
#endif
    arr[idx_vertex][idx_bit] = not arr[idx_vertex][idx_bit];
#if defined(DEBUG) && DEBUG > 8
    cerr << __func__ << ".arr[" << idx_vertex << "][" << idx_bit << "] = ";
    print_vertex(cerr, arr[idx_vertex]) << sep;
#endif
}

void build0(result_matrix& result, vertex& start, int h, vertex& end) {
    int n = start.size();
    array_vertex * next = new array_vertex;
    next->resize(h + 1, vertex(start));
    (*next)[h] = end;
#if defined(DEBUG) && DEBUG > 8
            cerr << __func__
                 << ":n=" << n
                 << ";h=" << h
                 << endl;
#endif
    int count = 0;
    for (int path = 0; path < h; ++path, ++count) {
        for (int vert = 1; vert < h; ++vert) {
            (*next)[vert] = (*next)[vert - 1];
            int bit = n - h + (vert + count - 1) % h;
#if defined(DEBUG) && DEBUG > 8
            cerr << __func__
                 << ":path=" << path
                 << ";vert=" << vert
                 << ";bit="  << bit << endl;
#endif
            bit_reverse(*next, vert, bit);
        }
#if defined(DEBUG) && DEBUG > 8
        cerr << "result: " << endl;
        print_result_matrix(cout, result);
#endif
        result.push_back(*next);
#if defined(DEBUG) && DEBUG > 8
        cout << "result: " << endl;
        print_result_matrix(cout, result);
#endif
    }
    delete next;
}

void build1(result_matrix& result, vertex& start, int h, vertex& end) {
    int n = start.size();
    int h2 = h + 2;
    array_vertex * next = new array_vertex;
    next->resize(h2 + 1, vertex(start));
    (*next)[h2] = end;
#if defined(DEBUG) && DEBUG > 8
    cerr << __func__
         << ":n=" << n
         << ";h=" << h
         << ";h2=" << h2
         << endl;
#endif
    int count = 0;
    for (int path = result.size(); path < n; ++path, ++count) {
        int bit = count;
        (*next)[1] = (*next)[0];
        bit_reverse(*next, 1, bit);
        for (int vert = 2; vert < h2; ++vert) {
#if defined(DEBUG) && DEBUG > 8
            cerr << __func__ << ":path=" << path;
            cerr << ";h=" << h << ";h2=" << h2;
#endif
#if defined(DEBUG) && DEBUG > 8
            cerr << ";left_vert=" << vert << ";bit=" << bit;
#endif

            (*next)[vert] = (*next)[vert - 1];
            bit = n - h + (vert + count - 2) % h;
#if defined(DEBUG) && DEBUG > 8
            cerr << ";right_vert=" << vert << endl;
#endif
            bit_reverse(*next, vert, bit);
        }
        if (0 == h2 % 2) {
            int middle = h2 / 2;
            int bit = n - h + (middle + count - 1) % h;
            (*next)[middle] = (*next)[middle - 1];
            bit_reverse(*next, middle, bit);
        }
        result.push_back(*next);
    }
    delete next;
}

pstack * generality(vertex& start, vertex& end, int h) {
    pstack * result = new pstack;
    if (h == start.size()) {
        return result;
    }
    for (int i = 0; i < start.size(); ++i) {
        if (start[i] != end[i]) {
            for (int j = start.size() - 1; j > i; --j) {
                if (start[j] == end[j]) {
#if defined(DEBUG) && DEBUG > 7
                    cerr << "swap: " << i << " " << j << endl;
#endif
                    bool t = start[i];
                    start[i] = start[j];
                    start[j] = t;
                    t = end[i];
                    end[i] = end[j];
                    end[j] = t;
                    result->push(make_pair(i, j));
                }
            }
        }
    }
    return result;
}

int hamming(vertex& start, vertex& end) {
    int result = 0;
    for (int i = 0; i < start.size(); ++i) {
        if (start[i] != end[i]) {
            result++;
        }
    }
    return result;
}

vertex * get_vertex(istream& in) {
    int c;
    vertex * vert = new vertex;
    while (not in.eof()) {
        c = in.get();
        if ( c == ' ' || c == '\t' || c == '\n' ) {
            break;
        }
        if ('0' == c) {
            vert->push_back(false);
        }
        if ('1' == c) {
            vert->push_back(true);
        }
    }
    return vert;
}

/* let's go */
int main(int argc, char * argv[]) {
    ios_base::sync_with_stdio(0);
    // vertex start = {1, 0, 0, 0};
    // vertex end   = {1, 1, 1, 1};
    vertex * start = get_vertex(cin); // new start
    vertex * end   = get_vertex(cin); // new end
#if defined(DEBUG) && DEBUG > 7
    cerr << "start = ";
    print_vertex(cerr, *start) << endl;
    cerr << "end   = ";
    print_vertex(cerr, *end) << endl;
#endif
    result_matrix result;
    int h = hamming(*start, *end);
    vertex_map = NULL;
    vertex_map = generality(*start, *end, h); // new vertex_map
#if defined(DEBUG) && DEBUG > 7
    cerr << "start = ";
    print_vertex(cerr, *start) << endl;
    cerr << "end   = ";
    print_vertex(cerr, *end) << endl;
#endif
    build0(result, *start, h, *end);
    build1(result, *start, h, *end);
#if defined(DEBUG) && DEBUG > 8
    cerr << "result: " << endl;
#endif
    print_result_matrix(cout, result);
    delete vertex_map;
    delete end;
    delete start;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
