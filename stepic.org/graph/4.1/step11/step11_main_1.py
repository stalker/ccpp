#!/usr/bin/env python
#-*- encoding: utf-8 -*-

import sys, warnings

warnings.simplefilter('always')

def make_nCube(k):
    res = {}
    for n in range(2**k):
        res[n] = [n^(2**x) for x in range(k)]
    return res

def int2bin(n, l):
    s = bin(n).replace("0b", "")
    for i in range(len(s), l):
        s = "0" + s
    return s

def main(argv=sys.argv):
    n = int(input())
    ncube = make_nCube(n)
    for i in range(len(ncube)):
        for j in range(len(ncube[i])):
            sys.stdout.write("%0s " % (int2bin(ncube[i][j], n)))
            sys.stdout.flush()
        print
    pass

if __name__ == "__main__":
    sys.exit(main())
# vim: syntax=python:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et

