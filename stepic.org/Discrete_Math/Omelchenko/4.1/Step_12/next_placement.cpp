#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
 
using namespace std;
 
string letters = "ABCDEFG";
bool next_placement(string &perm, int m) {
    reverse(perm.begin()+m, perm.end());
    return next_permutation(perm.begin(), perm.end());
}
int main() {
    // freopen("input.txt","r", stdin);
    // freopen("output.txt","w",stdout);
    int n,m;
    cin>>n>>m;
 
    string perm = letters.substr(0,n);
 
    char mask[10];
    sprintf(mask,"%%.%ds\n",m); /* для m = 2 mask = "%.2s\n" */
    do {
        printf(mask, perm.c_str()); // выводим только первые m символов
    } while(next_placement(perm,m));
 
    return 0;
} 
