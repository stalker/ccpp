/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Нужно сгенерировать все возможные k-перестановки n-элементов без
 * повторений.
 * Формат входные данные:
 * Два числа n и k через пробел. Для них гарантированно выполняется
 * условие: 0 < k ≤ n.
 * 
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из
 * которых содержится k чисел от 0 до n, разделенных пробелом. 
 */


#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "step_12.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItrVector;
typedef vector<int>::const_iterator CItVector;

int n, m;

string letters = "0123456789";

bool next_placement(string &perm, int m) {
    reverse(perm.begin() + m, perm.end());
    return next_permutation(perm.begin(), perm.end());
}

int main() {
    int n, m;
    cin >> n >> m;
 
    string perm = letters.substr(0, n);
 
    do {
        string::iterator it1 = perm.begin();
        string::iterator end = perm.end();
        for (int i = 0; i < m; ++i, ++it1) {
            cout << *it1
                 << ((it1 == (end - 1)) ? "" : " ");
        }
        cout << endl;
    } while(next_placement(perm, m));
 
    return 0;
} 

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
