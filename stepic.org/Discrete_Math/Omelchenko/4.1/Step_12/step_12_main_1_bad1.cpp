/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Нужно сгенерировать все возможные k-перестановки n-элементов без
 * повторений.
 * Формат входные данные:
 * Два числа n и k через пробел. Для них гарантированно выполняется
 * условие: 0 < k ≤ n.
 * 
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из
 * которых содержится k чисел от 0 до n, разделенных пробелом. 
 */

#include <cstdlib>

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstddef>
#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "step_12.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

typedef vector<int> Vector;

/* let's go */
int main(int argc, char *argv[]) {
    int n, m, i, j, k;
    cin >> n >> m;
    Vector v(m, 0);
    auto it1 = v.begin();
    auto end = v.cend();
    for (i = 0; i != n; ++i) {
        it1 = v.begin();
        *it1 = i;
        for (j = 0; j != n; ++j) {
            it1 = v.begin() + 1;
            if (i == j)
                continue;
            for (k = j;it1 != end; ++k, ++it1) {
                if (k == i)
                    k++;
                *it1 = k;
            }
            it1 = v.begin();
            for (; it1 != end; ++it1) {
                cout << *it1
                     << ((it1 == (end - 1)) ? "" : " ");
            }
            cout << endl;
        }
    }
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
