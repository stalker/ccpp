/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */
/* Нужно сгенерировать все возможные k-перестановки n-элементов без
 * повторений.
 * Формат входные данные:
 * Два числа n и k через пробел. Для них гарантированно выполняется
 * условие: 0 < k ≤ n.
 *
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из
 * которых содержится k чисел от 0 до n, разделенных пробелом. 
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "step_12.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;

bool next_placement_vec(IntVector & v, int m) {
    reverse(v.begin() + m, v.end());
    return next_permutation(v.begin(), v.end());
}

int n, m;

int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cin >> n >> m;

    IntVector v(n, 0);
    ItIVector it1 = v.begin();
    ItIVector end = v.end();
    for (int i = 0; it1 != end; ++i, ++it1)
        *it1 = i;

    do {
        it1 = v.begin();
        for (int i = 0; i < m and it1 != end; ++i, ++it1) {
            cout << *it1
                 << ((it1 == (end - 1))
                      || (i == (m - 1))
                     ? "" : " " );
        }
        cout << endl;
    } while (next_placement_vec(v, m));

    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
