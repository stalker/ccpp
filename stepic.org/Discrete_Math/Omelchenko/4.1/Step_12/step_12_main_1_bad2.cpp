#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>

using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItrVector;

int n, m;

void inc(int & k, int i = -1) {
    k++;
    if (k == n) {
        if (0 == i)
            k = 1;
        else
            k = 0;
    }
    return;
}

int main() {
    int i, j, k;
    cin >> n >> m;
    IntVector v(m, 0);
    ItrVector it1 = v.begin();
    ItrVector end = v.end();
    for (i = 0; i != n; ++i) {
        it1 = v.begin();
        *it1 = i;
        for (j = 0; j != n; ++j) {
            it1 = v.begin() + 1;
            if (i == j)
                continue;
            for (k = j;it1 != end; inc(k), ++it1) {
                if (k == i)
                    inc(k, i);
                *it1 = k;
            }
            it1 = v.begin();
            for (; it1 != end; ++it1) {
                cout << *it1
                     << ((it1 == (end - 1)) ? "" : " ");
            }
            cout << endl;
        }
    }
    return EXIT_SUCCESS;
}
