#include <iostream>
#include <cstdio>
#include <vector>
 
using namespace std;
vector<int> cur;
bool next_placement_rep(vector<int> &cur, int n, int k) {
    int r = 1;
    int pos = n-1;
    while (pos>=0 && r) {
        cur[pos]++;
        r = cur[pos] == k;
        if (r) cur[pos] = 0;
        --pos;
    }
    return !r;
}
int main() {
    // freopen("input.txt","r",stdin);
    // freopen("output.txt","w",stdout);
    int n,k;
    cin>>n>>k;
    cur.resize(n,0);
    do {
        for (int i=0;i<cur.size();++i)
            printf("%d",cur[i]);
        printf("\n");
    }while (next_placement_rep(cur,n,k));
    return 0;
} 
