#include <cstddef>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 1

#ifdef DEBUG
# include "mydebug.h"
#endif

template <typename T>
class Array
{
public:
    // Список операций:
    //
    // explicit Array(size_t size = 0, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    explicit Array(size_t size = 0, const T& value = T())
                     : size_(size)
                     , data_(new T[size])
    {
        for (size_t i = 0; i < size; i++)
            data_[i] = value;
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()  { delete [] data_; data_ = 0; }
    //
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Считайте, что для типа
    //   T определен оператор присваивания.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = new T[size_];
        for (size_t i = 0; i < size_; i++)
            data_[i] = v.data_[i];
    }
    //
    // Array& operator=(...)
    //   оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            delete [] data_;
            size_ = v.size_;
            data_ = new T[size_];
            for (size_t i = 0; i < size_; i++)
                data_[i] = v.data_[i];
        }
        return *this;
    }
    //
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элементов).
    size_t size() const { return size_; }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
    T    * data_;
    size_t size_;
};


using namespace std;

int n, m;

typedef Array< vector<float> > Matrix;

void go_left_to_right(Matrix * a, int c) {
  int i, j;
  float d = (*a)[c][c];
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(d=, d);
#endif
  for (j = c; j < m; ++j) {
    (*a)[c][j] = (*a)[c][j]/d;
  }
  vector<float> vd(n, 0.0);
  for (i = c + 1; i < n; ++i) {
    vd[i] = (*a)[i][c] / (*a)[c][c];
    for (j = c; j < m; ++j) {
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*vd[i];
    }
  }
  return;
}

void go_down_to_up(Matrix * a, int c) {
  int i, j;
  for (i = c - 1; i >= 0 ; --i) {
    float s = (*a)[i][c];
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(s=, s);
#endif
    for (j = c; j < m; ++j) {
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
#endif
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*s;
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
#endif
    }
  }
  return;
}

const int not_res = 1;
const int resolvs = 2;

int scan_matrix_not_zero(Matrix * a1) {
  int i, j;
  bool nr = false;
  bool zr = false;

  for (i = 0; i < n; ++i) {
    float s = 0.0;
    for (j = 0; j < m - 1; ++j) {
      if (0.0 == (*a1)[i][j])
        s += 0.0;
      else 
        s = (*a1)[i][j];
    }
    if (0.0 == s) {
      zr = true;
      if (0.0 != (*a1)[i][j]) {
        nr = true;
      }
    }
  }
  if (nr) return not_res;
  if (zr) return resolvs;
  return 0;
}

void prepare(Matrix * a, int k) {
  if (0.0 != (*a)[k][k]) return;
  int i, j;
  vector<int> v(n, 0);
  for (i = k + 1; i < n; ++i) {
    if (0.0 != (*a)[i][k]) {
      for (j = k; j < m; ++j) {
        cerr_FILE_LINE;
        cerr_func_X_V_Y(a[, i,]);
        cerr_X_V_Y([, j,]);
        cerr_X_V_nl(=, (*a)[i][j]);
        if (0.0 != (*a)[i][j]) {
          v[i] += 1;
        }
      }
    }
  }
  int maxPos = 0;
  for (i = 0; i < v.size(); ++i) {
    if (v[i] > v[maxPos]) // Found a bigger max
      maxPos = i;
  }
  cout << "\nMaximum element in Vector v is: "
       << *(std::max_element(v.begin(), v.end()));
  cout << endl;
  cout << "\nMaximum element position in Vector v is: "
       << maxPos;
  cout << endl;
  return;
}

void cerr_matrix(Matrix * a, const char * func, const char * var);

int main() {
  cin >> n >> m;
  m++;
  vector<float> v1(m, 0.0);
  Matrix *a1  = new Matrix(n, v1);
  int i, j, k;
  for (i = 0; i < n; ++i) {
    for (j = 0; j < m; ++j) {
      cin >> (*a1)[i][j];
    }
  }
  for (k = 0; k < n; ++k) {
    prepare(a1, k);
    cerr_matrix(a1, "go_left_to_right", "a");
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
    go_left_to_right(a1, k);
    cerr_matrix(a1, "go_left_to_right", "a");
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
  }
  for (k = n - 1; k > 0; --k) {
    go_down_to_up(a1, k);
    cerr_matrix(a1, "go_down_to_up", "a");
  }
  for (i = 0; i < n; ++i) {
    for (j = 0; j < m - 1; ++j) {
      cout << (*a1)[i][j] << " ";
    }
    cout << (*a1)[i][j] << endl;
  }
  cout << endl;
  return 0;
}

void cerr_matrix(Matrix * a, const char * func, const char * var) {
#ifdef DEBUG
  int i, j;
  for (i = 0; i < n; ++i) {
    cerr_FILE_LINE;
    cerr << ":" << func << " | ";
    for (j = 0; j < m - 1; ++j) {
      cerr_X_V_Y(a[, i,]);
      cerr_X_V_Y([, j,]=);
      cerr.width(4);
      cerr << (*a)[i][j] << " | ";
    }
    cerr_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
  }
#endif
}

