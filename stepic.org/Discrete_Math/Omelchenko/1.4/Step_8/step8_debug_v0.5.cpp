#include <cmath>
#include <cfloat>
#include <cstddef>
#include <iostream>
#include <vector>
#include <algorithm>

// #define DEBUG 1

#ifdef DEBUG
# include "mydebug.h"
#endif

template <typename T>
class Array
{
public:
    // Список операций:
    //
    // explicit Array(size_t size = 0, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    explicit Array(size_t size = 0, const T& value = T())
                     : size_(size)
                     , data_(new T[size])
    {
        for (size_t i = 0; i < size; i++)
            data_[i] = value;
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()  { delete [] data_; data_ = 0; }
    //
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Считайте, что для типа
    //   T определен оператор присваивания.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = new T[size_];
        for (size_t i = 0; i < size_; i++)
            data_[i] = v.data_[i];
    }
    //
    // Array& operator=(...)
    //   оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            delete [] data_;
            size_ = v.size_;
            data_ = new T[size_];
            for (size_t i = 0; i < size_; i++)
                data_[i] = v.data_[i];
        }
        return *this;
    }
    //
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элементов).
    size_t size() const { return size_; }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
    T    * data_;
    size_t size_;
};


using namespace std;

int n, m;

typedef Array< vector<double> > Matrix;

bool areEqual(double x, double y) {
  static double SMALL_NUM = DBL_EPSILON*1.0E9;
#if defined(DEBUG) && DEBUG > 8
  cerr << "DBL_EPSILON   :" << DBL_EPSILON << endl;
  cerr << "DBL_DIG       :" << DBL_DIG << endl;
  cerr << "FLT_DIG       :" << FLT_DIG << endl;
  cerr << "SMALL_NUM     :" << SMALL_NUM << endl;
#endif
  if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
    return std::abs(x-y) < SMALL_NUM;
  else
    return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

void go_left_to_right(Matrix * a, int c) {
  int i, j;
  double d = (*a)[c][c];
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(d=, d);
#endif
  for (j = c; j < m; ++j) {
    (*a)[c][j] = (*a)[c][j]/d;
  }
  vector<double> vd(n, 0.0);
  for (i = c + 1; i < n; ++i) {
    vd[i] = (*a)[i][c] / (*a)[c][c];
    for (j = c; j < m; ++j) {
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*vd[i];
    }
  }
  return;
}

void go_down_to_up(Matrix * a, int c) {
  int i, j;
  for (i = c - 1; i >= 0 ; --i) {
    double s = (*a)[i][c];
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(s=, s);
#endif
    for (j = c; j < m; ++j) {
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
#endif
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*s;
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
#endif
    }
  }
  return;
}

const int not_res = 1;
const int resolvs = 2;

int scan_matrix_not_zero(Matrix * a1) {
  int i, j;
  bool nr = false;
  bool zr = false;

  for (i = 0; i < n; ++i) {
    double s = 0.0;
    for (j = 0; j < m - 1; ++j) {
      if (areEqual(0.0, (*a1)[i][j]))
        s += 0.0;
      else 
        s = (*a1)[i][j];
    }
    if (areEqual(0.0, s)) {
      zr = true;
      if (!areEqual(0.0, (*a1)[i][j])) {
        nr = true;
      }
    }
  }
  if (nr) return not_res;
  if (zr) return resolvs;
  return 0;
}

void swap(Matrix * a, int i1, int i2) {
  vector<double> &v = (*a)[i1];
  (*a)[i1] = (*a)[i2];
  (*a)[i2] = v;
  return;
}

void prepare(Matrix * a, int k) {
  if (0.0 != (*a)[k][k]) return;
  int i, j;
  vector<int> v(n, 0);
  for (i = k + 1; i < n; ++i) {
    if (!areEqual(0.0, (*a)[i][k])) {
      for (j = k; j < m; ++j) {
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V_Y(a[, i,]);
        cerr_X_V_Y([, j,]);
        cerr_X_V_nl(=, (*a)[i][j]);
#endif
        if (!areEqual(0.0, (*a)[i][j])) {
          v[i] += 1;
        }
      }
    }
  }
  int maxPos = 0;
  for (i = 0; i < v.size(); ++i) {
    if (v[i] > v[maxPos]) // Found a bigger max
      maxPos = i;
  }
  swap(a, k, maxPos);
  return;
}

bool non_zero_column(Matrix * a) {
  int i, j;
  vector<int> v(n, 0);
  for (i = 0; i < n; ++i) {
    for (j = 0; j < m - 1; ++j) {
#ifdef DEBUG
      cerr_FILE_LINE;
      cerr_func_X_V_Y(a[, i,]);
      cerr_X_V_Y([, j,]);
      cerr_X_V_nl(=, (*a)[i][j]);
#endif
      if (!areEqual(0.0, (*a)[i][j])) {
        v[i] += 1;
        if (v[1] > 1)
          return true;
      }
    }
  }
  return false;
}

void cerr_matrix(Matrix * a, const char * func, const char * var);

int main() {
  cin >> n >> m;
  m++;
  vector<double> v1(m, 0.0);
  Matrix *a1  = new Matrix(n, v1);
  int i, j, k;
  for (i = 0; i < n; ++i) {
    for (j = 0; j < m; ++j) {
      cin >> (*a1)[i][j];
    }
  }
  for (k = 0; k < n; ++k) {
    prepare(a1, k);
    cerr_matrix(a1, "go_left_to_right", "a");
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
    go_left_to_right(a1, k);
    cerr_matrix(a1, "go_left_to_right", "a");
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
  }
  for (k = n - 1; k > 0; --k) {
    go_down_to_up(a1, k);
    cerr_matrix(a1, "go_down_to_up", "a");
  }
  if (non_zero_column(a1)) {
    cout << "INF" << endl;
    return 0;
  }
  for (i = 0; i < n - 1; ++i) {
    cout.precision(17);
    cout << (*a1)[i][m - 1] << " ";
  }
  cout.precision(17);
  cout << (*a1)[i][m - 1] << endl;
  return 0;
}

void cerr_matrix(Matrix * a, const char * func, const char * var) {
#ifdef DEBUG
  int i, j;
  for (i = 0; i < n; ++i) {
    cerr_FILE_LINE;
    cerr << ":" << func << " | ";
    for (j = 0; j < m - 1; ++j) {
      cerr_X_V_Y(a[, i,]);
      cerr_X_V_Y([, j,]=);
      cerr.width(4);
      cerr << (*a)[i][j] << " | ";
    }
    cerr_X_V_Y(a[, i,]);
    cerr_X_V_Y([, j,]);
    cerr_X_V_nl(=, (*a)[i][j]);
  }
#endif
}

