/* step8.cpp */
/* $Date$
 * $Id$
 * $Version: 1.0$
 * $Revision: 1$
 */

#include <cmath>
#include <cfloat>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

template <typename T>
class Array {
public:
    // Список операций:
    //
    // explicit Array(int size = 0, const T& value = T())
    //  конструктор класса, который создает
    //  Array размера size, заполненный значениями
    //  value типа T. Считайте что у типа T есть
    //  конструктор, который можно вызвать без
    //  без параметров, либо он ему не нужен.
    explicit Array(int size = 0, const T& value = T())
                                 : size_(size) , data_(new T[size]) {
        for (int i = 0; i < size; i++)
            data_[i] = value;
    }
    //
    // ~Array()
    //   деструктор, если он вам необходим.
    ~Array()    { delete [] data_; data_ = 0; }
    //
    // Array(const Array &)
    //  конструктор копирования, который создает
    //  копию параметра. Считайте, что для типа
    //  T определен оператор присваивания.
    Array(const Array &v) {
        size_ = v.size_;
        data_ = new T[size_];
        for (int i = 0; i < size_; i++)
            data_[i] = v.data_[i];
    }
    //
    // Array& operator=(...)
    //  оператор присваивания.
    Array& operator=(const Array &v) {
        if (this != &v) {
            delete [] data_;
            size_ = v.size_;
            data_ = new T[size_];
            for (int i = 0; i < size_; i++)
                data_[i] = v.data_[i];
        }
        return *this;
    }
    //
    // int size() const
    //  возвращает размер массива (количество элементов).
    int size() const { return size_; }
    //
    // T& operator[](int)
    // const T& operator[](int) const
    //  две версии оператора доступа по индексу.
    T& operator[](int i)
    { return data_[i]; }
    const T& operator[](int i) const
    { return data_[i]; }
    T        * data_;
    int size_;
};

using namespace std;

int n, m, x, y;

typedef Array< vector<double> > Matrix;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = FLT_EPSILON *1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

bool areEqualTo0(double & x) {
    if (areEqual(x , 0.0)) {
        x = 0.0;
        return true;
    }
    return false;
}

void cerr_matrix(Matrix & a, const char * func, const char * var, int l);

// функция input_maxtrix
// читает матрицу n на m со стандартного ввода
// вход: матрица Matrix & a
// номера строк:
//   откуда вычитаем - int from
//   что вычитаем    - int subt
bool input_maxtrix(Matrix & a, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i, j;
    for (i = 0; i < n1; ++i) {
        for (j = 0; j < m1; ++j) cin >> a[i][j];
    }
    return true;
}

// функция exchange
// меняет строки местами
// вход: матрица Matrix & a
// номера строк:
//   откуда и куда - int i1
//   куда и откуда - int i2
bool exchange(Matrix & a, int i1, int i2) {
    if (n <= i1 || n <= i2) return false;
    vector<double> v;
    v = a[i1];
    a[i1] = a[i2];
    a[i2] = v;
    return true;
}

// функция exchange_0_at_diag_el
// проверяет является ли диагональный элемент 0
// если да пытается поменять строку на которой он найден с
// ниже стоящей строкой
// (Matrix & a, int n1, int m1, int i1) {
// вход: матрица Matrix & a
// номера строк:
//   откуда и куда - int i1
//   куда и откуда - int i2
bool exchange_if0_at_diag_el(Matrix & a, int i1) {
    if (i1 >= n || i1 >= m) return false;
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(| exchange_0_at_diag_el i1=, i1);
#endif
    // if (n <= i1) return false;
    int j;
    if (areEqual(a[i1][i1], 0.0)) {
        bool ok = false;
        if (i1 < (n - 1)) {
            for (j = i1 + 1; j < n && j < (m - 1); ++j) {
#if defined(DEBUG) && DEBUG > 7
                cerr_FILE_LINE;
                cerr_func_X_V_nl(| j=, j);
                cerr_FILE_LINE;
                cerr_func_X_V_Y(| a[, i1,]);
                cerr_X_V_Y(][, j ,]);
                cerr_X_V_nl(=, a[i1][j]);
#endif
                if (!areEqual(a[i1][j], 0.0)) {
                    ok = exchange(a, i1, j);
                }
            }
        }
        return ok;
    }
    return true;
}

bool check_0_line0(Matrix & a, int m1, int i1) {
    if (m < m1 || n <= i1) return false;
    int j;
    for (j = 0; j < m1; ++j)
        if (!areEqual(a[i1][j], 0.0))
            return false;
    return true;
}
// ф-ция: check_0bad_line
// проверяет есть является ли строка такой что:
// a[i1][0], a[i1][1], ..., a[i1][m-1]
// такие что все елементы от a[i1][0] до a[i1][m-2] равны 0
// а елемент a[i1][m-1] не равен
// если да возвращает true
bool check_0bad_line(Matrix & a, int m1, int i1) {
    if (m < m1 || n <= i1) return false;
    if (areEqual(a[i1][m1-1], 0.0)) return false;
    int j;
    for (j = 0; j < (m1 - 1); ++j)
        if (!areEqual(a[i1][j], 0.0))
            return false;
    return true;
}

bool lines_0_to_bottom(Matrix & a, int n1, int m1) {
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V(| n1=, n1);
    cerr_X_V_nl(| m1=, m1);
#endif
    if (n < n1 || m < m1) return false;
    int i, k;
    for (i = 0; i < n1; ++i) {
#if defined(DEBUG) && DEBUG > 6
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| i=, i );
#endif
        if (check_0_line0(a, m1, i)) {
            for (k = (n1 - 1); k > i; --k) {
                if (!check_0_line0(a, m1, k)) {
#if defined(DEBUG) && DEBUG > 7
                    cerr_FILE_LINE;
                    cerr_func_X_V(| i=, i );
                    cerr_X_V_nl(| to k=, k);
#endif
                    exchange(a, i, k);
                    break;
                }
            } // end for (k = (n1 - 1); k > i; ++k)
        }
    }
#if defined(DEBUG) && DEBUG > 5
    cerr_matrix(a, __func__, "a", __LINE__);
#endif
    for (k = (n1 - 1); k > 0; --k) {
#if defined(DEBUG) && DEBUG > 6
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| exchange_0_at_diag_el k=, k);
#endif
        if (check_0_line0(a, m1, k)) {
            n = k;
#if defined(DEBUG) && DEBUG > 6
                cerr_FILE_LINE;
                cerr_func_X_V_nl(| exchange_0_at_diag_el remove k=, k);
#endif
        }
    } // for (k = (n1 - 1); k >= m1; ++k)
#ifdef DEBUG
    cerr_matrix(a, __func__, "a", __LINE__);
#endif
    return true;
}

bool lines_0bad_to_bottom(Matrix & a, int n1, int m1) {
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V(| n1=, n1);
    cerr_X_V_nl(| m1=, m1);
#endif
    if (n < n1 || m < m1) return false;
    int i, k;
    for (i = 0; i < n1; ++i) {
#if defined(DEBUG) && DEBUG > 6
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| i=, i );
#endif
        if (check_0bad_line(a, m1, i)) {
            for (k = (n1 - 1); k > i; --k) {
                if (!check_0bad_line(a, m1, k)
                    && !check_0_line0(a, m1, k)) {
#if defined(DEBUG) && DEBUG > 7
                    cerr_FILE_LINE;
                    cerr_func_X_V(| i=, i );
                    cerr_X_V_nl(| to k=, k);
#endif
                    exchange(a, i, k);
                    break;
                }
            } // end for k
        }
    } // end for i
#ifdef DEBUG
    cerr_matrix(a, __func__, "a", __LINE__);
#endif
    return true;
}

bool check_bad_lines(Matrix & a, int n1, int m1) {
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V(| n1=, n1);
    cerr_X_V_nl(| m1=, m1);
#endif
    if (n < n1 || m < m1) return false;
    int i;
    for (i = 0; i < n1; ++i) {
#if defined(DEBUG) && DEBUG > 6
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| i=, i );
#endif
        if (check_0bad_line(a, m1, i)) {
#if defined(DEBUG) && DEBUG > 7
            cerr_FILE_LINE;
            cerr_func_X_V_nl(| i=, i );
#endif
            return true;
        }
    } // end for i
    return false;
}

int first_pass_at(Matrix & a, int n1, int m1, int e1) {
    if (n < n1 || m < m1 || n <= e1) return false;
    double divider = a[e1][e1];
#ifdef DEBUG
    cerr_FILE_LINE;
    cerr_func_X_V_nl(| divider=, divider);
#endif
    if (areEqual(divider, 0.0)) {
        cout << "NO" << endl;
        exit(0);
    }
    int j;
    for (j = e1; j < m1; ++j) {
        a[e1][j] = a[e1][j] / divider;
    }
    int i;
    for (i = e1 + 1; i < n1; ++i) {
        double subt = a[i][e1] / a[e1][e1];
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| subt=, subt);
#endif
        for (j = e1; j < m1; ++j) {
            a[i][j] = a[i][j] - a[e1][j]*subt;
        }
    }
    return 1;
}

// ф-ция: first_pass
// первый проход, приводит матрицу к ступенчатому виду
int first_pass(Matrix & a, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i;
    for (i = 0; i < (m1 - 1); ++i) {
        if (!exchange_if0_at_diag_el(a, i)) {
#ifdef DEBUG
            cerr_FILE_LINE;
            cerr_func_X_V_Y_nl(| not exchange_0_at_diag_el [, i,]);
#endif
        }
        first_pass_at(a, n1, m1, i);
#ifdef DEBUG
        cerr_matrix(a, __func__, "a", __LINE__);
#endif
    }
    return 0;
}

bool check_square_diagonal(Matrix & a, int m1) {
    if (n < m1) return false;
    int i;
    for (i = 0; i < m1; ++i)
        if (!areEqual(a[i][i], 1.0))
            return false;
    return true;
}

int reversal(vector<double> & v, Matrix & a, int n1, int m1) {
    if (m > m1 || n > n1) return 0;
    int i, j;
    for (i = (n1 - 1); i >= 0; --i) {
        v[i] = a[i][m - 1];
        for (j = (i + 1); j < (m1 - 1); ++j) {
            v[i] -= a[i][j] * v[j];
        }
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| i=, i);
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| v[i]=, v[i]);
#endif
    }
    return 0;
}

bool resolution(Matrix & a, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int min = ((n1 < m1) ? (n1 - 1) : (m1 - 1));
    int i, j;
    for (i = 0; i < min; ++i) {
        double sum = 0.0;
        for (j = 0; j < (m1 - 1); ++j)
            sum += a[i][j];
        // end for 
        if (areEqual(sum, 0.0) && !areEqual(a[i][m1 - 1], 0.0))
            return false;
    }
    return true;
}

int main(void) {
    cin >> n >> m;
    if (n < 1 || m < 1) {
        cout << "NO" << endl;
        return 0;
    }
    x = m; y = n; x--; m++;
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| m=, m);
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| x=, x);
#endif
    vector<double> v1(m, 0.0);
#ifdef DEBUG
        cerr_FILE_LINE;
        cerr_func_X_V_nl(| size=, v1.size());
#endif
    Matrix *a1 = new Matrix(n, v1);
    input_maxtrix(*a1, n, m);
    cerr_matrix(*a1, __func__, "a1", __LINE__);
    // TODO check_first_row
    lines_0_to_bottom(*a1, n, m);
    lines_0bad_to_bottom(*a1, n, m);
    first_pass(*a1, n, m);
    cerr_matrix(*a1, __func__, "a1", __LINE__);
    if (n > (m - 1)) {
        // move zero lines to bottom
        // from i == (m - 1) to n
        lines_0_to_bottom(*a1, n, m);
    }
    cerr_matrix(*a1, __func__, "a1", __LINE__);
    if (!resolution(*a1, n, m)) {
        cout << "NO" << endl;
        return 0;
    }
    cerr_matrix(*a1, __func__, "a1", __LINE__);
    if (n == (m - 1)) {
        if (check_square_diagonal(*a1, n)) {
            reversal(v1, *a1, n, m);
            cerr_matrix(*a1, __func__, "a3", __LINE__);
            cout << "YES" << endl;
            cout.precision(16);
            for (int i = 0; i < (m - 1); ++i)
                cout << v1[i] << (((m - 2) == i) ? "" : " ");
            cout << endl;
        }
    } else {
        if (check_bad_lines(*a1, n, m)) {
            cout << "NO" << endl;
        } else
            cout << "INF" << endl;
    }
    return 0;
}

void cerr_matrix(Matrix & a, const char * func, const char * var, int l) {
#ifdef DEBUG
    int i, j;
    for (i = 0; i < n; ++i) {
        cerr << __FILE__ << ':' << l;
        cerr << ":" << func << " | ";
        for (j = 0; j < m - 1; ++j) {
            cerr_X_V_Y(a[, i,]);
            cerr_X_V_Y([, j,]=);
            cerr.width(4);
            cerr << a[i][j] << " | ";
        }
        cerr_X_V_Y(a[, i,]);
        cerr_X_V_Y([, j,]);
        cerr_X_V_nl(=, a[i][j]);
    }
#endif
}

/* vim: syntax=c:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
