#!/bin/sh
NAME=step8
RELEASE=
diff=colordiff
if g++ ${NAME}${RELEASE}.cpp -o ${NAME}${RELEASE}
then
  for N in ${NAME}_in*.txt
  do
    N=${N##*_in}
    N=${N%%.txt}
    echo $N
    ./${NAME}${RELEASE} < ${NAME}_in${N}.txt \
                        > ${NAME}_out${N}.txt \
                        2> ${NAME}_err${N}.txt
    if [ -f ${NAME}_ok${N}.txt ]
    then
      $diff -Nru ${NAME}_out${N}.txt ${NAME}_ok${N}.txt
    fi
  done
fi
