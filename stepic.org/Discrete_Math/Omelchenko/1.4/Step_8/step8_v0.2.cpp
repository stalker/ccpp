#include <cstddef>
#include <iostream>
#include <vector>
#include <algorithm>

template <typename T>
class Array
{
public:
    explicit Array(size_t size = 0, const T& value = T())
                     : size_(size) , data_(new T[size]) {
        for (size_t i = 0; i < size; i++)
            data_[i] = value;
    }
    ~Array()  { delete [] data_; data_ = 0; }
    Array(const Array &v) {
        size_ = v.size_;
        data_ = new T[size_];
        for (size_t i = 0; i < size_; i++)
            data_[i] = v.data_[i];
    }
    Array& operator=(const Array &v) {
        if (this != &v) {
            delete [] data_;
            size_ = v.size_;
            data_ = new T[size_];
            for (size_t i = 0; i < size_; i++)
                data_[i] = v.data_[i];
        }
        return *this;
    }
    size_t size() const { return size_; }
    T& operator[](size_t i)
    { return data_[i]; }
    const T& operator[](size_t i) const
    { return data_[i]; }
    T    * data_;
    size_t size_;
};

using namespace std;
int n, m;
typedef Array< vector<double> > Matrix;

void go_left_to_right(Matrix * a, int c) {
  int i, j;
  double d = (*a)[c][c];
  for (j = c; j < m; ++j)
    (*a)[c][j] = (*a)[c][j]/d;
  vector<double> vd(n, 0.0);
  for (i = c + 1; i < n; ++i) {
    vd[i] = (*a)[i][c] / (*a)[c][c];
    for (j = c; j < m; ++j)
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*vd[i];
  }
  return;
}

void go_down_to_up(Matrix * a, int c) {
  int i, j;
  for (i = c - 1; i >= 0 ; --i) {
    double s = (*a)[i][c];
    for (j = c; j < m; ++j)
      (*a)[i][j] = (*a)[i][j] - (*a)[c][j]*s;
  }
  return;
}

const int not_res = 1;
const int resolvs = 2;

int scan_matrix_not_zero(Matrix * a1) {
  int i, j;
  bool nr = false;
  bool zr = false;

  for (i = 0; i < n; ++i) {
    double s = 0.0;
    for (j = 0; j < m - 1; ++j) {
      if (0.0 == (*a1)[i][j])
        s += 0.0;
      else 
        s = (*a1)[i][j];
    }
    if (0.0 == s) {
      zr = true;
      if (0.0 != (*a1)[i][j])
        nr = true;
    }
  }
  if (nr) return not_res;
  if (zr) return resolvs;
  return 0;
}

void swap(Matrix * a, int i1, int i2) {
  vector<double> &v = (*a)[i1];
  (*a)[i1] = (*a)[i2];
  (*a)[i2] = v;
  return;
}

void prepare(Matrix * a, int k) {
  if (0.0 != (*a)[k][k]) return;
  int i, j;
  vector<int> v(n, 0);
  for (i = k + 1; i < n; ++i)
    if (0.0 != (*a)[i][k])
      for (j = k; j < m; ++j)
        if (0.0 != (*a)[i][j])
          v[i] += 1;
  int maxPos = 0;
  for (i = 0; i < v.size(); ++i)
    if (v[i] > v[maxPos]) // Found a bigger max
      maxPos = i;
  swap(a, k, maxPos);
  return;
}

int main() {
  cin >> n >> m;
  m++;
  vector<double> v1(m, 0.0);
  Matrix *a1  = new Matrix(n, v1);
  int i, j, k;
  for (i = 0; i < n; ++i)
    for (j = 0; j < m; ++j)
      cin >> (*a1)[i][j];
  for (k = 0; k < n; ++k) {
    prepare(a1, k);
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
    go_left_to_right(a1, k);
    switch (scan_matrix_not_zero(a1)) {
      case not_res:
        cout << "NO" << endl;
        return 0;
      case resolvs:
        cout << "INF" << endl;
        return 0;
    }
  }
  for (k = n - 1; k > 0; --k) {
    go_down_to_up(a1, k);
  }
  cout << "YES" << endl;
  for (i = 0; i < n - 1; ++i) {
    cout.precision(17);
    cout << (*a1)[i][m - 1] << " ";
  }
  cout.precision(17);
  cout << (*a1)[i][m - 1] << endl;
  return 0;
}
