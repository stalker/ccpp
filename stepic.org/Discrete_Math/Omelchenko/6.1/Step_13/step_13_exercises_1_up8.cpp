/* step_13_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>


using std::stack;
using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;
typedef vector<IntVector>  IntMatrix;

class Graph {
    int en_; // Edge number
    int vn_; // Vertex number
    IntMatrix * al_; // Adjacency List
    IntMatrix * am_; // Adjacency Matrix
    public:
    explicit Graph( int en = 0, int vn = 0 )
                  : en_(en) , vn_(vn) {
        al_ = new IntMatrix(vn);
        am_ = new IntMatrix(vn, IntVector (vn, 0));
    }
    int operator()(size_t i, size_t j) {
        return (*am_)[i][j];
    }
    IntVector& operator[](size_t i)
    { return (*am_)[i]; }
    const IntVector& operator[](size_t i) const
    { return (*am_)[i]; }
    IntVector& al(size_t i)
    { return (*al_)[i]; }
    const IntVector& al(size_t i) const
    { return (*al_)[i]; }
    bool add(size_t a, size_t b, size_t e) {
        --a; --b; --e;
        ++(*am_)[a][b];
        ++(*am_)[b][a];
        (*al_)[a].push_back(b);
        (*al_)[b].push_back(a);
        return true;
    }
    int is_edge(size_t a, size_t b) {
        size_t sl = (*al_)[a].size();
        for (int k = 0; k < sl; ++k) {
            if (b == (*al_)[a][k])
                return k;
        }
        return -1;
    }
    bool delete_edge(size_t a, size_t b) {
        size_t k1, k2;
        if (-1 == (k1 = is_edge(a, b))) return false;
        if (-1 == (k2 = is_edge(b, a))) return false;
        ItIVector pos = ((*al_)[a].begin() + k1);
        (*al_)[a].erase(pos);
        --(*am_)[a][b];
        pos = ((*al_)[b].begin() + k2);
        (*al_)[b].erase(pos);
        --(*am_)[b][a];
        return true;
    }
    bool edges_exists() {
        for (int i = 0; i < vn_; ++i) {
            for (int j = 0; j < vn_; ++j) {
                if (0 != (*am_)[i][j])
                    return true;
            }
        }
        return false;
    }
    void al_reverse_sort(void) {
        for (size_t i = 0; i < (*al_).size(); ++i) {
            sort((*al_)[i].begin(), (*al_)[i].end(), std::greater<int>());
        }
    }
    int size()      { return vn_; }
    int edge_size() { return en_; }
    int esize()     { return en_; }
    int vsize()     { return vn_; }
};

bool walk(IntVector & r, Graph & g, int f) {
    r.push_back(f);
    for (int i = 0; i < g.al(f).size(); ++i) {
        int k = g.al(f)[i];
        g.delete_edge(f, k);
        walk(r, g, k);
    }
    return true;
}
int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    int m;
    cin >> m;
    Graph g(m, n);
    if (0 == n or 0 == m) {
      cout << "NONE" << endl;
      return 0;
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a;
        cin >> b;
        g.add(a, b, i);
    }
    size_t vn = g.size();
    g.al_reverse_sort();
    IntVector deg(n);
    for (int i = 0; i < vn; ++i)
        for (int j = 0; j < vn; ++j) {
            int d = g[i][j] + g[j][i];
            deg[i] += d;
        }
    for (int i = 0; i < vn; ++i) {
        if (0 != (deg[i] % 2)) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } else if (0 == deg[i]) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } 
    }

    IntVector res;
    if (walk(res, g, 0)) {
        if (0 == res[res.size() - 1]) {
            for (size_t i = 0; i < res.size(); ++i) {
                if (g.al(res[i]).size() > 0) {
                    IntVector end(res.begin() + i, res.end());
                    res.erase(res.begin() + i, res.end());
                    walk(res, g, res[i]);
                    for (size_t j = 0; j < end.size(); ++j)
                        res.push_back(end[j]);
                }
            }
        }
        if (g.edges_exists()) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        }
        for (size_t i = 0; i < (res.size() - 1); ++i)
            cout << (res[i] + 1) << " ";
        cout << endl;
    }
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
