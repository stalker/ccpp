/* step_13_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

using std::stack;
using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;
typedef vector<vector<int> >  IntMatrix;

void set_path(IntMatrix & g, int a, int b) {
    ++g[a][b];
}

int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    IntMatrix g(n, IntVector (n));

    int m;
    cin >> m;
    if (0 == n or 0 == m) {
      cout << "NONE" << endl;
      return 0;
    }
    for (int i = 0; i < m; i++) {
      int a, b;
        cin >> a;
        cin >> b;
        set_path(g, a - 1, b - 1);
        set_path(g, b - 1, a - 1);
    }

    IntVector deg(n);
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            deg[i] += g[i][j];

    int first = 0;
    while (!deg[first]) ++first;

    int v1 = -1,  v2 = -1;
    bool bad = false;
    for (int i = 0; i < n; ++i)
        if (deg[i] & 1) {
            if (v1 == -1) {
                v1 = i;
            } else if (v2 == -1) {
                v2 = i;
            } else
                bad = true;
        }

    if (v1 != -1)
        ++g[v1][v2], ++g[v2][v1];

    stack<int> st;
    st.push(first);
    IntVector res;
    while (!st.empty()) {
        int v = st.top();
        int i;
        for (i = 0; i < n; ++i)
            if (g[v][i])
                break;
        if (i == n) {
            res.push_back (v);
            st.pop();
        } else {
            --g[v][i];
            --g[i][v];
            st.push (i);
        }
    }

    if (v1 != -1)
        for (size_t i=0; i+1<res.size(); ++i)
            if (res[i] == v1 && res[i+1] == v2 \
                || res[i] == v2 && res[i+1] == v1)
            {
                IntVector res2;
                for (size_t j = i + 1; j < res.size(); ++j)
                    res2.push_back(res[j]);
                for (size_t j = 1; j <= i; ++j)
                    res2.push_back(res[j]);
                res = res2;
                break;
            }
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            if (g[i][j])
                bad = true;
    if (bad)
        cout << "NONE";
    else if (res[0] != res[res.size() - 1])
        cout << "NONE";
    else {
        res.pop_back();
        for (size_t i = 0; i < res.size(); ++i)
            cout << (res[i] + 1)
              << ((i < res.size() ? " " : ""));
    }
    cout << endl;
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
