/* step_13_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

using std::stack;
using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;
typedef vector<IntVector>  IntMatrix;

class Graph {
    int en_; // Edge number
    int vn_; // Vertex number
    IntMatrix * al_; // Adjacency List
    IntMatrix * am_; // Adjacency Matrix
    public:
    explicit Graph( int en = 0, int vn = 0 )
                  : en_(en) , vn_(vn) {
        al_ = new IntMatrix(vn);
        am_ = new IntMatrix(vn, IntVector (vn, 0));
    }
    int operator()(size_t i, size_t j) {
        return (*am_)[i][j];
    }
    IntVector& operator[](size_t i)
    { return (*am_)[i]; }
    const IntVector& operator[](size_t i) const
    { return (*am_)[i]; }
    IntVector& al(size_t i)
    { return (*al_)[i]; }
    const IntVector& al(size_t i) const
    { return (*al_)[i]; }
    bool add(size_t a, size_t b, size_t e) {
        --a; --b; --e;
        ++(*am_)[a][b];
        ++(*am_)[b][a];
        (*al_)[a].push_back(b);
        (*al_)[b].push_back(a);
        return true;
    }
    int is_edge(size_t a, size_t b) {
        size_t sl = (*al_)[a].size();
        for (int k = 0; k < sl; ++k) {
            if (b == (*al_)[a][k])
                return k;
        }
        return -1;
    }
    bool delete_edge(size_t a, size_t b) {
        size_t k1, k2;
        if (-1 == (k1 = is_edge(a, b))) return false;
        if (-1 == (k2 = is_edge(b, a))) return false;
        ItIVector pos = ((*al_)[a].begin() + k1);
        (*al_)[a].erase(pos);
        --(*am_)[a][b];
        pos = ((*al_)[b].begin() + k2);
        (*al_)[b].erase(pos);
        --(*am_)[b][a];
        return true;
    }
    bool edges_exists() {
        for (int i = 0; i < vn_; ++i) {
            for (int j = i; j < vn_; ++j) {
                if (0 != (*am_)[i][j])
                    return true;
            }
        }
        return false;
    }
    bool non_zero_edges_exists() {
        for (int i = 1; i < vn_; ++i) {
            for (int j = i; j < vn_; ++j) {
                if (0 != (*am_)[i][j])
                    return true;
            }
        }
        return false;
    }
    void al_reverse_sort(void) {
        // std::sort(numbers.begin(), numbers.end(), std::greater<int>());
        for (size_t i = 0; i < (*al_).size(); ++i) {
            sort((*al_)[i].begin(), (*al_)[i].end(), std::greater<int>());
        }
    }
    int size()      { return vn_; }
    int edge_size() { return en_; }
    int esize()     { return en_; }
    int vsize()     { return vn_; }
};

#if defined(DEBUG)
void cerr_adj_list(Graph &);
void cerr_adj_matrix(Graph &);
void cerr_intvector(IntVector &);
#endif

int walk(IntVector & r, Graph & g, int f) {
    for (int i = 0; i < g.al(f).size(); ++i) {
        if (0 == g.al(f)[i]) {
            ItIVector pos = g.al(f).begin() + i;
            g.al(f).erase(pos);
            g.al(f).push_back(0);
        }
    }
    for (int i = 0; i < g.al(f).size(); ++i) {
        int k = g.al(f)[i];
#if defined(DEBUG)
        cerr << "walk2 k = " << k << " g.edges_exists():"
                 << g.non_zero_edges_exists() << endl;
#endif
        g.delete_edge(f, k);
        walk(r, g, k);
    }
    r.push_back(f);
#if defined(DEBUG)
    cerr << "walk r.push_back(" << f + 1 << ")" << endl;
#endif
#if defined(DEBUG)
    cerr_adj_list(g);
    cerr_adj_matrix(g);
#endif
    return true;
}
int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    int m;
    cin >> m;
    Graph g(m, n);
    if (0 == n or 0 == m) {
      cout << "NONE" << endl;
      return 0;
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a;
        cin >> b;
        g.add(a, b, i);
    }
    size_t vn = g.size();
#if defined(DEBUG)
    cerr_adj_list(g);
    cerr_adj_matrix(g);
#endif

    IntVector deg(n);
    for (int i = 0; i < vn; ++i)
        for (int j = 0; j < vn; ++j) {
            int d = g[i][j] + g[j][i];
            deg[i] += d;
        }
    for (int i = 0; i < vn; ++i) {
        if (0 != (deg[i] % 2)) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } else if (0 == deg[i]) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } 
    }

    IntVector res;
    if (walk(res, g, 0)) {
/*
        if (0 == res[res.size() - 1]) {
            for (size_t i = 0; i < res.size(); ++i) {
                if (g.al(res[i]).size() > 0) {
#if defined(DEBUG)
                    cerr << "vector res before: ";
                    cerr_intvector(res);
#endif
                    IntVector end(res.begin() + i, res.end());
#if defined(DEBUG)
                    cerr << "vector end before: ";
                    cerr_intvector(end);
#endif
                    res.erase(res.begin() + i, res.end());
#if defined(DEBUG)
                    cerr << "vector res after cut: ";
                    cerr_intvector(res);
#endif
                    walk(res, g, res[i]);
#if defined(DEBUG)
                    cerr << "vector res after walk: ";
                    cerr_intvector(res);
#endif
                    for (size_t j = 0; j < end.size(); ++j)
                        res.push_back(end[j]);
#if defined(DEBUG)
                    cerr << "vector res after concatenate: ";
                    cerr_intvector(res);
#endif
                }
            }
        }
*/
        if (g.edges_exists()) {
            cout << "NONE" << endl;
#if defined(DEBUG)
            cerr_adj_list(g);
            cerr_adj_matrix(g);
#endif
            return EXIT_SUCCESS;
        }
        for (size_t i = 0; i < (res.size() - 1); ++i)
            cout << (res[i] + 1) << " ";
        cout << endl;
    }
    return EXIT_SUCCESS;
}

#if defined(DEBUG)
void cerr_adj_list(Graph & g) {
    size_t vn = g.size();
    cerr << "Adjacency List:" << endl;
    for (int i = 0; i < vn; ++i) {
        size_t sl = g.al(i).size();
        cerr << "Vertex number: " << (i + 1) << endl;
        for (int j = 0; j < sl; ++j) {
            cerr << (g.al(i)[j] + 1)
                 << (((sl - 1) == j) ? "" : " ");
        }
        cerr << endl;
    }
}

void cerr_adj_matrix(Graph & g) {
    size_t vn = g.size();
    cerr << "Adjacency matrix:" << endl;
    for (int i = 0; i < vn; ++i) {
        for (int j = 0; j < vn; ++j) {
            cerr << g[i][j]
                 << (((vn - 1) == j) ? "" : " ");
        }
        cerr << endl;
    }
}

void cerr_intvector(IntVector & v) {
    size_t vn = v.size();
    for (int i = 0; i < vn; ++i) {
            cerr << (v[i] + 1)
                 << (((vn - 1) == i) ? "" : " ");
    }
    cerr << endl;
}

#endif

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
