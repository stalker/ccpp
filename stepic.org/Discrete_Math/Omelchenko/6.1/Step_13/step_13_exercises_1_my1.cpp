/* step_13_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

using std::stack;
using std::string;
using std::vector;
using namespace std;

typedef vector<char> ChrVector;
typedef vector<char>::iterator ItCVector;
typedef vector<ChrVector>  ChrMatrix;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;
typedef vector<IntVector>  IntMatrix;

class Graph {
    int en_; // Edge number
    int vn_; // Vertex number
    IntMatrix * al_; // Adjacency List
    IntMatrix * am_; // Adjacency Matrix
    ChrMatrix * im_; // Incidence Matrix

    public:
    explicit Graph( int en = 0, int vn = 0 )
                  : en_(en) , vn_(vn) {
        al_ = new IntMatrix(vn);
        am_ = new IntMatrix(vn, IntVector (vn, 0));
        im_ = new ChrMatrix(vn, ChrVector (en, 0));
    }

    int operator()(size_t i, size_t j) {
        return (*am_)[i][j];
    }
    //
    // T& operator[](size_t)
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.
    IntVector& operator[](size_t i)
    { return (*am_)[i]; }
    const IntVector& operator[](size_t i) const
    { return (*am_)[i]; }

    IntVector& al(size_t i)
    { return (*al_)[i]; }
    const IntVector& al(size_t i) const
    { return (*al_)[i]; }

    bool add(size_t a, size_t b, size_t e) {
        --a; --b; --e;
        ++(*am_)[a][b];
        ++(*am_)[b][a];
        (*im_)[a][e] = 1;
        (*im_)[b][e] = 1;
        (*al_)[a].push_back(b);
        (*al_)[b].push_back(a);
        return true;
    }

    int is_edge(size_t a, size_t b) {
        size_t sl = (*al_)[a].size();
        for (int k = 0; k < sl; ++k) {
            if (b == (*al_)[a][k])
                return k;
        }
        return -1;
    }

    bool delete_edge(size_t a, size_t b) {
        size_t k1, k2;
        if (-1 == (k1 = is_edge(a, b))) return false;
        if (-1 == (k2 = is_edge(b, a))) return false;
        ItIVector pos = ((*al_)[a].begin() + k1);
        (*al_)[a].erase(pos);
        --(*am_)[a][b];
        pos = ((*al_)[b].begin() + k2);
        (*al_)[b].erase(pos);
        --(*am_)[b][a];
        // TODO (*im_)[a][e] = 1;
        // TODO (*im_)[b][e] = 1;
        return true;
    }

    int size()      { return vn_; }
    int edge_size() { return en_; }
    int esize()     { return en_; }
    int vsize()     { return vn_; }
};

int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    int m;
    cin >> m;
    Graph g(m, n);
    if (0 == n or 0 == m) {
      cout << "NONE" << endl;
      return 0;
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a;
        cin >> b;
        g.add(a, b, i);
    }
    size_t vn = g.size();

#if defined(DEBUG)
    cout << "Adjacency List:" << endl;
    for (int i = 0; i < vn; ++i) {
        size_t sl = g.al(i).size();
        cout << "Vertex number: " << (i + 1) << endl;
        for (int j = 0; j < sl; ++j) {
            cout << (g.al(i)[j] + 1)
                 << (((sl - 1) == j) ? "" : " ");
        }
        cout << endl;
    }
    cout << "Adjacency matrix:" << endl;
    for (int i = 0; i < vn; ++i) {
        for (int j = 0; j < vn; ++j) {
            cout << g[i][j] << " "
                 << (((vn - 1) == j) ? "" : " ");
        }
        cout << endl;
    }
#endif

    IntVector deg(n);
    for (int i = 0; i < vn; ++i)
        for (int j = 0; j < vn; ++j) {
            deg[i] += g[i][j];
        }
    for (int i = 0; i < vn; ++i)
        if (0 != (deg[i] % 2)) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } else if (0 == deg[i]) {
            cout << "NONE" << endl;
            return EXIT_SUCCESS;
        } 

#if defined(DEBUG)
    cout << "deg:" << endl;
    for (int i = 0; i < vn; ++i)
        cout << deg[i]
             << (((vn - 1) == i) ? "" : " ");
    cout << endl;
    if (-1 != g.is_edge(0, 1)) {
        cout << "Ok" << endl;
        g.delete_edge(0, 1);
    }

#endif
    stack<int> st;
    st.push(0);
    IntVector res;
    while (!st.empty()) {
        int v = st.top();
        int s = g.al(v).size();
        if (0 == s) {
            res.push_back(v);
            st.pop();
        } else
            for (int i = 0; i < s; ++i) {
                int k = g.al(v)[i];
                cout << "g.delete_edge(" << v << ", " << k << ")" << endl;
                g.delete_edge(v, k); 
                st.push(k);
            }
    }
    for (size_t i = 0; i < res.size(); ++i)
        cout << (res[i] + 1)
            << ((i < res.size() ? " " : ""));
    cout << endl;

#if defined(DEBUG)
    cout << "Adjacency List:" << endl;
    for (int i = 0; i < vn; ++i) {
        size_t sl = g.al(i).size();
        cout << "Vertex number: " << (i + 1) << endl;
        for (int j = 0; j < sl; ++j) {
            cout << (g.al(i)[j] + 1)
                 << (((sl - 1) == j) ? "" : " ");
        }
        cout << endl;
    }
    cout << "Adjacency matrix:" << endl;
    for (int i = 0; i < vn; ++i) {
        for (int j = 0; j < vn; ++j) {
            cout << g[i][j] << " "
                 << (((vn - 1) == j) ? "" : " ");
        }
        cout << endl;
    }
#endif
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
