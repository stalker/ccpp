/* step7.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

int n;
int main(void) {
    cin >> n;
    if (n < 1) {
        cout << "NO" << endl;
        return 0;
    }
    vector<double> a(n, 0.0);
    vector<double> b(n, 0.0);
    int i, j, k;
    for (i = 0; i < n; ++i) {
        cin >> a[i];
    }
    for (i = 0; i < n; ++i) {
        cin >> b[i];
    }
    // Найдем скалярное произведение векторов:
    // scalar product
    double scalar_mult = 0.0;
    for (i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    cout << "scalar product(a, b) = " << scalar_mult << endl;
    // Найдем длины векторов:
    // modulus of vector
    double a_mod_vect = 0.0;
    for (i = 0; i < n; ++i) {
        a_mod_vect = a_mod_vect + a[i]*a[i];
    }
    a_mod_vect = sqrt(a_mod_vect);
    cout << "modulus of vector(a) = " << a_mod_vect << endl;
    double b_mod_vect = 0.0;
    for (i = 0; i < n; ++i) {
        b_mod_vect = b_mod_vect + b[i]*b[i];
    }
    b_mod_vect = sqrt(b_mod_vect);
    cout << "modulus of vector(b) = " << b_mod_vect << endl;
    // Найдем угол между векторами:
    double alpha = acos(scalar_mult/(a_mod_vect*b_mod_vect))*180/M_PI;
    cout << "alpha = " << alpha << endl;
    //    cout << "S = " << ((sqrt(3)/4)*(a_b*a_b)) << endl;
    // if (areEqual(a_b, a_c) && areEqual(a_b, b_c) && areEqual(a_c, b_c))
    //    cout << "S = " << ((sqrt(3)/4)*(a_b*a_b)) << endl;
    return 0;
}
/* vim: syntax=c:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
