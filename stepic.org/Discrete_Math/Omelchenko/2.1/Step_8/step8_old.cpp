/* step7.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * Молекула метана CH4 расположена в пространстве в виде пирамиды, в центре
 * которой находится атом углерода, а в вершинах — атомы водорода. Если вершины
 * разместить в точках с координатами (0,0,0), (1,1,0), (1,0,1) и (0,1,1), то
 * получится правильный тетраэдр, длины всех ребер которого равны 2√.  Каким
 * будет косинус угла между любыми двумя лучами, идущими из центра тетраэдра,
 * расположенного в точке (1/2, 1/2, 1/2), к вершинам этого тетраэдра?
 */

#include <cmath>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

int n;
int main(void) {
    cin >> n;
    if (n < 1) {
        cout << "NO" << endl;
        return 0;
    }
    vector<double> a(n, 0.0);
    vector<double> b(n, 0.0);
    vector<double> c;
    c.insert(c.end(), 0.5);
    c.insert(c.end(), 0.5);
    c.insert(c.end(), 0.5);
    int i, j;
    for (i = 0; i < n; ++i) {
        cin >> a[i];
        // a[i] -= c[i];
    }
    for (i = 0; i < n; ++i) {
        cin >> b[i];
        // b[i] -= c[i];
    }
    // Найдем скалярное произведение векторов:
    // scalar product
    double a_b_scalar_mult = scalar_product(a, b, n);
    double a_c_scalar_mult = scalar_product(a, c, n);
    double b_c_scalar_mult = scalar_product(b, c, n);
    cout << "scalar product(a, b) = "
         << a_b_scalar_mult << endl;
    cout << "scalar product(a, c) = "
         << a_c_scalar_mult << endl;
    cout << "scalar product(b, c) = "
         << b_c_scalar_mult << endl;
    double a_modulus_vect = modulus_vector(a, n);
    double b_modulus_vect = modulus_vector(b, n);
    double c_modulus_vect = modulus_vector(c, n);
    cout << "modulus of vector(a) = "
         << a_modulus_vect << endl;
    cout << "modulus of vector(b) = "
         << b_modulus_vect << endl;
    cout << "modulus of vector(c) = "
         << c_modulus_vect << endl;
    double a_b_cos = a_b_scalar_mult/(a_modulus_vect*b_modulus_vect);
    cout << "cos(a. b) = "
         << a_b_cos << endl;
    return 0;
    double a_mod_vect = 0.0;
    for (i = 0; i < n; ++i) {
        a_mod_vect = a_mod_vect + a[i]*a[i];
    }
    a_mod_vect = sqrt(a_mod_vect);
    cout << "modulus of vector(a) = " << a_mod_vect << endl;
    double b_mod_vect = 0.0;
    for (i = 0; i < n; ++i) {
        b_mod_vect = b_mod_vect + b[i]*b[i];
    }
    b_mod_vect = sqrt(b_mod_vect);
    cout << "modulus of vector(b) = " << b_mod_vect << endl;


    double a_b_lenght =  lenght(a, b, n);
    double a_c_lenght =  lenght(a, c, n);
    double b_c_lenght =  lenght(b, c, n);
    cout << "lenght(a, b) = "
         << a_b_lenght << endl;
    cout << "lenght(a, c) = "
         << a_c_lenght << endl;
    cout << "lenght(b, c) = "
         << b_c_lenght << endl;
    double cos_ca_cb = ( a_c_lenght*a_c_lenght
                        + b_c_lenght*b_c_lenght
                        - a_b_lenght*a_b_lenght ) /
                       (2*a_c_lenght*b_c_lenght);
    double alpha_ca_cb = acos(cos_ca_cb)*180/M_PI;
    cout << "cos(ca, cb) = "
         << cos_ca_cb << endl
         << "alpha(ca, cb) = "
         << alpha_ca_cb
         << endl;
    return 0;
}
/* vim: syntax=c:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
