/* step6.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cfloat>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif


using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

int n;
int main(void) {
    cin >> n;
    if (n < 1) {
        cout << "NO" << endl;
        return 0;
    }
    vector<double> a(n, 0.0);
    vector<double> b(n, 0.0);
    vector<double> c(n, 0.0);
    int i, j, k;
    for (i = 0; i < n; ++i) {
        cin >> a[i];
    }
    for (i = 0; i < n; ++i) {
        cin >> b[i];
    }
    for (i = 0; i < n; ++i) {
        cin >> c[i];
    }
    double a_b_sqr_sum = 0.0;
    double b_c_sqr_sum = 0.0;
    double a_c_sqr_sum = 0.0;
    for (i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        a_b_sqr_sum = a_b_sqr_sum + t*t;
        t = a[i] - c[i];
        a_c_sqr_sum = a_c_sqr_sum + t*t;
        t = b[i] - c[i];
        b_c_sqr_sum = b_c_sqr_sum + t*t;
    }
    double a_b = sqrt(a_b_sqr_sum);
    double a_c = sqrt(a_c_sqr_sum);
    double b_c = sqrt(b_c_sqr_sum);
    cout << "lenght(a,b) = " << a_b << endl;
    cout << "lenght(a,c) = " << a_c << endl;
    cout << "lenght(b,c) = " << b_c << endl;
    double p = (a_b + a_c + b_c)/2;
    cout << "p((a,b) + (a,c) + (b,c)/2) = " << p << endl;
    double s = sqrt(p*(p - a_b)*(p - a_c)*(p - b_c));
    cout << "S = sqrt(p*(p - (a,b))*(p - (a,c))*(p - (b,c))) = " << s << endl;
    if (areEqual(a_b, a_c) && areEqual(a_b, b_c) && areEqual(a_c, b_c))
        cout << "S = " << ((sqrt(3)/4)*(a_b*a_b)) << endl;
    return 0;
}
/* vim: syntax=c:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
