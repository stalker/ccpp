var
i,j,k,n,m           : integer ;
A                   : array [1..40,1..40] of real ;
B,X,Y,P             : array [1..20] of real ;
c,c1,F              : real ;
begin
  writeln('Введите  размерность массива:');
  read(n);
  writeln('Введите  степень полинома:');
  read(m);
  writeln(' Введите массив X');
  for i:=1 to n do
    read(x[i]);
  writeln(' Введите массив Y ');
  for i:=1 to n do
    read(y[i]);
  for i:=1 to m+1 do begin
    for j:=1 to m+1 do begin
      A[i,j]:=0;
      for k:=1 to n do
        A[i,j]:=A[i,j]+exp((i+j-2)*ln((x[k])))
    end;
    A[i,m+2]:=0;
    for k:=1 to n do
      A[i,m+2]:=A[i,m+2]+y[k]*exp((i-1)*ln(x[k]))
  end;
  for i:=1 to m+1 do begin  c:=A[i,i];
  for j:=1 to m+2 do
    A[i,j]:=A[i,j]/c;
  for k:= 1 to m+2 do
    if k<>i then begin
      C1:=A[k,i];
      for j:=1 to m+2 do
        A[k,j]:=A[k,j]-C1*A[i,j]
    end;
  end;
  writeln (  ' Параметры модели: '); 
  for i:=1 to m+1 do begin
    B[i]:=A[i,m+2];
    writeln ( i , ' –  ',  B[i]:6:4);
  end;
  writeln (  'Вычисленые значения для Р(х): '); 
  F:=0;
  for i:=1 to n do begin
    P[i]:=0;
    for k:=1 to m+1 do begin
      P[i]:=P[i]+B[k]*exp((k-1)*ln(x[i]))
    end;
    writeln ('P ', i ,'-ая  ', P[i]:6:4);
    F:=F+((P[i]-Y[i])*(P[i]-Y[i]));
  end;
  F:=SQRT(F/(n-1));
  writeln('Среднеквадратичное отклонение =', F:6:4);
end.
