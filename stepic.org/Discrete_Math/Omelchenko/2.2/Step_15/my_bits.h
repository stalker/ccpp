
/* $Date$
 * $Id$
 * $Version: 2.3$
 * $Revision: 1$
 */

#ifndef MY_BITS_H
#define MY_BITS_H

#if     _MSC_VER <= 1800 && _MSC_VER > 1000
#if     _INTEGRAL_MAX_BITS >= 64
#define OK_LLONG                       1
/* minimum signed 64 bit value */
#define LLONG_MIN                      _I64_MIN
/* maximum signed 64 bit value */
#define LLONG_MAX                      _I64_MAX
/* maximum unsigned 64 bit value */
#define ULLONG_MAX                     _UI64_MAX
#endif
#endif

/* BEGIN T_I16 T_U16 T_I32 T_U32 T_I64 T_U64 */
#if _MSC_VER <= 1800 && _MSC_VER > 1000
/* Microsoft Visual C++ 6 Integer types for float <fpieee.h> */
#include <fpieee.h>
#define OK_T_I16 1
#define OK_T_I32 1
#define OK_T_I64 1
#define T_I16 _I16
#define T_U16 _U16
#define T_I32 _I32
#define T_U32 _U32
#define T_I64 _Q64
#define T_U64 unsigned __int64
#else

#if ( (defined(__STDC__) && __STDC__ && __STDC_VERSION__ >= 199901L) \
    || (defined(__DMC__) && __DMC__ >= 0x800 ) \
    || (defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 2) \
    || (defined(_MSC_VER) && _MSC_VER >= 1600) \
    || (defined(__WATCOMC__) && __WATCOMC__ >= 1250 ) \
    || (defined(__xlC__) && __xlC__ >= 0x0800) )
/* ISO C99: 7.18 Integer types <stdint.h> */
#include <stdint.h>
#define OK_T_I16 1
#define OK_T_I32 1
#define T_I16 int16_t
#define T_U16 uint16_t
#define T_I32 int32_t
#define T_U32 uint32_t
#define T_I64 int64_t
#define T_U64 uint64_t
/* #endif __USE_ISOC99 && defined __GNUC__ else if */
#elif defined _ANSI_C_SOURCE /* AIX Ok */
#include <stdint.h>
#define OK_T_I16 1
#define OK_T_I32 1
#define T_I16 int16_t
#define T_U16 uint16_t
#define T_I32 int32_t
#define T_U32 uint32_t

#ifndef _STD_UINT64_T

#ifdef  __64BIT__
#define OK_T_I64 1
#define T_I64 int64_t
#define T_U64 uint64_t
#else   /* _ILP32 */
#if defined(_LONG_LONG)
#define OK_T_I64 1
#define T_I64 int64_t
#define T_U64 uint64_t
#endif /* _LONG_LONG */
#endif /* __64BIT__ */

#endif /* _STD_UINT64_T */

#endif /* __USE_ISOC99 && defined __GNUC__ */

#endif /* _MSC_VER */
/* END   T_I16 T_U16 T_I32 T_U32 T_I64 T_U64 */


int my_bitcount(unsigned int x);
unsigned int my_getbits(unsigned int x, unsigned int p, unsigned int n);
unsigned int my_invert(unsigned int x, int p, int n);
unsigned int my_leftrot(unsigned int x, int n);
unsigned int my_rightrot(unsigned int x, int n);
unsigned int my_setbits(unsigned int x, int p, int n, unsigned int y);
void my_puts_ui32_bits(T_U32 x);
void my_puts_ui32_bits_msg(char msg[], T_U32 x);
void my_puts_ui64_bits(T_U64 x);
void my_puts_ui64_bits_msg(char msg[], T_U64 x);
void my_puts_long_bits(unsigned long x);
void my_puts_uint_bits_msg(char msg[], unsigned int x);
void my_puts_uint_bits(unsigned int x);
void my_puts_ushort_bits(unsigned short x);

#endif /* MY_BITS_H */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
