/* step_15_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

using std::string;
using std::vector;
using namespace std;

typedef vector<vector<double> > Matrix;
typedef vector<double> Vector;

bool areEqual(double x, double y) {
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

double scalar_product(Vector & a, Vector & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

double modulus_vector(Vector & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

double lenght(Vector & a, Vector & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

int m, n;

bool exchange(Matrix & a, int i1, int i2) {
    if (n <= i1 || n <= i2) return false;
    vector<double> v;
    v = a[i1];
    a[i1] = a[i2];
    a[i2] = v;
    return true;
}

bool exchange_if0_at_diag_el(Matrix & a, int i1) {
    if (i1 >= n || i1 >= m) return false;
    // if (n <= i1) return false;
    int j;
    if (areEqual(a[i1][i1], 0.0)) {
        bool ok = false;
        if (i1 < (n - 1)) {
            for (j = i1 + 1; j < n && j < (m - 1); ++j) {
                if (!areEqual(a[i1][j], 0.0)) {
                    ok = exchange(a, i1, j);
                }
            }
        }
        return ok;
    }
    return true;
}

int first_pass_at(Matrix & a, int n1, int m1, int e1) {
    if (n < n1 || m < m1 || n <= e1) return false;
    double divider = a[e1][e1];
    if (areEqual(divider, 0.0)) {
        cout << "NO" << endl;
        exit(0);
    }
    int j;
    for (j = e1; j < m1; ++j) {
        a[e1][j] = a[e1][j] / divider;
    }
    int i;
    for (i = e1 + 1; i < n1; ++i) {
        double subt = a[i][e1] / a[e1][e1];
        for (j = e1; j < m1; ++j) {
            a[i][j] = a[i][j] - a[e1][j]*subt;
        }
    }
    return 1;
}

int first_pass(Matrix & a, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i;
    for (i = 0; i < (m1 - 1); ++i) {
        if (!exchange_if0_at_diag_el(a, i)) {
        }
        first_pass_at(a, n1, m1, i);
    }
    return 0;
}

int reversal(Vector & v, Matrix & a, int n1, int m1) {
    int i, j;
    for (i = (n1 - 1); i >= 0; --i) {
        v[i] = a[i][m - 1];
        for (j = (i + 1); j < (m1 - 1); ++j) {
            v[i] -= a[i][j] * v[j];
        }
    }
    return 0;
}

bool input_data(Vector & f, Matrix & e) {
    int i, j;
    cin >> n >> m;
    Matrix r;
    for (i = 0; i != n; ++i) {
        Vector t(m, 0.0);
        for (j = 0; j != m; ++j) {
            cin >> t[j];
        }
        r.push_back(t);
        f.push_back(0.0);
        cin >> f[i];
    }
    for (i = 0; i != m; ++i) {
        Vector t(n, 0.0);
        for (j = 0; j != n; ++j) {
            t[j] = r[j][i];
        }
        e.push_back(t);
    }
    return true;
}

bool method_least_squares(Matrix & a, Vector & f, Matrix & e, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i, j;
    for (i = 0; i != m1; ++i) {
        a.push_back(Vector(m1 + 1, 0.0));
        for (j = 0; j != m1; ++j) {
            a[i][j] = scalar_product(e[i], e[j], n1);
        }
        a[i][m1] = scalar_product(f, e[i], n1);
    }
    return true;
}

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    int i, j;
    Matrix a;
    Matrix e;
    Vector f;
    input_data(f, e);
    method_least_squares(a, f, e, n, m);
    int n = m++;
    first_pass(a, n, n + 1);
    Vector r(m, 0.0);
    reversal(r, a, n, n + 1);
    for (i = 0; i < n; ++i) {
        cout << setprecision(16) << r[i] << " ";
    }
    cout << endl;
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
