/*
 * Version 2.1.3
 */

// #include <stdio.h>
#include "my_bits.h"
#include "my_fd_ieee754.h"

union Bits_float
{   float F;
    unsigned I;
} bf;

union Bits_double
{   double D;
    unsigned long I;
} bd;

union Bits_float32
{   float F;
    T_U32 I;
} bf32;
union Bits_float64
{   float F;
    T_U64 I;
} bf64;

union Bits_double32
{   double D;
    T_U32 I;
} bd32;
union Bits_double64
{   double D;
    T_U64 I;
} bd64;


float get_plus_zero_float(void)
{
    if (sizeof (float) == 4) {
        bf32.I = PLUS_ZERO_UI32_HEX;
        return bf32.F;
    } else if (sizeof (float) == 8) {
        bf64.I = PLUS_ZERO_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

float get_minus_zero_float(void)
{
    if (sizeof (float) == 4) {
        bf32.I = MINUS_ZERO_UI32_HEX;
        return bf32.F;
    } else if (sizeof (float) == 8) {
        bf64.I = MINUS_ZERO_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

float get_plus_infinity_float(void)
{
    if (sizeof (float) == 4) {
        bf32.I = PLUS_INFINITY_UI32_HEX;
        return bf32.F;
    } else if (sizeof (float) == 8) {
        bf64.I = PLUS_INFINITY_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

float get_minus_infinity_float(void)
{
    if (sizeof (float) == 4) {
        bf32.I = MINUS_INFINITY_UI32_HEX;
        return bf32.F;
    } else if (sizeof (float) == 8) {
        bf64.I = MINUS_INFINITY_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

double get_plus_zero_double(void)
{
    if (sizeof (double) == 4) {
        bd32.I = PLUS_ZERO_UI32_HEX;
        return bd32.D;
    } else if (sizeof (double) == 8) {
        bd64.I = PLUS_ZERO_UI64_HEX;
        return bd64.D;
    }

    return 0;
}

double get_minus_zero_double(void)
{
    if (sizeof (double) == 4) {
        bd32.I = MINUS_ZERO_UI32_HEX;
        return bd32.D;
    } else if (sizeof (double) == 8) {
        bd64.I = MINUS_ZERO_UI64_HEX;
        return bd64.D;
    }

    return 0;
}

double get_plus_infinity_double(void)
{
    if (sizeof (double) == 4) {
        bf32.I = PLUS_INFINITY_UI32_HEX;
        return bf32.F;
    } else if (sizeof (double) == 8) {
        bf64.I = PLUS_INFINITY_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

double get_minus_infinity_double(void)
{
    if (sizeof (double) == 4) {
        bf32.I = MINUS_INFINITY_UI32_HEX;
        return bf32.F;
    } else if (sizeof (double) == 8) {
        bf64.I = MINUS_INFINITY_UI64_HEX;
        return bf64.F;
    }

    return 0;
}

int is_infinity_float(float f)
{
    if (sizeof (float) == 4) {
        bf32.F = f;
        if (PLUS_INFINITY_UI32_HEX == bf32.I || MINUS_INFINITY_UI32_HEX == bf32.I)
            return 1;
    } else if (sizeof (float) == 8) {
        bf64.F = f;
        if (PLUS_INFINITY_UI64_HEX == bf64.I || MINUS_INFINITY_UI64_HEX == bf64.I)
            return 1;
    }

    return 0;
}

int is_nan_float(float f)
{
    if (sizeof (float) == 4) {
        bf32.F = f;
        if (PLUS_NAN_UI32_MIN_HEX <= bf32.I && PLUS_NAN_UI32_MAX_HEX <= bf32.I)
            return 1;
    } else if (sizeof (float) == 8) {
        bf64.F = f;
        if (PLUS_NAN_UI64_MIN_HEX >= bf64.I && PLUS_NAN_UI64_MAX_HEX <= bf64.I)
            return 1;
    }

    return 0;
}

int is_infinity_double(double d)
{
    if (sizeof (double) == 4) {
        bd32.D = d;
        if (PLUS_INFINITY_UI32_HEX == bd32.I || MINUS_INFINITY_UI32_HEX == bd32.I)
            return 1;
    } else if (sizeof (double) == 8) {
        bd64.D = d;
        if (PLUS_INFINITY_UI64_HEX == bd64.I || MINUS_INFINITY_UI64_HEX == bd64.I)
            return 1;
    }

    return 0;
}

int is_nan_double(double d)
{
    if (sizeof (double) == 4) {
        bd32.D = d;
        if (PLUS_NAN_UI32_MIN_HEX >= bd32.I && PLUS_NAN_UI32_MAX_HEX <= bd32.I)
            return 1;
    } else if (sizeof (double) == 8) {
        bd64.D = d;
        if (PLUS_NAN_UI64_MIN_HEX >= bd64.I && PLUS_NAN_UI64_MAX_HEX <= bd64.I)
            return 1;
    }

    return 0;
}

/* power: raise base to n-th power; n >= 0; version 2 */
float power_float(float base, int n)
{
    float p;

    for (p = 1; n > 0; --n)
        p = p * base;

    return p;
}

/* power: raise base to n-th power; n >= 0; version 2 */
double power_double(double base, int n)
{
    double p;

    for (p = 1; n > 0; --n)
        p = p * base;

    return p;
}


void puts_float(char msg[], float f)
{
    //TODO printf("%-8s = %e\n", msg,  f);
    if (sizeof (float) == 4) {
        bf32.F = f;
        //TODO printf("%-8s = 0x%08x = %u\n", msg, bf32.I, bf32.I);
        //TODO printf("%-8s = 0b", msg);
        my_puts_ui32_bits(bf32.I);
    } else if (sizeof (float) == 8) {
        bf64.F = f;
        //TODO printf("%-8s = 0x%08x = %lu\n", msg, bf64.I, bf64.I);
        //TODO printf("%-8s = 0b", msg);
        my_puts_ui64_bits(bf64.I);
    }
}

void puts_double(char msg[], double d)
{
    //TODO printf("%-8s = %e\n", msg,  d);
    if (sizeof (double) == 4) {
        bd32.D = d;
        //TODO printf("%-8s = 0x%016x = %u\n", msg, bd32.I, bd32.I);
        //TODO printf("%-8s = 0b", msg);
        my_puts_ui32_bits(bd32.I);
    } else if (sizeof (double) == 8) {
        bd64.D = d;
        //TODO printf("%-8s = 0x%016lx = %lu\n", msg, bd64.I, bd64.I);
        //TODO printf("%-8s = 0b", msg);
        my_puts_ui64_bits(bd64.I);
    }
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
