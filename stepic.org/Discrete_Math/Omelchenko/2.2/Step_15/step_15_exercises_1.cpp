/* step_15_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "my_discr_vects.h"
#include "my_fd_ieee754.h"
#include "step_15.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using std::string;
using std::vector;
using namespace std;

typedef vector<vector<double> > Matrix;
typedef vector<double> Vector;
void cerr_matrix(Matrix &, int, int, const char *, const char *, int);

int m, n;

// функция exchange
// меняет строки местами
// вход: матрица Matrix & a
// номера строк:
//   откуда и куда - int i1
//   куда и откуда - int i2
bool exchange(Matrix & a, int i1, int i2) {
    if (n <= i1 || n <= i2) return false;
    vector<double> v;
    v = a[i1];
    a[i1] = a[i2];
    a[i2] = v;
    return true;
}

// функция exchange_0_at_diag_el
// проверяет является ли диагональный элемент 0
// если да пытается поменять строку на которой он найден с
// ниже стоящей строкой
// (Matrix & a, int n1, int m1, int i1) {
// вход: матрица Matrix & a
// номера строк:
//   откуда и куда - int i1
//   куда и откуда - int i2
bool exchange_if0_at_diag_el(Matrix & a, int i1) {
    if (i1 >= n || i1 >= m) return false;
    // if (n <= i1) return false;
    int j;
    if (areEqual(a[i1][i1], 0.0)) {
        bool ok = false;
        if (i1 < (n - 1)) {
            for (j = i1 + 1; j < n && j < (m - 1); ++j) {
                if (!areEqual(a[i1][j], 0.0)) {
                    ok = exchange(a, i1, j);
                }
            }
        }
        return ok;
    }
    return true;
}

int first_pass_at(Matrix & a, int n1, int m1, int e1) {
    if (n < n1 || m < m1 || n <= e1) return false;
    double divider = a[e1][e1];
    if (areEqual(divider, 0.0)) {
        cout << "NO" << endl;
        exit(0);
    }
    int j;
    for (j = e1; j < m1; ++j) {
        a[e1][j] = a[e1][j] / divider;
    }
    int i;
    for (i = e1 + 1; i < n1; ++i) {
        double subt = a[i][e1] / a[e1][e1];
        for (j = e1; j < m1; ++j) {
            a[i][j] = a[i][j] - a[e1][j]*subt;
        }
    }
    return 1;
}

// ф-ция: first_pass
// первый проход, приводит матрицу к ступенчатому виду
int first_pass(Matrix & a, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i;
    for (i = 0; i < (m1 - 1); ++i) {
        if (!exchange_if0_at_diag_el(a, i)) {
        }
        first_pass_at(a, n1, m1, i);
#ifdef DEBUG
        cerr_matrix(a, n1, m1, __func__, "a", __LINE__);
#endif
    }
    return 0;
}

int reversal(Vector & v, Matrix & a, int n1, int m1) {
    // if (m > m1 || n > n1) return 0;
    int i, j;
    for (i = (n1 - 1); i >= 0; --i) {
        v[i] = a[i][m - 1];
        for (j = (i + 1); j < (m1 - 1); ++j) {
            v[i] -= a[i][j] * v[j];
        }
    }
#ifdef DEBUG
        cerr_matrix(a, n1, m1, __func__, "a", __LINE__);
#endif
    return 0;
}

// функция input_maxtrix
// читает матрицу n на m со стандартного ввода
// вход: матрица Matrix & a
// номера строк:
//   откуда вычитаем - int from
//   что вычитаем    - int subt
bool input_data(Vector & f, Matrix & e) {
    int i, j;
    cin >> n >> m;
    Matrix r;
    for (i = 0; i != n; ++i) {
        Vector t(m, 0.0);
        for (j = 0; j != m; ++j) {
            cin >> t[j];
        }
        r.push_back(t);
        f.push_back(0.0);
        cin >> f[i];
    }
    for (i = 0; i != m; ++i) {
        Vector t(n, 0.0);
        for (j = 0; j != n; ++j) {
            t[j] = r[j][i];
        }
        e.push_back(t);
    }
    return true;
}

bool method_least_squares(Matrix & a, Vector & f, Matrix & e, int n1, int m1) {
    if (n < n1 || m < m1) return false;
    int i, j;
    /*
    for (i = 0; i != m; ++i) {
        for (j = 0; j != n; ++j) {
            cout << "e[" << i << "][" << j << "] = "
                 << FF_11_6 << e[i][j] << " | ";
        }
        cout << " f[" << i << "] = "
             << FF_11_6 << f[i] << endl;
    }
    */
    for (i = 0; i != m1; ++i) {
        a.push_back(Vector(m1 + 1, 0.0));
        for (j = 0; j != m1; ++j) {
            a[i][j] = scalar_product(e[i], e[j], n1);
            // cout << "(e" << i << ",e" << j << ") = "
            //      << FF_11_6 << a[i][j] << " | ";
            // cout << ((j == (m - 1)) ? "" : " | ");
        }
        a[i][m1] = scalar_product(f, e[i], n1);
        // cout << "(f,e" << i << ") = "
        //      << FF_11_6 << a[i][m];
        // cout << endl;
    }
    return true;
}

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    int i, j;
    Matrix a;
    Matrix e;
    Vector f;
    input_data(f, e);
    for (i = 0; i != m; ++i) {
        for (j = 0; j != n; ++j) {
            cout << "e[" << i << "][" << j << "] = "
                 << FF_11_6 << e[i][j] << " | ";
        }
        cout << " f[" << i << "] = "
             << FF_11_6 << f[i] << endl;
    }
    method_least_squares(a, f, e, n, m);
    for (i = 0; i != m; ++i) {
        for (j = 0; j != m; ++j) {
            cout << "(e" << i << ",e" << j << ") = "
                 << FF_11_6 << a[i][j] << " | ";
            // cout << ((j == (m - 1)) ? "" : " | ");
        }
        cout << "(f,e" << i << ") = "
             << FF_11_6 << a[i][m];
        cout << endl;
    }
    int n = m++;
    first_pass(a, n, n + 1);
    Vector r(m, 0.0);
    reversal(r, a, n, n + 1);
    for (i = 0; i < n; ++i) {
        cout << setprecision(16) << r[i] << " ";
    }
    cout << endl;
    return 0;
    vector<double> f1;
    vector<double> e1;
    vector<double> e2;
    e1.push_back(5);
    e1.push_back(3);
    e1.push_back(8);
    e1.push_back(2);
    cout << "(e1,e1) = "
         << FF_11_6 << scalar_product(e1, e1, n) << " | ";
    e2.push_back(7);
    e2.push_back(2);
    e2.push_back(4);
    e2.push_back(6);
    cout << "(e1,e2) = "
         << FF_11_6 << scalar_product(e1, e2, n) << " | ";
    cout << "(e2,e1) = "
         << FF_11_6 << scalar_product(e2, e1, n) << endl;
    cout << "(e2,e2) = "
         << FF_11_6 << scalar_product(e2, e2, n) << " | ";
    f1.push_back(6);
    f1.push_back(1);
    f1.push_back(3);
    f1.push_back(0);
    cout << "(f1,e1) = "
         << FF_11_6 << scalar_product(f1, e1, n) << " | ";
    cout << "(f2,e2) = "
         << FF_11_6 << scalar_product(f1, e2, n) << endl;
    // Ok TODO method Gaussa

    return 0;
}

void cerr_matrix
(Matrix & a, int n1, int m1, const char * f, const char * var, int l) {
#ifdef DEBUG
    int i, j;
    for (i = 0; i < n1; ++i) {
        cerr << __FILE__ << ':' << l;
        cerr << ":" << f << " | ";
        for (j = 0; j < m1 - 1; ++j) {
            cerr_X_V_Y(a[, i,]);
            cerr_X_V_Y([, j,]=);
            cerr.width(4);
            cerr << a[i][j] << " | ";
        }
        cerr_X_V_Y(a[, i,]);
        cerr_X_V_Y([, j,]);
        cerr_X_V_nl(=, a[i][j]);
    }
#endif
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
