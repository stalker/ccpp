
/* $Date$
 * $Id$
 * $Version: 2.1.3$
 * $Revision: 1$
 */

#ifndef MY_FD_IEEE754_H
#define MY_FD_IEEE754_H

#define PLUS_ZERO_UINT_HEX      0x00000000U
#define MINUS_ZERO_UINT_HEX     0x80000000U
#define PLUS_INFINITY_UINT_HEX  0x7F800000U
#define MINUS_INFINITY_UINT_HEX 0xFF800000U
#define PLUS_NAN_UINT_MIN       0x7F800001U
#define PLUS_NAN_UINT_MAX       0x7FFFFFFFU
#define MINUS_NAN_UINT_MIN      0xFF800001U
#define MINUS_NAN_UINT_MAX      0xFFFFFFFFU

#define PLUS_DOUBLE_ZERO_UINT_HEX      0x0000000000000000UL
#define MINUS_DOUBLE_ZERO_UINT_HEX     0x8000000000000000UL
#define PLUS_DOUBLE_INFINITY_UINT_HEX  0x7FF0000000000000UL
#define MINUS_DOUBLE_INFINITY_UINT_HEX 0xFFF0000000000000UL
#define PLUS_DOUBLE_NAN_UINT_MIN       0x7FF0000000000001UL
#define PLUS_DOUBLE_NAN_UINT_MAX       0x7FFFFFFFFFFFFFFFUL
#define MINUS_DOUBLE_NAN_UINT_MIN      0xFFF0000000000001UL
#define MINUS_DOUBLE_NAN_UINT_MAX      0xFFFFFFFFFFFFFFFFUL

#define PLUS_ZERO_UI32_HEX             0x00000000U
#define MINUS_ZERO_UI32_HEX            0x80000000U
#define PLUS_INFINITY_UI32_HEX         0x7F800000U
#define MINUS_INFINITY_UI32_HEX        0xFF800000U
#define PLUS_NAN_UI32_MIN_HEX          0x7F800001U
#define PLUS_NAN_UI32_MAX_HEX          0x7FFFFFFFU
#define MINUS_NAN_UI32_MIN_HEX         0xFF800001U
#define MINUS_NAN_UI32_MAX_HEX         0xFFFFFFFFU

#define PLUS_ZERO_UI64_HEX             0x0000000000000000UL
#define MINUS_ZERO_UI64_HEX            0x8000000000000000UL
#define PLUS_INFINITY_UI64_HEX         0x7FF0000000000000UL
#define MINUS_INFINITY_UI64_HEX        0xFFF0000000000000UL
#define PLUS_NAN_UI64_MIN_HEX          0x7FF0000000000001UL
#define PLUS_NAN_UI64_MAX_HEX          0x7FFFFFFFFFFFFFFFUL
#define MINUS_NAN_UI64_MIN_HEX         0xFFF0000000000001UL
#define MINUS_NAN_UI64_MAX_HEX         0xFFFFFFFFFFFFFFFFUL

#define FD_DOUBLE_MAXLOOP 99999
#define FD_FLOAT_MAXLOOP 99999

#include "my_bits.h"

float get_plus_zero_float(void);
float get_minus_zero_float(void);
float get_plus_infinity_float(void);
float get_minus_infinity_float(void);

double get_plus_zero_double(void);
double get_minus_zero_double(void);
double get_plus_infinity_double(void);
double get_minus_infinity_double(void);

int is_infinity_float(float f);
int is_nan_float(float f);
int is_infinity_double(double d);
int is_nan_double(double d);

float power_float(float base, int n);
double power_double(double base, int n);

void puts_double(char msg[], double d);
void puts_float(char msg[], float f);

#endif /* MY_FD_IEEE754_H */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
