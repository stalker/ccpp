/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Вычислите длину проекции вектора (1,2,7) на подпространство, базисом
 * которого являются векторы (1,1,−2) и (1,−1,4). 
 * Ответ округлите до третьего знака после запятой.
 * -11 =  6∙ε₁ -  8∙ε₂
 *  27 = -8∙ε₁ + 18∙ε₂
 *  ε₁ = 9/22
 *  ε₂ = 37/22
 *  v₃ = ε₁∙(1, 1, -2) + ε₂∙(1, -1, 4)
 *  v₃ = (23/11, -14/11, 65/11)
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "step_12.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}
mpf_class scalar_product(vector<mpf_class> & a, vector<mpf_class> & b, int n) {
    mpf_class scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}
mpf_class modulus_vector(vector<mpf_class> & a, int n) {
    mpf_class mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}
mpf_class lenght(vector<mpf_class> & a,vector<mpf_class> & b, int n) {
    mpf_class lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        mpf_class t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

int n = 3;
int bits = 1000;
double arr1[] = {1.0, 1.0, -2.0};
double arr2[] = {1.0, -1.0, 4.0};

void init_mpf_class_vec(vector<mpf_class> & v, double * a, int n) {
    for (int i = 0; i < n; ++i)
        v.push_back(mpf_class(a[i], bits));
    return;
}

/* let's go */
int main(int argc, char *argv[]) {
    int i;
    vector<mpf_class> vec1;
    vector<mpf_class> vec2;
    vector<mpf_class> vec3;
    vec3.push_back(mpf_class(23, bits));
    vec3.push_back(mpf_class(-14, bits));
    vec3.push_back(mpf_class(65, bits));
    vec3[0] = vec3[0] / 11;
    vec3[1] = vec3[1] / 11;
    vec3[2] = vec3[2] / 11;
    init_mpf_class_vec(vec1, arr1, n);
    init_mpf_class_vec(vec2, arr2, n);
    mpf_class vec1_mod = modulus_vector(vec1, n);
    mpf_class vec2_mod = modulus_vector(vec2, n);
    mpf_class v1v2_scp = scalar_product(vec1, vec2, n);
    mpf_class vec3_mod = modulus_vector(vec3, n);
    cout << FF_11_6
         << "vec3_mod = " << vec3_mod
         << endl;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
