/* step7.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * С использованием процедуры Грама-Шмидта получите ортонормированный базис из
 * базиса вида f₁ = (0, 0, 1), f₂ = (0, 1, 1), f₃ = (1, 1, 1)
 * В качестве результата введите через пробел координаты полученных векторов,
 * округленные до третьего знака после запятой.
 */

#include <cmath>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

#include <iostream>
#include <math.h>
using namespace std;

// example: http://www.mia.uni-saarland.de/Teaching/NAVC-SS11/sol_c8.pdf
// page 5

double a[3][3] = {
    {0.0, 0.0, 1.0}, // f₁
    {0.0, 1.0, 1.0}, // f₂
    {1.0, 1.0, 1.0}  // f₃
};
// any column of a is a vector

double r[3][3], q[3][3];

int main(int argc, char *argv[]) {
    int k, i, j;
    for (k = 0; k < 3; k++) {
        r[k][k]=0; // equivalent to sum = 0
        for (i=0; i<3; i++)
            // rkk = sqr(a0k) + sqr(a1k) + sqr(a2k) 
            r[k][k] = r[k][k] + a[i][k] * a[i][k];
        r[k][k] = sqrt(r[k][k]);  // ||a||
        cout << endl
             << "R" << k << k
             << ": " << r[k][k];
        for (i = 0; i < 3; i++) {
            q[i][k] = a[i][k]/r[k][k];
            cout << " q" << i << k
                 << ": " << q[i][k] << " "; // result
        }
        for(j = k + 1; j < 3; j++) {
            r[k][j] = 0;
            for(i = 0; i < 3; i++)
                r[k][j] += q[i][k] * a[i][j];
            cout << endl
                 << "r" << k << j
                 << ": " << r[k][j]
                 << endl;
            for (i = 0; i < 3; i++)
                a[i][j] = a[i][j] - r[k][j]*q[i][k];
            for (i = 0; i < 3; i++)
                cout << "a"<<j<<": " << a[i][j]<< " ";
        }
    }
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
