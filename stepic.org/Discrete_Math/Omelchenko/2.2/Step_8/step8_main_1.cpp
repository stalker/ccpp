/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "step8.h"
#include "mydebug.h"

using std::string;
using std::vector;
using namespace std;

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}
mpf_class scalar_product(vector<mpf_class> & a, vector<mpf_class> & b, int n) {
    mpf_class scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}
mpf_class modulus_vector(vector<mpf_class> & a, int n) {
    mpf_class mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}
mpf_class lenght(vector<mpf_class> & a,vector<mpf_class> & b, int n) {
    mpf_class lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        mpf_class t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

#include <iostream>
#include <math.h>
using namespace std;

// example: http://www.mia.uni-saarland.de/Teaching/NAVC-SS11/sol_c8.pdf
// page 5

int bits = 10000;
int n = 3;
double aa[3][3] = {   // any column of a is a vector
    {1.0, 1.0, 2.0},  // x1, y1, z1
    {2.0, 3.0, 1.0},  // x2, y2, z2
    {2.0, 1.0, 1.0}   // x3, y3, z3
};

vector<vector<mpf_class> > a;

vector<vector<mpf_class> > r, q;

/* let's go */
int main(int argc, char *argv[]) {
    int k, i, j;
    for (i = 0; i < n; ++i) {
        vector<mpf_class> t;
        for (j = 0; j < n; ++j) {
            t.push_back(0.0);
        }
        a.push_back(t);
        r.push_back(t);
        q.push_back(t);
    }
    for (i = 0; i < n; ++i) {
        vector<mpf_class> t;
        for (j = 0; j < n; ++j) {
            a[i][j] = mpf_class(aa[i][j], bits);
            // a[i][j] = aa[i][j];
        }
        cout << endl;
    }
    cout.precision(16);
    for (k = 0; k < 3; k++) {
        r[k][k] = 0.0; // equivalent to sum = 0
        for (i = 0; i < 3; i++)
            // rkk = sqr(a0k) + sqr(a1k) + sqr(a2k) 
            r[k][k] = r[k][k] + a[i][k] * a[i][k];
        r[k][k] = sqrt(r[k][k]);  // ||a||
        cout << endl
             << "R" << k << k << ": "
             << (1e5 > r[k][k] ?
                     setiosflags(ios::fixed) :
                     setiosflags(ios::scientific))
             << setiosflags(ios::right)
             << setw(15)
             << setprecision(10) 
             << setfill(' ')
             << r[k][k].get_mpf_t();
            // gmp_printf("% 20.10Ff",  r[k][k].get_mpf_t());
        for (i = 0; i < 3; i++) {
            q[i][k] = a[i][k]/r[k][k];
            cout << " q" << i << k << ": "
                 << (1e5 > q[i][k] ?
                         setiosflags(ios::fixed) :
                         setiosflags(ios::scientific))
                 << setiosflags(ios::right)
                 << setw(15)
                 << setprecision(10) 
                 << setfill(' ')
                 << q[i][k].get_mpf_t();
            // gmp_printf("% 20.10Ff",  q[i][k].get_mpf_t());
            cout << ((n == i + 1) ? "" : " " );
        }
        for(j = k + 1; j < 3; j++) {
            r[k][j] = 0;
            for(i = 0; i < 3; i++)
                r[k][j] += q[i][k] * a[i][j];
            cout << endl
                 << "r" << k << j << ": "
                 << (1e5 > r[k][j] ?
                         setiosflags(ios::fixed) :
                         setiosflags(ios::scientific))
                 << setiosflags(ios::right)
                 << setw(15)
                 << setprecision(10) 
                 << setfill(' ')
                 << r[k][j].get_mpf_t();
            // gmp_printf("% 15.10Ff", r[k][j].get_mpf_t());
            for (i = 0; i < 3; i++)
                a[i][j] = a[i][j] - r[k][j]*q[i][k];
            for (i = 0; i < 3; i++) {
                cout << "  a" << j << ": "
                     << (1e5 > a[i][j] ?
                             setiosflags(ios::fixed) :
                             setiosflags(ios::scientific))
                     << setiosflags(ios::right)
                     << setw(15)
                     << setprecision(10) 
                     << setfill(' ')
                     << a[i][j].get_mpf_t();
                // gmp_printf("% 20.10Ff", a[i][j].get_mpf_t());
                cout << ((n == i + 1) ? "" : " " );
            }
        }
    }
    cout << endl;
    vector<double> ad;
    ad.push_back(4080000.0);
    ad.push_back(4080000.0);
    ad.push_back(8170000.0);
    double ad_modulus_vector = modulus_vector(ad, 3);
    cout << "ad_modulus_vector = "
         << (1e3 > ad_modulus_vector ?
                 setiosflags(ios::fixed) :
                 setiosflags(ios::scientific))
         << setiosflags(ios::right)
         << setw(11)
         << setprecision(6) 
         << setfill(' ')
         << ad_modulus_vector << endl;
    vector<mpf_class> am;
    am.push_back(mpf_class(4080000.0, bits));
    am.push_back(mpf_class(4080000.0, bits));
    am.push_back(mpf_class(8170000.0, bits));
    mpf_class am_modulus_vector = modulus_vector(am, 3);
    cout << "am_modulus_vector = "
         << setiosflags(ios::fixed)
         << setiosflags(ios::right)
         << setw(11)
         << setprecision(6) 
         << setfill(' ')
         << am_modulus_vector << endl;
    // gmp_printf("% 11.6Ff", am_modulus_vector.get_mpf_t());
    //
    vector<mpf_class> bm;
    bm.push_back(mpf_class(8170000.0, bits));
    bm.push_back(mpf_class(4080000.0, bits));
    bm.push_back(mpf_class(8170000.0, bits));
    mpf_class am_bm_scalar_product = scalar_product(am, bm, n);
    cout << "a_b_scalar_product = "
         << am_bm_scalar_product << endl;
    cout << endl;
    return EXIT_SUCCESS;
}
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
