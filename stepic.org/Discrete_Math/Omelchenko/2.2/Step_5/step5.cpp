/* step7.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * Молекула метана CH4 расположена в пространстве в виде пирамиды, в центре
 * которой находится атом углерода, а в вершинах — атомы водорода. Если вершины
 * разместить в точках с координатами (0,0,0), (1,1,0), (1,0,1) и (0,1,1), то
 * получится правильный тетраэдр, длины всех ребер которого равны 2√.  Каким
 * будет косинус угла между любыми двумя лучами, идущими из центра тетраэдра,
 * расположенного в точке (1/2, 1/2, 1/2), к вершинам этого тетраэдра?
 */

#include <cmath>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

int n = 3;
int main(void) {
    if (n < 1) {
        cout << "NO" << endl;
        return 0;
    }
    vector<double> a;
    vector<double> b;
    vector<double> c;
    a.push_back(1/sqrt(3));
    a.push_back(1/sqrt(3));
    a.push_back(1/sqrt(3));
    b.push_back(1/sqrt(14));
    b.push_back(2/sqrt(14));
    b.push_back(-3/sqrt(14));
    c.push_back(-5/sqrt(42));
    c.push_back(2*sqrt(2/21));
    c.push_back(1/sqrt(42));
    int i, j;
    double a_modulus_vector = modulus_vector(a, n);
    cout << "a_modulus_vector = "
         << a_modulus_vector << endl;
    double b_modulus_vector = modulus_vector(b, n);
    cout << "b_modulus_vector = "
         << b_modulus_vector << endl;
    double c_modulus_vector = modulus_vector(c, n);
    cout << "c_modulus_vector = "
         << c_modulus_vector << endl;
    double a_b_scalar_product = scalar_product(a, b, n);
    cout << "a_b_scalar_product = "
         << a_b_scalar_product << endl;
    double a_c_scalar_product = scalar_product(a, c, n);
    cout << "a_c_scalar_product = "
         << a_c_scalar_product << endl;
    double b_c_scalar_product = scalar_product(b, c, n);
    cout << "b_c_scalar_product = "
         << b_c_scalar_product << endl;
    return 0;
}
/* vim: syntax=c:ff=unix:textwidth=76:ts=4:sw=4:sts=4:et
 * EOF */
