/* step_14_exercises_1.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* При помощи метода наименьших квадратов найдите наилучшее решение системы
 * уравнений:
 * x - y = 4
 * x     = 5
 * x + y = 9
 * В качестве ответа укажите значение суммы квадратов отклонений.
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "my_fd_ieee754.h"
#include "my_discr_vects.h"
#include "step_14.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using std::string;
using std::vector;
using namespace std;

int n = 3;
int bits = 1000;
double arr1[] = {1.0, 1.0, 1.0};
double arr2[] = {-1.0, 0.0, 1.0};
double arr3[] = {4.0, 5.0, 9.0};

void init_mpf_class_vec(vector<mpf_class> & v, double * a, int n) {
    for (int i = 0; i < n; ++i)
        v.push_back(mpf_class(a[i], bits));
    return;
}

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cout << endl;
    vector<mpf_class> e1;
    vector<mpf_class> e2;
    vector<mpf_class> f;
    init_mpf_class_vec(e1, arr1, n);
    init_mpf_class_vec(e2, arr2, n);
    init_mpf_class_vec(f,  arr3, n);
    mpf_class f_e1  = scalar_product(f,  e1, n);
    mpf_class f_e2  = scalar_product(f,  e2, n);
    mpf_class e1_e1 = scalar_product(e1, e1, n);
    mpf_class e1_e2 = scalar_product(e1, e2, n);
    mpf_class e2_e1 = scalar_product(e2, e1, n);
    mpf_class e2_e2 = scalar_product(e2, e2, n);
    cout << "(f,  e1) = " << FF_11_6 << f_e1 << endl
         << "(f,  e2) = " << FF_11_6 << f_e2 << endl
         << "(e1, e1) = " << FF_11_6 << e1_e1 << endl
         << "(e1, e2) = " << FF_11_6 << e1_e2 << endl
         << "(e2, e1) = " << FF_11_6 << e2_e1 << endl
         << "(e2, e2) = " << FF_11_6 << e2_e2 << endl
         << endl;

    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
