
/* $Date$
 * $Id$
 * $Version: 2.3.3$
 * $Revision: 1$
 */

#include <stdio.h>
#include "my_bits.h"

/* bitcount: count 1 bits in x */
int my_bitcount(unsigned int x)
{
    int b;

    for (b = 0; x != 0; x >>= 1) if (x & 01)
        b++;

    return b;
}

/* getbits:  извлекает n бит, начиная с p-й позиции */
/*   Предпологая, что позиция 0 находится на правом */
/*  на правом  краю числа и что  n и p - корректные */
/*  положительные числа */
unsigned int my_getbits(unsigned int x, unsigned int p, unsigned int n) {
  /*
  unsigned int p1 = p + 1 - n;
  unsigned int s1 = (x >> p1);
  unsigned int a2 = ~(~0 << n);
  unsigned int s2 = s1 & a2;
  return s2; */
  return (x >> (p + 1 - n)) & ~(~0 << n);
}


void my_puts_ushort_bits(unsigned short x)
{
    int i;

    for (i = sizeof(unsigned short)*8 - 1 ; i >= 0; i--) {
        char c = ((x >> i) & ~(~0 << 1)) ? '1' : '0' ;
        putchar(c);
    }
}

void my_puts_ui32_bits(T_U32 x)
{
    int i;

    for (i = sizeof(T_U32)*8 - 1 ; i >= 0; i--) {
        char c = ((x >> i) & ~(~0 << 1)) ? '1' : '0' ;
        putchar(c);
    }
}

void my_puts_ui32_bits_msg(char msg[], T_U32 x)
{
    printf("%-18s = 0b", msg);
    my_puts_uint_bits(x);
#ifdef PRIu32
    printf(" = %10" PRIu32, x);
# ifdef PRIx32
    printf(" = 0x%08" PRIx32 "\n", x);
# endif
#else
    printf(" = %10u", x);
    printf(" = 0x%08x\n", x);
#endif
}

void my_puts_ui64_bits(T_U64 x)
{
  int i;

  for (i = sizeof(T_U64)*8 - 1 ; i >= 0; i--) {
    char c = ((x >> i) & ~(~0 << 1)) ? '1' : '0' ;
    putchar(c);
  }
}

void my_puts_ui64_bits_msg(char msg[], T_U64 x)
{
    printf("%-18s = 0b", msg);
    my_puts_uint_bits(x);
#ifdef PRIu64
    printf(" = %10" PRIu64, x);
# ifdef PRIx64
    printf(" = 0x%08" PRIx64 "\n", x);
# endif
#else
    printf(" = %10lu", x);
    printf(" = 0x%08x\n", x);
#endif
}

void my_puts_long_bits(unsigned long x)
{
    int i;

    for (i = sizeof(unsigned long)*8 - 1 ; i >= 0; i--) {
        char c = ((x >> i) & ~(~0 << 1)) ? '1' : '0' ;
        putchar(c);
    }
}

void my_puts_uint_bits(unsigned int x)
{
    int i;

    for (i = sizeof(unsigned int)*8 - 1 ; i >= 0; i--) {
        char c = ((x >> i) & ~(~0 << 1)) ? '1' : '0' ;
        putchar(c);
    }
}

void my_puts_uint_bits_msg(char msg[], unsigned int x)
{
    printf("%-18s = 0b", msg);
    my_puts_uint_bits(x);
    printf(" = %10u", x);
    printf(" = 0x%08x\n", x);
}

/* my_setbits: x получает n правых бит из y, начиная с p-й позиции */
/* прототип первый.
 * ( позиции справа на лево, первая позиция 0 )
 * shift переменная для сдвига значения y и маски mask1
 *       на позицию p
 * необходимо создать маску
 * mask1 маска из n единиц в правой части, остальное 0
 * вкратце: алгорим накладывает маски на x и y
 *          для x обнуляет именяемые биты (n-бит) с позиции p
 *          для y обнуляет биты за исключением n-правых бит
 *          сдвигает y, n-правых бит на позицию p
 *          битовой оп-цией или переносит из y в x n-бит с позиции p
 * 1. вычисляем переменную для сдвига y и маски
 * 2. вычисляем маску mask1
 * 3. временная маска maskp из n единиц с позиции p
 * 4. накладываем на x перевёрнутую маску maskp (обнуляем (n-бит) с позиции p)
 * 5. накладываем на y маску mask1 и сдвигаем у на позицию p
 * 6. переносим из y n-бит с позиции p в битовой операцией или
 */
unsigned int my_setbits(unsigned int x, int p, int n, unsigned int y)
{
    int shift = p + 1 - n;
    unsigned int mask1 = ~(~0 << n);

    return (x & ~(mask1 << shift)) | ((y & mask1) << shift);
}

/* my_invert: возвращает значение х в котором инвертированы n битов, начиная
 * с позиции p. Остальные биты не изменяются. */
/* прототип первый.
 * необходимо создать маску
 * mask1 маска из n единиц в правой части, остальное 0
 * shift переменная для сдвига значения маски mask1 на позицию p
 * вкратце: алгорим накладывает маску на x
 * сдвигает mask1, n-правых бит на позицию p
 * битовой оп-цией исключающее или инвертирует в x n битов начиная с позиции p.
 */
unsigned int my_invert(unsigned int x, int p, int n) {
    int shift = p + 1 - n;
    unsigned int mask1 = ~(~0 << n) << shift;

    return x ^ mask1;
}

/* my_leftrot: циклически сдвигает x влево на n разрядов. */
/*
 *
 */
unsigned int my_leftrot(unsigned int x, int n)
{
  unsigned int i, t;
  unsigned int mask0 = ~(~0U << 1);
  unsigned int maskl = ~(~0U >> 1);

  for (i = 0; i < n; i++) {
    t = 0; t = x & maskl;
    if (t) {
      x = (x << 1) | mask0;
    } else {
      x = x << 1 ;
    }
  }

  return x;
}

/* my_rightrot: циклически сдвигает x вправо на n разрядов. */
/*
 *
 */
unsigned int my_rightrot(unsigned int x, int n)
{
  int uint_bit_size = my_bitcount(~0U) - 1;
  int n1 = n % uint_bit_size;
  unsigned int t = my_setbits(0, uint_bit_size, n, x);

  return (x >> n) | t;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
