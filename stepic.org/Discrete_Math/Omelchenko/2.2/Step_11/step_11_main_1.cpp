/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Найдите значение α, при котором расстояние от (α,α,α) до (2,4,1) минимально.
 * Ответ округлите до третьего знака после запятой.
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "step_11.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

using std::string;
using std::vector;
using namespace std;

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}
mpf_class scalar_product(vector<mpf_class> & a, vector<mpf_class> & b, int n) {
    mpf_class scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}
mpf_class modulus_vector(vector<mpf_class> & a, int n) {
    mpf_class mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}
mpf_class lenght(vector<mpf_class> & a,vector<mpf_class> & b, int n) {
    mpf_class lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        mpf_class t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

int n = 3;
int bits = 1000;
double arr1[] = {2, 4, 1};

/* let's go */
int main(int argc, char *argv[]) {
    int i;
    vector<mpf_class> vec1;
    for (i = 0; i < n; ++i)
        vec1.push_back(mpf_class(arr1[i], bits));
    mpf_class end(2.4, bits);
    mpf_class itr(2.2, bits);
    mpf_class step(0.001, bits);
    vector<mpf_class> res1;
    res1.push_back(mpf_class(0.0, bits));
    res1.push_back(mpf_class(9.0, bits));
    while ((itr += step) < end) {
        vector<mpf_class> vec2;
        for (i = 0; i < n; ++i)
            vec2.push_back(itr);
        mpf_class y = lenght(vec1, vec2, n);
        cout << FF_11_6
             << "itr = "  << itr
             << " , y = " << y
             << endl;
        if (res1[1] > y) {
            res1[0] = itr;
            res1[1] = y;
        }
    }
    cout << FF_11_6
         << "res = "  << res1[0]
         << " , y = " << res1[1]
         << endl;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
