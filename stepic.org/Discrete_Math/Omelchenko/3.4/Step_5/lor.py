#!/usr/bin/env python3
def comb(x,l):
    if x==0 or len(l) < x: raise Exception
    elif len(l) == x: yield l
    elif x==1:
        for e in l: yield [e,]
    else: # len(l) > x:
        for i in xrange(len(l)-x+1):
            for m in comb(x-1,l[i+1:]):
                yield [l[i]]]+m
comb(2, 3)
