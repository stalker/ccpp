/* step_5_exercises_1.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Нужно сгенерировать все возможные k-сочетания из n-элементов.
 *
 * Формат входные данные:
 * Два числа k и n через пробел. Для них гарантированно выполняется
 * условие: 0≤k≤n. 
 *
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из
 * которых содержится k чисел от 0 до n, разделенных пробелом. 
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "my_fd_ieee754.h"
#include "step_5.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using std::string;
using std::vector;
using namespace std;

typedef vector<int> IntVector;
typedef vector<int>::iterator ItIVector;

bool next_combination(IntVector & a, int n) {
    int k = (int)a.size();
    for (int i = k - 1; i >= 0; --i)
        if (a[i] < (n - k + i + 1)) {
            ++a[i];
            for (int j = i + 1; j < k; ++j)
                a[j] = a[j-1] + 1;
            return true;
        }
    return false;
}

int n = 2;
int k;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cin >> k;
    cin >> n; n--;
    if (0 == k)
        return 0;

    IntVector v(k, 0);
    ItIVector it1 = v.begin();
    ItIVector end = v.end();
    for (int i = 0; it1 != end; ++i, ++it1)
        *it1 = i;

    do {
        it1 = v.begin();
        for (int i = 0; i < k and it1 != end; ++i, ++it1) {
            cout << *it1
                 << ((it1 == (end - 1))
                      || (i == (k - 1))
                     ? "" : " " );
        }
        cout << endl;
    } while (next_combination(v, n));

    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
