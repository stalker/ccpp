
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <vector>
#include <gmpxx.h>
#include "my_discr_vects.h"

using std::vector;
using namespace std;

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y) {
    // static double SMALL_NUM = LDBL_EPSILON*1.0E9;
    static double SMALL_NUM = DBL_EPSILON*1.0E5;
#if defined(DEBUG) && DEBUG > 8
    cerr << "DBL_EPSILON:" << LDBL_EPSILON << endl;
    cerr << "DBL_DIG    :" << DBL_DIG << endl;
    cerr << "FLT_DIG    :" << FLT_DIG << endl;
    cerr << "SMALL_NUM  :" << SMALL_NUM << endl;
#endif
    if (std::abs(x) < SMALL_NUM && std::abs(y) < SMALL_NUM)
        return std::abs(x-y) < SMALL_NUM;
    else
        return std::abs(x-y) < std::abs(x) * SMALL_NUM;
}

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(vector<double> & a,vector<double> & b, int n) {
    double scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}
mpf_class scalar_product(vector<mpf_class> & a, vector<mpf_class> & b, int n) {
    mpf_class scalar_mult = 0.0;
    for (int i = 0; i < n; ++i) {
        scalar_mult = scalar_mult + a[i]*b[i];
    }
    return scalar_mult;
}

// Найдем длины векторов:
// modulus of vector
double modulus_vector(vector<double> & a, int n) {
    double mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}
mpf_class modulus_vector(vector<mpf_class> & a, int n) {
    mpf_class mod_vect = 0.0;
    for (int i = 0; i < n; ++i) {
        mod_vect = mod_vect + a[i]*a[i];
    }
    mod_vect = sqrt(mod_vect);
    return mod_vect;
}

// расстояние межде двумя точками
double lenght(vector<double> & a,vector<double> & b, int n) {
    double lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        double t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}
mpf_class lenght(vector<mpf_class> & a,vector<mpf_class> & b, int n) {
    mpf_class lenght_result = 0.0;
    for (int i = 0; i < n; ++i) {
        mpf_class t = a[i] - b[i];
        lenght_result = lenght_result + t*t;
    }
    return sqrt(lenght_result);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
