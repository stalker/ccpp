#include <iostream>
#include <vector>

using namespace std;

void backtrack(int n, int k, int prev, vector<int> &v)   {
   if (k == 0)   {
      cout << "{" << v.back();
      for (size_t i = v.size()-1; i > 0; i--)
         cout << ", " << v[i-1];
      cout << "}" << endl;
   }   else   {
      for (int i = prev+1; i <= n; i++)   {
         v[k-1] = i;
         backtrack(n, k-1, i, v);
      }
   }
}

int main()   {
   int n, k;
   cin >> n >> k;
   vector<int> v(k);
   backtrack(n, k, 0, v);
}
