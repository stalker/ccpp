/*
 * Version 2.1
 */

#ifndef MY_DISCR_VECTS_H
#define MY_DISCR_VECTS_H

#include <vector>

// сравнение двух вещественных чисел типа double
// пороговое значение DBL_EPSILON*1.0E5 обычно ~ 2.22e-11
bool areEqual(double x, double y);

// Найдем скалярное произведение векторов:
// scalar product
double scalar_product(std::vector<double> & a, std::vector<double> & b, int n);
mpf_class scalar_product(std::vector<mpf_class> & a, std::vector<mpf_class> & b, int n);

// Найдем длины векторов:
// modulus of vector
double modulus_vector(std::vector<double> & a, int n);
mpf_class modulus_vector(std::vector<mpf_class> & a, int n);

// расстояние межде двумя точками
double lenght(std::vector<double> & a, std::vector<double> & b, int n);
mpf_class lenght(std::vector<mpf_class> & a, std::vector<mpf_class> & b, int n);

#endif /* MY_DISCR_VECTS_H */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
