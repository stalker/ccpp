#include <stdio.h>
#include <memory.h>

int main() {
        const int n = 5;
        const int k = 3;
        int m[k];
        memset(m, 0, sizeof m);
        for (int i = 0; i < k;) {
                for (i = 0; i < k; ++i)
                        printf("%d ", m[i]);
                putchar('\n');
                for (i = 0; i < k; ++i)
                        if (++m[i] == n)
                                m[i] = 0;
                        else
                                break;
        }
        return 0;
}
