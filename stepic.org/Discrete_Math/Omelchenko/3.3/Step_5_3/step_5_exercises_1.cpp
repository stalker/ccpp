/* step_5_exercises_1.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * Нужно сгенерировать все возможные k-сочетания из n-элементов.
 *
 * Формат входные данные:
 * Два числа k и n через пробел. Для них гарантированно выполняется условие:
 * 0≤k≤n. 
 *
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из которых
 * содержится k чисел от 0 до n, разделенных пробелом. 
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "my_fd_ieee754.h"
#include "step_5.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using std::string;
using std::vector;
using namespace std;

/*
 * Стоит заметить, что возможна и в некотором смысле более эффективная
 * реализация, которая будет строить всевозможные сочетания на ходу,
 * и тем самым работать за O (Cnk n). С другой стороны, эта реализация
 * представляет собой рекурсивную функцию, и поэтому для небольших n,
 * вероятно, она имеет большую скрытую константу, чем предыдущее решение.
 * Собственно сама реализация - это непосредственное следование формуле:
 * G(N,K) = 0G(N-1,K) ∪ 1G(N-1,K-1)R
 * Эта формула легко получается из приведённой выше формулы для
 * последовательности Грея - мы просто выбираем подпоследовательность
 * из подходящих нам элементов.
 */

#define MAXN 255

bool ans[MAXN];

void gen(int n, int k, int l, int r, bool rev, int old_n) {
	if (k > n || k < 0)  return;
	if (!n) {
		for (int i = 0; i < old_n; ++i)
			printf("%d ", (int)ans[i]);
		puts("");
		return;
	}
	ans[ rev ? r : l] = false;
	gen (n - 1, k, ! rev ? l + 1 : l, ! rev ? r : r - 1, rev, old_n);
	ans[rev?r:l] = true;
	gen(n - 1, k - 1, ! rev ? l + 1 : l, ! rev ? r : r - 1, !rev, old_n);
}

void all_combinations(int n, int k) {
	gen(n, k, 0, n - 1, false, n);
}

int n = 2;
int k;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cin >> k;
    cin >> n;
    all_combinations(n, k);
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
