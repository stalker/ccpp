/* step_5_exercises_1.cpp */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * Нужно сгенерировать все возможные k-сочетания из n-элементов.
 *
 * Формат входные данные:
 * Два числа k и n через пробел. Для них гарантированно выполняется условие:
 * 0≤k≤n. 
 *
 * Формат выходных данных:
 * Необходимое число лексикографически упорядоченных строк, в каждой из которых
 * содержится k чисел от 0 до n, разделенных пробелом. 
 */

#include <gmpxx.h>
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "my_fd_ieee754.h"
#include "step_5.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setw(11) << setprecision(6) \
             << setfill(' ')
#define FFXY(X,Y) setiosflags(ios::fixed) \
               << setiosflags(ios::right) \
               << setw((X)) << setprecision((Y)) \
               << setfill(' ')

using std::string;
using std::vector;
using namespace std;

int gray_code (int n) {
	return n ^ (n >> 1);
}

int count_bits (int n) {
	int res = 0;
	for (; n; n>>=1)
		res += n & 1;
	return res;
}

void all_unsort_combinations(int n, int k) {
	for (int i = 0; i < (1 << n); ++i) {
		int cur = gray_code(i);
		if (count_bits(cur) == k) {
			for (int j = 0; j < n; ++j)
				if (cur & (1 << j))
					printf("%d ", j);
			puts("");
		}
	}
}

typedef vector<int> Vector;
typedef vector<vector<int> > Matrix;

void bad_all_sort_combinations(Matrix & a, int n, int k) {
	for (int i = 0; i < (1 << n); ++i) {
		int cur = gray_code(i);
		if (count_bits(cur) == k) {
            Vector t(k, 0);
			for (int j = 0; j < n; ++j) {
				if (cur & (1 << j))
                    t.push_back(j);
					printf("%d ", j);
            }
            a.push_back(t);
            puts("");
		}
	}
    sort(a.begin(), a.end(),
              [](const Vector & aa, const Vector & bb) {
      return aa[0] < bb[0];
    });
    for(auto v: a) {
        for(auto e: v) {
            cout << e << " ";
        }
        cout << endl;
    }
}

int n = 2;
int k;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cin >> k; 
    cin >> n;
    Matrix a;
    bad_all_sort_combinations(a, n, k);
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
