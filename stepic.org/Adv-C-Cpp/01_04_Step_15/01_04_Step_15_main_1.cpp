/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

/*
 * Напишите на языке C / C++ программу поиска максимальной по длине  монотонно
 * неубывающей подпоследовательности во входной последовательности  веществен-
 * ных чисел.  Если таких подпоследовательностей несколько, выведите первую из
 * них (найденную при просмотре входного потока первой).
 *
 * Примечание:  искомая  подпоследовательность  является «плотной», в  ней нет
 * разрывов и посторонних элементов, включая числовые значения.
 *
 * Вход: одно целое N (длина последовательности; возможно, со знаком  «плюс»),
 * за которым следует k вещественных чисел в любой  разрешенной  форме  записи
 * (и, возможно, символ «перевод строки» /n).  Если k = N, анализируется вход-
 * ная последовательность вещественных целиком;  если  k  >  N,  анализируется
 * только N первых (расположенных в традиционной  записи  слева)  членов  этой
 * последовательности.  Если k < N (возможно, k = 0) или N < 2, входная после-
 * довательность некорректна.
 *
 * Выход: два целых (натуральных), первое из которых  L  соответствует  длине,
 * а второе — порядковому номеру i первого  элемента  найденной  подпоследова-
 * тельности (принять i = 1, 2,  3,  ...).   Если  входная  последовательность
 * корректна, но искомая подпоследовательность не обнаружена,  вывести  символ
 * 0 (ноль).  Во всех случаях подачи на вход некорректных, в  том  числе  сим-
 * вольных / строковых данных, вывести строку [error]  (вместе  с  квадратными
 * скобками).
 */

#define TEST
#if defined(TEST)
# include <unistd.h>
#endif

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define DEBUG 4
#ifdef DEBUG
# include "mydebug.hpp"
#endif
// #include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using std::string;
using std::vector;
using namespace std;

#if defined(DEBUG) && defined(PRINTM)
template<typename T>
std::ostream&
printd_vector(vector<T> array) {
    if (array.size() > 0) {
        size_t size1 = array.size() - 1;
        for (size_t i = 0; i < size1; i++) {
            printe("[", i, "]=", array[i], ", ");
        }
        return printe("[", size1, "]=", array[size1], ";");
    }
    return printe("!!!EMPTY!!!");
}
#endif

typedef signed   long long sll;
typedef unsigned long long ull;

// Функция: check_integer, проверка  является  ли  строка  символов,  валидным
//          представлением целого положительного числа.
// Вход:  line - std::string
// Выход: true (Да)
//        false (Нет)
bool
check_integer(string & line) {
    const char * p = line.c_str();
    if (*p == '+') p++;
    while (*p != '\n' and *p != '\0') {
        if (!isdigit(*p))
            return false;
        p++;
    }
    return true;
}

int
is_valid(const char *s) {
  int flag = 1;

  do {
    if ( *s != ' ' && *s != '\n' && *s != '+' && *s != '-'
         && *s != 'E' && *s != 'e' && *s != '.' && !isdigit(*s) ) {
      flag = 0;
      break;
    }
    s++;
  } while (*s);

  return flag;
}

// Функция: is_float, проверка является ли  строка  символов,  валидным  пред-
//          ставлением числа с плавающей точкой (вещественного).
// Вход:  s - указатель на массив символов строка с нуль-терминатором
// Выход: 1 - true (Да)
//        0 - false (Нет)
int
is_float(const char *s) {
  int n;
  double d;
  sscanf(s, "%lf%n", &d, &n);

  return (n == strlen(s)) ? 1 : 0;
}

// Функция: check_integer, проверка  является  ли  строка  символов,  валидным
//          представлением целого положительного числа.
// Вход:  line - std::string
// Выход: true (Да)
//        false (Нет)
bool
check_double(string & line) {
    const char * p = line.c_str();
    if (*p == '+') p++;
    while (*p != '\n' and *p != '\0') {
        if (!isdigit(*p))
            return false;
        p++;
    }
    return true;
}

// Функция: is_int, проверка является ли строка символов, валидным представле-
//          нием целого числа.
// Вход:  s - указатель на массив символов строка с нуль-терминатором
// Выход: 1 - true (Да)
//        0 - false (Нет)
int
is_int(const char *s) {
  int n;
  int i;
  sscanf(s, "%d%n", &i, &n);

  return (i > 0 && n == strlen(s)) ? 1 : 0;
}

// Процедура: putback_string, возврат набора символов  из  строки  во  входной
//            поток.
// Вход:  is  - входной поток
//        str - std::string набор символов которые нужно вернуть
void putback_string(istream & is, string str) {
    while (str.size() > 0) {
        is.putback(str.back());
        str.pop_back();
    }
}

// Функция: get_count, получение первого числа, кол-ва чисел  котопые  необхо-
//          димо обработать.
// Вход:  iss - входной строковый поток
// Выход: > 0 - требуемое число
//        0   - ошибка
sll
get_count(istringstream & iss) {
    string first;
    iss >> first;
#if defined(DEBUG) && defined(PRINTM)
    printd("first: ", first) << endl;
#endif
    if (first.size() == 0 || !check_integer(first)) {
        return 0;
#if defined(DEBUG) && defined(PRINTM)
        printd("return ", 0) << endl;
#endif
    }
    putback_string(iss, first);
    sll result;
    iss >> result;
#if defined(DEBUG) && defined(PRINTM)
    printd("return ", result) << endl;
#endif
    return result;
}

// Функция: get_sequence, получение анализируемой последовательности и  запол-
//          нение результирующего массива (vector-а).
// Вход:  iss   - входной строковый поток
//        count - предел (ограничение количества элементов последовательности)
// Выход: array - результирующий массив
//              - размер массива
#if defined(__GNUC__) && __GNUC__ > 3
__attribute__((always_inline)) size_t
#else
inline size_t
#endif
get_sequence(vector<double> & array, istringstream & iss, sll count) {
    string in;
    //  читаем  из  потока  пока  кол-во  полученных   эл-тов   меньше   count
    while (iss >> in && count-- > 0) {
        // проверка на валидность введённого эл-та
        if (is_valid(in.c_str()) && is_float(in.c_str())) {
            // возврат эл-та во входной поток
            putback_string(iss, in);
            double d;
            iss >> d;           // чтение текущего эл-та
            array.push_back(d); // добавление текущего эл-та
        }
    }
    return array.size();
}

struct selected {
    size_t start;
    size_t lenght;
};

struct current  {
    size_t start;
    size_t index;
};

#if defined(DEBUG) && defined(PRINTM)
std::ostream&
printd_selected(selected sel) {
    return printe("selected{ start=", sel.start, ", lenght=", sel.lenght, " }");
}
std::ostream&
printd_current(current & cur) {
    return printe("current{ start=", cur.start, ", index=", cur.index, " }");
}
#endif

// Функция: go_break_or_end,  если длинна текущго «окна» больще сохранённого и
//          не нулевая то заменяем сохранённое текущим.
// Вход:    cur - текущее «окно»
// Измен:   sel - сохранённое (выбранное) «окно»
inline void
go_break_or_end(current & cur, selected & sel) {
    size_t len;
    if ((len = (cur.index - cur.start)) > 0 && len > sel.lenght) {
        sel.start = cur.start;
        sel.lenght = len;
    }
}

// Функция: go_break, если длинна текущго  «окна»  больще  сохранённого  и  не
//          нулевая то заменяем сохранённое текущим. Текущее окно переносим на
//          границу между сравниваемых эл-тов и обнуяем его длину.
// Вход:    nxt - индекс второго (правого) из сравниваемых эл-тов
// Измен:   cur - текущее «окно»
//          sel - сохранённое (выбранное) «окно»
#if defined(__GNUC__) && __GNUC__ > 3
__attribute__((always_inline)) void
#else
inline void
#endif
go_break(current & cur, selected & sel, size_t nxt) {
#if defined(DEBUG) && defined(PRINTM)
    printd("cur: ");
    printd_current(cur) << endl;
    printd("sel: ");
    printd_selected(sel) << endl;
#endif
    go_break_or_end(cur, sel);
    cur.start = nxt;
    cur.index = nxt;
#if defined(DEBUG) && defined(PRINTM)
    printd("cur: ");
    printd_current(cur) << endl;
    printd("sel: ");
    printd_selected(sel) << endl;
#endif
}

// Функция: go_continue, переносим правый эл-т текущего «окна» по индексу nxt.
// Вход:    nxt - индекс второго (правого) из сравниваемых эл-тов
// Измен:   cur - текущее «окно»
#if defined(__GNUC__) && __GNUC__ > 3
__attribute__((always_inline)) void
#else
inline void
#endif
go_continue(current & cur, size_t nxt) {
    cur.index = nxt;
#if defined(DEBUG) && defined(PRINTM)
    printd("cur: ");
    printd_current(cur) << endl;
#endif
}

// Функция: go_end, завершение обработки входного массива, если длинна теку-
//          щего «окна» больще сохранённого и не нулевая  то  заменяем  сох-
//          ранённое текущим. (выполняется в процедуре go_break_or_end)
// Вход:    cur - текущее «окно»
// Выход:   sel - сохранённое (выбранное) «окно»
void
go_end(current & cur, selected & sel) {
#if defined(DEBUG) && defined(PRINTM)
    printd("cur: ");
    printd_current(cur) << endl;
    printd("sel: ");
    printd_selected(sel) << endl;
#endif
    go_break_or_end(cur, sel);
#if defined(DEBUG) && defined(PRINTM)
    printd("sel: ");
    printd_selected(sel) << endl;
#endif
}

// Функция: solution, основное решение, алгоритм:
//     инициализируем «окна»: cur - текущее и sel - сохранённое (выбранное) 
//     обход соседних пар эл-тов входного массива, кол-во сравниваемых пар k-1
//     | сравнимаем соседние пары
//     | | если пары не входят в неубывающую последовательность
//     | | | переходим к процедуре go_break
//     | | иначе переходим к процедуре go_continue
//     после цикла выполняем процедуру go_end
//     искомый результат в сохранённом (выбранном) «окне»
// Вход:  array - входной массив
//            k - кол-во эл-тов во входном массиве
// Выход: index - порядковый номер начального эл-та найденной  подпоследовате-
//                льности во входном массиве
//              - длина найденной  подпоследовательности
size_t
solution(size_t & index, vector<double> & array, size_t k) {
    current cur = {0, 0};
#if defined(DEBUG) && defined(PRINTM)
    printd("cur: ");
    printd_current(cur) << endl;
#endif
    selected sel = {0, 0};
#if defined(DEBUG) && defined(PRINTM)
    printd("sel: ");
    printd_selected(sel) << endl;
#endif
    size_t size1 = array.size() - 1;
    for (size_t idx = 0, nxt = 1; idx < size1; idx++, nxt++) {
        if (array[idx] > array[nxt]) {
#if defined(DEBUG) && defined(PRINTM)
            printd("go_break: idx=", idx) << endl;
#endif
            go_break(cur, sel, nxt);
        } else {
            go_continue(cur, nxt);
        }
    }
    go_end(cur, sel);
    if (sel.start == 0 && sel.lenght == 0) {
        index = 0;
        return 0;
    }
    index = sel.start + 1;
    return sel.lenght + 1;
}

// Функция: go,
//          
// Вход:        - 
// Выход:       - 
int
go(string line) {
#if defined(DEBUG) && defined(PRINTM)
    printd("line: ", line) << endl;
#endif
    istringstream iss(line);
    sll count = get_count(iss);                       // N
#if defined(DEBUG) && defined(PRINTM)
    printd("count: ", count) << endl;
#endif
    if (count < 2) {
        cout << "[error]" << endl;
        return EXIT_SUCCESS;
    }
    vector<double> array;
    size_t k = get_sequence(array, iss, count);       // k
#if defined(DEBUG) && defined(PRINTM)
    printd("k: ", k) << endl;
    printd("array: ");
    printd_vector(array) << endl;
#endif
    if (k < count) {
        cout << "[error]" << endl;
        return EXIT_SUCCESS;
    }
    size_t index, l;
    l = solution(index, array, k);
    if (l == 0 && index == 0)
        cout << l << endl;
    else
        cout << l << " " << index << endl;
    return EXIT_SUCCESS;
}

#define MAXLEN 65535

#if defined(TEST)
// Функция: go_break_or_end,
//          
// Вход:        - 
// Выход:       - 
int
test() {
    char test_numbers_d[][MAXLEN] = {
        "10 1 2 3 1 2 3 1 1 1 1", "4 7",        //  0
        "+10 1 2 3 1 2 3 1 1 1 1", "4 7",
        "10\n1 2 3 1 2 3 1 1 1 1", "4 7",
        "10 1 2 3 4 3 3 1 2 3 4", "4 1",
        "10 1.5 2 3 4.7 3 3 1 2 3 4", "4 1",
        "3 1 2 3 1 2 3 1 1 1 1", "3 1",
        "3 10 20 30 40 50", "3 1",
        "5 5 4 3 2 1", "0",
        "4 3 2 1 2", "2 3",
        "4 3e1 2e1 1e1 -2.700000e+00", "0",
        "6 -1 -2 -1 -2 -1 -2", "2 2",
        "4 0.5 1.789 -0.555 +0.555", "2 1",
        "3 4 1 2", "2 2",
        "3 1 0 2", "2 2",
        "7 1 2 1 2 1 2 3", "3 5",
        "4 1 2 1 2", "2 1",
        "3 1 2 3", "3 1",
        "", "[error]",                          // 17
        "1 1", "[error]",
        "10.1 1 2 3 1 2 3 1 1 1 1", "[error]",
        "10 1 2 3 1 2 3 1 1 1 1", "4 7",
        "10 1 2 3 1 2 3 1 1 1 1", "4 7",
        "10 1 2 3", "[error]",
        "-3 1 2 3", "[error]",
        "3- 1 2 3", "[error]",
        "10", "[error]",
        "3 10 20 30 40 50 zxczxc", "3 1",
        "3 1 2 -2.70e0000e+00", "[error]",      // 27
        "3 1 2 -2.70E0000E+00", "[error]",
        "3 1 2 -2.70e0000E+00", "[error]",
        "3 1 2 -2.70+0000E+2", "[error]",
        "3 1 2 -2.70-0000E-2", "[error]",
    };
    size_t len_test_numbers_d = sizeof test_numbers_d[0];
    size_t count_test_numbers_d = sizeof(test_numbers_d)
                                / sizeof test_numbers_d[0];
#if defined(_SC_LEVEL1_DCACHE_LINESIZE)
    char ** test_numbers;
    int errflag; // код ошибки posix_memalign
    // Установить размер линии кеш-памяти данных 1-го уровня
    // (L1d): типичное значение
    long l1dcls = sysconf(_SC_LEVEL1_DCACHE_LINESIZE);
    // Проверить, удался ли вызов sysconf()
    if (l1dcls == -1) {
        // если вызов sysconf() неудачен, использовать значение
        // выравнивания по умолчанию
        l1dcls = sizeof(void*);
    }
    errflag = posix_memalign((void**)&test_numbers, l1dcls,
                             count_test_numbers_d*sizeof(char *));
    if (errflag) {
        cerr << endl << "posix_memalign error" << endl;
        exit(EXIT_FAILURE);
    }
    cout << endl << "L1d cache line size is " << l1dcls << endl;
    for (int i = 0; i < count_test_numbers_d; ++i) {
        errflag = posix_memalign((void**)&test_numbers[i], l1dcls,
                                 sizeof test_numbers_d[i]);
        if (!errflag) {
            test_numbers[i] = static_cast<char*>(memcpy(&test_numbers[i],
                                                    &test_numbers_d[i],
                                                    sizeof test_numbers_d[i]));
        }
    }
#else
    char * test_numbers[count_test_numbers_d];
    for (int i = 0; i < count_test_numbers_d; ++i) {
        test_numbers[i] = new char[MAXLEN];
        if (test_numbers[i] == nullptr) {
            cerr << endl << "new char[" << MAXLEN << "] error" << endl;
            exit(EXIT_FAILURE);
        }
        test_numbers[i] = static_cast<char*>(memcpy(test_numbers[i],
                                              test_numbers_d[i],
                                              strlen(test_numbers_d[i])));
    }
#endif
    cout << "sizeof test_numbers     :" << sizeof test_numbers << endl;
    cout << "sizeof test_numbers_d   :" << sizeof test_numbers_d << endl;
    cout << "sizeof test_numbers_d[0]:" << sizeof test_numbers_d[0] << endl;
#if defined(DBG_COUNT)
    int count = 2*28;
#else
    int count = count_test_numbers_d;
#endif
// #define DBG_TST
#if defined(DBG_TST)
    int idx = 2*25;
    cout << endl;
    go(string(test_numbers[idx]));
    cout << "input:  " << test_numbers[idx] << endl
         << "result: " << test_numbers[idx + 1] << endl;
#else
    for (int idx = 0; idx < count; idx+=2) {
        cout << endl;
        cout << "input : " << test_numbers[idx] << endl
             << "result: ";
        go(string(test_numbers[idx]));
        cout << "check : " << test_numbers[idx + 1] << endl;
    }
#endif
    return EXIT_SUCCESS;
}
#endif

/* let's go */
int
main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
#if defined(DEBUG) && defined(PRINTM)
    printd("argc: ", argc, "; argv: ", argv) << endl;
    printd("sizeof(size_t): ", sizeof(size_t),
           "; sizeof(current)): ", sizeof(current)) << endl;
#endif
#if defined(TEST)
    return test();
#else
    string line;
    getline(cin, line);
    return go(line);
#endif
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
