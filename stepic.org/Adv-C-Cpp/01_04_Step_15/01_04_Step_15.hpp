
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#ifndef _01_04_Step_15_HPP_
#define _01_04_Step_15_HPP_

#define Beg(X) (X).begin()
#define End(X) (X).end()

#endif /* _01_04_Step_15_HPP_ */

/* EOF */
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
