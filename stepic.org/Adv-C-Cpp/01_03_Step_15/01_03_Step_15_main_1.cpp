/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstddef>
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>

#define DEBUG 4

#ifdef DEBUG
# include "mydebug.hpp"
#endif

/**
 * 
 */

#include "01_03_Step_15.hpp"

#include <cstdlib>
#include <iostream>
#include <sstream>

using std::string;
using std::vector;
using namespace std;

typedef unsigned long long ull;

const ull up = 4298000000;

bool check_line(string & line) {
    const char * p = line.c_str();
    if (*p == '+') p++;
    while (*p != '\n' and *p != '\0') {
        if (!isnumber(*p))
            return false;
        p++;
    }
    return true;
}

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
#if defined(DEBUG) && defined(PRINTM)
    printd("argc: ", argc, "; argv: ", argv) << endl;
#endif

    string line;
    getline(cin, line);
    if (!check_line(line)) {
        cout << 0 << endl;
        return EXIT_SUCCESS;
    }

    istringstream ss(line);
    ull in;
    ss >> in;

    for (ull i = 1; i < up; i++) {
        if ((i*(i+1)/2) == in) {
            cout << i << endl;
            return EXIT_SUCCESS;
        }
    }
    cout << 0 << endl;

    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
