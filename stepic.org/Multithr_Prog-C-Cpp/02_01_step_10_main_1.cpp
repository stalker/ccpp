/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cctype>
#include <cfloat>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include "02_01_step_10.h"
#include "mydebug.h"

#define DEBUG 8

#ifdef DEBUG
# include "mydebug.h"
#endif

/**
 * 
 */

#include <map>
#include <regex>
#include <string>
#include <vector>
#include <cstddef>
#include <cstring>
#include <sstream>
#include <iostream>

const char * sp = " ";

std::vector<std::string> & get_monoms(std::string plnml) {
    std::vector<std::string> * result; 
    result = new std::vector<std::string>();
    if (plnml.size() == 0) {
        result->push_back("");
        return *result;
    }
    try {
        std::regex re("([-\\+]{0,1}[0-9xX\\^\\* ]+)");
        std::sregex_token_iterator next(plnml.begin(), plnml.end(), re, 1 );
        std::sregex_token_iterator end;
        while (next != end) {
            result->push_back(*next);
            next++;
        }
    } catch (std::regex_error& e) {
        // Syntax error in the regular expression
        std::cerr << "Syntax error in the regular expression" << std::endl;
    }
    return *result;
}

int get_power(const std::string & monom) {
    int count = 0;
    int result = 0;
    try {
        std::regex re("\\^");
        std::sregex_token_iterator next(monom.begin(), monom.end(), re, -1);
        std::sregex_token_iterator end;
        while (next != end) {
            if (count > 1) break;
            if (count == 1) {
                std::istringstream ist(*next);
                ist >> result;
            }
            count++;
            next++;
        } 
    } catch (std::regex_error& e) {
        // Syntax error in the regular expression
        std::cerr << "Syntax error in the regular expression" << std::endl;
    }
    if (0 == result && std::strchr(monom.c_str(), 'x') != nullptr)
        return 1;
    return result;
}

int get_coefficient(const std::string & monom) {
    if (0 == monom.size()) return 0;
    int count = 0;
    int result = 1;
    if (std::strchr(monom.c_str(), '-') != nullptr) result = -1;
    try {
        std::regex re("\\*x");
        std::sregex_token_iterator next(monom.begin(), monom.end(), re, -1);
        std::sregex_token_iterator end;
        while (next != end) {
            if (count > 0) break;
            if (count == 0 && next->length() > 0 && 
                std::strchr(monom.c_str(), '*') != nullptr) {
                std::istringstream ist(*next);
                ist >> result;
            }
            count++;
            next++;
        } 
    } catch (std::regex_error& e) {
        // Syntax error in the regular expression
        std::cerr << "Syntax error in the regular expression" << std::endl;
    }
    return result;
}

std::string out_monom(int c, int p, std::string prefix = "") {
    if (0 == c) {
        return (std::string)"";
    }
    std::ostringstream result;
    if (c > 0) result << prefix;
    result << c;
    if (p > 0) {
        if (p > 1)
            result << "*x^" << p;
        else
            result << "*x";
    }
    return result.str();
}

std::string derivative(std::string polynomial) {
    std::map<int,int> polynom;
    std::map<int,int> derivative;
    derivative[0] = 0;
    std::vector<std::string> & monoms = get_monoms(polynomial);
    for (auto monom : monoms) {
        int power = get_power(monom);
        if (0 == polynom.count(power))
            polynom[power] = 0;
        int coefficient = get_coefficient(monom);
        polynom[power] += coefficient;
    }
    delete &monoms;
    for (auto it = polynom.begin(); it != polynom.end(); ++it) {
        if (it->first > 0) {
            derivative[it->first - 1] = it->second * it->first;
        }
    }
    std::ostringstream result;
    auto it = derivative.rbegin();
    result << out_monom(it->second, it->first);
    ++it;
    for (; it != derivative.rend(); ++it) {
        result << out_monom(it->second, it->first, "+");
    }
    return result.str();
}

using std::string;
using std::vector;
using namespace std;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    int count = 0;
    string ss[] = { "", "x^2+x", "2*x+1", "2*x^100+100*x^2"
                  , "x^10000+x+1", "-x^2-x^3" , "10*x-9*x"
                  , "x+x+x+x+x+x+x+x+x+x" };
    for (auto s : ss) {
        cout << "::" << count++ << "::" << endl;
        cout << s << endl;
        cout << derivative(s);
        cout << endl;
    }
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
