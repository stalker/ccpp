/* $Date$
 * $Id$
 * $Version: 0.8$
 * $Revision: 8$
 */

#define DEBUG 9
const bool debug = true;

#include <cmath>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <cstdlib>
#include <iomanip>
#include <cinttypes>
#include <iostream>
#include <sstream>

const char * em = "";
const char * & empty = em;
const char * sp = " ";
const char * & space = sp;
const char * cl = ":";
const char * & colon = cl;
const char * sc = ";";
const char * & semicolon = sc;
const std::string brackets[] = {"[", ",", "]"};
const std::string parens[] = {"(", ",", ")"};
const std::string (& parentheses)[3] = parens;
const std::string braces[] = {"{", ",", "}"};
const std::string scall = "call";
const std::string slastp = "lastp:";
const std::string sheader = "header";
const std::string smalloc = "malloc";
const std::string sreturn = "return";
const std::string sreturn_call = "return call";
const std::string sreturn_nullptr = "return nullptr";

#define BYTE int8_t
typedef unsigned int tsize;
typedef BYTE Align;

union header {
    struct {
        union header *next;
        unsigned int size;
        bool free;
    } s;
    Align x[16];
};

typedef union header Header;

#define HEAP_SIZE 512
// #define HEAP_SIZE 1048576
#define HEADER_SIZE sizeof(Header)

#if defined(_MSC_VER)
# define __func__ __FUNCTION__
#endif

std::ostream&
print_header(std::ostream& stm, Header *h, const std::string *b = brackets) {
    stm << b[0] + sp << h << b[1] + sp << h->s.next
        << b[1] + sp << h->s.size << b[1] + sp
        << (h->s.free ? "1" : "0") << b[2];
    return stm;
}

template <typename T>
std::ostream& print_sp_r(std::ostream& stm, const T &t) {
    return stm << t;
}

template <typename T, typename... Args>
std::ostream& print_sp_r(std::ostream& stm, const T &t, const Args&... r) {
    stm << t << parens[1] << sp;
    return print_sp_r(stm, r...);
}

template <typename... Args>
std::ostream& print_sp(std::ostream& stm, std::string f, const Args&... r) {
    stm << f << parens[0];
    print_sp_r(stm, r...);
    stm << parens[2];
    return stm;
}

#define PRINT_SP(STM, ...) if (debug) { \
        STM << __FILE__ << cl << __LINE__ << cl; \
        print_sp(STM, __func__, __VA_ARGS__) \
        << sc << std::endl; \
}
#define PRINT_FN(STM, ...) { \
        STM << __FILE__ << cl << __LINE__ << cl; \
        print_sp(STM, __func__, __VA_ARGS__) \
        << cl; \
}
#define PRINT_LN(STM, ...) { print_sp(STM, __VA_ARGS__) << std::endl; }
#define PRINT_FT(STM, ...) { print_sp(STM, __VA_ARGS__); }
#define INDEBUG(...) if (debug) { __VA_ARGS__; }

class SmallAllocator {
public:
        SmallAllocator(tsize heapsize = HEAP_SIZE) {
            INDEBUG(PRINT_SP(std::cerr, static_cast<void *>(this->Memory)));
            Memory = (BYTE *)::operator new(heapsize*sizeof(BYTE));
            INDEBUG(PRINT_FN(std::cerr, static_cast<void *>(this->Memory));
                    PRINT_LN(std::cerr, "heapsize:", heapsize));
            base.s.next = (Header *)this->Memory;
            base.s.size = heapsize;
            lastp = base.s.next;
            lastp->s.size = base.s.size - HEADER_SIZE;
            endp = base.s.next + (heapsize / HEADER_SIZE);
            freep = nullptr;
        }
        ~SmallAllocator() {
            INDEBUG(PRINT_SP(std::cerr, static_cast<void *>(this->Memory)));
            ::operator delete(Memory);
        }
        void *Alloc(tsize Size) {
            return this->malloc(Size);
        };
        void *ReAlloc(void *Pointer, tsize Size) {
            return this->realloc(Pointer, Size);
        };
        void Free(void *Pointer) { this->free(Pointer); };
#if defined(DEBUG)
        void print_status();
#endif
private:
        void *malloc_in_free_split(tsize nbytes);
        void *malloc_in_free(tsize nbytes);
        void *malloc(tsize nbytes);
        void *realloc_new(Header *h, void *p, tsize nbytes);
        void *realloc_last(Header *p, tsize nbytes);
        void *realloc(void *p, tsize nbytes);
        void free(void *ap);
        void compact_free();
        Header base;
        Header *freep; // TODO
        Header *endp;
        Header *lastp;
        BYTE * Memory;
};

void *SmallAllocator::malloc_in_free_split(tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, nbytes));
    tsize nunits = nbytes + HEADER_SIZE;
    Header * i = freep;
    for (; i != lastp && i != nullptr; i = i->s.next) {
        if (i->s.free && i->s.size > (nbytes + 2*HEADER_SIZE + 1)) {
            INDEBUG(PRINT_FN(std::cerr, nbytes);
                    PRINT_LN(std::cerr, "i->s.size", i->s.size);
                    PRINT_FN(std::cerr, nbytes);
                    PRINT_LN(std::cerr, "nbytes + 2*HEADER_SIZE + 1",
                    nbytes + 2*HEADER_SIZE + 1));
            void * result = i + 1;
            Header * next = (Header *)(static_cast<BYTE *>(result) + nbytes);
            next->s.next = i->s.next;
            i->s.free = false;
            i->s.next = next;
            i->s.next->s.free = true;
            i->s.next->s.size = i->s.size - nunits; 
            i->s.size = nunits;
            INDEBUG(PRINT_FN(std::cerr, nbytes);
                    PRINT_FT(std::cerr, sheader, result);
                    print_header(std::cerr, i) << std::endl;
                    PRINT_FN(std::cerr, nbytes);
                    PRINT_LN(std::cerr, sreturn, result));
            return result;
        } else if (i->s.free && i->s.size > (nbytes + HEADER_SIZE)) {
            void * result = i + 1;
            i->s.free = false;
            INDEBUG(PRINT_FN(std::cerr, nbytes);
                    PRINT_LN(std::cerr, sreturn, result));
            return result;
        }
    }
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, sreturn_nullptr, em));
    return nullptr;
}

void *SmallAllocator::malloc_in_free(tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, nbytes));
    Header * i = freep;
    for (; i != lastp && i != nullptr; i = i->s.next) {
        if (i->s.free && i->s.size == (nbytes + HEADER_SIZE)) {
            i->s.free = false;
            void * result = i + 1;
            INDEBUG(PRINT_FN(std::cerr, nbytes);
                    PRINT_LN(std::cerr, sreturn, result));
            return result;
        }
    }
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, scall, "compact_free"));
    this->compact_free();
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, sreturn_call, "malloc_in_free_split"));
    return this->malloc_in_free_split(nbytes);
}

void *SmallAllocator::malloc(tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, nbytes));
    tsize nunits = nbytes + HEADER_SIZE;
    if (lastp->s.size < nunits) {
        INDEBUG(PRINT_FN(std::cerr, nbytes);
                PRINT_LN(std::cerr, sreturn_call, "malloc_in_free"));
        return this->malloc_in_free(nbytes);
    }
    void * result = lastp + 1;
    BYTE * next   = static_cast<BYTE *>(result) + nbytes;
    lastp->s.free = false;
    lastp->s.next = (Header *)next;
    lastp->s.next->s.size = lastp->s.size - nunits; 
    lastp->s.size = nunits;
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_FT(std::cerr, sheader, lastp);
            print_header(std::cerr, lastp) << std::endl);
    lastp = lastp->s.next;
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, slastp, lastp));
    lastp->s.next = nullptr;
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, sreturn, result));
    return result;
}

void *SmallAllocator::realloc_last(Header *p, tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, p, nbytes));
    tsize nunits = nbytes + HEADER_SIZE;
    if ((p->s.size + lastp->s.size) < nunits) {
        INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                PRINT_LN(std::cerr, sreturn_nullptr, em));
        return nullptr;
    }
    tsize size = p->s.size + p->s.next->s.size;
    void * result = p + 1;
    lastp = (Header *)(static_cast<BYTE *>(result) + nbytes);
    lastp->s.free = false;
    lastp->s.next = nullptr;
    lastp->s.size = size - nunits; 
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, slastp, lastp));
    p->s.free = true;
    p->s.size = nunits;
    p->s.next = lastp;
    INDEBUG(PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, sreturn, result));
    return result;
}

void *SmallAllocator::realloc_new(Header *h, void *p, tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, h, p, nbytes);
            PRINT_FN(std::cerr, nbytes);
            PRINT_LN(std::cerr, scall, smalloc));
    tsize news = (h->s.size < (nbytes + HEADER_SIZE))
                 ? h->s.size - HEADER_SIZE : nbytes;
    void * newp = this->malloc(nbytes);
    if (nullptr == newp) {
        INDEBUG(PRINT_FN(std::cerr, h, p, nbytes);
                PRINT_LN(std::cerr, sreturn_nullptr, em));
        return nullptr;
    }
    INDEBUG(PRINT_FN(std::cerr, h, p, nbytes);
            PRINT_LN(std::cerr, "memcpy source",  news);
            /* auto flags = std::cerr.flags();
            BYTE * p1 = (BYTE *)p;
            for (int i = 0; i < (h->s.size - HEADER_SIZE); ++i) {
                std::cerr << std::setw(2) << std::setfill('0')
                          << std::hex << ((int)p1[i] & 0x00FF) << sp;
            }; std::cerr << std::endl;
            std::cerr.flags(flags); */ );
    memcpy(newp, p, news);
    INDEBUG(PRINT_FN(std::cerr, h, p, nbytes);
            PRINT_LN(std::cerr, "memcpy destination", news);
            /* auto flags = std::cerr.flags();
            BYTE * p1 = (BYTE *)newp;
            for (int i = 0; i < news; ++i) {
                std::cerr << std::setw(2) << std::setfill('0')
                          << std::hex << ((int)p1[i] & 0x00FF) << sp;
            }; std::cerr << std::endl;
            std::cerr.flags(flags); */ );
    this->free(p);
    INDEBUG(PRINT_FN(std::cerr, h, p, nbytes);
            PRINT_LN(std::cerr, sreturn, newp));
    return newp;
}

void *SmallAllocator::realloc(void *p, tsize nbytes) {
    INDEBUG(PRINT_SP(std::cerr, p, nbytes));
    if (nullptr == p) {
        INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                PRINT_LN(std::cerr, sreturn_call, smalloc));
        return this->malloc(nbytes);
    }
    if (0 == nbytes) {
        this->free(p);
        INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                PRINT_LN(std::cerr, sreturn_nullptr, em));
        return nullptr;
    }
    Header * i = base.s.next;
    for (; i != lastp && i != nullptr; i = i->s.next) {
        if ((void *)(((BYTE *)p) - HEADER_SIZE) == i) {
            if (i->s.next == lastp) {
                void * result;
                if (nullptr == (result = this->realloc_last(i, nbytes))) {
                    INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                    PRINT_LN(std::cerr, sreturn_call, "realloc_new"));
                    return this->realloc_new(i, p, nbytes);
                }
                INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                        PRINT_LN(std::cerr, sreturn_call, "realloc_last"));
                return result;

            }
            INDEBUG(PRINT_FN(std::cerr, p, nbytes);
                    PRINT_LN(std::cerr, sreturn_call, "realloc_new"));
            return this->realloc_new(i, p, nbytes);
        }
    }
    INDEBUG(PRINT_FN(std::cerr, p, nbytes);
            PRINT_LN(std::cerr, sreturn, p));
    return p;
}

void SmallAllocator::free(void *ap) {
    INDEBUG(PRINT_SP(std::cerr, ap));
    if (nullptr == ap) { return; }
    Header * i = base.s.next;
    for (;  i != lastp && i != nullptr; i = i->s.next) {
        if (!i->s.free && (void *)(((BYTE *)ap) - HEADER_SIZE) == i) {
            INDEBUG(PRINT_FN(std::cerr, ap);
                    PRINT_LN(std::cerr, "released", em));
            i->s.free = true;
            if (nullptr == freep) {
                freep = i;
                return;
            }
            std::ptrdiff_t diff = freep - i; // TODO
            if (diff > 0) { freep = i; }
            return;
        }
    }
}

void SmallAllocator::compact_free() {
    INDEBUG(PRINT_SP(std::cerr, em));
    if (nullptr == freep) { // TODO
        return;
    }
    Header * p = (Header *)freep;
    Header * i = (Header *)freep->s.next;
    while (i != lastp && i != nullptr) {
        if (i->s.free && p->s.free) {
            p->s.free = true;
            p->s.next = i->s.next;
            p->s.size += i->s.size;
            i = i->s.next;
        } else {
            p = i; i = i->s.next;
        }
    }
}

#if defined(DEBUG)
void SmallAllocator::print_status() {
    Header * i = (Header *)base.s.next;
    print_header(std::cout, &base) << std::endl;
    for (; i != lastp && i != nullptr; i = i->s.next) {
        print_header(std::cout, i) << std::endl;
    }
    print_header(std::cout, lastp) << std::endl;
    std::cout << "[ " << endp << " ]" << std::endl;
}
#endif

using std::string;
#include <vector>
using std::vector;
using namespace std;

/* let's go */
int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    cout << "sizeof(char):     " << sizeof(char) << endl;
    cout << "sizeof(BYTE):     " << sizeof(BYTE) << endl;
    cout << "sizeof(int):      " << sizeof(int) << endl;
    cout << "sizeof(long):     " << sizeof(long) << endl;
    cout << "sizeof(long long):" << sizeof(long long) << endl;
    cout << "sizeof(Align):    " << sizeof(Align) << endl;
    cout << "sizeof(Header):   " << sizeof(Header) << endl;
    cout << "HEADER_SIZE:      " << HEADER_SIZE << endl;
    SmallAllocator * alloc = new SmallAllocator(512);
    cout << "ap0 = alloc->Alloc(sizeof(double))" << endl;
    double * ap0 = (double *)alloc->Alloc(sizeof(double));
    cout << "ap1 = alloc->Alloc(10 * sizeof(int))" << endl;
    int * ap1 = (int *)alloc->Alloc(10 * sizeof(int));
    cerr << "alloc->print_status() a" << endl;
    cout << "alloc->print_status() a" << endl;
    alloc->print_status();

    for (unsigned int i = 0; i < 10; i++)
        ap1[i] = i;
    // ap1[9] = 0x1FFFFFFF;

    for (unsigned int i = 0; i < 10; i++) {
        cout << "tmp = alloc->Alloc(sizeof(double))" << endl;
        double * tmp = (double *)alloc->Alloc(sizeof(double));
        cout << "alloc->Free(tmp)" << endl;
        alloc->Free(tmp);
    }
    cerr << "alloc->print_status() b" << endl;
    cout << "alloc->print_status() b" << endl;
    alloc->print_status();

    cerr << "ap2 = alloc->Alloc(10 * sizeof(int))" << endl;
    cout << "ap2 = alloc->Alloc(10 * sizeof(int))" << endl;
    int * ap2 = (int *)alloc->Alloc(10 * sizeof(int));
    cerr << "alloc->print_status() c" << endl;
    cout << "alloc->print_status() c" << endl;
    alloc->print_status();
    cerr << "ap1 = alloc->ReAlloc(20 * sizeof(int))" << endl;
    cout << "ap1 = alloc->ReAlloc(20 * sizeof(int))" << endl;
    ap1 = (int *)alloc->ReAlloc(ap1, 20 * sizeof(int));

    for (unsigned int i = 10; i < 20; i++)
        ap1[i] = i;
    cerr << "alloc->print_status() d" << endl;
    cout << "alloc->print_status() d" << endl;
    alloc->print_status();

    cout << "ap1 = alloc->Alloc(30 * sizeof(int))" << endl;
    ap1 = (int *)alloc->ReAlloc(ap1, 30 * sizeof(int));
    for (unsigned int i = 20; i < 30; i++)
        ap1[i] = i;
    for (unsigned int i = 0; i < 30; i++) {
        cout << ap1[i] << sp;
        assert(ap1[i] == i);
    }
    cout << endl;
    cerr << "alloc->print_status() e" << endl;
    cout << "alloc->print_status() e" << endl;
    alloc->print_status();
    double * pd;
    cerr << "pd = alloc->Alloc(12 * sizeof(double))" << endl;
    cout << "pd = alloc->Alloc(12 * sizeof(double))" << endl;
    pd = (double *)alloc->Alloc(12*sizeof(double));
    cerr << "alloc->print_status() f" << endl;
    cout << "alloc->print_status() f" << endl;
    alloc->print_status();
    alloc->Free(pd);
    cerr << "alloc->print_status() g" << endl;
    cout << "alloc->print_status() g" << endl;
    double *ap[12];
    int i = 0; for (; i < 19; ++i ) {
        cerr << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
        cout << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
        if (nullptr == (ap[i] = (double *)alloc->Alloc(sizeof(double)))) {
            --i;
            cerr << "break : " << i << endl;
            cout << "break : " << i << endl;
            break;
        }
        *(ap[i]) = 3.14;
        cout << "alloc->print_status() " << i << endl;
        alloc->print_status();
    }
    int apsize = i;
    cerr << "length(ap[12]) :" << apsize << endl;
    cout << "length(ap[12]) :" << apsize << endl;
    cerr << "alloc->print_status() :" << i++ << endl;
    cout << "alloc->print_status() :" << i++ << endl;
    alloc->print_status();
    cerr << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
    cout << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
    pd = (double *)alloc->Alloc(sizeof(double));
    cerr << "alloc->print_status() :" << i++ << endl;
    cout << "alloc->print_status() :" << i++ << endl;
    alloc->print_status();
    cerr << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
    cout << "pd = alloc->Alloc(sizeof(double)) : " << i << endl;
    pd = (double *)alloc->Alloc(sizeof(double));
    assert(nullptr == pd);
    cerr << "alloc->print_status() : end" << endl;
    cout << "alloc->print_status() : end" << endl;
    alloc->print_status();
    // while (nullptr != (pd = (double *)alloc->Alloc(sizeof(double)))) {
    //     v.push_back(pd);
    // }
    delete alloc;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
