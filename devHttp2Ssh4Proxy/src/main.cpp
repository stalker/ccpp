// main.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#ifdef MACOS
# define EV_USE_KQUEUE 1
#endif

#include <iostream>
#include <cstdlib>  // for EXIT_FAILURE
#include "mydebug.hpp"

int main(int argc, char** argv) {
    std::ios_base::sync_with_stdio(0);
#if defined(DEBUG) && defined(PRINTM)
    printd("argc: ", argc, "; argv: ", argv) << std::endl;
#endif
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
