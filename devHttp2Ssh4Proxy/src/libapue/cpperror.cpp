// cpperror.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <cerrno>
#include <cstdarg>
#include <cppue.hpp>

namespace local
{
    extern "C" {
      int strerror_r(int errnum, char *buf, size_t buflen); /* XSI-compliant */
    }
}

std::ostream& cpfprintf(std::ostream& stm, const char* fmt, ...) {
    static char buf[BUF_SIZE + 1];

    va_list ap; va_start(ap, fmt);
    size_t n = vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);
    if(n >= sizeof(buf)) {
        cpperror(13, "Buffer overflow in cpfprintf.\n"); // buffer overflow
        buf[BUF_SIZE] = '\0';
    }
    std::cout << buf;
    return stm;
}

std::ostream& cpperror(int err, const char* msg) {
    static char buf[BUF_SIZE + 1];
    static char str[BUF_SIZE / 2 + 32];
    int ret = local::strerror_r(err, str, BUF_SIZE / 2 + 31);
    if (ret != EINVAL)
        snprintf(buf, BUF_SIZE, "%s: %s\n", msg, str);
    else
        snprintf(buf, BUF_SIZE, "%s: Unknown error %d\n", msg, err);
    std::cerr << buf;
    return std::cerr;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
