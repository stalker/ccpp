/* Circle_sort.c */
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* http://rosettacode.org/wiki/Circle_sort#C */
/*
 * Show both the initial, unsorted list and the final sorted list.
 * (Intermediate steps during sorting are optional.)
 *
 * Optimizations (like doing 0.5 log2(n) iterations and then continue with an
 * Insertion sort) are optional.
 *
 * Pseudo code: 
 * function circlesort (index lo, index hi, swaps)
 *  {
 *    if lo == hi return (swaps)
 *    high := hi
 *    low := lo
 *    mid := int((hi-lo)/2)
 *    while lo < hi {
 *      if  (value at lo) > (value at hi) {
 *         swap.values (lo,hi)
 *         swaps++
 *      }
 *      lo++
 *      hi--
 *    }
 *    if lo == hi
 *      if (value at lo) > (value at hi+1) {
 *          swap.values (lo,hi+1)
 *          swaps++
 *      }
 *    swaps := circlesort(low,low+mid,swaps)
 *    swaps := circlesort(low+mid+1,high,swaps)
 *    return(swaps)
 *  }
 *  while circlesort (0, sizeof(array)-1, 0)
 */

#include <stdio.h>

int circle_sort_inner(int *start, int *end)
{
    int *p, *q, t, swapped;

    if (start == end) return 0;

    // funny "||" on next line is for the center element of odd-lengthed array
    for (swapped = 0, p = start, q = end; p<q || (p==q && ++q); p++, q--)
        if (*p > *q)
            t = *p, *p = *q, *q = t, swapped = 1;

    // q == p-1 at this point
    return swapped | circle_sort_inner(start, q) | circle_sort_inner(p, end);
}

//helper function to show arrays before each call
void circle_sort(int *x, int n)
{
    do {
        int i;
        for (i = 0; i < n; i++) printf("%d ", x[i]);
        putchar('\n');
    } while (circle_sort_inner(x, x + (n - 1)));
}

int main(void)
{
    int x[] = {5, -1, 101, -4, 0, 1, 8, 6, 2, 3};
    circle_sort(x, sizeof(x) / sizeof(*x));

    return 0;
}
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */

