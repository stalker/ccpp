/* merge_sort.cpp */
/* $Date$
 * $Id$
 * $Version: 1.0$
 * $Revision: 1$
 */
// compl. example: g++ -std=c++11 merge_sort.cpp -o merge_sort
//                 icc -std=c++11 merge_sort.cpp -o merge_sort

#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# include "stdafx.h"
#endif
#include <cctype>
#include <cassert>
#include <string>
#include <iterator>
#include <vector>
#include <iostream>

using std::string;
using namespace std;

// merge ( упорядоченные последовательности A, B , буфер C ) {
// 		пока A и B непусты {
// 			cравнить первые элементы A и B 
// 			переместить наименьший в буфер
//      }
// 		если в одной из последовательностей еще есть элементы
// 			дописать их в конец буфера, сохраняя имеющийся порядок
// }

template<class T>
void merge(T a[], long lb, long split, long ub) {
// Слияние упорядоченных частей массива в буфер temp
// с дальнейшим переносом содержимого temp в a[lb]...a[ub]

    // текущая позиция чтения из первой последовательности a[lb]...a[split]
    long pos1 = lb;

    // текущая позиция чтения из второй последовательности a[split+1]...a[ub]
    long pos2 = split + 1;

    // текущая позиция записи в temp
    long pos3 = 0;  
    long temp_size = ub - lb + 1;

    T *temp = new T[temp_size];

    // идет слияние, пока есть хоть один элемент в каждой последовательности
    while (pos1 <= split && pos2 <= ub) {
        if (a[pos1] < a[pos2])
            temp[pos3++] = a[pos1++];
        else
            temp[pos3++] = a[pos2++];
    }

    // одна последовательность закончилась - 
    // копировать остаток другой в конец буфера 
    while (pos2 <= ub)   // пока вторая последовательность непуста 
        temp[pos3++] = a[pos2++];
    while (pos1 <= split)  // пока первая последовательность непуста
        temp[pos3++] = a[pos1++];

    // скопировать буфер temp в a[lb]...a[ub]
    for (pos3 = 0; pos3 < ub - lb + 1; pos3++)
        a[lb+pos3] = temp[pos3];

    delete [] temp;
}

// a - сортируемый массив, его левая граница lb, правая граница ub
template<class T>
void mergeSort(T a[], long lb, long ub) { 
    long split;                         // индекс, по которому делим массив

    if (lb < ub) {                      // если есть более 1 элемента

        split = (lb + ub)/2;

        mergeSort(a, lb, split);        // сортировать левую половину 
        mergeSort(a, split + 1, ub);  // сортировать правую половину 
        merge(a, lb, split, ub);        // слить результаты в общий массив
    }
}

template<class T>
void merge(vector<T> & v, long lb, long split, long ub) {
// Слияние упорядоченных частей массива в буфер temp
// с дальнейшим переносом содержимого temp в v[lb]...v[ub]
    // текущая позиция чтения из первой последовательности v[lb]...v[split]
    long pos1 = lb;
    // текущая позиция чтения из второй последовательности v[split+1]...v[ub]
    long pos2 = split + 1;
    // текущая позиция записи в temp
    long pos3 = 0;  
    long temp_size = ub - lb + 1;

    T *temp = new T[temp_size];

    // идет слияние, пока есть хоть один элемент в каждой последовательности
    while (pos1 <= split && pos2 <= ub) {
        if (v[pos1] < v[pos2])
            temp[pos3++] = v[pos1++];
        else
            temp[pos3++] = v[pos2++];
    }

    // одна последовательность закончилась - 
    // копировать остаток другой в конец буфера 
    while (pos2 <= ub)   // пока вторая последовательность непуста 
        temp[pos3++] = v[pos2++];
    while (pos1 <= split)  // пока первая последовательность непуста
        temp[pos3++] = v[pos1++];

    // скопировать буфер temp в a[lb]...a[ub]
    for (pos3 = 0; pos3 < ub - lb + 1; ++pos3)
        v[lb + pos3] = temp[pos3];

    delete [] temp;
}

// a - сортируемый массив, его левая граница lb, правая граница ub
template<class T>
void mergeSort(vector<T> & v, long lb, long ub) { 
    long split;                         // индекс, по которому делим массив

    if (lb < ub) {                      // если есть более 1 элемента

        split = (lb + ub)/2;

        mergeSort(v, lb, split);        // сортировать левую половину 
        mergeSort(v, split + 1, ub);  // сортировать правую половину 
        merge(v, lb, split, ub);        // слить результаты в общий массив
    }
}

// a - сортируемый массив, его левая граница lb, правая граница ub
template<class T>
void mergeSort(vector<T> & v) { 
    long lb = 0;
    long split;                         // индекс, по которому делим массив

    if (v.empty())
       assert(0 == 0);

    long ub = v.size();

    mergeSort(v, lb, ub - 1);
}

template<class T>
size_t bin_search(T a[], long lb, long ub, T e) {
    if ((ub - lb) < 1)
        return -1;
    auto mid = lb + (ub - lb)/2;
    while (mid != lb && a[mid] != e) {
        if (e < a[mid])
            ub = mid;
        else
            lb = mid + 1;
        mid = lb + (ub - lb)/2;
    }
    if (mid != ub) {
        return mid - lb;
    }
    return -1;
}

template<class T>
T & bin_search(int & i, vector<T> v, T & e) {
    i = -1;
    assert(not v.empty());
    auto v_beg = v.begin(), v_end = v.end();
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    if (3 > (v_end - v_beg)) {
        vector<T>::iterator p;
        for (p = v.begin(); p != v.end(); ++p)
            if (e == *p) {
                i = v_beg - p;
                return *p;
            }
        return *v_beg;
    }
#endif
    auto v_mid = v_beg + (v_end - v_beg)/2;
    while (v_mid != v_end && *v_mid != e) {
        if (e < *v_mid)
            v_end = v_mid;
        else
            v_beg = v_mid + 1;
        v_mid = v_beg + (v_end - v_beg)/2;
    }
    if (v_mid != v_end) {
        i = v_mid - v_beg;
        return *v_mid;
    }
    return e;
}

template<class T>
size_t bin_search(vector<T> v, T e) {
    size_t idx = -1;
    auto result = bin_search(idx, v, e);
    return idx;
}


#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# define main _tmain
#else
# define _TCHAR char
#endif
int main(int argc, _TCHAR * argv[])
{
    string s("Hello World!!!");
    for (auto &c : s)
        c = toupper(c);
    cout << s << endl;
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    articles.push_back("the");
    articles.push_back("a");
    articles.push_back("an");
#else
    vector<int> articles = {983,762,763,834,109,453,985,763,567,290,784,390};
#endif
    for (auto &e : articles)
        cout << "a:" << e << endl;
    int size = articles.size();
    int * arr = new int[size];
    std::copy(articles.begin(), articles.end(), arr);
    for (int i = 0; i < size; ++i)
        cout << "1 arr:" << arr[i] << endl;
    mergeSort(arr, 0, size - 1);
    for (int i = 0; i < size; ++i)
        cout << "2 arr:" << arr[i] << endl;
    // std::vector<int> v(std::begin(arr), std::end(arr));
    // std::vector<int> v(arr, arr + sizeof(arr)/sizeof(arr[0])) ;
    std::vector<int> v(articles);
    mergeSort(v);
    // for (int i = 0; i < size; ++i)
    //    v.push_back(arr[i]);

    delete [] arr;

    for (auto &e : v)
        cout << "v:" << e << endl;
    int tgt = 453;
    int idx = -2;
    int & res = bin_search(idx, v, tgt);
    if (-1 != idx) {
        cout << "res = "
           << res << endl
           << "index of res = "
           << idx
           << endl;
    } else
        cout << "res not found!" << endl
             << "return index = " << idx
             << endl;
    cout << "&tgt = "
         << static_cast<void*>(&tgt)
         << " tgt = "
         << tgt
         << endl;
    cout << "&res = "
         << static_cast<void*>(&res)
         << " res = "
         << res
         << endl;
    cout << "&v[" << idx << "] = "
         << static_cast<void*>(&v[idx])
         << " v[" << idx << "] = "
         << v[idx]
         << endl;
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */

