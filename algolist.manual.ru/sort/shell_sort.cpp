/* shell_sort.cpp */
/* $Date$
 * $Id$
 * $Version: 1.0$
 * $Revision: 1$
 */
// compl. example: g++ -std=c++11 shell_sort.cpp -o shell_sort
//                 icc -std=c++11 shell_sort.cpp -o shell_sort

#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# include "stdafx.h"
#endif
#include <cctype>
#include <string>
#include <iterator>
#include <vector>
#include <iostream>

using std::string;
using namespace std;

int increment(long inc[], long size) {
    int p1, p2, p3, s;

    p1 = p2 = p3 = 1;
    s = -1;
    do {
        if (++s % 2) {
            inc[s] = 8*p1 - 6*p2 + 1;
        } else {
            inc[s] = 9*p1 - 9*p3 + 1;
            p2 *= 2;
            p3 *= 2;
        }
        p1 *= 2;
    } while(3*inc[s] < size);  

    return s > 0 ? --s : 0;
}

template<class T>
void shellSort(T a[], long size) {
    long inc, i, j, seq[40];
    int s;

    // вычисление последовательности приращений
    s = increment(seq, size);
    while (s >= 0) {
        // сортировка вставками с инкрементами inc[] 
        inc = seq[s--];

        for (i = inc; i < size; i++) {
            T temp = a[i];
            for (j = (i - inc); (j >= 0) && (a[j] > temp); j -= inc)
                a[j + inc] = a[j];
            a[j + inc] = temp;
        }
    }
}

template<class T>
size_t bin_search(vector<T> v, T e) {
    if (v.empty())
        return -1;
    auto v_beg = v.begin(), v_end = v.end();
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    if (3 > (v_end - v_beg)) {
        vector<T>::iterator p;
        for (p = v.begin(); p != v.end(); ++p)
            if (e == *p) {
                return v_beg - p;
            }
        return v_beg - v_beg;
    }
#endif
    auto v_mid = v_beg + (v_end - v_beg)/2;
    while (v_mid != v_end && *v_mid != e) {
        if (e < *v_mid)
            v_end = v_mid;
        else
            v_beg = v_mid + 1;
        v_mid = v_beg + (v_end - v_beg)/2;
    }
    if (v_mid != v_end) {
        return v_mid - v_beg;
    }
    return e;
}


template<class T>
size_t bin_search(T a[], long lb, long ub, T e) {
    if ((ub - lb) < 1)
        return -1;
    auto mid = lb + (ub - lb)/2;
    while (mid != lb && a[mid] != e) {
        if (e < a[mid])
            ub = mid;
        else
            lb = mid + 1;
        mid = lb + (ub - lb)/2;
    }
    if (mid != ub) {
        return mid - lb;
    }
    return e;
}

#if (defined(_MSC_VER) && _MSC_VER >= 1600)
# define main _tmain
#else
# define _TCHAR char
#endif
int main(int argc, _TCHAR * argv[])
{
    string s("Hello World!!!");
    for (auto &c : s)
        c = toupper(c);
    cout << s << endl;
#if (defined(_MSC_VER) && _MSC_VER >= 1600)
    articles.push_back("the");
    articles.push_back("a");
    articles.push_back("an");
#else
    vector<int> articles = {983,762,763,834,109,453,985,763,567,290,784,390};
#endif
    int size = articles.size();
    int * arr = new int[size];
    std::copy(articles.begin(), articles.end(), arr);
    for (int i = 0; i < size; ++i)
        cout << "arr[" << i << "]=" << arr[i] << endl;
    for (int i = 0; i < size; ++i)
        cout << "arr[" << i << "]=" << arr[i] << endl;

    size_t i;
    auto res = bin_search(arr, 0, size, 453);
    if (-1 != i) {
        cout << "res = "
           << res << endl
           << "index of res = "
           << i
           << endl;
    } else
        cout << "res not found!" << endl
             << "return index = " << i
             << endl;
    delete [] arr;
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */

