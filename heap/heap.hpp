/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 4$
 */

#ifndef _HEAP_HPP_
#define _HEAP_HPP_

#include <cstddef>
#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>

#define DEBUG 1

static const char * __sp = " ";

enum heap_type { MaxHeap, MinHeap };

// индекс_родителя: для zero-based ← ⌊(i-1)/2⌋
inline ptrdiff_t parent(ptrdiff_t idx) { return (idx-1)/2; }
// левый_ребёнок:   для zero-based ← 2⋅i + 1
inline ptrdiff_t left(ptrdiff_t idx)   { return 2*idx + 1; }
// правый_ребёнок:  для zero-based ← 2⋅i + 2
inline ptrdiff_t right(ptrdiff_t idx)  { return 2*idx + 2; }

template <class T>
bool min_cmp(const std::vector<T> & arr, ptrdiff_t left_idx, ptrdiff_t right_idx) {
    return arr[right_idx] < arr[left_idx];
}

template <class T>
bool max_cmp(const std::vector<T> & arr, ptrdiff_t left_idx, ptrdiff_t right_idx) {
    return arr[left_idx] < arr[right_idx];
}

template <class T> class heap {
public:
    heap(heap_type heaptype = MaxHeap)
    : _ht(heaptype), _data(0), _size(0)
    , cmp(_ht == MaxHeap ? &max_cmp<T>: &min_cmp<T>) { /* None */ }

    typedef typename std::vector<T>::iterator itr_vec;

    heap(std::vector<T> v, heap_type heaptype = MaxHeap)
    : _ht(heaptype), _data(v), _size(v.size())
    , cmp(_ht == MaxHeap ? &max_cmp<T>: &min_cmp<T>) { /* None */ }

    heap(bool (*acmp)(const std::vector<T> &, ptrdiff_t, ptrdiff_t), heap_type ht)
    : _ht(ht), _data(0), _size(0), cmp(acmp) { /* None */ }

    const T& operator[](ptrdiff_t idx) const
    throw(std::range_error) {
        check_index(idx, "operator[]", __LINE__);
        return this->_data[idx];
    }

    ptrdiff_t size(void) const { return this->_size; }
    ptrdiff_t vector_size(void) const {
        return static_cast<ptrdiff_t>(this->_data.size());
    }

    std::vector<T> vector() const { return this->_data; }

    void swap(ptrdiff_t idx1, ptrdiff_t idx2)
    throw(std::range_error) {
        check_index(idx1, "swap(idx1)", __LINE__);
        check_index(idx2, "swap(idx2)", __LINE__);
        T t; t = this->_data[idx1];
        this->_data[idx1] = this->_data[idx2];
        this->_data[idx2] = t;
    }

    void set_heap_type(heap_type heaptype = MaxHeap)
    throw(std::range_error) {
        this->_ht = heaptype;
        if (this->_ht == MaxHeap)
            this->cmp = &max_cmp;
        else
            this->cmp = &min_cmp;
        build_heap();
    }

    heap_type get_heap_type() const {
        return this->_ht;
    }

    /**
     * { Просеивание вверх }
     * Heap_sift_up(A, i)
     *   пока i > 0 и A[индекс_родителя(i)] < A[i]
     *        Обменять A[i] ↔ A[индекс_родителя(i)]
     *        i ← индекс_родителя(i)
     */
    ptrdiff_t sift_up(ptrdiff_t idx)
    throw(std::range_error) {
        check_index(idx, "sift_up()", __LINE__);
        // default MaxHeap cmp: _data[parent(idx)] < _data[idx]
        while (idx > 0 && cmp(this->_data, parent(idx), idx)) {
            swap(idx, parent(idx));
            idx = parent(idx);
        }
        return idx;
    }

    /**
     * { Добовление value в кучу. Возвращает новый номер этого же элемента }
     * Heap_add(A, key)
     *   A.heap_size ← A.heap_size + 1
     *   A[A.heap_size] ← key
     *   Heap_sift_up(A, A.heap_size - 1)
     */
    ptrdiff_t add(T value)
    throw(std::range_error) {
        this->_size++;
        this->_data.push_back(value);
        return sift_up(this->size() - 1);
    }

    /**
     *{ Извлечение элемента }
     * Heap_remove(A, key)
     *   A[A.heap_size] ← -∞
     *   Heap_sift_up(A, A.heap_size - 1)
     *   Heap_extract_Max(A)
     */
    ptrdiff_t remove(ptrdiff_t idx)
    throw(std::range_error) {
        check_index(idx, "remove()", __LINE__);
        this->_data[idx] = this->_data[0];
        sift_up(idx);
        extract_top();
        return 1;
    }

    T top(void) const
    throw(std::range_error) {
        if (this->size() > 0)
            return this->_data[0];
        throw std::range_error("Heap Range Error in top() on line: " +
              std::to_string(__LINE__));
    }

    /**
     *{ Извлечение вехнего (максимума/минимума) элемента                       }
     *{ Выполняет извлечение максимального элемента из кучи за время O(log n). }
     *{ Извлечение выполняется в четыре этапа:                                 }
     *{ * значение корневого элемента (он и является максимальным) сохраняется }
     *{   для последующего возврата                                            }
     *{ * последний элемент копируется в корень, после чего удаляется из кучи  }
     *{ * вызывается Heapify для корня                                         }
     *{ * сохранённый элемент возвращается                                     }
     * Heap_extract_Max(A)
     *   if A.heap_size < 1
     *      then error "Куча пуста"
     *   result ← A[0]
     *   A[0] ← A[A.heap_size - 1]
     *   A.heap_size ← A.heap_size - 1
     *   Heapify(A, 0)
     *   return result
     */
    T extract_top()
    throw(std::range_error) {
        T top;
        check_heap_empty("top()", __LINE__);
        top = this->_data[0];
        this->_data[0] = this->_data[this->size() - 1];
        this->_data.pop_back();
        this->_size--;
        if (this->size() > 0) heapify(0);
        return top;
    }

    /**
     *{ Просеивание вниз }
     *{ Процедура выполняется за время O(log n). }
     * Heap_sift_down(A, i)
     *   l ← левый_ребёнок(i)
     *   r ← правый_ребёнок(i)
     *   maxIndex ← i
     *   если l < size и A[l] > A[maxIndex]
     *        maxIndex ← l
     *   если r < size и A[r] > A[maxIndex]
     *        maxIndex ← r
     *   если i <> maxIndex
     *        Обменять A[i] ↔ A[maxIndex]
     *        Heap_sift_down(A, maxIndex)
     */ // other heapify no recursive
    ptrdiff_t sift_down(ptrdiff_t idx)
    throw(std::range_error) {
        bool finish = false;
        while (left(idx) < this->size() && !finish) {
            ptrdiff_t cur_left = left(idx);
            ptrdiff_t cur_right = right(idx);
            ptrdiff_t extremum = idx;
            if (cmp(this->_data, extremum, cur_left))
                extremum = cur_left;
            if (cur_right < this->size()
                && cmp(this->_data, extremum, cur_right))
                extremum = cur_right;
            if (idx == extremum)
                finish = true;
            else
                swap(idx, extremum);
            idx = extremum;
        }
        return idx;
    }

    /**
     *{ Просеивание вверх                                                      }
     *{ Восстановление свойств кучи                                            }
     *{ Если в куче изменяется один  из  элементов,  то  она  может  перестать }
     *{ удовлетворять  свойству  упорядоченности.   Для  восстановления  этого }
     *{ свойства служит процедура Heapify. Она восстанавливает свойство кучи в }
     *{ дереве, у которого левое и правое поддеревья удовлетворяют  ему.   Эта }
     *{ процедура принимает на вход  массив  элементов  A  и  индекс  i.   Она }
     *{ восстанавливает свойство упорядоченности  во  всём  поддереве,  корнем }
     *{ которого является элемент A[i].                                        }
     *{ Если i-й элемент больше, чем его сыновья, всё поддерево  уже  является }
     *{ кучей, и делать ничего не надо.  В противном случае меняем местами i-й }
     *{ элемент с наибольшим из его сыновей, после чего выполняем Heapify  для }
     *{ этого сына.                                                            }
     *{ Процедура выполняется за время O(log n).                               }
     * Heap_heapify_recursive(A, i)
     *   l ← левый_ребёнок(i)
     *   r ← правый_ребёнок(i)
     *   maxIndex ← i
     *   если l < size и A[l] > A[maxIndex]
     *        maxIndex ← l
     *   если r < size и A[r] > A[maxIndex]
     *        maxIndex ← r
     *   если i <> maxIndex
     *        Обменять A[i] ↔ A[maxIndex]
     *        Heap_sift_down(A, maxIndex)
     */ // other sift_down recursive
    void heapify_recursive(ptrdiff_t idx)
    throw(std::range_error) {
        ptrdiff_t cur_left  = left(idx);
        ptrdiff_t cur_right = right(idx);
        ptrdiff_t extremum  = idx;

        // default MaxHeap cmp: _data[extremum] < _data[cur_left]
        if (cur_left  < this->size() &&
            cmp(this->_data, extremum, cur_left))
            extremum = cur_left;
        // default MaxHeap cmp: _data[extremum] < _data[cur_right]
        if (cur_right < this->size() &&
            cmp(this->_data, extremum, cur_right))
            extremum = cur_right;
        if (extremum != idx) {
            swap(idx, extremum);
            heapify_recursive(extremum);
        }
    }

    void heapify(ptrdiff_t idx)
    throw(std::range_error) {
        heapify_recursive(idx);
    }

    /**
     *{ Построение кучи                                                        }
     *{ Эта процедура предназначена  для  создания  кучи  из  неупорядоченного }
     *{ массива входных данных.                                                }
     *{ достаточно вызвать Heapify для всех элементов массива A, начиная  (при }
     *{ нумерации с первого элемента) с [N/2]-го и кончая первым.              }
     *{ Сложность O(n).                                                        }
     * Build_Heap(A)
     *   A.heap_size ← A.length
     *   для i ← ⌊A.length/2⌋ downto 0
     *     Heapify(A, i)
     */
    void build_heap()
    throw(std::range_error) {
        this->_size = static_cast<ptrdiff_t>(this->_data.size());
        for (ptrdiff_t idx = this->size() / 2 ; idx >= 0; --idx)
            heapify(idx);
    }

    /**
     *{ Пирамидальная сортировка                                               }
     *{ Восстановление свойств кучи                                            }
     *{ Процедура Heapsort сортирует  массив  без  привлечения  дополнительной }
     *{ памяти за время O(n log n)                                             }
     * Heap_sort(A)
     *   Build_Heap(A)
     *   для i ← A.length downto 0
     *     Обменять A[0] ↔ A[i]
     *     A.heap_size ← A.heap_size-1
     *     Heapify(A,0)
     */
    void sort()
    throw(std::range_error) {
        build_heap();
        for (ptrdiff_t idx = this->_data.size() - 1; idx >= 0; --idx) {
            swap(0, idx);
            this->_size--;
            heapify(0);
        }
    }

    /**
     *{ Проверка индекса на валидность.                                        }
     *{ В случае ошибочного интекса бросает исключение std::range_error, с     }
     *{ указанием функции где это произшло и строки.                           }
     */
    void check_index(ptrdiff_t idx, const std::string sfun, int line)
    throw(std::range_error) {
        if (0 > idx || idx > this->size())
            throw std::range_error("Heap Range Error index: " +
                  std::to_string(idx) + " in: " + sfun + " on line: " +
                  std::to_string(line));
    }

    /**
     *{ Проверка является ли куча пустой.                                      }
     *{ В случае пустой кучи бросает исключение std::range_error, с указанием  }
     *{ указанием функции где это произшло и строки.                           }
     */
    void check_heap_empty(const std::string sfun, int line)
    throw(std::range_error) {
        if (0 >= this->size())
            throw std::range_error("Heap Range Error size in: " +
                  sfun + " on line: " + std::to_string(line));
    }

    std::ostream& print(std::ostream& stm, const char * sp = __sp) const {
        ptrdiff_t idx = 0; for (; idx < this->size() - 1; ++idx)
            stm << this->_data[idx] << sp;
        if (this->size() > 0)
            stm << this->_data[idx];
        return stm;
    }

private:
    heap_type _ht;        // Тип кучи -- enum heap_type
    std::vector<T> _data; // Массив кучи -- A
    ptrdiff_t _size;      // Размер кучи -- heap_size
    bool (*cmp)(const std::vector<T> & v, ptrdiff_t left_idx, ptrdiff_t right_idx);
};

#endif
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
