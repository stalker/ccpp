/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 2$
 */

#include <iostream>
#include <iomanip>
#include <sstream>

#include "heap.hpp"

#define DEBUG 1

const char * sp = " ";

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setprecision(6)

/**
 *
 * Построение кучи
 * Переставить  элементы  заданного   массива   чисел   так,   чтобы   он
 * удовлетворял свойству мин-кучи.
 *
 * Вход. Массив чисел A[0 ... n − 1].
 * Выход. Переставить элементы массива так, чтобы выполнялись неравенства
 * A[i] ≤ A[2i + 1] и A[i] ≤ A[2i + 2] для всех i.
 *
 * Построение кучи — ключевой шаг  алгоритма  сортировки  кучей.   Данный
 * алгоритм имеет время работы O(n log n) в худшем случае  в  отличие  от
 * алгоритма быстрой сортировки, который гарантирует такую оценку  только
 * в среднем случае.  Алгоритм  быстрой  сортировки  чаще  используют  на
 * практике, поскольку в большинстве  случаев  он  работает  быстрее,  но
 * алгоритм сортировки кучей используется для внешней сортировки  данных,
 * когда  необходимо   отсортировать   данные   огромного   размера,   не
 * помещающиеся в память компьютера.
 * 
 * Чтобы превратить данный массив в кучу, необходимо произвести несколько
 * обменов его элементов.  Обменом мы называем базовую операцию,  которая
 * меняет местами элементы A[i] и A[j].  Ваша  цель  в  данной  задаче  —
 * преобразовать заданный массив в кучу за линей- ное количество обменов.
 *
 * Ограничения.  1 ≤ n ≤ 10⁵; 0 ≤ A[i] ≤ 10⁹ для всех 0 ≤ i ≤ n − 1;  все
 * A[i] попарно различны; i ̸= j.
 *
 * Отсортировать кучу пирамидальной сортировкой.
 */
int go(std::istream& instm, std::ostream& outstm) try {
    int n = 0;
    instm >> n;
    std::vector<int> inputVector;
    int i = 0; for (; i < n; i++) {
        int v = 0;
        instm >> v;
        inputVector.push_back(v);
    }
    heap<int> hpmin(inputVector, MinHeap);
    hpmin.build_heap();
    hpmin.print(outstm) << " | ";
    hpmin.sort();
    std::vector<int> s = hpmin.vector();
    i = 0; for (; i < s.size() - 1; ++i) {
        outstm << s[i] << " ";
    }
    outstm << s[i];
    return EXIT_SUCCESS;
} catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

std::string array_tests[] = {
"5"
" 1 2 3 4 5",
"1 2 3 4 5 | 5 4 3 2 1",
"5"
" 5 4 3 2 1",
"1 2 3 5 4 | 5 4 3 2 1",
"6"
" 0 1 2 3 4 5",
"0 1 2 3 4 5 | 5 4 3 2 1 0",
"6"
" 7 6 5 4 3 2",
"2 3 5 4 6 7 | 7 6 5 4 3 2"
};

int tests() {
    for (int i = 0; i < sizeof(array_tests) / sizeof(array_tests[0]) / 2; ++i) {
        std::cout << "Test" << i << std::endl;
        std::cout << "In: " << array_tests[2*i] << std::endl;
        std::istringstream iss(array_tests[2*i]);
        std::ostringstream oss;
        go(iss, oss);
        std::cout << "Out: " << oss.str() << " ";
        if (array_tests[2*i+1] == oss.str()) {
            std::cout << "== Ok!" << std::endl;
        } else {
            std::cout << "---- FAIL!!!" << std::endl;
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

/* let's go */
int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(0);
    return tests();
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
