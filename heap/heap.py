#!/usr/bin/python
# -- coding: utf-8 --
################################################################################
# $Date$
# $Id$
# $Version: 0.2$
# $Revision: 2$
# $Author: Victor |Stalker| Skurikhin <stalker@quake.ru>$
################################################################################
"""
heap
"""
# http://www.codeskulptor.org/#user40_k4fkfsEPly_1.py
#
################################################################################

BITS = 32
DEBUG = 1
DRAISE = 1

def to_binary(num):
    """
    return string binary code of num
    """
    return ''.join(str(1 & int(num) >> i) for i in range(BITS)[::-1])
#end def to_binary(n):

def parent(idx):
    """
    parent
    """
    return idx // 2
#end def parent(idx):

def left(idx):
    """
    left
    """
    return 2*idx
#end def left(idx):

def right(idx):
    """
    right
    """
    return 2*idx + 1
#end def right(idx):

def min_cmp(arr, left_idx, right_idx):
    """
    min_cmp
    """
    return arr[right_idx] < arr[left_idx]
#end def min_cmp(arr, left_idx, right_idx):

def max_cmp(arr, left_idx, right_idx):
    """
    max_cmp
    """
    return arr[left_idx] < arr[right_idx] 
#end def max_cmp(arr, left_idx, right_idx):

MinHeap = False
MaxHeap = True

class Heap(object):
    """
    binary heap
    """
    def __init__(self, heaptype = MaxHeap):
        self._data = [0]
        self._ht = heaptype
        self.len = 0
    #end def __init__(self):

    def __str__(self):
        return ' '.join(str(elm) for elm in self._data[1:])
    #end def __str__(self):

    def __getitem__(self, idx):
        if 0 < idx and idx < self.len:
            return self._data[idx]
        if DRAISE == DEBUG:
            raise IndexError('in __getitem__! list index out of range!')
        return None
    #end def __getitem__(self, index):

    def __len__(self):
        return self.len
    #end def __len__(self):

    def __ilshift__(self, other):
        """
        <<=
        """
        for elm in other:
            self._data.append(elm)
            self.len += 1
        #end for elm in other:
        return self
    #end def __ilshift__(self, other):

    def __lshift__(self, other):
        """
        <<
        """
        self.__ilshift__(other)
        self.build_heap()
        return self
    #end def __lshift__(self, other):

    def cmp(self, left_idx, right_idx):
        """
        cmp
        """
        if self._ht:
            return max_cmp(self._data, left_idx, right_idx)
        else:
            return min_cmp(self._data, left_idx, right_idx)
    #end def cmp(self, left_idx, right_idx):

    def swap(self, idx1, idx2):
        """
        binary heap swap
        """
        t = self._data[idx1]
        self._data[idx1] = self._data[idx2]
        self._data[idx2] = t
    #end def swap(self, idx1, idx2):

    def set_heap_type(self, heaptype = MaxHeap):
        """
        set_heap_type
        """
        self._ht = heaptype
        self.build_heap()
    #end def set_heap_type(self, heaptype = MaxHeap):

    def get_heap_type(self):
        """
        get_heap_type
        """
        return self._ht
    #end def get_heap_type(self):

    def sift_up(self, idx):
        """
        binary heap sift_up
        """
        if DRAISE == DEBUG and (1 > idx or idx > self.len):
            raise IndexError('in sift_up! list index out of range!')
        if idx > self.len: return 0
        # default MaxHeap cmp: _data[parent(idx)] < _data[idx]
        while idx > 1 and self.cmp(parent(idx), idx):
            self.swap(idx, parent(idx))
            idx = parent(idx)
        #end while idx > 1 and self.cmp(partn(idx), idx):
        return idx
    #end def sift_up(self, idx):

    def add(self, value):
        """
        binary heap add
        """
        self._data.append(value)
        self.len += 1
        return self.sift_up(self.len)
    #end def add(self, value):

    # TODO need test
    def remove(self, idx):
        """
        binary heap remove 
        """
        if 2 > idx or idx > self.len: return 0
        self._data[idx] = self._data[1]
        self.sift_up(idx)
        self.extract_top()
        return 1
    #end def remove(self, idx):

    def top(self):
        """
        binary heap extract 
        """
        top = None
        if self.len > 0:
            return self._data[1]
        #end if this->len > 0:
        if DRAISE == DEBUG:
            raise IndexError('in top! empty heap!')
        return None
    #end def top(self):

    def extract_top(self):
        """
        binary heap extract 
        """
        top = None
        if self.len == 0:
            if DRAISE == DEBUG:
                raise IndexError('in extract_top! empty heap!')
            return top
        #end if this->len == 0:
        top = self._data[1]
        self._data[1] = self._data[self.len]
        self._data.pop()
        self.len -= 1
        if self.len > 0: self.heapify(1)
        return top
    #end def extract_top(self):

    def get_opposite(self):
        """
        TODO get_opposite
        """
        start = self.len//2
        end   = self.len + 1
        if self.len < 1: return self._data[0]
        opposite = self._data[1]
        for idx in range(start, end):
            if self._ht == MaxHeap:
                if self._data[idx] < opposite:
                    opposite = self._data[idx]
            elif opposite < self_data[idx]: 
                opposite = self._data[idx]
            #end if self._ht == MaxHeap:
        #end for idx in range(start, end):
        return opposite
    #end def get_opposite(self):

    def sift_down(self, idx):
        """
        sift_down 
        other heapify no recursive
        """
        if 1 > idx and DRAISE == DEBUG:
            raise IndexError('in sift_down! list index out of range!')
        finish = False
        while left(idx) <= self.len and not finish:
            cur_left  = left(idx)
            cur_right = right(idx)
            extremum  = idx
            # default MaxHeap cmp: _data[temp] < _data[left(idx)]
            if self.cmp(extremum, cur_left):
                extremum = left(idx)
            # default MaxHeap cmp: _data[temp] < _data[right(idx)]
            if cur_right <= self.len and self.cmp(extremum, cur_right):
                extremum = cur_right
            if idx == extremum:
                finish = True
            else:
                self.swap(idx, extremum)
            idx = extremum
        #end while left(idx) <= self.len and not finish:
        return idx
    #end def sift_down(self, idx):

    def heapify_recursive(self, idx):
        """
        binary heap heapify self.cmp(partn(idx), idx)
        """
        cur_left  = left(idx)
        cur_right = right(idx)
        extremum  = idx
        if cur_left  <= self.len and self.cmp(extremum, cur_left):
            extremum = cur_left
        if cur_right <= self.len and self.cmp(extremum, cur_right):
            extremum = cur_right
        if extremum != idx:
            self.swap(idx, extremum)
            self.heapify_recursive(extremum)
        #end if extremum != idx:
    #end def heapify_recursive(self, idx):

    def heapify(self, idx):
        """
        heapify
        """
        if DRAISE == DEBUG and (1 > idx or idx > self.len):
            raise IndexError('in heapify! list index out of range!')
        self.heapify_recursive(idx)
    #end def heapify(self, idx):

    def build_heap(self):
        """
        binary heap build_heap
        """
        for idx in range(len(self) // 2, 0, -1):
            self.heapify(idx)
    #end def build_heap(self):
#end class Heap:

class MyPair(object):
    """
    binary heap
    """
    def __init__(self, first, key):
        self._data = tuple([first, key])
    #end def __init__(self):

    def __str__(self):
        return ' '.join(str(elm) for elm in self._data[:])
    #end def __str__(self):

    def __getitem__(self, index):
        return self._data[index]
    #end def __getitem__(self, index):

    def __len__(self):
        return len(self._data)
    #end def __len__(self):

    def __cmp__(self, other):
        if self._data[1] == other._data[1]: return 0
        if self._data[1] < other._data[1]:  return -1
        if self._data[1] > other._data[1]:  return 1
    #end def __cmp__(self, other):

    def __lt__(self, other):
        if self._data[1] < other._data[1]:
            return True
        else: return False
    #end def __cmp__(self, other):
#end class MyPair(object):

def main():
    """
    entry point
    """
    if DEBUG:
        print("test1")
        h1 = Heap(MinHeap)
        h1 << [MyPair(0, 1), MyPair(1, 0)]
        print(h1)
        h1.build_heap()
        print(h1)
        print("len = %d" % (len(h1)))
        print("top = %s" % (h1.top()))
        while len(h1) > 0:
            print(h1.extract_top())
        # print("" % ())
        try:
            print("h1.top() = %d" % (h1.top()))
        except IndexError:
            pass
        try:
            print("h1.extract_top() = %d" % (h1.extract_top()))
        except IndexError:
            pass
        print("test2")
        h2 = Heap()
        h2 << [0, 2, 4, 1, 8, 7, 9, 3, 14, 10, 16]
        print(h2)
        h2.build_heap()
        print(h2)
        print("len = %d" % (len(h2)))
        print("top = %s" % (h2.top()))
        h2.remove(11)
        print("h2.remove(11)")
        print(h2)
        h3 = Heap()
        h3 << [16, 14, 9, 3, 10, 7, 4, 0, 1, 2]
        print(h3)
        h2.remove(2)
        print("h2.remove(2)")
        print(h2)
        h3 = Heap()
        h3 << [16, 10, 9, 3, 2, 7, 4, 0, 1]
        print(h3)
        while len(h2) > 0:
            print(h2.extract_top())
        print("test4")
        h4 = Heap()
        print(h4._data)
        h4 <<= [0, 2, 4, 1, 8, 7, 9, 3, 14, 10, 16]
        print(h4)
        h4.sift_down(1)
        print(h4)
        try:
            h4.sift_up(0)
        except IndexError:
            pass
        try:
            h4.sift_up(20)
        except IndexError:
            pass
        try:
            h4.sift_down(0)
        except IndexError:
            pass
        try:
            h4.sift_down(20)
        except IndexError:
            pass
        try:
            h4.heapify(0)
        except IndexError:
            pass
        try:
            h4.heapify(20)
        except IndexError:
            pass
        try:
            print(h4[0])
        except IndexError:
            pass
        try:
            print(h4[20])
        except IndexError:
            pass
    else:
        n = int(raw_input())
        h = Heap()
        line = raw_input()
        for elm in line.split():
            h.add(int(elm))
        #end for _ in range(n):
        a = h.extract(1)
        b = h.extract(1)
        print(a * b)
    #end if DEBUG:
#end def main():

if __name__ == "__main__":
    main()

################################################################################
#EOF
# vim: syntax=python:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
