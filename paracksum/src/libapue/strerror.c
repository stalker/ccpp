#include    <errno.h>
#include    <stdio.h>

#if !defined(LINUX) && !defined(MACOS)
extern char    *sys_errlist[];
extern int    sys_nerr;
#endif

/**
 * вынужден использовать устаревшие sys_errlist[] и sys_nerr в обработчиках
 * сигналов, потому  что  функции  strerror()  и  strerror_r() не  являются
 * Async-Signal-Safe, и их нельзя использовать в обработчиках сигналов.
 */
char *
strerror(int error)
{
    static char    mesg[30];

    if (error >= 0 && error <= sys_nerr)
        return((char *)sys_errlist[error]);

    sprintf(mesg, "Unknown error (%d)", error);
    return(mesg);
}
