// main.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <deque>
#include <locale>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <cstdint>
extern "C" {
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <copy3.h>
#include <popen3.h>
}
#include <cppue.hpp>
#include <utf8.h>
#include "mydebug.hpp"

#define MICROSLEEP 9999
#define WITH_LIMIT 9

#define MSG1 " Done handling "
#define MSG3 " Error: cannot handle: "

#ifndef DEBUG
# define DEBUG 0
#endif

int do_cmd_debug = DEBUG;
int verbose = 0;

const int parallelism = 2;

void try_out_tmpfiles(std::deque<std::string> &);

/**
 * ф-ция выводит информацию о ppid и pid в stderr
 * возвращает - поток вывода cerr
 */
std::ostream & print_cerr_ppid_and_pid() {
    return printe("PID: ", getpid(), "; PPID: ", getppid());
}

void handler(int signal) {
    const char* sig_name;
    int rc = 0;
 
    switch (signal) {
      case SIGCHLD:
        sig_name = "SIGCHLD";
        int st;
        wait(&st);
        if (do_cmd_debug)
            printe("PID: ", getpid(), MSG1, sig_name, " status: ", st)
                << std::endl;
        return;
      case SIGTERM:
        sig_name = "SIGTERM";
        break;
      case SIGINT:
        sig_name = "SIGINT";
        return;
      default:
        if (do_cmd_debug)
            printe("Other signal: ", signal) << std::endl;
        return;
    }
    if (do_cmd_debug)
        printe(MSG1, sig_name, " for PID: ", getpid()) << std::endl;
    exit(rc);
}

void set_sig_handler() {
    struct sigaction sa;

    sa.sa_handler = &handler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        printe(MSG3, "SIGTERM");
        exit(2);
    }
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        printe(MSG3, "SIGINT");
        exit(2);
    }
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        printe(MSG3, "SIGCHLD");
        exit(2);
    }
}

/**
 * ф-ция возвращает true если в utf8-строке присутвует искомый символ
 * иначе false
 */
template<typename in_iterator>
bool does_qmark_exist(in_iterator begin, in_iterator end, char qmark = '"') {
    const size_t sizeSimbol = 5;
    char Symbol[sizeSimbol] = {0, 0, 0, 0, 0};
    uint32_t cp;

    while (begin < end) {
        memset(Symbol, 0, sizeSimbol);
        cp = utf8::next(begin, end);
        utf8::append(cp, Symbol);
        if (*Symbol == qmark) return true;
    }
    return false;
}

/**
 * ф-ция возвращает true если в utf8-строке присутствует символ
 * double quotation mark "
 * иначе false
 */
template<typename in_iterator>
bool does_double_qmark_exist(in_iterator begin, in_iterator end) {
    return does_qmark_exist(begin, end);
}

/**
 * ф-ция возвращает true если в utf8-строке присутствует символ
 * single quotation mark '
 * иначе false
 */
template<typename in_iterator>
bool does_single_qmark_exist(in_iterator begin, in_iterator end) {
    return does_qmark_exist(begin, end, '\'');
}

/**
 * ф-ция загружает и запускает cksum. Если вызов функции завершается успешно,
 * процесс cksum накладывается на "родительский" процесс; причем должно быть
 * достаточно памяти для загрузки и выполнения процесса cksum.
 * выход - void.
 * вход  - параметры:
 *      fdin - файловый дескриптор POSIX на чтение (stdin для cksum)
 *      fdout - файловый дескриптор POSIX на запись (stdout для cksum)
 */
void run_cksum(int fdin, int fdout) {
    const char cmd[] = "cksum";
    char * arguments[] = { (char *)cmd, NULL };
    if (execvp(cmd, arguments) < 0)
        err_sys("ошибка запуска программы %s", cmd);
    _exit(1);
}

/**
 * возвращает - шаблон имени временного файла
 */
const char * get_sql_templ_file() {
    static bool first = true;
    static std::string templ_file;
    if (first) {
        first = false;
#if defined(LINUX)
        templ_file = "/dev/shm/." + std::to_string(getpid()) + ".paracksum.out.";
#else
        templ_file = "/tmp/." + std::to_string(getpid()) + ".paracksum.out.";
#endif
    }
    return templ_file.c_str();
}

void try_out_tmpfiles(std::deque<std::string> & deq) {
    typedef std::deque<std::string>::iterator deq_iter_t;
    for (deq_iter_t i = deq.begin(); i != deq.end();) {
        std::ifstream is;
        std::string filename = *i;
        is.open(filename);
        if (!is.is_open()) {
            print_cerr_ppid_and_pid()
                << " WARNING! open:" << filename << " " << std::endl;
            ++i;
        } else {
            std::string sql;
            while (std::getline(is, sql)) {
                std::cout << sql << std::endl;
                std::cerr << sql << std::endl;
            }
            is.close();
            const char * cfilename = filename.c_str();
            if (-1 == unlink(cfilename)) {
                err_msg("ошибка выполнения функции unlink %s ", cfilename);
            } else {
                print_cerr_ppid_and_pid()
                    << " OK:" << filename << " " << std::endl;
            }
            i = deq.erase(i);
        }
        usleep(MICROSLEEP);
    }
}

void que_push_back(std::deque<std::string> & que) {
    for (int i = WITH_LIMIT; i > 0; --i) {
        std::string in;
        if (getline(std::cin, in)) {
            que.push_back(in);
            if (verbose) {
                print_cerr_ppid_and_pid()
                    << " QUEUE:" << in << std::endl;
            }
        } else {
            return;
        }
    }
}

/**
 * вспомогательная функция
 */
inline void while_my_waitpid(pid_t pid) {
    while (my_waitpid(pid) > 0) { /* empty */ }
}

int exec_child(std::string i, std::string o, int count, char *argv[]) {
    FILE * fpin, * fpout;
    const char * in = i.c_str();
    const char * out = o.c_str();

    /* проверяем возможность работы с файлами in, out и err */
    if (NULL != (fpin = fopen_on_err_return(in, "r"))) {
        fclose(fpin);
        int fdout = open_ex_err_exit(out);
        close(fdout);
        if (-1 == unlink(out)) {
            err_msg("ошибка выполнения функции unlink %s ", out);
        }
        /* argv[1] != NULL - имя определено иначе вывод в stderr */
        if (NULL != argv[1]) {
            FILE * fperr = fopen_on_err_exit(argv[1], "a+");
            fclose(fperr);
        }
#ifdef TEST_SLEEP
        popen3(run_mysleep, in, out, argv[1]);
#else
        popen3(run_cksum, in, out, argv[1]);
#endif
        printd(" popen3() failed!") << std::endl;
        _exit(1);
    } else {
        /* ошибка открытия на чтение файла in */
        /* создаём пустой фаил отчёта */
        fpout = fopen_on_err_return(out, "a+");
        fclose(fpout);
        exit(EXIT_FAILURE);
    }
}

int do_main(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <errpathname>";
        err_quit(ss.str().c_str());
    }

    std::deque<std::string> deq;
    std::deque<std::string> que;
    std::string tmpl_filename(get_sql_templ_file());

#if defined(WITH_LIMIT)
    que_push_back(que);
#else
    std::string in;
    while(getline(std::cin, in)) {
        que.push_back(in);
    }
#endif

    int count = 0;
    while (not que.empty()) {
        count++;
        if (verbose) {
            print_cerr_ppid_and_pid()
                << " COUNT:" << count << std::endl;
        }
        pid_t pid = -1;
        const std::string filename = que.front(); que.pop_front();
        const std::string tmp_filename = tmpl_filename + std::to_string(count);

        usleep(MICROSLEEP);
        pid = fork();

        if (pid == 0) {              // — порожденный процесс
            exec_child(filename, tmp_filename, count, argv);
        } else if (pid < 0) {        // — родительский процесс ошибка
            printd(" fork() failed!") << std::endl;
            exit(EXIT_FAILURE);
        }

        deq.push_back(tmp_filename);

        if (verbose) {
            print_cerr_ppid_and_pid()
                << " PUSH:" << tmp_filename
                << " CHILD: " << pid
                << " TARGET: "<< filename
                << std::endl;
        }

        if (0 == (count % parallelism)) {
            while_my_waitpid(-1);
            try_out_tmpfiles(deq);
        }
#if defined(WITH_LIMIT)
        que_push_back(que);
#endif
    }
    usleep(MICROSLEEP);
    while (not deq.empty()) {
        // while_my_waitpid(-1);
        try_out_tmpfiles(deq);
    }
    if (verbose) {
        std::cerr << "prepared: " << count << " files." << std::endl;
        std::cerr << "deq.size(): " << deq.size() << std::endl;
        std::cerr << "que.size(): " << que.size() << std::endl;
    }
    return EXIT_SUCCESS;
}

/* let's go */
int main(int argc, char * argv[]) {
    set_sig_handler();
    do_main(argc, argv);
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
