#pragma once
#ifndef _CPPUE_HPP_
#define _CPPUE_HPP_

#include <cassert>
extern "C" {
#include "apue.h"
}
#include <iostream>

#ifdef __APPLE__
# define MY_NOSIGNAL SO_NOSIGPIPE
#else
# define MY_NOSIGNAL MSG_NOSIGNAL
#endif

#ifndef BUF_SIZE
#define BUF_SIZE 0x1000
#endif

std::ostream& cpfprintf(std::ostream& stm, const char* fmt, ...);
std::ostream& cpperror(int err, const char* msg);

#define cpprintf(FORMAT, ...) \
        cpfprintf(std::cout, FORMAT, __VA_ARGS__)

#define handle_error_en(en, msg) \
        do { cpperror(en, msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
        do { cpperror(errno, msg); exit(EXIT_FAILURE); } while (0)

#endif  /* _CPPUE_HPP_ */
