// cpperror.hpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
#ifndef _CPPERROR_H_
#define _CPPERROR_H_

#ifndef _XOPEN_SOURCE
# if defined(SOLARIS)       /* Solaris 10 */
#  define _XOPEN_SOURCE  600
# else
#  define _XOPEN_SOURCE  700
# endif
#endif

#include <cerrno>
#include <cstdio>
#include <cstring> // #include <string.h>
#include <iostream>

#ifndef BUF_SIZE
#define BUF_SIZE 4096
#endif

std::ostream& cpperror(int err, const char* msg);

#define handle_error_en(en, msg) \
        do { cpperror(en, msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
        do { cpperror(errno, msg); exit(EXIT_FAILURE); } while (0)

#endif  /* _CPPERROR_H_ */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
