// main.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <deque>
#include <locale>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <ctime>
extern "C" {
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <copy3.h>
#include <popen3.h>
}
#include <cppue.hpp>
#include "mydebug.hpp"

// #define TEST_SLEEP 1

#define WITH_LIMIT_9 1

#define MSG1 "Done handling "
#define MSG3 "Error: cannot handle: "

#define MICROSLEEP 9999
#define WITH_LIMIT 9

#define SLEEP 1000000
#define DEF_PAGER "/bin/more"
const char def_pager[] = DEF_PAGER;

int do_cmd_debug = 1;
int verbose = 9;

const int parallelism = 1;

void try_out_tmpfiles(std::deque<std::string> &);
/**
 * ф-ция выводит информацию о ppid и pid в stderr
 * возвращает - поток вывода cerr
 */
std::ostream & print_cerr_ppid_and_pid() {
    return printe("PPID: ", getppid(), "; PID: ", getpid());
}

/**
 * ф-ция загружает и запускает cksum. Если вызов функции завершается успешно,
 * процесс cksum накладывается на "родительский" процесс; причем должно быть
 * достаточно памяти для загрузки и выполнения процесса cksum.
 * выход - void.
 * вход  - параметры:
 *      fdin - файловый дескриптор POSIX на чтение (stdin для cksum)
 *      fdout - файловый дескриптор POSIX на запись (stdout для cksum)
 */
void run_cksum(int fdin, int fdout) {
    const char cmd[] = "cksum";
    char * arguments[] = { (char *)cmd, NULL };
    if (execvp(cmd, arguments) < 0)
        err_sys("ошибка запуска программы %s", cmd);
    _exit(1);
}

void run_mysleep(int rfd, int wfd) {
    const char cmd[] = "mysleep";
    char * arguments[] = { (char *)cmd, NULL };
    if (execvp(cmd, arguments) < 0)
        err_sys("ошибка запуска программы %s", cmd);
    _exit(1);
}

static bool trigger = true;

void handler(int signal) {
    const char* sig_name;
    int rc = 0;
 
    switch (signal) {
      case SIGCHLD:
        sig_name = "SIGCHLD";
        int st;
        wait(&st);
        printe(MSG1, sig_name, " status:", st, " for PID: ", getpid())
            << std::endl;
        return;
      case SIGTERM:
        sig_name = "SIGTERM";
        break;
      case SIGINT:
        sig_name = "SIGINT";
        trigger = not trigger;
        return;
      default:
        printe("Other signal: ", signal) << std::endl;
        return;
    }
    printe(MSG1, sig_name, " for PID: ", getpid()) << std::endl;
    exit(rc);
}

void set_sig_handler() {
    struct sigaction sa;

    sa.sa_handler = &handler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        printe(MSG3, "SIGTERM");
        exit(2);
    }
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        printe(MSG3, "SIGINT");
        exit(2);
    }
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        printe(MSG3, "SIGCHLD");
        exit(2);
    }
}

void do_test1() {
    int   n;
    int   fd[2];
    pid_t pid;
    char  line[MAXLINE];

    if (pipe(fd) < 0)
        err_sys("pipe error");
    if ((pid = fork()) < 0) {
        err_sys("fork error");
    } else if (pid > 0) {                  /* parent */
        close(fd[0]);
        write(fd[1], "hello world\n", 12);
    } else {                               /* child */
        close(fd[1]);
        n = read(fd[0], line, MAXLINE);
        write(STDOUT_FILENO, line, n);
    }
    exit(EXIT_SUCCESS);
}

void do_test2() {
    int   n;
    int   fd[2];
    pid_t pid;
    char  line[MAXLINE];

    if (pipe(fd) < 0)
        err_sys("pipe error");
    if ((pid = fork()) < 0) {
        err_sys("fork error");
    } else if (pid > 0) {                  /* parent */
        printe("Ok fork PID: ", pid) << std::endl;

        close(fd[0]);
        while (trigger) {
            usleep(SLEEP);
            write(fd[1], "hello world\n", 12);
        }
    } else {                               /* child */
        close(fd[1]);
        while (0 < (n = read(fd[0], line, MAXLINE))) {
            write(STDOUT_FILENO, line, n);
            usleep(SLEEP);
        }
    }
    exit(EXIT_SUCCESS);
}

int do_test3(int argc, char * argv[]) {
    int   n;
    int   fd[2];
    pid_t pid;
    char  *pager, *argv0;
    char  line[MAXLINE];
    FILE  *fp;

    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <pathname>";
        err_quit(ss.str().c_str());
    }

    if ((fp = fopen(argv[1], "r")) == NULL)
        err_sys("невозможно открыть %s", argv[1]);
    if (pipe(fd) < 0)
        err_sys("ошибка вызова функции pipe");

    if ((pid = fork()) < 0) {
        err_sys("ошибка вызова функции fork");
    } else if (pid > 0) {            /* родительский процесс */
        close(fd[0]);                /* закрыть дескриптор для чтения */

        /* родительский процесс копирует argv[1] в канал */
        while (fgets(line, MAXLINE, fp) != NULL) {
            n = strlen(line);
            if (write(fd[1], line, n) != n)
                err_sys("ошибка записи в канал");
        }
        if (ferror(fp))
            err_sys("ошибка вызова функции fgets");

        close(fd[1]);                /* закрыть дескриптор для записи */

        if (waitpid(pid, NULL, 0) < 0)
            err_sys("ошибка вызова функции waitpid");
        exit(0);
    } else {                         /* дочерний процесс */
        close(fd[1]);                /* закрыть дескриптор для записи */
        if (fd[0] != STDIN_FILENO) {
            if (dup2(fd[0], STDIN_FILENO) != STDIN_FILENO)
                err_sys("ошибка переназначения канала на stdin");
            close(fd[0]);            /* уже не нужен вызов после вызова dup2 */
        }

        /* определить аргументы для execl() */
        if ((pager = getenv("PAGER")) == NULL)
            pager = const_cast<char*>(def_pager);
        if ((argv0 = strrchr(pager, '/')) != NULL)
            argv0++;                 /* перейти за последний слеш */
        else                           
            argv0 = pager;           /* в имени программы нет слеша */

        if (execl(pager, argv0, (char *)0) < 0)
            err_sys("ошибка запуска программы %s", pager);
    }

    return EXIT_SUCCESS;
}

inline FILE * my_fopen2(char * argv[], int num, const char * mode) {
    FILE * fp;
    if ((fp = fopen(argv[num], mode)) == NULL)
        err_sys("невозможно открыть %s", argv[num]);
    return fp;
}

inline void copy_fd_to_fp_chomp(FILE * fp, int fd) {
    char buffer[8192];
    while (1) {
        ssize_t count = read(fd, buffer, sizeof(buffer));
        if (count == -1) {
            if (errno == EINTR) {
                continue;
            } else {
                perror("read");
                exit(1);
            }
        } else if (count == 0) {
            break;
        } else {
            // handle_child_process_output(buffer, count);
            if ('\n' == buffer[count - 1])
                buffer[count - 1] = ' ';
            fwrite(buffer, sizeof(char), count, fp);
        }
    }
}

// http://www.microhowto.info/howto/capture_the_output_of_a_child_process_in_c.html
int do_test4(int argc, char * argv[]) {
    int   fd1[2];
    int   fd2[2];
    pid_t pid;
    char  *pager, *argv0;
    FILE  *fpin, *fpout;

    if (argc != 3) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <pathname>";
        err_quit(ss.str().c_str());
    }

    fpin = my_fopen2(argv, 1, "r");
    fclose(fpin);
    fpout = my_fopen2(argv, 2, "w");
    fclose(fpout);

    if (pipe(fd1) < 0)
        err_sys("ошибка вызова функции pipe для fd1");
    if (pipe(fd2) < 0)
        err_sys("ошибка вызова функции pipe для fd2");

    if ((pid = fork()) < 0) {
        err_sys("ошибка вызова функции fork");
    } else if (pid > 0) {            /* родительский процесс */
        int save_out = dup(STDOUT_FILENO);
        close(fd1[0]);               /* закрыть дескриптор для чтения */
        close(fd2[1]);               /* закрыть дескриптор для записи */

        fpin = my_fopen2(argv, 1, "r");
        /* родительский процесс копирует argv[1] в канал */
        copy_fp_to_fd(fd1[1], fpin);
        if (ferror(fpin))
            err_sys("ошибка вызова функции fgets");
        fclose(fpin);
        close(fd1[1]);                /* закрыть дескриптор для записи */

        fpout = my_fopen2(argv, 2, "w");
        copy_fd_to_fp_chomp(fpout, fd2[0]);
        fputs(argv[1], fpout);
        fclose(fpout);

        close(fd2[0]);                /* закрыть дескриптор для чтения */
        dup2(save_out, STDOUT_FILENO);

        std::cout << "Ok!" << std::endl;
        if (waitpid(pid, NULL, 0) < 0)
            err_sys("ошибка вызова функции waitpid");
        exit(0);
    } else {                          /* дочерний процесс */
        close(fd1[1]);                /* закрыть дескриптор для записи */
        if (fd1[0] != STDIN_FILENO) {
            if (dup2(fd1[0], STDIN_FILENO) != STDIN_FILENO)
                err_sys("ошибка переназначения канала на stdin");
            close(fd1[0]);            /* уже не нужен вызов после вызова dup2 */
        }

        /* определить аргументы для execl() */
        if ((pager = getenv("PAGER")) == NULL)
            pager = const_cast<char*>(def_pager);
        if ((argv0 = strrchr(pager, '/')) != NULL)
            argv0++;                 /* перейти за последний слеш */
        else                           
            argv0 = pager;           /* в имени программы нет слеша */

        while ((dup2(fd2[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
        close(fd2[0]);
        close(fd2[1]);

        if (execl(pager, argv0, (char *)0) < 0)
            err_sys("ошибка запуска программы %s", pager);
        _exit(1);
    }

    return EXIT_SUCCESS;
}

int do_test5(int argc, char * argv[]) {
    if (argc != 3) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <inpathname> <outpathname>";
        err_quit(ss.str().c_str());
    }
    popen3(run_cksum, argv[1], argv[2], NULL);
    return EXIT_SUCCESS;
}

int do_test6(int argc, char * argv[]) {
    if (argc != 4) {
        std::stringstream ss;
        ss << "usage: " << argv[0]
           << " <inpathname> <outpathname> <errpathname>";
        err_quit(ss.str().c_str());
    }
    popen3(run_cksum, argv[1], argv[2], argv[3]);
    return EXIT_SUCCESS;
}

int do_test7(int argc, char * argv[]) {
    if (argc != 4) {
        std::stringstream ss;
        ss << "usage: " << argv[0]
           << " <inpathname> <outpathname> <errpathname>";
        err_quit(ss.str().c_str());
    }
    popen3(run_mysleep, argv[1], argv[2], argv[3]);
    return EXIT_SUCCESS;
}

#ifdef NONE
int do_test8(int argc, char * argv[]) {
    if (argc != 3) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <outpathname> <errpathname>";
        err_quit(ss.str().c_str());
    }
    std::string in;
    std::vector<std::string> arr;

    while (getline(std::cin, in)) {
        arr.push_back(in);
    }

    int count = 0;
    while (not arr.empty()) {
        count++;
        pid_t pid = -1;
        std::string filename = arr.back(); arr.pop_back();

        usleep(9999);
        pid = fork();

        if (pid == 0) {              // — порожденный процесс
            popen3(run_mysleep, filename.c_str(), argv[1], argv[2]);
            printd(" popen3() failed!") << std::endl;
            _exit(1);
        } else if (pid < 0) {        // — родительский процесс ошибка
            printd(" fork() failed!") << std::endl;
            exit(EXIT_FAILURE);
        }

        if (0 == (count % parallelism))
            while (my_waitpid(-1) > 0) { /* empty */ }
    }
    while (my_waitpid(-1) > 0) { /* empty */ }
    return EXIT_SUCCESS;
}
#endif

int do_testX(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <pathname>";
        err_quit(ss.str().c_str());
    }
    FILE * fptmp;
    if (NULL == (fptmp = fopen_on_err_return(argv[1], "r"))) {
        fptmp = fopen_on_err_return(argv[1], "a+");
        fclose(fptmp);
        return 0;
    }
    fclose(fptmp);
    return 0;
}

/**
 * возвращает - шаблон имени временного файла
 */
const char * get_sql_templ_file() {
    static bool first = true;
    static std::string templ_file;
    if (first) {
        first = false;
#if defined(LINUX)
        templ_file = "/dev/shm/." + std::to_string(getpid()) + ".paracksum.out.";
#else
        templ_file = "/tmp/." + std::to_string(getpid()) + ".paracksum.out.";
#endif
    }
    return templ_file.c_str();
}

inline void while_my_waitpid(pid_t pid) {
    while (my_waitpid(pid) > 0) { /* empty */ }
}

void try_out_tmpfiles0() {
    static std::string tmpl_filename(get_sql_templ_file());
    int count = 0;
    for (int i = 1; i <= count; ++i) {
        std::ifstream is;
        std::string tmp_filename = tmpl_filename + std::to_string(i);
        is.open(tmp_filename);
        if (!is.is_open()) {
            print_cerr_ppid_and_pid()
                << " WARNING! open:" << tmp_filename << " " << std::endl;
        }
        else {
            std::string sql;
            while (std::getline(is, sql)) {
                std::cout << sql << std::endl;
            }
            is.close();
            const char * ctmp_filename = tmp_filename.c_str();
            if (-1 == unlink(ctmp_filename)) {
                err_msg("ошибка выполнения функции unlink %s ", ctmp_filename);
            }
        }
    }
}

int do_test9(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <errpathname>";
        err_quit(ss.str().c_str());
    }

    std::string in;
    std::deque<std::string> deq;
    std::deque<std::string> que;
    std::string tmpl_filename(get_sql_templ_file());

#if defined(WITH_LIMIT_9)
    for (int i = 9; i > 0; --i) {
        if (getline(std::cin, in)) {
            que.push_back(in);
            print_cerr_ppid_and_pid()
                << " QUEUE:" << in << std::endl;
        } else
            break;
    }
#else
    while(getline(std::cin, in)) {
        que.push_back(in);
    }
#endif
    int count = 0;
    do {
        count++;
        pid_t pid = -1;
        std::string filename = que.back(); que.pop_back();
        const std::string tmp_filename = tmpl_filename + std::to_string(count);

        usleep(9999);
        pid = fork();

        if (pid == 0) {              // — порожденный процесс
            FILE * fpin, * fpout;
            const char * in = filename.c_str();
            const char * out = tmp_filename.c_str();

            /* проверяем возможность работы с файлами in, out и err */
            if (NULL != (fpin = fopen_on_err_return(in, "r"))) {
                fclose(fpin);
                int fdout = open_ex_err_exit(out);
                close(fdout);
                if (-1 == unlink(out)) {
                    err_msg("ошибка выполнения функции unlink %s ", out);
                }
                /* argv[1] != NULL - имя определено иначе вывод в stderr */
                if (NULL != argv[1]) { 
                    FILE * fperr = fopen_on_err_exit(argv[1], "a+");
                    fclose(fperr);
                }
                popen3(run_mysleep, in, out, argv[1]);
                printd(" popen3() failed!") << std::endl;
                _exit(1);
            } else {
                /* ошибка открытия на чтение файла in */
                fpout = fopen_on_err_exit(out, "a+");
                fclose(fpout);
                exit(EXIT_FAILURE);
            }
        } else if (pid < 0) {        // — родительский процесс ошибка
            printd(" fork() failed!") << std::endl;
            exit(EXIT_FAILURE);
        }
        deq.push_back(tmp_filename);
        print_cerr_ppid_and_pid()
            << " PUSH:" << tmp_filename
            << " CHILD: " << pid
            << " TARGET: "<< filename
            << std::endl;

        if (0 == (count % parallelism)) {
            while_my_waitpid(-1);
            try_out_tmpfiles(deq);
        }
#if defined(WITH_LIMIT_9)
        for (int i = 9; i > 0; --i) {
            if (getline(std::cin, in)) {
                que.push_back(in);
                print_cerr_ppid_and_pid()
                    << " QUEUE:" << in << std::endl;
            } else
                break;
        }
#endif
    } while (not que.empty());
    usleep(9999);
    while (not deq.empty()) {
        while_my_waitpid(-1);
        try_out_tmpfiles(deq);
    }
    std::cerr << "prepared: " << count << " files." << std::endl;
    std::cerr << "deq.size(): " << deq.size() << std::endl;
    std::cerr << "que.size(): " << que.size() << std::endl;
    return EXIT_SUCCESS;
}

int do_test10(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <errpathname>";
        err_quit(ss.str().c_str());
    }
    std::string in;
    std::deque<std::string> deq;
    std::deque<std::string> arr;

    while (getline(std::cin, in)) {
        arr.push_back(in);
    }

    std::string tmpl_filename(get_sql_templ_file());

    int count = 0;
    while (not arr.empty()) {
        count++;
        pid_t pid = -1;
        std::string filename = arr.back(); arr.pop_back();
        const std::string tmp_filename = tmpl_filename + std::to_string(count);

        usleep(9999);
        pid = fork();

        if (pid == 0) {              // — порожденный процесс
            FILE * fpin, * fpout;
            const char * in = filename.c_str();
            const char * out = tmp_filename.c_str();

            /* проверяем возможность работы с файлами in, out и err */
            if (NULL != (fpin = fopen_on_err_return(in, "r"))) {
                fclose(fpin);
                int fdout = open_ex_err_exit(out);
                close(fdout);
                if (-1 == unlink(out)) {
                    err_msg("ошибка выполнения функции unlink %s ", out);
                }
                /* argv[1] != NULL - имя определено иначе вывод в stderr */
                if (NULL != argv[1]) { 
                    FILE * fperr = fopen_on_err_exit(argv[1], "a+");
                    fclose(fperr);
                }
                popen3(run_cksum, in, out, argv[1]);
                printd(" popen3() failed!") << std::endl;
                _exit(1);
            } else {
                /* ошибка открытия на чтение файла in */
                fpout = fopen_on_err_exit(out, "a+");
                fclose(fpout);
                exit(EXIT_FAILURE);
            }
        } else if (pid < 0) {        // — родительский процесс ошибка
            printd(" fork() failed!") << std::endl;
            exit(EXIT_FAILURE);
        }
        deq.push_back(tmp_filename);

        if (0 == (count % parallelism))
            while_my_waitpid(-1);
        try_out_tmpfiles(deq);
    }
    std::cerr << "prepared: " << count << " files." << std::endl;
    return EXIT_SUCCESS;
}

int do_test11(int argc, char * argv[]) {
    if (argc != 3) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <in> <out>";
        err_quit(ss.str().c_str());
    }
    FILE * fpin;
    if (NULL != (fpin = fopen_on_err_return(argv[1], "rb"))) {
        int fdout = open_rw_err_exit(argv[2]);
        FILE * dest = copy_fp_to_fd2(fdout, fpin);
        fclose(dest);
        fclose(fpin);
    }
    return EXIT_SUCCESS;
}

int do_test12(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <in>";
        err_quit(ss.str().c_str());
    }
    FILE * fpin;
    if (NULL != (fpin = fopen_on_err_return(argv[1], "rb"))) {
        int fdout = open_rw_err_exit(argv[2]);
        copy_fp_to_fd2(fileno(stdout), fpin);
        close(fdout);
        fclose(fpin);
    }
    return EXIT_SUCCESS;
}

int do_test13(int argc, char * argv[]) {
    if (argc != 3) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <in> <out>";
        err_quit(ss.str().c_str());
    }
    if (!copy_file9(argv[1], argv[2])) {
        perror("Error copy_file9!");
    }
    return EXIT_SUCCESS;
}

void try_out_tmpfiles(std::deque<std::string> & deq) {
    typedef std::deque<std::string>::iterator deq_iter_t;
    for (deq_iter_t i = deq.begin(); i != deq.end();) {
        std::ifstream is;
        std::string filename = *i;
        is.open(filename);
            print_cerr_ppid_and_pid()
                << " WARNING! !open:" << filename << " " << std::endl;
        if (!is.is_open()) {
            print_cerr_ppid_and_pid()
                << " WARNING! !open:" << filename << " " << std::endl;
            ++i;
        } else {
            std::string sql;
            while (std::getline(is, sql)) {
                std::cout << sql << std::endl;
                std::cerr << sql << std::endl;
            }
            is.close();
            const char * cfilename = filename.c_str();
            if (-1 == unlink(cfilename)) {
                err_msg("ошибка выполнения функции unlink %s ", cfilename);
            } else {
                print_cerr_ppid_and_pid()
                    << " OK:" << filename << " " << std::endl;
            }
            i = deq.erase(i);
        }
        usleep(9999);
    }
}

void que_push_back(std::deque<std::string> & que) {
    for (int i = WITH_LIMIT; i > 0; --i) {
        std::string in;
        if (getline(std::cin, in)) {
            que.push_back(in);
            if (verbose) {
                print_cerr_ppid_and_pid()
                    << " QUEUE:" << in << std::endl;
            }
        } else {
            return;
        }
    }
}

/**
 * вспомогательная функция
 */
static inline void my_while_dup2(int oldfd, int newfd) {
    while ((dup2(oldfd, newfd) == -1) && (errno == EINTR)) { /* empty */ }
}

int exec_child(std::string i, std::string o, int count, char *argv[]) {
    FILE * fpin, * fpout;
    const char * in = i.c_str();
    const char * out = o.c_str();

    /* проверяем возможность работы с файлами in, out и err */
    if (NULL != (fpin = fopen_on_err_return(in, "r"))) {
        fclose(fpin);
        int fdout = open_ex_err_exit(out);
        close(fdout);
        if (-1 == unlink(out)) {
            err_msg("ошибка выполнения функции unlink %s ", out);
        }
        /* argv[1] != NULL - имя определено иначе вывод в stderr */
        if (NULL != argv[1]) {
            FILE * fperr = fopen_on_err_exit(argv[1], "a+");
            fclose(fperr);
        }
#ifdef TEST_SLEEP
        popen3(run_mysleep, in, out, argv[1]);
#else
        popen3(run_cksum, in, out, argv[1]);
#endif
        printd(" popen3() failed!") << std::endl;
        _exit(1);
    } else {
        /* ошибка открытия на чтение файла in */
        /* создаём пустой фаил отчёта */
        fpout = fopen_on_err_return(out, "a+");
        fclose(fpout);
        exit(EXIT_FAILURE);
    }
}

int do_main(int argc, char * argv[]) {
    if (argc != 2) {
        std::stringstream ss;
        ss << "usage: " << argv[0] << " <errpathname>";
        err_quit(ss.str().c_str());
    }

    std::deque<std::string> deq;
    std::deque<std::string> que;
    std::string tmpl_filename(get_sql_templ_file());

#if defined(WITH_LIMIT)
    que_push_back(que);
#else
    std::string in;
    while(getline(std::cin, in)) {
        que.push_back(in);
    }
#endif

    int count = 0;
    while (not que.empty()) {
        count++;
        if (verbose) {
            print_cerr_ppid_and_pid()
                << " COUNT:" << count << std::endl;
        }
        pid_t pid = -1;
        const std::string filename = que.front(); que.pop_front();
        const std::string tmp_filename = tmpl_filename + std::to_string(count);

        usleep(MICROSLEEP);
        pid = fork();

        if (pid == 0) {              // — порожденный процесс
            exec_child(filename, tmp_filename, count, argv);
        } else if (pid < 0) {        // — родительский процесс ошибка
            printd(" fork() failed!") << std::endl;
            exit(EXIT_FAILURE);
        }

        deq.push_back(tmp_filename);

        if (verbose) {
            print_cerr_ppid_and_pid()
                << " PUSH:" << tmp_filename
                << " CHILD: " << pid
                << " TARGET: "<< filename
                << std::endl;
        }

        if (0 == (count % parallelism)) {
            while_my_waitpid(-1);
            try_out_tmpfiles(deq);
        }
#if defined(WITH_LIMIT)
        que_push_back(que);
#endif
    }
    usleep(MICROSLEEP);
    while (not deq.empty()) {
        // while_my_waitpid(-1);
        try_out_tmpfiles(deq);
    }
    if (verbose) {
        std::cerr << "prepared: " << count << " files." << std::endl;
        std::cerr << "deq.size(): " << deq.size() << std::endl;
        std::cerr << "que.size(): " << que.size() << std::endl;
    }
    return EXIT_SUCCESS;
}

/* let's go */
int main(int argc, char * argv[]) {
    clock_t start, end;
    start = clock();
    set_sig_handler();
    print_cerr_ppid_and_pid() << std::endl;
    do_main(argc, argv);
    end = clock();
    if (verbose) {
        std::cerr << "CLOCKS_PER_SEC " << CLOCKS_PER_SEC << "\n";
        std::cerr << "CPU-TIME START " << start << "\n";
        std::cerr << "CPU-TIME END " << end << "\n";
        std::cerr << "CPU-TIME END - START " << end - start << "\n";
        std::cerr << "TIME(SEC) "
                  << static_cast<double>(end - start) / CLOCKS_PER_SEC << "\n";
    }
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
