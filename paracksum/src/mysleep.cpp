// main.cpp
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <locale>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cppue.hpp>
extern "C" {
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
}
#include "cpperror.hpp"
#include "mydebug.hpp"

#define MSG1 "Done handling "
#define MSG3 "Error: cannot handle: "

/**
 * ф-ция выводит информацию о ppid и pid в stderr
 * возвращает - поток вывода cerr
 */
std::ostream & print_cerr_ppid_and_pid() {
    return printe("PPID: ", getppid(), "; PID: ", getpid());
}

static bool trigger = true;

void handler(int signal) {
    const char* sig_name;
    int rc = 0;
 
    switch (signal) {
      case SIGCHLD:
        sig_name = "SIGCHLD";
        int st;
        wait(&st);
        printe(MSG1, sig_name, " status:", st, " for PID: ", getpid())
            << std::endl;
        return;
      case SIGTERM:
        sig_name = "SIGTERM";
        break;
      case SIGINT:
        sig_name = "SIGINT";
        trigger = not trigger;
        return;
      default:
        printe("Other signal: ", signal) << std::endl;
        return;
    }
    printe(MSG1, sig_name, " for PID: ", getpid()) << std::endl;
    exit(rc);
}

void set_sig_handler() {
    struct sigaction sa;

    sa.sa_handler = &handler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        printe(MSG3, "SIGTERM");
        exit(2);
    }
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        printe(MSG3, "SIGINT");
        exit(2);
    }
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        printe(MSG3, "SIGCHLD");
        exit(2);
    }
}

void run_cksum(int rfd, int wfd) {
    const char cmd[] = "cksum";
    char * arguments[] = { (char *)cmd, NULL };
    if (execvp(cmd, arguments) < 0)
        err_sys("ошибка запуска программы %s", cmd);
    _exit(1);
}

/* let's go */
int main(int argc, char * argv[]) {
    set_sig_handler();
    useconds_t s = 0;
    if (argc == 2) {
        std::ifstream is;
        is.open(argv[1]);
        std::string msg("open file: ");
        msg += argv[1];
        if (!is.is_open()) {
            cpperror(errno, msg.c_str());
            return EXIT_FAILURE;
        }
        std::cerr << "Ok " << msg << std::endl;
        is >> s;
        is.close();
    } else {
        std::cin >> s;
    }
    std::cout << "PID: " << getpid() << " usleep: " << s << "..." << std::endl;
    std::cerr << "PID: " << getpid() << " usleep: " << s << "..." << std::endl;
    usleep(s);
    std::cout << "PID: " << getpid() << " usleep: " << s << " DONE." << std::endl;
    std::cerr << "PID: " << getpid() << " usleep: " << s << " DONE" << std::endl;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
