// copy3.c
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#ifdef LINUX
# include <sys/sendfile.h>
#elif defined(__APPLE__) || defined(__FreeBSD__)
# include <copyfile.h>
#else
# include <sys/socket.h>
# include <sys/uio.h>
#endif
#include <sys/socket.h>
#include <copy3.h>

extern int do_cmd_debug;

FILE * fopen_on_err_exit(const char * filename, const char * mode) {
    FILE * fp;
    if ((fp = fopen(filename, mode)) == NULL)
        err_sys("error! невозможно открыть %s", filename);
    return fp;
}

FILE * fopen_on_err_return(const char * filename, const char * mode) {
    FILE * fp;
    if ((fp = fopen(filename, mode)) == NULL)
        err_ret("warning невозможно открыть %s", filename);
    return fp;
}

int open_ex_err_return(const char * filename) {
    int fd;
    if ((fd = open(filename, O_RDWR | O_CREAT | O_EXCL, MASK644)) < 0)
        err_ret("PID: %d, warning невозможно открыть %s", getpid(), filename);
    return fd;
}

int open_ex_err_exit(const char * filename) {
    int fd;
    if ((fd = open(filename, O_RDWR | O_CREAT | O_EXCL, MASK644)) < 0)
        err_sys("PID: %d, warning невозможно открыть %s", getpid(), filename);
    return fd;
}

int open_rw_err_exit(const char * filename) {
    int fd;
    if ((fd = open(filename, O_RDWR | O_CREAT | O_APPEND, MASK644)) < 0)
        err_sys("PID: %d, warning невозможно открыть %s", getpid(), filename);
    return fd;
}

int copy_fp0(FILE * fdest, FILE * fsource) {
    for (;;) {
        int c = fgetc(fsource);
        if (ferror(fsource) || ferror(fdest)) {
         /* perror printa anche l'errore che ferror ha incontrato */
            return 0; /* Failure */
        }
        if (c != EOF) {
            fputc(c, fdest);
        } else {
            break;
        }
    }
    return 1; /* Success */
}

/**
 * ANSI-C-WAY
 */
int copy_fp1(FILE * fdest, FILE * fsource) {
    char buf[BUFSIZE];
    // BUFSIZE default is 8192 bytes
    // BUFSIZE of 1 means one chareter at time
    // good values should fit to blocksize, like 1024 or 4096
    // higher values reduce number of system calls
    // size_t BUFFER_SIZE = 4096;
    size_t size;

    // clean and more secure
    // feof(FILE* stream) returns non-zero if the end of file indicator for stream is set
    while (0 != (size = fread(buf, 1, BUFSIZE, fsource))) {
        fwrite(buf, 1, size, fdest);
        if (ferror(fsource) || ferror(fdest)) {
            return 0; /* Failure */
        }
    }
    return 1; /* Success */
}

/**
 * POSIX-WAY (K&R use this in "The C programming language", more low-level)
 */
int copy_fd(int ddest, int dsource) {
    char buf[BUFSIZE];
    ssize_t size;

    // int source = fileno(fsource);

    while ((size = read(dsource, buf, BUFSIZ)) > 0) {
        write(ddest, buf, size);
    }

    if (size < 0)
        return 0; /* Failure */
    return 1;     /* Success */
}

/**
 * вспомогательная функция копирует файл из предварительно открытого указателя
 * на файловый поток ISO C в предварительно открытый файловый дескриптор POSIX
 * выход - void
 * вход  - параметры:
 *      fd - файловый дескриптор POSIX
 *      fp - указатель на файловый поток ISO C
 */
FILE * copy_fp_to_fd1(int dest, FILE * fsource) {
    FILE * fdest = fdopen(dest, "a");

    if (NULL == fdest) {
        err_sys("Error in copy_fp_to_fd1(fdopen): ");
    }

    if (!copy_fp(fdest, fsource)) {
     /* perror printa anche l'errore che ferror ha incontrato */
        err_sys("Error in copy_fp_to_fd1(copy_fp): ");
    }
    return fdest;
}

/**
 * POSIX-WAY (K&R use this in "The C programming language", more low-level)
 */
FILE * copy_fp_to_fd2(int ddest, FILE * fsource) {
    int dsource = fileno(fsource);

    if (!copy_fd(ddest, dsource)) {
     /* perror printa anche l'errore che ferror ha incontrato */
        err_sys("Error in copy_fp_to_fd2(copy_fd): ");
    }

    FILE * fdest = fdopen(ddest, "a");
    if (NULL == fdest) {
        err_sys("Error in copy_fp_to_fd2(fdopen): ");
    }
    return fdest;
}

/**
 * вспомогательная функция копирует файл
 * выход - false(0) - ошибка true(1) - удачно
 * вход  - параметры:
 */
int copy_file0(const char * source, const char * dest) {
    FILE * fsource = fopen(source, "rb");
    FILE * fdest = fopen(dest, "wb");
    if (NULL == fsource || NULL == fdest) {
        return 0; /* Failure */
    }
    if (!copy_fp0(fdest, fsource)) {
     /* perror printa anche l'errore che ferror ha incontrato */
        perror("Error in copy_file0(copy_fp0)");
        fclose(fsource);
        fclose(fdest);
        return 0; /* Failure */
    }
    fclose(fsource);
    fclose(fdest);
    return 1; /* Success */
}

/**
 * ANSI-C-WAY
 */
int copy_file1(const char * source, const char * dest) {
    FILE * fsource = fopen(source, "rb");
    FILE * fdest = fopen(dest, "wb");
    if (NULL == fsource || NULL == fdest) {
        return 0; /* Failure */
    }
    if (!copy_fp1(fdest, fsource)) {
     /* perror printa anche l'errore che ferror ha incontrato */
        perror("Error in copy_file1(copy_fp1)");
        fclose(fsource);
        fclose(fdest);
        return 0; /* Failure */
    }
    return 1; /* Success */
}

/**
 * POSIX-WAY (K&R use this in "The C programming language", more low-level)
 */
int copy_file2(const char * source, const char * dest) {
    int dsource = open(source, O_RDONLY, 0);
    int ddest = open(dest, O_WRONLY | O_CREAT /*| O_TRUNC*/, MASK644);
    if (dsource < 0 || ddest < 0)
        return 0; /* Failure */
    int result = copy_fd(ddest, dsource);
    close(dsource);
    close(ddest);
    return result;
}

#if defined(LINUX)
ssize_t do_sendfile(int out_fd, int in_fd, off_t offset, size_t count) {
    ssize_t bytes_sent;
    size_t total_bytes_sent = 0;
    while (total_bytes_sent < count) {
        if ((bytes_sent = sendfile(out_fd, in_fd, &offset,
                count - total_bytes_sent)) <= 0) {
            if (errno == EINTR || errno == EAGAIN) {
                // Interrupted system call/try again
                // Just skip to the top of the loop and try again
                continue;
            }
            perror("sendfile");
            return -1;
        }
        total_bytes_sent += bytes_sent;
    }
    return total_bytes_sent;
}


/* http://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c
 * http://www.unixguide.net/unix/programming/2.5.shtml
 * About locking mechanism...
 */
int copy_fileX(const char *source, const char *dest){
   int dsource = open(source, O_RDONLY);

   /* Caf's comment about race condition... */
   if (dsource > 0){
     if (lockf(dsource, F_TEST, 0) == -1) { return 0; } /* FAILURE */
   } else return 0; /* FAILURE */

   err_msg("ok");

   /* Now the fdSource is locked */

   int ddest = open(dest, O_RDWR | O_CREAT, MASK644);
   struct stat source_stat;

   if (dsource > 0 && ddest > 0){
      if (!stat(source, &source_stat)){
          ssize_t len = do_sendfile(ddest, dsource, 0, source_stat.st_size);
          if (len > 0 && len == source_stat.st_size){
               close(ddest);
               close(dsource);

               /* Sanity Check for Lock, if this is locked -1 is returned! */
               if (lockf(dsource, F_TLOCK, 0) == -1){
                   if (lockf(dsource, F_ULOCK, 0) == -1){
                      /* WHOOPS! WTF! FAILURE TO UNLOCK! */
                   } else {
                      return 1; /* Success */
                   }
               } else {
                   /* WHOOPS! WTF! TEST LOCK IS 0 WTF! */
                   return 0; /* FAILURE */
               }
          }
      }
   }
   return 0; /* Failure */
}
#endif

/**
 * LINUX-WAY // requires kernel >= 2.6.33
 * or FreeBSD and and OS X 10.5+
 */
FILE * copy_fp_to_fd9(int ddest, FILE * fsource) {
    err_msg("Ok copy_fp_to_fd9");
    int dsource = fileno(fsource);
    if (dsource < 0) {
        err_sys("Error in copy_fp_to_fd9(fileno): ");
    }

#ifdef LINUX
    // struct required, rationale: function stat() exists also
    struct stat fileinfo = {0};
    fstat(dsource, &fileinfo);
    //sendfile will work with non-socket ddest (i.e. regular file) on Linux 2.6.33+
    // sendfile(int, int, off_t, off_t *, struct sf_hdtr *, int);
    // ssize_t result = do_sendfile(dest, source, 0, stat_source.st_size);
    ssize_t result = do_sendfile(ddest, dsource, 0, fileinfo.st_size);
#elif defined(__APPLE__) || defined(__FreeBSD__)
    //fcopyfile works on FreeBSD and OS X 10.5+ 
    copyfile_state_t s;
    s = copyfile_state_alloc();
    int result = fcopyfile(dsource, ddest, NULL, COPYFILE_ALL);
    copyfile_state_free(s);
#else
    err_sys("Error copy_fp_to_fd9 only Linux, FreeBSD and Mac OS X!");
#endif

    if (0 > result) {
        if (EINVAL == errno) {
            err_msg("EINVAL");
        }
        err_sys("Error in copy_fp_to_fd9(sendfile): ");
    }

    FILE * fdest = fdopen(ddest, "a");
    if (NULL == fdest) {
        err_sys("Error in copy_fp_to_fd9(fdopen):");
        exit(1);
    }
    return fdest;
}

int copy_file9(const char* source, const char* destination)
{
    int dsource, ddest;
    if ((dsource = open(source, O_RDONLY)) == -1) {
        return 0; /* Failure */
    }
    if ((ddest = open(destination, O_RDWR | O_CREAT, MASK644 )) == -1) {
        close(dsource);
        return 0; /* Failure */
    }

    //Here we use kernel-space copying for performance reasons
#if defined(__APPLE__) || defined(__FreeBSD__)
    //fcopyfile works on FreeBSD and OS X 10.5+ 
    int result = fcopyfile(dsource, ddest, 0, COPYFILE_ALL);
#else
    //sendfile will work with non-socket ddest (i.e. regular file) on Linux 2.6.33+
    off_t bytesCopied = 0;
    struct stat fileinfo = {0};
    fstat(dsource, &fileinfo);
    ssize_t result = do_sendfile(ddest, dsource, 0, fileinfo.st_size);
#endif

    close(dsource);
    close(ddest);

    return result >= 0;
}


/**
 * вспомогательная функция копирует файл из предварительно открытого файлового
 * дескриптора POSIX в предварительно открытый указатель на файловый поток ISO C
 * выход - void
 * вход  - параметры:
 *      fp - указатель на файловый поток ISO C
 *      fd - файловый дескриптор POSIX
 * особенность/feature: если последний прочитанный символ перевод строки,
 *                      заменяет его на пробел
 */
void copy_fd_to_fp_chomp(FILE * fp, int fd) {
    char buffer[MAXBUFFER];
    while (1) {
        ssize_t count = read(fd, buffer, sizeof(buffer));
        if (count == -1) {
            if (errno == EINTR) {
                continue;
            } else {
                perror("read");
                exit(1);
            }
        } else if (count == 0) {
            break;
        } else {
            // handle child process output
            if ('\n' == buffer[count - 1])
                buffer[count - 1] = ' ';
            fwrite(buffer, sizeof(char), count, fp);
        }
    }
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
