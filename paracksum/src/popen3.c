// popen3.c
/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <copy3.h>
#include <popen3.h>

extern int do_cmd_debug;

/**
 * waitpid(): on success, returns  the process ID of the child whose state has
 * changed.   On  error, -1  is returned.
 *
 * WIFEXITED(status)   - returns true  if  the child terminated normally, that
 *                       is,  by calling exit(3) or _exit(2),  or by returning
 *                       from main().
 *
 * WIFSIGNALED(status) - returns true if the child process was terminated by a
 *                       signal.
 *
 * The parent process executes a loop that monitors the child using waitpid(),
 * and uses the W*() macros to analyze the wait status value.
 * Родительский процесс выполняет цикл, который контролирует ребенка с помощью
 * waitpid (), и использует W * () макросы, чтобы проанализировать значение
 * состояниe ожидания.
 */

/**
 * выход - pid - номер процесса.
 * вход  - параметры:
 *      fd - файловый дескриптор POSIX
 *      fp - указатель на файловый поток ISO C
 */
int my_waitpid(pid_t pid) {
    int status, w;
    do {
        w = waitpid(pid, &status, WUNTRACED | WCONTINUED);
        if (w == -1) {
            if (do_cmd_debug)
                err_msg("PID: %d, waitpid(%d):", getpid(), pid);
            /* or * perror("waitpid"); exit(EXIT_FAILURE); */
        } else if (do_cmd_debug)
            err_msg("PID: %d, waitpid, return=%d", getpid(), w);

        if (do_cmd_debug && WIFEXITED(status)) {
            err_msg("PID: %d, exited, status=%d",getpid(), WEXITSTATUS(status));
        } else if (do_cmd_debug && WIFSIGNALED(status)) {
            err_msg("PID: %d, killed by signal %d",getpid(), WTERMSIG(status));
        } else if (do_cmd_debug && WIFSTOPPED(status)) {
            err_msg("PID: %d, stopped by signal %d",getpid(), WSTOPSIG(status));
        } else if (do_cmd_debug && WIFCONTINUED(status)) {
            err_msg(".");
        }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    return w;
}

/**
 * вспомогательная функция
 */
static inline void my_while_dup2(int oldfd, int newfd) {
    while ((dup2(oldfd, newfd) == -1) && (errno == EINTR)) { /* empty */ }
}

/**
 * ф-ция  подготовки  и запуска  команды,  а так  же подготовки межпроцессного
 * взаимодействия, данная ф-ция создаёт две трубы (pipe), первая труба (канал)
 * pcfd - создаётся для перенаправления стандартного вывода stdout родителя на
 * стандартный вход stdin потомка запущенного в pfunc, схема:
 *
 * parent ---(stdout)---> [pcfd[1] --- pcfd[0]] ---(stdin)---> pfunc(exec)
 *
 * вторая труба (канал) cpfd  - создаётся   для  перенаправления  стандартного
 * вывода stdout потомка запущенного  в pfunc(exec)  на стандартный вход stdin
 * родителя через файл out, схема:
 *
 * pfunc(exec/child) --(stdout)--> [cpfd[1] --- cpfd[0]] --(stdin)--> parent
 *
 * выход - зевершение процесса!
 * вход  - параметры:
 *      pfunc - указатель  на функцию (типа pfunc_t), эта функция вызывается в 
 *              дочернем процессе который порождается fork-ом.
 *      in    - имя  входного файла, файл открывается  на чтение и потоётся на
 *              stdin-вход в функцию pfunc;
 *      out   - имя  выходного файла, файл  открывается/создаётся  на запись и
 *              вывод из stdout-потока функции pfunc направляется в этот файл;
 */
int popen3(pfunc_t pfunc, const char *in, const char *out, const char *err) {
    int   pcfd[2]; /* труба/канал(pipe) от родительского процесса к дочернему */
    int   cpfd[2]; /* труба/канал(pipe) от дочернего процесса к родительскому */

    int fderr, save_err = -1;

    pid_t parent_pid = getpid();

    if (NULL != err) {               /* перенаправляем sterr если имя опр-но */
        if (do_cmd_debug)
            err_msg("PID: %d, перенаправляем stderr в:%s", parent_pid, err);
        int flags = O_RDWR | S_IRUSR | S_IWUSR | O_APPEND;
        if ((fderr = open(err, flags)) > 0) {
            lseek(fderr, 0L, SEEK_END);
            save_err = dup(STDERR_FILENO);
            my_while_dup2(fderr, STDERR_FILENO);
        } else
            err_sys("невозможно открыть %s", err);
    }

    if (pipe(pcfd) < 0)
        err_sys("PID: %d, ошибка вызова функции pipe для pcfd", parent_pid);
    if (pipe(cpfd) < 0 )
        err_sys("PID: %d, ошибка вызова функции pipe для cpfd", parent_pid);

    if (do_cmd_debug)
        err_msg("PID: %d, fork ...", parent_pid);

    pid_t pid;

    if ((pid = fork()) < 0) {
        err_sys("PID: %d, ошибка вызова функции fork", parent_pid);
        return EXIT_SUCCESS;
    } else if (0 == pid) {       /* дочерний процесс */
        pid_t child_pid = getpid();
        if (do_cmd_debug)
            err_msg("PID: %d, forked by parent: %d ", child_pid, getppid());
        close(pcfd[1]);              /* закрыть дескриптор для записи */
        if (pcfd[0] != STDIN_FILENO) {
            if (dup2(pcfd[0], STDIN_FILENO) != STDIN_FILENO)
                err_sys("ошибка переназначения канала на stdin");
            close(pcfd[0]);          /* уже не нужен вызов после вызова dup2 */
        }

        my_while_dup2(cpfd[1], STDOUT_FILENO);

        close(cpfd[0]);
        // close(cpfd[1]); ???

        if (do_cmd_debug)
            err_msg("PID: %d, запускаем: pfunc", child_pid);
        pfunc(pcfd[0], cpfd[1]);
    }

    /* родительский процесс */

    FILE * fpin, * fpout;
    int save_out = dup(STDOUT_FILENO);
    close(pcfd[0]);              /* закрыть дескриптор для чтения */
    close(cpfd[1]);              /* закрыть дескриптор для записи */

    fpin = fopen_on_err_return(in, "rb");
    /* родительский процесс копирует содержимое файла с именем in в канал */
    FILE * dest = copy_fp_to_fd(pcfd[1], fpin);
    fclose(dest);
    close(pcfd[1]);              /* закрыть дескриптор для записи */
    fclose(fpin);

    /* родительский процесс читает из канала вывод дочернего процесса */
    fpout = fdopen(open_ex_err_return(out), "w+");
    copy_fd_to_fp_chomp(fpout, cpfd[0]);
    fputs(in, fpout);
    fclose(fpout);

    close(cpfd[0]);              /* закрыть дескриптор для чтения */

    if (do_cmd_debug)
        err_msg("PID: %d, восстанавливаем stdout:%d stderr:%d",
                parent_pid, save_out, save_err);

    dup2(save_out, STDOUT_FILENO);

    int catch_pid = 0;
    if ((catch_pid = my_waitpid(pid)) < 0)
        if (do_cmd_debug)
            err_ret("PID: %d, ошибка вызова функции my_waitpid(%d): "
                    "catch_pid:%d", parent_pid, pid, catch_pid);
    if (save_err != -1) {
        dup2(save_err, STDERR_FILENO);
    }
    exit(0);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
