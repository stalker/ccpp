#!/bin/bash
if [ "x$1" = "x" -o "x$2" = "x" ] ; then
  echo "Usage: $0 <dir> <Number>"
  exit 1
fi
if [ ! -d $1 ] ; then
  echo "Is doesn't directory: $1"
  exit 2
fi
if [ "${2}0" -ge 10000000 ] ; then
  echo "Munber must be less than 1000000!"
  exit 3
fi
D=`date +%F`
REPORT="${HOME}/tmp/${D}.out_run_test.txt"
ERR1REP="${HOME}/tmp/${D}.err1_run_test.txt"
ERR2REP="${HOME}/tmp/${D}.err2_run_test.txt"
rm -f ${ERR1REP}
date > ${REPORT}
# find $1 -type f | head -$2 |
find $1 -type f -printf '%s\t\t%p\n' | sort -nr | awk -F'\t\t' '{print$2}' |
     ./tst ${ERR1REP} 2>${ERR2REP}
     tee -a ${REPORT}
date >> ${REPORT}
