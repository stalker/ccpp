#!/bin/bash
if [ "x$1" = "x" -o "x$2" = "x" ] ; then
  echo "Usage: $0 <dir> <Number>"
  exit 1
fi
if [ "${2}0" -ge 10000 ] ; then
  echo "Munber must be less than 1000!"
  exit 2
fi
if [ ! -d $1 ] ; then
  mkdir $1
fi
cd $1
for I in $(seq 1 $2);do V=$((($RANDOM % 100 ) * $I * 10000)); echo $V > file$I ;done
