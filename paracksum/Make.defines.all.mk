################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link

#LDDIR       = -L$(ROOT)/lib
#LDLIBS      = $(LDDIR) -lpthread $(EXTRALIBS)
#LIBAPUE     = $(ROOT)/lib/liba.a

# Common temp files to delete from each directory.
TEMPFILES   = core core.* *.o temp.* *.out

#EOF
