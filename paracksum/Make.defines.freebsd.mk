################################################################################
# Common make definitions, customized for each platform
#
# $Date$
# $Id$
# $Version: 1.0$
# $Revision: 1$
#
# Definitions required in all program directories to compile and link
# C programs using gcc and g++.

RANLIB      = ranlib
AR          = ar
AWK         = awk

CCSTD       = -std=c99
CXXSTD      =
CC          = gcc
LD          = g++ -o
CXX         = g++
CFLAGS      = -c $(CCSTD) -Wall
CXXFLAGS    = -c $(CCCSTD) -Wall
CFLAGS     += -DBSD -D__BSD_VISIBLE $(EXTRA)
CXXFLAGS   += -DBSD -D__BSD_VISIBLE $(EXTRA)
CFLAGS     += -I/usr/local/include
CXXFLAGS   += -I/usr/local/include
LDFLAGS    += -L/usr/local/lib

#EOF
