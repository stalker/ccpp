#!/bin/sh
cd src/lib && pwd &&
./autogen.sh &&
make &&
cd ../.. && pwd &&
mkdir -p build &&
cd build &&
cmake ../src && pwd &&
make
