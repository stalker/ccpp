#!/bin/sh
cd src/lib &&
./autogen.sh &&
make &&
cd ../.. &&
mkdir -p build &&
cd build &&
CC=/opt/freeware/bin/gcc cmake --check-system-vars ../src &&
make
