// main.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#include <tests.hpp>
#include "argmatch.h"
#include "tests/macros.h"

enum backup_type
{
  no_backups,
  simple_backups,
  numbered_existing_backups,
  numbered_backups
};

static const enum backup_type backup_vals[] =
{
  no_backups, no_backups, no_backups,
  simple_backups, simple_backups, simple_backups,
  numbered_existing_backups, numbered_existing_backups, numbered_existing_backups,
  numbered_backups, numbered_backups, numbered_backups
};

/* let's go */
int main(int argc, char * argv[]) {
    if (ARGMATCH ("cloexec", argv, backup_vals) == 1) {
      std::cout << "test_cloexec(): "
                << std::boolalpha
                << (test_cloexec() == 0)
                << std::endl;
    }
    if (ARGMATCH ("dup2", argv, backup_vals) == 1) {
        std::cout << "test_dup2(): "
                  << std::boolalpha
                  << (test_dup2() == 0)
                  << std::endl;
    }
    if (ARGMATCH ("dup_safer", argv, backup_vals) == 1) {
        std::cout << "test_dup_safer(): "
                  << std::boolalpha
                  << (test_dup_safer() == 0)
                  << std::endl;
        return 0;
    }
    std::cout << "test_accept(): "
              << std::boolalpha
              << (test_accept() == 0)
              << std::endl;
    std::cout << "test_accept_opt(): "
              << std::boolalpha
              << (test_alloca_opt() == 0)
              << std::endl;
    std::cout << "test_areadlink_with_size(): "
              << std::boolalpha
              << (test_areadlink_with_size() == 0)
              << std::endl;
    std::cout << "test_areadlinkat(): "
              << std::boolalpha << (test_areadlinkat() == 0)
              << std::endl;
    std::cout << "test_argmatch(argc, argv): "
              << std::boolalpha
              << (test_argmatch(argc, argv) == 0)
              << std::endl;
    if (ARGMATCH ("binary_io", argv, backup_vals) == 1) {
        std::cout << "test_binary_io(argc - 1, argv + 1): "
                  << std::boolalpha
                  << (test_binary_io(argc - 1, argv + 1) == 0)
                  << std::endl;
    }
    std::cout << "test_bind(): "
              << std::boolalpha
              << (test_bind() == 0)
              << std::endl;
    std::cout << "test_btowc(argc, argv): "
              << std::boolalpha
              << (test_btowc(argc, argv) == 0)
              << std::endl;
    std::cout << "test_byteswap(): "
              << std::boolalpha
              << (test_byteswap() == 0)
              << std::endl;
    std::cout << "test_c_ctype(): "
              << std::boolalpha
              << (test_c_ctype() == 0)
              << std::endl;
    std::cout << "test_c_strcasestr(): "
              << std::boolalpha
              << (test_c_strcasestr() == 0)
              << std::endl;
    std::cout << "test_c_strstr(): "
              << std::boolalpha
              << (test_c_strstr() == 0)
              << std::endl;
    std::cout << "test_canonicalize(): "
              << std::boolalpha
              << (test_canonicalize() == 0)
              << std::endl;
    std::cout << "test_chdir(): "
              << std::boolalpha
              << (test_chdir() == 0)
              << std::endl;
    std::cout << "test_close(): "
              << std::boolalpha
              << (test_close() == 0)
              << std::endl;
    if (ARGMATCH ("closein", argv, backup_vals) == 1) {
        std::cout << "test_closein(argc, argv): "
                  << std::boolalpha
                  << (test_closein(argc - 1, argv + 1) == 0)
                  << std::endl;
    }
    std::cout << "test_connect(): "
              << std::boolalpha
              << (test_connect() == 0)
              << std::endl;
    std::cout << "test_ctype(): "
              << std::boolalpha
              << (test_ctype() == 0)
              << std::endl;

    if (ARGMATCH ("dirent_safer", argv, backup_vals) == 1) {
        std::cout << "test_dirent_safer(): "
                  << std::boolalpha
                  << (test_dirent_safer() == 0)
                  << std::endl;
    }
    std::cout << "test_dirent(): "
              << std::boolalpha
              << (test_dirent() == 0)
              << std::endl;
    std::cout << "test_dirname(): "
              << std::boolalpha
              << (test_dirname() == 0)
              << std::endl;
    std::cout << "test_dup(): "
              << std::boolalpha
              << (test_dup() == 0)
              << std::endl;
    std::cout << "test_environ(): "
              << std::boolalpha
              << (test_environ() == 0)
              << std::endl;
    std::cout << "test_errno(): "
              << std::boolalpha
              << (test_errno() == 0)
              << std::endl;
    return EXIT_SUCCESS;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
