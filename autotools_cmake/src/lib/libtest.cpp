// libtest.cpp
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
extern "C" {
#include <alloca.h>
#include <assert.h>
#include <byteswap.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <locale.h>
#include <regex.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
// #include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <wchar.h>
#include "areadlink.h"
#include "c-strcasestr.h"
#include "c-strstr.h"
#include "canonicalize.h"
#include "cloexec.h"
#include "closein.h"
#include "closeout.h"
#include "dirent-safer.h"
#include "dirname.h"
#include "error.h"
#include "filemode.h"
#include "fnmatch.h"
#include "hash.h"
#include "human.h"
#include "idcache.h"
#include "mbswidth.h"
#include "modechange.h"
#include "mountlist.h"
#include "parse-datetime.h"
#include "pathmax.h"
#include "progname.h"
#include "quotearg.h"
#include "safe-read.h"
#include "save-cwd.h"
#include "savedir.h"
#include "stat-macros.h"
#include "stat-size.h"
#include "stat-time.h"
#include "stdio-safer.h"
#include "strftime.h"
#include "timespec.h"
#include "version-etc.h"
#include "xgetcwd.h"
#include "xstrtod.h"
#include "xstrtol.h"
#include "yesno.h"
}
#include <math.h>
#include "argmatch.h"
#include "fts_.h"
// #include "gettext.h"
#include "verify.h"
#include "xalloc.h"

int main() {
    return 0;
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
