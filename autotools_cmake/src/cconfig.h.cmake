/* config.h.in.  Generated from configure.ac by autoheader.  */

#ifndef _CMAKE_CONFIG_H
#define _CMAKE_CONFIG_H

/* Define to the number of bits in type 'ptrdiff_t'. */
#ifndef BITSIZEOF_PTRDIFF_T
#cmakedefine BITSIZEOF_PTRDIFF_T @BITSIZEOF_PTRDIFF_T@
#endif

/* Define to the number of bits in type 'sig_atomic_t'. */
#ifndef BITSIZEOF_SIG_ATOMIC_T
#cmakedefine BITSIZEOF_SIG_ATOMIC_T @BITSIZEOF_SIG_ATOMIC_T@
#endif

/* Define to the number of bits in type 'size_t'. */
#ifndef BITSIZEOF_SIZE_T
#cmakedefine BITSIZEOF_SIZE_T @BITSIZEOF_SIZE_T@
#endif

/* Define to the number of bits in type 'wchar_t'. */
#ifndef BITSIZEOF_WCHAR_T
#cmakedefine BITSIZEOF_WCHAR_T @BITSIZEOF_WCHAR_T@
#endif

/* Define to the number of bits in type 'wint_t'. */
#ifndef BITSIZEOF_WCHAR_T
#cmakedefine BITSIZEOF_WINT_T @BITSIZEOF_WINT_T@
#endif

/* Define to 1 if the system has the type 'long long int'. */
#ifndef HAVE_LONG_LONG_INT
#cmakedefine HAVE_LONG_LONG_INT @HAVE_LONG_LONG_INT@
#endif

/* Define to 1 if the system has the type 'unsigned long long int'. */
#ifndef HAVE_UNSIGNED_LONG_LONG_INT
#cmakedefine HAVE_UNSIGNED_LONG_LONG_INT @HAVE_UNSIGNED_LONG_LONG_INT@
#endif

/* Define to 1 if the system has the type `_Bool'. */
#ifndef HAVE__BOOL
#cmakedefine HAVE__BOOL @HAVE__BOOL@
#endif


/* ## Begin ### Header checks */

/* Define to 1 if you have <alloca.h> and it should be used (not on Ultrix).
   */
#ifndef HAVE_ALLOCA_H
#cmakedefine HAVE_ALLOCA_H @HAVE_ALLOCA_H@
#endif

/* Define to 1 if you have the <fcntl.h> header file. */
#ifndef HAVE_FCNTL_H
#cmakedefine HAVE_FCNTL_H @HAVE_FCNTL_H@
#endif

/* Define to 1 if you have the <dirent.h> header file. */
#ifndef HAVE_DIRENT_H
#cmakedefine HAVE_DIRENT_H @HAVE_DIRENT_H@
#endif

/* Define to 1 if you have the <dlfcn.h> header file. */
#ifndef HAVE_DLFCN_H
#cmakedefine HAVE_DLFCN_H @HAVE_DLFCN_H@
#endif

/* Define to 1 if you have the <inttypes.h> header file. */
#ifndef HAVE_INTTYPES_H
#cmakedefine HAVE_INTTYPES_H @HAVE_INTTYPES_H@
#endif

/* Define to 1 if you have the <mach-o/dyld.h> header file. */
#ifndef HAVE_MACH_O_DYLD_H
#cmakedefine HAVE_MACH_O_DYLD_H @HAVE_MACH_O_DYLD_H@
#endif

/* Define to 1 if you have the <memory.h> header file. */
#ifndef HAVE_MEMORY_H
#cmakedefine HAVE_MEMORY_H @HAVE_MEMORY_H@
#endif

/* Define to 1 if you have the <search.h> header file. */
#ifndef HAVE_SEARCH_H
#cmakedefine HAVE_SEARCH_H @HAVE_SEARCH_H@
#endif

/* Define to 1 if you have the <stddef.h> header file. */
#ifndef HAVE_STDDEF_H
#cmakedefine HAVE_STDDEF_H @HAVE_STDDEF_H@
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#ifndef HAVE_STDINT_H
#cmakedefine HAVE_STDINT_H @HAVE_STDINT_H@
#endif

/* Define to 1 if you have the <stdio.h> header file. */
#ifndef HAVE_STDIO_H
#cmakedefine HAVE_STDIO_H @HAVE_STDIO_H@
#endif

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef HAVE_STDLIB_H
#cmakedefine HAVE_STDLIB_H @HAVE_STDLIB_H@
#endif

/* Define to 1 if you have the <strings.h> header file. */
#ifndef HAVE_STRINGS_H
#cmakedefine HAVE_STRINGS_H @HAVE_STRINGS_H@
#endif

/* Define to 1 if you have the <string.h> header file. */
#ifndef HAVE_STRING_H
#cmakedefine HAVE_STRING_H @HAVE_STRING_H@
#endif

/* Define to 1 if you have the <sys/bitypes.h> header file. */
#ifndef HAVE_SYS_BITYPES_H
#cmakedefine HAVE_SYS_BITYPES_H @HAVE_SYS_BITYPES_H@
#endif

/* Define to 1 if you have the <sys/inttypes.h> header file. */
#ifndef HAVE_SYS_INTTYPES_H
#cmakedefine HAVE_SYS_INTTYPES_H @HAVE_SYS_INTTYPES_H@
#endif

/* Define to 1 if you have the <sys/param.h> header file. */
#ifndef HAVE_SYS_PARAM_H
#cmakedefine HAVE_SYS_PARAM_H @HAVE_SYS_PARAM_H@
#endif

/* Define to 1 if you have the <sys/socket.h> header file. */
#ifndef HAVE_SYS_SOCKET_H
#cmakedefine HAVE_SYS_SOCKET_H @HAVE_SYS_SOCKET_H@
#endif

/* Define to 1 if you have the <sys/stat.h> header file. */
#ifndef HAVE_SYS_STAT_H
#cmakedefine HAVE_SYS_STAT_H @HAVE_SYS_STAT_H@
#endif

/* Define to 1 if you have the <sys/time.h> header file. */
#ifndef HAVE_SYS_TIME_H
#cmakedefine HAVE_SYS_TIME_H @HAVE_SYS_TIME_H@
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#ifndef HAVE_SYS_TYPES_H
#cmakedefine HAVE_SYS_TYPES_H @HAVE_SYS_TYPES_H@
#endif

/* Define to 1 if you have the <sys/uio.h> header file. */
#ifndef HAVE_SYS_UIO_H
#cmakedefine HAVE_SYS_UIO_H @HAVE_SYS_UIO_H@
#endif

/* Define to 1 if you have the <sys/utsname.h> header file. */
#ifndef HAVE_SYS_UTSNAME_H
#cmakedefine HAVE_SYS_UTSNAME_H @HAVE_SYS_UTSNAME_H@
#endif

/* Define to 1 if you have the <sys/vfs.h> header file. */
#ifndef HAVE_SYS_VFS_H
#cmakedefine HAVE_SYS_VFS_H @HAVE_SYS_VFS_H@
#endif

/* Define to 1 if you have the <sys/wait.h> header file. */
#ifndef HAVE_SYS_WAIT_H
#cmakedefine HAVE_SYS_WAIT_H @HAVE_SYS_WAIT_H@
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#ifndef HAVE_UNISTD_H
#cmakedefine HAVE_UNISTD_H @HAVE_UNISTD_H@
#endif

/* Define to 1 if you have the <wchar.h> header file. */
#ifndef HAVE_WCHAR_H
#cmakedefine HAVE_WCHAR_H @HAVE_WCHAR_H@
#endif

/* Define to 1 if you have the <winsock2.h> header file. */
#ifndef HAVE_WINSOCK2_H
#cmakedefine HAVE_WINSOCK2_H @HAVE_WINSOCK2_H@
#endif

/* ## End ### Header checks */


/* Define to one of `_getb67', `GETB67', `getb67' for Cray-2 and Cray-YMP
   systems. This function is required for `alloca.c' support on those systems.
   */
#ifndef CRAY_STACKSEG_END
#cmakedefine CRAY_STACKSEG_END @CRAY_STACKSEG_END@
#endif

/* Define to l, ll, u, ul, ull, etc., as suitable for constants of type
   'ptrdiff_t'. */
#ifndef PTRDIFF_T_SUFFIX
#cmakedefine PTRDIFF_T_SUFFIX @PTRDIFF_T_SUFFIX@
#endif

/* Define to l, ll, u, ul, ull, etc., as suitable for constants of type
   'sig_atomic_t'. */
#ifndef SIG_ATOMIC_T_SUFFIX
#cmakedefine SIG_ATOMIC_T_SUFFIX @SIG_ATOMIC_T_SUFFIX@
#endif

/* Define to l, ll, u, ul, ull, etc., as suitable for constants of type
   'size_t'. */
#ifndef SIZE_T_SUFFIX
#cmakedefine SIZE_T_SUFFIX @SIZE_T_SUFFIX@
#endif

/* If using the C implementation of alloca, define if you know the
   direction of stack growth for your system; otherwise it will be
   automatically deduced at runtime.
	STACK_DIRECTION > 0 => grows toward higher addresses
	STACK_DIRECTION < 0 => grows toward lower addresses
	STACK_DIRECTION = 0 => direction of growth unknown */
#ifndef STACK_DIRECTION
#cmakedefine STACK_DIRECTION @STACK_DIRECTION@
#endif

/* Define to the prefix of C symbols at the assembler and linker level, either
   an underscore or empty. */
#ifndef USER_LABEL_PREFIX
#cmakedefine USER_LABEL_PREFIX @USER_LABEL_PREFIX@
#endif

/* Version number of package */
#ifndef VERSION
#cmakedefine VERSION "@VERSION@"
#endif

/* Define to l, ll, u, ul, ull, etc., as suitable for constants of type
   'wchar_t'. */
#ifndef WCHAR_T_SUFFIX
#cmakedefine WCHAR_T_SUFFIX @WCHAR_T_SUFFIX@
#endif

/* Define to l, ll, u, ul, ull, etc., as suitable for constants of type
   'wint_t'. */
#ifndef WINT_T_SUFFIX
#cmakedefine WINT_T_SUFFIX @WINT_T_SUFFIX@
#endif

/* The _Noreturn keyword of draft C1X.  */
#ifndef _Noreturn
# if (3 <= __GNUC__ || (__GNUC__ == 2 && 8 <= __GNUC_MINOR__) \
      || 0x5110 <= __SUNPRO_C)
#  define _Noreturn __attribute__ ((__noreturn__))
# elif 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn
# endif
#endif

/* Define to 2 if the system does not provide POSIX.1 features except with
   this defined. */
#cmakedefine _POSIX_1_SOURCE 2

/* Define to 1 if you need to in order for `stat' and other things to work. */
#cmakedefine _POSIX_SOURCE 1

/* Define to 500 only on HP-UX. */
#cmakedefine _XOPEN_SOURCE 500

/* Enable extensions on AIX 3, Interix.  */
#ifndef _ALL_SOURCE
# undef _ALL_SOURCE
#endif
/* Enable general extensions on MacOS X.  */
#ifndef _DARWIN_C_SOURCE
# undef _DARWIN_C_SOURCE
#endif
/* Enable GNU extensions on systems that have them.  */
#ifndef _GNU_SOURCE
# undef _GNU_SOURCE
#endif
/* Enable threading extensions on Solaris.  */
#ifndef _POSIX_PTHREAD_SEMANTICS
# undef _POSIX_PTHREAD_SEMANTICS
#endif
/* Enable extensions on HP NonStop.  */
#ifndef _TANDEM_SOURCE
# undef _TANDEM_SOURCE
#endif
/* Enable general extensions on Solaris.  */
#ifndef __EXTENSIONS__
# undef __EXTENSIONS__
#endif


/* Define to `int' if <sys/types.h> doesn't define. */
#cmakedefine gid_t @GID_T@

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
#undef inline
#endif

/* Work around a bug in Apple GCC 4.0.1 build 5465: In C99 mode, it supports
   the ISO C 99 semantics of 'extern inline' (unlike the GNU C semantics of
   earlier versions), but does not display it by setting __GNUC_STDC_INLINE__.
   __APPLE__ && __MACH__ test for MacOS X.
   __APPLE_CC__ tests for the Apple compiler and its version.
   __STDC_VERSION__ tests for the C99 mode.  */
#if defined __APPLE__ && defined __MACH__ && __APPLE_CC__ >= 5465 && !defined __cplusplus && __STDC_VERSION__ >= 199901L && !defined __GNUC_STDC_INLINE__
# define __GNUC_STDC_INLINE__ 1
#endif

/* Define to a type if <wchar.h> does not define. */
#cmakedefine mbstate_t @MBSTATE_T@

/* Define to the type of st_nlink in struct stat, or a supertype. */
#cmakedefine nlink_t @MBSTATE_T@

/* Define to `unsigned int' if <sys/types.h> does not define. */
#cmakedefine size_t @SIZE_T@

/* Define as a signed type of the same size as size_t. */
#cmakedefine ssize_t @SSIZE_T@

/* Define to `int' if <sys/types.h> doesn't define. */
#cmakedefine uid_t @UID_T@

/* Define to the equivalent of the C99 'restrict' keyword, or to
   nothing if this is not supported.  Do not define if restrict is
   supported directly.  */
#cmakedefine HAVE_C99_RESTRICT @HAVE_C99_RESTRICT@
#if !(defined __cplusplus || __DECCXX || __IBMCPP__ )
# ifndef HAVE_C99_RESTRICT
#  define restrict /* */
# endif
#endif
/* Work around a bug in Sun C++: it does not support _Restrict or
   __restrict__, even though the corresponding Sun C compiler ends up with
   "#define restrict _Restrict" or "#define restrict __restrict__" in the
   previous line.  Perhaps some future version of Sun C++ will work with
   restrict; if so, hopefully it defines __RESTRICT like Sun C does.  */
#if defined __SUNPRO_CC && !defined __RESTRICT
# define _Restrict
# define __restrict__
#endif

/* Define as a marker that can be attached to declarations that might not
    be used.  This helps to reduce warnings, such as from
    GCC -Wunused-parameter.  */
#if __GNUC__ >= 3 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)
# define _GL_UNUSED __attribute__ ((__unused__))
#else
# define _GL_UNUSED
#endif
/* The name _UNUSED_PARAMETER_ is an earlier spelling, although the name
   is a misnomer outside of parameter lists.  */
#define _UNUSED_PARAMETER_ _GL_UNUSED

/* The __pure__ attribute was added in gcc 2.96.  */
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96)
# define _GL_ATTRIBUTE_PURE __attribute__ ((__pure__))
#else
# define _GL_ATTRIBUTE_PURE /* empty */
#endif

/* The __const__ attribute was added in gcc 2.95.  */
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
# define _GL_ATTRIBUTE_CONST __attribute__ ((__const__))
#else
# define _GL_ATTRIBUTE_CONST /* empty */
#endif



/* On Windows, variables that may be in a DLL must be marked specially.  */
#if defined _MSC_VER && defined _DLL
# define DLL_VARIABLE __declspec (dllimport)
#else
# define DLL_VARIABLE
#endif

/* my adds */
/******************************************************************************/

/* To check the size of a primitive type: */
#cmakedefine SIZEOF_INT @SIZEOF_INT@

/* Define to 1 if your system has a working `getgroups' function. */

#cmakedefine HAVE_SYM_GETGROUPS @HAVE_SYM_GETGROUPS@

#if defined(HAVE_RAW_DECL_GETGROUPS) || defined(HAVE_SYM_GETGROUPS)
# define HAVE_GETGROUPS 1
#else
# define HAVE_GETGROUPS 0
#endif

/* Define to the type that is the result of default argument promotions of
   type mode_t. */
#ifndef PROMOTED_MODE_T
#cmakedefine SIZEOF_MODE_T @SIZEOF_MODE_T@
#if defined(SIZEOF_MODE_T) && defined(SIZEOF_INT) && SIZEOF_INT <= SIZEOF_MODE_T
# define PROMOTED_MODE_T mode_t
#else
# define PROMOTED_MODE_T int
#endif
#endif

/* Define to the type of elements in the array set by `getgroups'. Usually
   this is either `int' or `gid_t'. */
#ifndef GETGROUPS_T
#cmakedefine GETGROUPS_T @GETGROUPS_T@
#ifndef GETGROUPS_T
# define GETGROUPS_T int
#endif
#endif

/* Define to 1 if you have the `closedir' function. */
#ifndef HAVE_CLOSEDIR
#cmakedefine HAVE_CLOSEDIR @HAVE_CLOSEDIR@
#endif

/* Define to nothing if C supports flexible array members, and to 1 if it does
   not. That way, with a declaration like 'struct s { int n; double
   d[FLEXIBLE_ARRAY_MEMBER]; };', the struct hack can be used with pre-C99
   compilers. When computing the size of such an object, don't use 'sizeof
   (struct s)' as it overestimates the size. Use 'offsetof (struct s, d)'
   instead. Don't use 'offsetof (struct s, d[0])', as this doesn't work with
   MSVC and with C++ compilers. */
#ifndef FLEXIBLE_ARRAY_MEMBER
#cmakedefine HAVE_FLEXIBLE_ARRAY_MEMBER @HAVE_FLEXIBLE_ARRAY_MEMBER@
#ifdef HAVE_FLEXIBLE_ARRAY_MEMBER
# define FLEXIBLE_ARRAY_MEMBER 0
#else
# define FLEXIBLE_ARRAY_MEMBER 1
#endif
#endif

/* Define to a C preprocessor expression that evaluates to 1 or 0, depending
   whether the gnulib module dirname shall be considered present. */
#define GNULIB_DIRNAME 1

/* Define to the name of the strftime replacement function. */
#define my_strftime nstrftime

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
#  undef WORDS_BIGENDIAN
# endif
#endif

#if ! (defined _Noreturn \
       || (defined __STDC_VERSION__ && 201112 <= __STDC_VERSION__))
# if (3 <= __GNUC__ || (__GNUC__ == 2 && 8 <= __GNUC_MINOR__) \
      || 0x5110 <= __SUNPRO_C)
#  define _Noreturn __attribute__ ((__noreturn__))
# elif defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn
# endif
#endif

/* Please see the Gnulib manual for how to use these macros.

   Suppress extern inline with HP-UX cc, as it appears to be broken; see
   <http://lists.gnu.org/archive/html/bug-texinfo/2013-02/msg00030.html>.

   Suppress extern inline with Sun C in standards-conformance mode, as it
   mishandles inline functions that call each other.  E.g., for 'inline void f
   (void) { } inline void g (void) { f (); }', c99 incorrectly complains
   'reference to static identifier "f" in extern inline function'.
   This bug was observed with Sun C 5.12 SunOS_i386 2011/11/16.

   Suppress extern inline (with or without __attribute__ ((__gnu_inline__)))
   on configurations that mistakenly use 'static inline' to implement
   functions or macros in standard C headers like <ctype.h>.  For example,
   if isdigit is mistakenly implemented via a static inline function,
   a program containing an extern inline function that calls isdigit
   may not work since the C standard prohibits extern inline functions
   from calling static functions.  This bug is known to occur on:

     OS X 10.8 and earlier; see:
     http://lists.gnu.org/archive/html/bug-gnulib/2012-12/msg00023.html

     DragonFly; see
     http://muscles.dragonflybsd.org/bulk/bleeding-edge-potential/latest-per-pkg/ah-tty-0.3.12.log

     FreeBSD; see:
     http://lists.gnu.org/archive/html/bug-gnulib/2014-07/msg00104.html

   OS X 10.9 has a macro __header_inline indicating the bug is fixed for C and
   for clang but remains for g++; see <http://trac.macports.org/ticket/41033>.
   Assume DragonFly and FreeBSD will be similar.  */
#if (((defined __APPLE__ && defined __MACH__) \
      || defined __DragonFly__ || defined __FreeBSD__) \
     && (defined __header_inline \
         ? (defined __cplusplus && defined __GNUC_STDC_INLINE__ \
            && ! defined __clang__) \
         : ((! defined _DONT_USE_CTYPE_INLINE_ \
             && (defined __GNUC__ || defined __cplusplus)) \
            || (defined _FORTIFY_SOURCE && 0 < _FORTIFY_SOURCE \
                && defined __GNUC__ && ! defined __cplusplus))))
# define _GL_EXTERN_INLINE_STDHEADER_BUG
#endif
#if ((__GNUC__ \
      ? defined __GNUC_STDC_INLINE__ && __GNUC_STDC_INLINE__ \
      : (199901L <= __STDC_VERSION__ \
         && !defined __HP_cc \
         && !(defined __SUNPRO_C && __STDC__))) \
     && !defined _GL_EXTERN_INLINE_STDHEADER_BUG)
# define _GL_INLINE inline
# define _GL_EXTERN_INLINE extern inline
# define _GL_EXTERN_INLINE_IN_USE
#elif (2 < __GNUC__ + (7 <= __GNUC_MINOR__) && !defined __STRICT_ANSI__ \
       && !defined _GL_EXTERN_INLINE_STDHEADER_BUG)
# if defined __GNUC_GNU_INLINE__ && __GNUC_GNU_INLINE__
   /* __gnu_inline__ suppresses a GCC 4.2 diagnostic.  */
#  define _GL_INLINE extern inline __attribute__ ((__gnu_inline__))
# else
#  define _GL_INLINE extern inline
# endif
# define _GL_EXTERN_INLINE extern
# define _GL_EXTERN_INLINE_IN_USE
#else
# define _GL_INLINE static _GL_UNUSED
# define _GL_EXTERN_INLINE static _GL_UNUSED
#endif

#if 4 < __GNUC__ + (6 <= __GNUC_MINOR__)
# if defined __GNUC_STDC_INLINE__ && __GNUC_STDC_INLINE__
#  define _GL_INLINE_HEADER_CONST_PRAGMA
# else
#  define _GL_INLINE_HEADER_CONST_PRAGMA \
     _Pragma ("GCC diagnostic ignored \"-Wsuggest-attribute=const\"")
# endif
  /* Suppress GCC's bogus "no previous prototype for 'FOO'"
     and "no previous declaration for 'FOO'"  diagnostics,
     when FOO is an inline function in the header; see
     <http://gcc.gnu.org/bugzilla/show_bug.cgi?id=54113>.  */
# define _GL_INLINE_HEADER_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wmissing-prototypes\"") \
    _Pragma ("GCC diagnostic ignored \"-Wmissing-declarations\"") \
    _GL_INLINE_HEADER_CONST_PRAGMA
# define _GL_INLINE_HEADER_END \
    _Pragma ("GCC diagnostic pop")
#else
# define _GL_INLINE_HEADER_BEGIN
# define _GL_INLINE_HEADER_END
#endif

/* Work around a bug in Apple GCC 4.0.1 build 5465: In C99 mode, it supports
   the ISO C 99 semantics of 'extern inline' (unlike the GNU C semantics of
   earlier versions), but does not display it by setting __GNUC_STDC_INLINE__.
   __APPLE__ && __MACH__ test for Mac OS X.
   __APPLE_CC__ tests for the Apple compiler and its version.
   __STDC_VERSION__ tests for the C99 mode.  */
#if defined __APPLE__ && defined __MACH__ && __APPLE_CC__ >= 5465 && !defined __cplusplus && __STDC_VERSION__ >= 199901L && !defined __GNUC_STDC_INLINE__
# define __GNUC_STDC_INLINE__ 1
#endif

/* Define as a marker that can be attached to declarations that might not
    be used.  This helps to reduce warnings, such as from
    GCC -Wunused-parameter.  */
#if __GNUC__ >= 3 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)
# define _GL_UNUSED __attribute__ ((__unused__))
#else
# define _GL_UNUSED
#endif
/* The name _UNUSED_PARAMETER_ is an earlier spelling, although the name
   is a misnomer outside of parameter lists.  */
#define _UNUSED_PARAMETER_ _GL_UNUSED

/* gcc supports the "unused" attribute on possibly unused labels, and
   g++ has since version 4.5.  Note to support C++ as well as C,
   _GL_UNUSED_LABEL should be used with a trailing ;  */
#if !defined __cplusplus || __GNUC__ > 4 \
    || (__GNUC__ == 4 && __GNUC_MINOR__ >= 5)
# define _GL_UNUSED_LABEL _GL_UNUSED
#else
# define _GL_UNUSED_LABEL
#endif

/* The __pure__ attribute was added in gcc 2.96.  */
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96)
# define _GL_ATTRIBUTE_PURE __attribute__ ((__pure__))
#else
# define _GL_ATTRIBUTE_PURE /* empty */
#endif

/* The __const__ attribute was added in gcc 2.95.  */
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
# define _GL_ATTRIBUTE_CONST __attribute__ ((__const__))
#else
# define _GL_ATTRIBUTE_CONST /* empty */
#endif

#if defined(__STDC__)
# define PREDEF_STANDARD_C89
# if defined(__STDC_VERSION__)
#  define PREDEF_STANDARD_C90
#  if (__STDC_VERSION__ >= 199409L)
#   define PREDEF_STANDARD_C94
#  endif
#  if (__STDC_VERSION__ >= 199901L)
#   define PREDEF_STANDARD_C99
#  endif
# endif
#endif

#if defined(_XOPEN_VERSION)
# if (_XOPEN_VERSION >= 3)
#  define PREDEF_STANDARD_XPG3
# endif
# if (_XOPEN_VERSION >= 4)
#  define PREDEF_STANDARD_XPG4
# endif
# if (_XOPEN_VERSION >= 4) && defined(_XOPEN_SOURCE_EXTENDED)
#  define PREDEF_STANDARD_UNIX95
# endif
# if (_XOPEN_VERSION >= 500)
#  define PREDEF_STANDARD_UNIX98
# endif
# if (_XOPEN_VERSION >= 600)
#  define PREDEF_STANDARD_UNIX01
# endif
#endif

#endif
