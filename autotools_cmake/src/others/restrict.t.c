typedef int * int_ptr;
int foo(int_ptr restrict ip) { return ip[0]; }
int main( ) { int s[1]; int * restrict t = s; t[0] = 0; return foo(t); }
