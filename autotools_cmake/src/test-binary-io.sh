#!/bin/sh

tmpfiles=""
trap 'rm -fr $tmpfiles' 1 2 3 15

tmpfiles="$tmpfiles t-bin-out0.tmp t-bin-out1.tmp t-bin-out2.tmp"
./tst binary_io 1 > t-bin-out1.tmp || exit 1
cmp t-bin-out0.tmp t-bin-out1.tmp > /dev/null || exit 1
./tst binary_io 2 > t-bin-out2.tmp || exit 1
cmp t-bin-out0.tmp t-bin-out2.tmp > /dev/null || exit 1

rm -fr $tmpfiles

exit 0
