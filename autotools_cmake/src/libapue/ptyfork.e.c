#line 45 "/usr/include/stdint.h"
typedef signed char		int8_t;
typedef signed short		int16_t;
typedef signed int		int32_t;
#line 52
typedef	signed long long	int64_t;
#line 56
typedef unsigned char		uint8_t;
typedef unsigned short		uint16_t;
typedef unsigned int		uint32_t;
#line 64
typedef unsigned long long	uint64_t;
#line 74
typedef int64_t			intmax_t;
typedef uint64_t		uintmax_t;
#line 86
typedef signed long		intptr_t;
typedef unsigned long		uintptr_t;
#line 93
typedef signed char		int_least8_t;
typedef signed short		int_least16_t;
typedef signed int		int_least32_t;
#line 100
typedef signed long long	int_least64_t;
#line 104
typedef unsigned char		uint_least8_t;
typedef unsigned short		uint_least16_t;
typedef unsigned int		uint_least32_t;
#line 111
typedef unsigned long long	uint_least64_t;
#line 121
typedef signed char	int_fast8_t;
typedef int16_t		int_fast16_t;
typedef int32_t		int_fast32_t;
typedef unsigned char	uint_fast8_t;
typedef uint16_t	uint_fast16_t;
typedef uint32_t	uint_fast32_t;

typedef int64_t		int_fast64_t;
typedef uint64_t	uint_fast64_t;
#line 112 "/usr/include/sys/inttypes.h"
typedef int32_t         intfast_t;
typedef uint32_t        uintfast_t;
#line 563
typedef signed long	__long32_t;
typedef unsigned long	__ulong32_t;
#line 577
typedef signed int	__long64_t;
typedef unsigned int	__ulong64_t;
#line 590
typedef signed int	int32long64_t;
typedef unsigned int	uint32long64_t;
#line 603
typedef signed long	long32int64_t;
typedef unsigned long	ulong32int64_t;
#line 88 "/usr/include/sys/types.h"
typedef long		ptrdiff_t;
#line 96
typedef unsigned short	wchar_t;
#line 102
typedef unsigned int	wctype_t;
#line 111
typedef long	fpos_t;
#line 128
typedef int		time_t;
#line 135
typedef int		clock_t;
#line 140
typedef	unsigned long	size_t;
#line 151
typedef	unsigned char	uchar_t;
#line 154
typedef	unsigned short	ushort_t;
typedef	unsigned int	uint_t;
typedef unsigned long	ulong_t;
#line 160
typedef	signed long	ssize_t;
#line 166
typedef int		level_t;
typedef	__long64_t	daddr_t;	 
typedef	int		daddr32_t;	 

typedef	int64_t		daddr64_t;	 

typedef	char *		caddr_t;	 
typedef	__ulong64_t	ino_t;		 
typedef	uint_t 		ino32_t;	 

typedef	uint64_t 	ino64_t;	 

typedef short		cnt_t;
#line 183
typedef	__ulong64_t	dev_t;		 

typedef uint_t 		dev32_t;	 

typedef uint64_t 	dev64_t;	 

typedef	int		chan_t;		 
typedef int		time32_t;		 
typedef int		pid32_t;		 
typedef int		tid32_t;		 

typedef uint64_t     pid64_t;      
typedef uint64_t     tid64_t;      

typedef int64_t      time64_t;     
#line 208
typedef void * __ptr32;
typedef char * __cptr32;
#line 212
typedef int soff_t;			 
#line 219
typedef long		off_t;		 
#line 227
typedef	long		paddr_t;
#line 232
typedef	int32long64_t	key_t;
#line 237
typedef __long64_t	timer_t;	 

typedef int		timer32_t;	 

typedef int64_t 	timer64_t;	 

typedef	short		nlink_t;
#line 248
typedef	uint_t		mode_t;		 
#line 254
typedef uint_t		uid_t;		 
typedef uint_t		gid_t;		 
typedef uint_t		rid_t;		 
#line 259
typedef ushort_t        authnum_t;       
typedef	__ptr32 	mid_t;		 
#line 264
typedef	int32long64_t	pid_t;		 
#line 267
typedef __long64_t	tid_t;		 
typedef char		slab_t[12];	 
typedef long		mtyp_t;		 
typedef int             boolean_t;
typedef int     	crid_t;
#line 277
typedef __long64_t	blkcnt_t;  
#line 283
typedef __long64_t	blksize_t;  
#line 288
typedef int		blkcnt32_t;  
typedef int		blksize32_t;  

typedef uint64_t	blkcnt64_t;  
typedef uint64_t	blksize64_t;  
#line 296
typedef ulong_t 	fsblkcnt_t;	 
typedef ulong_t		fsfilcnt_t;	 
#line 301
	typedef	int		wint_t;		 
#line 305
typedef uint32long64_t	id_t;		 
typedef unsigned int	useconds_t;	 
typedef signed   int	suseconds_t;	 
#line 311
typedef long long       clockid_t;       
#line 320
typedef struct sigset_t	{
#line 332
	unsigned int __losigs;
	unsigned int __hisigs;
#line 336
} sigset_t;

typedef struct {
#line 342
	unsigned int __losigs, __hisigs;

} sigset32_t;
#line 358
typedef int signal_t;
#line 363
typedef struct fsid_t {
#line 370
	unsigned int __val[2];
#line 373
} fsid_t;
#line 381
typedef struct fsid64_t {
#line 385
	uint32_t __val[4];

} fsid64_t;

typedef void *pthread_attr_t;
typedef	void *pthread_condattr_t;	
typedef	void *pthread_mutexattr_t;
#line 394
typedef	void *pthread_rwlockattr_t;
#line 398
typedef	void *pthread_barrierattr_t;
#line 401
typedef unsigned int pthread_t;
typedef unsigned int pthread_key_t;

typedef struct
{
#line 409
	int	__mt_word[13];

}
pthread_mutex_t;

typedef struct
{
#line 419
	int	__cv_word[11];

}
pthread_cond_t;

typedef struct
{
#line 429
        int     __on_word[28];

}
pthread_once_t;
#line 435
typedef struct
{
#line 440
	int	__sp_word[6];

}
pthread_spinlock_t;

typedef struct
{
#line 450
	int	__br_word[8];

}
pthread_barrier_t;
#line 457
typedef struct
{
#line 462
	int	__rw_word[52];

}
pthread_rwlock_t;
#line 469
typedef void *trace_attr_t;
typedef unsigned trace_event_id_t;
typedef unsigned trace_id_t;

typedef uint64_t trace_event_set_t;
#line 738
typedef unsigned long long size64_t;    
#line 69 "/usr/include/sys/stat.h"
struct timespec {
	time_t	tv_sec;		 
	long	tv_nsec;	 
};
#line 76
typedef struct st_timespec {
	time_t	tv_sec;		 
	int	tv_nsec;	 
} st_timespec_t;
#line 84
struct	stat 
{
	dev_t	st_dev;		
#line 91
	ino_t	st_ino;		 
#line 96
	mode_t	st_mode;	 

	nlink_t st_nlink;	 
	ushort_t st_flag;	 
#line 106
	uid_t	st_uid;		 
	gid_t	st_gid;		 
#line 110
	dev_t	st_rdev;       	
#line 114
	off_t	st_size;	  	
#line 119
	st_timespec_t st_atim;	 
	st_timespec_t st_mtim;	 
	st_timespec_t st_ctim;	 
#line 130
	blksize_t st_blksize;	 
	blkcnt_t st_blocks;	
#line 134
	int	st_vfstype;	 
	uint_t	st_vfs;		 
	uint_t	st_type;	 
	uint_t	st_gen;		 
#line 140
	uint_t	st_reserved[9];
#line 145
};
#line 359
	extern mode_t	umask(mode_t); 
#line 407
	extern int	mkdir(const char *, mode_t); 

	extern int	mkdirat(int, const char *, mode_t); 

	extern int	stat(const char *restrict, struct stat *restrict);
	extern int	fstat(int, struct stat *);

	extern int  fstatat(int, const char *restrict, struct stat *restrict, int);
#line 424
	extern int	chmod(const char *, mode_t);

	extern int	fchmodat(int, const char *, mode_t, int);
#line 430
	extern int	fchmod(int, mode_t);
	extern int	lstat(const char *restrict, struct stat *restrict);
#line 436
	extern int	mknod(const char *, mode_t, dev_t);

	extern int	mknodat(int, const char *, mode_t, dev_t);
#line 445
	extern int	mkfifo(const char *, mode_t);

	extern int	mkfifoat(int, const char *, mode_t);
	extern int	futimens(int, const struct timespec *);
	extern int	utimensat(int, const char *, const struct timespec *,
				int);
#line 57 "/usr/include/termios.h"
typedef unsigned int	tcflag_t;

typedef unsigned char   cc_t;
#line 62
typedef unsigned int	speed_t;
#line 68
struct termios {
	tcflag_t	c_iflag;	 
	tcflag_t	c_oflag;	 
	tcflag_t	c_cflag;	 
	tcflag_t	c_lflag;	 
	cc_t		c_cc[16];	 
};
#line 100
extern int tcgetattr(int, struct termios *);

extern pid_t tcgetsid(int);

extern int tcsetattr(int, int, const struct termios *);
extern int tcsendbreak(int, int);
extern int tcdrain(int);
extern int tcflush(int, int);
extern int tcflow(int, int);
extern speed_t cfgetospeed(const struct termios *);
extern speed_t cfgetispeed(const struct termios *);
extern int cfsetospeed(struct termios *, speed_t);
extern int cfsetispeed(struct termios *, speed_t);
#line 54 "/usr/include/sys/ttychars.h"
struct ttychars {
	char	tc_erase;	 
	char	tc_kill;	 
	char	tc_intrc;	 
	char	tc_quitc;	 
	char	tc_startc;	 
	char	tc_stopc;	 
	char	tc_eofc;	 
	char	tc_brkc;	 
	char	tc_suspc;	 
	char	tc_dsuspc;	 
	char	tc_rprntc;	 
	char	tc_flushc;	 
	char	tc_werasc;	 
	char	tc_lnextc;	 
};
#line 105 "/usr/include/sys/ioctl.h"
struct tchars {
	char	t_intrc;	 
	char	t_quitc;	 
	char	t_startc;	 
	char	t_stopc;	 
	char	t_eofc;		 
	char	t_brkc;		 
};
struct ltchars {
	char	t_suspc;	 
	char	t_dsuspc;	 
	char	t_rprntc;	 
	char	t_flushc;	 
	char	t_werasc;	 
	char	t_lnextc;	 
};
#line 126
struct sgttyb {
	char	sg_ispeed;		 
	char	sg_ospeed;		 
	char	sg_erase;		 
	char	sg_kill;		 
	short	sg_flags;		 
};
#line 137
struct ttysize {
	unsigned short	ts_lines;
	unsigned short	ts_cols;
	unsigned short	ts_xxx;
	unsigned short	ts_yyy;
};
#line 187
typedef struct wpar_ckpt_op {
	int	op;	 
	int	opt;	 

} wpar_ckpt_op_t; 

typedef struct wpar_ckpt_resp {
	int		opcnt;
	wpar_ckpt_op_t	op[5];
} wpar_ckpt_resp_t;
#line 544
int stty(int fd, struct sgttyb *sg);
int gtty(int fd, struct sgttyb *sg);
#line 167 "/usr/include/stdio.h"
typedef struct {
	unsigned char	*_ptr;
	int	_cnt;
	unsigned char	*_base;
	unsigned char   *_bufendp;
	short	_flag;
	short	_file;
	int	__stdioid;
	char	*__newbase;
	void	*_lock;
} FILE;
#line 183
extern FILE	_iob[16];
#line 196
extern size_t 		fread(void *restrict, size_t, size_t, FILE *restrict);
extern size_t		fwrite(const void *restrict, size_t, size_t,FILE *restrict);
#line 62 "/usr/include/va_list.h"
typedef	char *va_list;
#line 207 "/usr/include/stdio.h"
extern int  vscanf(const char *restrict, va_list);
extern int  vfscanf(FILE *restrict, const char *restrict, va_list);
extern int  vsscanf(const char *restrict, const char *restrict, va_list);
#line 279
extern int	__flsbuf(unsigned char, FILE *);
extern int	__filbuf(FILE *);
extern int 	ferror(FILE *);
extern int 	feof(FILE *);
extern void 	clearerr(FILE *);
extern int 	putchar(int);
extern int 	getchar(void);
extern int 	putc(int, FILE *);
extern int 	getc(FILE *);
extern int	remove(const char *);
extern int	rename(const char *, const char *);

extern int	renameat(int, const char *, int, const char *);

extern FILE 	*tmpfile(void);
extern char 	*tmpnam(char *);
extern int 	fclose(FILE *);
extern int 	fflush(FILE *);
extern FILE *	fopen(const char *restrict, const char *restrict);
extern FILE *	freopen(const char *restrict, const char *restrict, FILE *restrict);
#line 304
extern FILE *	fmemopen(void *restrict, size_t, const char *restrict);
extern FILE *	open_memstream(char **, size_t *);

extern void 	setbuf(FILE *restrict, char *restrict);
extern int 	setvbuf(FILE *restrict, char *restrict, int, size_t);
extern int	fprintf(FILE *restrict, const char *restrict, ...); 
extern int	fscanf(FILE *restrict, const char *restrict, ...);

extern int      dprintf(int, const char * restrict, ...);

extern int	printf(const char *restrict, ...); 
extern int	scanf(const char *restrict, ...); 
extern int	sprintf(char *restrict, const char *restrict, ...); 

extern int	snprintf(char *restrict, size_t, const char *restrict, ...);

extern int	sscanf(const char *restrict, const char *restrict, ...); 
#line 323
extern int	vfprintf(FILE *restrict, const char *restrict, va_list);
extern int	vprintf(const char *restrict, va_list); 
extern int	vsprintf(char *restrict, const char *restrict, va_list);

extern int	vsnprintf(char *restrict, size_t, const char *restrict, va_list);
#line 330
extern int vdprintf(int, const char *restrict, va_list);
#line 346
extern int 	fgetc(FILE *);
extern char *	fgets(char *restrict, int, FILE *restrict);
extern int 	fputc(int, FILE *);
extern int 	fputs(const char *restrict, FILE *restrict);
extern char 	*gets(char *);
extern int 	puts(const char *);
extern int	ungetc(int, FILE *);
extern int	fgetpos(FILE *restrict, fpos_t *restrict);
extern int 	fseek(FILE *, long int, int);
extern int	fsetpos(FILE *, const fpos_t *);
extern long	ftell(FILE *);
extern void	rewind(FILE *);
extern void 	perror(const char *); 
#line 364
extern int	getc_unlocked(FILE *);
extern int	getchar_unlocked(void);
extern int	putc_unlocked(int, FILE *);
extern int	putchar_unlocked(int);

extern ssize_t	getdelim(char **, size_t *, int, FILE *);
extern ssize_t	getline(char **, size_t *, FILE *);
#line 492
extern int 	fileno(FILE *);
extern FILE 	*fdopen(int,const char *);
extern char	*ctermid(char *);
extern FILE 	*popen(const char *, const char *);
extern int 	pclose(FILE *);
#line 499
extern void	flockfile(FILE *);
extern void	funlockfile(FILE *);
extern int	fseeko(FILE *, off_t, int);
extern off_t	ftello(FILE *);
extern int	ftrylockfile(FILE *);
extern void	funlockfile(FILE *);
#line 544
extern char 	*tempnam(const char*, const char*);
#line 20 "/usr/vac/include/stdlib.h"
# pragma info(none)
#line 105 "/usr/include/stdlib.h"
typedef struct div_t  {	         
	int quot;                
	int rem;                 
} div_t;

typedef struct ldiv_t  {	 
	long int quot;           
	long int rem;            
} ldiv_t;
#line 120
extern size_t __getmbcurmax (void);
extern int __getmaxdispwidth (void);
#line 131
        extern double   strtod(const char * restrict, char ** restrict);        extern long int strtol(const char * restrict, char ** restrict,
int);
        extern unsigned long int strtoul(const char * restrict, char ** restrict, int);
#line 144
	extern void     	_Exit(int);
	extern float    	strtof(const char * restrict, char ** restrict);
#line 153
typedef struct lldiv_t {
     long long int quot;  
     long long int rem ;  
} lldiv_t;
#line 166
extern long long int    atoll(const char *);
extern long long int llabs( long long int );
extern lldiv_t lldiv( long long int, long long int ); 

extern long long int strtoll(
     const char * restrict,  
     char ** restrict,       
     int );                      
extern unsigned long long int strtoull(
     const char * restrict,  
     char ** restrict,       
     int );                      
#line 187
static long double
strtold(const char * restrict __a, char ** restrict __b) {
	return ((long double)strtod (__a, __b));
}
#line 206
	extern int	 	mblen(const char *, size_t);
	extern size_t 	mbstowcs(wchar_t * restrict, const char * restrict, size_t);
	extern int		mbtowc(wchar_t * restrict, const char * restrict, size_t);
	extern size_t	wcstombs(char * restrict, const wchar_t * restrict, size_t);
	extern int		wctomb(char *, const wchar_t);
#line 266
	extern double 	atof(const char *);
	extern int 	atoi(const char *);
	extern long int atol(const char *);
	extern int 	rand(void);
	extern void	srand(unsigned int);
	extern void 	*calloc(size_t, size_t);
	extern void	free(void *);
	extern void	*malloc(size_t);
	extern void 	*realloc(void *, size_t);
	extern void	abort(void);
	extern int	atexit(void (*)(void));
	extern void	exit(int);
	extern char	*getenv(const char *);
	extern int 	system(const char *);
	extern void 	*bsearch(const void *, const void *, size_t, size_t, int(*)(const void *,const void *));
	extern void 	qsort(void *, size_t, size_t, int(*)(const void *,const void *));
#line 296
	extern int 	abs(int);
	extern struct div_t	div(int, int);
	extern long int	labs(long int);
	extern struct ldiv_t 	ldiv(long int, long int);
#line 320
extern int posix_memalign(void **,size_t,size_t);
#line 86 "/usr/include/sys/signal.h"
extern void (*signal(int, void (*)(int)))(int);
#line 94
extern int raise(int);
#line 97
typedef volatile int sig_atomic_t;  
#line 231
union sigval
{
#line 236
        void *  sival_ptr;	 

        int     sival_int;	 
};
#line 252
typedef struct {
	int si_signo;		 
	int si_errno;		 
	int si_code;		 
#line 266
	pid_t si_pid;		 
#line 269
	uid_t si_uid;		 
#line 293
	void *si_addr;		 
	int si_status;		 
#line 297
	long si_band;		 
#line 300
        union sigval si_value;	 
#line 305
        int __si_flags;
#line 309
        int __pad[6];		 
#line 313
} siginfo_t;
#line 321
struct sigaction {
   union {
#line 329
	void	(*__su_handler)(int);	 
#line 333
	void    (*__su_sigaction) (int, siginfo_t *, void *);
#line 337
    } sa_union;
	sigset_t sa_mask;		 
	int	sa_flags;		 
};
#line 38 "/usr/include/sys/vm_types.h"
typedef long            vmid_t;          
typedef ulong_t		vmhandle_t;	 
#line 45
typedef int             vmid32_t;        
typedef uint_t		vmhandle32_t;	 
#line 54
typedef long32int64_t	kvmid_t;	 
typedef ulong32int64_t	kvmhandle_t;	 
#line 65
typedef long long	vmid64_t;
typedef long long	rpn64_t;
typedef long long       cnt64_t;         
typedef long long       psize_t;         
#line 76
typedef int32long64_t	vmidx_t;	 

typedef uint32long64_t  vmfkey_t;        

typedef uint32long64_t  vmprkey_t;       
typedef int32long64_t	vmkey_t;	 
typedef int32long64_t	vmhwkey_t;       
typedef int             vmhkey_t;        
typedef int32long64_t	vpn_t;		 
typedef int32long64_t	rpn_t;		 

typedef unsigned long   ptex_t;          
typedef unsigned long   swhatx_t;        

typedef uint32long64_t  esid_t;          
#line 95
typedef ushort_t	aptx_t;		 
#line 98
typedef int             pdtx_t;          
typedef short           psx_t;           
typedef ushort_t        pshift_t;        
typedef ushort_t        sshift_t;        
#line 106
typedef uint_t		vm_mpss_t;

typedef int             unidx_t;         
typedef int             snidx_t;         
typedef int             vmnodeidx_t;     

typedef int		kvpn_t;		 

   typedef int		krpn_t;		 

typedef int32long64_t   vmsize_t;	 

typedef int32long64_t   vmm_lock_t;      

typedef uint_t          amef_t;          
typedef int             vmcbpx_t;        

typedef uint32_t	cnt32_t;	 

typedef unsigned long ureg_t;		 
#line 144
typedef struct
#line 148
{		 
#line 153
	vmhandle_t	__srval;	 
	caddr_t		__offset;	 

} vmaddr_t;
#line 160
typedef struct
#line 164
{		 
#line 169
	ulong32int64_t	__alloc;	 
	vmhandle_t	__srval[16];	 

} adspace_t;
#line 263
typedef vmhandle_t      vmlpghandle_t;     
#line 48 "/usr/include/sys/m_types.h"
typedef struct label_t
{					 
    long __opaque[25];
} label_t;

typedef int32long64_t	ext_t;		  
#line 49 "/usr/include/sys/mstsave.h"
struct
#line 53
__mstsave

{
#line 95
	struct __mstsave *__prev;	 
	label_t		*__kjmpbuf;	 
	char		*__stackfix;	 
	char		__intpri; 	 
	char		__backt;	 
	char		__rsvd[2];	 
	pid_t		__curid;	 

	int		__excp_type;	 
	ulong_t		__iar;		 
	ulong_t		__msr;		 
	ulong_t		__cr;		 
	ulong_t		__lr;		 
	ulong_t		__ctr;		 
	ulong_t		__xer;		 
	ulong_t		__mq;		 
 	ulong_t		__tid;		 
	ulong_t		__fpscr;	 
	char		__fpeu;		 
	char            __fpinfo;        
	uchar_t		__fpscr24_31;	 
	char            __pad[1];        
	ulong_t         __except[5];	 
	char            __pad1[4];       
	ulong_t         __o_iar;         
	ulong_t         __o_toc;         
	ulong_t         __o_arg1;        
	ulong_t		__excbranch;	
#line 126
	ulong_t         __fpscrx;        
	ulong_t         __o_vaddr;       
	ulong_t		__cachealign[7]; 
	adspace_t	__as;		 
	ulong_t		__gpr[32];	 
	double		__fpr[32];	 

};
#line 235
extern char __pmap_stack[];
#line 66 "/usr/include/sys/context.h"
typedef struct {
#line 71
	void  *ss_sp;		 
	size_t ss_size;		 
#line 75
	int    ss_flags;	 

	int    __pad[4];	 

} stack_t;
#line 268
typedef struct {
	unsigned int __v[4];
} __vmxreg_t;

typedef struct __vmx_context {
	__vmxreg_t 	__vr[32];	 
	unsigned int	__pad1[3];	 
	unsigned int	__vscr;		 
	unsigned int	__vrsave;	 
	unsigned int	__pad2[3];	 	
} __vmx_context_t;
#line 319
typedef struct __vsx_context {

	unsigned long long __vsr_dw1[32];  
#line 329
} __vsx_context_t;
#line 361
typedef struct __extctx {
	unsigned int	__flags;	 
	unsigned int	__rsvd1[3];	 
	__vmx_context_t	__vmx;		 

	unsigned int	__ukeys[2];	 
#line 369
	__vsx_context_t	__vsx;		 
	char	__reserved[(4096 - sizeof(__vmx_context_t) - (7 * sizeof(int)) - (sizeof(__vsx_context_t)))]; 
#line 373
	int		__extctx_magic;   
#line 377
} __extctx_t;
#line 384
struct	__jmpbuf {
#line 388
	struct	__mstsave __jmp_context;

}; 
#line 427
struct	__sigcontext {
#line 438
	int		__sc_onstack;	 
#line 442
	sigset_t	__sc_mask;	 

	int		__sc_uerror;	 
	struct __jmpbuf __sc_jmpbuf;	 

};
#line 456
typedef struct __jmpbuf   mcontext_t;

typedef struct ucontext_t {
	int	    __sc_onstack;  
#line 464
        sigset_t    uc_sigmask;   
#line 467
        int	    __sc_uerror;   
	mcontext_t  uc_mcontext;   
#line 472
	struct ucontext_t *uc_link; 
#line 475
	stack_t     uc_stack;      
#line 479
	__extctx_t  *__extctx;	   
#line 482
	int	__extctx_magic;   
#line 488
	int	    __pad[2];	   
#line 496
} ucontext_t;
#line 640 "/usr/include/sys/signal.h"
struct sigevent {
	union sigval		sigev_value;
	int			sigev_signo;
	int			sigev_notify;
#line 648
	void			(*sigev_notify_function)(union sigval);
	pthread_attr_t *	sigev_notify_attributes;

};
#line 1119
extern int kill(pid_t, int);
extern int sigprocmask(int, const sigset_t *restrict, sigset_t *restrict);
extern int sigsuspend(const sigset_t *);
#line 1127
extern int sigwait(const sigset_t *restrict, int *restrict);
#line 1131
extern int sigaction(int, const struct sigaction *restrict, struct sigaction *restrict);

extern int sigemptyset(sigset_t *);
extern int sigfillset(sigset_t *);
extern int sigaddset(sigset_t *, int);
extern int sigdelset(sigset_t *, int);
extern int sigismember(const sigset_t *, int);
extern int sigpending(sigset_t *);
#line 1144
extern int killpg(pid_t, int);
extern int sighold(int);
extern int sigignore(int);
extern int siginterrupt(int, int);
extern int sigpause(int);
extern int sigrelse(int);
extern void (*sigset(int, void(*)(int)))(int);

extern int sigaltstack(const stack_t *restrict, stack_t *restrict);
#line 1159
extern int pthread_kill(pthread_t, int);
extern int pthread_sigmask(int, const sigset_t *, sigset_t *);
extern int sigqueue(pid_t, int, const union sigval);
struct timespec;
extern int sigtimedwait(const sigset_t *restrict, siginfo_t *restrict, const struct timespec *restrict);
extern int sigwaitinfo(const sigset_t *restrict, siginfo_t *restrict);
#line 142 "/usr/include/sys/wait.h"
typedef enum {P_ALL, P_PID, P_PGID} idtype_t;
#line 160
extern pid_t wait(int *);

extern pid_t waitpid(pid_t, int *, int);
#line 176
extern int waitid(idtype_t, id_t, siginfo_t *, int);
#line 421 "/usr/include/stdlib.h"
	extern double 		drand48(void);
	extern double 		erand48(unsigned short[]);
	extern long 		jrand48(unsigned short[]);
	extern void 		lcong48(unsigned short int *);
	extern long 		lrand48(void);
	extern long 		mrand48(void);
	extern long 		nrand48(unsigned short[]);
	extern unsigned short 	*seed48(unsigned short[]);
	extern void 		setkey(const char *);
	extern void 		srand48(long);

	extern int 		putenv(char *);
#line 450
	extern	int	rand_r(unsigned int *);
#line 474
	extern long a64l(const char *);
#line 481
	extern int  getsubopt(char **, char *const *, char **);
	extern int  grantpt(int);
	extern char *initstate(unsigned, char *, size_t);
	extern char *l64a(long);
	extern int  mkstemp(char *);

	extern char *mkdtemp(char *);
#line 495
	extern char *ptsname(int);
	extern long random(void);
	extern char *realpath(const char *, char *);
#line 501
	extern char *setstate(char *);

	extern void srandom(unsigned);
	extern int  unlockpt(int);
#line 520
	extern int  	posix_openpt	(int);
	extern int      setenv		(const char *, const char *, int);
	extern int      unsetenv	(const char *);
#line 241 "/usr/vac/include/stdlib.h"
# pragma info(restore)
#line 20 "/usr/vac/include/string.h"
# pragma info(none)
#line 150 "/usr/include/string.h"
        extern void     	*memchr(const void *, int, size_t);

	extern void     *memcpy(void * restrict, const void * restrict, size_t);
        extern void     *memset(void *, int, size_t);
#line 157
        extern size_t   strcspn(const char *, const char *);
        extern size_t   strlen(const char *);
        extern size_t   strspn(const char *, const char *);
#line 164
	typedef void * locale_t;
#line 205
	extern char		*strchr(const char *, int);
	extern char		*strpbrk(const char *, const char *);
	extern char		*strrchr(const char *, int);
	extern char		*strstr(const char *, const char *);
#line 211
        extern void     *memmove(void *, const void *, size_t);
        extern char     *strcpy(char * restrict, const char * restrict);
        extern char     *strncpy(char * restrict, const char * restrict, size_t);
        extern char     *strcat(char * restrict, const char * restrict);
        extern char     *strncat(char * restrict, const char * restrict, size_t);
        extern int      memcmp(const void *, const void *,size_t);
        extern int      strcmp(const char *, const char *);
        extern int      strncmp(const char *,const char *,size_t);
        extern int      strncollen(const char *, const int );
        extern char     *strtok(char * restrict, const char * restrict);
        extern char     *strerror(int);
        extern int      strcoll(const char *, const char *);
        extern size_t strxfrm(char * restrict, const char * restrict, size_t);

		extern int      strcoll_l(const char *, const char *, locale_t);
		extern size_t strxfrm_l(char * restrict, const char * restrict, size_t, locale_t);

		extern char *strtok_r(char *, const char *, char **);

	    extern int	strerror_r(int, char *, size_t);
#line 242
        extern void     *memccpy(void * restrict, const void * restrict, int, size_t);
#line 251
extern char *strdup(const char *);
#line 202 "/usr/vac/include/string.h"
# pragma info(restore)
#line 156 "/usr/include/unistd.h"
extern int access(const char *, int);

extern int faccessat(int, const char *, int, int);

extern unsigned int alarm(unsigned int);
extern int chdir(const char *);
extern int chown(const char *, uid_t, gid_t);

extern int fchownat(int, const char *, uid_t, gid_t, int);

extern int close(int);
extern char *ctermid(char *);
extern int dup(int);
extern int dup2(int, int);
extern int execl(const char *, const char *, ...);
extern int execv(const char *, char *const []);
extern int execle(const char *, const char *, ...);
extern int execve(const char *, char *const [], char *const []);

extern int fexecve(int, char *const [], char *const []);

extern int execlp(const char *, const char *, ...);
extern int execvp(const char *, char *const []);
extern void _exit(int);
extern pid_t fork(void);
extern long fpathconf(int, int);
extern char *getcwd(char *, size_t);
extern gid_t getegid(void);
extern uid_t geteuid(void);
extern gid_t getgid(void);
extern int getgroups(int, gid_t []);
extern char *getlogin(void);

extern pid_t getpgrp(void);

extern pid_t getpid(void);
extern pid_t getppid(void);
extern uid_t getuid(void);
extern int isatty(int);
extern int link(const char *, const char *);

extern int linkat(int, const char *, int, const char *, int);

extern off_t lseek(int, off_t, int);
#line 203
extern long pathconf(const char *, int);
extern int pause(void);
extern int pipe(int []);

extern int pthread_atfork(void (*)(void), void (*)(void), void (*)(void));
#line 212
extern ssize_t read(int, void *, size_t);
extern int rmdir(const char *);
extern int setgid(gid_t);
extern int setpgid(pid_t, pid_t);
extern pid_t setsid(void);
extern int setuid(uid_t);
extern unsigned int sleep(unsigned int);
extern long sysconf(int);
extern pid_t tcgetpgrp(int);
extern int tcsetpgrp(int, pid_t);
extern char *ttyname(int);
extern int unlink(const char *);

extern int unlinkat(int, const char *, int);

extern ssize_t write(int, const void *, size_t);
#line 330
extern  char    *optarg;
extern  int     optind, opterr, optopt;
#line 346
	extern	size_t	confstr(int, char*, size_t);
	extern  char    *crypt(const char *, const char *);
	extern  void    encrypt(char *, int);
	extern  int     fsync(int);
	extern	int	getopt(int, char* const*, const char*);
	extern	int	nice(int);
	extern  void    swab(const void *, void *, ssize_t);
	extern int	fdatasync(int);
#line 62 "/usr/include/sys/lockf.h"
	extern int lockf (int, int, off_t);
#line 913 "/usr/include/unistd.h"
	extern int		fchdir(int);
	extern int		fchown(int, uid_t, gid_t);
	extern int		ftruncate(int, off_t);
#line 919
	extern int		gethostname(char *, size_t);
	extern long		gethostid(void);
	extern pid_t		getpgid(pid_t);
	extern pid_t		getsid(pid_t);
#line 926
	extern int		lchown(const char *, uid_t, gid_t);
#line 949
	extern ssize_t readlink(const char *restrict, char *restrict, size_t);

	extern ssize_t readlinkat(int, const char *restrict,
		char *restrict, size_t);
#line 963
	extern pid_t		setpgrp(void);

	extern int		setregid(gid_t, gid_t);
	extern int		setreuid(uid_t, uid_t);
	extern int		symlink(const char *, const char *);

	extern int		symlinkat(const char *, int, const char *);

	extern void		sync(void);
	extern int		truncate(const char *, off_t);
#line 982
	extern int		getlogin_r(char *, size_t);
	extern int		ttyname_r(int, char *, size_t);
#line 990
	extern ssize_t		pread(int, void *, size_t, off_t);
	extern ssize_t		pwrite(int, const void *, size_t, off_t);
#line 45 "../include/apue.h"
typedef    void    Sigfunc(int);     
#line 53
char    *path_alloc(size_t *);                 
long     open_max(void);                     

int         set_cloexec(int);                     
void     clr_fl(int, int);
void     set_fl(int, int);                     

void     pr_exit(int);                         

void     pr_mask(const char *);                 
Sigfunc    *signal_intr(int, Sigfunc *);         

void     daemonize(const char *);             

void     sleep_us(unsigned int);             
ssize_t     readn(int, void *, size_t);         
ssize_t     writen(int, const void *, size_t);     

int         fd_pipe(int *);                     
int         recv_fd(int, ssize_t (*func)(int,
                 const void *, size_t));     
int         send_fd(int, int);                     
int         send_err(int, int,
                  const char *);             
int         serv_listen(const char *);             
int         serv_accept(int, uid_t *);             
int         cli_conn(const char *);             
int         buf_args(char *, int (*func)(int,
                  char **));                 

int         tty_cbreak(int);                     
int         tty_raw(int);                         
int         tty_reset(int);                     
void     tty_atexit(void);                     
struct termios    *tty_termios(void);             

int         ptym_open(char *, int);             
int         ptys_open(char *);                     

pid_t     pty_fork(int *, char *, int, const struct termios *,
                  const struct winsize *);     
#line 96
int        lock_reg(int, int, int, off_t, int, off_t);  
#line 109
pid_t    lock_test(int, int, off_t, int, off_t);         
#line 116
void    err_msg(const char *, ...);             
void    err_dump(const char *, ...) __attribute__((noreturn));
void    err_quit(const char *, ...) __attribute__((noreturn));
void    err_cont(int, const char *, ...);
void    err_exit(int, const char *, ...) __attribute__((noreturn));
void    err_ret(const char *, ...);
void    err_sys(const char *, ...) __attribute__((noreturn));

void    log_msg(const char *, ...);             
void    log_open(const char *, int, int);
void    log_quit(const char *, ...) __attribute__((noreturn));
void    log_ret(const char *, ...);
void    log_sys(const char *, ...) __attribute__((noreturn));
void    log_exit(int, const char *, ...) __attribute__((noreturn));

void    TELL_WAIT(void);         
void    TELL_PARENT(pid_t);
void    TELL_CHILD(pid_t);
void    WAIT_PARENT(void);
void    WAIT_CHILD(void);
typedef struct winsize struct_winsize;
#line 4 "ptyfork.c"
pid_t
pty_fork(int *ptrfdm, char *slave_name, int slave_namesz,
         const struct termios *slave_termios,
         const struct winsize *slave_winsize)
{
    int        fdm, fds;
    pid_t    pid;
    char    pts_name[20];

    if ((fdm = ptym_open(pts_name, sizeof(pts_name))) < 0)
        err_sys("can't open master pty: %s, error %d", pts_name, fdm);

    if (slave_name != 0) {
#line 21
        __strncpy(slave_name, pts_name, slave_namesz);
        slave_name[slave_namesz - 1] = '\0';
    }

    if ((pid = fork()) < 0) {
        return(-1);
    } else if (pid == 0) {         
        if (setsid() < 0)
            err_sys("setsid error");
#line 34
        if ((fds = ptys_open(pts_name)) < 0)
            err_sys("can't open slave pty");
        close(fdm);         
#line 48
        if (slave_termios != 0) {
            if (tcsetattr(fds, 0, slave_termios) < 0)
                err_sys("tcsetattr error on slave pty");
        }
		unsigned long req = ((0x40000000 << 1) | ( ((sizeof(struct_winsize))  &0x7f) << 16) | ('t'<<8) | 103);
        if (slave_winsize != 0) {
            if (ioctl(fds, req, slave_winsize) < 0)
                err_sys("TIOCSWINSZ error on slave pty");
        }
#line 60
        if (dup2(fds, 0) != 0)
            err_sys("dup2 error to stdin");
        if (dup2(fds, 1) != 1)
            err_sys("dup2 error to stdout");
        if (dup2(fds, 2) != 2)
            err_sys("dup2 error to stderr");
        if (fds != 0 && fds != 1 &&
          fds != 2)
            close(fds);
        return(0);         
    } else {                     
        *ptrfdm = fdm;     
        return(pid);     
    }
}
