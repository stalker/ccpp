// tests.h
/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
#ifndef _TESTS_H_
#define _TESTS_H_

#include <cassert>
#include <cerrno>
#include <cfloat>
#include <climits>
#include <csetjmp>
#include <cstdarg>
#include <cstdbool>
#include <cstddef>

#include <config.h>

extern "C" {
#include <alloca.h>
#include <assert.h>
#include <byteswap.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <locale.h>
#include <regex.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <wchar.h>
#include "areadlink.h"
#include "c-strcasestr.h"
#include "c-strstr.h"
#include "canonicalize.h"
#include "cloexec.h"
#include "closein.h"
#include "closeout.h"
#include "dirent-safer.h"
#include "dirname.h"
#include "error.h"
#include "filemode.h"
#include "fnmatch.h"
#include "hash.h"
#include "human.h"
#include "idcache.h"
#include "mbswidth.h"
#include "modechange.h"
#include "mountlist.h"
#include "parse-datetime.h"
#include "pathmax.h"
#include "progname.h"
#include "quotearg.h"
#include "safe-read.h"
#include "save-cwd.h"
#include "savedir.h"
#include "stat-macros.h"
#include "stat-size.h"
#include "stat-time.h"
#include "stdio-safer.h"
#include "strftime.h"
#include "timespec.h"
#include "version-etc.h"
#include "xgetcwd.h"
#include "xstrtod.h"
#include "xstrtol.h"
#include "yesno.h"
}
#include <math.h>
#include "argmatch.h"
#include "fts_.h"
#include "verify.h"
#include "xalloc.h"

#include <algorithm>
#include <cinttypes>
#include <deque>
#include <exception>
#include <fstream>
#include <iostream>
#include <initializer_list> //  C++11
#include <limits>
#include <locale>
#include <sstream>
#include <stdexcept>
#include <string>
#include <typeindex>        //  C++11
#include <typeinfo>
#include <type_traits>      // C++11
#include <utility>

#include <csignal>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <cppue.hpp>
#include <cconfig.h>

int test_accept(void);
int test_alloca_opt(void);
int test_areadlink_with_size(void);
int test_areadlinkat(void);
int test_argmatch(int argc, char *argv[]);
int test_bind(void);
int test_binary_io(int argc, char *argv[]);
int test_btowc(int argc, char *argv[]);
int test_byteswap(void);
int test_c_ctype(void);
int test_c_strcasestr(void);
int test_c_strstr(void);
int test_canonicalize(void);
int test_chdir(void);
int test_cloexec(void);
int test_close(void);
int test_closein(int argc, char **argv);
int test_connect(void);
int test_ctype(void);
int test_dirent_safer(void);
int test_dirent(void);
int test_dirname(void);
int test_dup_safer(void);
int test_dup(void);
int test_dup2(void);
int test_environ(void);
int test_errno(void);

#endif  /* _TESTS_H_ */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
