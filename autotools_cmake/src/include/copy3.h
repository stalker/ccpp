#pragma once
#ifndef _COPY3_H_
#define _COPY3_H_

#include "apue.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define MAXLINE   4096
#define MAXBUFFER 8192
#define BUFSIZE   MAXBUFFER

#define MASK644 (S_IRWXU | S_IRGRP | S_IROTH)

FILE * fopen_on_err_exit(const char * filename, const char * mode);
FILE * fopen_on_err_return(const char * filename, const char * mode);
int open_ex_err_return(const char * filename);
int open_ex_err_exit(const char * filename);
int open_rw_err_exit(const char * filename);

int copy_fp0(FILE * fdest, FILE * fsource);
int copy_fp1(FILE * fdest, FILE * fsource);
int copy_fd(int ddest, int dsource);
#define copy_fp copy_fp1

FILE * copy_fp_to_fd1(int dest, FILE * fsource);
FILE * copy_fp_to_fd2(int dest, FILE * fsource);
FILE * copy_fp_to_fd9(int dest, FILE * fsource);
#if defined(LINUX)
# define copy_fp_to_fd copy_fp_to_fd9
#elif defined(__APPLE__) || defined(__FreeBSD__)
# define copy_fp_to_fd copy_fp_to_fd2
#else
# define copy_fp_to_fd copy_fp_to_fd2
#endif

int copy_file0(const char * source, const char * dest);
int copy_file1(const char * source, const char * dest);
int copy_file2(const char * source, const char * dest);
int copy_file9(const char * source, const char * dest);
#if defined(LINUX)
# define copy_file copy_file9
#elif defined(__APPLE__) || defined(__FreeBSD__)
# define copy_file copy_file9
#else
# define copy_file copy_file2
#endif


void copy_fd_to_fp_chomp(FILE * fp, int fd);

#endif /* _COPY3_H_ */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
