#pragma once
#ifndef _POPEN3_H_
#define _POPEN3_H_

#include "apue.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

typedef void pfunc_t (int rfd, int wfd);

int my_waitpid(pid_t pid);

/**
 * ф-ция  подготовки  и запуска  команды,  а так  же подготовки межпроцессного
 * взаимодействия, данная ф-ция создаёт две трубы (pipe), первая труба (канал)
 * pfd - создаётся  для перенаправления стандартного вывода stdout родителя на
 * стандартный вход stdin потомка запущенного в pfunc, схема:
 *
 * parent ---(stdout)---> [pcfd[1] --- pcfd[0]] ---(stdin)---> pfunc(exec)
 *
 * вторая труба (канал) cfd  -  создаётся   для  перенаправления  стандартного
 * вывода stdout потомка запущенного  в pfunc(exec)  на стандартный вход stdin
 * родителя через файловый дескриптор  POSIX *piop  (в  ф-цию передаётся адрес
 * (указатель) указателя файлового дескриптора), схема:
 *
 * pfunc(exec/child) --(stdout)--> [cpfd[1] --- cpfd[0]] --(stdin)--> parent
 *
 * выход - номер (pid) порождённого процесса (если запустить не удалось -1)
 * piop  - открытый файловый указатель на входной поток от порожденного
 *         потомка, открытие файлового указателя не проверяется!
 */
int popen3(pfunc_t p, const char *in, const char *out, const char *err);

#endif /* _POPEN3_H_ */

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
