/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

/*
 */
#include <cstring>
#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

#include "disjoint_set_union.hpp"

#define DEBUG 1

const char * sp = " ";

#define FF_11_6 setiosflags(ios::fixed) \
             << setiosflags(ios::right) \
             << setprecision(6)

using std::string;
using std::vector;
using namespace std;

typedef long long int lli_t;

int go(std::istream& instm, std::ostream& outstm) try {
    bool eq = true;

    lli_t n = 0, e = 0, d = 0;
    instm >> n >> e >> d;

    disjoint_set_union<> us(n + 1);

    for (lli_t i = 0; i < n; ++i) {
        us.make_set(i + 1);
    }

    for (lli_t k = 0; k < e; ++k) {
        lli_t i = 0, j = 0;
        instm >> i >> j;
        us.union_sets(i, j);
    }

    for (lli_t k = 0; k < d; ++k) {
        lli_t p = 0, q = 0;
        instm >> p >> q;
        lli_t sp = us.find(p);
        lli_t sq = us.find(q);
        if (sp == sq) {
            eq = false;
        }
    }
    outstm << eq << '\n';

    return EXIT_SUCCESS;
} catch (exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

int test(const std::string in, std::string out) {
    std::istringstream iss(in);
    std::ostringstream oss;
    go(iss, oss);
    if (oss.str() == out)
        return EXIT_SUCCESS;
    return EXIT_FAILURE;
}

/* let's go */
int main(int argc, char  *argv[]) {
    ios_base::sync_with_stdio(0);
#if defined(DEBUG) && defined(PRINTM)
    printd("argc: ", argc, "; argv: ", argv) << endl;
#endif
    const std::string test1_string = "4 6 0 " "1 2 " "1 3 " "1 4 " "2 3 "
                                     "2 4 " "3 4 ";
    bool t1 = test(test1_string, "1");
    std::cout << "test1: " << std::boolalpha << t1 << std::endl;


    const std::string test2_string = "4 6 1 " "1 2 " "1 3 " "1 4 " "2 3 " 
                                     "2 4 " "3 4 " "1 2";
    bool t2 = test(test2_string, "0");
    std::cout << "test2: " << std::boolalpha << t2 << std::endl;


    const std::string test3_string = "4 0 6 " "1 2 " "1 3 " "1 4 " "2 3 "
                                     "2 4 " "3 4";
    bool t3 = test(test3_string, "1");
    std::cout << "test3: " << std::boolalpha << t2 << std::endl;

    return (t1 && t2 && t3 ? EXIT_SUCCESS : EXIT_FAILURE);
    // return go(std::cin, std::cout);
}

/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
