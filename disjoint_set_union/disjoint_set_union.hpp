/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

#ifndef _DISJOINT_SET_UNION_HPP_
#define _DISJOINT_SET_UNION_HPP_

#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <vector>

const char * __sp = " ";

template <class T = ptrdiff_t, class U = unsigned>
class disjoint_set_union {
public:
    disjoint_set_union(ptrdiff_t n = 0) {
        this->parent.resize(n);
        this->rank.resize(n);
        for (ptrdiff_t idx = 0; idx < this->parent.size(); ++idx)
            this->parent[idx] = static_cast<T>(idx);
    }

    const T& operator[](ptrdiff_t idx) const
    throw(std::range_error) {
        check_index(idx, "operator[]()", __LINE__);
        return this->parent[idx];
    }

    ptrdiff_t size(void) const { return this->parent.size(); }

    /**
     * { }
     * UnionSet_make_set(i)
     *   parent[i] ← i
     */
    ptrdiff_t make_set(T idx, unsigned r = 0)
    throw(std::range_error) {
        check_index(idx, "make_set()", __LINE__);
        this->rank[idx] = r;
        return this->parent[idx] = static_cast<T>(idx);
    }

    /**
     * { }
     * UnionSet_find(i)
     *   пока i <> parent[i]:
     *        parent[i] ← find(parent[i])
     *   вернуть parent[i]
     */
    ptrdiff_t find(T idx)
    throw(std::range_error) {
        check_index(idx, "find()", __LINE__);
        return ( idx == this->parent[idx] ? idx
                : this->parent[idx] = find(this->parent[idx]) );
    }

    /**
     * { }
     * UnionSet_union(i, j)
     *   i ←  find(i)]
     *   j ←  find(j)]
     *   если i = j выход
     *   если rank[i] < rank[j]
     *        parent[j]: ←  parent[i]
     *   иначе
     *        parent[i]: ←  parent[j]
     *        если rank[i] = rank[j]
     *            rank[j]: ←  rank[j] + 1
     */
    void union_sets(T idx, T jdx)
    throw(std::range_error) {
        check_index(idx, "union_sets(idx)", __LINE__);
        check_index(jdx, "union_sets(jdx)", __LINE__);
        if ((idx = find(idx)) == (jdx = find(jdx))) return;

        if (this->rank[idx] < this->rank[jdx]) {
            this->parent[idx] = jdx;
        } else {
            this->parent[jdx] = idx;
            if (this->rank[idx] == this->rank[jdx])
                this->rank[idx] += 1; 
        }
    }

    void check_index(ptrdiff_t idx, const std::string sfun, int line)
    throw(std::range_error) {
        if (0 > idx || idx > this->parent.size())
            throw std::range_error("Union Range Error index: " +
                  std::to_string(idx) + " in: " + sfun + " on line: " +
                  std::to_string(line));
    }

    void check_union_set_empty(const std::string sfun, int line)
    throw(std::range_error) {
        if (0 >= this->parent.size())
            throw std::range_error("Union Range Error size in: " +
                  sfun + " on line: " + std::to_string(line));
    }

    std::ostream& print(std::ostream& stm, const char * sp = __sp) const {
        stm << "i" << sp;
        ptrdiff_t idx = 0; for (; idx < this->parent.size(); ++idx)
            stm << idx << sp;
        stm << std::endl;
        stm << "p" << sp;
        idx = 0; for (; idx < this->parent.size(); ++idx)
            stm << this->parent[idx] << sp;
        stm << std::endl;
        stm << "r" << sp;
        idx = 0; for (; idx < this->parent.size(); ++idx)
            stm << this->rank[idx] << sp;
        stm << std::endl;
        return stm;
    }

    std::vector<T> parent;      // бинарное дерево
    std::vector<U> rank; // ранки /высота/ деревьев построенных для множеств
};

#endif // _DISJOINT_SET_UNION_HPP_
/* vim: syntax=cpp:fileencoding=utf-8:fileformat=unix:tw=78:ts=4:sw=4:sts=4:et
 * EOF */
